// Index
$('#autotable').PioTable({
    baseUrl: laroute.route('notification.list')
});

$('#kt_daterangepicker_4').daterangepicker({
    buttonClasses: ' btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',

    timePicker: true,
    timePickerIncrement: 30,
    locale: {
        format: 'MM/DD/YYYY h:mm A'
    }
}, function (start, end, label) {
    $('#kt_daterangepicker_4 .form-control').val(start.format('MM/DD/YYYY h:mm A') + ' / ' + end.format('MM/DD/YYYY h:mm A'));
});

$(".is_actived").change(function () {
    var notiId = $(this).attr('data-id');
    var check;
    this.checked ? check = 1 : check = 0;
    var time = $(this).attr("data-non-specific-value");
    var type = $(this).attr("data-non-specific-type");
    // dùng để hiển thị lại giờ tương đối
    var oldTime = $("#time-"+notiId).text();
    $.ajax({
        method: 'POST',
        url: laroute.route('admin.notification.updateIsActived', {id:notiId}),
        data: {
            check: check,
            non_specific_value: time,
            non_specific_type: type,
            old_time: oldTime
        },
        success: function (result) {
            if (result.success == 1) {
                toastr.success('Cập nhật thành công!');
                if (check == 1) {
                    $("#status-"+notiId).html(statusPending);
                } else if (check == 0) {
                    $("#status-"+notiId).html(statusNot);
                }

                if (time != '' && type != '') {
                    $("#time-"+notiId).html(result.send_at);
                }
            } else {
                swal.fire("", result.message, "error").then(function (result) {
                    location.reload();
                });
                // $(this).attr('checked', false);
            }
        }
    });
});

function removeItem(id) {
    swal.fire({
        title: 'Bạn có muốn xóa thông báo không',
        html: 'Khi thông báo bị xóa sẽ không thẻ khôi phục lại được.<br/> Bạn có chắc chắn muốn xóa thông báo này không?',
        buttonsStyling: false,

        confirmButtonText: 'Đồng ý xóa!',
        confirmButtonClass: "btn btn-sm btn-default btn-bold",

        showCancelButton: true,
        cancelButtonText: 'Không xóa',
        cancelButtonClass: "btn btn-sm btn-bold btn-brand"
    }).then(function (result) {
        if (result.value) {
            $.post(laroute.route("admin.notification.destroy", {id:id}), function (res) {
                if (res.error == false) {
                    swal.fire(res.message, "", "success").then(function () {
                        location.reload();
                    });
                } else {
                    swal.fire(res.message, "", "error");
                }
            })
        }
    });
}

// Xử lý thao tác
$("select[name=action]").change(function () {
    var value = $(this).children(":selected").val();
    if (value == 0) {
        return false;
    } else if (value != 'delete') {
        window.location.href = value;
    } else {
        var route = $(this).children(":selected").attr("data-route");
        var id = $(this).children(":selected").attr("data-id");
        removeItem(route, id);
    }
});
// End Index


var detailType = 0;
var activeDelete = 0; // lần đầu load trang sẽ ko xóa params nếu có
$("#end_point").change(function () {
    // xóa detail nếu có
    if (activeDelete == 1) {
        $("input[name=end_point_detail]").val('');
        $("input[name=end_point_detail_click]").val('');
    }
    activeDelete = 1;
    // $("input[name=end_point_detail]").val('');
    $("#end-point-detail").css("display", "none");
    $("input[name=end_point_detail]").attr('disabled', 'disabled');
    $("#end-point-modal").html('');

    var detailType1 = $(this).children(":selected").attr("data-type");
    if (detailType1 != '') {
        $("#end-point-detail").css("display", "flex");
        $("input[name=end_point_detail]").removeAttr('disabled');
        detailType = detailType1;
    }

    $("input[name=is_detail]").val($(this).children(":selected").attr("is-detail"));
    $("input[name=notification_type_id]").val($(this).children(":selected").attr("data-id"));
});

function handleClick() {
    $.getJSON('/admin/validation', function (json) {
        $.ajax({
            url: laroute.route("admin.notification.detailEndPoint"),
            method: "GET",
            data: {
                view: 'modal',
                detail_type: detailType
            },
            success: function (res) {
                end_point_value = $("input[name=end_point_detail]").val();
                $("#end-point-modal").html(res);
                $("#end-point-modal").find("#modal-end-point").modal();
            }
        });
    });
}

$('#specific_time').datetimepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'top-left',
    startDate:new Date(),
    format: 'dd-mm-yyyy h:ii'
});

$("input[name=send_time_radio]").change(function () {
    radio = $("input[name=send_time_radio]:checked").val();
    if (radio == 1) {
        $("#schedule-time").css("display", "flex");
    } else {
        $("#schedule-time").css("display", "none");
    }
});

$("select[name=schedule_time]").change(function () {
    value = $(this).val();
    if (value == 'non_specific_time') {
        $("#non_specific_time_display").css("display", "block");
        $("#specific_time_display").css("display", "none");
        $("#specific_time").removeClass("is-invalid");
        $("#specific_time-error").remove();
        $(".invalid-feedback").remove();
    }
    if (value == 'specific_time') {
        $("#specific_time_display").css("display", "flex");
        $("#non_specific_time_display").css("display", "none");
        $("#non_specific_time").removeClass("is-invalid");
        $("#non_specific_time-error").remove();
        $(".invalid-feedback").remove();
    }
});

$("select[name=action_group]").change(function () {
    value = $(this).val();
    if (value == 1) {
        $("#cover-action").removeAttr('style');
        $("input[name=action_name]").removeAttr('disabled');
    } else {
        $("#cover-action").css("display", "none");
        $("input[name=action_name]").attr('disabled', 'disabled');
    }
});

// trigger change cho view edit
$("select[name=action_group]").change();
$("#end_point").change();
$("input[name=send_time_radio]").change();

var script = {
    uploadAvatar: function (input) {
        if (input.files && input.files[0]) {
            if (input.files[0].type == 'image/jpeg'
                || input.files[0].type == 'image/png'
                || input.files[0].type == 'image/jpeg'
                || input.files[0].type == 'jpg|png|jpeg'
            ) {
                var reader = new FileReader();
                var imageAvatar = $('#image');
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
                var file_data = $('#getFile').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('link', '_notification.');
                var fsize = input.files[0].size;
                var fileInput = input,
                    file = fileInput.files && fileInput.files[0];
                var img = new Image();

                img.src = window.URL.createObjectURL(file);

                img.onload = function () {
                    var imageWidth = img.naturalWidth;
                    var imageHeight = img.naturalHeight;

                    window.URL.revokeObjectURL(img.src);

                    $('.image-size').text(imageWidth + "x" + imageHeight + "px");

                };
                $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

                $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

                if (Math.round(fsize / 1024) <= 10240) {
                    $.ajax({
                        url: laroute.route("config.upload"),
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (res) {
                            if (res.success == 1) {
                                $('#background').val(res.file);
                            }
                        }
                    });
                } else {
                    swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
                }
            } else {
                swal("File không đúng định dạng", "", "error");
            }
        }
    },
    submit_add: function (is_quit) {
        $(".invalid-feedback").remove();
        $.getJSON('/admin/validation', function (json) {
            var form = $('#form-add');
            rules = {
                title_vi: {required: true, maxlength: 255},
                title_en: {required: true, maxlength: 255},
                description_vi: {required: true, maxlength: 255},
                description_en: {required: true, maxlength: 255},
                specific_time: {required: true},
                background: {required: true},
            };

            messages = {
                title_vi: {
                    required: 'Yêu cầu nhập tiêu đề thông báo (VI)',
                    maxlength: 'Tiêu đề thông báo (VI) vượt quá 255 ký tự'
                },
                title_en: {
                    required: 'Yêu cầu nhập tiêu đề thông báo (EN)',
                    maxlength: 'Tiêu đề thông báo (EN) vượt quá 255 ký tự'
                },
                description_vi: {
                    required: 'Yêu cầu nhập chi tiết thông báo (VI)',
                    maxlength: 'Chi tiết thông báo (VI) vượt quá 255 ký tự'
                },
                description_en: {
                    required: 'Yêu cầu nhập chi tiết thông báo (EN)',
                    maxlength: 'Chi tiết thông báo (EN) vượt quá 255 ký tự'
                },
                specific_time: {
                    required: 'Yêu cầu chọn thời gian gửi thông báo',
                },
                background: {
                    required: 'Yêu cầu chọn ảnh background',
                },

            };

            form.validate({
                rules: rules,
                messages: messages
            });

            if (!form.valid()) {
                return false;
            }
            $.ajax({
                url: laroute.route('admin.notification.store'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#form-add').serialize(),
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message, "", 'error');
                    } else {
                        setTimeout(function () {
                            swal(res.message, "", 'success').then(function (result) {
                                if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                    window.location.href = laroute.route('admin.notification');
                                }
                                if (result.value == true) {
                                    if (is_quit === 0) {
                                        window.location.reload();
                                    } else {
                                        window.location.href = laroute.route('admin.notification');
                                    }
                                }
                            });
                        }, 1500);
                    }

                },
                error: function (res) {
                    var mess_error = '';
                    jQuery.each(res.responseJSON.errors, function (key, val) {
                        mess_error = mess_error.concat(val + '<br/>');
                    });
                    swal("", mess_error, "error");
                }
            });
        });
    },
    submit_edit: function ($noti_template_id) {
        $(".invalid-feedback").remove();
        var form = $('#form-add');
        rules = {
            title_vi: {required: true, maxlength: 255},
            title_en: {required: true, maxlength: 255},
            description_vi: {required: true, maxlength: 255},
            description_en: {required: true, maxlength: 255},
            specific_time: {required: true},
            background: {required: true},
        };

        messages = {
            title_vi: {
                required: 'Yêu cầu nhập tiêu đề thông báo (VI)',
                maxlength: 'Tiêu đề thông báo (VI) vượt quá 255 ký tự'
            },
            title_en: {
                required: 'Yêu cầu nhập tiêu đề thông báo (EN)',
                maxlength: 'Tiêu đề thông báo (EN) vượt quá 255 ký tự'
            },
            description_vi: {
                required: 'Yêu cầu nhập chi tiết thông báo (VI)',
                maxlength: 'Chi tiết thông báo (VI) vượt quá 255 ký tự'
            },
            description_en: {
                required: 'Yêu cầu nhập chi tiết thông báo (EN)',
                maxlength: 'Chi tiết thông báo (EN) vượt quá 255 ký tự'
            },
            specific_time: {
                required: 'Yêu cầu chọn thời gian gửi thông báo',
            },
            background: {
                required: 'Yêu cầu chọn ảnh background',
            },

        };

        form.validate({
            rules: rules,
            messages: messages
        });

        if (!form.valid()) {
            return false;
        }
        $.ajax({
            url: laroute.route('admin.notification.update',{'id' : $noti_template_id }),
            method: 'POST',
            dataType: 'JSON',
            data: $('#form-add').serialize(),
            success: function (res) {
                if (res.error == true) {
                    swal(res.message, "", 'error');
                } else {
                    setTimeout(function () {
                        swal(res.message, "", 'success').then(function (result) {
                            window.location.href = laroute.route('admin.notification');
                        });
                    }, 1500);
                }

            },
            error: function (res) {
                var mess_error = '';
                jQuery.each(res.responseJSON.errors, function (key, val) {
                    mess_error = mess_error.concat(val + '<br/>');
                });
                swal("", mess_error, "error");
            }
        });
    },
    showPopup: function() {
        script.searchPopup(1);
    },

    deleteSearch: function(){
        $('#service_name').val('');
        $('#service_category').val('').trigger('change');
        script.searchPopup(1);
    },

    searchPopup: function(page){
        $.ajax({
            url: laroute.route("admin.notification.getListService"),
            method: "POST",
            data: $('#search_service').serialize()+'&page='+page,
            success: function (res) {
                $('.table-service').empty();
                $('.table-service').append(res.view);
            }
        });
    },

    addListService: function () {
        var listService = [];
        $.each($('.table_service_popup').find(".service_id"), function () {
            if ($(this).is(':checked')){
                listService.push($(this).val());
            }
        });
        $.ajax({
            url: laroute.route("admin.notification.addListService"),
            method: "POST",
            data: {
                listService : listService
            },
            success: function (res) {
                swal.fire('Thêm dịch vụ thành công', "", "success").then(function () {
                    if(res.error == false) {
                        $('.list_add_service').empty();
                        $('.list_add_service').append(res.view);
                    }
                });
            }
        });
    },

    removeService : function (service_id) {
        $.ajax({
            url: laroute.route("admin.notification.removeService"),
            method: "POST",
            data: {
                service_id : service_id
            },
            success: function (res) {
                swal.fire('Xóa dịch vụ thành công', "", "success").then(function () {
                    if(res.error == false) {
                        $('.list_add_service').empty();
                        $('.list_add_service').append(res.view);
                    }
                });
            }
        });
    },

    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('').trigger('change');
        $('#created_at').val('');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    },
};
$(document).ready(function () {
    $('.voucher').change(function () {
        if ($('.voucher').prop('checked')){
            $('.select-voucher').prop('disabled',false);
            $('.input-money').val(null);
            $('.input-money').prop('disabled',true);
        } else {
            $('.input-money').prop('disabled',false);
            $('.select-voucher').prop('disabled',true);
        }
    });

    $('.money').change(function () {
        if ($('.money').prop('checked')){
            $('.input-money').prop('disabled',false);
            $('.select-voucher').prop('disabled',true);
        } else {
            $('.select-voucher').prop('disabled',false);
            $('.input-money').val(null);
            $('.input-money').prop('disabled',true);
        }
    });

    $('.from-all').click(function () {
        if ($('.from-all').prop('checked')){
            $('.rank').prop('checked',false);
            $('.rank').prop('disabled',true);
        } else {
            $('.rank').prop('disabled',false);
        }
    });

    $('#popup_service').on('hidden.bs.modal', function (e) {
        script.deleteSearch();
    });

    new AutoNumeric.multiple('.number-money', {
        currencySymbol: '',
        decimalCharacter: '.',
        digitGroupSeparator: ',',
        decimalPlaces: 2
    });
})