var detail = {
    _init:function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#transport_id').select2({
                placeholder: json['Chọn hình thức giao']
            });

            $('#delivery_staff').select2({
                placeholder: json['Chọn nhân viên']
            });
        });


        // $('#amount').mask('000,000,000', {reverse: true});

        $("#time_ship").datetimepicker({
            todayHighlight: !0,
            autoclose: !0,
            pickerPosition: "bottom-left",
            format: "dd/mm/yyyy hh:ii",
            // minDate: new Date(),
            // locale: 'vi'
        });

        $('.quantity').ForceNumericOnly();
    },
    editHistory: function (deliveryHistoryId) {
        $.ajax({
            url: laroute.route('delivery.edit-history'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                deliver_history_id: deliveryHistoryId
            },
            success: function (res) {
                $('#my-modal').html(res.url);
                $('#modal-edit').modal('show');

                $.getJSON(laroute.route('translate'), function (json) {
                    $('#delivery_staff').select2({
                        placeholder: json['Chọn nhân viên']
                    });
                });

                $("#time_ship").datetimepicker({
                    todayHighlight: !0,
                    autoclose: !0,
                    pickerPosition: "bottom-left",
                    format: "dd/mm/yyyy hh:ii",
                    // minDate: new Date(),
                    // locale: 'vi'
                });
            }
        });
    },
    submitEditHistory: function (deliveryHistoryId) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-edit');

            form.validate({
                rules: {
                    time_ship: {
                        required: true
                    },
                    delivery_staff: {
                        required: true
                    }
                },
                messages: {
                    time_ship: {
                        required: json['Hãy chọn thời gian giao hàng dự kiến']
                    },
                    delivery_staff: {
                        required: json['Hãy chọn nhân viên giao hàng'],
                    }
                },
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('delivery.update-history'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    delivery_history_id: deliveryHistoryId,
                    time_ship: $('#time_ship').val(),
                    delivery_staff: $('#delivery_staff').val()
                },
                success:function (res) {
                    if (res.error == false) {
                        swal(res.message, "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                window.location.reload();
                            }
                            if (result.value == true) {
                                window.location.reload();
                            }
                        });
                    } else {
                        swal(res.message, '', "error");
                    }
                }
            });
        });
    }
};

var edit = {
    _init:function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#transport_id').select2({
                placeholder: json['Chọn hình thức giao']
            });

            $('#delivery_staff').select2({
                placeholder: json['Chọn nhân viên']
            });
        });


        $('#amount').mask('000,000,000', {reverse: true});

        $("#time_ship").datetimepicker({
            todayHighlight: !0,
            autoclose: !0,
            pickerPosition: "bottom-left",
            format: "dd/mm/yyyy hh:ii",
            // minDate: new Date(),
            // locale: 'vi'
        });

        $('.quantity').ForceNumericOnly();
    },
    save:function (deliveryHistoryId) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-edit');

            form.validate({
                rules: {
                    contact_name: {
                        required: true,
                        maxlength: 250
                    },
                    contact_phone: {
                        required: true,
                        integer: true,
                        maxlength: 10
                    },
                    contact_address: {
                        required: true,
                        maxlength: 250
                    },
                    time_ship: {
                        required: true
                    },
                    delivery_staff: {
                        required: true
                    },
                    transport_code: {
                        maxlength: 250
                    },
                    pick_up: {
                        required: true,
                        maxlength: 250
                    }
                },
                messages: {
                    contact_name: {
                        required: json['Hãy nhập người nhận'],
                        maxlength: json['Người nhận tối đa 250 kí tự']
                    },
                    contact_phone: {
                        required: json['Hãy nhập số điện thoại người nhận'],
                        integer: json['Số điện thoại người nhận không hợp lệ'],
                        maxlength: json['Số điện thoại người nhận tối đa 10 kí tự']
                    },
                    contact_address: {
                        required: json['Hãy nhập địa chỉ người nhận'],
                        maxlength: json['Địa chỉ người nhận tối đa 250 kí tự']
                    },
                    time_ship: {
                        required: json['Hãy chọn thời gian giao hàng dự kiến']
                    },
                    delivery_staff: {
                        required: json['Hãy chọn nhân viên giao hàng'],
                    },
                    transport_code: {
                        maxlength: json['Mã đơn vị vận chuyển tối đa 250 kí tự']
                    },
                    pick_up: {
                        required: json['Hãy nhập nơi lấy hàng'],
                        maxlength: json['Nơi lấy hàng tối đa 250 kí tự']
                    }
                },
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('delivery-history.update'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    delivery_history_id: deliveryHistoryId,
                    contact_name: $('#contact_name').val(),
                    contact_phone: $('#contact_phone').val(),
                    contact_address: $('#contact_address').val(),
                    time_ship: $('#time_ship').val(),
                    delivery_staff: $('#delivery_staff').val(),
                    transport_code: $('#transport_code').val(),
                    pick_up: $('#pick_up').val(),
                    note: $('#note').val(),
                    transport_id: $('#transport_id').val()
                },
                success:function (res) {
                    if (res.error == false) {
                        swal(res.message, "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                window.location.reload();
                            }
                            if (result.value == true) {
                                window.location.reload();
                            }
                        });
                    } else {
                        swal(res.message, '', "error");
                    }
                }
            });
        });
    }
};

$('#autotable').PioTable({
    baseUrl: laroute.route('delivery-history.list')
});