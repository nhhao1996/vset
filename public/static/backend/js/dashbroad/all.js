var All = {
    status: null,
    queue: null,
    pioTable: null,
    year: null,
    start: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $('.m_selectpicker').selectpicker();

            $('#m_year').on('change', function () {
                All.year = this.value;
                All.orderChart('', All.year);

                $('#m_month').val('').selectpicker('refresh')
            });

            $('#m_month').on('change', function () {
                var month = this.value;
                All.orderChart(month, All.year);
            });

            var a = moment(),
                t = moment();

            var arrRange = {};
            arrRange[json['Hôm nay']] = [moment(), moment()],
                arrRange[json['Hôm qua']] = [moment().subtract(1, "days"), moment().subtract(1, "days")],
                arrRange[json["7 ngày trước"]] = [moment().subtract(6, "days"), moment()],
                arrRange[json["30 ngày trước"]] = [moment().subtract(29, "days"), moment()],
                arrRange[json["Trong tháng"]] = [moment().startOf("month"), moment().endOf("month")],
                arrRange[json["Tháng trước"]] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]

            $("#m_daterangepicker_6").daterangepicker({
                buttonClasses: "m-btn btn",
                applyClass: "btn-primary",
                cancelClass: "btn-secondary",
                autoclose: true,
                startDate: a,
                endDate: t,
                autoApply: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    "applyLabel": "Đồng ý",
                    "cancelLabel": "Thoát",
                    "customRangeLabel": json["Tùy chọn ngày"],
                    daysOfWeek: [
                        json["CN"],
                        json["T2"],
                        json["T3"],
                        json["T4"],
                        json["T5"],
                        json["T6"],
                        json["T7"]
                    ],
                    "monthNames": [
                        json["Tháng 1 năm"],
                        json["Tháng 2 năm"],
                        json["Tháng 3 năm"],
                        json["Tháng 4 năm"],
                        json["Tháng 5 năm"],
                        json["Tháng 6 năm"],
                        json["Tháng 7 năm"],
                        json["Tháng 8 năm"],
                        json["Tháng 9 năm"],
                        json["Tháng 10 năm"],
                        json["Tháng 11 năm"],
                        json["Tháng 12 năm"]
                    ],
                    "firstDay": 1
                },
                ranges: arrRange
            }, function (a, t, n) {
                $("#m_daterangepicker_6 .form-control").val(a.format("DD/MM/YYYYY") + " - " + t.format("DD/MM/YYYY"));
                All.salesChart(a.format('YYYY-MM-DD'), t.format('YYYY-MM-DD'));
            });

            $("#m_daterangepicker_7").daterangepicker({
                buttonClasses: "m-btn btn",
                applyClass: "btn-primary",
                cancelClass: "btn-secondary",
                autoclose: true,
                startDate: a,
                endDate: t,
                autoApply: true,
                locale: {
                    format: 'DD/MM/YYYY',
                    // "applyLabel": "Đồng ý",
                    // "cancelLabel": "Thoát",
                    "customRangeLabel": json["Tùy chọn ngày"],
                    daysOfWeek: [
                        json["CN"],
                        json["T2"],
                        json["T3"],
                        json["T4"],
                        json["T5"],
                        json["T6"],
                        json["T7"]
                    ],
                    "monthNames": [
                        json["Tháng 1 năm"],
                        json["Tháng 2 năm"],
                        json["Tháng 3 năm"],
                        json["Tháng 4 năm"],
                        json["Tháng 5 năm"],
                        json["Tháng 6 năm"],
                        json["Tháng 7 năm"],
                        json["Tháng 8 năm"],
                        json["Tháng 9 năm"],
                        json["Tháng 10 năm"],
                        json["Tháng 11 năm"],
                        json["Tháng 12 năm"]
                    ],
                    "firstDay": 1
                },
                ranges: arrRange
            }, function (a, t, n) {
                $("#m_daterangepicker_7 .form-control").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"));
                All.topSalesChart(a.format('YYYY-MM-DD'), t.format('YYYY-MM-DD'));
            });
        });
    },
    appointmentChart: function () {
        $.get(laroute.route('dashbroad.appointment-by-date'), function (res) {
            AmCharts.makeChart("m_appointments", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "autoMargins": true,
                "marginLeft": 30,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#fff"
                },
                "dataProvider": res,
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left",
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "Ngày",
                    "type": "column",
                    "valueField": "appointment",
                    "dashLengthField": "dashLengthColumn",
                }],
                "categoryField": "date",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0

                }
            });

        });
    },
    orderChart: function (month, year) {
        $.post(laroute.route('dashbroad.order-by-month-year'), {month: month, year: year}, function (res) {
            AmCharts.makeChart("m_orders", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "autoMargins": false,
                "marginLeft": 30,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },
                "dataProvider": res,
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[category]]<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "Tháng",
                    "type": "column",
                    "valueField": "order",
                    "dashLengthField": "dashLengthColumn"
                }],
                "categoryField": "month",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0
                }
            });

        });
    },
    salesChart: function ($form, $to) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.post(laroute.route('dashbroad.order-by-object-type'), {formDate: $form, toDate: $to}, function (res) {
                AmCharts.makeChart("m_amcharts_8", {
                    "theme": "light",
                    "type": "serial",
                    "dataProvider": res,
                    "valueAxes": [{
                        "id": "distanceAxis",
                        "unit": json["VNĐ"],
                        "position": "left",
                    },
                        {
                            "id": "durationAxis",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            // "inside": true,
                            "position": "right"
                        }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": json['Số lượng'] + ": <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "quantity",
                        "valueAxis": "durationAxis"
                    }, {
                        "balloonText":  json['Tổng tiền'] + ": <b>[[value]] " +json['VNĐ'] +"</b>  ",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "clustered": false,
                        "columnWidth": 0.5,
                        "valueField": "amount",
                        "valueAxis": "distanceAxis"
                    }],
                    "plotAreaFillAlphas": 0.1,
                    "categoryField": "type",
                    "categoryAxis": {
                        "gridPosition": "start"
                    }

                });

            });
        });
    },
    topSalesChart: function ($form, $to) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.post(laroute.route('dashbroad.get-top-service'), {formDate: $form, toDate: $to}, function (res) {
                AmCharts.makeChart("m_amcharts_5", {
                    "theme": "light",
                    "type": "serial",
                    "dataProvider": res,
                    "valueAxes": [{
                        "id": "distanceAxis",
                        "amount": " VNĐ",
                        "position": "left",
                    },
                        {
                            "id": "durationAxis",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            // "inside": true,
                            "position": "right"
                        }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText":  json['Số lượng'] + ": <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "used",
                        "valueAxis": "durationAxis"
                    }, {
                        "balloonText": json['Tổng tiền'] + ": <b>[[value]] "+json['VNĐ']+"</b>  ",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "clustered": false,
                        "columnWidth": 0.5,
                        "valueField": "amount",
                        "valueAxis": "distanceAxis"
                    }],
                    "rotate": true,
                    "plotAreaFillAlphas": 0.1,
                    "categoryField": "name",
                    "categoryAxis": {
                        "gridPosition": "start"
                    }

                });

            });
        });
    }
};


All.start();
All.appointmentChart();
All.orderChart();
All.salesChart();
All.topSalesChart();
