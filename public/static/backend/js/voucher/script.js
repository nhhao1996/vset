var stt = 1;
var arrTemplateRemove = [];

var view = {
    _init:function () {
        $(document).ready(function () {
            $('#service_category_id').select2({
                placeholder: 'Chọn nhà cung cấp'
            });

            // $('.summernote').summernote({
            //     height: 150,
            //     placeholder: 'Nhập mô tả chi tiết...',
            //     toolbar: [
            //         ['style', ['bold', 'italic', 'underline']],
            //         ['fontsize', ['fontsize']],
            //         ['color', ['color']],
            //         ['para', ['ul', 'ol', 'paragraph']],
            //     ]
            // });

            new AutoNumeric.multiple('#price_standard' ,{
                currencySymbol : '',
                decimalCharacter : '.',
                digitGroupSeparator : ',',
                decimalPlaces: decimal_number
            });

            $(".valid_from_date, .valid_to_date").datetimepicker({
                language: 'en',
                autoclose: !0,
                format: 'dd/mm/yyyy hh:ii',
                pickerPosition: "bottom-right"
            });
        });
    },
    dropzone:function () {
        Dropzone.options.dropzoneone = {
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictRemoveFile: 'Xóa',
            dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
            dictInvalidFileType: 'Tệp không hợp lệ',
            dictCancelUpload: 'Hủy',
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                var random = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (let z = 0; z < 10; z++) {
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
            },
            init: function () {
                this.on("success", function (file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text', laroute.route('voucher.upload-dropzone'));

                    if (file.status === "success") {
                        //Xóa image trong dropzone
                        $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                            file.previewElement.remove();
                        });
                        $('#dropzoneone').removeClass('dz-started');
                        //Append vào div image
                        let tpl = $('#imageShow').html();
                        tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                        tpl = tpl.replace(/{link_hidden}/g, response);
                        $('#upload-image').append(tpl);
                    }
                });
                this.on('removedfile', function (file,response) {
                    var name = file.upload.filename;
                    $.ajax({
                        url: laroute.route('admin.service.delete-image'),
                        method: "POST",
                        data: {

                            filename: name
                        },
                        success: function () {
                            $("input[class='file_Name']").each(function () {
                                var $this = $(this);
                                if ($this.val() === name) {
                                    $this.remove();
                                }
                            });

                        }
                    });
                });
            }
        }
    },
    remove_img: function (e) {
        $(e).closest('.image-show-child').remove();
    },
    addTemplate: function () {
        var continute = true;

        //check các trường dữ liệu rỗng thì báo lỗi
        $.each($('#table-template').find(".tr_template"), function () {
            var number = $(this).find($('.number')).val();
            var valid_from_date = $(this).find($('.valid_from_date')).val();
            var valid_to_date = $(this).find($('.valid_to_date')).val();

            if (valid_from_date == '') {
                $('.error_valid_from_date_' + number + '').text('Hãy nhập ngày bắt đầu');
                continute = false;
            } else {
                $('.error_valid_from_date_' + number + '').text('');
            }

            if (valid_to_date == '') {
                $('.error_valid_to_date_' + number + '').text('Hãy nhập ngày hết hạn');
                continute = false;
            } else {
                $('.error_valid_to_date_' + number + '').text('');
            }

            if (valid_from_date != '' && valid_to_date != '' && valid_from_date >= valid_to_date) {
                $('.error_valid_to_date_' + number + '').text('Ngày hết hạn phải lớn hơn ngày kết thúc');
                continute = false;
            }
        });



        if (continute == true) {
            stt++;
            //append tr table
            var tpl = $('#tpl-tr-template').html();
            tpl = tpl.replace(/{stt}/g, stt);
            $('#table-template > tbody').append(tpl);

            $(".valid_from_date, .valid_to_date").datetimepicker({
                language: 'en',
                autoclose: !0,
                format: 'dd/mm/yyyy hh:ii',
                pickerPosition: "bottom-right"
            });
        }
    },
    removeTr: function (obj) {
        $(obj).closest('tr').remove();
    },
    removeTrOld:function (obj) {
        $(obj).closest('tr').remove();

        arrTemplateRemove.push({
            service_voucher_template_id: $(obj).closest('tr').find('.service_voucher_template_id').val()
        });
    }
};

var create = {
    save:function () {
        var form = $('#form-register');

        form.validate({
            rules: {
                service_category_id: {
                    required: true
                },
                // service_name_vi: {
                //     required: true,
                //     maxlength: 250
                // },
                // service_name_en: {
                //     required: true,
                //     maxlength: 250
                // },
                price_standard: {
                    required: true
                },
                description: {
                    maxlength: 250
                },
                description_en: {
                    maxlength: 250
                }
            },
            messages: {
                service_category_id: {
                    required: 'Hãy chọn nhà cung cấp'
                },
                service_name_vi: {
                    required: 'Hãy nhập tên gói dịch vụ (VI)',
                    maxlength: 'Tên gói dịch vụ (VI) tối đa 250 kí tự'
                },
                service_name_en: {
                    required: 'Hãy nhập tên gói dịch vụ (EN)',
                    maxlength: 'Tên gói dịch vụ (EN) tối đa 250 kí tự'
                },
                price_standard: {
                    required: 'Hãy nhập giá bán'
                },
                description: {
                    maxlength: 'Mô tả ngắn (VI) tối đa 250 kí tự'
                },
                description_en: {
                    maxlength: 'Mô tả ngắn (EN) tối đa 250 kí tự'
                }
            },
        });

        if (!form.valid()) {
            return false;
        }

        var next = true;

        var serviceImage = [];
        var template = [];

        $.each($('#upload-image').find(".image-show-child"), function () {
            var imgService = $(this).find($('input[name=img-sv]')).val();

            serviceImage.push({
                image: imgService
            });
        });
        var check = 0;
        $.each($('#table-template').find(".tr_template"), function () {
            var number = $(this).find($('.number')).val();
            var valid_from_date = $(this).find($('.valid_from_date')).val();
            var valid_to_date = $(this).find($('.valid_to_date')).val();
            if (valid_from_date == '') {
                $('.error_valid_from_date_' + number + '').text('Hãy nhập ngày bắt đầu');
                next = false;
            } else {
                $('.error_valid_from_date_' + number + '').text('');
            }

            if (valid_to_date == '') {
                $('.error_valid_to_date_' + number + '').text('Hãy nhập ngày hết hạn');
                next = false;
            } else {
                $('.error_valid_to_date_' + number + '').text('');
            }

            // if (valid_from_date != '' && valid_to_date != '' && new Date(valid_from_date) >= new Date(valid_to_date)) {
            //     $('.error_valid_to_date_' + number + '').text('Ngày hết hạn phải lớn hơn ngày kết thúc');
            //     next = false;
            // }

            var is_actived = 0;
            if ($(this).find($('input[name=is_actived]')).is(':checked')) {
                is_actived = 1;
                check = 1;
            }

            template.push({
                valid_from_date: valid_from_date,
                valid_to_date: valid_to_date,
                is_actived: is_actived
            });
        });

        if (template.length == 0) {
            $('.error_table_template').text('Hãy thêm template của gói dịch vụ');
            next = false;
        } else {
            $('.error_table_template').text('');
            if (check == 0) {
                swal("Hãy chọn template", "", "error");
                next = false;
            }
        }
        var is_actived = 0;
        if($('.is_actived').is(':checked')){
            is_actived = 1;
        }


        if (next == true) {
            $.ajax({
                url: laroute.route('voucher.store'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    service_category_id: $('#service_category_id').val(),
                    // service_name_vi: $('#service_name_vi').val(),
                    // service_name_en: $('#service_name_en').val(),
                    price_standard: $('#price_standard').val(),
                    description: $('#description').val(),
                    description_en: $('#description_en').val(),
                    service_avatar_list: $('#service_avatar_list').val(),
                    service_avatar_list_mobile: $('#service_avatar_list_mobile').val(),
                    service_avatar_home: $('#service_avatar_home').val(),
                    service_avatar: $('#service_avatar').val(),
                    service_avatar_mobile: $('#service_avatar_mobile').val(),
                    detail_description: $('#detail_description').val(),
                    detail_description_en: $('#detail_description_en').val(),
                    serviceImage: serviceImage,
                    template: template,
                    is_actived:is_actived
                },
                success: function (res) {
                    if (res.error == false) {
                        swal(res.message, "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                window.location.href = laroute.route('voucher');
                            }
                            if (result.value == true) {
                                window.location.href = laroute.route('voucher');
                            }
                        });
                    } else {
                        swal(res.message, '', "error");
                    }
                },
                error: function (res) {
                    var mess_error = '';
                    $.map(res.responseJSON.errors, function (a) {
                        mess_error = mess_error.concat(a + '<br/>');
                    });
                    swal('Thêm thất bại', mess_error, "error");
                }
            });
        }
    }
};

var edit = {
    save:function (id) {
        var form = $('#form-edit');

        form.validate({
            rules: {
                service_category_id: {
                    required: true
                },
                // service_name_vi: {
                //     required: true,
                //     maxlength: 250
                // },
                // service_name_en: {
                //     required: true,
                //     maxlength: 250
                // },
                price_standard: {
                    required: true
                },
                description: {
                    maxlength: 250
                },
                description_en: {
                    maxlength: 250
                }
            },
            messages: {
                service_category_id: {
                    required: 'Hãy chọn nhà cung cấp'
                },
                service_name_vi: {
                    required: 'Hãy nhập tên gói dịch vụ',
                    maxlength: 'Tên gói dịch vụ tối đa 250 kí tự'
                },
                service_name_en: {
                    required: 'Hãy nhập tên gói dịch vụ (VI)',
                    maxlength: 'Tên gói dịch vụ (EN) tối đa 250 kí tự'
                },
                price_standard: {
                    required: 'Hãy nhập giá bán'
                },
                description: {
                    maxlength: 'Mô tả ngắn (VI) tối đa 250 kí tự'
                },
                description_en: {
                    maxlength: 'Mô tả ngắn (EN) tối đa 250 kí tự'
                }
            },
        });

        if (!form.valid()) {
            return false;
        }

        var next = true;

        var serviceImage = [];
        var template = [];
        var templateOld = [];
        var templateIsActive = [];

        $.each($('#upload-image').find(".image-show-child"), function () {
            var imgService = $(this).find($('input[name=img-sv]')).val();
            var type = $(this).find($('input[name=type]')).val();

            serviceImage.push({
                image: imgService,
                type: type
            });
        });

        var check = 0 ;
        $.each($('#table-template').find(".tr_template"), function () {
            var number = $(this).find($('.number')).val();
            var valid_from_date = $(this).find($('.valid_from_date')).val();
            var valid_to_date = $(this).find($('.valid_to_date')).val();


            if (valid_from_date == '') {
                $('.error_valid_from_date_' + number + '').text('Hãy nhập ngày bắt đầu');
                next = false;
            } else {
                $('.error_valid_from_date_' + number + '').text('');
            }

            if (valid_to_date == '') {
                $('.error_valid_to_date_' + number + '').text('Hãy nhập ngày hết hạn');
                next = false;
            } else {
                $('.error_valid_to_date_' + number + '').text('');
            }

            // if (valid_from_date != '' && valid_to_date != '' && new Date(valid_from_date) >= new Date(valid_to_date)) {
            //     $('.error_valid_to_date_' + number + '').text('Ngày hết hạn phải lớn hơn ngày kết thúc');
            //     next = false;
            // }

            var is_actived = 0;
            if ($(this).find($('input[name=is_actived]')).is(':checked')) {
                is_actived = 1;
                check = 1;
            }

            template.push({
                valid_from_date: valid_from_date,
                valid_to_date: valid_to_date,
                is_actived: is_actived
            });

            templateIsActive.push(is_actived);
        });

        $.each($('#table-template').find(".tr_template_old"), function () {
            var number = $(this).find($('.number')).val();
            var valid_from_date = $(this).find($('.valid_from_date')).val();
            var valid_to_date = $(this).find($('.valid_to_date')).val();
            var service_voucher_template_id = $(this).find($('.service_voucher_template_id')).val();

            if (valid_from_date == '') {
                $('.error_valid_from_date_' + number + '').text('Hãy nhập ngày bắt đầu');
                next = false;
            } else {
                $('.error_valid_from_date_' + number + '').text('');
            }

            if (valid_to_date == '') {
                $('.error_valid_to_date_' + number + '').text('Hãy nhập ngày hết hạn');
                next = false;
            } else {
                $('.error_valid_to_date_' + number + '').text('');
            }

            // if (valid_from_date != '' && valid_to_date != '' && new Date(valid_from_date) >= new Date(valid_to_date)) {
            //     $('.error_valid_to_date_' + number + '').text('Ngày hết hạn phải lớn hơn ngày kết thúc');
            //     next = false;
            // }

            var is_actived = 0;
            if ($(this).find($('input[name=is_actived]')).is(':checked')) {
                is_actived = 1;
                check = 1;
            }

            templateOld.push({
                service_voucher_template_id: service_voucher_template_id,
                valid_from_date: valid_from_date,
                valid_to_date: valid_to_date,
                is_actived: is_actived
            });

            templateIsActive.push(is_actived);
        });

        if (template.length + templateOld.length == 0) {
            $('.error_table_template').text('Hãy thêm template của gói dịch vụ');
            next = false;
        } else {
            $('.error_table_template').text('');
            if (check == 0) {
                swal("Hãy chọn template", "", "error");
                next = false;
            }
        }

        var is_actived = 0;
        if($('.is_actived').is(':checked')){
            is_actived = 1;
        }

        if (next == true) {
            $.ajax({
                url: laroute.route('voucher.update'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    service_id: id,
                    service_category_id: $('#service_category_id').val(),
                    // service_name_vi: $('#service_name_vi').val(),
                    // service_name_en: $('#service_name_en').val(),
                    price_standard: $('#price_standard').val(),
                    description: $('#description').val(),
                    description_en: $('#description_en').val(),
                    service_avatar_list: $('#service_avatar_list').val(),
                    service_avatar_list_mobile: $('#service_avatar_list_mobile').val(),
                    service_avatar_home: $('#service_avatar_home').val(),
                    service_avatar: $('#service_avatar').val(),
                    service_avatar_mobile: $('#service_avatar_mobile').val(),
                    detail_description: $('#detail_description').val(),
                    detail_description_en: $('#detail_description_en').val(),
                    serviceImage: serviceImage,
                    template: template,
                    templateOld: templateOld,
                    arrTemplateRemove: arrTemplateRemove,
                    templateIsActive: templateIsActive,
                    is_actived:is_actived
                },
                success: function (res) {
                    if (res.error == false) {
                        swal(res.message, "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                window.location.href = laroute.route('voucher');
                            }
                            if (result.value == true) {
                                window.location.href = laroute.route('voucher');
                            }
                        });
                    } else {
                        swal(res.message, '', "error");
                    }
                },
                error: function (res) {
                    var mess_error = '';
                    $.map(res.responseJSON.errors, function (a) {
                        mess_error = mess_error.concat(a + '<br/>');
                    });
                    swal('Chỉnh sửa thất bại', mess_error, "error");
                }
            });
        }
    }
};

function uploadAvatar(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_voucher.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#service_avatar').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function uploadAvatarMobile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            $('#blah_mobile').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFileMobile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_voucher_mobile.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#service_avatar_mobile').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function uploadAvatarPage(input,type) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            if (type == 'list') {
                $('#blah_list').attr('src', e.target.result);
            } else if (type == 'list-mobile'){
                $('#blah_list_mobile').attr('src', e.target.result);
            } else if (type == 'home') {
                $('#blah_home').attr('src', e.target.result);
            }
        };
        reader.readAsDataURL(input.files[0]);

        if (type == 'list') {
            var file_data = $('#getFileList').prop('files')[0];
        } else if (type == 'list-mobile'){
            var file_data = $('#getFileListMobile').prop('files')[0];
        } else if (type == 'home') {
            var file_data = $('#getFileHome').prop('files')[0];
        }
        var form_data = new FormData();
        form_data.append('file', file_data);
        if (type == 'list') {
            form_data.append('link', '_voucher_list.');
        } else if (type == 'list-mobile'){
            form_data.append('link', '_voucher_list_mobile.');
        } else if (type == 'home') {
            form_data.append('link', '_voucher_home.');
        }
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        if (type == 'list') {
                            $('#service_avatar_list').val(res.file);
                        } else if (type == 'list-mobile'){
                            $('#service_avatar_list_mobile').val(res.file);
                        } else if (type == 'home') {
                            $('#service_avatar_home').val(res.file);
                        }
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };