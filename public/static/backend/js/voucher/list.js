$('#autotable').PioTable({
    baseUrl: laroute.route('voucher.list')
});

var listService = {
    changeStatus: function (serviceId, obj) {
        var is_actived = 0;
        if ($(obj).is(':checked')) {
            is_actived = 1;
        }

        $.ajax({
            url: laroute.route('voucher.change-status'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                service_id: serviceId,
                is_actived: is_actived
            },
            success: function (res) {
                if (res.error == false) {
                    swal.fire(res.message, "", "success");
                } else {
                    swal.fire(res.message, '', "error");
                }
            }
        });
    },
    remove: function (serviceId) {
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('voucher.destroy'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        service_id: serviceId
                    },
                    success: function (res) {
                        if (res.error == false) {
                            swal.fire(res.message, "", "success");
                            $('#autotable').PioTable('refresh');
                        } else {
                            swal.fire(res.message, '', "error");
                        }
                    }
                });
            }
        });
    }
};