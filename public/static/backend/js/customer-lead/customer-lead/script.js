var listLead = {
    _init: function () {
        $('#autotable').PioTable({
            baseUrl: laroute.route('customer-lead.list')
        });
    },
    remove: function (id, load) {
        $.getJSON(laroute.route('translate'), function (json) {
            swal({
                title: json['Thông báo'],
                text: json["Bạn có muốn xóa không?"],
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: json['Xóa'],
                cancelButtonText: json['Hủy'],
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('customer-lead.destroy'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            customer_lead_id: id
                        },
                        success: function (res) {
                            if (res.error == false) {
                                if (load == true) {
                                    swal.fire(res.message, "", "success");

                                    $('#kanban').remove();
                                    $('.parent_kanban').append('<div id="kanban"></div>');
                                    kanBanView._init();
                                } else {
                                    swal.fire(res.message, "", "success");
                                    $('#autotable').PioTable('refresh');
                                }
                            } else {
                                swal.fire(res.message, '', "error");
                            }
                        }
                    });
                }
            });
        });
    },
    popupCustomerCare: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('customer-lead.popup-customer-care'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_lead_id: id
                },
                success: function (res) {
                    $('#my-modal').html(res.html);
                    $('#modal-customer-care').modal('show');

                    $('#care_type').select2({
                        placeholder: json['Chọn loại chăm sóc']
                    });
                }
            });
        });
    },
    submitCustomerCare: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-care');

            form.validate({
                rules: {
                    care_type: {
                        required: true,
                    },
                    content: {
                        required: true,
                    },
                },
                messages: {
                    care_type: {
                        required: json['Hãy chọn loại chăm sóc'],
                    },
                    content: {
                        required: json['Hãy nhập nội dung chăm sóc']
                    }
                },
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('customer-lead.customer-care'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    care_type: $('#care_type').val(),
                    content: $('#content').val(),
                    customer_lead_id: id
                },
                success: function (res) {
                    if (res.error == false) {
                        swal(res.message, "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                $('#modal-customer-care').modal('hide');
                            }
                            if (result.value == true) {
                                $('#modal-customer-care').modal('hide');
                            }
                        });
                    } else {
                        swal(res.message, '', "error");
                    }
                }
            });
        });
    },
    detail: function (id) {
        $.ajax({
            url: laroute.route('customer-lead.show'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                customer_lead_id: id,
                view: 'detail'
            },
            success: function (res) {
                $('#my-modal').html(res.html);
                $('#modal-detail').modal('show');

                $('#tag_id').select2({
                    placeholder: json['Chọn tag']
                });

                $('#pipeline_code').select2({
                    placeholder: json['Chọn pipeline']
                });

                $('#customer_type').select2({
                    placeholder: json['Chọn loại khách hàng']
                });

                $('#journey_code').select2({
                    placeholder: json['Chọn hành trình']
                });

                $('#business_clue').select2({
                    placeholder: json['Chọn đầu mối doanh nghiệp']
                });

                $('.phone').ForceNumericOnly();
            }
        });
    }
};

var create = {
    popupCreate: function (load) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('customer-lead.create'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    load: load
                },
                success: function (res) {
                    $('#my-modal').html(res.html);
                    $('#modal-create').modal('show');

                    $('#tag_id').select2({
                        placeholder: json['Chọn tag']
                    });

                    $('#pipeline_code').select2({
                        placeholder: json['Chọn pipeline']
                    });

                    $('#customer_type').select2({
                        placeholder: json['Chọn loại khách hàng']
                    });

                    $('#customer_source').select2({
                        placeholder: json['Chọn nguồn khách hàng']
                    });

                    $('#business_clue').select2({
                        placeholder: json['Chọn đầu mối doanh nghiệp']
                    });

                    $('.phone').ForceNumericOnly();
                }
            });
        });
    },
    save: function (load) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-register');

            form.validate({
                rules: {
                    full_name: {
                        required: true,
                        maxlength: 250
                    },
                    phone: {
                        required: true,
                        integer: true,
                        maxlength: 10
                    },
                    address: {
                        maxlength: 250
                    },
                    pipeline_code: {
                        required: true
                    },
                    customer_type: {
                        required: true
                    },
                    tax_code: {
                        required: true,
                        maxlength: 50
                    },
                    representative: {
                        required: true,
                        maxlength: 250
                    },
                    customer_source: {
                        required: true
                    }
                },
                messages: {
                    full_name: {
                        required: json['Hãy nhập họ và tên'],
                        maxlength: json['Họ và tên tối đa 250 kí tự']
                    },
                    phone: {
                        required: json['Hãy nhập số điện thoại'],
                        integer: json['Số điện thoại không hợp lệ'],
                        maxlength: json['Số điện thoại tối đa 10 kí tự']
                    },
                    address: {
                        maxlength: json['Địa chỉ tối đa 250 kí tự']
                    },
                    pipeline_code: {
                        required: json['Hãy chọn pipeline']
                    },
                    customer_type: {
                        required: json['Hãy chọn loại khách hàng']
                    },
                    tax_code: {
                        required: json['Hãy nhập mã số thuế'],
                        maxlength: json['Mã số thuế tối đa 50 kí tự']
                    },
                    representative: {
                        required: json['Hãy nhập người đại diện'],
                        maxlength: json['Người đại diện tối đa 250 kí tự']
                    },
                    customer_source: {
                        required: json['Hãy chọn nguồn khách hàng']
                    }
                },
            });

            if (!form.valid()) {
                return false;
            }

            var continute = true;

            var arrPhoneAttack = [];
            var arrEmailAttack = [];
            var arrFanpageAttack = [];
            var arrContact = [];

            $.each($('.phone_append').find(".div_phone_attach"), function () {
                var phone = $(this).find($('.phone_attach')).val();
                var number = $(this).find($('.number_phone')).val();

                if (phone == '') {
                    $('.error_phone_attach_' + number + '').text(json['Hãy nhập số điện thoại']);
                    continute = false;
                } else {
                    $('.error_phone_attach_' + number + '').text('');
                }

                arrPhoneAttack.push({
                    phone: phone
                });
            });

            $.each($('.email_append').find(".div_email_attach"), function () {
                var email = $(this).find($('.email_attach')).val();
                var number = $(this).find($('.number_email')).val();

                if (email == '') {
                    $('.error_email_attach_' + number + '').text(json['Hãy nhập email']);
                    continute = false;
                } else {
                    $('.error_email_attach_' + number + '').text('');
                }

                arrEmailAttack.push({
                    email: email
                });
            });

            $.each($('.fanpage_append').find(".div_fanpage_attach"), function () {
                var fanpage = $(this).find($('.fanpage_attach')).val();
                var number = $(this).find($('.number_fanpage')).val();

                if (fanpage == '') {
                    $('.error_fanpage_attach_' + number + '').text(json['Hãy nhập fanpage']);
                    continute = false;
                } else {
                    $('.error_fanpage_attach_' + number + '').text('');
                }

                arrFanpageAttack.push({
                    fanpage: fanpage
                });
            });

            $.each($('#table-contact').find(".tr_contact"), function () {
                var fullName = $(this).find($('.full_name_contact')).val();
                var phoneContact = $(this).find($('.phone_contact')).val();
                var emailContact = $(this).find($('.email_contact')).val();
                var addressContact = $(this).find($('.address_contact')).val();
                var number = $(this).find($('.number_contact')).val();

                if (fullName == '') {
                    $('.error_full_name_contact_' + number + '').text(json['Hãy nhập họ và tên']);
                    continute = false;
                } else {
                    $('.error_full_name_contact_' + number + '').text('');
                }

                if (phoneContact == '') {
                    $('.error_phone_contact_' + number + '').text(json['Hãy nhập số điện thoại']);
                    continute = false;
                } else {
                    $('.error_phone_contact_' + number + '').text('');
                }

                if (addressContact == '') {
                    $('.error_address_contact_' + number + '').text(json['Hãy nhập địa chỉ']);
                    continute = false;
                } else {
                    $('.error_address_contact_' + number + '').text('');
                }

                if (emailContact == '') {
                    $('.error_email_contact_' + number + '').text(json['Hãy nhập email']);
                    continute = false;
                } else {
                    $('.error_email_contact_' + number + '').text('');

                    if (isValidEmailAddress(emailContact) == false) {
                        $('.error_email_contact_' + number + '').text(json['Email không hợp lệ']);
                        continute = false;
                    } else {
                        $('.error_email_contact_' + number + '').text('');
                    }
                }

                arrContact.push({
                    full_name: fullName,
                    phone: phoneContact,
                    email: emailContact,
                    address: addressContact
                });
            });

            if (continute == true) {
                $.ajax({
                    url: laroute.route('customer-lead.store'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        full_name: $('#full_name').val(),
                        phone: $('#phone').val(),
                        gender: $('input[name="gender"]:checked').val(),
                        address: $('#address').val(),
                        avatar: $('#avatar').val(),
                        email: $('#email').val(),
                        tag_id: $('#tag_id').val(),
                        pipeline_code: $('#pipeline_code').val(),
                        customer_type: $('#customer_type').val(),
                        hotline: $('#hotline').val(),
                        fanpage: $('#fanpage').val(),
                        zalo: $('#zalo').val(),
                        arrPhoneAttack: arrPhoneAttack,
                        arrEmailAttack: arrEmailAttack,
                        arrFanpageAttack: arrFanpageAttack,
                        arrContact: arrContact,
                        tax_code: $('#tax_code').val(),
                        representative: $('#representative').val(),
                        customer_source: $('#customer_source').val(),
                        business_clue: $('#business_clue').val()
                    },
                    success: function (res) {
                        if (res.error == false) {
                            if (load == true) {
                                swal(res.message, "", "success").then(function (result) {
                                    if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                        $('#modal-create').modal('hide');
                                    }
                                    if (result.value == true) {
                                        $('#modal-create').modal('hide');
                                    }
                                });

                                $('#kanban').remove();
                                $('.parent_kanban').append('<div id="kanban"></div>');
                                kanBanView._init();
                            } else {
                                swal(res.message, "", "success").then(function (result) {
                                    if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                        $('#modal-create').modal('hide');
                                    }
                                    if (result.value == true) {
                                        $('#modal-create').modal('hide');
                                    }
                                });
                                $('#autotable').PioTable('refresh');
                            }
                        } else {
                            swal(res.message, '', "error");
                        }
                    },
                    error: function (res) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal(json['Thêm mới thất bại'], mess_error, "error");
                    }
                });
            }
        });
    }
};

var edit = {
    popupEdit: function (id, load) {
        $.ajax({
            url: laroute.route('customer-lead.edit'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                customer_lead_id: id,
                load: load,
                view: 'edit'
            },
            success: function (res) {
                $('#my-modal').html(res.html);
                $('#modal-edit').modal('show');

                $('#tag_id').select2({
                    placeholder: json['Chọn tag']
                });

                $('#pipeline_code').select2({
                    placeholder: json['Chọn pipeline']
                });

                $('#customer_type').select2({
                    placeholder: json['Chọn loại khách hàng']
                });

                $('#journey_code').select2({
                    placeholder: json['Chọn hành trình']
                });

                $('#customer_source').select2({
                    placeholder: json['Chọn nguồn khách hàng']
                });

                $('#business_clue').select2({
                    placeholder: json['Chọn đầu mối doanh nghiệp']
                });

                $('.phone').ForceNumericOnly();
            }
        });
    },
    save: function (id, load) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-edit');

            form.validate({
                rules: {
                    full_name: {
                        required: true,
                        maxlength: 250
                    },
                    phone: {
                        required: true,
                        integer: true,
                        maxlength: 10
                    },
                    address: {
                        maxlength: 250
                    },
                    pipeline_code: {
                        required: true
                    },
                    journey_code: {
                        required: true
                    },
                    customer_type: {
                        required: true
                    },
                    tax_code: {
                        required: true,
                        maxlength: 50
                    },
                    representative: {
                        required: true,
                        maxlength: 250
                    },
                    customer_source: {
                        required: true
                    }
                },
                messages: {
                    full_name: {
                        required: json['Hãy nhập họ và tên'],
                        maxlength: json['Họ và tên tối đa 250 kí tự']
                    },
                    phone: {
                        required: json['Hãy nhập số điện thoại'],
                        integer: json['Số điện thoại không hợp lệ'],
                        maxlength: json['Số điện thoại tối đa 10 kí tự']
                    },
                    address: {
                        maxlength: json['Địa chỉ tối đa 250 kí tự']
                    },
                    pipeline_code: {
                        required: json['Hãy chọn pipeline']
                    },
                    journey_code: {
                        required: json['Hãy chọn hành trình khách hàng']
                    },
                    customer_type: {
                        required: json['Hãy chọn loại khách hàng']
                    },
                    tax_code: {
                        required: json['Hãy nhập mã số thuế'],
                        maxlength: json['Mã số thuế tối đa 50 kí tự']
                    },
                    representative: {
                        required: json['Hãy nhập người đại diện'],
                        maxlength: json['Người đại diện tối đa 250 kí tự']
                    },
                    customer_source: {
                        required: json['Hãy chọn nguồn khách hàng']
                    }
                },
            });

            if (!form.valid()) {
                return false;
            }

            var continute = true;

            var arrPhoneAttack = [];
            var arrEmailAttack = [];
            var arrFanpageAttack = [];
            var arrContact = [];

            $.each($('.phone_append').find(".div_phone_attach"), function () {
                var phone = $(this).find($('.phone_attach')).val();
                var number = $(this).find($('.number_phone')).val();

                if (phone == '') {
                    $('.error_phone_attach_' + number + '').text(json['Hãy nhập số điện thoại']);
                    continute = false;
                } else {
                    $('.error_phone_attach_' + number + '').text('');
                }

                arrPhoneAttack.push({
                    phone: phone
                });
            });

            $.each($('.email_append').find(".div_email_attach"), function () {
                var email = $(this).find($('.email_attach')).val();
                var number = $(this).find($('.number_email')).val();

                if (email == '') {
                    $('.error_email_attach_' + number + '').text(json['Hãy nhập email']);
                    continute = false;
                } else {
                    $('.error_email_attach_' + number + '').text('');
                }

                arrEmailAttack.push({
                    email: email
                });
            });

            $.each($('.fanpage_append').find(".div_fanpage_attach"), function () {
                var fanpage = $(this).find($('.fanpage_attach')).val();
                var number = $(this).find($('.number_fanpage')).val();

                if (fanpage == '') {
                    $('.error_fanpage_attach_' + number + '').text(json['Hãy nhập fanpage']);
                    continute = false;
                } else {
                    $('.error_fanpage_attach_' + number + '').text('');
                }

                arrFanpageAttack.push({
                    fanpage: fanpage
                });
            });

            $.each($('#table-contact').find(".tr_contact"), function () {
                var fullName = $(this).find($('.full_name_contact')).val();
                var phoneContact = $(this).find($('.phone_contact')).val();
                var emailContact = $(this).find($('.email_contact')).val();
                var addressContact = $(this).find($('.address_contact')).val();
                var number = $(this).find($('.number_contact')).val();

                if (fullName == '') {
                    $('.error_full_name_contact_' + number + '').text(json['Hãy nhập họ và tên']);
                    continute = false;
                } else {
                    $('.error_full_name_contact_' + number + '').text('');
                }

                if (phoneContact == '') {
                    $('.error_phone_contact_' + number + '').text(json['Hãy nhập số điện thoại']);
                    continute = false;
                } else {
                    $('.error_phone_contact_' + number + '').text('');
                }

                if (addressContact == '') {
                    $('.error_address_contact_' + number + '').text(json['Hãy nhập địa chỉ']);
                    continute = false;
                } else {
                    $('.error_address_contact_' + number + '').text('');
                }

                if (emailContact == '') {
                    $('.error_email_contact_' + number + '').text(json['Hãy nhập email']);
                    continute = false;
                } else {
                    $('.error_email_contact_' + number + '').text('');

                    if (isValidEmailAddress(emailContact) == false) {
                        $('.error_email_contact_' + number + '').text(json['Email không hợp lệ']);
                        continute = false;
                    } else {
                        $('.error_email_contact_' + number + '').text('');
                    }
                }

                arrContact.push({
                    full_name: fullName,
                    phone: phoneContact,
                    email: emailContact,
                    address: addressContact
                });
            });

            if (continute == true) {
                $.ajax({
                    url: laroute.route('customer-lead.update'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        full_name: $('#full_name').val(),
                        phone: $('#phone').val(),
                        gender: $('input[name="gender"]:checked').val(),
                        address: $('#address').val(),
                        avatar: $('#avatar').val(),
                        email: $('#email').val(),
                        tag_id: $('#tag_id').val(),
                        pipeline_code: $('#pipeline_code').val(),
                        journey_code: $('#journey_code').val(),
                        customer_type: $('#customer_type').val(),
                        hotline: $('#hotline').val(),
                        fanpage: $('#fanpage').val(),
                        zalo: $('#zalo').val(),
                        customer_lead_id: id,
                        customer_lead_code: $('#customer_lead_code').val(),
                        arrPhoneAttack: arrPhoneAttack,
                        arrEmailAttack: arrEmailAttack,
                        arrFanpageAttack: arrFanpageAttack,
                        arrContact: arrContact,
                        tax_code: $('#tax_code').val(),
                        representative: $('#representative').val(),
                        customer_source: $('#customer_source').val(),
                        business_clue: $('#business_clue').val()
                    },
                    success: function (res) {
                        if (res.error == false) {
                            if (load == true) {
                                swal(res.message, "", "success").then(function (result) {
                                    if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                        $('#modal-edit').modal('hide');
                                    }
                                    if (result.value == true) {
                                        $('#modal-edit').modal('hide');
                                    }
                                });

                                $('#kanban').remove();
                                $('.parent_kanban').append('<div id="kanban"></div>');
                                kanBanView._init();
                            } else {
                                swal(res.message, "", "success").then(function (result) {
                                    if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                        $('#modal-edit').modal('hide');
                                    }
                                    if (result.value == true) {
                                        $('#modal-edit').modal('hide');
                                    }
                                });
                                $('#autotable').PioTable('refresh');
                            }
                        } else {
                            swal(res.message, '', "error");
                        }
                    },
                    error: function (res) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal(json['Chỉnh sửa thất bại'], mess_error, "error");
                    }
                });
            }
        });
    }
};

var numberPhone = 0;
var numberEmail = 0;
var numberFanpage = 0;
var numberContact = 0;

var view = {
    addPhone: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var continute = true;

            //check các trường dữ liệu rỗng thì báo lỗi
            $.each($('.phone_append').find(".div_phone_attach"), function () {
                var phone = $(this).find($('.phone_attach')).val();
                var number = $(this).find($('.number_phone')).val();

                if (phone == '') {
                    $('.error_phone_attach_' + number + '').text(json['Hãy nhập số điện thoại']);
                    continute = false;
                } else {
                    $('.error_phone_attach_' + number + '').text('');
                }
            });

            if (continute == true) {
                numberPhone++;
                //append tr table
                var tpl = $('#tpl-phone').html();
                tpl = tpl.replace(/{number}/g, numberPhone);
                $('.phone_append').append(tpl);

                $('.phone').ForceNumericOnly();
            }
        });
    },
    removePhone: function (obj) {
        $(obj).closest('.div_phone_attach').remove();
    },
    addEmail: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var continute = true;

            //check các trường dữ liệu rỗng thì báo lỗi
            $.each($('.email_append').find(".div_email_attach"), function () {
                var email = $(this).find($('.email_attach')).val();
                var number = $(this).find($('.number_email')).val();

                if (email == '') {
                    $('.error_email_attach_' + number + '').text(json['Hãy nhập email']);
                    continute = false;
                } else {
                    $('.error_email_attach_' + number + '').text('');
                }
            });

            if (continute == true) {
                numberEmail++;
                //append tr table
                var tpl = $('#tpl-email').html();
                tpl = tpl.replace(/{number}/g, numberEmail);
                $('.email_append').append(tpl);
            }
        });
    },
    removeEmail: function (obj) {
        $(obj).closest('.div_email_attach').remove();
    },
    addFanpage: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var continute = true;

            //check các trường dữ liệu rỗng thì báo lỗi
            $.each($('.fanpage_append').find(".div_fanpage_attach"), function () {
                var fanpage = $(this).find($('.fanpage_attach')).val();
                var number = $(this).find($('.number_fanpage')).val();

                if (fanpage == '') {
                    $('.error_fanpage_attach_' + number + '').text(json['Hãy nhập fanpage']);
                    continute = false;
                } else {
                    $('.error_fanpage_attach_' + number + '').text('');
                }
            });

            if (continute == true) {
                numberFanpage++;
                //append tr table
                var tpl = $('#tpl-fanpage').html();
                tpl = tpl.replace(/{number}/g, numberFanpage);
                $('.fanpage_append').append(tpl);
            }
        });
    },
    removeFanpage: function (obj) {
        $(obj).closest('.div_fanpage_attach').remove();
    },
    changeType: function (obj) {
        $.getJSON(laroute.route('translate'), function (json) {
            if ($(obj).val() == 'personal') {
                $('.append_type').empty();

                $('.append_contact').empty();
                $('.div_add_contact').css('display', 'none');

                $('#table-contact > tbody').empty();

                $('.div_business_clue').css('display', 'block');

                $('#business_clue').select2({
                    placeholder: json['Chọn đầu mối doanh nghiệp']
                });
            } else {
                var tpl = $('#tpl-type').html();
                $('.append_type').append(tpl);

                $('.div_add_contact').css('display', 'block');

                $('.div_business_clue').css('display', 'none');
            }
        });
    },
    addContact:function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var continute = true;

            //check các trường dữ liệu rỗng thì báo lỗi
            $.each($('#table-contact').find(".tr_contact"), function () {
                var fullName = $(this).find($('.full_name_contact')).val();
                var phoneContact = $(this).find($('.phone_contact')).val();
                var emailContact = $(this).find($('.email_contact')).val();
                var addressContact = $(this).find($('.address_contact')).val();
                var number = $(this).find($('.number_contact')).val();

                if (fullName == '') {
                    $('.error_full_name_contact_' + number + '').text(json['Hãy nhập họ và tên']);
                    continute = false;
                } else {
                    $('.error_full_name_contact_' + number + '').text('');
                }

                if (phoneContact == '') {
                    $('.error_phone_contact_' + number + '').text(json['Hãy nhập số điện thoại']);
                    continute = false;
                } else {
                    $('.error_phone_contact_' + number + '').text('');
                }

                if (addressContact == '') {
                    $('.error_address_contact_' + number + '').text(json['Hãy nhập địa chỉ']);
                    continute = false;
                } else {
                    $('.error_address_contact_' + number + '').text('');
                }

                if (emailContact == '') {
                    $('.error_email_contact_' + number + '').text(json['Hãy nhập email']);
                    continute = false;
                } else {
                    $('.error_email_contact_' + number + '').text('');

                    if (isValidEmailAddress(emailContact) == false) {
                        $('.error_email_contact_' + number + '').text(json['Email không hợp lệ']);
                        continute = false;
                    } else {
                        $('.error_email_contact_' + number + '').text('');
                    }
                }
            });

            if (continute == true) {
                numberContact++;
                //append tr table
                var tpl = $('#tpl-contact').html();
                tpl = tpl.replace(/{number}/g, numberContact);
                $('#table-contact > tbody').append(tpl);

                $('.phone').ForceNumericOnly();
            }
        });
    },
    removeContact: function (obj) {
        $(obj).closest('.tr_contact').remove();
    }
};

var idClick = '';

var kanBanView = {
    _init: function () {
        $(document).ready(function () {
            $.getJSON(laroute.route('translate'), function (json) {
                $('#pipeline_id').select2({
                    placeholder: json['Chọn pipeline']
                });

                mApp.block("#m_blockui_1_content", {
                    overlayColor: "#000000",
                    type: "loader",
                    state: "success",
                    message: json["Đang tải..."]
                });

                $.ajax({
                    url: laroute.route('customer-lead.load-kan-ban-view'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        pipeline_id: $('#pipeline_id').val()
                    },
                    success: function (res) {
                        mApp.unblock("#m_blockui_1_content");

                        var columns = [];
                        var loadDataSource = [];
                        var loadDataResourceSource = [];

                        $.map(res.journey, function (val) {
                            columns.push({
                                text: val.journey_name,
                                dataField: val.journey_code
                            });
                        });

                        $.map(res.customerLead, function (val) {
                            var hex = '';

                            if (val.default_system == 'new') {
                                hex = '#34bfa3';
                            } else if (val.default_system == 'fail') {
                                hex = '#f4516c';
                            } else if (val.default_system == 'win') {
                                hex = '#5867dd';
                            } else {
                                hex = '#36a3f7';
                            }

                            loadDataSource.push({
                                id: val.customer_lead_id,
                                state: val.journey_code,
                                name: val.full_name + "<br/><br/>" + val.phone + "<br/>" + val.email,
                                tags: "<i class='la la-gratipay'></i>, <i class='la la-edit'></i>, <i class='la la-trash'></i>, <i class='la la-eye'></i>",
                                hex: hex,
                                resourceId: val.customer_lead_id
                            });

                            loadDataResourceSource.push({
                                id: val.customer_lead_id,
                                name: val.full_name,
                                image: val.avatar
                            });
                        });

                        if (loadDataSource.length == 0 && loadDataResourceSource.length == 0) {
                            loadDataSource.push({
                                id: 0,
                                state: '',
                                name: '',
                                tags: "<i class='la la-gratipay'></i>, <i class='la la-edit'></i>, <i class='la la-trash'></i>",
                                hex: '',
                                resourceId: ''
                            });

                            loadDataResourceSource.push({
                                id: 0,
                                name: 'asd',
                                image: ''
                            });
                        }
                        kanBanView.loadView(columns, loadDataSource, loadDataResourceSource);
                    }
                });
            });
        });
    },
    loadView: function (columns, loadDataSource, loadDataResourceSource) {
        var fields = [
            {name: "id", type: "string"},
            {name: "status", map: "state", type: "string"},
            {name: "text", map: "name", type: "string"},
            {name: "tags", type: "string"},
            {name: "color", map: "hex", type: "string"},
            {name: "resourceId", type: "number"}
        ];
        var source =
            {
                localData: loadDataSource,
                dataType: "array",
                dataFields: fields
            };

        var dataAdapter = new $.jqx.dataAdapter(source);

        var resourcesAdapterFunc = function () {
            var resourcesSource =
                {
                    localData: loadDataResourceSource,
                    dataType: "array",
                    dataFields: [
                        {name: "id", type: "number"},
                        {name: "name", type: "string"},
                        {name: "image", type: "string"},
                        {name: "common", type: "boolean"}
                    ]
                };
            var resourcesDataAdapter = new $.jqx.dataAdapter(resourcesSource);
            return resourcesDataAdapter;
        };

        $('#kanban').jqxKanban({
            width: getWidth('kanban'),
            height: 550,
            resources: resourcesAdapterFunc(),
            source: dataAdapter,
            columns: columns,
            columnRenderer: function (element, collapsedElement, column) {
                var columnItems = $("#kanban").jqxKanban('getColumnItems', column.dataField).length;
                // update header's status.
                element.find(".jqx-kanban-column-header-status").html(" (" + columnItems + ")");
                // update collapsed header's status.
                collapsedElement.find(".jqx-kanban-column-header-status").html(" (" + columnItems + ")");
            },
            template: "<div class='jqx-kanban-item' id=''>"
                + "<div class='jqx-kanban-item-color-status'></div>"
                + "<div style='display: block;' class='jqx-kanban-item-avatar'></div>"
                + "<div class='jqx-icon jqx-icon-close-white jqx-kanban-item-template-content jqx-kanban-template-icon'></div>"
                + "<div class='jqx-kanban-item-text'></div>"
                + "<div style='display: block;' class='jqx-kanban-item-footer'></div>"
                + "</div>",
        });
        //Event kéo thả
        $('#kanban').on('itemMoved', function (event) {
            var args = event.args;
            var itemId = args.itemId;
            var oldParentId = args.oldParentId;
            var newParentId = args.newParentId;
            var itemData = args.itemData;
            var oldColumn = args.oldColumn;
            var newColumn = args.newColumn;

            mApp.block("#m_blockui_1_content", {
                overlayColor: "#000000",
                type: "loader",
                state: "success",
                message: json["Đang tải..."]
            });

            $.ajax({
                url: laroute.route('customer-lead.update-journey'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_lead_id: itemId,
                    journey_old: oldColumn.dataField,
                    journey_new: newColumn.dataField
                },
                success: function (res) {
                    mApp.unblock("#m_blockui_1_content");

                    if (res.error == false) {
                        setTimeout(function () {
                            toastr.success(res.message)
                        }, 60);
                    } else {
                        setTimeout(function () {
                            toastr.error(res.message)
                        }, 60);
                    }

                    $('#kanban').remove();
                    $('.parent_kanban').append('<div id="kanban"></div>');
                    kanBanView._init();
                }
            });
        });
        //Lấy id button được nhấp
        $('#kanban').on('itemAttrClicked', function (event1) {
            var args = event1.args;
            var itemId = args.itemId;
            var attribute = args.attribute; // template, colorStatus, content, keyword, text, avatar

            idClick = itemId;
        });

        //Event click
        $('#kanban').click(function (event) {
            //Get button nào được nhấp
            // event.target.innerText
            if (event.target.className == 'la la-gratipay') {
                listLead.popupCustomerCare(idClick);
            } else if (event.target.className == 'la la-edit') {
                edit.popupEdit(idClick, true);
            } else if (event.target.className == 'la la-trash') {
                listLead.remove(idClick, true);
            } else if (event.target.className == 'la la-eye') {
                listLead.detail(idClick);
            }
        });
    },
    changePipeline:function () {
        $('#kanban').remove();
        $('.parent_kanban').append('<div id="kanban"></div>');

        kanBanView._init();
    }
};

function uploadAvatar(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_customer-lead.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#avatar').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}