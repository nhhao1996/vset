// todo: Global---------------------
String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};
// !------Global----------------------

var StockChartHandler = {
    init(){

    },
    showPopupAdd(){
        $("#popup-add").modal("show");
    },
    setValue(name, item){
        $(`[name='${name}']`).val(item[name]);

    },
    showPopupEdit(e, id){
        e.preventDefault();
        const item = LIST.data.find(e=>e.stock_chart_id == id);
        const time = $(`[data-time="${id}"]`).text();
        $("[name='time']").val(time);

        this.setValue("value",item);
        this.setValue("stock_chart_id",item);
        $("#popup-edit").modal("show");
    },
    add(){
        if(!this.validate("frmAddStockChart")){
            return;
        }
        // todo: Check Exist----------


        $.ajax({
            url: laroute.route("admin.stock-chart.add"),
            method: "POST",
            dataType: "JSON",
            data: $(`#frmAddStockChart`).serialize() ,

            success: function (data) {
                if (data.error == 1) {
                    swal("", data.message, "error");
                } else {
                    Swal("", data.message, "success").then(function (result) {
                        if (result) window.location.reload();
                    });
                }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
        });
    },
    edit(){
        if(!this.validate("frmEditStockChart")){
            return;
        }
        $.ajax({
            url: laroute.route("admin.stock-chart.edit"),
            method: "POST",
            dataType: "JSON",
            data: $(`#frmEditStockChart`).serialize() ,

            success: function (data) {
                console.log('data la gi: ', data);
                if (data.error == 1) {
                    swal("", data.message, "error");
                } else {
                    Swal("", data.message, "success").then(function (result) {
                        if (result) window.location.reload();
                    });
                }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
        });
    },
    search(e){

        $.ajax({
            url: laroute.route("admin.stock-chart.list"),
            method: "POST",
            data: $(`.frmFilter`).serialize() ,

            success: function (data) {
                $(".table-content").html(data);

                // if (data.error == 1) {
                //     swal("", data.message, "error");
                // } else {
                //     Swal("", data.message, "success").then(function (result) {
                //         if (result) window.location.reload();
                //     });
                // }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
        });
    },
    showPopupImport(){
        $("#import-excel").modal("show");
    },
    UploadFile(){
            var file_data = $("#file_excel").prop('files')[0];
            var form_data = new FormData();  // Create a FormData object
            form_data.append('file', file_data);  // Append all element in FormData  object
            if(!file_data){
                Swal("", "Bạn chưa chọn file import", "error");
                return;
            }
            var extension=$('#file_excel').val().replace(/^.*\./, '');
            if(extension !== "xls"){
                Swal("", "Chỉ có thể chọn file excel", "error");
                return;

            }
            $.ajax({
                url         : laroute.route('admin.stock-chart.import'),     // point to server-side PHP script
                dataType    : 'text',           // what to expect back from the PHP script, if anything
                cache       : false,
                contentType : false,
                processData : false,
                data        : form_data,
                type        : 'post',
                success     : function(data){
                    console.log('data la gi: ', data);
                    Swal.fire("", "Import file thành công", "success").then(function (result) {
                        if (result.value) window.location.reload();
                    });

                }
            });
            // $('#pic').val('');                     /* Clear the input type file */

    },
    showNameFile: function () {
        var fileNamess = $('input[type=file]').val();
        $('#show').val(fileNamess);
    },
    delete(event,$id){
        event.preventDefault();
        Swal.fire({
            title: '',
            text: "Bạn chắc chắn muốn xóa",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Đồng ý!',
            cancelButtonText: 'Hủy'
        }).then((result) => {
            if (result.value) {
                var data = {
                    stock_chart_id:$id
                };
                $.ajax({
                    url: laroute.route("admin.stock-chart.remove"),
                    method: "POST",
                    data:data ,

                    success: function (data) {

                        if (data.error == 1) {
                            swal("", data.message, "error");
                        } else {
                            Swal("", data.message, "success").then(function (result) {
                                if (result) window.location.reload();
                            });
                        }
                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }
                });

            }
        })

    },
    validate(frmForm){
        $("#"+frmForm).validate({
            rules: {
               time:{
                   required:true,
               },
                value:{
                   required:true,
                    min:0
                }


            },
            messages: {
                time:{
                    required:'Yêu cầu nhập Thời gian',
                },
                value:{
                    required:"Yêu cầu nhập giá trị",
                    min:"Yêu cầu nhập giá trị lớn hơn 0"
                }

            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $(`#${frmForm}`).valid()

    },


};
