$(document).ready(function (){
    $('#filter_type').change(function (){
        if ($('#filter_type').val() != 'week'){
        }
        $('.block_hide').hide();
        var block = $('#filter_type').val();
        $('.'+block+'_block').show();
    });
});

var statisticsTransaction = {

    colorOption: ['#f28f43', '#f7f7ff', '#2f7ed8', '#8bbc21',
        '#910000','#492970', '#f28f43', '#77a1e5'],
    init: function () {
            var arrRange = {};
            arrRange["Hôm nay"] = [moment(), moment()];
            arrRange["Ngày mai"] = [moment().add(1, "days"), moment().add(1, "days")];
            arrRange["7 ngày sau"] = [moment(), moment().add(6, "days")];
            arrRange["30 ngày sau"] = [moment(), moment().add(29, "days")];
            arrRange["Trong tháng"] = [moment().startOf("months"), moment().endOf("months")];
            arrRange["Tháng sau"] = [moment().add(1, "months").startOf("months"), moment().add(1, "months").endOf("months")];
        $("#time").daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            buttonClasses: "m-btn btn",
            applyClass: "btn-primary",
            cancelClass: "btn-danger",
            "dateLimit": {
                "month": 6
            },
            locale: {
                format: 'DD/MM/YYYY',
                "applyLabel": "Đồng ý",
                "cancelLabel": "Thoát",
                "customRangeLabel": "Tùy chọn ngày",
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                ],
                "monthNames": [
                    "Tháng 1 năm",
                    "Tháng 2 năm",
                    "Tháng 3 năm",
                    "Tháng 4 năm",
                    "Tháng 5 năm",
                    "Tháng 6 năm",
                    "Tháng 7 năm",
                    "Tháng 8 năm",
                    "Tháng 9 năm",
                    "Tháng 10 năm",
                    "Tháng 11 năm",
                    "Tháng 12 năm"
                ],
                "firstDay": 1
            },
            // ranges: {
            //     'Hôm nay': [moment(), moment()],
            //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
            //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
            //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
            //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
            //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            // }
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            statisticsTransaction.filter();
        });
        $("#time-hidden").daterangepicker({
            startDate: moment().add(-1, "months"),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('#time').val($("#time-hidden").val());

        statisticsTransaction.filter();
    },
    filter: function () {
        $.ajax({
            url: laroute.route('admin.report.statistics-transaction.filter'),
            method: "POST",
            dataType: 'json',
            data: {
                'filter_type' : $('#filter_type').val(),
                'time' : $('#time').val(),
                'year' : $('#year option:selected').val(),
            },
            sync: false,
            success: function (res) {
                $('#chart').html('');
                showChart(res.cate,res.chart);

                // AutoNumeric.multiple('.money',{
                //     currencySymbol : '',
                //     decimalCharacter : '.',
                //     digitGroupSeparator : ',',
                //     decimalPlaces: decimal_number
                // });
            }
        });
    },
};
function showChart(cate,chart){
    Highcharts.chart('chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: cate
        },
        yAxis: {
            unit : [' GD'],
            min: 0,
            title: {
                text: 'Số lượng giao dịch'
            },
        },
        // colors: statisticsTransaction.colorOption,
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: `{series.name}: {point.y} GD<br/>Total: {point.stackTotal} GD`
        },
        plotOptions: {
            column: {
                stacking: 'normal',
            }
        },
        series: chart
    });
}
statisticsTransaction.init();

$(document).ready(function (){
    $(".example").daterangepicker({
        single: true,
        standalone: true
    });
})