$('#autotable').PioTable({
    baseUrl: laroute.route('admin.stock-category.list')
});
var stockCategory = {
    search: function () {
        $(".btn-search").trigger("click");
    },

    save : function () {
        $('#form-category').validate({
            rules: {
                stock_category_title_vi: {
                    required: true,
                    maxlength: 255
                },
                stock_category_title_en: {
                    required: true,
                    maxlength: 255
                },
            },
            messages: {
                stock_category_title_vi: {
                    required: 'Yêu cầu nhập tên danh mục(VI)',
                    maxlength: 'Tên danh mục(VI) vượt quá 255 ký tự'
                },
                stock_category_title_en: {
                    required: 'Yêu cầu nhập tên danh mục(EN)',
                    maxlength: 'Tên danh mục(EN) vượt quá 255 ký tự'
                },
            },
        });
        if (!$('#form-category').valid()) {
            return true;
        } else {
            var is_active = 0 ;
            if ($('#is_active').is(':checked')) {
                is_active = 1;
            }
            $.ajax({
                url: laroute.route('admin.stock-category.save'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#form-category').serialize()+'&is_active='+is_active,
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            location.reload();
                        });
                    }
                },
            });
        }
    },

    addForm: function (type) {
        $.ajax({
            url: laroute.route('admin.stock-category.add-stock-category'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                type : type
            },
            success: function (res) {
                $('.append-form-category').empty();
                $('.append-form-category').append(res.view);
                $('#popup_category').modal('show');
            },
        });
    },

    detailForm : function (type,id) {
        $.ajax({
            url: laroute.route('admin.stock-category.detail-stock-category'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                stock_category_id : id,
                type : type
            },
            success: function (res) {
                $('.append-form-category').empty();
                $('.append-form-category').append(res.view);
                $('#popup_category').modal('show');
            },
        });
    },

    editForm : function (type,id) {
        $.ajax({
            url: laroute.route('admin.stock-category.edit-stock-category'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                stock_category_id : id,
                type : type
            },
            success: function (res) {
                $('.append-form-category').empty();
                $('.append-form-category').append(res.view);
                $('#popup_category').modal('show');
            },
        });
    },

    addBank : function () {
        $('#form-bank').validate({
            rules: {
                bank_name: {
                    required: true,
                    maxlength: 255
                },
                bank_icon:{
                    required: true,
                }
            },
            messages: {
                bank_name: {
                    required: 'Yêu cầu nhập tên ngân hàng',
                    maxlength: 'Tên ngân hàng vượt quá 255 ký tự'
                },
                bank_icon:{
                    required: 'Yêu cầu chọn ảnh ngân hàng',
                }
            },
        });
        if (!$('#form-bank').valid()) {
            return  true;
        } else {
            $.ajax({
                url: laroute.route('admin.stock-category.add'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#form-bank').serialize(),
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            location.reload();
                        });
                    }
                },
            });
        }
    },

    remove: function (obj, id) {
        $(obj).closest('tr').addClass('m-table__row--danger');

        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.stock-category.remove-category'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal(res.message,'','error');
                        } else {
                            swal(
                                'Xóa thành công',
                                '',
                                'success'
                            ).then(function () {
                                location.reload();
                            })
                        }
                    },
                });

            }
        });
    },
}
