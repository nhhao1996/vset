var index = {
    detail: function (id) {
        $.ajax({
            url: laroute.route('admin.receipt.detail'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                customer_debt_id: id
            },
            success: function (res) {
                $('#div-detail').html(res.url);
                $('#div-detail').find('#modal-detail').modal({
                    backdrop: 'static', keyboard: false
                });
            }
        });
    },
    receipt: function (id) {
        $.ajax({
            url: laroute.route('admin.receipt.receipt'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                customer_debt_id: id
            },
            success: function (res) {
                $.getJSON(laroute.route('translate'), function (json) {
                    $('#div-receipt').html(res.url);
                    $('#div-receipt').find('#modal-receipt').modal({
                        backdrop: 'static', keyboard: false
                    });

                    new AutoNumeric.multiple('#amount_receipt_cash', {
                        currencySymbol : '',
                        decimalCharacter : '.',
                        digitGroupSeparator : ',',
                        decimalPlaces: decimal_number,
                        eventIsCancelable: true
                    });

                    $('#receipt_type').select2({
                        placeholder: json['Chọn hình thức thanh toán']
                    }).on('select2:select', function (event) {
                        if (event.params.data.id == 'cash') {
                            $('.cash').empty();
                            var tpl = $('#type-receipt-tpl').html();
                            tpl = tpl.replace(/{label}/g, 'Tiền mặt');
                            tpl = tpl.replace(/{money}/g, '*');
                            tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_cash');
                            tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_cash');
                            $('.cash').append(tpl);

                            new AutoNumeric.multiple('#amount_receipt_cash', {
                                currencySymbol : '',
                                decimalCharacter : '.',
                                digitGroupSeparator : ',',
                                decimalPlaces: decimal_number,
                                eventIsCancelable: true
                            });
                        }
                        if (event.params.data.id == 'transfer') {
                            $('.transfer').empty()
                            var tpl = $('#type-receipt-tpl').html();
                            tpl = tpl.replace(/{label}/g, json['Tiền chuyển khoản']);
                            tpl = tpl.replace(/{money}/g, '*');
                            tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_atm');
                            tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_atm');
                            $('.transfer').append(tpl);

                            new AutoNumeric.multiple('#amount_receipt_atm', {
                                currencySymbol : '',
                                decimalCharacter : '.',
                                digitGroupSeparator : ',',
                                decimalPlaces: decimal_number,
                                eventIsCancelable: true
                            });
                        }
                        if (event.params.data.id == 'visa') {
                            $('.visa').empty()
                            var tpl = $('#type-receipt-tpl').html();
                            tpl = tpl.replace(/{label}/g, json['Tiền chuyển Visa']);
                            tpl = tpl.replace(/{money}/g, '*');
                            tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_visa');
                            tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_visa');
                            $('.visa').append(tpl);

                            new AutoNumeric.multiple('#amount_receipt_visa', {
                                currencySymbol : '',
                                decimalCharacter : '.',
                                digitGroupSeparator : ',',
                                decimalPlaces: decimal_number,
                                eventIsCancelable: true
                            });
                        }
                        if (event.params.data.id == 'member_money') {
                            var money = $('#member_money').val();
                            $('.member_money').empty();
                            var tpl = $('#type-receipt-tpl').html();
                            tpl = tpl.replace(/{label}/g, json['Tài khoản thành viên']);
                            tpl = tpl.replace(/{money}/g, json['(Còn '] + formatNumber(money) + ')');
                            tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_money');
                            tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_money');
                            $('.member_money').append(tpl);

                            new AutoNumeric.multiple('#amount_receipt_money', {
                                currencySymbol : '',
                                decimalCharacter : '.',
                                digitGroupSeparator : ',',
                                decimalPlaces: decimal_number,
                                eventIsCancelable: true
                            });
                        }
                    }).on('select2:unselect', function (event) {
                        var amount_this = 0;

                        if (event.params.data.id == 'cash') {
                            amount_this = $('#amount_receipt_cash').val().replace(new RegExp('\\,', 'g'), '');
                            $('.cash').empty();
                        }
                        if (event.params.data.id == 'transfer') {
                            amount_this = $('#amount_receipt_atm').val().replace(new RegExp('\\,', 'g'), '');
                            $('.transfer').empty();
                        }
                        if (event.params.data.id == 'visa') {
                            amount_this = $('#amount_receipt_visa').val().replace(new RegExp('\\,', 'g'), '');
                            $('.visa').empty();
                        }
                        if (event.params.data.id == 'member_money') {
                            amount_this = $('#amount_receipt_money').val().replace(new RegExp('\\,', 'g'), '');
                            $('.member_money').empty();
                        }
                        var amount_rest = $('#amount_rest').val().replace(new RegExp('\\,', 'g'), '');
                        var amount_return = $('#amount_return').val().replace(new RegExp('\\,', 'g'), '');
                        var all = $('#amount_all').val().replace(new RegExp('\\,', 'g'), '');

                        $('#amount_all').val(formatNumber((all - amount_this).toFixed(decimal_number)));
                        $('.cl_amount_all').text(formatNumber((all - amount_this).toFixed(decimal_number)));

                        if (((amount_rest) + (amount_this) - (amount_return)) > 0) {
                            $('#amount_rest').val(formatNumber(((amount_rest) + (amount_this) - (amount_return)).toFixed(decimal_number)));
                            $('.cl_amount_rest').text(formatNumber(((amount_rest) + (amount_this) - (amount_return)).toFixed(decimal_number)));
                        } else {
                            $('#amount_rest').val(0);
                            $('.cl_amount_rest').text(0);
                        }

                        if (amount_return - amount_this > 0) {
                            $('#amount_return').val(formatNumber((amount_return - amount_this)));
                            $('.cl_amount_return').text(formatNumber((amount_return - amount_this)));
                        } else {
                            $('#amount_return').val(0);
                            $('.cl_amount_return').text(0);
                        }
                    });
                });
            }
        });
    },
    submit_receipt: function (id) {
        var receipt_type = $('#receipt_type').val();
        var receipt_cash = $('#amount_receipt_cash').val();
        var receipt_atm = $('#amount_receipt_atm').val();
        var receipt_visa = $('#amount_receipt_visa').val();
        var receipt_money = $('#amount_receipt_money').val();
        var amount_bill = $('#receipt_amount').val();
        var amount_return = $('#amount_return').val();
        $.getJSON(laroute.route('translate'), function (json) {
            if (receipt_type == '') {
                $('.error_type').text(json['Hãy chọn hình thức thanh toán']);
                return false;
            } else {
                $('.error_type').text('');
            }

            $.ajax({
                url: laroute.route('admin.receipt.submit-receipt'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_debt_id: id,
                    receipt_type: receipt_type,
                    receipt_cash: receipt_cash,
                    receipt_atm: receipt_atm,
                    receipt_visa: receipt_visa,
                    receipt_money: receipt_money,
                    amount_bill: amount_bill,
                    amount_return: amount_return,
                    note: $('#note').val()
                },
                success: function (res) {
                    if (res.error == true) {
                        swal(json["Thanh toán công nợ thất bại"], res.message, "error");
                    } else {
                        swal(json["Thanh toán công nợ thành công"], "", "success");
                        window.location.reload();
                    }
                },
                error: function (res) {
                    swal(json["Thanh toán công nợ thất bại"], "", "error");
                }
            });
        });
    },
    submit_receipt_bill: function (id) {
        var receipt_type = $('#receipt_type').val();
        var receipt_cash = $('#amount_receipt_cash').val();
        var receipt_atm = $('#amount_receipt_atm').val();
        var receipt_visa = $('#amount_receipt_visa').val();
        var receipt_money = $('#amount_receipt_money').val();
        var amount_bill = $('#receipt_amount').val();
        var amount_return = $('#amount_return').val();
        $.getJSON(laroute.route('translate'), function (json) {
            if (receipt_type == '') {
                $('.error_type').text(json['Hãy chọn hình thức thanh toán']);
                return false;
            } else {
                $('.error_type').text('');
            }

            $.ajax({
                url: laroute.route('admin.receipt.submit-receipt'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_debt_id: id,
                    receipt_type: receipt_type,
                    receipt_cash: receipt_cash,
                    receipt_atm: receipt_atm,
                    receipt_visa: receipt_visa,
                    receipt_money: receipt_money,
                    amount_bill: amount_bill,
                    amount_return: amount_return,
                    note: $('#note').val()
                },
                success: function (res) {
                    if (res.error == true) {
                        swal(json["Thanh toán công nợ thất bại"], res.message, "error");
                    } else {
                        $('#customer_debt_id').val(id);
                        $('#amount_bill').val(amount_bill);
                        $('#amount_return_bill').val(amount_return);
                        $('#receipt_id').val(res.receipt_id);
                        $('#bill-receipt').submit();

                        swal(json["Thanh toán công nợ thành công"], "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                window.location.reload();
                            }
                            if (result.value == true) {
                                window.location.reload();
                            }
                        });
                    }
                },
                error: function (res) {
                    swal(json["Thanh toán công nợ thất bại"], "", "error");
                }
            });
        });
    },
    changeAmountReceipt:function (obj) {
        var amount_receipt_cash = 0;
        if ($('#amount_receipt_cash').val() != undefined && $('#amount_receipt_cash').val() != '') {
            amount_receipt_cash = $('#amount_receipt_cash').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_atm = 0;
        if ($('#amount_receipt_atm').val() != undefined && $('#amount_receipt_atm').val() != '') {
            amount_receipt_atm = $('#amount_receipt_atm').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_visa = 0;
        if ($('#amount_receipt_visa').val() != undefined && $('#amount_receipt_visa').val() != '') {
            amount_receipt_visa = $('#amount_receipt_visa').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_money = 0;
        if ($('#amount_receipt_money').val() != undefined && $('#amount_receipt_money').val() != '') {
            amount_receipt_money = $('#amount_receipt_money').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_all = Number(amount_receipt_cash) + Number(amount_receipt_atm) + Number(amount_receipt_visa) + Number(amount_receipt_money);

        $('#amount_all').val(formatNumber(amount_all.toFixed(decimal_number)));
        $('.cl_amount_all').text(formatNumber(amount_all.toFixed(decimal_number)));

        var rest = $('#receipt_amount').val().replace(new RegExp('\\,', 'g'), '');
        if (rest - amount_all > 0) {
            $('#amount_rest').val(formatNumber((rest - amount_all).toFixed(decimal_number)));
            $('.cl_amount_rest').text(formatNumber((rest - amount_all).toFixed(decimal_number)));
            if ($(obj).val() == '') {
                if (rest - amount_all < 0) {
                    $('#amount_return').val(formatNumber((amount_all - rest).toFixed(decimal_number)));
                    $('.cl_amount_return').text(formatNumber((amount_all - rest).toFixed(decimal_number)));
                } else {
                    $('#amount_return').val(0);
                    $('.cl_amount_return').text(0);
                }
            }
        } else {
            $('#amount_rest').val(0);
            $('#amount_return').val(formatNumber((amount_all - rest).toFixed(decimal_number)));
            $('.cl_amount_rest').text(0);
            $('.cl_amount_return').text(formatNumber((amount_all - rest).toFixed(decimal_number)));
        }
        discountCustomerInput();
    },
};


$('#autotable').PioTable({
    baseUrl: laroute.route('admin.receipt.list')
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}