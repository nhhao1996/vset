var ServiceSerial = {
    edit : function () {
        var is_actived = 0;
        if ($('.is_actived').is(':checked')){
            is_actived = 1;
        }
        $.ajax({
            url: laroute.route('admin.service-serial.editPost'),
            method: 'POST',
            dataType: 'JSON',
            data: $('#edit-service-serial').serialize()+'&is_actived='+is_actived,
            success: function (res) {
                if (res.error == false) {
                    swal.fire(res.message,'','success').then(function () {
                        window.location.href=laroute.route('admin.service-serial');
                    });
                } else {
                    swal.fire(res.message,'','error');
                }
            },
        });
    }
}
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.service-serial.list')
});