$(document).ready(function () {
    $.getJSON(laroute.route('translate'), function (json) {
        $.ajax({
            url: laroute.route('admin.potential-customers.load-district'),
            dataType: 'JSON',
            data: {
                id_province: $('#province_id').val()
            },
            method: 'POST',
            success: function (res) {
                $.map(res.optionDistrict, function (a) {
                    $('.district').append('<option value="' + a.id + '">' + a.type + ' ' + a.name + '</option>');
                });
                // $('#district_id').val(778);
            }
        });

        $('#province_id').select2({
            placeholder: json["Chọn tỉnh/thành"],
        });

        $('#province_id').change(function () {
            $.ajax({
                url: laroute.route('admin.potential-customers.load-district'),
                dataType: 'JSON',
                data: {
                    id_province: $('#province_id').val(),
                },
                method: 'POST',
                success: function (res) {
                    $('.district').empty();
                    $.map(res.optionDistrict, function (a) {
                        $('.district').append('<option value="' + a.id + '">' + a.type + ' ' + a.name + '</option>');
                    });
                }
            });
        });

        $('#district_id').select2({
            placeholder: json["Chọn quận/huyện"],
            ajax: {
                url: laroute.route('admin.potential-customers.load-district'),
                data: function (params) {
                    return {
                        id_province: $('#province_id').val(),
                        search: params.term,
                        page: params.page || 1
                    };
                },
                dataType: 'JSON',
                method: 'POST',
                processResults: function (res) {
                    res.page = res.page || 1;
                    return {
                        results: res.optionDistrict.map(function (item) {
                            return {
                                id: item.id,
                                text: item.name
                            };
                        }),
                        pagination: {
                            more: res.pagination
                        }
                    };
                },
            }
        });

        $('#customer_group_id').select2({
            placeholder: json["Chọn nhóm khách hàng"],
        });
        $('.op_day').select2({
            placeholder: json["Ngày"],

        });
        $('#month').select2({
            placeholder: json["Tháng"],

        });
        $('#year').select2({
            placeholder: json["Năm"],

        });
        $("#created_at").daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                format: 'DD/MM/YYYY',
                daysOfWeek: [
                    json["CN"],
                    json["T2"],
                    json["T3"],
                    json["T4"],
                    json["T5"],
                    json["T6"],
                    json["T7"]
                ],
                "monthNames": [
                    json["Tháng 1 năm"],
                    json["Tháng 2 năm"],
                    json["Tháng 3 năm"],
                    json["Tháng 4 năm"],
                    json["Tháng 5 năm"],
                    json["Tháng 6 năm"],
                    json["Tháng 7 năm"],
                    json["Tháng 8 năm"],
                    json["Tháng 9 năm"],
                    json["Tháng 10 năm"],
                    json["Tháng 11 năm"],
                    json["Tháng 12 năm"]
                ],
                "firstDay": 1
            }
        });
        $('#search_birthday').datepicker({
            dataFormat: "dd/mm/yy",
            onSelect: function () {
                $(this).trigger('change');
            }
        });
        $('#customer_refer_id').select2({
            placeholder: json["Chọn người giới thiệu"],
            ajax: {
                url: laroute.route('admin.potential-customers.search-customer-refer'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1
                    };
                    return query;
                },
                processResults: function (response) {
                    console.log(response);
                    response.page = response.page || 1;
                    return {
                        results: response.search.results,
                        pagination: {
                            more: response.pagination
                        }
                    };
                },
                cache: true,
                delay: 250
            },
            allowClear: true
            // minimumInputLength: 3
        });
        $('#addphone').click(function () {
            $('#div-add').empty();
            $("#div-add").append('<input name="phone2" id="phone2" type="text" ' +
                'class="form-control m--margin-top-10" placeholder="Nhập số điện thoại"/>');
        });
        $('#birthday').datepicker({
            format: "dd/mm/yyyy"
        });
        $('#h_birthday').datepicker({
            format: "dd/mm/yyyy"
        });
        $('#h_customer_refer_id').select2({
            placeholder: json["Nhập thông tin người giới thiệu"],
            ajax: {
                url: laroute.route('admin.potential-customers.search-customer-refer'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1
                    };
                    return query;
                }
            },
            minimumInputLength: 3
        });
        $('#customer_source_id').select2({
            placeholder: json["Chọn nguồn khách hàng"],
            allowClear: true
        });
        $('#addphone-edit').click(function () {
            if ($("#h_phone2").length == 0) {
                $('#div-add-edit').empty();
                $("#div-add-edit").append('<input name="phone2" id="h_phone2" ' +
                    'class="form-control col-lg" placeholder="Nhập số điện thoại"/>');
            }

        });
        $('#action').click(function () {
            $('#div-action').css('display', 'block');
            $('#div-service').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-debt').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'none');
        });
        $('#service').click(function () {
            $('#div-service').css('display', 'block');
            $('#div-action').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-debt').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'none');
        });
        $('#calendar').click(function () {
            $('#div-calendar').css('display', 'block');
            $('#div-action').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-service').css('display', 'none');
            $('#div-debt').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'none');
        });
        $('#order').click(function () {
            $('#div-order').css('display', 'block');
            $('#div-action').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-service').css('display', 'none');
            $('#div-debt').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'none');
        });
        $('#debt').click(function () {
            $('#div-debt').css('display', 'block');
            $('#div-action').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-service').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'none');
        });
        $('#order_commission').click(function () {
            $('#div-debt').css('display', 'none');
            $('#div-action').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-service').css('display', 'none');
            $('#div-order-commission').css('display', 'block');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'none');
        });
        $('#commission_log').click(function () {
            $('#div-debt').css('display', 'none');
            $('#div-action').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-service').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'block');
            $('#div-loyalty').css('display', 'none');
        });
        $('#loyalty').click(function () {
            $('#div-debt').css('display', 'none');
            $('#div-action').css('display', 'none');
            $('#div-calendar').css('display', 'none');
            $('#div-order').css('display', 'none');
            $('#div-service').css('display', 'none');
            $('#div-order-commission').css('display', 'none');
            $('#div-commission-log').css('display', 'none');
            $('#div-loyalty').css('display', 'block');
        });

        $('.btn-delete-img').click(function () {
            $('.img').remove();
            // $(this).remove();
            $('#ima').val('');

        });
        $('.btn-add').click(function () {
            $('#form-add').validate({
                rules: {
                    customer_group_id: {
                        required: true
                    },
                    full_name: {
                        required: true
                    },
                    phone1: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 11
                    },
                    address: {
                        required: true
                    },
                },
                messages: {
                    customer_group_id: {
                        required: json["Hãy chọn nhóm khách hàng"]
                    },
                    full_name: {
                        required: json["Hãy nhập tên khách hàng"]
                    },
                    phone1: {
                        required: json["Hãy nhập số điện thoại"],
                        number: json["Số điện thoại không hợp lệ"],
                        minlength: json["Tối thiểu 10 số"],
                        maxlength: json["Tối đa 11 số"]
                    },
                    address: {
                        required: json["Hãy nhập địa chỉ"]
                    },

                },
                submitHandler: function () {
                    mApp.block("#m_blockui_1_content", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var gender = $('input[name="gender"]:checked').val();
                    var customer_group_id = $('#customer_group_id').val();
                    var full_name = $('#full_name').val();
                    var phone1 = $('#phone1').val();
                    var phone2 = $('#phone2').val();
                    var province_id = $('#province_id').val();
                    var district_id = $('#district_id').val();
                    var address = $('#address').val();
                    var email = $('#email').val();
                    var day = $('#day').val();
                    var month = $('#month').val();
                    var year = $('#year').val();
                    var customer_source_id = $('#customer_source_id').val();
                    var customer_refer_id = $('#customer_refer_id').val();
                    var facebook = $('#facebook').val();
                    var note = $('#note').val();
                    var customer_avatar = $('#customer_avatar').val();
                    if (email != '') {
                        if (!isValidEmailAddress(email)) {
                            mApp.unblock("#m_blockui_1_content");
                            $('.error_email').text(json["Email không hợp lệ"]);
                            return false;
                        } else {

                            $('.error_email').text('');
                            $.ajax({
                                url: laroute.route('admin.potential-customers.submitAdd'),
                                dataType: 'JSON',
                                method: 'POST',
                                data: {
                                    gender: gender,
                                    customer_group_id: customer_group_id,
                                    full_name: full_name,
                                    phone1: phone1,
                                    phone2: phone2,
                                    province_id: province_id,
                                    district_id: district_id,
                                    address: address,
                                    email: email,
                                    day: day,
                                    month: month,
                                    year: year,
                                    customer_source_id: customer_source_id,
                                    customer_refer_id: customer_refer_id,
                                    facebook: facebook,
                                    note: note,
                                    customer_avatar: customer_avatar,
                                    postcode: $('#postcode').val()
                                },
                                success: function (res) {
                                    mApp.unblock("#m_blockui_1_content");
                                    if (res.error_phone1 == 1) {
                                        $('.error_phone1').text(json["Số điện thoại đã tồn tại"]);
                                    } else {
                                        $('.error_phone1').text('');
                                    }
                                    if (res.error_phone2 == 1) {
                                        $('.error_phone2').text(json["Số điện thoại đã tồn tại"]);
                                    } else {
                                        $('.error_phone2').text('');
                                    }
                                    if (res.error_birthday == 1) {
                                        $('.error_birthday').text(json["Ngày sinh không hợp lệ"]);
                                    } else {
                                        $('.error_birthday').text('');
                                    }
                                    if (res.success == 1) {
                                        swal(json["Thêm khách hàng thành công"], "", "success");
                                        window.location.reload();
                                    }
                                }
                            });
                        }
                    } else {
                        $.ajax({
                            url: laroute.route('admin.potential-customers.submitAdd'),
                            dataType: 'JSON',
                            method: 'POST',
                            data: {
                                gender: gender,
                                customer_group_id: customer_group_id,
                                full_name: full_name,
                                phone1: phone1,
                                phone2: phone2,
                                province_id: province_id,
                                district_id: district_id,
                                address: address,
                                email: email,
                                day: day,
                                month: month,
                                year: year,
                                customer_source_id: customer_source_id,
                                customer_refer_id: customer_refer_id,
                                facebook: facebook,
                                note: note,
                                customer_avatar: customer_avatar,
                                postcode: $('#postcode').val()
                            },
                            success: function (res) {
                                mApp.unblock("#m_blockui_1_content");
                                if (res.error_phone1 == 1) {
                                    $('.error_phone1').text(json["Số điện thoại đã tồn tại"]);
                                } else {
                                    $('.error_phone1').text('');
                                }
                                if (res.error_phone2 == 1) {
                                    $('.error_phone2').text(json["Số điện thoại đã tồn tại"]);
                                } else {
                                    $('.error_phone2').text('');
                                }
                                if (res.error_birthday == 1) {
                                    $('.error_birthday').text(json["Ngày sinh không hợp lệ"]);
                                } else {
                                    $('.error_birthday').text('');
                                }
                                if (res.success == 1) {
                                    swal(json["Thêm khách hàng thành công"], "", "success");
                                    window.location.reload();
                                }
                            }
                        });
                    }

                }
            });
        });
        $('.btn-add-close').click(function () {
            $('#form-add').validate({
                rules: {
                    customer_group_id: {
                        required: true
                    },
                    full_name: {
                        required: true
                    },
                    phone1: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 11
                    },
                    address: {
                        required: true
                    },
                },
                messages: {
                    customer_group_id: {
                        required: json["Hãy chọn nhóm khách hàng"]
                    },
                    full_name: {
                        required: json["Hãy nhập tên khách hàng"]
                    },
                    phone1: {
                        required: json["Hãy nhập số điện thoại"],
                        number: json["Số điện thoại không hợp lệ"],
                        minlength: json["Tối thiểu 10 số"],
                        maxlength: json["Tối đa 10 số"],
                    },
                    address: {
                        required: json["Hãy nhập địa chỉ"]
                    },

                },
                submitHandler: function () {
                    mApp.block("#m_blockui_1_content", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var gender = $('input[name="gender"]:checked').val();
                    var customer_group_id = $('#customer_group_id').val();
                    var full_name = $('#full_name').val();
                    var phone1 = $('#phone1').val();
                    var phone2 = $('#phone2').val();
                    var province_id = $('#province_id').val();
                    var district_id = $('#district_id').val();
                    var address = $('#address').val();
                    var email = $('#email').val();
                    var day = $('#day').val();
                    var month = $('#month').val();
                    var year = $('#year').val();
                    var customer_source_id = $('#customer_source_id').val();
                    var customer_refer_id = $('#customer_refer_id').val();
                    var facebook = $('#facebook').val();
                    var note = $('#note').val();
                    var customer_avatar = $('#customer_avatar').val();
                    if (email != '') {
                        if (!isValidEmailAddress(email)) {
                            $('.error_email').text(json["Email không hợp lệ"]);
                            mApp.unblock("#m_blockui_1_content");
                            return false;
                        } else {
                            $('.error_email').text('');
                            $.ajax({
                                url: laroute.route('admin.potential-customers.submitAdd'),
                                dataType: 'JSON',
                                method: 'POST',
                                data: {
                                    gender: gender,
                                    customer_group_id: customer_group_id,
                                    full_name: full_name,
                                    phone1: phone1,
                                    phone2: phone2,
                                    province_id: province_id,
                                    district_id: district_id,
                                    address: address,
                                    email: email,
                                    day: day,
                                    month: month,
                                    year: year,
                                    customer_source_id: customer_source_id,
                                    customer_refer_id: customer_refer_id,
                                    facebook: facebook,
                                    note: note,
                                    customer_avatar: customer_avatar,
                                    postcode: $('#postcode').val()
                                },
                                success: function (res) {
                                    mApp.unblock("#m_blockui_1_content");
                                    if (res.error_phone1 == 1) {
                                        $('.error_phone1').text(json["Số điện thoại đã tồn tại"]);
                                    } else {
                                        $('.error_phone1').text('');
                                    }
                                    if (res.error_phone2 == 1) {
                                        $('.error_phone2').text(json["Số điện thoại đã tồn tại"]);
                                    } else {
                                        $('.error_phone2').text('');
                                    }
                                    if (res.error_birthday == 1) {
                                        $('.error_birthday').text(json["Ngày sinh không hợp lệ"]);
                                    } else {
                                        $('.error_birthday').text('');
                                    }
                                    if (res.success == 1) {
                                        swal(json["Thêm khách hàng thành công"], "", "success");
                                        window.location = laroute.route('admin.potential-customers');
                                    }
                                }
                            });
                        }
                    } else {
                        $.ajax({
                            url: laroute.route('admin.potential-customers.submitAdd'),
                            dataType: 'JSON',
                            method: 'POST',
                            data: {
                                gender: gender,
                                customer_group_id: customer_group_id,
                                full_name: full_name,
                                phone1: phone1,
                                phone2: phone2,
                                province_id: province_id,
                                district_id: district_id,
                                address: address,
                                email: email,
                                day: day,
                                month: month,
                                year: year,
                                customer_source_id: customer_source_id,
                                customer_refer_id: customer_refer_id,
                                facebook: facebook,
                                note: note,
                                customer_avatar: customer_avatar,
                                postcode: $('#postcode').val()
                            },
                            success: function (res) {
                                mApp.unblock("#m_blockui_1_content");
                                if (res.error_phone1 == 1) {
                                    $('.error_phone1').text(json["Số điện thoại đã tồn tại"]);
                                } else {
                                    $('.error_phone1').text('');
                                }
                                if (res.error_phone2 == 1) {
                                    $('.error_phone2').text(json["Số điện thoại đã tồn tại"]);
                                } else {
                                    $('.error_phone2').text('');
                                }
                                if (res.error_birthday == 1) {
                                    $('.error_birthday').text(json["Ngày sinh không hợp lệ"]);
                                } else {
                                    $('.error_birthday').text('');
                                }
                                if (res.success == 1) {
                                    swal(json["Thêm khách hàng thành công"], "", "success");
                                    window.location = laroute.route('admin.potential-customers');
                                }
                            }
                        });
                    }

                }
            });
        });
        // $('#amount_debt').mask('000,000,000', {reverse: true});
    });
});

var customer = {

    changeStatus: function (obj, id, action) {
        // // $.getJSON(laroute.route('translate'), function (json) {
        // $.ajax({
        //     url: laroute.route('admin.potential-customers.change-status'),
        //     method: "POST",
        //     data: {
        //         id: id, action: action
        //     },
        //     dataType: "JSON"
        // }).done(function (data) {
        //     $('#autotable').PioTable('refresh');
        // });
        // // });

            var is_active = 0;
            if ($(obj).is(':checked')) {
                is_active = 1;
            }
            $.ajax({
                url: laroute.route('admin.potential-customers.change-status'),
                method: "POST",
                dataType: "JSON",
                data: {
                    id: id,
                    is_active: is_active
                },
                success: function (res) {
                    if (res.error == false) {
                        swal.fire(res.message, "", "success");
                    } else {
                        swal.fire(res.message, '', "error");
                    }
                }
            });
    },

};

var detail = {
    _init: function () {

    },
    modal_process_card: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('admin.potential-customers.modal-process-card'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    id: id_customer
                },
                success: function (res) {
                    $('#my-popup').html(res.url);
                    $('#my-popup').find('#modal-process-card').modal({
                        backdrop: 'static', keyboard: false
                    });
                    $('#service_card_id').select2({
                        placeholder: json["Chọn thẻ dịch vụ"]
                    });
                    $('#actived_date').datepicker({
                        format: 'dd/mm/yyyy',
                        language: 'vi',
                        // startDate: '0d'
                    });
                    $('#expired_date').datepicker({
                        format: 'dd/mm/yyyy',
                        language: 'vi',
                        // startDate: '0d'
                    });
                }
            });
        });
    },
    choose_service_card: function (obj) {
        $.ajax({
            url: laroute.route('admin.potential-customers.choose-service-card'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                service_card_id: $(obj).val()
            },
            success: function (res) {
                $('#actived_date').val('').prop("disabled", false);
                $('#expired_date').val('').prop("disabled", true);

                if (res.service_card.service_card_type == 'money') {
                    $('#count_using').val(1).prop("disabled", true);
                    $('#end_using').val(0).prop("disabled", true);
                } else {
                    $('#count_using').val('').prop("disabled", false);
                    $('#end_using').val('').prop("disabled", false);
                }
            }
        });
    },
    change_active_date: function (obj) {
        $.ajax({
            url: laroute.route('admin.potential-customers.change-active-date'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                service_card_id: $('#service_card_id').val(),
                actived_date: $(obj).val()
            },
            success: function (res) {
                $('#expired_date').prop("disabled", false);
                if (res.expired_date != 0) {
                    $('#expired_date').val(res.expired_date);
                }
            }
        });
    },
    submit_process_card: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-process-card');

            form.validate({
                rules: {
                    service_card_id: 'required',
                    actived_date: 'required',
                    expired_date: 'required',
                    count_using: {
                        number: true,
                        required: true
                    },
                    end_using: {
                        number: true,
                        required: true
                    }
                },
                messages: {
                    service_card_id: {
                        required: json["Hãy chọn thẻ dịch vụ"]
                    },
                    actived_date: {
                        required: json["Hãy chọn ngày kích hoạt"],
                    },
                    expired_date: {
                        required: json["Hãy chọn ngày hết hạn"],
                    },
                    count_using: {
                        required: json["Hãy nhập số lần sử dụng"],
                        number: json["Số lần sử dụng không hợp lệ"],
                    },
                    end_using: {
                        required: json["Hãy nhập số lần còn lại"],
                        number: json["Số lần sử dụng không hơp lệ"]
                    },
                }
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('admin.potential-customers.submit-process-card'),
                dataType: 'JSON',
                method: 'POST',
                data: {
                    customer_id: id,
                    service_card_id: $('#service_card_id').val(),
                    actived_date: $('#actived_date').val(),
                    expired_date: $('#expired_date').val(),
                    count_using: $('#count_using').val(),
                    end_using: $('#end_using').val()
                },
                success: function (res) {
                    if (res.error == true) {
                        swal(json["Tạo thẻ liệu trình thất bại"], "", "error");
                    } else {
                        swal(json["Tạo thẻ liệu trình thành công"], "", "success");
                        window.location.reload();
                    }
                },
                error: function (res) {
                    swal(json["Tạo thẻ liệu trình thất bại"], "", "error");
                }
            });
        });
    },
    modal_commission: function (id, commission_money) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('admin.potential-customers.commission'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_id: id,
                    commission_money: commission_money
                },
                success: function (res) {
                    $('#my-popup').html(res.url);
                    $('#my-popup').find('#modal-commission').modal({
                        backdrop: 'static', keyboard: false
                    });
                    $('#type').select2({
                        placeholder: json["Chọn hình thức quy đổi"]
                    });
                    $('#money').mask('000,000,000', {reverse: true});
                }
            });
        });
    },
    submit_commission: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-commission');

            form.validate({
                rules: {
                    money: {
                        required: true
                    },
                    type: {
                        required: true
                    }
                },
                messages: {
                    money: {
                        required: json["Hãy nhập tiền quy đổi"]
                    },
                    type: {
                        required: json["Hãy chọn hình thức quy đổi"]
                    }
                }
            });
            if (!form.valid()) {
                return false;
            }
            $.ajax({
                url: laroute.route('admin.potential-customers.submit-commission'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    money: $('#money').val(),
                    type: $('#type').val(),
                    note: $('#note').val(),
                    customer_id: id
                },
                success: function (res) {
                    if (res.error == true) {
                        swal(json["Quy đổi tiền thất bại"], "", "error");
                    } else {
                        swal(json["Quy đổi tiền thành công"], "", "success");
                        window.location.reload();
                    }
                },
                error: function (res) {
                    swal(json["Quy đổi tiền thất bại"], "", "error");
                }
            });
        });
    }
};


$('#autotable').PioTable({
    baseUrl: laroute.route('admin.potential-customers.list')
});


function onmouseoverAddNew() {
    $('.dropdow-add-new').show();
}


function onmouseoutAddNew() {
    $('.dropdow-add-new').hide();
}


$('.btn-add-phone2').click(function () {
    $('.phone2').show(350);
    $('.btn-add-phone2').hide(350);
});
$('.delete-phone').click(function () {
    $('.phone2').hide(350);
    $('#phone2').val('');
    $('.btn-add-phone2').show(350);
});
$('.m_selectpicker').selectpicker();

$('.add-new-info').click(function () {
    if ($('.hidden-add-info').val() == 0) {
        $('.add-info').show(350);
        $('.hidden-add-info').val(1);
    } else {
        $('.add-info').hide(350);
        $('.hidden-add-info').val(0);
    }

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function isValidDate(year, month, day) {
    var d = new Date(year, month, day);
    if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
        return true;
    }
    return false;
}

function uploadImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#customer_avatar');
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);

        };

        reader.readAsDataURL(input.files[0]);

        var file_data = $('#getFile').prop('files')[0];
        console.log(file_data);
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: laroute.route("admin.potential-customers.uploads"),
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.success == 1) {
                    $('#customer_avatar').val(res.file);
                }
            }
        });
    }
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
