"use strict";

var vsetScript = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.potential-customers.list-history-customer-vset'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_history'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'order_code',
                    title: 'Mã đơn hàng',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    template: function (row) {
                        return '<span>' + row.order_code + '</span>';
                    }

                },
                {
                    field: 'product_name_vi',
                    title: 'Tên gói',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span>' + row.product_name_vi + '</span>';
                    }
                },
                {
                    field: 'price_standard',
                    title: 'Giá',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.price_standard).toLocaleString()  + ' VNĐ </span>';
                    }

                },
                {
                    field: 'payment_method_name_vi',
                    title: 'Hình thức giao dịch',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {

                        return '<span class="m--font-bolder">' + row.payment_method_name_vi +'</span>';
                    }

                },
                {
                    field: 'quantity',
                    title: 'Số lượng',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {

                        return '<span class="m--font-bolder">' + row.quantity +'</span>';
                    }

                },
                {
                    field: 'process_status',
                    title: 'Trạng thái',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        if (row.process_status == 'new') {
                            return '<span class="m--font-bolder">Mới</span>';
                        } else if (row.process_status == 'paysuccess') {
                            return '<span class="m--font-bolder">Thanh toán thành công</span>';
                        } else if (row.process_status == 'confirmed') {
                            return '<span class="m--font-bolder">Xác nhận</span>';
                        } else if (row.process_status == 'pay-half') {
                            return '<span class="m--font-bolder">Thanh toán 1 phần</span>';
                        }
                    }

                },
                {
                    field: 'total',
                    title: 'Thành tiền',
                    filterable: false, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.total).toLocaleString() + ' VNĐ </span>';
                    }
                },
                {
                    field: 'created_at',
                    title: 'Ngày đặt hàng',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        let d = new Date(row.created_at);
                        let day = d.getDate();
                        let month = d.getMonth() + 1;
                        let year = d.getFullYear();
                        let hour = d.getHours();
                        let minute = d.getMinutes();
                        let second =d.getSeconds();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        if (second < 10) {
                            second = "0" + second;
                        }
                        let date = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
                        return '<span>' + date + '</span>';
                    }
                }
            ],
        };
        vsetScript.pioTable = $('#datatable_list_history').mDatatable(options);
    },
    tab_list_history:function () {
        $('#datatable_list_history').mDatatable().search('');
    },
};

// setTimeout(function(){
//
// }, 1200);
vsetScript.init();