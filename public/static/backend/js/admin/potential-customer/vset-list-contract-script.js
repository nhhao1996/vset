"use strict";

var vsetListContract = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.potential-customers.list-product-view'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_contract'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: 'type',
                    title: 'Loại',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        if (row.type == 'product'){
                            return '<span>'+row.category+'</span>';
                        } else {
                            return '<span>Dịch vụ</span>';
                        }

                    }
                },
                {
                    field: 'name',
                    title: 'Tên',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>'+row.name+'</span>';

                    }
                },
                {
                    field: 'total',
                    title: 'Số lần',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>'+row.total+'</span>';

                    }
                },
            ],
        };
        vsetListContract.pioTable = $('#datatable_list_product_view').mDatatable(options);
    },
    tab_list_product_view:function () {
        $('#datatable_list_product_view').mDatatable().search('');
    }
};

// setTimeout(function(){
//
// }, 1200);
vsetListContract.init();

var vsetListCustomerSearch = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.potential-customers.list-customer-search'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_contract'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: 'type',
                    title: 'Loại',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        if (row.type == 'product'){
                            return '<span>'+row.category+'</span>';
                        } else {
                            return '<span>Dịch vụ</span>';
                        }

                    }
                },
                {
                    field: 'name',
                    title: 'Tên',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>'+row.name+'</span>';

                    }
                },
                {
                    field: 'total',
                    title: 'Số lần',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>'+row.total+'</span>';

                    }
                },
            ],
        };
        vsetListCustomerSearch.pioTable = $('#datatable_list_customer_search').mDatatable(options);
    },
    tab_list_customer_search:function () {
        $('#datatable_list_customer_search').mDatatable().search('');
    }
};

// setTimeout(function(){
//
// }, 1200);
vsetListCustomerSearch.init();