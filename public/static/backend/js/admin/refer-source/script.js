
$(document).ready(function () {
    // $('input[name="type_bonus_refer"]').trigger('click');
    $('input[name="type_bonus_refer"]').click(function () {
        if ($(this).val() == 'money'){
            $('.voucher_refer').prop('disabled',true);
            $('.money_refer').prop('disabled',false);
        } else {
            $('.money_refer').prop('disabled',true);
            $('.voucher_refer').prop('disabled',false);
        }
    });

    $('input[name="type_bonus_register"]').click(function () {
        if ($(this).val() == 'money'){
            $('.voucher_register').prop('disabled',true);
            $('.money_register').prop('disabled',false);
        } else {
            $('.money_register').prop('disabled',true);
            $('.voucher_register').prop('disabled',false);
        }
    });
});

var linkSource = {

    deleteSearch: function(){
        $('#service_name').val('');
        $('#service_category').val('').trigger('change');
        linkSource.searchPopup(1);
    },

    removeService : function (obj,id,type) {
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa dịch vụ không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route("admin.refer-source.removeService"),
                    method: "POST",
                    data: {
                        service_id : id,
                        type: type
                    },
                    success: function (res) {
                        $('.body_tr_'+type+' #tr_'+id).remove();
                    }
                });
            }
        });
    },

    showPopup: function(type) {
        linkSource.searchPopup(1,type);
    },

    searchPopup: function(page,type = null){
        if (type == null) {
            type = $('#type_popup').val();
        } else {
            $('#type_popup').val(type);
        }
        $.ajax({
            url: laroute.route("admin.refer-source.getListService"),
            method: "POST",
            data: $('#search_service').serialize()+'&page='+page+'&type='+type+'&refer_source_id='+$('#refer_source_id').val(),
            success: function (res) {
                console.log(res);
                $('.table-service').empty();
                $('.table-service').append(res.view);
                $('#popup_service').modal('show');
            }
        });
    },

    addListService: function () {
        var listService = [];
        $.each($('.table_service_popup').find(".service_id"), function () {
            if ($(this).is(':checked')){
                listService.push($(this).val());
            }
        });
        if (listService.length == 0) {
            swal.fire('Vui lòng chọn dịch vụ để thêm', "", "error")
        } else {
            $.ajax({
                url: laroute.route("admin.refer-source.addListService"),
                method: "POST",
                data: {
                    listService : listService,
                    type : $('#type_popup').val(),
                    refer_source_id : $('#refer_source_id').val()
                },
                success: function (res) {
                    if ($('#type_popup').val() == 'refer') {
                        $('.body_tr_refer').append(res.view);
                    } else {
                        $('.body_tr_register').append(res.view);
                    }

                }
            });
        }
    },

    saveConfig: function (type) {
        if (type == 'refer') {
            var is_active = 0;
            if ($('.is_active_refer').is(':checked')){
                is_active = 1;
            }

            data = $('.form-refer').serialize()+'&is_active='+is_active+'&type='+type+'&refer_source_id='+$('#refer_source_id').val()
        } else {
            var is_active = 0;
            if ($('.is_active_register').is(':checked')){
                is_active = 1;
            }
            data = $('.form-register').serialize()+'&is_active='+is_active+'&type='+type+'&refer_source_id='+$('#refer_source_id').val()
        }
        $.ajax({
            url: laroute.route("admin.refer-source.saveConfig"),
            method: "POST",
            data: data,
            success: function (res) {
                if (res.error == false){
                    swal.fire(res.message,'','success').then(function () {
                        location.reload();
                    });
                } else {
                    swal.fire(res.message,'','error');
                }
            }
        });
    },
}