var StockContractHandler = {
    init(){

    },
    editStockContract(id){       
        $.ajax({
            url: laroute.route('admin.stock-contract.submitEditStockContract'),
            method: "POST",
            dataType: "JSON",
            data:$(`#frmStockContract`).serialize(),

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    }
}