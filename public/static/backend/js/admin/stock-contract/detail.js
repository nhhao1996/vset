$(document).ready(function () {

    $('#file_contract').click(function () {
        $('#div_file_contract').css('display', 'block');
        $('#div_interest_month').css('display', 'none');
        $('#div_interest_date').css('display', 'none');
        $('#div_interest_time').css('display', 'none');
        $('#div_withdraw_group').css('display', 'none');
        $('#div_withdraw').css('display', 'none');
        $('#div_serial_contract').css('display', 'none');
    });
    $('#interest_month').click(function () {
        $('#div_file_contract').css('display', 'none');
        $('#div_interest_month').css('display', 'block');
        $('#div_interest_date').css('display', 'none');
        $('#div_interest_time').css('display', 'none');
        $('#div_withdraw_group').css('display', 'none');
        $('#div_withdraw').css('display', 'none');
        $('#div_serial_contract').css('display', 'none');
    });
    $('#interest_date').click(function () {
        $('#div_file_contract').css('display', 'none');
        $('#div_interest_month').css('display', 'none');
        $('#div_interest_date').css('display', 'block');
        $('#div_interest_time').css('display', 'none');
        $('#div_withdraw_group').css('display', 'none');
        $('#div_withdraw').css('display', 'none');
        $('#div_serial_contract').css('display', 'none');
    });
    $('#interest_time').click(function () {
        $('#div_file_contract').css('display', 'none');
        $('#div_interest_month').css('display', 'none');
        $('#div_interest_date').css('display', 'none');
        $('#div_interest_time').css('display', 'block');
        $('#div_withdraw_group').css('display', 'none');
        $('#div_withdraw').css('display', 'none');
        $('#div_serial_contract').css('display', 'none');
    });
    $('#withdraw_group').click(function () {
        $('#div_file_contract').css('display', 'none');
        $('#div_interest_month').css('display', 'none');
        $('#div_interest_date').css('display', 'none');
        $('#div_interest_time').css('display', 'none');
        $('#div_withdraw_group').css('display', 'block');
        $('#div_withdraw').css('display', 'none');
        $('#div_serial_contract').css('display', 'none');
    });
    $('#withdraw').click(function () {
        $('#div_file_contract').css('display', 'none');
        $('#div_interest_month').css('display', 'none');
        $('#div_interest_date').css('display', 'none');
        $('#div_interest_time').css('display', 'none');
        $('#div_withdraw_group').css('display', 'none');
        $('#div_withdraw').css('display', 'block');
        $('#div_serial_contract').css('display', 'none');
    });
    $('#serial_contract').click(function () {
        $('#div_file_contract').css('display', 'none');
        $('#div_interest_month').css('display', 'none');
        $('#div_interest_date').css('display', 'none');
        $('#div_interest_time').css('display', 'none');
        $('#div_withdraw_group').css('display', 'none');
        $('#div_withdraw').css('display', 'none');
        $('#div_serial_contract').css('display', 'block');
    });

    //List file hợp đồng

    $('#autotable-file').PioTable({
        baseUrl: laroute.route('admin.stock-contract.list-file')
    });
    $('#autotable-file').PioTable('search');
    $('#autotable-sub-file').PioTable({
        baseUrl: laroute.route('admin.stock-contract.list-sub-file')
    });
    $('#autotable-sub-file').PioTable('search');


    //List lãi xuất tháng
    $('#autotable-interest-month').PioTable({
        baseUrl: laroute.route('admin.customer-contract.list-interest-month')
    });
    $('#autotable-interest-month').PioTable('search');

    //List lãi xuất ngày
    $('#autotable-interest-date').PioTable({
        baseUrl: laroute.route('admin.customer-contract.list-interest-date')
    });
    $('#autotable-interest-date').PioTable('search');

    //List lãi xuất thời điểm
    $('#autotable-interest-time').PioTable({
        baseUrl: laroute.route('admin.customer-contract.list-interest-time')
    });
    $('#autotable-interest-time').PioTable('search');

    //List yêu cầu rút lãi
    $('#autotable-withdraw').PioTable({
        baseUrl: laroute.route('admin.customer-contract.list-withdraw-request')
    });
    $('#autotable-withdraw').PioTable('search');

    //List số serial
    $('#autotable-serial-contract').PioTable({
        baseUrl: laroute.route('admin.customer-contract.list-contract-serial')
    });
    $('#autotable-serial-contract').PioTable('search');

});

var contract = {
    confirm : function (id) {
        $.ajax({
            url: laroute.route('admin.customer-contract.confirm-contract'),
            method: "POST",
            dataType: "JSON",
            data: {
                id : id
            },
            success: function (res) {
                if (res.error == true) {
                    swal(res.message, "", "error");
                } else {
                    swal(res.message, "", "success").then(function () {
                        $('.btn-confirm-'+id).hide();
                    });
                }
            },
        });
    }
}


