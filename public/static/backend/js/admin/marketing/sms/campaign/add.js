$("#time-send").timepicker({
    minuteStep: 15,
    defaultTime: "08:00:00",
    showMeridian: !1,
    snapToStep: !0,
});
$('#day-send').datepicker({
    format: "dd/mm/yyyy",
    startDate: '0d',
    language: 'en',
}).datepicker("setDate", new Date());
;
var AddCampaign = {
    countCharacter: function (o) {
        let flag = true;
        let lengths = $(o).val().length;
        $.getJSON(laroute.route('translate'), function (json) {
            $('.count-character').text(lengths);
            if (lengths > 480) {
                $('.error-count-character').text(json['Vượt quá 480 ký tự.']);
                flag = false;
            } else {
                $('.error-count-character').text('');
            }
            $(o).val(AddCampaign.changeAlias(o));
        });
        return flag;
    },
    valueParameter: function (o) {
        if (o == "customer-name") {
            AddCampaign.insertAtCaret("{CUSTOMER_NAME}");
        }
        if (o == "customer-birthday") {
            AddCampaign.insertAtCaret("{CUSTOMER_BIRTHDAY}");
        }
        if (o == "customer-gender") {
            AddCampaign.insertAtCaret("{CUSTOMER_GENDER}");
        }
        if (o == "full-name") {
            AddCampaign.insertAtCaret("{CUSTOMER_FULL_NAME}");
        }

        $('.count-character').text($('#message-content').val().length);
        AddCampaign.countCharacter('#message-content');
    },
    searchCustomer: function () {
        $.ajax({
            url: laroute.route('admin.sms.search-customer'),
            method: "POST",
            data: {
                keyword: $('#keyword').val(),
                birthday: $('#birthday').val(),
                gender: $('#gender').val(),
            },
            success: function (data) {
                $('.customer_list').empty();
                $.getJSON(laroute.route('translate'), function (json) {
                    var gender = json['Khác'];
                    $.map(data.result, function (a) {
                        if (a.gender == 'male') {
                            gender = json['Nam'];
                        } else if (a.gender == 'female') {
                            gender = json['Nữ'];
                        }
                        var tpl = $('#customer-list-tpl').html();
                        tpl = tpl.replace(/{name}/g, a.full_name);
                        tpl = tpl.replace(/{customer_id}/g, a.customer_id);
                        tpl = tpl.replace(/{birthday}/g, a.birthday);
                        tpl = tpl.replace(/{phone}/g, a.phone);
                        tpl = tpl.replace(/{gender1}/g, a.gender);
                        tpl = tpl.replace(/{gender}/g, gender);
                        $('.customer_list').append(tpl);
                    });
                    sttAction();
                });

            }
        })
    },
    chooseCustomer: function () {
        var array = [];
        $.each($('.customer_list tr .check:checked').parentsUntil("tbody"), function () {
            var $tds = $(this).find("td input");
            $.each($tds, function () {
                array.push($(this).val());
            });
        });
        $.getJSON(laroute.route('translate'), function (json) {
            if (array == "") {
                $('.error-add-customer').text(json['Vui lòng thêm khách hàng.']);
            } else {
                $('.table-list-customer').empty();
                var result = chunkArray(array, 6);
                $.map(result, function (value) {
                    var tpl = $('#add-customer-list').html();
                    tpl = tpl.replace(/{customer_id}/g, value[0]);
                    tpl = tpl.replace(/{name}/g, value[1]);
                    tpl = tpl.replace(/{phone}/g, value[2]);
                    tpl = tpl.replace(/{birthday}/g, value[3]);
                    tpl = tpl.replace(/{gender1}/g, value[4]);
                    if (value[4] == 'male') {
                        tpl = tpl.replace(/{gender}/g, json['Nam']);
                    }
                    else if (value[4] == 'female') {
                        tpl = tpl.replace(/{gender}/g, json['Nữ']);
                    }
                    else {
                        tpl = tpl.replace(/{gender}/g, json['Khác']);
                    }
                    tpl = tpl.replace(/{daysend}/g, json['Chưa gửi']);
                    tpl = tpl.replace(/{status}/g, json["Đang cài đặt"]);
                    $('.table-list-customer').append(tpl);
                });
                sttAction();
                $('#m_modal_4').modal('hide');
            }
        });
    },
    removeCustomer: function (o) {
        $(o).closest('tr').remove();
        sttAction();
    },
    changeAlias: function (alias) {
        var str = $(alias).val();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");

        return str;
    },
    testTime: function (time1, time2) {
        let flag = true;
        var timeA = new Date();
        timeA.setHours(time1.split(":")[0], time1.split(":")[1]);
        var timeB = new Date();
        timeB.setHours(time2.split(":")[0], time2.split(":")[1]);
        if (timeA >= timeB) {
            flag = false;
        }
        return flag;
    },
    insertAtCaret: function (text) {
        var txtarea = document.getElementById('message-content');
        var scrollPos = txtarea.scrollTop;
        var caretPos = txtarea.selectionStart;

        var front = (txtarea.value).substring(0, caretPos);
        var back = (txtarea.value).substring(txtarea.selectionEnd, txtarea.value.length);
        txtarea.value = front + text + back;
        caretPos = caretPos + text.length;
        txtarea.selectionStart = caretPos;
        txtarea.selectionEnd = caretPos;
        txtarea.focus();
        txtarea.scrollTop = scrollPos;
    },
    testInput: function () {
        let flag = true;
        let name = $('#name').val();
        let content = $('#message-content').val();
        let daySent = $('#day-send').val();
        let timeSend = $('#time-send').val();
        let countCharacter = AddCampaign.countCharacter('#message-content');
        let errorName = $('.error-name');
        let errorContent = $('.error-count-character');
        let errorDateTime = $('.error-datetime');
        //Lấy thời gian hiện tại.
        var currentdate = new Date();
        let a = '';
        if (currentdate.getMonth() < 9) {
            a = '0'
        }
        var datetimeNow = +currentdate.getDate() + "-"
            + a + (currentdate.getMonth() + 1) + "-"
            + currentdate.getFullYear();
        var timeNow = currentdate.getHours() + ":"
            + currentdate.getMinutes();

        $.getJSON(laroute.route('translate'), function (json) {
            if (name == '') {
                flag = false;
                errorName.text(json['Vui lòng nhập tên chiến dịch']);
            } else {
                errorName.text('');
            }
            if (content == '') {
                flag = false;
                errorContent.text(json['Vui lòng nhập nội dung']);
            } else {
                errorContent.text('');
            }

            if ($('#is_now').is(':checked')) {

            } else {
                // let kq = AddCampaign.validateTwoDates(daySent.replace(new RegExp('\\/', 'g'), '-'), datetimeNow)
                // console.log(kq);
                // console.log(AddCampaign.process(daySent));
                // console.log(AddCampaign.process(datetimeNow.replace(new RegExp('\\-', 'g'), '/')));
                let kq = (AddCampaign.validateTwoDates(AddCampaign.process(daySent), AddCampaign.process(datetimeNow.replace(new RegExp('\\-', 'g'), '/'))));
                if (kq == false) {
                    if (AddCampaign.testTime(timeNow, timeSend) == false) {
                        errorDateTime.text(json['Thời gian gửi phải lớn hơn hoặc bằng thời gian hiện tại.']);
                        flag = false;
                    } else {
                        errorDateTime.text('');
                    }
                } else {
                    errorDateTime.text('');
                }
            }
        });
        return flag;
    },
    process: function (date) {
        var parts = date.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    },
    saveInfo: function () {
        if (AddCampaign.testInput() == true) {
            let isNow = 0;
            if ($('#is_now').is(':checked')) {
                isNow = 1;
            }
            $.ajax({
                url: laroute.route('admin.campaign.submit-add'),
                method: 'POST',
                data: {
                    name: $('#name').val(),
                    contents: $('#message-content').val(),
                    dateTime: $('#day-send').val() + " " + $('#time-send').val(),
                    isNow: isNow,
                },
                success: function (data) {
                    $.getJSON(laroute.route('translate'), function (json) {
                        if (data.error == 'slug') {
                            $('.error-name').text(json['Chiến dịch đã tồn tại.'])
                        }
                        if (data.error == 0) {
                            $('.error-name').text('');
                            swal(
                                json['Thêm chiến dịch thành công'],
                                '',
                                'success'
                            );
                            setTimeout(function () {
                                window.location = laroute.route('admin.campaign.edit', {id: data.id});
                            }, 1000);
                            // window.location.href = "{{URL::to('sms-campaign-edit/"+data.id+"')}}"
                        }
                    });
                }
            });
        }
    },
    validateTwoDates: function (time1, time2) {
        return (time1 > time2);
    }
};
$('#check-all').click(function () {
    if ($('#check-all').is(":checked")) {
        $('.check').prop("checked", true);
    } else {
        $('.check').prop("checked", false);

    }
});

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

function sttAction() {
    let stt = 1;
    $.each($('.stt'), function () {
        $(this).text(stt++);
    });
}

$('#gender').select2();
$('#birthday').datepicker({
    format: 'dd/mm/yyyy',
    language: 'vi',
});

$('#is_now').click(function () {
    if ($('#is_now').is(':checked')) {
        $('#day-send').prop('disabled', true);
        $('#time-send').prop('disabled', true);
    } else {
        $('#day-send').prop('disabled', false);
        $('#time-send').prop('disabled', false);
    }
});
$.ajaxSetup({
    async: false,
});