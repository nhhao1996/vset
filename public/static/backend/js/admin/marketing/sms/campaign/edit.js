var arrayLogDelete = new Array();

var EditCampaign = {
    countCharacter: function (o) {
        let flag = true;
        let lengths = $(o).val().length;
        $('.count-character').text(lengths);
        $.getJSON(laroute.route('translate'), function (json) {
            if (lengths > 480) {
                $('.error-count-character').text(json['Vượt quá 480 ký tự.']);
                flag = false;
            } else {
                $('.error-count-character').text('');
            }
        });
        $(o).val(EditCampaign.changeAlias(o));
        return flag;
    },
    changeAlias: function (alias) {
        var str = $(alias).val();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");

        return str;
    },
    insertAtCaret: function (text) {
        var txtarea = document.getElementById('message-content');
        var scrollPos = txtarea.scrollTop;
        var caretPos = txtarea.selectionStart;

        var front = (txtarea.value).substring(0, caretPos);
        var back = (txtarea.value).substring(txtarea.selectionEnd, txtarea.value.length);
        txtarea.value = front + text + back;
        caretPos = caretPos + text.length;
        txtarea.selectionStart = caretPos;
        txtarea.selectionEnd = caretPos;
        txtarea.focus();
        txtarea.scrollTop = scrollPos;
    },
    valueParameter: function (o) {
        if (o == "customer-name") {
            EditCampaign.insertAtCaret("{CUSTOMER_NAME}");
        }
        if (o == "customer-birthday") {
            EditCampaign.insertAtCaret("{CUSTOMER_BIRTHDAY}");
        }
        if (o == "customer-gender") {
            EditCampaign.insertAtCaret("{CUSTOMER_GENDER}");
        }
        if (o == "full-name") {
            EditCampaign.insertAtCaret("{CUSTOMER_FULL_NAME}");
        }

        $('.count-character').text($('#message-content').val().length);
        EditCampaign.countCharacter('#message-content');
    },
    testTime: function (time1, time2) {
        let flag = true;
        var timeA = new Date();
        timeA.setHours(time1.split(":")[0], time1.split(":")[1]);
        var timeB = new Date();
        timeB.setHours(time2.split(":")[0], time2.split(":")[1]);
        if (timeA >= timeB) {
            flag = false;
        }
        return flag;
    },
    validateTwoDates: function (time1, time2) {
        return (time1 > time2);
    },
    testInput: function () {
        let flag = true;

        let name = $('#name').val();
        let content = $('#message-content').val();
        let daySent = $('#day-send').val();
        let timeSend = $('#time-send').val();
        let countCharacter = EditCampaign.countCharacter('#message-content');
        let errorName = $('.error-name');
        let errorContent = $('.error-count-character');
        let errorDateTime = $('.error-datetime');
        //Lấy thời gian hiện tại.
        var currentdate = new Date();
        let a = '';
        if (currentdate.getMonth() < 9) {
            a = '0'
        }
        var datetimeNow = +currentdate.getDate() + "-"
            + a + (currentdate.getMonth() + 1) + "-"
            + currentdate.getFullYear();
        var timeNow = currentdate.getHours() + ":"
            + currentdate.getMinutes();
        $.getJSON(laroute.route('translate'), function (json) {
            if (name == '') {
                flag = false;
                errorName.text(json['Vui lòng nhập tên chiến dịch']);
            } else {
                errorName.text('');
            }
            if (content == '') {
                flag = false;
                errorContent.text(json['Vui lòng nhập nội dung']);
            } else {
                errorContent.text('');
            }

            if ($('#is_now').is(':checked')) {

            } else {
                let kq = (EditCampaign.validateTwoDates(
                    EditCampaign.process(daySent),
                    EditCampaign.process(datetimeNow.replace(new RegExp('\\-', 'g'), '/')))
                );
                if (kq == false) {
                    if (EditCampaign.testTime(timeNow, timeSend) == false) {
                        errorDateTime.text(json['Thời gian gửi phải lớn hơn hoặc bằng thời gian hiện tại.']);
                        flag = false;
                    } else {
                        errorDateTime.text('');
                    }
                } else {
                    errorDateTime.text('');
                }
            }
            if ($('#is_now').is(':checked')) {

            } else {
                if (daySent == '' || timeSend == '') {
                    errorDateTime.text(json['Thời gian gửi phải lớn hơn hoặc bằng thời gian hiện tại.']);
                    flag = false;
                }
            }
        });
        return flag;
    },
    process: function (date) {
        var parts = date.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    },
    saveChange: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            if (EditCampaign.testInput() == true) {
                let isNow = 0;
                if ($('#is_now').is(':checked')) {
                    isNow = 1;
                }
                $.ajax({
                    url: laroute.route('admin.campaign.submit-edit'),
                    method: 'POST',
                    data: {
                        name: $('#name').val(),
                        contents: $('#message-content').val(),
                        dateTime: $('#day-send').val() + " " + $('#time-send').val(),
                        isNow: isNow,
                        id: $('#id').val()
                    },
                    success: function (data) {
                        if (data.error == 'slug') {
                            $('.error-name').text(json['Chiến dịch đã tồn tại'])
                        }
                        if (data.error == 0) {
                            $('.error-name').text('');
                            swal(
                                json['Cập nhật chiến dịch thành công'],
                                '',
                                'success'
                            );
                        }
                    }
                });
            }
        });
    },
    searchCustomer: function () {
        $.ajax({
            url: laroute.route('admin.sms.search-customer'),
            method: "POST",
            async: false,
            data: {
                keyword: $('#keyword').val(),
                birthday: $('#birthday').val(),
                gender: $('#gender').val(),
            },
            success: function (data) {
                $('.customer_list').empty();

                $.getJSON(laroute.route('translate'), function (json) {
                    var gender = json['Khác'];
                    $.map(data.result, function (a) {
                        if (a.gender == 'male') {
                            gender = json['Nam'];
                        } else if (a.gender == 'female') {
                            gender = json['Nữ'];
                        }

                        var tpl = $('#customer-list-tpl').html();
                        tpl = tpl.replace(/{name}/g, a.full_name);
                        tpl = tpl.replace(/{customer_id}/g, a.customer_id);

                        if (a.birthday != null) {
                            tpl = tpl.replace(/{birthday}/g, a.birthday);
                        } else {
                            tpl = tpl.replace(/{birthday}/g, '');
                        }
                        tpl = tpl.replace(/{phone}/g, a.phone);
                        tpl = tpl.replace(/{gender}/g, gender);
                        $('.customer_list').append(tpl);
                    });
                    let stt = 1;
                    $.each($('.stt2'), function () {
                        $(this).text(stt++);
                    });
                });
            }
        })
    },
    sttAction: function () {
        let stt = 1;
        $.each($('.stt'), function () {
            $(this).text(stt++);
        });
    },
    chooseCustomer: function () {
        var array = [];
        $.each($('.customer_list tr .check:checked').parentsUntil("tbody"), function () {
            var $tds = $(this).find("td input");
            $.each($tds, function () {
                array.push($(this).val());
            });
        });
        $.getJSON(laroute.route('translate'), function (json) {
            if (array == "") {
                $('.error-add-customer').text(json['Vui lòng thêm khách hàng.']);
            } else {

                var result = chunkArray(array, 6);
                let messageContent = $('#message-content').val();

                $.map(result, function (value) {
                    let gioitinh = 'Anh';
                    if (value[4] == 'Nữ') {
                        gioitinh = 'Chi';
                    } else if (value[4] == json['Khác']) {
                        gioitinh = 'Anh/Chi';
                    }

                    let content = messageContent.replace(/{CUSTOMER_NAME}/g, " " + value[1].slice(value[1].lastIndexOf(' ') + 1, 20) + " ").replace(/{CUSTOMER_FULL_NAME}/g, " " + value[1] + " ").replace(/{CUSTOMER_BIRTHDAY}/g, " " + value[3] + " ").replace(/{CUSTOMER_GENDER}/g, " " + gioitinh + " ");
                    var tpl = $('#customer-list-append').html();
                    tpl = tpl.replace(/{customer_id}/g, value[0]);
                    tpl = tpl.replace(/{name}/g, value[1]);
                    tpl = tpl.replace(/{phone}/g, value[2]);
                    tpl = tpl.replace(/{content}/g, EditCampaign.xoa_dau(content));
                    $('.table-list-customer').append(tpl);
                });
                EditCampaign.sttAction();
                $('#add-customer').modal('hide');
            }
        });
    },
    xoa_dau: function (str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        return str;
    },
    removeCustomer: function (o) {
        $(o).closest('tr').remove();
        EditCampaign.sttAction();
    },
    saveLog: function () {
        var customer = $('.table-list-customer tr').length;
        let errorCustomerss = $('.error-customer-');
        let flag = true;

        $.getJSON(laroute.route('translate'), function (json) {
            if (customer < 1) {
                flag = false;
                errorCustomerss.text(json['Vui lòng chọn khách hàng'])
            } else {
                errorCustomerss.text('');
            }

            if (EditCampaign.testInput() == true && flag == true) {
                var array = [];
                $.each($('.table-list-customer tr .aaaa').parentsUntil("tbody"), function () {
                    var $tds = $(this).find("td input");
                    $.each($tds, function () {
                        array.push($(this).val());
                    });
                });
                $.ajax({
                    url: laroute.route('admin.campaign.sms-campaign-save-log'),
                    method: 'POST',
                    data: {
                        array: array,
                        id: $('#id').val(),
                        arrayLogDelete: arrayLogDelete
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.error == 0) {
                            swal(
                                json['Lưu danh sách gửi tin thành công'],
                                '',
                                'success'
                            );
                            setTimeout(function () {
                                location.reload();
                            }, 1500);
                        }
                    }
                });
            }
        });
    },
    removeCustomerLog: function (th, id) {
        $(th).closest('tr').remove();
        arrayLogDelete.push(id);
        EditCampaign.sttAction();
        // $.ajax({
        //     url: laroute.route('admin.campaign.remove-log'),
        //     method: 'POST',
        //     data: {id: id},
        //     success: function (data) {
        //         if (data.error == 0) {
        //             EditCampaign.sttAction();
        //             $(th).closest('tr').remove();
        //         }
        //     }
        // })
    },
    showNameFile: function () {
        var fileNamess = $('input[type=file]').val();
        $('.file-name-excels').val(fileNamess);
    },
    chooseFile: function () {
        var file_data = $('#file_excel').prop('files')[0];
        var fileNamess = $('input[type=file]').val();
        if (fileNamess != '') {
            $('.error-file-name-excels').text('');
            var form_data = new FormData();
            form_data.append('file', file_data);

            $.ajax({
                url: laroute.route("admin.campaign.import-file-excel"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    let messageContent = $('#message-content').val();

                    $.map(data, function (value) {

                        let content = messageContent.replace(/{CUSTOMER_NAME}/g, " " + value['customer_name'].slice(value['customer_name'].lastIndexOf(' ') + 1, 20) + " ").replace(/{CUSTOMER_FULL_NAME}/g, " " + value['customer_name'] + " ").replace(/{CUSTOMER_BIRTHDAY}/g, " " + value['birthday'] + " ").replace(/{CUSTOMER_GENDER}/g, " " + value['gender'] + " ");
                        var tpl = $('#customer-list-append').html();
                        tpl = tpl.replace(/{customer_id}/g, '');
                        tpl = tpl.replace(/{name}/g, value['customer_name']);
                        tpl = tpl.replace(/{phone}/g, value['phone']);
                        tpl = tpl.replace(/{content}/g, EditCampaign.xoa_dau(content));
                        $('.table-list-customer').append(tpl);
                    });
                    EditCampaign.sttAction();
                    $('#modalChooseFile').modal('hide');
                }
            });
        } else {
            $.getJSON(laroute.route('translate'), function (json) {
                $('.error-file-name-excels').text(json['Chưa có tệp']);
            });
        }
    },
    removeAllInput: function (thi) {
        $(thi).val('');
    },
    emptyListCustomer: function () {
        $('.customer_list').empty();
    }
};

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}

$("#time-send").timepicker({
    minuteStep: 15,
    defaultTime: $('#hidden-timeSent').val(),
    showMeridian: !1,
    snapToStep: !0,
});
$('#day-send').datepicker({
    format: "dd/mm/yyyy",
    startDate: '0d',
    language: 'vi',
}).datepicker("setDate", $('#hidden-daySent').val());
$('#is_now').click(function () {
    if ($('#is_now').is(':checked')) {
        $('#day-send').prop('disabled', true);
        $('#time-send').prop('disabled', true);
        $('.error-datetime').text('');
    } else {
        $('#day-send').prop('disabled', false);
        $('#time-send').prop('disabled', false);
    }
});
EditCampaign.countCharacter('#message-content');

$('#gender').select2();
$('#birthday').datepicker({
    format: 'dd/mm/yyyy',
    language: 'vi',
});
$('#check-all').click(function () {
    if ($('#check-all').is(":checked")) {
        $('.check').prop("checked", true);
    } else {
        $('.check').prop("checked", false);

    }
});
$.ajaxSetup({
    async: false,
});