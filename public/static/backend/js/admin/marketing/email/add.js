$(document).ready(function () {
    $.getJSON(laroute.route('translate'), function (json) {
        $("#time_sent").timepicker({
            minuteStep: 1,
            showMeridian: !1,
            snapToStep: !0,
        });
        $('#day_sent').datepicker({
            format: 'dd/mm/yyyy',
            language: 'vi',
            startDate: '0d'
        });
        $('#is_now').click(function () {
            if ($('#is_now').prop('checked')) {
                $('#day_sent').attr("disabled", true);
                $('#time_sent').attr("disabled", true);
                $(this).val(1);
            } else {
                $('#day_sent').attr("disabled", false);
                $('#time_sent').attr("disabled", false);
                $(this).val(0);
            }
        });
        $('#content').summernote({
            height:200,
            placeholder: json['Nhập nội dung'],
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ],

        });
    });
});
var add = {
    append_para: function (param) {
        var text = param;
        $('#content').summernote('pasteHTML', text);
    },
    submit_add: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-add');
            form.validate({
                rules: {
                    name: {
                        required: true
                    },
                    content: {
                        required: true
                    },
                },
                messages: {
                    name: {
                        required: json['Hãy nhập tên chiến dịch']
                    },
                    content: {
                        required: json['Hãy nhập nội dung tin nhắn']
                    },
                }
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('admin.email.submitAdd'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    name: $('#name').val(),
                    content_campaign: $('#content').summernote('code'),
                    day_sent: $('#day_sent').val(),
                    time_sent: $('#time_sent').val(),
                    is_now: $('#is_now').val(),
                }, success: function (res) {
                    if (res.error_slug == 1) {
                        $('.error_slug').text(json['Tên chiến dịch đã tồn tại']);
                    } else {
                        $('.error_slug').text('');
                    }
                    if (res.error_time == 1) {
                        $('.error_time').text(json['Thời gian gửi không hợp lệ']);
                    } else {
                        $('.error_time').text('');
                    }
                    if (res.success == 1) {
                        swal(json["Thêm chiến dịch thành công"], "", "success");
                        let routess = laroute.route('admin.email.edit');
                        window.location = routess.substring(0, 17) + '/' + res.id_add;
                    }
                }
            });
        });
    }
}
