$(document).ready(function () {
    $("#time_sent").timepicker({
        minuteStep: 1,
        showMeridian: !1,
        snapToStep: !0,
    });
    $('#day_sent').datepicker({
        format: 'dd/mm/yyyy',
        language: 'vi',
        startDate: '0d'
    });
    $('#is_now').click(function () {
        if ($('#is_now').prop('checked')) {
            $('#day_sent').attr("disabled", true);
            $('#time_sent').attr("disabled", true);
            $(this).val(1);
        } else {
            $('#day_sent').attr("disabled", false);
            $('#time_sent').attr("disabled", false);
            $(this).val(0);
        }
    });
    $('#birthday').datepicker({
        format: 'dd/mm/yyyy',
        language: 'vi',
    });
    $.getJSON(laroute.route('translate'), function (json) {
        $('#gender').select2({
        placeholder:json['Giới tính'],
        allowClear:true
        });
        $('.check_all').click(function () {
            if ($('.check_all').is(":checked")) {
                $('input[name="check"]').prop("checked", true);
            } else {
                $('input[name="check"]').prop("checked", false);
            }
        });
        $('.content').summernote({
            height:200,
            placeholder: json['Nhập nội dung'],
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });
        $('.note-btn').attr('title', '');
        $('#content').summernote('code',$('#content_hidden').val());
        $('.content').summernote('disable');
    });
});
var edit = {
    append_para: function (param) {
        var text = param;
        $('#content').summernote('pasteHTML', text);
    },
    submit_edit: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-edit');

            form.validate({
                rules: {
                    name_edit: {
                        required: true
                    },
                    content: {
                        required: true
                    },
                },
                messages: {
                    name_edit: {
                        required: json['Hãy nhập tên chiến dịch']
                    },
                    content: {
                        required: json['Hãy nhập nội dung tin nhắn']
                    },
                }
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('admin.email.submit-edit'),
                dataType: 'JSON',
                method: 'POST',
                data: {
                    campaign_id: $('#campaign_id').val(),
                    name: $('#name_edit').val(),
                    content_campaign: $('#content').summernote('code'),
                    is_now: $('#is_now').val(),
                    day_sent: $('#day_sent').val(),
                    time_sent: $('#time_sent').val(),
                }, success: function (res) {
                    if (res.error_slug == 1) {
                        $('.error_slug').text(json['Tên chiến dịch đã tồn tại']);
                    } else {
                        $('.error_slug').text('');
                    }
                    if (res.error_time == 1) {
                        $('.error_time').text(json['Thời gian gửi không hợp lệ']);
                    } else {
                        $('.error_time').text('');
                    }
                    if(res.error_content==1)
                    {
                        $('.error_content_html').text(json['Nội dung không hợp lệ']);
                    }else{
                        $('.error_content_html').text('');
                    }
                    if (res.success == 1) {
                        swal(json["Chỉnh sửa chiến dịch thành công"], "", "success");
                        if (res.is_now == 1) {
                            $('#day_sent').val(res.day_sent);
                            $('#time_sent').val(res.time_sent);
                        }
                    }
                }
            })
        });
    },
    modal_customer: function () {
        $('#add-customer').modal('show');
        $('#name').val('');
        $('#birthday').val('');
        $('#gender').val('');
        $('.customer_list_body').empty();
        $('.error_append').text('');
    },
    search: function () {
        $('.customer_list_body').empty();
        var data = $('#name').val();
        var birthday = $('#birthday').val();
        var gender = $('#gender').val();
        $.ajax({
            url: laroute.route('admin.email.search-customer'),
            dataType: 'JSON',
            method: 'POST',
            data: {
                data: data,
                birthday: birthday,
                gender: gender,
            },
            success: function (res) {
                $.map(res.arr_data, function (a) {
                    $.getJSON(laroute.route('translate'), function (json) {
                        var stts = $('.customer_list_body tr').length;
                        var tpl = $('#customer-list-tpl').html();
                        tpl = tpl.replace(/{stt}/g, stts + 1);
                        tpl = tpl.replace(/{name}/g, a.full_name);
                        tpl = tpl.replace(/{customer_id}/g, a.customer_id);
                        if(a.birthday!=undefined)
                        {
                            tpl = tpl.replace(/{birthday}/g, a.birthday);
                        }else{
                            tpl = tpl.replace(/{birthday}/g, '');
                        }

                        if (a.gender == 'male') {
                            tpl = tpl.replace(/{gender}/g, json['Nam']);
                        }
                        else if (a.gender == 'female') {
                            tpl = tpl.replace(/{gender}/g, json['Nữ']);
                        }
                        else {
                            tpl = tpl.replace(/{gender}/g, json['Khác']);
                        }
                        if (a.email != null) {
                            tpl = tpl.replace(/{email}/g, a.email);
                        } else {
                            tpl = tpl.replace(/{email}/g, '');
                        }
                        $('.customer_list_body').append(tpl);
                    });
                });
            }
        });
    },
    click_append: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var append = [];
            $.each($('.customer_list tr input[name="check"]:checked').parentsUntil("tbody"), function () {
                var $tds = $(this).find("input[name='customer_id']");
                $.each($tds, function () {
                    append.push($(this).val());
                });
            });
            if (append != '') {
                $.ajax({
                    url: laroute.route('admin.email.append-table'),
                    dataTye: 'JSON',
                    method: 'POST',
                    data: {
                        list: append,
                        campaign_id: $('#campaign_id').val()
                    },
                    success: function (res) {
                        $.map(res.data_list, function (a) {
                            var stts = $('.table_list_body tr').length;
                            var tpl = $('#list-send-tpl').html();
                            tpl = tpl.replace(/{stt}/g, stts + 1);
                            tpl = tpl.replace(/{name}/g, a.customer_name);
                            if (a.email != null) {
                                tpl = tpl.replace(/{email}/g, a.email);
                            } else {
                                tpl = tpl.replace(/{email}/g, '');
                            }
                            tpl = tpl.replace(/{content}/g, a.content);
                            $('.table_list_body').append(tpl);
                        });
                        $('#add-customer').modal('hide');
                    }
                });
            } else {
                $('.error_append').text(json['Vui lòng chọn khách hàng']);
            }
        });

    },
    seen_content_old:function(e){
        $('.content_cus').empty();
        var content=$(e).closest('.old').find('textarea[name="content"]').val();
        $('#content-customer').modal('show');
        $('.content_cus').append(content);
    },
    seen_content:function(e){
        $('.content_cus').empty();
        var content=$(e).closest('.send').find('textarea[name="content"]').val();
        $('#content-customer').modal('show');
        $('.content_cus').append(content);
    },
    remove_old: function (e) {
        $(e).closest('.old').remove();
    },
    remove: function (e) {
        $(e).closest('.send').remove();
    },
    save_log: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var check = [];
            $.each($('.table_list').find("tbody tr"), function () {
                var $tds = $(this).find("input");
                $.each($tds, function () {
                    check.push($(this).val());
                });
            });
            if (check == '') {
                $('.tb_log').text(json['Vui lòng thêm khách hàng']);
            } else {
                $('.tb_log').text('');
                var send = [];
                $.each($('.table_list').find(".send"), function () {
                    var $tds = $(this).find("input,textarea");
                    $.each($tds, function () {
                        send.push($(this).val());
                    });
                });

                var old = [];
                $.each($('.table_list').find(".old"), function () {
                    var $tds = $(this).find("input,textarea");
                    $.each($tds, function () {
                        old.push($(this).val());
                    });
                });
                $.ajax({
                    url: laroute.route('admin.email.save-log'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        list_send: send,
                        list_old: old,
                        campaign_id: $('#campaign_id').val(),
                    }, success: function (res) {
                        if (res.success == 1) {
                            swal(json["Lưu chiến dịch thành công"], "", "success");
                            window.location = laroute.route('admin.email');
                        }
                    }
                })
            }
        });
    },
    send_mail: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var check = [];
            $.each($('.table_list').find("tbody tr"), function () {
                var $tds = $(this).find("input");
                $.each($tds, function () {
                    check.push($(this).val());
                });
            });
            if (check == '') {
                $('.tb_log').text(json['Vui lòng thêm khách hàng']);
            } else {
                mApp.block("#m_blockui_1_content", {
                    overlayColor: "#000000",
                    type: "loader",
                    state: "success",
                    message: json["Đang tải..."]
                });
                $('.tb_log').text('');
                var send = [];
                $.each($('.table_list').find(".send"), function () {
                    var $tds = $(this).find("input,textarea");
                    $.each($tds, function () {
                        send.push($(this).val());
                    });
                });

                var old = [];
                $.each($('.table_list').find(".old"), function () {
                    var $tds = $(this).find("input,textarea");
                    $.each($tds, function () {
                        old.push($(this).val());
                    });
                });
                $.ajax({
                    url: laroute.route('admin.email.send-mail'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        list_send: send,
                        list_old: old,
                        campaign_id: $('#campaign_id').val(),
                    }, success: function (res) {
                        mApp.unblock("#m_blockui_1_content");
                        if (res.success == 1) {
                            swal("Gửi mail thành công", "", "success");
                            window.location = laroute.route('admin.email');
                        }
                    }
                })
            }
        });
        // var old = [];
        // $.each($('.table_list').find(".old"), function () {
        //     var $tds = $(this).find("input,textarea");
        //     $.each($tds, function () {
        //         old.push($(this).val());
        //     });
        // });
        // $.ajax({
        //     url: laroute.route('admin.email.send-mail'),
        //     dataType: 'JSON',
        //     method: 'POST',
        //     data: {
        //         list_old: old,
        //         campaign_id:$('#campaign_id').val()
        //     }, success: function (res) {
        //
        //     }
        // });
    },
    modal_file:function () {
        $('#modal-excel').modal('show');
        $('#show').val('');
        $('input[type=file]').val('');
    },
    import:function () {
        var file_data = $('#file_excel').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('campaign_id', $('#campaign_id').val());
        $.ajax({
            url: laroute.route("admin.email.import-excel"),
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                $.map(res.list_customer, function (a) {
                    var stts = $('.table_list_body tr').length;
                    var tpl = $('#list-send-tpl').html();
                    tpl = tpl.replace(/{stt}/g, stts + 1);
                    tpl = tpl.replace(/{name}/g, a.customer_name);
                    if (a.email != null) {
                        tpl = tpl.replace(/{email}/g, a.email);
                    } else {
                        tpl = tpl.replace(/{email}/g, '');
                    }
                    tpl = tpl.replace(/{content}/g, a.content);
                    $('.table_list_body').append(tpl);
                });
                $('#modal-excel').modal('hide');
            }
        });
    },
    showNameFile:function(){
        var fileNamess=$('input[type=file]').val();
        $('#show').val(fileNamess);
    },
}