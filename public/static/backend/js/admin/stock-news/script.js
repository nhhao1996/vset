$('#autotable').PioTable({
    baseUrl: laroute.route('admin.stock-news.list')
});
var stockNews = {
    search: function () {
        $(".btn-search").trigger("click");
    },

    addNews : function () {
        var form = $('#form');
        form.validate({
            rules: {
                stock_category_id: {
                    required: true,
                },
                stock_news_title_vi: {
                    required: true,
                    maxlength: 255
                },
                stock_news_title_en: {
                    required: true,
                    maxlength: 255
                }
            },
            messages: {
                stock_category_id: {
                    required: 'Yêu cầu chọn danh mục',
                },
                stock_news_title_vi: {
                    required: 'Yêu cầu nhập tên tin tức(VI)',
                    maxlength: 'Tên danh mục(VI) vượt quá 255 ký tự'
                },
                stock_news_title_en: {
                    required: 'Yêu cầu nhập tên tin tức(EN)',
                    maxlength: 'Tên danh mục(EN) vượt quá 255 ký tự'
                }
            }
        });

        if (!form.valid()) {
            return false;
        }

        let is_active=0;
        if ($('#is_active').is(":checked")) {
            is_active=1;
        }

        $.ajax({
            url: laroute.route('admin.stock-news.store'),
            data: $('#form').serialize()+'&is_active='+is_active,
            method: 'POST',
            dataType: "JSON",
            success: function (res) {
                if(res.error == false){
                    swal(res.message,'','success').then(function () {
                        window.location.href = laroute.route('admin.stock-news');
                    });
                } else {
                    swal(res.message,'','error');
                }
            }
        });
    },

    editNews : function () {
        var form = $('#form');
        form.validate({
            rules: {
                stock_category_id: {
                    required: true,
                },
                stock_news_title_vi: {
                    required: true,
                    maxlength: 255
                },
                stock_news_title_en: {
                    required: true,
                    maxlength: 255
                }
            },
            messages: {
                stock_category_id: {
                    required: 'Yêu cầu chọn danh mục',
                },
                stock_news_title_vi: {
                    required: 'Yêu cầu nhập tên tin tức(VI)',
                    maxlength: 'Tên danh mục(VI) vượt quá 255 ký tự'
                },
                stock_news_title_en: {
                    required: 'Yêu cầu nhập tên tin tức(EN)',
                    maxlength: 'Tên danh mục(EN) vượt quá 255 ký tự'
                }
            }
        });

        if (!form.valid()) {
            return false;
        }

        let is_active=0;
        if ($('#is_active').is(":checked")) {
            is_active=1;
        }

        $.ajax({
            url: laroute.route('admin.stock-news.update'),
            data: $('#form').serialize()+'&is_active='+is_active,
            method: 'POST',
            dataType: "JSON",
            success: function (res) {
                if(res.error == false){
                    swal(res.message,'','success').then(function () {
                        window.location.href = laroute.route('admin.stock-news');
                    });
                } else {
                    swal(res.message,'','error');
                }
            }
        });
    },

    remove: function (obj, id) {
        $(obj).closest('tr').addClass('m-table__row--danger');

        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.stock-news.remove'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        stock_news_id: id
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal(res.message,'','error');
                        } else {
                            swal(
                                'Xóa thành công',
                                '',
                                'success'
                            ).then(function () {
                                location.reload();
                            })
                        }
                    },
                });

            }
        });
    },
}

$(document).ready(function () {
    uploadImgCk = function (file,lang) {
        let out = new FormData();
        out.append('file', file, file.name);

        $.ajax({
            method: 'POST',
            url: laroute.route('admin.stock-news.upload'),
            contentType: false,
            cache: false,
            processData: false,
            data: out,
            success: function (img) {
                if ((file.type).indexOf('image') != -1){
                    $("#stock_news_content_"+lang).summernote('insertImage', img['file']);
                } else {
                    $("#stock_news_content_"+lang).summernote('pasteHTML', '<a href="'+img['file']+'">'+file.name+'</a>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus + " " + errorThrown);
            }
        });
    };
})

function uploadImageMain(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_main')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#stock_news_img').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_news.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("admin.stock-news.uploadImage"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#img_stock_news_img').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}