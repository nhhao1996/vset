var configTermInvestment = {
    showPopup: function (id = null) {
        $.ajax({
            url: laroute.route('admin.config-term-investment.showPopup'),
            method: "POST",
            dataType: "JSON",
            data: {
                id: id,
            },
            success: function (res) {
                if (res.error == false) {
                    $('.showPopup').empty();
                    $('.showPopup').append(res.view);
                    $('#show_popup').modal('show');
                    $('.select-fix').select2();
                    $('.year').hide();

                    $('#type_investment').change(function () {
                        if ($('#type_investment').val() == 'month'){
                            $('.year').hide();
                            $('.month').show();
                        } else {
                            $('.month').hide();
                            $('.year').show();
                        }
                        $('#form-investment').removeData('validator');
                    });

                    // if (res.investment_time_month != 0) {
                    //     if (res.investment_time_month > 11) {
                    //         $('#type_investment').val('year').trigger('change');
                    //     }
                    // }

                } else {
                    $('.showPopup').empty();
                }
            }
        });
    },

    addInvestment:function () {
        // $('#form-investment').resetForm();
        $('#form-investment').removeData('validator');
        // if ($('#type_investment').val() == 'month') {
        //     $('#form-investment').validate({
        //         rules: {
        //             product_category_id: {
        //                 required: true,
        //             },
        //             investment_time_month: {
        //                 required: true,
        //             },
        //         },
        //         messages: {
        //             product_category_id: {
        //                 required: 'Yêu cầu chọn loại gói'
        //             },
        //             investment_time_month: {
        //                 required: 'Yêu cầu chọn số tháng',
        //             },
        //         },
        //     });
        // } else {
        //     $('#form-investment').validate({
        //         rules: {
        //             product_category_id: {
        //                 required: true,
        //             },
        //             investment_time_year: {
        //                 required: true,
        //             },
        //         },
        //         messages: {
        //             product_category_id: {
        //                 required: 'Yêu cầu chọn loại gói'
        //             },
        //             investment_time_year: {
        //                 required: 'Yêu cầu chọn số năm',
        //             },
        //         },
        //     });
        // }

        $('#form-investment').validate({
            rules: {
                product_category_id: {
                    required: true,
                },
                investment_time_month: {
                    required: true,
                },
            },
            messages: {
                product_category_id: {
                    required: 'Yêu cầu chọn loại gói'
                },
                investment_time_month: {
                    required: 'Yêu cầu chọn số tháng',
                },
            },
        });

        if (!$('#form-investment').valid()) {
            return false;
        }

        $.ajax({
            url: laroute.route('admin.config-term-investment.store'),
            method: "POST",
            dataType: "JSON",
            data: $('#form-investment').serialize(),
            success: function (data) {
                if (data.error == true) {
                    swal('', data.message, "error");
                } else {
                    swal('', data.message, "success").then(function () {
                        location.reload();
                    });
                }
            },
            error: function (res) {
                if (res.responseJSON != undefined) {
                    var mess_error = '';
                    $.map(res.responseJSON.errors, function (a) {
                        mess_error = mess_error.concat(a + '<br/>');
                    });
                    swal.fire('', mess_error, "error");
                }
            }
        });
    },

    editInvestment:function () {
        // $('#form-investment').resetForm();
        $('#form-investment').removeData('validator');
        if ($('#type_investment').val() == 'month') {
            $('#form-investment').validate({
                rules: {
                    product_category_id: {
                        required: true,
                    },
                    investment_time_month: {
                        required: true,
                    },
                },
                messages: {
                    product_category_id: {
                        required: 'Yêu cầu chọn loại gói'
                    },
                    investment_time_month: {
                        required: 'Yêu cầu chọn số tháng',
                    },
                },
            });
        } else {
            $('#form-investment').validate({
                rules: {
                    product_category_id: {
                        required: true,
                    },
                    investment_time_year: {
                        required: true,
                    },
                },
                messages: {
                    product_category_id: {
                        required: 'Yêu cầu chọn loại gói'
                    },
                    investment_time_year: {
                        required: 'Yêu cầu chọn số năm',
                    },
                },
            });
        }

        if (!$('#form-investment').valid()) {
            return false;
        }

        $.ajax({
            url: laroute.route('admin.config-term-investment.update'),
            method: "POST",
            dataType: "JSON",
            data: $('#form-investment').serialize(),
            success: function (data) {
                if (data.error == true) {
                    swal('', data.message, "error");
                } else {
                    swal('', data.message, "success").then(function () {
                        location.reload();
                    });
                }
            },
            error: function (res) {
                if (res.responseJSON != undefined) {
                    var mess_error = '';
                    $.map(res.responseJSON.errors, function (a) {
                        mess_error = mess_error.concat(a + '<br/>');
                    });
                    swal.fire('', mess_error, "error");
                }
            }
        });
    },

    deleteTermInvestment : function (obj,id) {
        $(obj).closest('tr').addClass('m-table__row--danger');

        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.config-term-investment.delete'),
                    method: "POST",
                    dataType: "JSON",
                    data: {
                        id : id
                    },
                    success: function (data) {
                        if (data.error == true) {
                            swal('', data.message, "error");
                        } else {
                            swal('', data.message, "success").then(function () {
                                location.reload();
                            });
                        }
                    },
                });
            }
        });
    },

    changeStatus:function (product_category_id,id,status,month) {
        swal({
            title: 'Thông báo',
            text: "Bạn muốn thay đổi trạng thái?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Có',
            cancelButtonText: 'Không',
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.config-term-investment.changStatus'),
                    method: "POST",
                    dataType: "JSON",
                    data: {
                        id:id,
                        status : status,
                        product_category_id : product_category_id,
                        month:month
                    },
                    success: function (data) {
                        if (data.error == true) {
                            swal('', data.message, "error");
                            if (status == 0) {
                                $('#'+id).prop('checked',false);
                            } else {
                                $('#'+id).prop('checked',true);
                            }
                        } else {
                            swal('', data.message, "success").then(function () {
                                location.reload();
                            });
                        }
                    },
                    error: function (res) {
                        if (res.responseJSON != undefined) {
                            var mess_error = '';
                            $.map(res.responseJSON.errors, function (a) {
                                mess_error = mess_error.concat(a + '<br/>');
                            });
                            swal.fire('', mess_error, "error");
                        }
                    }
                });
            } else {
                if (status == 0) {
                    $('#'+id).prop('checked',false);
                } else {
                    $('#'+id).prop('checked',true);
                }
            }
        });

    }
}

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.config-term-investment.list')
});