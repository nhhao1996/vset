var service_category = {

    editPost:function() {
        $.getJSON(laroute.route('translate'), function (json) {
            var formEdit = $('#formEdit');
            formEdit.validate({
                rules: {
                    name_vi: {
                        required: true,
                        maxlength: 150
                    },
                    name_en: {
                        required: true,
                        maxlength: 150
                    }
                },
                messages: {
                    name_vi: {
                        required: json['Hãy nhập tên nhóm dịch vụ (VI)'],
                        maxlength: json['Tên nhóm dịch vụ (VI) vượt quá 150 ký tự'],
                    },
                    name_en: {
                        required: json['Hãy nhập tên nhóm dịch vụ (EN)'],
                        maxlength: json['Tên nhóm dịch vụ (EN) vượt quá 150 ký tự'],
                    }

                },
            });

            if (!formEdit.valid()) {
                return false;
            }

            var id = $('#hhidden').val();
            let isActive=0;
            if ($('#h_is_actived').is(":checked")) {
                isActive=1;
            }
            $.ajax({
                type: 'POST',
                url: laroute.route('admin.service_category.submitEdit'),
                data: {
                    id: id,
                    name_vi: $('#h_name_vi').val(),
                    name_en: $('#h_name_en').val(),
                    description_vi: $('#h_description_vi').val(),
                    description_en: $('#h_description_en').val(),
                    is_actived: isActive
                },
                dataType: "JSON",
                success: function (response) {
                    if (response.status == 1) {
                        $('.error-name').text('');
                        $("#editForm").modal("hide");
                        swal(json["Cập nhật nhóm dịch vụ thành công"], "", "success");
                        $('#autotable').PioTable('refresh');
                    } else {
                        swal(json["Nhóm dịch vụ đã tồn tại"], "", "error");
                    }
                }
            })
        });
    },
    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');
        $.getJSON(laroute.route('translate'), function (json) {
            swal({
                title: json['Thông báo'],
                text: json["Bạn có muốn xóa không?"],
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: json['Xóa'],
                cancelButtonText: json['Hủy'],
                onClose: function () {
                    // remove hightlight row
                    // $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    $.post(laroute.route('admin.service_category.remove', {id: id}), function (res) {
                        if (res.error == 0) {
                            $(obj).closest('tr').removeClass('m-table__row--danger');
                            swal(
                                res.message,
                                '',
                                'success'
                            );
                            // window.location.reload();
                            $('#autotable').PioTable('refresh');
                        } else {
                            $(obj).closest('tr').removeClass('m-table__row--danger');
                            $('button').blur();
                            swal(
                                res.message,
                                '',
                                'error'
                            );
                        }
                    });
                } else {
                    $(obj).closest('tr').removeClass('m-table__row--danger');
                    $('button').blur();
                }
            });
        });
    },
    changeStatus: function (obj, id, action) {
        $.ajax({
            url: laroute.route('admin.service_category.change-status'),
            method: "POST",
            data: {
                id: id, action: action
            },
            dataType: "JSON"
        }).done(function (data) {
            $('#autotable').PioTable('refresh');
        });
    },
    edit: function (id) {

        $.ajax({
            type: 'POST',
            url: laroute.route('admin.service_category.edit'),
            data: {
                id: id
            },
            dataType: 'JSON',
            success: function (response) {
                $('#add-form').empty();
                $('#add-form').append(response);
                $('#editForm').modal("show");
                // $('#hhidden').val(response["service_category_id"]);
                // $('#h_name_vi').val(response["name_vi"]);
                // $('#h_name_en').val(response["name_en"]);
                // $('#h_description_vi').val(response["description_vi"]);
                // $('#h_description_en').val(response["description_en"]);
                // $('.error-name').text('');
                // if (response["is_actived"]==1){
                //     $('#h_is_actived').prop("checked", true);
                // }else {
                //     $('#h_is_actived').prop("checked", false);
                // }
            }


        });
    },
    addForm: function () {

        $.ajax({
            type: 'POST',
            url: laroute.route('admin.service_category.add-form'),
            data: {
            },
            dataType: 'JSON',
            success: function (response) {
                $('#add-form').empty();
                $('#add-form').append(response);
                $('#addForm').modal("show");
            }
        });
    },
    add: function (close) {
        $('#type_add').val(close);
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form');
            form.validate({
                rules: {
                    name_vi: {
                        required: true,
                        maxlength: 150
                    },
                    name_en: {
                        required: true,
                        maxlength: 150
                    }
                },
                messages: {
                    name_vi: {
                        required: json['Hãy nhập tên nhóm dịch vụ (VI)'],
                        maxlength: json['Tên nhóm dịch vụ (VI) vượt quá 150 ký tự'],
                    },
                    name_en: {
                        required: json['Hãy nhập tên nhóm dịch vụ (EN)'],
                        maxlength: json['Tên nhóm dịch vụ (EN) vượt quá 150 ký tự'],
                    }
                }
            });

            if (!form.valid()) {
                return false;
            }

            let isActive=0;
            if ($('#h_is_actived').is(":checked")) {
                isActive=1;
            }

            var input = $('#type_add').val();
            var group_name = $('#group_name').val();
            var name_vi = $('#name_vi');
            var name_en = $('#name_en');
            var des_vi = $('#description_vi');
            var des_en = $('#description_en');
            var is_actived = isActive;
            var input = $('#type_add');
            $.ajax({
                url: laroute.route('admin.service_category.submitAdd'),
                    data: {
                        name_vi: name_vi.val(),
                        name_en: name_en.val(),
                        description_vi: des_vi.val(),
                        description_en: des_en.val(),
                        is_actived: is_actived,
                        close: input.val()
                    },
                    method: 'POST',
                    dataType: "JSON",
                success: function (response) {
                    if (response.status == 1) {
                        if (response.close != 0) {
                            $("#add").modal("hide");
                        }
                        $('#form')[0].reset();
                        $('.error-name').text('');
                        swal(json["Thêm nhóm dịch vụ thành công"], "", "success").then(function () {
                            location.reload();
                        });
                    } else {
                        swal(json['Nhóm dịch vụ đã tồn tại'], "", "error");

                    }
                }
            });
        });

    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('');
        $('.m_selectpicker').selectpicker('refresh');
        $(".btn-search").trigger("click");
    }


};
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.service_category.list')
});
