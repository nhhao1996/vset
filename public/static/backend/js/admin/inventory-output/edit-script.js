$('.m_selectpicker').select2();
$('.rdo').click(function () {
    $('.rdo').attr('class', 'btn btn-default rdo');
    $(this).attr('class', 'btn ss--button-cms-piospa active rdo');
});

$('.errs').css('color', 'red');
var check = true;
// $('#warehouse').select2();
$('#created-at').datepicker({dateFormat: "yy-mm-dd"});
quantityProduct();

function changeOutputQuantity(o) {
    let values = parseInt($(o).val());
    if (values > 0) {
        $(o).val(values);

    } else {
        $(o).val(0);
    }
    $('.errs').css('color', 'red');
    let productInventory = $(o).parents('tr').find('.product-inventory').val();
    if (values > parseInt(productInventory)) {
        $(o).parents('td').find('.error-output-quantity').text('Vượt quá số lượng');
        check = false;
    } else {
        $(o).parents('td').find('.error-output-quantity').empty();
        check = true;
    }
    console.log(values);
    quantityProduct();
    InventoryOutput.totalMoneyEverProduct(o);
    InventoryOutput.totalMoneyAllProduct();

}

function deleteProductInList(o) {
    $(o).closest('tr').remove();
    let table = $('#table-product > tbody tr').length;
    let a = 1;
    $.each($('.stt'), function () {
        $(this).text(a++);
    });

    InventoryOutput.totalMoneyEverProduct(o);
    InventoryOutput.totalMoneyAllProduct();
    quantityProduct();
}

function quantityProduct() {
    var sum = 0;
    $.each($('.outputQuantity'), function () {
        sum += parseInt($(this).val());
    });
    $('#total-product').val(sum);
    $('#total-product-text').text(sum);
}

$('#list-product').select2({
}).on("select2:select", function (e) {
    // $(this).empty();
    let id = e.params.data.id;
    let stt = 1 + $('#table-product > tbody > tr').length;

    let flag = true;

    if (id != '') {
        $.ajax({
            url: laroute.route('admin.inventory-output.get-product-child-by-id'),
            method: "POST",
            dataType: "JSON",
            data: {
                id: id,
                warehouse: $('#warehouse').val()
            },
            success: function (data) {
                $.each($('#table-product tbody tr'), function () {
                    let codeHidden = $(this).find("td input[name='hiddencode[]']");
                    let codeExists = codeHidden.val();
                    var code = data['product']['product_code'];
                    if (codeExists == code) {
                        flag = false;
                        let valueNumberProduct = codeHidden.parents('tr').find('.outputQuantity').val();
                        let numbers = parseInt(valueNumberProduct) + 1;
                        codeHidden.parents('tr').find('.outputQuantity').val(numbers);
                        let productInventory = codeHidden.parents('tr').find('.product-inventory');

                        let cost = codeHidden.parents('tr').find('input[name="cost-product-child"]').val();
                        let totalMoney = cost.replace(new RegExp('\\,', 'g'), '') * parseInt(numbers);
                        codeHidden.parents('tr').find('.total-money-product').text(Number(totalMoney).toFixed(decimal_number))

                        if (parseInt(codeHidden.parents('tr').find('.outputQuantity').val()) > parseInt(productInventory.val())) {
                            $('.errs').css('color', 'red');
                            productInventory.parents('tr').find('.error-output-quantity').text('Vượt quá số lượng');
                            check = false;
                        } else {
                            productInventory.parents('td').find('.error-output-quantity').text('');
                            check = true;
                        }
                        quantityProduct();
                        InventoryOutput.totalMoneyAllProduct();

                    }
                });
                if (flag == true) {
                    var option = "";
                    option += ('<option value="' + data['unitExists']['unit_id'] + '">' +
                        '' + data['unitExists']['name'] + '</option>');
                    $.each(data['unit'], function (index, element) {
                        option += ('<option value="' + index + '">' + element + '</option>');
                    });

                    let $_tpl = $('#product-childs').html();
                    let tpl = $_tpl;
                    tpl = tpl.replace(/{stt}/g, stt);
                    tpl = tpl.replace(/{name}/g, data['product']['product_child_name']);
                    tpl = tpl.replace(/{code}/g, data['product']['product_code']);
                    tpl = tpl.replace(/{productInventory}/g, data['productInventory']);
                    tpl = tpl.replace(/{outputQuantity}/g, 0);
                    tpl = tpl.replace(/{cost}/g, formatNumber2(data['product']['cost']));
                    tpl = tpl.replace(/{price}/g, formatNumber2(data['product']['price']));
                    // tpl = tpl.replace(/{totalMoney}/g, formatNumber2(data['product']['cost']));
                    tpl = tpl.replace(/{totalMoney}/g, 0);
                    tpl = tpl.replace(/{option}/g, option);
                    $('#table-product > tbody').append(tpl);

                    quantityProduct();
                    $('.unit').select2();
                    InventoryOutput.totalMoneyAllProduct();
                }
            }
        });
    }
});
$('#product-code').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});
$('#product-code').bind("enterKey", function (e) {
    let o = $(this);
    let flag = true;
    let codeInput = $(this).val().trim();
    $.each($('#table-product tbody tr'), function () {
        let codeHidden = $(this).find("td input[name='hiddencode[]']");
        var code = codeHidden.val();
        if (codeInput == code) {
            flag = false;
            let valueNumberProduct = codeHidden.parents('tr').find('.outputQuantity').val();
            let numbers = parseInt(valueNumberProduct) + 1;
            let cost = codeHidden.parents('tr').find('input[name="cost-product-child"]').val();
            codeHidden.parents('tr').find('.outputQuantity').val(numbers);
            let productInventory = codeHidden.parents('tr').find('.product-inventory');

            if (parseInt(codeHidden.parents('tr').find('.outputQuantity').val()) > parseInt(productInventory.val())) {
                $('.errs').css('color', 'red');
                productInventory.parents('tr').find('.error-output-quantity').text('Vượt quá số lượng');
                check = false;
            } else {
                productInventory.parents('td').find('.error-output-quantity').text('');
                check = true;
            }
            let totalMoney = (cost.replace(new RegExp('\\,', 'g'), '')) * parseInt(numbers);
            codeHidden.parents('tr').find('.total-money-product').text(formatNumber2(totalMoney))
            quantityProduct();
            o.val('');
            o.focus();
            $('.error-code-product').text('');
            InventoryOutput.totalMoneyAllProduct();
            InventoryOutput.totalProduct();
        }
    });
    if (flag == true) {
        let stt = 1 + $('#table-product > tbody > tr').length;
        let sum = 0;
        $.ajax({
            url: laroute.route('admin.inventory-output.get-product-child-by-code'),
            method: "POST",
            dataType: "JSON",
            data: {
                code: codeInput,
                warehouse: $('#warehouse').val()
            },
            success: function (data) {
                console.log(data)
                if (data == "") {
                    $('.error-code-product').css('color', 'red');
                    $('.error-code-product').text('Mã sản phẩm không hợp lệ');
                } else {
                    var option = "";
                    option += ('<option value="' + data['unitExists']['unit_id'] + '">' +
                        '' + data['unitExists']['name'] + '</option>');
                    $.each(data['unit'], function (index, element) {
                        option += ('<option value="' + index + '">' + element + '</option>');
                    });

                    let $_tpl = $('#product-childs').html();
                    let tpl = $_tpl;
                    tpl = tpl.replace(/{stt}/g, stt);
                    tpl = tpl.replace(/{name}/g, data['product']['product_child_name']);
                    tpl = tpl.replace(/{code}/g, data['product']['product_code']);
                    tpl = tpl.replace(/{number}/g, 0);
                    tpl = tpl.replace(/{outputQuantity}/g, 0);
                    tpl = tpl.replace(/{cost}/g, formatNumber2(data['product']['cost'])).replace(new RegExp('\\,', 'g'), ',');
                    tpl = tpl.replace(/{price}/g, formatNumber2(data['product']['price'])).replace(new RegExp('\\,', 'g'), ',');
                    tpl = tpl.replace(/{total}/g, formatNumber2(data['product']['cost'])).replace(new RegExp('\\,', 'g'), ',');
                    tpl = tpl.replace(/{totalMoney}/g, 0);
                    tpl = tpl.replace(/{productInventory}/g, data['product_inventory']);
                    tpl = tpl.replace(/{option}/g, option);

                    $('#table-product > tbody').append(tpl);

                    o.val('');
                    o.focus();
                    $('.error-code-product').text('');
                    $('.unit').select2();
                    InventoryOutput.totalMoneyAllProduct();
                    InventoryOutput.totalProduct();
                }
            }
        });
    }

});

function checkInput() {
    let flag = true;
    let errWarehouse = $('.error-warehouse');
    if ($('#warehouse').val() == "") {
        errWarehouse.text('Vui lòng chọn nhà kho');
        flag = false;
        $('#list-product').attr('disabled', true);
        $('#product-code').attr('disabled', true);
    } else {
        errWarehouse.text('');
        $('#list-product').attr('disabled', false);
        $('#product-code').attr('disabled', false);
    }
    return flag;
}

$('#warehouse').change(function () {
    $('#total-product-text').text('0');
    $('.total-money').text('0');
    $('tbody').empty();
    $('#list-product').empty();
    $('#total-product').val(0);
    $('#list-product').append('<option value="">Chọn sản phẩm</option>');
    if ($('#warehouse').val() != '') {
        $.ajax({
            url: laroute.route('admin.inventory-output.get-product-child-by-warehouse'),
            method: "POST",
            data: {
                warehouse_id: $('#warehouse').val()
            },
            success: function (data) {
                if (data != '') {
                    $('#list-product').empty();
                    $('#list-product').append('<option value="">Chọn sản phẩm</option>');
                    $.each(data, function (key, value) {
                        $('#list-product').append('<option value="' + key + '">' + value + '</option>');
                    })
                }

            }
        })
    }
});
$('.btn-save').click(function () {
    if (checkInput() == true && check == true) {
        let stt = $('#table-product > tbody > tr').length;
        var statusss = $('.active').find('input[name="options"]').val();
        if (stt < 1) {
            $('.error-product').text('Vui lòng thêm sản phẩm');
        } else {
            var arrayProducts = [];
            $.each($('#table-product tbody tr'), function () {
                var code = $(this).find("td input[name='hiddencode[]']").val();
                var unit = $(this).find("td .unit").val();
                var outputQuantity = $(this).find("td input.outputQuantity").val();
                arrayProducts.push(code, unit, outputQuantity);
            });
            $.ajax({
                url: laroute.route('admin.inventory-output.submit-edit'),
                method: "POST",
                data: {
                    id: $('#idHidden').val(),
                    warehouse_id: $('#warehouse').val(),
                    po_code: $('#code-inventory').val(),
                    status: statusss,
                    note: $('#note').val(),
                    arrayProducts: arrayProducts,
                    created_at: $('#created-at').val(),
                    type: $('#type').val()
                },
                success: function () {
                    swal("Cập nhật phiếu xuất thành công", "", "success");
                    location.reload();
                }
            })
        }
    }
});
$('#btn-save-draft').click(function () {
    if (checkInput() == true && check == true) {
        let stt = $('#table-product > tbody > tr').length;
        if (stt < 1) {
            $('.error-product').text('Vui lòng thêm sản phẩm');
        } else {
            var arrayProducts = [];
            $.each($('#table-product tbody tr'), function () {
                var code = $(this).find("td input[name='hiddencode[]']").val();
                var unit = $(this).find("td .unit").val();
                var outputQuantity = $(this).find("td input.outputQuantity").val();
                arrayProducts.push(code, unit, outputQuantity);
            });
            $.ajax({
                url: laroute.route('admin.inventory-output.submit-edit'),
                method: "POST",
                data: {
                    id: $('#idHidden').val(),
                    warehouse_id: $('#warehouse').val(),
                    po_code: $('#code-inventory').val(),
                    status: 'draft',
                    note: $('#note').val(),
                    arrayProducts: arrayProducts,
                    created_at: $('#created-at').val(),
                    type: $('#type').val()
                },
                success: function () {
                    swal("Cập nhật phiếu xuất thành công", "", "success");
                    location.reload();
                }
            })
        }
    }
});

function onKeyDownInput(o) {
    $(o).on('keydown', function (e) {
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110])
        || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true))
        && (!0 === e.ctrlKey || !0 === e.metaKey)
        || 35 <= e.keyCode && 40 >= e.keyCode
        || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode)
        && e.preventDefault()
    });
}

var InventoryOutput = {
    cong: function (o) {
        var inputNumberProduct = $(o).parents('td').find('.number-product');
        $(o).parents('td').find('.number-product').val(parseInt(inputNumberProduct.val()) + 1);
        InventoryOutput.totalProduct();
        InventoryOutput.totalMoneyEverProduct(inputNumberProduct);
        InventoryOutput.totalMoneyAllProduct();
        changeOutputQuantity(inputNumberProduct);
    },
    tru: function (o) {
        var inputNumberProduct = $(o).parents('td').find('.number-product');
        if (inputNumberProduct.val() > 0) {
            $(o).parents('td').find('.number-product').val(parseInt(inputNumberProduct.val()) - 1);
            if (inputNumberProduct.val() == 0) {
                inputNumberProduct.val(1);
            }
            InventoryOutput.totalProduct();
            InventoryOutput.totalMoneyEverProduct(inputNumberProduct);
            InventoryOutput.totalMoneyAllProduct();
            changeOutputQuantity(inputNumberProduct);
        }
        if (inputNumberProduct.val()==1){
            inputNumberProduct.val(0);
            InventoryOutput.totalProduct();
            InventoryOutput.totalMoneyEverProduct(inputNumberProduct);
            InventoryOutput.totalMoneyAllProduct();
            changeOutputQuantity(inputNumberProduct);
        }
    },
    totalProduct: function () {
        let totalQuantity = 0;
        $.each($('.outputQuantity'), function () {
            let quantity = $(this).val();
            totalQuantity += parseInt(quantity);
        });
        $('#total-product-text').text(totalQuantity);
    },
    totalMoneyEverProduct: function (o) {
        let cost = $(o).parents('tr').find("input[name='cost-product-child']").val().replace(new RegExp('\\,', 'g'), '');
        let sl = $(o).parents('tr').find(".number-product").val().replace(new RegExp('\\,', 'g'), '');
        $(o).parents('tr').find(".total-money-product").text(Number(cost * sl).toFixed(decimal_number));
    },
    totalMoneyAllProduct: function () {
        let total = 0;
        let arrSum = [];
        $.each($('.total-money-product'), function () {
            let valAttr = $(this).text().replace(new RegExp('\\,', 'g'), '');
            total += Number(valAttr);
        });
        $('.total-money').text(Number(total).toFixed(decimal_number));
    },
    formatQuantity: function (num) {
        // var txt = "#div-name-1234-characteristic:561613213213";
        var numb = num.match(/\d/g);
        numb = numb.join("");
        return numb;
    },

};

function totalMoneyEverProduct(o) {
    let cost = $(o).parents('tr').find("input[name='cost-product-child']").val().replace(new RegExp('\\,', 'g'), '');
    let sl = $(o).parents('tr').find(".number-product").val().replace(new RegExp('\\,', 'g'), '');
    $(o).parents('tr').find(".total-money-product").text(Number(cost * sl).toFixed(decimal_number));

}

function formatNumber2(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

InventoryOutput.totalProduct();
InventoryOutput.totalMoneyAllProduct();

// $('.outputQuantity').maskNumber({integer: true});

$('.unit').select2();

// Numeric only control handler
jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };

$('.outputQuantity').ForceNumericOnly();



