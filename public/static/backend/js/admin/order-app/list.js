let stt_tr = $('#table_add tbody tr').length;
$(document).ready(function () {
    $.getJSON(laroute.route('translate'), function (json) {
        $('#search').keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
            }
        });
        $('#search').bind("enterKey", function () {
            var id = $('#customer_id').val();
            var type = $('.type').find('.active').attr('data-name');
            // var type_load = $('.type').find('.active').attr('data-name');

            var search = $('#search').val();

            mApp.block("#m_blockui_1_content", {
                overlayColor: "#000000",
                type: "loader",
                state: "success",
                message: json["Đang tải..."]
            });
            $.ajax({
                url: laroute.route('admin.order.search'),
                data: {
                    type: type,
                    search: search,
                    id: id,
                    customer_id: $('#customer_id').val()
                },
                async: false,
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.type != 'member_card') {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            if (a.is_sale == 0) {
                                var tpl = $('#list-tpl').html();
                                tpl = tpl.replace(/{name}/g, a.name);
                                tpl = tpl.replace(/{price}/g, a.price);
                                tpl = tpl.replace(/{id}/g, a.id);
                                tpl = tpl.replace(/{code}/g, a.code);
                                if (a.avatar !== null && a.avatar !== '') {
                                    tpl = tpl.replace(/{img}/g, a.avatar);
                                } else {
                                    tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                                }
                                tpl = tpl.replace(/{price_hidden}/g, a.price);
                                tpl = tpl.replace(/{type}/g, a.type);
                                $('.append').append(tpl);
                            } else {
                                var tpl = $('#list-promotion-tpl').html();
                                tpl = tpl.replace(/{name}/g, a.name);
                                tpl = tpl.replace(/{price}/g, a.price);
                                tpl = tpl.replace(/{id}/g, a.id);
                                tpl = tpl.replace(/{code}/g, a.code);
                                if (a.avatar !== null && a.avatar !== '') {
                                    tpl = tpl.replace(/{img}/g, a.avatar);
                                } else {
                                    tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                                }
                                tpl = tpl.replace(/{price_hidden}/g, a.promotion_price);
                                tpl = tpl.replace(/{type}/g, a.type);
                                $('.append').append(tpl);
                            }
                        });
                    } else {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            var tpl = $('#list-card-tpl').html();
                            tpl = tpl.replace(/{card_name}/g, a.card_name);
                            tpl = tpl.replace(/{card_code}/g, a.card_code);
                            tpl = tpl.replace(/{id_card}/g, a.customer_service_card_id);
                            if (a.image != null) {
                                tpl = tpl.replace(/{img}/g, a.image);
                            } else {
                                tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                            }
                            if (a.count_using != 0) {
                                if (a.count_using == json['Không giới hạn']) {
                                    tpl = tpl.replace(/{quantity}/g, 'KGH');
                                } else {
                                    tpl = tpl.replace(/{quantity}/g, json['Còn '] + a.count_using);
                                }

                                tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                            } else {
                                tpl = tpl.replace(/{quantity}/g, json['Không giới hạn']);
                                tpl = tpl.replace(/{quantity_app}/g, json['Không giới hạn']);
                            }
                            $('.append').append(tpl);
                            $.each($('#table_add tbody tr'), function () {
                                var codeHidden = $(this).find("input[name='object_code']");
                                var value_code = codeHidden.val();
                                var code = a.card_code;
                                if (value_code == code) {
                                    var quantity = $(this).find("input[name='quantity']").val();
                                    var quantity_card = $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val();
                                    if (quantity_card != 'Không giới hạn') {
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val(quantity_card - quantity);
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').empty();
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val() + json[' (lần)']);
                                    }
                                }
                            });
                        });
                    }

                }
            });
        });

        order.loadDefault();

        $(".quantity").TouchSpin({
            initval: 1,
            min: 1,
            buttondown_class: "btn btn-default down btn-ct",
            buttonup_class: "btn btn-default up btn-ct"
        });
        $(".quantity_card").TouchSpin({
            initval: 1,
            min: 1,
            buttondown_class: "btn btn-default down btn-ct",
            buttonup_class: "btn btn-default up btn-ct"
        });
        $('.quantity').change(function () {
            $(this).closest('.tr_table').find('.amount-tr').empty();
            var id = $(this).closest('.tr_table').find('input[name="id"]').val();
            var type = $(this).closest('.tr_table').find('input[name="object_type"]').val();
            var stt = $(this).attr('data-id');

            var id_type = "";
            if (type === "service") {
                id_type = 1;
            } else if (type === "service_card") {
                id_type = 2;
            } else {
                id_type = 3;
            }
            var price = $(this).closest('.tr_table').find('input[name="price"]').val();
            var discount = $(this).closest('.tr_table').find('input[name="discount"]').val();
            var loc = discount.replace(new RegExp('\\,', 'g'), '');
            var quantity = $(this).val();
            var amount = ((price * quantity) - loc);
            $(this).closest('.tr_table').find('.amount-tr').append(formatNumber(amount.toFixed(decimal_number)) + json['đ']);
            $(this).closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount.toFixed(decimal_number) + '">');
            $('.append_bill').empty();
            var sum = 0;
            $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                sum += Number($(this).val());
            });
            $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
            $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
            $('.tag_a').remove();
            //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
            $('.amount_bill').append();
            $('.amount_bill').empty();
            var discount_bill = $('#discount_bill').val();
            $('.close').remove();
            if (discount_bill != 0) {
                $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
            } else {
                $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            }
            var amount_bill = (sum - discount_bill);
            if (amount_bill < 0) {
                amount_bill = 0;
            }
            $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
            $(this).closest('.tr_table').find('.abc').remove();
            if (discount != 0) {
                $(this).closest('.tr_table').find('.discount-tr-' + type + '-' + stt + '').prepend('<a class="abc" href="javascript:void(0)" onclick="list.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
            } else {
                $(this).closest('.tr_table').find('.discount-tr-' + type + '-' + stt + '').prepend('<a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus" href="javascript:void(0)" onclick="list.modal_discount(' + amount.toFixed(decimal_number) + ',' + id + ',' + id_type + ',' + stt + ')"><i class="la la-plus icon-sz"></i></a>');
            }
            discountCustomerInput();
            order.getPromotionGift();
        });
        $('.quantity_card').change(function () {
            $.getJSON(laroute.route('translate'), function (json) {
                var id = $(this).closest('.tr_table').find("input[name='id']").val();
                var max_quantity = $(this).closest('.tr_table').find("input[name='quantity_hidden']").val();
                var quan_this = $(this).val();
                var quan_hide = $('.card_check_' + id + '').find('.quantity_hide').val();
                if (quan_hide != 'Không giới hạn') {
                    $('.card_check_' + id + '').find('.quantity_card').val(quan_hide - quan_this);
                    $('.card_check_' + id + '').find('.quantity').empty();
                    $('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + id + '').find('.quantity_card').val() + json[' (lần)']);
                }
                if (max_quantity > 0) {
                    $(this).trigger("touchspin.updatesettings", {
                        max: max_quantity
                    });
                }
            });
            discountCustomerInput();
        });
        $('.remove').click(function () {
            var totalAfterDiscountMember = $('.amount_bill_input').val() - $('#member_level_discount').val();

            $(this).closest('.tr_table').remove();
            $('.append_bill').empty();
            var sum = 0;
            $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                sum += Number($(this).val());
            });
            $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
            $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
            $('.discount_bill').empty();
            $('.discount_bill').append(0 + json['đ']);
            $('.discount_bill').append('<input type="hidden" id="discount_bill" name="discount_bill" value="' + 0 + '">');
            $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            $('.amount_bill').empty();
            // var discount_bill = $('#discount_bill').val();
            // var amount_bill = (sum.toFixed(decimal_number));

            $('.amount_bill').append(formatNumber(totalAfterDiscountMember.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + totalAfterDiscountMember.toFixed(decimal_number) + '">');
            discountCustomerInput();
            order.getPromotionGift();
        });
        $('.remove_card').click(function () {
            $.getJSON(laroute.route('translate'), function (json) {
                var id = $(this).closest('.tr_table').find("input[name='id']").val();
                var quan_hide = $('.card_check_' + id + '').find('.quantity_hide').val();
                $('.card_check_' + id + '').find('.quantity_card').val(quan_hide);
                $('.card_check_' + id + '').find('.quantity').empty();
                $('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + id + '').find('.quantity_card').val() + json['(lần)']);
                $(this).closest('.tr_table').remove();
            });
            discountCustomerInput();
        });
        $('#receipt_type').select2({
            placeholder: json['Chọn hình thức thanh toán']
        }).on('select2:select', function (event) {
            if (event.params.data.id == 'cash') {
                $('.cash').empty();
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tiền mặt']);
                tpl = tpl.replace(/{money}/g, '*');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_cash');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_cash');
                $('.cash').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_cash', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
            if (event.params.data.id == 'transfer') {
                $('.transfer').empty();
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tiền chuyển khoản']);
                tpl = tpl.replace(/{money}/g, '*');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_atm');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_atm');
                $('.transfer').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_atm', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
            if (event.params.data.id == 'visa') {
                $('.visa').empty()
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tiền chuyển Visa']);
                tpl = tpl.replace(/{money}/g, '*');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_visa');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_visa');
                $('.visa').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_visa', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
            if (event.params.data.id == 'member_money') {
                var money = $('#member_money').val();
                $('.member_money').empty();
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tài khoản thành viên']);
                tpl = tpl.replace(/{money}/g, json['(Còn '] + formatNumber(money) + ')');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_money');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_money');
                $('.member_money').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_money', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
        }).on('select2:unselect', function (event) {
            var amount_this = 0;

            if (event.params.data.id == 'cash') {
                amount_this = $('#amount_receipt_cash').val().replace(new RegExp('\\,', 'g'), '');
                $('.cash').empty();
            }
            if (event.params.data.id == 'transfer') {
                amount_this = $('#amount_receipt_atm').val().replace(new RegExp('\\,', 'g'), '');
                $('.transfer').empty();
            }
            if (event.params.data.id == 'visa') {
                amount_this = $('#amount_receipt_visa').val().replace(new RegExp('\\,', 'g'), '');
                $('.visa').empty();
            }
            if (event.params.data.id == 'member_money') {
                amount_this = $('#amount_receipt_money').val().replace(new RegExp('\\,', 'g'), '');
                $('.member_money').empty();
            }
            var amount_rest = $('#amount_rest').val().replace(new RegExp('\\,', 'g'), '');
            var amount_return = $('#amount_return').val().replace(new RegExp('\\,', 'g'), '');
            var all = $('#amount_all').val().replace(new RegExp('\\,', 'g'), '');

            $('#amount_all').val(formatNumber((all - amount_this).toFixed(decimal_number)));
            $('.cl_amount_all').text(formatNumber((all - amount_this).toFixed(decimal_number)));

            if (((amount_rest) + (amount_this) - (amount_return)) > 0) {
                $('#amount_rest').val(formatNumber(((amount_rest) + (amount_this) - (amount_return)).toFixed(decimal_number)));
                $('.cl_amount_rest').text(formatNumber(((amount_rest) + (amount_this) - (amount_return)).toFixed(decimal_number)));
            } else {
                $('#amount_rest').val(0);
                $('.cl_amount_rest').text(0);
            }

            if (amount_return - amount_this > 0) {
                $('#amount_return').val(formatNumber((amount_return - amount_this)));
                $('.cl_amount_return').text(formatNumber((amount_return - amount_this)));
            } else {
                $('#amount_return').val(0);
                $('.cl_amount_return').text(0);
            }
        });

        $('#btn_order').click(function () {
            $('#receipt_amount').val(formatNumber($('input[name="amount_bill_input"]').val()));
            $('#amount_all').val(formatNumber($('input[name="amount_bill_input"]').val()));
            $('#amount_rest').val(0);
            $('#amount_return').val(0);
            //span
            $('.cl_receipt_amount').text(formatNumber($('input[name="amount_bill_input"]').val()));
            $('.cl_amount_all').text(formatNumber($('input[name="amount_bill_input"]').val()));
            $('.cl_amount_rest').text(0);
            $('.cl_amount_return').text(0);

            //Load sẵn hình thức thanh toán = tiền mặt
            $('#receipt_type').val('cash').trigger('change');
            $('.cash').empty();
            var tpl = $('#type-receipt-tpl').html();
            tpl = tpl.replace(/{label}/g, json['Tiền mặt']);
            tpl = tpl.replace(/{money}/g, '*');
            tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_cash');
            tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_cash');
            $('.cash').append(tpl);

            $('#amount_receipt_cash').val(formatNumber($('input[name="amount_bill_input"]').val()));

            new AutoNumeric.multiple('#amount_receipt_cash', {
                currencySymbol : '',
                decimalCharacter : '.',
                digitGroupSeparator : ',',
                decimalPlaces: 2
            });

            $('.checkbox_active_card').empty();
            $('#amount_receipt_detail').val('');
            $('.error_amount_small').text('');
            $('.error_amount_large').text('');
            $('.error_amount_null').text('');
            $('.card_list_error').text('');
            $('.quantity_error').text('');
            $('.error_card_pired_date').text('');
            $('.count_using_card_error').text('');
            $('.type_error').text('');
            $('.error_count').text('');
            $('.error_account_money').text('');
            $('.money_owed_zero').text('');
            $('.money_large_moneybill').text('');
            // $("#receipt_type").val('').trigger('change');
            $('.card_null_sv').text('');
            // $('.cash').empty();
            $('.transfer').empty();
            $('.visa').empty();
            $('.member_money').empty();
            $('#note').val('');
            $('.btn_receipt').empty();
            $.getJSON(laroute.route('translate'), function (json) {
                $('.btn_receipt').append('<button type="button" data-dismiss="modal" class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn" id="receipt-close-btn"><span><i class="la la-arrow-left"></i><span>' + json['HỦY'] + '</span></span></button>');
                $('.btn_receipt').append('<button type="button" class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10" id="receipt-btn-print-bill"><span>' + json['THANH TOÁN & IN HÓA ĐƠN'] + '</span></button>');
                $('.btn_receipt').append('<button type="button" class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10" id="receipt-btn"><span>' + json['THANH TOÁN'] + '</span></button>');
            });
            var continute = true;

            var table_subbmit = [];
            $.each($('#table_add').find(".tr_table"), function () {
                var $tds = $(this).find("input,select");
                var $check_amount = $(this).find("input[name='amount']");
                $.getJSON(laroute.route('translate'), function (json) {
                    if ($check_amount.val() < 0) {
                        $('.error-table').text(json['Tổng tiền không hợp lệ']);
                        continute = false;
                    }
                });
                $.each($tds, function () {
                    table_subbmit.push($(this).val());
                });
            });

            var table_remove = [];
            $.each($('#table_add').find(".tr_table"), function () {
                var $tds = $(this).find("input[name='id_detail']");
                $.each($tds, function () {
                    table_remove.push($(this).val());
                });
            });
            var table_add = [];
            $.each($('#table_add').find(".table_add"), function () {
                var $tds = $(this).find("input,select");
                var $check_amount = $(this).find("input[name='amount']");
                $.getJSON(laroute.route('translate'), function (json) {
                    if ($check_amount.val() < 0) {
                        $('.error-table').text(json['Tổng tiền không hợp lệ']);
                        continute = false;
                    }
                });
                $.each($tds, function () {
                    table_add.push($(this).val());
                });
            });
            var check_service_card = [];
            $.each($('#table_add').find("tbody tr"), function () {
                var $check_amount = $(this).find("input[name='object_type']");
                if ($check_amount.val() == 'service_card') {
                    $.each($check_amount, function () {
                        check_service_card.push($(this).val());
                    });
                }
            });
            $.getJSON(laroute.route('translate'), function (json) {
                if (table_subbmit == '' && table_add == '') {
                    $('.error-table').text(json['Vui lòng chọn dịch vụ/thẻ dịch vụ/sản phẩm']);
                } else {
                    if ($('#customer_id').val() != 1) {
                        if ($('#money').val() > 0) {
                            $('.member_money_op').remove();
                            $('#receipt_type').append('<option value="member_money" class="member_money_op">Tài khoản thành viên</option>');
                        } else {
                            $('.member_money_op').remove();
                        }
                        if (check_service_card.length > 0) {
                            var tpl = $('#active-tpl').html();
                            $('.checkbox_active_card').append(tpl);
                            $("#check_active").change(function () {
                                if ($(this).is(":checked")) {
                                    $(this).val(1);
                                } else if ($(this).not(":checked")) {
                                    $(this).val(0);
                                }
                            });

                        } else {
                            $('.checkbox_active_card').empty();
                        }
                    } else {
                        $('.member_card').remove();
                        $('.member_money').remove();
                        $('.checkbox_active_card').empty();
                    }
                    //Check voucher còn sử dụng được hay ko
                    var voucher_using = [];

                    $.each($('#table_add').find(".tr_table"), function () {
                        var voucher_code = $(this).find("input[name='voucher_code']").val();
                        var type = $(this).find("input[name='object_type']").val();
                        if (voucher_code != '') {
                            voucher_using.push({
                                code: voucher_code,
                                type: type
                            });
                        }
                    });
                    $.each($('#table_add').find(".table_add"), function () {
                        var voucher_code = $(this).find("input[name='voucher_code']").val();
                        var type = $(this).find("input[name='object_type']").val();
                        if (voucher_code != '') {
                            voucher_using.push({
                                code: voucher_code,
                                type: type
                            });
                        }
                    });

                    if (voucher_using.length > 0) {
                        $.ajax({
                            url: laroute.route('admin.order.check-voucher'),
                            method: 'POST',
                            async: false,
                            dataType: 'JSON',
                            data: {
                                voucher_bill: $('#voucher_code_bill').val(),
                                voucher_using: voucher_using
                            },
                            success: function (res) {
                                if (res.is_success == false) {
                                    $('.error-table').text(json['Voucher bạn đang sử dụng đã hết số lần sử dụng']);
                                } else {
                                    $('.error-table').text('');
                                    $('#modal-receipt').modal('show');
                                }
                            }
                        });
                    } else {
                        $('#modal-receipt').modal('show');
                    }

                    $('#receipt-btn').click(function () {
                        mApp.block("#load", {
                            overlayColor: "#000000",
                            type: "loader",
                            state: "success",
                            message: json["Đang tải..."]
                        });
                        var order_code = $('#order_code').val();
                        var order_id = $('#order_id').val();
                        var customer_id = $('#customer_id').val();
                        var total_bill = $('input[name="total_bill"]').val();
                        var discount_bill = $('input[name="discount_bill"]').val();
                        var voucher_bill = $('#voucher_code_bill').val();
                        var amount_bill = $('input[name="amount_bill_input"]').val();
                        var amount_return = $('input[name="amount_return"]').val();
                        var amount_receipt = $('#receipt_amount').val();
                        var receipt_type = $('#receipt_type').val();
                        var amount_receipt_cash = $('#amount_receipt_cash').val();
                        var amount_receipt_atm = $('#amount_receipt_atm').val();
                        var amount_receipt_visa = $('#amount_receipt_visa').val();
                        var amount_receipt_money = $('#amount_receipt_money').val();
                        var id_amount_card = $('#service_card_search').val();
                        var amount_card = $('#service_cash').val();
                        var card_code = $('#service_card_search').val();
                        var note = $('#note').val();
                        var member_money = $('#money').val();
                        var discount_member = $('#member_level_discount').val();

                        if (receipt_type == '') {
                            $('.error_type').text(json['Hãy chọn hình thức thanh toán']);
                            check = false;
                        } else {
                            $('.error_type').text('');
                            check = true;
                        }
                        if (check = true) {
                            $.ajax({
                                url: laroute.route('admin.order-app.submit-receipt'),
                                dataType: 'JSON',
                                async: false,
                                data: {
                                    order_code: order_code,
                                    order_id: order_id,
                                    customer_id: customer_id,
                                    member_money: member_money,
                                    table_edit: table_subbmit,
                                    table_add: table_add,
                                    table_remove: table_remove,
                                    total_bill: total_bill,
                                    discount_bill: discount_bill,
                                    voucher_bill: voucher_bill,
                                    amount_bill: amount_bill,
                                    // amount_load: amount_load,
                                    amount_return: amount_return,
                                    amount_receipt: amount_receipt,
                                    receipt_type: receipt_type,
                                    amount_receipt_cash: amount_receipt_cash,
                                    amount_receipt_atm: amount_receipt_atm,
                                    amount_receipt_visa: amount_receipt_visa,
                                    amount_receipt_money: amount_receipt_money,
                                    id_amount_card: id_amount_card,
                                    amount_card: amount_card,
                                    card_code: card_code,
                                    note: note,
                                    check_active: $('#check_active').val(),
                                    refer_id: $('#refer_id').val(),
                                    discount_member: discount_member,
                                    order_source_id: $('#order_source_id').val(),
                                    tranport_charge: $('#tranport_charge').val()
                                },
                                method: 'POST',
                                success: function (response) {
                                    mApp.unblock("#load");
                                    if (response.card_list_error == 1) {
                                        $('.card_list_error').text(json['Mã thẻ '] + response.name + json[' không tồn tại']);
                                    } else {
                                        $('.card_list_error').text('');
                                    }
                                    if (response.quantity_error == 1) {
                                        $('.quantity_error').text(json['Số lượng '] + response.name + json[' không hợp lệ']);
                                    } else {
                                        $('.quantity_error').text('');
                                    }
                                    if (response.amount_null == 1) {
                                        $('.error_amount_null').text(response.message);
                                    } else {
                                        $('.error_amount_null').text('');
                                    }
                                    if (response.amount_detail_large == 1) {
                                        $('.error_amount_large').text(response.message);
                                    } else {
                                        $('.error_amount_large').text('');
                                    }
                                    if (response.amount_detail_small == 1) {
                                        $('.error_amount_small').text(response.message);
                                    } else {
                                        $('.error_amount_small').text('');
                                    }
                                    if (response.error_account_money == 1) {
                                        $('.error_account_money').text(json['Tiền trong tài khoản không đủ']);
                                    } else {
                                        $('.error_account_money').text('');
                                    }
                                    if (response.count_using_card_error == 1) {
                                        $('.count_using_card_error').text(json['Thẻ '] + response.name_card + '(' + response.code + json[') còn '] + response.using + json[' lần sử dụng ']);
                                    } else {
                                        $('.count_using_card_error').text('');
                                    }
                                    if (response.money_owed_zero == 1) {
                                        $('.money_owed_zero').text(json['Tiền tài khoản không hợp lệ']);
                                    } else {
                                        $('.money_owed_zero').text('');
                                    }
                                    if (response.money_large_moneybill == 1) {
                                        $('.money_large_moneybill').text(json['Tiền tài khoản không hợp lệ']);
                                    } else {
                                        $('.money_large_moneybill').text('');
                                    }
                                    if (response.error == true) {
                                        $('#modal-receipt').modal('hide');
                                        if (response.print_card.length > 0) {
                                            mApp.block(".load_ajax", {
                                                overlayColor: "#000000",
                                                type: "loader",
                                                state: "success",
                                                message: "Đang tải..."
                                            });
                                            $.ajax({
                                                url: laroute.route('admin.order-app.render-card'),
                                                dataType: 'JSON',
                                                async: false,
                                                method: 'POST',
                                                data: {
                                                    list_card: response.print_card
                                                },
                                                success: function (res) {
                                                    mApp.unblock(".load");
                                                    $('.list-card').empty();
                                                    $('.list-card').append(res);
                                                    $('#modal-print').modal('show');

                                                    var list_code = [];
                                                    $.each($('.list-card').find(".toimg"), function () {
                                                        var $tds = $(this).find("input[name='code']");
                                                        $.each($tds, function () {
                                                            list_code.push($(this).val());
                                                        });
                                                    });
                                                    for (let i = 1; i <= list_code.length; i++) {
                                                        html2canvas(document.querySelector("#check-selector-" + i + "")).then(canvas => {
                                                            $('.canvas').append(canvas);
                                                            var canvas = $(".canvas canvas");
                                                            var context = canvas.get(0).getContext("2d");
                                                            var dataURL = canvas.get(0).toDataURL();
                                                            var img = $("<img class='img-canvas' id=" + i + "></img>");
                                                            img.attr("src", dataURL);
                                                            canvas.replaceWith(img);
                                                        });
                                                    }

                                                }
                                            });
                                            if (response.isSMS == 0) {
                                                $(".btn-send-sms").remove();
                                            }
                                            $('#modal-print').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                            // let flagLoyalty = loyalty(response.orderId);
                                        } else {
                                            swal(json["Thanh toán đơn hàng thành công"], "", "success");
                                            // let flagLoyalty = loyalty(response.orderId);
                                            // if (flagLoyalty == true) {
                                            window.location.reload();
                                            // }
                                        }
                                    }
                                }
                            })
                        }
                    });
                    //Thanh toán và in hóa đơn
                    $('#receipt-btn-print-bill').click(function () {
                        mApp.block("#load", {
                            overlayColor: "#000000",
                            type: "loader",
                            state: "success",
                            message: json["Đang tải..."]
                        });
                        var order_code = $('#order_code').val();
                        var order_id = $('#order_id').val();
                        var customer_id = $('#customer_id').val();
                        var total_bill = $('input[name="total_bill"]').val();
                        var discount_bill = $('input[name="discount_bill"]').val();
                        var voucher_bill = $('#voucher_code_bill').val();
                        var amount_bill = $('input[name="amount_bill_input"]').val();
                        var amount_return = $('input[name="amount_return"]').val();
                        var amount_receipt = $('#receipt_amount').val();
                        var receipt_type = $('#receipt_type').val();
                        var amount_receipt_cash = $('#amount_receipt_cash').val();
                        var amount_receipt_atm = $('#amount_receipt_atm').val();
                        var amount_receipt_visa = $('#amount_receipt_visa').val();
                        var amount_receipt_money = $('#amount_receipt_money').val();
                        var id_amount_card = $('#service_card_search').val();
                        var amount_card = $('#service_cash').val();
                        var card_code = $('#service_card_search').val();
                        var note = $('#note').val();
                        var member_money = $('#money').val();
                        var discount_member = $('#member_level_discount').val();
                        if (receipt_type == '') {
                            $('.error_type').text(json['Hãy chọn hình thức thanh toán']);
                            check = false;
                        } else {
                            $('.error_type').text('');
                            check = true;
                        }
                        if (check = true) {
                            $.ajax({
                                url: laroute.route('admin.order-app.submit-receipt'),
                                async: false,
                                dataType: 'JSON',
                                data: {
                                    order_code: order_code,
                                    order_id: order_id,
                                    customer_id: customer_id,
                                    member_money: member_money,
                                    table_edit: table_subbmit,
                                    table_add: table_add,
                                    table_remove: table_remove,
                                    total_bill: total_bill,
                                    discount_bill: discount_bill,
                                    voucher_bill: voucher_bill,
                                    amount_bill: amount_bill,
                                    // amount_load: amount_load,
                                    amount_return: amount_return,
                                    amount_receipt: amount_receipt,
                                    receipt_type: receipt_type,
                                    amount_receipt_cash: amount_receipt_cash,
                                    amount_receipt_atm: amount_receipt_atm,
                                    amount_receipt_visa: amount_receipt_visa,
                                    amount_receipt_money: amount_receipt_money,
                                    id_amount_card: id_amount_card,
                                    amount_card: amount_card,
                                    card_code: card_code,
                                    note: note,
                                    check_active: $('#check_active').val(),
                                    refer_id: $('#refer_id').val(),
                                    discount_member: discount_member,
                                    tranport_charge: $('#tranport_charge').val()
                                },
                                method: 'POST',
                                success: function (response) {
                                    mApp.unblock("#load");
                                    if (response.card_list_error == 1) {
                                        $('.card_list_error').text(json['Mã thẻ '] + response.name + json[' không tồn tại']);
                                    } else {
                                        $('.card_list_error').text('');
                                    }
                                    if (response.quantity_error == 1) {
                                        $('.quantity_error').text(json['Số lượng '] + response.name + json[' không hợp lệ']);
                                    } else {
                                        $('.quantity_error').text('');
                                    }
                                    if (response.amount_null == 1) {
                                        $('.error_amount_null').text(response.message);
                                    } else {
                                        $('.error_amount_null').text('');
                                    }
                                    if (response.amount_detail_large == 1) {
                                        $('.error_amount_large').text(response.message);
                                    } else {
                                        $('.error_amount_large').text('');
                                    }
                                    if (response.amount_detail_small == 1) {
                                        $('.error_amount_small').text(response.message);
                                    } else {
                                        $('.error_amount_small').text('');
                                    }
                                    if (response.error_account_money == 1) {
                                        $('.error_account_money').text(json['Tiền trong tài khoản không đủ']);
                                    } else {
                                        $('.error_account_money').text('');
                                    }
                                    if (response.count_using_card_error == 1) {
                                        $('.count_using_card_error').text(json['Thẻ '] + response.name_card + '(' + response.code + json[') còn '] + response.using + json[' lần sử dụng ']);
                                    } else {
                                        $('.count_using_card_error').text('');
                                    }
                                    if (response.money_owed_zero == 1) {
                                        $('.money_owed_zero').text(json['Tiền tài khoản không hợp lệ']);
                                    } else {
                                        $('.money_owed_zero').text('');
                                    }
                                    if (response.money_large_moneybill == 1) {
                                        $('.money_large_moneybill').text(json['Tiền tài khoản không hợp lệ']);
                                    } else {
                                        $('.money_large_moneybill').text('');
                                    }
                                    if (response.error == true) {
                                        $('#modal-receipt').modal('hide');
                                        if (response.print_card.length > 0) {
                                            mApp.block(".load_ajax", {
                                                overlayColor: "#000000",
                                                type: "loader",
                                                state: "success",
                                                message: json["Đang tải..."]
                                            });
                                            $.ajax({
                                                url: laroute.route('admin.order-app.render-card'),
                                                dataType: 'JSON',
                                                async: false,
                                                method: 'POST',
                                                data: {
                                                    list_card: response.print_card
                                                },
                                                success: function (res) {
                                                    mApp.unblock(".load");
                                                    $('.list-card').empty();
                                                    $('.list-card').append(res);
                                                    $('#modal-print').modal('show');

                                                    var list_code = [];
                                                    $.each($('.list-card').find(".toimg"), function () {
                                                        var $tds = $(this).find("input[name='code']");
                                                        $.each($tds, function () {
                                                            list_code.push($(this).val());
                                                        });
                                                    });
                                                    for (let i = 1; i <= list_code.length; i++) {
                                                        html2canvas(document.querySelector("#check-selector-" + i + "")).then(canvas => {
                                                            $('.canvas').append(canvas);
                                                            var canvas = $(".canvas canvas");
                                                            var context = canvas.get(0).getContext("2d");
                                                            var dataURL = canvas.get(0).toDataURL();
                                                            var img = $("<img class='img-canvas' id=" + i + "></img>");
                                                            img.attr("src", dataURL);
                                                            canvas.replaceWith(img);
                                                        });
                                                    }

                                                }
                                            });
                                            if (response.isSMS == 0) {
                                                $(".btn-send-sms").remove();
                                            }
                                            $('#modal-print').modal({
                                                backdrop: 'static',
                                                keyboard: false
                                            });
                                            $('#orderiddd').val(response.orderId);
                                            // let flagLoyalty = loyalty(response.orderId);
                                            // if (flagLoyalty == true) {
                                            $('#form-order-ss').submit();
                                            // }
                                        } else {
                                            swal(json["Thanh toán đơn hàng thành công"], "", "success");

                                            $('#orderiddd').val(response.orderId);

                                            $('#form-order-ss').submit();
                                            // let flagLoyalty = loyalty(response.orderId);
                                            // if (flagLoyalty == true) {
                                            setTimeout(function () {
                                                window.location.reload();
                                            }, 1000);
                                            // }
                                        }
                                    }
                                }
                            })
                        }


                        // $.ajax({
                        //     url: laroute.route('admin.order.print-bill'),
                        //     method: "POST",
                        //     data: {orderId: response.orderId},
                        //     success: function (data) {
                        //         $('#orderiddd').val(response.orderId);
                        //         $('#form-order-ss').submit();
                        //     }
                        //
                        // });
                        //

                    });
                }
            });
            discountCustomerInput();
        });

        $.getJSON(laroute.route('translate'), function (json) {
            $('.staff').select2({
                placeholder: json['Chọn nhân viên'],
                allowClear: true
            });
            $('#refer_id').select2({
                placeholder: json['Chọn người giới thiệu'],
                allowClear: true
            });
        });
        // $('#delivery_active').select2({
        //    placeholder: 'Xác nhận đơn hàng'
        // });
        new AutoNumeric.multiple('#tranport_charge, #discount-modal, #discount-bill', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: decimal_number
        });
    });
});
var order = {
    loadDefault: function () {
        var type_load = $('.type').find('.active').attr('data-name');

        $.ajax({
            url: laroute.route('admin.order.load-add'),
            data: {
                type: type_load,
                customer_id: $('#customer_id').val()
            },
            method: 'POST',
            dataType: "JSON",
            success: function (res) {
                $('#list-product').empty();
                $('#list-product').append(res);

            }
        });
    },
    click: function (param) {
        $.getJSON(laroute.route('translate'), function (json) {
            mApp.block("#m_blockui_1_content", {
                overlayColor: "#000000",
                type: "loader",
                state: "success",
                message: json["Đang tải..."]
            });
        });
        if (param != 'member_card') {
            $.ajax({
                url: laroute.route('admin.order.list-add'),
                async: false,
                data: {
                    object_type: param,
                    customer_id: $('#customer_id').val()
                },
                method: 'POST',
                dataType: "JSON",
                success: function (res) {
                    $('#list-product').empty();
                    $('#list-product').append(res);
                }
            });
        } else {
            var id = $('#customer_id').val();
            $.ajax({
                url: laroute.route('admin.order.check-card-customer'),
                async: false,
                dataType: 'JSON',
                method: 'POST',
                data: {
                    id: id
                },
                success: function (res) {
                    mApp.unblock("#m_blockui_1_content");
                    $('.append').empty();
                    $.map(res.data, function (a) {
                        var tpl = $('#list-card-tpl').html();
                        tpl = tpl.replace(/{card_name}/g, a.card_name);
                        tpl = tpl.replace(/{card_code}/g, a.card_code);
                        tpl = tpl.replace(/{id_card}/g, a.customer_service_card_id);
                        $.getJSON(laroute.route('translate'), function (json) {
                            if (a.image != null) {
                                tpl = tpl.replace(/{img}/g, a.image);
                            } else {
                                tpl = tpl.replace(/{img}/g, 'uploads/admin/icon/default-placeholder.png');
                            }

                            if (a.count_using != 0) {
                                if (a.count_using != json['Không giới hạn']) {
                                    tpl = tpl.replace(/{quantity}/g, json['Còn'] + a.count_using);
                                    tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                                } else {
                                    tpl = tpl.replace(/{quantity}/g, 'KGH');
                                    tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                                }
                            } else {
                                tpl = tpl.replace(/{quantity}/g, 'KGH');
                                tpl = tpl.replace(/{quantity_app}/g, json['Không giới hạn']);
                            }
                        });
                        $('.append').append(tpl);
                        $.each($('#table_add tbody tr'), function () {
                            var codeHidden = $(this).find("input[name='object_code']");
                            var value_code = codeHidden.val();
                            var code = a.card_code;
                            if (value_code == code) {
                                var quantity = $(this).find("input[name='quantity']").val();
                                var quantity_card = $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val();
                                if (quantity_card != 'Không giới hạn') {
                                    $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val(quantity_card - quantity);
                                    $('.card_check_' + a.customer_service_card_id + '').find('.quantity').empty();
                                    $('.card_check_' + a.customer_service_card_id + '').find('.quantity').append('Còn ' + $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val() + ' (lần)');
                                }
                            }
                        });
                    });


                }
            });
        }
        discountCustomerInput();
    },
    append_table: function (id, price, type, name, code) {
        $.getJSON(laroute.route('translate'), function (json) {
            var check = true;
            if (check == true) {
                stt_tr++;
                var loc = price.replace(new RegExp('\\,', 'g'), '');
                var tpl = $('#table-tpl').html();
                tpl = tpl.replace(/{stt}/g, stt_tr);
                tpl = tpl.replace(/{name}/g, name);
                tpl = tpl.replace(/{id}/g, id);
                if (type == 'service') {
                    tpl = tpl.replace(/{type}/g, json['Dịch vụ']);
                    tpl = tpl.replace(/{id_type}/g, '1');
                }
                if (type == 'service_card') {
                    tpl = tpl.replace(/{type}/g, json['Thẻ dịch vụ']);
                    tpl = tpl.replace(/{id_type}/g, '2');
                }
                if (type == 'product') {
                    tpl = tpl.replace(/{type}/g, json['Sản phẩm']);
                    tpl = tpl.replace(/{id_type}/g, '3');
                }
                tpl = tpl.replace(/{type_hidden}/g, type);
                tpl = tpl.replace(/{price}/g, (price));
                tpl = tpl.replace(/{price_hidden}/g, loc);
                tpl = tpl.replace(/{amount}/g, (price));
                tpl = tpl.replace(/{amount_hidden}/g, loc);
                tpl = tpl.replace(/{code}/g, code);
                if (type != 'member_card') {
                    tpl = tpl.replace(/{class}/g, 'abc');
                } else {
                    tpl = tpl.replace(/{class}/g, 'abc_member_card');
                }
                $('#table_add > tbody').append(tpl);
                $('.staff').select2({
                    placeholder: json['Chọn nhân viên'],
                    allowClear: true
                });
                $(".quantity_add").TouchSpin({
                    initval: 1,
                    min: 1,
                    buttondown_class: "btn btn-default down btn-ct",
                    buttonup_class: "btn btn-default up btn-ct"
                });
                $('.quantity_add').change(function () {
                    $(this).closest('.table_add').find('.amount-tr').empty();
                    var id = $(this).closest('.table_add').find('input[name="id"]').val();
                    var type = $(this).closest('.table_add').find('input[name="object_type"]').val();
                    var stt = $(this).attr('data-id');

                    var id_type = "";
                    if (type === "service") {
                        id_type = 1;
                    } else if (type === "service_card") {
                        id_type = 2;
                    } else {
                        id_type = 3;
                    }
                    var price = $(this).closest('.table_add').find('input[name="price"]').val();
                    var discount = $(this).closest('.table_add').find('input[name="discount"]').val();
                    var loc = discount.replace(new RegExp('\\,', 'g'), '');
                    var quantity = $(this).val();
                    var amount = ((price * quantity) - loc);
                    $(this).closest('.table_add').find('.amount-tr').append(formatNumber(amount.toFixed(decimal_number)) + json['đ']);
                    $(this).closest('.table_add').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount.toFixed(decimal_number) + '">');
                    $('.append_bill').empty();
                    var sum = 0;
                    $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                        sum += Number($(this).val());
                    });
                    $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                    $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                    $('.tag_a').remove();
                    //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                    $('.amount_bill').append();
                    $('.amount_bill').empty();
                    var discount_bill = $('#discount_bill').val();
                    $('.close').remove();
                    if (discount_bill != 0) {
                        $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                    } else {
                        $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                    }
                    var amount_bill = (sum - discount_bill);
                    if (amount_bill < 0) {
                        amount_bill = 0;
                    }
                    $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                    $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                    $(this).closest('.table_add').find('.abc').remove();
                    if (discount != 0) {
                        $(this).closest('.table_add').find('.discount-tr-' + type + '-' + stt + '').prepend('<a class="abc" href="javascript:void(0)" onclick="list.close_amount_add(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                    } else {
                        $(this).closest('.table_add').find('.discount-tr-' + type + '-' + stt + '').prepend('<a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus" href="javascript:void(0)" onclick="list.modal_discount_add(' + amount.toFixed(decimal_number) + ',' + id + ',' + id_type + ',' + stt + ')"><i class="la la-plus icon-sz"></i></a>');
                    }
                    order.getPromotionGift();
                });
                $('.none').css('display', 'block');
                $('.append_bill').empty();
                var tpl_bill = $('#bill-tpl').html();
                var sum = 0;
                $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                    sum += Number($(this).val());
                });
                tpl_bill = tpl_bill.replace(/{total_bill_label}/g, formatNumber(sum.toFixed(decimal_number)));
                tpl_bill = tpl_bill.replace(/{total_bill}/g, sum.toFixed(decimal_number));
                $('.append_bill').prepend(tpl_bill);
                $('.amount_bill').empty();
                $('.tag_a').remove();
                //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                var discount_bill = $('#discount_bill').val();
                $('.close').remove();
                if (discount_bill != 0) {
                    $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                } else {
                    $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                }
                var amount_bill = (sum - discount_bill);
                if (amount_bill < 0) {
                    amount_bill = 0;
                }
                $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');

                order.getPromotionGift();
            }

            $(".quantity").TouchSpin({
                initval: 1,
                min: 1,
                buttondown_class: "btn btn-default down btn-ct",
                buttonup_class: "btn btn-default up btn-ct"
            });
            // $('.discount').mask('000,000,000', {reverse: true});
            $('.remove').click(function () {
                var totalAfterDiscountMember = $('.amount_bill_input').val() - $('#member_level_discount').val();

                $(this).closest('.table_add').remove();
                $('.append_bill').empty();
                var sum = 0;
                $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                    sum += Number($(this).val());
                });
                $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                $('.discount_bill').empty();
                $('.discount_bill').append(0 + json['đ']);
                $('.discount_bill').append('<input type="hidden" id="discount_bill" name="discount_bill" value="' + 0 + '">');
                $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
                $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                $('.amount_bill').empty();
                // var discount_bill = $('#discount_bill').val();
                // var amount_bill = parseInt(sum);
                $('.amount_bill').append(formatNumber(totalAfterDiscountMember.toFixed(decimal_number)) + json['đ']);
                $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + totalAfterDiscountMember.toFixed(decimal_number) + '">');
                discountCustomerInput();
                order.getPromotionGift();
            });
        });
        discountCustomerInput();
    },
    append_table_card: function (id, price, type, name, quantity_using, code, e) {
        $.getJSON(laroute.route('translate'), function (json) {
            if (quantity_using != json['Không giới hạn']) {
                var check = true;
                $.each($('#table_add tbody tr'), function () {
                    let codeHidden = $(this).find("input[name='object_code']");
                    let value_id = codeHidden.val();

                    let code_card = code;
                    if (value_id == code_card) {
                        check = false;
                        var count_using = $(e).find('.card_check_' + id + '').find('.quantity_card').val();
                        if (count_using > 0) {
                            $(e).find('.card_check_' + id + '').find('.quantity_card').val(count_using - 1);
                            $(e).find('.card_check_' + id + '').find('.quantity').empty();
                            $(e).find('.card_check_' + id + '').find('.quantity').append('Còn ' + $(e).find('.card_check_' + id + '').find('.quantity_card').val() + ' (lần)');
                            let quantitySv = codeHidden.parents('tr').find('input[name="quantity"]').val();
                            let numbers = parseInt(quantitySv) + 1;
                            codeHidden.parents('tr').find('input[name="quantity"]').val(numbers);
                        }
                    }
                });
                if (check == true) {
                    var stt = $('#table_add tr').length;
                    var loc = price.replace(/\D+/g, '');
                    var tpl = $('#table-card-tpl').html();
                    // tpl = tpl.replace(/{stt}/g, stt);
                    tpl = tpl.replace(/{name}/g, json['Sử dụng thẻ '] + name);
                    tpl = tpl.replace(/{id}/g, id);
                    tpl = tpl.replace(/{type_hidden}/g, type);
                    tpl = tpl.replace(/{price}/g, price);
                    tpl = tpl.replace(/{price_hidden}/g, loc);
                    tpl = tpl.replace(/{amount}/g, price);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{quantity_hid}/g, quantity_using);
                    tpl = tpl.replace(/{code}/g, code);
                    if (type != 'member_card') {
                        tpl = tpl.replace(/{class}/g, 'abc');
                    } else {
                        tpl = tpl.replace(/{class}/g, 'abc_member_card');
                    }
                    $('#table_add > tbody').append(tpl);
                    $('.staff').select2({
                        placeholder: json['Chọn nhân viên'],
                        allowClear: true
                    });
                    if (type == 'member_card') {
                        $('.abc_member_card ').remove();
                    }
                    var count_using = $(e).find('.card_check_' + id + '').find('.quantity_card').val();
                    $(e).find('.card_check_' + id + '').find('.quantity_card').val(count_using - 1);
                    $(e).find('.card_check_' + id + '').find('.quantity').empty();
                    $(e).find('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $(e).find('.card_check_' + id + '').find('.quantity_card').val() + json[' (lần)']);

                }

                $(".quantity_c").TouchSpin({
                    initval: 1,
                    min: 1,
                    max: quantity_using,
                    buttondown_class: "btn btn-default down btn-ct",
                    buttonup_class: "btn btn-default up btn-ct"
                });
                $('.quantity_c').change(function () {
                    var quan_val = $(this).val();
                    var quan_db = $(this).closest('.table_add').find("input[name='quantity_hid']").val();
                    var id = $(this).closest('.table_add').find("input[name='id']").val();
                    $('.card_check_' + id + '').find('.quantity_card').val(quan_db - quan_val);
                    $('.card_check_' + id + '').find('.quantity').empty();
                    $('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + id + '').find('.quantity_card').val() + json[' (lần)']);
                });
                $('.remove_card_new').click(function () {
                    var quan_db = $(this).closest('.table_add').find("input[name='quantity_hid']").val();
                    var id = $(this).closest('.table_add').find("input[name='id']").val();
                    $('.card_check_' + id + '').find('.quantity_card').val(quan_db);
                    $('.card_check_' + id + '').find('.quantity').empty();
                    $('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + id + '').find('.quantity_card').val() + json[' (lần)']);
                    $(this).closest('.table_add').remove();
                });
            } else {
                var check = true;
                $.each($('#table_add tbody tr'), function () {
                    let codeHidden = $(this).find("input[name='object_code']");
                    let value_id = codeHidden.val();
                    let code_card = code;
                    if (value_id == code_card) {
                        check = false;
                        let quantitySv = codeHidden.parents('tr').find('input[name="quantity"]').val();
                        let numbers = parseInt(quantitySv) + 1;
                        codeHidden.parents('tr').find('input[name="quantity"]').val(numbers);
                    }
                });
                if (check == true) {
                    var stt = $('#table_add tr').length;
                    var loc = price.replace(/\D+/g, '');
                    var tpl = $('#table-card-tpl').html();
                    // tpl = tpl.replace(/{stt}/g, stt);
                    tpl = tpl.replace(/{name}/g, json['Sử dụng thẻ '] + name);
                    tpl = tpl.replace(/{id}/g, id);
                    tpl = tpl.replace(/{type_hidden}/g, type);
                    tpl = tpl.replace(/{price}/g, price);
                    tpl = tpl.replace(/{price_hidden}/g, loc);
                    tpl = tpl.replace(/{amount}/g, price);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{quantity_hid}/g, 0);
                    tpl = tpl.replace(/{code}/g, code);
                    if (type != 'member_card') {
                        tpl = tpl.replace(/{class}/g, 'abc');
                    } else {
                        tpl = tpl.replace(/{class}/g, 'abc_member_card');
                    }
                    $('#table_add > tbody').append(tpl);
                    if (type == 'member_card') {
                        $('.abc_member_card ').remove();
                    }

                }

                $(".quantity_c").TouchSpin({
                    initval: 1,
                    min: 1,
                    buttondown_class: "btn btn-default down btn-ct",
                    buttonup_class: "btn btn-default up btn-ct"
                });
                $('.remove_card_new').click(function () {
                    $(this).closest('.table_add').remove();
                });
            }
        });
    },
    print: function (code) {
        $.getJSON(laroute.route('translate'), function (json) {
            var stt_image = $('.toimg.' + code + '').find('input[name="stt"]').val();
            var base = $('.canvas').find('#' + stt_image + '').attr('src');
            $.ajax({
                url: laroute.route('admin.order.print-card-one'),
                async: false,
                method: 'POST',
                dataType: 'JSON',
                data: {
                    base: base
                }, success: function (res) {
                    $('.content-print-card').empty();
                    $('.content-print-card').append(res);
                    jQuery('#print-card').print()
                }
            });
        });
    },
    print_all: function () {
        var list_image = [];
        $.each($('.canvas'), function () {
            var $tds = $(this).find(".img-canvas");
            $.each($tds, function () {
                list_image.push($(this).attr('src'));
            });
        });
        $.ajax({
            url: laroute.route('admin.order.print-card-all'),
            async: false,
            method: "POST",
            data: {
                list_image: list_image
            },
            success: function (res) {
                $('.content-print-card').empty();
                $('.content-print-card').append(res);
                jQuery('#print-card').print();
            }

        });
    },
    send_mail: function () {
        var customer_id = $('#customer_id').val();
        if (customer_id == 1) {
            $('#modal-enter-email').modal('show');
        } else {
            $.ajax({
                url: laroute.route('admin.order.check-email-customer'),
                dataTye: 'JSON',
                method: 'POST',
                data: {
                    customer_id: customer_id
                }, success: function (res) {
                    if (res.email_null == 1) {
                        $('#modal-enter-email').modal('show');
                    }
                    if (res.email_success == 1) {
                        $('#enter_email').val(res.email).attr('disabled', true);
                        $('#modal-enter-email').modal('show');
                    }
                }
            });
        }
    },
    submit_send_email: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#submit_email').validate({
                rules: {
                    enter_email: {
                        required: true,
                        email: true
                    },
                },
                messages: {
                    enter_email: {
                        required: json['Hãy nhập email'],
                        email: json['Email không hợp lệ']
                    },
                },
                submitHandler: function () {
                    ///list image
                    var list_image = [];
                    $.each($('.canvas'), function () {
                        var $tds = $(this).find(".img-canvas");
                        $.each($tds, function () {
                            list_image.push($(this).attr('src'));
                        });
                    });
                    $.ajax({
                        url: laroute.route('admin.order.submit-send-email'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {
                            email: $('#enter_email').val(),
                            customer_id: $('#customer_id').val(),
                            list_image: list_image
                        }, success: function (res) {
                            if (res.success == 1) {
                                swal(json["Gửi email thành công"], "", "success");
                                $('#modal-enter-email').modal('hide');
                            }
                        }
                    });
                }
            });
        });
    },
    changeTranportCharge: function () {
        if ($('#tranport_charge').val() == '') {
            $('#tranport_charge').val(0);
        }
        discountCustomerInput();
    },
    changeAmountReceipt:function (obj) {
        var amount_receipt_cash = 0;
        if ($('#amount_receipt_cash').val() != undefined && $('#amount_receipt_cash').val() != '') {
            amount_receipt_cash = $('#amount_receipt_cash').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_atm = 0;
        if ($('#amount_receipt_atm').val() != undefined && $('#amount_receipt_atm').val() != '') {
            amount_receipt_atm = $('#amount_receipt_atm').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_visa = 0;
        if ($('#amount_receipt_visa').val() != undefined && $('#amount_receipt_visa').val() != '') {
            amount_receipt_visa = $('#amount_receipt_visa').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_money = 0;
        if ($('#amount_receipt_money').val() != undefined && $('#amount_receipt_money').val() != '') {
            amount_receipt_money = $('#amount_receipt_money').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_all = Number(amount_receipt_cash) + Number(amount_receipt_atm) + Number(amount_receipt_visa) + Number(amount_receipt_money);

        $('#amount_all').val(formatNumber(amount_all.toFixed(decimal_number)));
        $('.cl_amount_all').text(formatNumber(amount_all.toFixed(decimal_number)));

        var rest = $('#receipt_amount').val().replace(new RegExp('\\,', 'g'), '');
        if (rest - amount_all > 0) {
            $('#amount_rest').val(formatNumber((rest - amount_all).toFixed(decimal_number)));
            $('.cl_amount_rest').text(formatNumber((rest - amount_all).toFixed(decimal_number)));
            if ($(obj).val() == '') {
                if (rest - amount_all < 0) {
                    $('#amount_return').val(formatNumber((amount_all - rest).toFixed(decimal_number)));
                    $('.cl_amount_return').text(formatNumber((amount_all - rest).toFixed(decimal_number)));
                } else {
                    $('#amount_return').val(0);
                    $('.cl_amount_return').text(0);
                }
            }
        } else {
            $('#amount_rest').val(0);
            $('#amount_return').val(formatNumber((amount_all - rest).toFixed(decimal_number)));
            $('.cl_amount_rest').text(0);
            $('.cl_amount_return').text(formatNumber((amount_all - rest).toFixed(decimal_number)));
        }
        discountCustomerInput();
    },
    getPromotionGift:function () {
        $.getJSON(laroute.route('translate'), function (json) {
            //Lấy total quantity sp, dv, thẻ dv
            var arrParam = [];
            $.each($('#table_add').find('.tr_table, .table_add'), function () {
                var objectType = $(this).find("input[name='object_type']").val();
                var objectCode = $(this).find("input[name='object_code']").val();
                var quantity = $(this).find("input[name='quantity']").val();

                arrParam.push({
                    objectType: objectType,
                    objectCode: objectCode,
                    quantity: quantity
                });
            });

            //Check promotion gift
            $.ajax({
                url: laroute.route('admin.order.check-gift'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_id: $('#customer_id').val(),
                    arrParam: arrParam
                },
                async: false,
                success: function (res) {
                    $('.promotion_gift').remove();
                    if (res.gift > 0) {
                        $.map(res.arr_gift, function (a) {
                            stt_tr++;
                            var zero = 0;
                            var tpl = $('#table-gift-tpl').html();
                            tpl = tpl.replace(/{stt}/g, stt_tr);
                            tpl = tpl.replace(/{name}/g, a.gift_object_name + ' (' + json['quà tặng'] + ')');
                            tpl = tpl.replace(/{id}/g, a.gift_object_id);
                            tpl = tpl.replace(/{code}/g, a.gift_object_code);
                            tpl = tpl.replace(/{type_hidden}/g, a.gift_object_type + '_gift');
                            tpl = tpl.replace(/{price}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{price_hidden}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{amount}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{amount_hidden}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{quantity}/g, a.quantity_gift);
                            tpl = tpl.replace(/{quantity_hid}/g, zero.toFixed(decimal_number));
                            $('#table_add > tbody').append(tpl);
                        });
                    }
                }
            });
        });
    },
    removeGift: function (obj) {
        $(obj).closest('.tr_table').remove();
    },
};

var list = {
    remove: function (obj, id) {
        // hightlight row
        $.getJSON(laroute.route('translate'), function (json) {
            $(obj).closest('tr').addClass('m-table__row--danger');

            swal({
                title: 'Thông báo',
                text: "Bạn có muốn xóa không?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Xóa',
                cancelButtonText: 'Hủy',
                onClose: function () {
                    // remove hightlight row
                    $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    $.post(laroute.route('admin.order.remove', {id: id}), function () {
                        swal(
                            'Xóa thành công',
                            '',
                            'success'
                        );
                        // window.location.reload();
                        $('#autotable').PioTable('refresh');
                    });
                }
            });
        });
    },
    refresh: function () {
        $('input[name="search"]').val('');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var id = $('#customer_id').val();
            var type = $('.type').find('.active').attr('data-name');
            // var type_load = $('.type').find('.active').attr('data-name');

            var search = $('#search').val();
            mApp.block("#m_blockui_1_content", {
                overlayColor: "#000000",
                type: "loader",
                state: "success",
                message: json["Đang tải..."]
            });
            $.ajax({
                url: laroute.route('admin.order.search'),
                data: {
                    type: type,
                    search: search,
                    id: id
                },
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.type != 'member_card') {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            var tpl = $('#list-tpl').html();
                            tpl = tpl.replace(/{name}/g, a.name);
                            tpl = tpl.replace(/{price}/g, a.price);
                            tpl = tpl.replace(/{id}/g, a.id);
                            if (a.avatar != null) {
                                tpl = tpl.replace(/{img}/g, a.avatar);
                            } else {
                                tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                            }
                            tpl = tpl.replace(/{price_hidden}/g, a.price);
                            tpl = tpl.replace(/{type}/g, a.type);
                            $('.append').append(tpl);
                        });
                    } else {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            var tpl = $('#list-card-tpl').html();
                            tpl = tpl.replace(/{card_name}/g, a.card_name);
                            tpl = tpl.replace(/{card_code}/g, a.card_code);
                            tpl = tpl.replace(/{id_card}/g, a.customer_service_card_id);
                            if (a.image != null) {
                                tpl = tpl.replace(/{img}/g, a.image);
                            } else {
                                tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                            }
                            if (a.count_using != 0) {
                                tpl = tpl.replace(/{quantity}/g, json['Còn '] + a.count_using);
                                tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                            } else {
                                tpl = tpl.replace(/{quantity}/g, json['Không giới hạn']);
                                tpl = tpl.replace(/{quantity_app}/g, json['Không giới hạn']);
                            }
                            $('.append').append(tpl);
                            $.each($('#table_add tbody tr'), function () {
                                var codeHidden = $(this).find("input[name='object_code']");
                                var value_code = codeHidden.val();
                                var code = a.card_code;
                                if (value_code == code) {
                                    var quantity = $(this).find("input[name='quantity']").val();
                                    var quantity_card = $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val();
                                    if (quantity_card != 'Không giới hạn') {
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val(quantity_card - quantity);
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').empty();
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val() + json[' (lần)']);
                                    }
                                }
                            });
                        });
                    }

                }
            })
        });

    },
    close_amount: function (id, id_type, stt) {
        $.getJSON(laroute.route('translate'), function (json) {
            var type_class = "";
            if (id_type == 1) {
                type_class = "service";
            } else if (id_type == 2) {
                type_class = "service_card";
            } else {
                type_class = "product";
            }
            $(".discount-tr-" + type_class + "-" + stt + "").empty();
            var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
            var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
            var discount_new = 0;
            var amount_new = (price * quantity - discount_new);
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" value="0">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus" href="javascript:void(0)" onclick="list.modal_discount(' + amount_new.toFixed(decimal_number) + ',' + id + ',' + id_type + ',' + stt + ')"><i class="la la-plus icon-sz"></i></a>');
            $('.append_bill').empty();
            var sum = 0;
            $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                sum += Number($(this).val());
            });
            $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
            $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
            $('.tag_a').remove();
            //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
            $('.amount_bill').append();
            $('.amount_bill').empty();
            var discount_bill = $('#discount_bill').val();
            $('.close').remove();
            if (discount_bill != 0) {
                $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
            } else {
                $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            }
            var amount_bill = (sum - discount_bill);
            $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
        });
    },
    modal_discount: function (amount, id, id_type, stt) {
        $('#modal-discount').modal('show');
        $('#amount-tb').val(amount);
        $('#discount-modal').val(0);
        $('#discount-code-modal').val('');
        $('.error-discount1').text('');
        $('.error-discount').text('');
        $('.error_discount_code').text('');
        $('.error_discount_expired').text('');
        $('.error_discount_not_using').text('');
        $('.error_discount_amount_error').text('');
        $('.error_discount_null').text('');
        $('.branch_not').text('');
        $('.btn-click').empty();
        // $('.btn-click').append('<button type="button" onclick="list.discount(' + id + ',' + id_type + ')" class="btn btn-primary">Áp dụng</button>');
        // $('.btn-click').append('<input type="button" data-dismiss="modal"  class="btn btn-default" value="Hủy">');
        var tpl = $('#button-discount-tpl').html();
        tpl = tpl.replace(/{id}/g, id);
        tpl = tpl.replace(/{id_type}/g, id_type);
        tpl = tpl.replace(/{stt}/g, stt);
        $('.btn-click').append(tpl);
        $('#live-tag').click(function () {
            $('#discount-code-modal').val('');
            $('.error_discount_code').text('');
            $('.error_discount_expired').text('');
            $('.error_discount_not_using').text('');
            $('.error_discount_amount_error').text('');
            $('.error_discount_null').text('');
        });
        $('#code-tag').click(function () {
            $('#discount-modal').val(0);
            $('.error-discount1').text('');
            $('.error-discount').text('');
        });
    },
    discount: function (id, id_type, stt) {
        $.getJSON(laroute.route('translate'), function (json) {
            var amount = $('#amount-tb').val();
            var discount = $('#discount-modal').val().replace(new RegExp('\\,', 'g'), '');
            var type_discount = $("input[name='type-discount']:checked").val();
            var voucher_code = $('#discount-code-modal').val();
            var type_class = "";
            var amount_bill = $('input[name="total_bill"]').val();
            var total_using_voucher = 0;

            $("input[name='voucher_code']").each(function (val) {
                var value = $(this).val();
                if (value === voucher_code.trim()) {
                    total_using_voucher++;
                }
            });

            if (id_type == 1) {
                type_class = "service";
            } else if (id_type == 2) {
                type_class = "service_card";
            } else {
                type_class = "product";
            }
            $.ajax({
                url: laroute.route('admin.order.add-discount'),
                data: {
                    amount: amount,
                    discount: discount,
                    type: type_discount,
                    voucher_code: voucher_code,
                    type_order: type_class,
                    id_order: id,
                    amount_bill: amount_bill,
                    total_using_voucher: total_using_voucher
                },
                async: false,
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.error_money === 1) {
                        $('.error-discount1').text(json['Số tiền giảm giá không hợp lệ']);
                    } else {
                        $('.error-discount1').text('');
                    }
                    if (response.error_money === 0) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(discount) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + discount + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="" >');
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);

                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc"  href="javascript:void(0)" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                        //Xóa giảm giá tổng bill
                        ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
                        order.close_discount_bill($('input[name=total_bill]').val());
                    }
                    if (response.error_percent === 1) {
                        $('.error-discount').text(json['Số tiền giảm giá không hợp lệ']);
                    } else {
                        $('.error-discount').text('');
                    }
                    if (response.error_percent === 0) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(response.discount_percent) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + response.discount_percent + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="" >');
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);

                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" href="javascript:void(0)" style="color:red" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                        //Xóa giảm giá tổng bill
                        ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
                        list.close_discount_bill($('input[name=total_bill]').val());
                    }
                    if (response.voucher_null === 1) {
                        $('.error_discount_null').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_discount_null').text('');
                    }
                    if (response.voucher_not_exist == 1) {
                        $('.error_discount_code').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_discount_code').text('');
                    }
                    if (response.voucher_expired == 1) {
                        $('.error_discount_expired').text(json['Mã giảm giá hết hạn sử dụng']);
                    } else {
                        $('.error_discount_expired').text('');
                    }
                    if (response.voucher_not_using == 1) {
                        $('.error_discount_not_using').text(json['Mã giảm giá đã hết số lần sử dụng']);
                    } else {
                        $('.error_discount_not_using').text('');
                    }
                    if (response.voucher_amount_error == 1) {
                        $('.error_discount_amount_error').text(json['Tổng tiền không đủ sử dụng mã giảm giá']);
                    } else {
                        $('.error_discount_amount_error').text('');
                    }
                    if (response.branch_not == 1) {
                        $('.branch_not').text(json['Mã giảm giá không sử dụng cho chi nhánh này']);
                    } else {
                        $('.branch_not').text('');
                    }
                    if (response.voucher_success == 1) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(response.discount_voucher) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + response.discount_voucher + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="' + response.voucher_name + '" >')
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);
                        if (amount_new < 0) {
                            amount_new = 0;
                        }
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" style="color:red" href="javascript:void(0)" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="close" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                        //Xóa giảm giá tổng bill
                        ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
                        order.close_discount_bill($('input[name=total_bill]').val());
                    }
                }
            });
        });
        discountCustomerInput();
    },
    modal_discount_add: function (amount, id, id_type, stt) {
        $('#modal-discount').modal('show');
        $('#amount-tb').val(amount);
        $('#discount-modal').val(0);
        $('#discount-code-modal').val('');
        $('.error-discount1').text('');
        $('.error-discount').text('');
        $('.error_discount_code').text('');
        $('.error_discount_expired').text('');
        $('.error_discount_not_using').text('');
        $('.error_discount_amount_error').text('');
        $('.error_discount_null').text('');
        $('.branch_not').text('');
        $('.btn-click').empty();
        // $('.btn-click').append('<button type="button" onclick="list.discount_add(' + id + ',' + id_type + ')" class="btn btn-primary">Áp dụng</button>');
        // $('.btn-click').append('<input type="button" data-dismiss="modal"  class="btn btn-default" value="Hủy">');
        var tpl = $('#button-discount-add-tpl').html();
        tpl = tpl.replace(/{id}/g, id);
        tpl = tpl.replace(/{id_type}/g, id_type);
        tpl = tpl.replace(/{stt}/g, stt);
        $('.btn-click').append(tpl);
        $('#live-tag').click(function () {
            $('#discount-code-modal').val('');
            $('.error_discount_code').text('');
            $('.error_discount_expired').text('');
            $('.error_discount_not_using').text('');
            $('.error_discount_amount_error').text('');
            $('.error_discount_null').text('');
        });
        $('#code-tag').click(function () {
            $('#discount-modal').val(0);
            $('.error-discount1').text('');
            $('.error-discount').text('');
        });
    },
    discount_add: function (id, id_type, stt) {
        $.getJSON(laroute.route('translate'), function (json) {
            var amount = $('#amount-tb').val();
            var discount = $('#discount-modal').val().replace(new RegExp('\\,', 'g'), '');
            var type_discount = $("input[name='type-discount']:checked").val();
            var voucher_code = $('#discount-code-modal').val();
            var amount_bill = $('input[name="total_bill"]').val();
            var total_using_voucher = 0;
            $("input[name='voucher_code']").each(function (val) {
                var value = $(this).val();
                if (value === voucher_code.trim()) {
                    total_using_voucher++;
                }
            });
            var type_class = "";
            if (id_type == 1) {
                type_class = "service";
            } else if (id_type == 2) {
                type_class = "service_card";
            } else {
                type_class = "product";
            }
            $.ajax({
                url: laroute.route('admin.order.add-discount'),
                data: {
                    amount: amount,
                    discount: discount,
                    type: type_discount,
                    voucher_code: voucher_code,
                    type_order: type_class,
                    id_order: id,
                    amount_bill: amount_bill,
                    total_using_voucher: total_using_voucher
                },
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.error_money === 1) {
                        $('.error-discount1').text(json['Số tiền giảm giá không hợp lệ']);
                    } else {
                        $('.error-discount1').text('');
                    }
                    if (response.error_money === 0) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(discount) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + discount + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="" >');
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="discount"]').val();
                        var amount_new = parseInt(price * quantity - discount_new);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)));
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" href="javascript:void(0)" onclick="list.close_amount_add(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.append_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                    }
                    if (response.error_percent === 1) {
                        $('.error-discount').text(json['Số tiền giảm giá không hợp lệ']);
                    } else {
                        $('.error-discount').text('');
                    }
                    if (response.error_percent === 0) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(response.discount_percent) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + response.discount_percent + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="" >');
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)));
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" href="javascript:void(0)" onclick="list.close_amount_add(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></span></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                    }
                    if (response.voucher_null === 1) {
                        $('.error_discount_null').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_discount_null').text('');
                    }
                    if (response.voucher_not_exist == 1) {
                        $('.error_discount_code').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_discount_code').text('');
                    }
                    if (response.voucher_expired == 1) {
                        $('.error_discount_expired').text(json['Mã giảm giá hết hạn sử dụng']);
                    } else {
                        $('.error_discount_expired').text('');
                    }
                    if (response.voucher_not_using == 1) {
                        $('.error_discount_not_using').text(json['Mã giảm giá đã hết số lần sử dụng']);
                    } else {
                        $('.error_discount_not_using').text('');
                    }
                    if (response.voucher_amount_error == 1) {
                        $('.error_discount_amount_error').text(json['Tổng tiền không đủ sử dụng mã giảm giá']);
                    } else {
                        $('.error_discount_amount_error').text('');
                    }
                    if (response.branch_not == 1) {
                        $('.branch_not').text(json['Mã giảm giá không sử dụng cho chi nhánh này']);
                    } else {
                        $('.branch_not').text('');
                    }
                    if (response.voucher_success == 1) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(response.discount_voucher) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + response.discount_voucher + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="' + response.voucher_name + '" >')
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);
                        if (amount_new < 0) {
                            amount_new = 0;
                        }
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" href="javascript:void(0)" onclick="list.close_amount_add(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                    }
                }
            })
        });
    },
    close_amount_add: function (id, id_type, stt) {
        $.getJSON(laroute.route('translate'), function (json) {
            var type_class = "";
            if (id_type == 1) {
                type_class = "service";
            } else if (id_type == 2) {
                type_class = "service_card";
            } else {
                type_class = "product";
            }
            $(".discount-tr-" + type_class + "-" + stt + "").empty();
            var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="price"]').val();
            var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('input[name="quantity"]').val();
            var discount_new = 0;
            var amount_new = (price * quantity - discount_new);
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').empty();
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.table_add').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" value="0">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus" href="javascript:void(0)" onclick="list.modal_discount_add(' + amount_new.toFixed(decimal_number) + ',' + id + ',' + id_type + ',' + stt + ')"><i class="la la-plus icon-sz"></i></a>');
            $('.append_bill').empty();
            var sum = 0;
            $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                sum += Number($(this).val());
            });
            $('.append_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
            $('.append_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
            $('.tag_a').remove();
            //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
            $('.amount_bill').append();
            $('.amount_bill').empty();
            var discount_bill = $('#discount_bill').val();
            $('.close').remove();
            if (discount_bill != 0) {
                $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
            } else {
                $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            }
            var amount_bill = (sum - discount_bill);
            $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
        });
    },
    modal_discount_bill: function (amount_bill) {
        $('#modal-discount-bill').modal('show');
        $('#amount-bill').val(amount_bill);
        $('#discount-bill').val(0);
        $('#discount-code-bill-modal').val('');
        $('.error-discount-bill').text('');
        $('.error-discount-bill-percent').text('');
        $('.error_bill_null').text('');
        $('.error_bill_expired').text('');
        $('.error_bill_amount').text('');
        $('.error_bill_not_using').text('');
        $('.branch_not').text('');
        $('#live-tag-bill').click(function () {
            $('#discount-code-bill-modal').val('');
            $('.error_bill_null').text('');
            $('.error_bill_expired').text('');
            $('.error_bill_amount').text('');
            $('.error_bill_not_using').text('');
        });
        $('#code-tag-bill').click(function () {
            $('#discount-bill').val(0);
            $('.error-discount-bill').text('');
            $('.error-discount-bill-percent').text('');
        });
        $('.btn-click-bill').empty();
        // $('.btn-click-bill').append('<button type="button" onclick="list.modal_discount_bill_click()" class="btn btn-primary">Áp dụng</button>');
        // $('.btn-click-bill').append('<button type="button" onclick="list.close_modal_discount_bill()" class="btn btn-default">Hủy</button>');
        var tpl = $('#button-discount-bill-tpl').html();
        $('.btn-click-bill').append(tpl);
        // $('#discount-bill').mask('000,000,000', {reverse: true});
    },
    close_modal_discount_bill: function () {
        $('#modal-discount-bill').modal('hide');
    },
    modal_discount_bill_click: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var total_bill = $("input[name=total_bill]").val() - $('#member_level_discount').val();
            var discount_bill = $('#discount-bill').val().replace(new RegExp('\\,', 'g'), '');
            var type_discount_bill = $("input[name='type-discount-bill']:checked").val();
            var voucher_code_bill = $('#discount-code-bill-modal').val();
            $.ajax({
                url: laroute.route('admin.order.add-discount-bill'),
                data: {
                    total_bill: total_bill,
                    discount_bill: discount_bill,
                    type_discount_bill: type_discount_bill,
                    voucher_code_bill: voucher_code_bill
                },
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.error_money_bill == 1) {
                        $('.error-discount-bill').text(json['Số tiền không hợp lệ']);
                    } else {
                        $('.error-discount-bill').text('');
                    }
                    if (response.error_percent_bill == 1) {
                        $('.error-discount-bill-percent').text(json['Số tiền không hợp lệ']);
                    } else {
                        $('.error-discount-bill-percent').text('');
                    }
                    if (response.error_money_bill == 0) {
                        $('#modal-discount-bill').modal('hide');
                        $('.discount_bill').empty();
                        $('.discount_bill').append(formatNumber(response.discount_bill) + json['đ']);
                        $('.discount_bill').append('<input type="hidden" id="discount_bill" name="discount_bill" value=' + response.discount_bill + '>');
                        $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
                        $('.discount_bill').prepend('<a class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + total_bill + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                        var amount_bill = (total_bill - response.discount_bill);
                        $('.amount_bill').empty();
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + amount_bill.toFixed(decimal_number) + '>');
                        $('.amount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
                    }
                    if (response.error_percent_bill == 0) {
                        $('#modal-discount-bill').modal('hide');
                        $('.discount_bill').empty();
                        $('.discount_bill').append(formatNumber(response.discount_bill) + json['đ']);
                        $('.discount_bill').append('<input type="hidden" id="discount_bill" name="discount_bill" value=' + response.discount_bill + '>');
                        $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
                        $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + total_bill + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                        var amount_bill = (total_bill - response.discount_bill);
                        $('.amount_bill').empty();
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + amount_bill.toFixed(decimal_number) + '>');
                    }

                    if (response.voucher_bill_null == 1) {
                        $('.error_bill_null').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_bill_null').text('');
                    }
                    if (response.voucher_bill_expired == 1) {
                        $('.error_bill_expired').text(json['Mã giảm giá hết hạn sử dụng']);
                    } else {
                        $('.error_bill_expired').text('');
                    }
                    if (response.voucher_amount_bill_error == 1) {
                        $('.error_bill_amount').text(json['Tổng tiền không đủ để sử dụng mã giảm giá']);
                    } else {
                        $('.error_bill_amount').text('');
                    }
                    if (response.voucher_bill_not_using == 1) {
                        $('.error_bill_not_using').text(json['Mã giảm giá đã hết số lần sử dụng']);
                    } else {
                        $('.error_bill_not_using').text('');
                    }
                    if (response.branch_not == 1) {
                        $('.branch_not').text(json['Mã giảm giá không sử dụng cho chi nhánh này']);
                    } else {
                        $('.branch_not').text('');
                    }
                    if (response.voucher_success_bill == 1) {
                        $('#modal-discount-bill').modal('hide');
                        $('.discount_bill').empty();
                        $('.discount_bill').append(formatNumber(response.discount_voucher_bill) + json['đ']);
                        $('.discount_bill').append('<input type="hidden" id="discount_bill" name="discount_bill" value=' + response.discount_voucher_bill + '>');
                        $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="' + response.voucher_name_bill + '">');
                        $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="list.close_discount_bill(' + total_bill + ')"><i class="la la-close cl_amount_bill m--margin-right-5"></i></a>');
                        var amount_bill = (total_bill - response.discount_voucher_bill);
                        if (amount_bill < 0) {
                            amount_bill = 0;
                        }
                        $('.amount_bill').empty();
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + amount_bill.toFixed(decimal_number) + '>');
                    }
                }
            })
        });
        discountCustomerInput();
    },
    close_discount_bill: function (total_bill) {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#modal-discount-bill').modal('hide');
            // var total_bill = $('#amount-bill').val();
            $('.discount_bill').empty();
            $('.discount_bill').append(0 + json['đ']);
            $('.discount_bill').append('<input type="hidden" name="discount_bill" id="discount_bill" value=' + 0 + '>')
            $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="list.modal_discount_bill(' + total_bill + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            $('.amount_bill').empty();
            $('.amount_bill').append(formatNumber(total_bill.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + total_bill.toFixed(decimal_number) + '>');
        });
        discountCustomerInput();
    },
    apply_branch: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('admin.order.apply-branch'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    order_id: id
                },
                success: function (res) {
                    $('#my-modal').html(res.url);
                    $('#modal-apply-branch').modal('show');
                    $('#branch_id').select2({
                        placeholder: json['Chọn chi nhánh']
                    });
                }
            });
        });
    },
    submit_apply_branch: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-apply-branch');

            form.validate({
                rules: {
                    branch_id: {
                        required: true,
                    }
                },
                messages: {
                    branch_id: {
                        required: json['Hãy chọn chi nhánh']
                    }
                },
            });

            if (!form.valid()) {
                return false;
            }

            $.ajax({
                url: laroute.route('admin.order.submit-apply-branch'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    order_id: id,
                    branch_id: $('#branch_id').val()
                },
                success: function (res) {
                    if (res.error == false) {
                        swal(res.message, "", "success").then(function (result) {
                            if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                window.location.reload();
                            }
                            if (result.value == true) {
                                window.location.reload();
                            }
                        });
                    } else {
                        swal(res.message, "", "error");
                    }
                },
                error: function (res) {
                    var mess_error = '';
                    $.map(res.responseJSON.errors, function (a) {
                        mess_error = mess_error.concat(a + '<br/>');
                    });
                    swal(mess_error, "", "error")
                }
            });
        });
    },
};
var save = {
    submit_edit: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            //Lưu thông tin cho đơn hàng mới
            var continute = true;
            var table_subbmit = [];
            $.each($('#table_add').find(".tr_table"), function () {
                var $tds = $(this).find("input,select");
                var $check_amount = $(this).find("input[name='amount']");
                if ($check_amount.val() < 0) {
                    $('.error-table').text(json['Tổng tiền không hợp lệ']);
                    continute = false;
                }
                $.each($tds, function () {
                    table_subbmit.push($(this).val());
                });
            });

            var table_add = [];
            $.each($('#table_add').find(".table_add"), function () {
                var $tds = $(this).find("input,select");
                var $check_amount = $(this).find("input[name='amount']");
                if ($check_amount.val() < 0) {
                    $('.error-table').text(json['Tổng tiền không hợp lệ']);
                    continute = false;
                }
                $.each($tds, function () {
                    table_add.push($(this).val());
                });
            });
            var check_service_card = [];
            $.each($('#table_add').find("tbody tr"), function () {
                var $check_amount = $(this).find("input[name='object_type']");
                if ($check_amount.val() == 'service_card') {
                    $.each($check_amount, function () {
                        check_service_card.push($(this).val());
                    });
                }
            });

            if (table_subbmit == '' && table_add == '') {
                $('.error-table').text(json['Vui lòng chọn dịch vụ/thẻ dịch vụ/sản phẩm']);
            } else {
                $('.error-table').text('');
                var total_bill = $('input[name="total_bill"]').val();
                var discount_bill = $('input[name="discount_bill"]').val();
                var voucher_bill = $('#voucher_code_bill').val();
                var amount_bill = $('input[name="amount_bill_input"]').val();

                var delivery_active = 0;
                if ($('#delivery_active').is(':checked')) {
                    delivery_active = 1;
                }

                $.ajax({
                    url: laroute.route('admin.order.submit-edit'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        table_edit: table_subbmit,
                        table_add: table_add,
                        total_bill: total_bill,
                        discount_bill: discount_bill,
                        voucher_bill: voucher_bill,
                        amount_bill: amount_bill,
                        order_id: id,
                        order_code: $('#order_code').val(),
                        refer_id: $('#refer_id').val(),
                        delivery_active: delivery_active,
                        customer_id: $('#customer_id').val(),
                        tranport_charge: $('#tranport_charge').val()
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal(json["Lưu đơn hàng thất bại"], "", "error");
                        } else {
                            swal(json["Lưu đơn hàng thành công"], "", "success");
                            window.location.href = laroute.route('admin.order-app');
                        }
                    }
                });
            }
        });
    }
};

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.order-app.list')
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function loyalty(orderId) {
    var flag = true;
    $.ajax({
        url: laroute.route('admin.order.loyalty'),
        method: "POST",
        async: false,
        data: {order_id: orderId},
        success: function (res) {
        }
    });
    return flag;
}

/*
Giảm tiền theo hạng thành viên
 */
function discountCustomer(moneyTotal, pt) {
    var result = 0;
    result = moneyTotal * (pt / 100);
    return formatNumber((result));
}

function discountCustomerInput() {
    $.getJSON(laroute.route('translate'), function (json) {
        //Tổng tiền
        var moneyTotal = $("input[name=total_bill]").val();
        $('#total-money-discount').val();
        //Phần trăm giảm.
        var pt = $('.pt-discount').val();
        var moneyDiscountCustomer = discountCustomer(moneyTotal, pt);
        $('.span_member_level_discount').text(formatNumber(moneyDiscountCustomer));

        $('#member_level_discount').val(moneyDiscountCustomer.replace(new RegExp('\\,', 'g'), ''));

        ////Thành tiền.
        //Tiền giảm theo m
        // Member level.
        var memberLevelDiscount = $('#member_level_discount').val().replace(new RegExp('\\,', 'g'), '');
        //Giảm giá
        var discountBill = $('#discount_bill').val().replace(new RegExp('\\,', 'g'), '');
        var tranportCharge = $('#tranport_charge').val().replace(new RegExp('\\,', 'g'), '');

        var amountBill = Number(moneyTotal) - Number(memberLevelDiscount) - Number(discountBill) + Number(tranportCharge);

        $('.amount_bill').empty();
        $('.amount_bill').append(formatNumber(amountBill.toFixed(decimal_number)) + json['đ']);
        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amountBill.toFixed(decimal_number) + '">');
    });
}

// setInterval(function () {
//     discountCustomerInput();
// }, 800/* in milliseconds 1s */);
