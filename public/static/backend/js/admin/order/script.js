let stt_tr = 0;
var order = {
    loadDefault: function () {
        var type_load = $('.type').find('.active').attr('data-name');

        $.ajax({
            url: laroute.route('admin.order.load-add'),
            data: {
                type: type_load,
                customer_id: $('#customer_id').val()
            },
            method: 'POST',
            dataType: "JSON",
            success: function (res) {
                $('#list-product').empty();
                $('#list-product').append(res);

            }
        });
    },
    click: function (param) {
        $.getJSON(laroute.route('translate'), function (json) {
            if (param != 'member_card') {
                $.ajax({
                    url: laroute.route('admin.order.list-add'),
                    data: {
                        object_type: param,
                    },
                    method: 'POST',
                    dataType: "JSON",
                    success: function (res) {
                        $('#list-product').empty();
                        $('#list-product').append(res);
                    }
                });
            } else {
                var id = $('#customer_id').val();
                $.ajax({
                    url: laroute.route('admin.order.check-card-customer'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(res.data, function (a) {
                            var tpl = $('#list-card-tpl').html();
                            tpl = tpl.replace(/{card_name}/g, a.card_name);
                            tpl = tpl.replace(/{card_code}/g, a.card_code);
                            tpl = tpl.replace(/{id_card}/g, a.customer_service_card_id);
                            if (a.image != null) {
                                tpl = tpl.replace(/{img}/g, a.image);
                            } else {
                                tpl = tpl.replace(/{img}/g, 'uploads/admin/icon/default-placeholder.png');
                            }
                            if (a.count_using != json['Không giới hạn']) {
                                tpl = tpl.replace(/{quantity}/g, json['Còn '] + a.count_using);
                                tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                            } else {
                                tpl = tpl.replace(/{quantity}/g, json['KGH']);
                                tpl = tpl.replace(/{quantity_app}/g, json['Không giới hạn']);
                            }
                            $('.append').append(tpl);
                            $.each($('#table_add tbody tr'), function () {
                                var codeHidden = $(this).find("input[name='object_code']");
                                var value_code = codeHidden.val();
                                var code = a.card_code;
                                if (value_code == code) {
                                    var quantity = $(this).find("input[name='quantity']").val();
                                    var quantity_card = $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val();
                                    $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val(quantity_card - quantity);
                                    $('.card_check_' + a.customer_service_card_id + '').find('.quantity').empty();
                                    $('.card_check_' + a.customer_service_card_id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val() + json[' (lần)']);

                                }
                            });
                        });


                    }
                });
            }
        });

    },
    append_table: function (id, price, type, name, code) {
        $.getJSON(laroute.route('translate'), function (json) {
            var check = true;
            if (check == true) {
                stt_tr++;
                var loc = price.replace(new RegExp('\\,', 'g'), '');
                var tpl = $('#table-tpl').html();
                tpl = tpl.replace(/{stt}/g, stt_tr);
                tpl = tpl.replace(/{name}/g, name);
                tpl = tpl.replace(/{id}/g, id);
                if (type == 'service') {
                    tpl = tpl.replace(/{type}/g, json['Dịch vụ']);
                    tpl = tpl.replace(/{id_type}/g, '1');
                }
                if (type == 'service_card') {
                    tpl = tpl.replace(/{type}/g, json['Thẻ dịch']);
                    tpl = tpl.replace(/{id_type}/g, '2');
                }
                if (type == 'product') {
                    tpl = tpl.replace(/{type}/g, json['Sản phẩm']);
                    tpl = tpl.replace(/{id_type}/g, '3');
                }
                tpl = tpl.replace(/{code}/g, code);
                tpl = tpl.replace(/{type_hidden}/g, type);
                tpl = tpl.replace(/{price}/g, (price));
                tpl = tpl.replace(/{price_hidden}/g, loc);
                tpl = tpl.replace(/{amount}/g, (price));
                tpl = tpl.replace(/{amount_hidden}/g, loc);
                tpl = tpl.replace(/{quantity_hid}/g, 0);
                if (type != 'member_card') {
                    tpl = tpl.replace(/{class}/g, 'abc');
                } else {
                    tpl = tpl.replace(/{class}/g, 'abc_member_card');
                }
                $('#table_add > tbody').append(tpl);

                $('.staff').select2({
                    placeholder: json['Chọn nhân viên'],
                    allowClear: true
                });

                $('.none').css('display', 'block');
                $('.append_bill').empty();
                var tpl_bill = $('#bill-tpl').html();
                var sum = 0;
                $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                    sum += Number($(this).val());
                });

                tpl_bill = tpl_bill.replace(/{total_bill_label}/g, formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                tpl_bill = tpl_bill.replace(/{total_bill}/g, sum.toFixed(decimal_number));
                $('.append_bill').prepend(tpl_bill);
                $('.amount_bill').empty();
                $('.tag_a').remove();
                //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                var discount_bill = $('#discount_bill').val();
                $('.close').remove();
                if (discount_bill != 0) {
                    $('.discount_bill').prepend('<a class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + formatNumber(sum.toFixed(decimal_number)) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                } else {
                    $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + formatNumber(sum.toFixed(decimal_number)) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                }
                var amount_bill = (sum - discount_bill);
                if (amount_bill < 0) {
                    amount_bill = 0;
                }
                $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');

                order.getPromotionGift();
            }
            $(".quantity").TouchSpin({
                initval: 1,
                min: 1,
                buttondown_class: "btn btn-default down btn-ct",
                buttonup_class: "btn btn-default up btn-ct"

            });
            // $('.discount').mask('000,000,000', {reverse: true});
            $('.quantity').change(function () {
                $(this).closest('.tr_table').find('.amount-tr').empty();
                var id = $(this).closest('.tr_table').find('input[name="id"]').val();
                var type = $(this).closest('.tr_table').find('input[name="object_type"]').val();
                var stt = $(this).attr('data-id');
                var id_type = "";
                if (type === "service") {
                    id_type = 1;
                } else if (type === "service_card") {
                    id_type = 2;
                } else {
                    id_type = 3;
                }
                var price = $(this).closest('.tr_table').find('input[name="price"]').val();
                var discount = $(this).closest('.tr_table').find('input[name="discount"]').val();
                var loc = discount.replace(new RegExp('\\,', 'g'), '');
                var quantity = $(this).val();

                var amount = ((price * quantity) - loc);
                $(this).closest('.tr_table').find('.amount-tr').append(formatNumber(amount.toFixed(decimal_number)) + json['đ']);
                $(this).closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount.toFixed(decimal_number) + '">');
                $('.total_bill').empty();
                var sum = 0;
                $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                    sum += Number($(this).val());
                });
                $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                $('.tag_a').remove();
                //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                $('.amount_bill').append();
                $('.amount_bill').empty();
                var discount_bill = $('#discount_bill').val();
                $('.close').remove();
                if (discount_bill != 0) {
                    $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                } else {
                    $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                }
                var amount_bill = (sum - discount_bill);
                if (amount_bill < 0) {
                    amount_bill = 0;
                }
                $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                $(this).closest('.tr_table').find('.abc').remove();
                if (discount != 0) {
                    $(this).closest('.tr_table').find('.discount-tr-' + type + '-' + stt + '').prepend('<a class="abc" href="javascript:void(0)" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount" ></i></a>');
                } else {
                    $(this).closest('.tr_table').find('.discount-tr-' + type + '-' + stt + '').append('<a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus" href="javascript:void(0)" onclick="order.modal_discount(' + amount.toFixed(decimal_number) + ',' + id + ',' + id_type + ',' + stt + ')"><i class="la la-plus icon-sz"></i></a>');
                }

                order.getPromotionGift();
            });
            $('.remove').click(function () {
                var totalAfterDiscountMember = $('.amount_bill_input').val() - $('#member_level_discount').val();

                $(this).closest('.tr_table').remove();
                $('.total_bill').empty();
                var sum = 0;
                $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                    sum += Number($(this).val());
                });
                $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                $('.discount_bill').empty();
                $('.discount_bill').append(0 + json['đ']);
                $('.discount_bill').append('<input type="hidden" id="discount_bill" name="discount_bill" value="' + 0 + '">');
                $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
                $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                $('.amount_bill').empty();
                // var discount_bill = $('#discount_bill').val();
                var amount_bill = sum.toFixed(decimal_number);
                $('.amount_bill').append(formatNumber(totalAfterDiscountMember.toFixed(decimal_number)) + json['đ']);
                $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + totalAfterDiscountMember.toFixed(decimal_number) + '">');

                order.getPromotionGift();
            });
            discountCustomerInput();
        });
    },
    append_table_card: function (id, price, type, name, quantity_using, code, e) {
        $.getJSON(laroute.route('translate'), function (json) {
            if (quantity_using != json['Không giới hạn']) {
                var check = true;
                $.each($('#table_add tbody tr'), function () {
                    let codeHidden = $(this).find("input[name='id']");
                    let value_id = codeHidden.val();
                    let id_card = id;
                    if (value_id == id_card) {
                        check = false;
                        var count_using = $(e).find('.card_check_' + id + '').find('.quantity_card').val();
                        if (count_using > 0) {
                            $(e).find('.card_check_' + id + '').find('.quantity_card').val(count_using - 1);
                            $(e).find('.card_check_' + id + '').find('.quantity').empty();
                            $(e).find('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $(e).find('.card_check_' + id + '').find('.quantity_card').val() + ' (lần)');
                            let quantitySv = codeHidden.parents('tr').find('input[name="quantity"]').val();
                            let numbers = parseInt(quantitySv) + 1;
                            codeHidden.parents('tr').find('input[name="quantity"]').val(numbers);
                        }
                    }
                });
                if (check == true) {
                    var stt = $('#table_add tr').length;
                    var loc = price.replace(/\D+/g, '');
                    var tpl = $('#table-card-tpl').html();
                    tpl = tpl.replace(/{stt}/g, stt);
                    tpl = tpl.replace(/{name}/g, json['Sử dụng thẻ '] + name);
                    tpl = tpl.replace(/{id}/g, id);
                    tpl = tpl.replace(/{type_hidden}/g, type);
                    tpl = tpl.replace(/{price}/g, price);
                    tpl = tpl.replace(/{price_hidden}/g, loc);
                    tpl = tpl.replace(/{amount}/g, price);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{quantity_hid}/g, quantity_using);
                    tpl = tpl.replace(/{code}/g, code);
                    if (type != 'member_card') {
                        tpl = tpl.replace(/{class}/g, 'abc');
                    } else {
                        tpl = tpl.replace(/{class}/g, 'abc_member_card');
                    }
                    $('#table_add > tbody').append(tpl);
                    $('.staff').select2({
                        placeholder: json['Chọn nhân viên'],
                        allowClear: true
                    });
                    if (type == 'member_card') {
                        $('.abc_member_card ').remove();
                    }
                    var count_using = $(e).find('.card_check_' + id + '').find('.quantity_card').val();
                    $(e).find('.card_check_' + id + '').find('.quantity_card').val(count_using - 1);
                    $(e).find('.card_check_' + id + '').find('.quantity').empty();
                    $(e).find('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $(e).find('.card_check_' + id + '').find('.quantity_card').val() + ' (lần)');

                }
                $(".quantity_c").TouchSpin({
                    initval: 1,
                    min: 1,
                    max: quantity_using,
                    buttondown_class: "btn btn-default down btn-ct",
                    buttonup_class: "btn btn-default up btn-ct"
                });
                $('.quantity_c').change(function () {
                    var quan_val = $(this).val();
                    var quan_db = $(this).closest('.tr_table').find("input[name='quantity_hid']").val();
                    var id = $(this).closest('.tr_table').find("input[name='id']").val();
                    $('.card_check_' + id + '').find('.quantity_card').val(quan_db - quan_val);
                    $('.card_check_' + id + '').find('.quantity').empty();
                    $('.card_check_' + id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + id + '').find('.quantity_card').val() + ' (lần)');
                });
                $('.remove_card').click(function () {
                    var quan_db = $(this).closest('.tr_table').find("input[name='quantity_hid']").val();
                    var id = $(this).closest('.tr_table').find("input[name='id']").val();
                    $('.card_check_' + id + '').find('.quantity_card').val(quan_db);
                    $('.card_check_' + id + '').find('.quantity').empty();
                    $('.card_check_' + id + '').find('.quantity').append(json['Còn '] + quan_db + ' (lần)');
                    $(this).closest('.tr_table').remove();
                });
            } else {
                var check = true;
                $.each($('#table_add tbody tr'), function () {
                    let codeHidden = $(this).find("input[name='id']");
                    let value_id = codeHidden.val();
                    let id_card = id;
                    if (value_id == id_card) {
                        check = false;
                        let quantitySv = codeHidden.parents('tr').find('input[name="quantity"]').val();
                        let numbers = parseInt(quantitySv) + 1;
                        codeHidden.parents('tr').find('input[name="quantity"]').val(numbers);
                    }
                });
                if (check == true) {
                    var stt = $('#table_add tr').length;
                    var loc = price.replace(/\D+/g, '');
                    var tpl = $('#table-card-tpl').html();
                    tpl = tpl.replace(/{stt}/g, stt);
                    tpl = tpl.replace(/{name}/g, json['Sử dụng thẻ '] + name);
                    tpl = tpl.replace(/{id}/g, id);
                    tpl = tpl.replace(/{type_hidden}/g, type);
                    tpl = tpl.replace(/{price}/g, price);
                    tpl = tpl.replace(/{price_hidden}/g, loc);
                    tpl = tpl.replace(/{amount}/g, price);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{amount_hidden}/g, loc);
                    tpl = tpl.replace(/{quantity_hid}/g, 0);
                    tpl = tpl.replace(/{code}/g, code);
                    if (type != 'member_card') {
                        tpl = tpl.replace(/{class}/g, 'abc');
                    } else {
                        tpl = tpl.replace(/{class}/g, 'abc_member_card');
                    }
                    $('#table_add > tbody').append(tpl);
                    if (type == 'member_card') {
                        $('.abc_member_card ').remove();
                    }

                }
                $(".quantity_c").TouchSpin({
                    initval: 1,
                    min: 1,
                    buttondown_class: "btn btn-default down btn-ct",
                    buttonup_class: "btn btn-default up btn-ct"
                });

                $('.remove_card').click(function () {
                    $(this).closest('.tr_table').remove();
                });
            }
        });

    },
    search: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var id = $('#customer_id').val();
            var type = $('.type').find('.active').attr('data-name');
            // var type_load = $('.type').find('.active').attr('data-name');

            var search = $('#search').val();
            mApp.block("#m_blockui_1_content", {
                overlayColor: "#000000",
                type: "loader",
                state: "success",
                message: json["Đang tải..."]
            });
            $.ajax({
                url: laroute.route('admin.order.search'),
                data: {
                    type: type,
                    search: search,
                    id: id
                },
                method: 'POST',
                dataType: "JSON",
                success: function (response) {

                    if (response.type != 'member_card') {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            var tpl = $('#list-tpl').html();
                            tpl = tpl.replace(/{name}/g, a.name);
                            tpl = tpl.replace(/{price}/g, a.price);
                            tpl = tpl.replace(/{id}/g, a.id);
                            if (a.avatar != null) {
                                tpl = tpl.replace(/{img}/g, '/' + a.avatar);
                            } else {
                                tpl = tpl.replace(/{img}/g, 'https://quan9tphcm.weebly.com/uploads/6/1/2/0/61203721/9400570_orig.png');
                            }
                            tpl = tpl.replace(/{price_hidden}/g, a.price);
                            tpl = tpl.replace(/{type}/g, a.type);
                            $('.append').append(tpl);
                        });
                    } else {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            var tpl = $('#list-card-tpl').html();
                            tpl = tpl.replace(/{card_name}/g, a.card_name);
                            tpl = tpl.replace(/{card_code}/g, a.card_code);
                            tpl = tpl.replace(/{id_card}/g, a.customer_service_card_id);
                            if (a.image != null) {
                                tpl = tpl.replace(/{img}/g, '/uploads/' + a.image);
                            } else {
                                tpl = tpl.replace(/{img}/g, 'https://secure.bankofamerica.com/content/images/ContextualSiteGraphics/Instructional/en_US/Banner_Credit_Card_Activation.png');
                            }
                            if (a.count_using != json['Không giới hạn']) {
                                tpl = tpl.replace(/{quantity}/g, json['Còn '] + a.count_using);
                                tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                            } else {
                                tpl = tpl.replace(/{quantity}/g, json['KGH']);
                                tpl = tpl.replace(/{quantity_app}/g, json['Không giới hạn']);
                            }
                            $('.append').append(tpl);
                            $.each($('#table_add tbody tr'), function () {
                                var codeHidden = $(this).find("input[name='object_code']");
                                var value_code = codeHidden.val();
                                var code = a.card_code;
                                if (value_code == code) {
                                    var quantity = $(this).find("input[name='quantity']").val();
                                    var quantity_card = $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val();
                                    if (quantity_card != json['Không giới hạn']) {
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val(quantity_card - quantity);
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').empty();
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val() + ' (lần)');
                                    }
                                }
                            });
                        });
                    }

                }
            })
        });
    },
    modal_discount: function (amount, id, id_type, stt) {
        $('#modal-discount').modal('show');

        $('#amount-tb').val(amount);
        $('#discount-modal').val(0);
        $('#discount-code-modal').val('');
        $('.error-discount1').text('');
        $('.error-discount').text('');
        $('.error_discount_code').text('');
        $('.error_discount_expired').text('');
        $('.error_discount_not_using').text('');
        $('.error_discount_amount_error').text('');
        $('.error_discount_null').text('');
        $('.btn-click').empty();
        // $('.btn-click').append('<button type="button" onclick="order.discount(' + id + ',' + id_type + ')" class="btn btn-primary">Áp dụng</button>');
        // $('.btn-click').append('<input type="button" onclick="order.close_modal_discount()" class="btn btn-default" value="Hủy">');
        var tpl = $('#button-discount-tpl').html();
        tpl = tpl.replace(/{id}/g, id);
        tpl = tpl.replace(/{id_type}/g, id_type);
        tpl = tpl.replace(/{stt}/g, stt);
        $('.btn-click').append(tpl);
        $('#live-tag').click(function () {
            $('#discount-code-modal').val('');
            $('.error_discount_code').text('');
            $('.error_discount_expired').text('');
            $('.error_discount_not_using').text('');
            $('.error_discount_amount_error').text('');
            $('.error_discount_null').text('');
        });
        $('#code-tag').click(function () {
            $('#discount-modal').val(0);
            $('.error-discount1').text('');
            $('.error-discount').text('');
        });
    },
    close_modal_discount: function () {
        $('#modal-discount').modal('hide');
    },
    modal_customer: function () {
        $('#modal-customer').modal('show');
        // $('#form-customer')[0].reset();
        // $('#customer-search').val(null).trigger("change");
        // $('#customer_id_modal').val('');
        // $('#full_name').val('').removeAttr('disabled', 'disabled');
        // $('#phone').val('').removeAttr('disabled', 'disabled');
        // $('#customer_avatar').val('');
        // $('#member_money').val('');
        $('.error_name').text('');
        $('.error_phone').text('');
        $('.name').attr('class', 'form-group m-form__group col-lg-6 name');
        $('.phone').attr('class', 'form-group m-form__group col-lg-6 phone');
    },
    modal_customer_click: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var customer_id = $('#customer_id').val();
            var id = $('#customer_id_modal').val();
            var name = $('#full_name').val();
            var phone = $('#phone').val();
            var image = $('#customer_avatar').val();
            var money = $('#member_money').val();
            var memberLevel = 'Chưa có';
            var icon = '';

            if (id != "") {
                $.ajax({
                    url: laroute.route('admin.customer.get-info-customer-detail'),
                    method: "POST",
                    data: {id: id},
                    async: false,
                    success: function (res) {
                        if (res.member_level_name != null) {
                            memberLevel = res.member_level_name;
                        } else {
                            memberLevel = 'Thành Viên';
                        }
                        if (res.member_level_id == 1) {
                            icon = '<i class="fa flaticon-presentation icon_color"></i>';
                        } else if (res.member_level_id == 2) {
                            icon = '<i class="fa flaticon-confetti icon_color"></i>';
                        } else if (res.member_level_id == 3) {
                            icon = '<i class="fa flaticon-medal icon_color"></i>';
                        } else if (res.member_level_id == 4) {
                            icon = '<i class="fa flaticon-customer icon_color"></i>';
                        }

                        //% giám giá theo hạng thành viên.
                        if (res.member_level_discount != null) {
                            $('.pt-discount').val(res.member_level_discount);
                            // var discountMember = discountCustomer($('.amount_bill_input').val(), res.member_level_discount);
                            // console.log(discountMember);
                            // $('.span_member_level_discount').text(discountMember);
                            // var amountBill = formatNumber($('.amount_bill_input').val() - discountMember.replace(/\D+/g, ''));
                            // $('.amount_bill').empty();
                            // $('.amount_bill').append(formatNumber(amountBill) + json['đ']);
                            // $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amountBill + '">');
                            // $('#member_level_discount').val(discountMember.replace(/\D+/g, ''));
                        } else {
                            $('.pt-discount').val(0);
                        }
                    }
                });

                $('.customer').empty();
                var tpl = $('#customer-tpl').html();
                tpl = tpl.replace(/{full_name}/g, name);
                tpl = tpl.replace(/{phone}/g, phone);
                tpl = tpl.replace(/{member_level_name}/g, json[memberLevel]);
                tpl = tpl.replace(/{icon}/g, icon);
                if (image != '') {
                    tpl = tpl.replace(/{img}/g, image);
                } else {
                    tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/person.png');
                }
                if (money != "") {
                    tpl = tpl.replace(/{money}/g, formatNumber(money));
                    $('#money_customer').val(money);
                } else {
                    tpl = tpl.replace(/{money}/g, 0);
                    $('#money_customer').val(0);
                }

                $('.customer').append(tpl);
                $('#customer_id').val(id);
                $.ajax({
                    url: laroute.route('admin.order.check-card-customer'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        $('.tab_member_card').remove();
                        if (res.number_card > 0) {
                            var tpl = $('#tab-card-tpl').html();
                            $('.tab-list').append(tpl);
                        }

                    }
                });

                $('.cus_haunt').remove();
                var tpl = $('#button-tpl').html();
                $('.button_tool').prepend(tpl);
                $('#modal-customer').modal('hide');
            } else {
                var check_name = true;
                var check_phone = true;
                var check_phone_length = true;
                var name = $('#full_name').val();
                var phone = $('#phone').val();

                if (name != "") {
                    check_name = true;
                    $('.error_name').text('');
                } else {
                    $('.error_name').text(json['Hãy nhập tên khách hàng']);
                    check_name = false
                }
                if (phone == "") {
                    $('.error_phone').text(json['Hãy nhập số điện thoại']);
                    check_phone = false;
                } else if (phone.length < 10) {
                    $('.error_phone').text(json['Số điện thoại tối thiểu 10 số']);
                    check_phone_length = false;
                } else if (phone.length > 11) {
                    $('.error_phone').text(json['Số điện thoại tối đa 11 số']);
                    check_phone_length = false;
                } else if (phone.length >= 10 && phone.length <= 11) {
                    check_phone_length = true;
                    $('.error_phone').text('');
                } else {
                    check_phone = true;
                    $('.error_phone').text(json['Số điện thoại không hợp lệ']);
                    check_phone_length = false;
                }
                if (check_name == true && check_phone == true && check_phone_length == true) {
                    $.ajax({
                        url: laroute.route('admin.order.add-customer'),
                        data: {
                            full_name: name,
                            phone: phone
                        },
                        method: 'POST',
                        dataType: "JSON",
                        success: function (response) {
                            if (response.error === 1) {
                                $('.error_phone').text('Số điện thoại đã tồn tại');
                            } else {
                                $('#form-customer')[0].reset();
                                $('.error_phone').text('');
                                //Append customer

                                $('.customer').empty();
                                var tpl = $('#customer-tpl').html();
                                tpl = tpl.replace(/{full_name}/g, response.customer.full_name);
                                tpl = tpl.replace(/{phone}/g, response.customer.phone);
                                tpl = tpl.replace(/{member_level_name}/g, 'Chưa có');
                                tpl = tpl.replace(/{icon}/g, '');
                                if (image != '') {
                                    tpl = tpl.replace(/{img}/g, response.customer.image);
                                } else {
                                    tpl = tpl.replace(/{img}/g, 'uploads/admin/icon/person.png');
                                }
                                if (money != "") {
                                    tpl = tpl.replace(/{money}/g, formatNumber(0));
                                    $('#money_customer').val(0);
                                } else {
                                    tpl = tpl.replace(/{money}/g, 0);
                                    $('#money_customer').val(0);
                                }
                                $('.customer').append(tpl);
                                $('#customer_id').val(response.customer.id);
                                swal(json["Thêm khách hàng thành công"], "", "success");
                                $('#modal-customer').modal('hide');
                            }
                        }
                    });
                }
            }

            if (customer_id != id) {
                $.each($('#table_add tbody tr'), function () {
                    var codeHidden = $(this).find("input[name='object_type']");
                    var value_code = codeHidden.val();
                    if (value_code == 'member_card') {
                        $(this).closest('.tr_table').remove();
                    }
                });
                $("#load").trigger("click");
            }
            //Xóa giảm giá tổng bill
            ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
            order.close_discount_bill($('input[name=total_bill]').val());
            order.loadDefault();
            order.getPromotionGift();
        });
    },
    discount: function (id, id_type, stt) {
        $.getJSON(laroute.route('translate'), function (json) {
            var amount = $('#amount-tb').val();
            var discount = $('#discount-modal').val().replace(new RegExp('\\,', 'g'), '');
            var type_discount = $("input[name='type-discount']:checked").val();
            var voucher_code = $('#discount-code-modal').val();
            var type_class = "";
            var amount_bill = $('input[name="total_bill"]').val();
            var total_using_voucher = 0;

            $("input[name='voucher_code']").each(function (val) {
                var value = $(this).val();
                if (value === voucher_code.trim()) {
                    total_using_voucher++;
                }
            });

            if (id_type == 1) {
                type_class = "service";
            } else if (id_type == 2) {
                type_class = "service_card";
            } else {
                type_class = "product";
            }
            $.ajax({
                url: laroute.route('admin.order.add-discount'),
                data: {
                    amount: amount,
                    discount: discount,
                    type: type_discount,
                    voucher_code: voucher_code,
                    type_order: type_class,
                    id_order: id,
                    amount_bill: amount_bill,
                    total_using_voucher: total_using_voucher
                },
                async: false,
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.error_money === 1) {
                        $('.error-discount1').text(json['Số tiền giảm giá không hợp lệ']);
                    } else {
                        $('.error-discount1').text('');
                    }
                    if (response.error_money === 0) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(discount) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + discount + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="" >');
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);

                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc"  href="javascript:void(0)" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                        //Xóa giảm giá tổng bill
                        ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
                        order.close_discount_bill($('input[name=total_bill]').val());
                    }
                    if (response.error_percent === 1) {
                        $('.error-discount').text(json['Số tiền giảm giá không hợp lệ']);
                    } else {
                        $('.error-discount').text('');
                    }
                    if (response.error_percent === 0) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(response.discount_percent) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + response.discount_percent + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="" >');
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);

                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" href="javascript:void(0)" style="color:red" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                        //Xóa giảm giá tổng bill
                        ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
                        order.close_discount_bill($('input[name=total_bill]').val());
                    }
                    if (response.voucher_null === 1) {
                        $('.error_discount_null').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_discount_null').text('');
                    }
                    if (response.voucher_not_exist == 1) {
                        $('.error_discount_code').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_discount_code').text('');
                    }
                    if (response.voucher_expired == 1) {
                        $('.error_discount_expired').text(json['Mã giảm giá hết hạn sử dụng']);
                    } else {
                        $('.error_discount_expired').text('');
                    }
                    if (response.voucher_not_using == 1) {
                        $('.error_discount_not_using').text(json['Mã giảm giá đã hết số lần sử dụng']);
                    } else {
                        $('.error_discount_not_using').text('');
                    }
                    if (response.voucher_amount_error == 1) {
                        $('.error_discount_amount_error').text(json['Tổng tiền không đủ sử dụng mã giảm giá']);
                    } else {
                        $('.error_discount_amount_error').text('');
                    }
                    if (response.branch_not == 1) {
                        $('.branch_not').text(json['Mã giảm giá không sử dụng cho chi nhánh này']);
                    } else {
                        $('.branch_not').text('');
                    }
                    if (response.voucher_success == 1) {
                        $('#modal-discount').modal('hide');
                        $(".discount-tr-" + type_class + "-" + stt + "").empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend(formatNumber(response.discount_voucher) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" class="form-control discount" value="' + response.discount_voucher + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="' + response.voucher_name + '" >')
                        var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
                        var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
                        var discount_new = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="discount"]').val();
                        var amount_new = (price * quantity - discount_new);
                        if (amount_new < 0) {
                            amount_new = 0;
                        }
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
                        $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
                        $(".discount-tr-" + type_class + "-" + stt + "").prepend('<a class="abc" style="color:red" href="javascript:void(0)" onclick="order.close_amount(' + id + ',' + id_type + ',' + stt + ')"><i class="la la-close cl_amount"></i></a>');
                        $('.total_bill').empty();
                        var sum = 0;
                        $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                            sum += Number($(this).val());
                        });
                        $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
                        $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
                        $('.tag_a').remove();
                        //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
                        $('.amount_bill').append();
                        $('.amount_bill').empty();
                        var discount_bill = $('#discount_bill').val();
                        $('.close').remove();
                        if (discount_bill != 0) {
                            $('.discount_bill').prepend('<a  class="close" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
                        } else {
                            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
                        }
                        var amount_bill = (sum - discount_bill);
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
                        //Xóa giảm giá tổng bill
                        ////Thêm giảm giá từng dòng thì xóa giảm giá tổng bill.
                        order.close_discount_bill($('input[name=total_bill]').val());
                    }
                }
            });
        });
        discountCustomerInput();
    },
    close_amount: function (id, id_type, stt) {
        $.getJSON(laroute.route('translate'), function (json) {
            var type_class = "";
            if (id_type == 1) {
                type_class = "service";
            } else if (id_type == 2) {
                type_class = "service_card";
            } else {
                type_class = "product";
            }
            $(".discount-tr-" + type_class + "-" + stt + "").empty();
            var price = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="price"]').val();
            var quantity = $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('input[name="quantity"]').val();
            var discount_new = 0;
            var amount_new = (price * quantity - discount_new);
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').empty();
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append(formatNumber(amount_new.toFixed(decimal_number)) + json['đ']);
            $(".discount-tr-" + type_class + "-" + stt + "").closest('.tr_table').find('.amount-tr').append('<input type="hidden" name="amount" class="form-control amount" value="' + amount_new.toFixed(decimal_number) + '">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="discount" value="0">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<input type="hidden" name="voucher_code" value="">');
            $(".discount-tr-" + type_class + "-" + stt + "").append('<a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus" href="javascript:void(0)" onclick="order.modal_discount(' + amount_new.toFixed(decimal_number) + ',' + id + ',' + id_type + ',' + stt + ')"><i class="la la-plus icon-sz"></i></a>');
            $('.total_bill').empty();
            var sum = 0;
            $.each($('#table_add > tbody').find('input[name="amount"]'), function () {
                sum += Number($(this).val());
            });
            $('.total_bill').append(formatNumber(sum.toFixed(decimal_number)) + json['đ']);
            $('.total_bill').append(' <input type="hidden" name="total_bill" class="form-control total_bill" value="' + sum.toFixed(decimal_number) + '">');
            $('.tag_a').remove();
            //$('.discount_bill').append('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum + ')" class="tag_a m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary m--margin-left-50"><i class="la la-plus"></i></a>');
            $('.amount_bill').append();
            $('.amount_bill').empty();
            var discount_bill = $('#discount_bill').val();
            $('.close').remove();
            if (discount_bill != 0) {
                $('.discount_bill').prepend('<a class="tag_a" href="javascript:void(0)" onclick="order.close_discount_bill(' + sum.toFixed(decimal_number) + ')"><i class="la la-close cl_amount_bill"></i></a>');
            } else {
                $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + sum.toFixed(decimal_number) + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            }
            var amount_bill = (sum - discount_bill);
            $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amount_bill.toFixed(decimal_number) + '">');
        });
        discountCustomerInput();
    },
    modal_discount_bill: function (amount_bill) {
        $('#modal-discount-bill').modal('show');
        $('#amount-bill').val(amount_bill);
        $('#discount-bill').val(0);
        $('.branch_not').text('');
        $('#discount-code-bill-modal').val('');
        $('.error-discount-bill').text('');
        $('.error-discount-bill-percent').text('');
        $('.error_bill_null').text('');
        $('.error_bill_expired').text('');
        $('.error_bill_amount').text('');
        $('.error_bill_not_using').text('');
        $('#live-tag-bill').click(function () {
            $('#discount-code-bill-modal').val('');
            $('.error_bill_null').text('');
            $('.error_bill_expired').text('');
            $('.error_bill_amount').text('');
            $('.error_bill_not_using').text('');
        });
        $('#code-tag-bill').click(function () {
            $('#discount-bill').val(0);
            $('.error-discount-bill').text('');
            $('.error-discount-bill-percent').text('');
        });
        $('.btn-click-bill').empty();
        // $('.btn-click-bill').append('<button type="button" onclick="order.modal_discount_bill_click()" class="btn btn-primary">Áp dụng</button>');
        // $('.btn-click-bill').append('<button type="button" onclick="order.close_modal_discount_bill()" class="btn btn-default">Hủy</button>');
        var tpl = $('#button-discount-bill-tpl').html();
        $('.btn-click-bill').append(tpl);
    },
    close_modal_discount_bill: function () {
        $('#modal-discount-bill').modal('hide');
    },
    modal_discount_bill_click: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            // var total_bill = $('#amount-bill').val() - $('#member_level_discount').val();
            var total_bill = $("input[name=total_bill]").val() - $('#member_level_discount').val();
            var discount_bill = $('#discount-bill').val().replace(new RegExp('\\,', 'g'), '');
            var type_discount_bill = $("input[name='type-discount-bill']:checked").val();
            var voucher_code_bill = $('#discount-code-bill-modal').val();
            $.ajax({
                url: laroute.route('admin.order.add-discount-bill'),
                data: {
                    total_bill: total_bill,
                    discount_bill: discount_bill,
                    type_discount_bill: type_discount_bill,
                    voucher_code_bill: voucher_code_bill,
                },
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.error_money_bill == 1) {
                        $('.error-discount-bill').text(json['Số tiền không hợp lệ']);
                    } else {
                        $('.error-discount-bill').text('');
                    }
                    if (response.error_percent_bill == 1) {
                        $('.error-discount-bill-percent').text(json['Số tiền không hợp lệ']);
                    } else {
                        $('.error-discount-bill-percent').text('');
                    }
                    if (response.error_money_bill == 0) {
                        $('#modal-discount-bill').modal('hide');
                        $('.discount_bill').empty();
                        var tpl = $('#close-discount-bill').html();
                        tpl = tpl.replace(/{discount}/g, formatNumber(response.discount_bill));
                        tpl = tpl.replace(/{discount_hidden}/g, response.discount_bill);
                        tpl = tpl.replace(/{close_discount_hidden}/g, total_bill);
                        tpl = tpl.replace(/{code_bill}/g, '');
                        $('.discount_bill').append(tpl);
                        var amount_bill = (total_bill - response.discount_bill);
                        $('.amount_bill').empty();
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + amount_bill.toFixed(decimal_number) + '>');
                        $('.amount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
                    }
                    if (response.error_percent_bill == 0) {
                        $('#modal-discount-bill').modal('hide');
                        $('.discount_bill').empty();
                        var tpl = $('#close-discount-bill').html();
                        tpl = tpl.replace(/{discount}/g, formatNumber(response.discount_bill.replace(new RegExp('\\,', 'g'), '')));
                        tpl = tpl.replace(/{discount_hidden}/g, response.discount_bill.replace(new RegExp('\\,', 'g'), ''));
                        tpl = tpl.replace(/{close_discount_hidden}/g, total_bill);
                        tpl = tpl.replace(/{code_bill}/g, '');
                        $('.discount_bill').append(tpl);
                        var amount_bill = (total_bill - response.discount_bill.replace(new RegExp('\\,', 'g'), ''));

                        $('.amount_bill').empty();
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + amount_bill.toFixed(decimal_number) + '>');

                    }

                    if (response.voucher_bill_null == 1) {
                        $('.error_bill_null').text(json['Mã giảm giá không tồn tại']);
                    } else {
                        $('.error_bill_null').text('');
                    }
                    if (response.voucher_bill_expired == 1) {
                        $('.error_bill_expired').text(json['Mã giảm giá hết hạn sử dụng']);
                    } else {
                        $('.error_bill_expired').text('');
                    }
                    if (response.voucher_amount_bill_error == 1) {
                        $('.error_bill_amount').text(json['Tổng tiền không đủ để sử dụng mã giảm giá']);
                    } else {
                        $('.error_bill_amount').text('');
                    }
                    if (response.voucher_bill_not_using == 1) {
                        $('.error_bill_not_using').text(json['Mã giảm giá đã hết số lần sử dụng']);
                    } else {
                        $('.error_bill_not_using').text('');
                    }
                    if (response.branch_not == 1) {
                        $('.branch_not').text(json['Mã giảm giá không sử dụng cho chi nhánh này']);
                    } else {
                        $('.branch_not').text('');
                    }
                    if (response.voucher_success_bill == 1) {
                        $('#modal-discount-bill').modal('hide');
                        $('.discount_bill').empty();
                        var tpl = $('#close-discount-bill').html();
                        tpl = tpl.replace(/{discount}/g, formatNumber(response.discount_voucher_bill));
                        tpl = tpl.replace(/{discount_hidden}/g, response.discount_voucher_bill);
                        tpl = tpl.replace(/{close_discount_hidden}/g, total_bill);
                        tpl = tpl.replace(/{code_bill}/g, response.voucher_name_bill);
                        $('.discount_bill').append(tpl);
                        var amount_bill = (total_bill - response.discount_voucher_bill);
                        if (amount_bill < 0) {
                            amount_bill = 0;
                        }
                        $('.amount_bill').empty();
                        $('.amount_bill').append(formatNumber(amount_bill.toFixed(decimal_number)) + json['đ']);
                        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + amount_bill.toFixed(decimal_number) + '>');
                    }
                }
            })
        });
        discountCustomerInput();
    },
    close_discount_bill: function (total_bill) {
        $.getJSON(laroute.route('translate'), function (json) {
            // var total_bill = $('#amount-bill').val();
            $('.discount_bill').empty();
            $('.discount_bill').append(0 + json['đ']);
            $('.discount_bill').append('<input type="hidden" name="discount_bill" id="discount_bill" value=' + 0 + '>')
            $('.discount_bill').append('<input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">');
            $('.discount_bill').prepend('<a href="javascript:void(0)" onclick="order.modal_discount_bill(' + total_bill + ')" class="tag_a"><i class="fa fa-plus-circle icon-sz m--margin-right-5" style="color: #4fc4cb;"></i></a>');
            $('.amount_bill').empty();
            $('.amount_bill').append(formatNumber(total_bill.toFixed(decimal_number)) + json['đ']);
            $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value=' + total_bill.toFixed(decimal_number) + '>');
        });
        discountCustomerInput();
    },
    receipt: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#receipt_amount').val(formatNumber($('input[name="amount_bill_input"]').val()));
            $('#amount_all').val(formatNumber($('input[name="amount_bill_input"]').val()));
            $('#amount_rest').val(0);
            $('#amount_return').val(0);
            //span receipt
            $('.cl_receipt_amount').text(formatNumber($('input[name="amount_bill_input"]').val()));
            $('.cl_amount_all').text(formatNumber($('input[name="amount_bill_input"]').val()));
            $('.cl_amount_rest').text(0);
            $('.cl_amount_return').text(0);
            //Load sẵn hình thức thanh toán = tiền mặt
            $('#receipt_type').val('cash').trigger('change');
            $('.cash').empty();
            var tpl = $('#type-receipt-tpl').html();
            tpl = tpl.replace(/{label}/g, json['Tiền mặt']);
            tpl = tpl.replace(/{money}/g, '*');
            tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_cash');
            tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_cash');
            $('.cash').append(tpl);
            $('#amount_receipt_cash').val(formatNumber($('input[name="amount_bill_input"]').val()));

            new AutoNumeric.multiple('#amount_receipt_cash', {
                currencySymbol : '',
                decimalCharacter : '.',
                digitGroupSeparator : ',',
                decimalPlaces: 2
            });

            $('.error_amount_small').text('');
            $('.error_amount_large').text('');
            $('.error_amount_null').text('');
            $('.not_id_table').text('');
            $('.error_card_pired_date').text('');
            $('.type_error').text('');
            $('.error_count').text('');
            $('.card_null_sv').text('');
            $('.error_account_money').text('');
            $('.error_account_money_null').text('');
            $('.money_owed_zero').text('');
            $('.money_large_moneybill').text('');
            // $("#receipt_type").val('').trigger('change');
            // $('.cash').empty();
            $('.transfer').empty();
            $('.visa').empty();
            $('.member_money').empty();
            $('.price_member_card').empty();
            $('#note').val('');
            $('.checkbox_active_card').empty();
            $('.btn_receipt').empty();
            $('.btn_receipt').append('<button type="button" class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn" id="receipt-close-btn"><span><i class="la la-arrow-left"></i><span>' + json['HỦY'] + '</span></span></button>');
            $('.btn_receipt').append('<button type="button" class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10" id="receipt-print-btn"><span>' + json['THANH TOÁN & IN HÓA ĐƠN'] + '</span></button>');
            $('.btn_receipt').append('<button type="button" class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10" id="receipt-btn"><span>' + json['THANH TOÁN'] + '</span></button>');
            var continute = true;
            var table_subbmit = [];
            $.each($('#table_add').find(".tr_table"), function () {
                var $tds = $(this).find("input,select");
                var $check_amount = $(this).find("input[name='amount']");
                if ($check_amount.val() < 0) {
                    $('.error-table').text(json['Tổng tiền không hợp lệ']);
                    continute = false;
                }
                $.each($tds, function () {
                    table_subbmit.push($(this).val());
                });
            });
            var check_service_card = [];
            $.each($('#table_add').find(".tr_table"), function () {
                var $check_amount = $(this).find("input[name='object_type']");
                if ($check_amount.val() == 'service_card') {
                    $.each($check_amount, function () {
                        check_service_card.push($(this).val());
                    });
                }
            });

            if (table_subbmit == '') {
                $('.error-table').text(json['Vui lòng chọn dịch vụ/thẻ dịch vụ/sản phẩm']);
            } else {
                if ($('#customer_id').val() != 1) {

                    if ($('#money_customer').val() > 0) {
                        $('.member_money_op').remove();
                        $('#receipt_type').append('<option value="member_money" class="member_money_op">Tài khoản thành viên</option>');
                    } else {
                        $('.member_money_op').remove();
                    }

                    if (check_service_card.length > 0) {
                        var tpl = $('#active-tpl').html();
                        $('.checkbox_active_card').append(tpl);
                        $("#check_active").change(function () {
                            if ($(this).is(":checked")) {
                                $(this).val(1);
                            } else if ($(this).not(":checked")) {
                                $(this).val(0);
                            }
                        });

                    } else {
                        $('.checkbox_active_card').empty();
                    }
                } else {
                    $('.member_money').empty();
                    $('.checkbox_active_card').empty();
                }

                $('#modal-receipt').modal('show');
                //Submit thanh toán
                $('#receipt-btn').click(function () {
                    mApp.block("#load", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var check = true;
                    var customer_id = $('#customer_id').val();
                    var voucher_bill = $('#voucher_code_bill').val();
                    var total_bill = $('input[name="total_bill"]').val();
                    var discount_bill = $('input[name="discount_bill"]').val();
                    var amount_bill = $('#receipt_amount').val();
                    var amount_return = $('#amount_return').val();
                    var receipt_type = $('#receipt_type').val();
                    var amount_receipt_cash = $('#amount_receipt_cash').val();
                    var amount_receipt_atm = $('#amount_receipt_atm').val();
                    var amount_receipt_visa = $('#amount_receipt_visa').val();
                    var amount_receipt_money = $('#amount_receipt_money').val();
                    var id_amount_card = $('#service_card_search').val();
                    var amount_card = $('#service_cash').val();
                    var card_code = $('#service_card_search').val();
                    var note = $('#note').val();
                    var member_money = $('#money_customer').val();
                    var discount_member = $('#member_level_discount').val();

                    if (receipt_type == '') {
                        $('.error_type').text(json['Hãy chọn hình thức thanh toán']);
                        check = false;
                    } else {
                        $('.error_type').text('');
                        check = true;
                    }
                    if (check == true) {
                        $.ajax({
                            url: laroute.route('admin.order.submitAddReceipt'),
                            data: {
                                customer_id: customer_id,
                                member_money: member_money,
                                total_bill: total_bill,
                                discount_bill: discount_bill,
                                amount_bill: amount_bill,
                                amount_return: amount_return,
                                table_add: table_subbmit,
                                voucher_bill: voucher_bill,
                                receipt_type: receipt_type,
                                amount_receipt_cash: amount_receipt_cash,
                                amount_receipt_atm: amount_receipt_atm,
                                amount_receipt_visa: amount_receipt_visa,
                                amount_receipt_money: amount_receipt_money,
                                id_amount_card: id_amount_card,
                                amount_card: amount_card,
                                card_code: card_code,
                                note: note,
                                check_active: $('#check_active').val(),
                                refer_id: $('#refer_id').val(),
                                discount_member: discount_member
                            },
                            method: 'POST',
                            dataType: "JSON",
                            success: function (response) {
                                mApp.unblock("#load");
                                if (response.amount_null == 1) {
                                    $('.error_amount_null').text(response.message);
                                } else {
                                    $('.error_amount_null').text('');
                                }
                                if (response.amount_detail_large == 1) {
                                    $('.error_amount_large').text(response.message);
                                } else {
                                    $('.error_amount_large').text('');
                                }
                                if (response.amount_detail_small == 1) {
                                    $('.error_amount_small').text(response.message);
                                } else {
                                    $('.error_amount_small').text('');
                                }
                                if (response.not_id_table == 1) {
                                    $('.not_id_table').text(json['Không có dịch vụ để sử dụng thẻ']);
                                } else {
                                    $('.not_id_table').text('');
                                }
                                if (response.error_account_money == 1) {
                                    $('.error_account_money').text(json['Tiền trong tài khoản không đủ']);
                                } else {
                                    $('.error_account_money').text('');
                                }
                                if (response.error_account_money_null == 1) {
                                    $('.error_account_money_null').text(json['Tiền trong tài khoản không đủ']);
                                } else {
                                    $('.error_account_money_null').text('');
                                }
                                if (response.money_owed_zero == 1) {
                                    $('.money_owed_zero').text(json['Tiền tài khoản không hợp lệ']);
                                } else {
                                    $('.money_owed_zero').text('');
                                }
                                if (response.money_large_moneybill == 1) {
                                    $('.money_large_moneybill').text(json['Tiền tài khoản không hợp lệ']);
                                } else {
                                    $('.money_large_moneybill').text('');
                                }
                                if (response.error == true) {
                                    $('#modal-receipt').modal('hide');
                                    if (response.print_card.length > 0) {
                                        mApp.block(".load_ajax", {
                                            overlayColor: "#000000",
                                            type: "loader",
                                            state: "success",
                                            message: json["Đang tải..."]
                                        });
                                        $.ajax({
                                            url: laroute.route('admin.order.render-card'),
                                            dataType: 'JSON',
                                            method: 'POST',
                                            data: {
                                                list_card: response.print_card
                                            },
                                            async: false,
                                            success: function (res) {
                                                mApp.unblock(".load");
                                                $('.list-card').empty();
                                                $('.list-card').append(res);
                                                $('#modal-print').modal('show');

                                                var list_code = [];
                                                $.each($('.list-card').find(".toimg"), function () {
                                                    var $tds = $(this).find("input[name='code']");
                                                    $.each($tds, function () {
                                                        list_code.push($(this).val());
                                                    });
                                                });
                                                for (let i = 1; i <= list_code.length; i++) {
                                                    html2canvas(document.querySelector("#check-selector-" + i + "")).then(canvas => {
                                                        $('.canvas').append(canvas);
                                                        var canvas = $(".canvas canvas");
                                                        var context = canvas.get(0).getContext("2d");
                                                        var dataURL = canvas.get(0).toDataURL();
                                                        var img = $("<img class='img-canvas' id=" + i + "></img>");
                                                        img.attr("src", dataURL);
                                                        canvas.replaceWith(img);
                                                    });
                                                }

                                            }
                                        });

                                        if (response.isSMS == 0) {
                                            $(".btn-send-sms").remove();
                                        }
                                        $('#modal-print').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        });
                                        // let flagLoyalty = loyalty(response.orderId);
                                    } else {
                                        swal(json["Thanh toán đơn hàng thành công"], "", "success");
                                        // let flagLoyalty = loyalty(response.orderId);
                                        // if (flagLoyalty == true) {
                                        window.location = laroute.route('admin.order');
                                        // }
                                    }
                                    $('.hiddenOrderIdss').val(response.orderId);
                                }
                            }
                        })
                    }
                });
                //Submit thánh toán và in hóa đơn
                $('#receipt-print-btn').click(function () {
                    mApp.block("#load", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var check = true;
                    var customer_id = $('#customer_id').val();
                    var voucher_bill = $('#voucher_code_bill').val();
                    var total_bill = $('input[name="total_bill"]').val();
                    var discount_bill = $('input[name="discount_bill"]').val();
                    var amount_bill = $('#receipt_amount').val();
                    var amount_return = $('#amount_return').val();
                    var receipt_type = $('#receipt_type').val();
                    var amount_receipt_cash = $('#amount_receipt_cash').val();
                    var amount_receipt_atm = $('#amount_receipt_atm').val();
                    var amount_receipt_visa = $('#amount_receipt_visa').val();
                    var amount_receipt_money = $('#amount_receipt_money').val();
                    var id_amount_card = $('#service_card_search').val();
                    var amount_card = $('#service_cash').val();
                    var card_code = $('#service_card_search').val();
                    var note = $('#note').val();
                    var member_money = $('#money_customer').val();
                    var discount_member = $('#member_level_discount').val();
                    if (receipt_type == '') {
                        $('.error_type').text(json['Hãy chọn hình thức thanh toán']);
                        check = false;
                    } else {
                        $('.error_type').text('');
                        check = true;
                    }
                    if (check == true) {
                        $.ajax({
                            url: laroute.route('admin.order.submitAddReceipt'),
                            data: {
                                customer_id: customer_id,
                                member_money: member_money,
                                total_bill: total_bill,
                                discount_bill: discount_bill,
                                amount_bill: amount_bill,
                                amount_return: amount_return,
                                table_add: table_subbmit,
                                voucher_bill: voucher_bill,
                                receipt_type: receipt_type,
                                amount_receipt_cash: amount_receipt_cash,
                                amount_receipt_atm: amount_receipt_atm,
                                amount_receipt_visa: amount_receipt_visa,
                                amount_receipt_money: amount_receipt_money,
                                id_amount_card: id_amount_card,
                                amount_card: amount_card,
                                card_code: card_code,
                                note: note,
                                check_active: $('#check_active').val(),
                                refer_id: $('#refer_id').val(),
                                discount_member: discount_member
                            },
                            method: 'POST',
                            dataType: "JSON",
                            success: function (response) {
                                mApp.unblock("#load");
                                if (response.amount_null == 1) {
                                    $('.error_amount_null').text(response.message);
                                } else {
                                    $('.error_amount_null').text('');
                                }
                                if (response.amount_detail_large == 1) {
                                    $('.error_amount_large').text(response.message);
                                } else {
                                    $('.error_amount_large').text('');
                                }
                                if (response.amount_detail_small == 1) {
                                    $('.error_amount_small').text(response.message);
                                } else {
                                    $('.error_amount_small').text('');
                                }
                                if (response.not_id_table == 1) {
                                    $('.not_id_table').text(json['Không có dịch vụ để sử dụng thẻ']);
                                } else {
                                    $('.not_id_table').text('');
                                }
                                if (response.error_account_money == 1) {
                                    $('.error_account_money').text(json['Tiền trong tài khoản không đủ']);
                                } else {
                                    $('.error_account_money').text('');
                                }
                                if (response.error_account_money_null == 1) {
                                    $('.error_account_money_null').text(json['Tiền trong tài khoản không đủ']);
                                } else {
                                    $('.error_account_money_null').text('');
                                }
                                if (response.money_owed_zero == 1) {
                                    $('.money_owed_zero').text(json['Tiền tài khoản không hợp lệ']);
                                } else {
                                    $('.money_owed_zero').text('');
                                }
                                if (response.money_large_moneybill == 1) {
                                    $('.money_large_moneybill').text(json['Tiền tài khoản không hợp lệ']);
                                } else {
                                    $('.money_large_moneybill').text('');
                                }
                                if (response.error == true) {

                                    $('#modal-receipt').modal('hide');
                                    if (response.print_card.length > 0) {
                                        mApp.block(".load_ajax", {
                                            overlayColor: "#000000",
                                            type: "loader",
                                            state: "success",
                                            message: json["Đang tải..."]
                                        });
                                        $.ajax({
                                            url: laroute.route('admin.order.render-card'),
                                            dataType: 'JSON',
                                            method: 'POST',
                                            data: {
                                                list_card: response.print_card
                                            },
                                            success: function (res) {
                                                mApp.unblock(".load");
                                                $('.list-card').empty();
                                                $('.list-card').append(res);
                                                $('#modal-print').modal('show');

                                                var list_code = [];
                                                $.each($('.list-card').find(".toimg"), function () {
                                                    var $tds = $(this).find("input[name='code']");
                                                    $.each($tds, function () {
                                                        list_code.push($(this).val());
                                                    });
                                                });
                                                for (let i = 1; i <= list_code.length; i++) {
                                                    html2canvas(document.querySelector("#check-selector-" + i + "")).then(canvas => {
                                                        $('.canvas').append(canvas);
                                                        var canvas = $(".canvas canvas");
                                                        var context = canvas.get(0).getContext("2d");
                                                        var dataURL = canvas.get(0).toDataURL();
                                                        var img = $("<img class='img-canvas' id=" + i + "></img>");
                                                        img.attr("src", dataURL);
                                                        canvas.replaceWith(img);
                                                    });
                                                }

                                            }
                                        });
                                        if (response.isSMS == 0) {
                                            $(".btn-send-sms").remove();
                                        }
                                        $('#modal-print').modal({
                                            backdrop: 'static',
                                            keyboard: false
                                        });
                                        $('#orderiddd').val(response.orderId);

                                        // let flagLoyalty = loyalty(response.orderId);
                                        // if (flagLoyalty == true) {
                                        $('#form-order-ss').submit();
                                        // }
                                    } else {
                                        $('#orderiddd').val(response.orderId);
                                        $('#form-order-ss').submit();

                                        swal(json["Thanh toán đơn hàng thành công"], "", "success");
                                        // let flagLoyalty = loyalty(response.orderId);
                                        // if (flagLoyalty == true) {
                                        setTimeout(function () {
                                            window.location = laroute.route('admin.order');
                                        }, 1000);
                                        // }
                                    }
                                    $('.hiddenOrderIdss').val(response.orderId);
                                    // $('#orderiddd').val(response.orderId);
                                    // $('#form-order-ss').submit();
                                    // setTimeout(function () {
                                    //     location.reload();
                                    // }, 1000);
                                }
                            }
                        })
                    }
                });

                // $('#amount_receipt_detail').mask('000,000,000', {reverse: true});
                $('#receipt-close-btn').click(function () {
                    $('#modal-receipt').modal('hide');
                });
            }
        });
    },
    modal_card: function (id) {
        $('#modal-card').modal('show');
    },
    customer_haunt: function (id, e) {
        $('#customer_id').val(id);
        $('#money_customer').val('');
        $('.customer').empty();
        var tpl = $('#customer-haunt-tpl').html();
        $('.customer').append(tpl);
        $.each($('#table_add tbody tr'), function () {
            var codeHidden = $(this).find("input[name='object_type']");
            var value_code = codeHidden.val();
            if (value_code == 'member_card') {
                $(this).closest('.tr_table').remove();
            }
        });
        $("#load").trigger("click");
        $('.tab_member_card').remove();
        $('#customer-search').val(null).trigger("change");
        $('#customer_id_modal').val('');
        $('#full_name').val('').removeAttr('disabled', 'disabled');
        $('#phone').val('').removeAttr('disabled', 'disabled');
        $('#customer_avatar').val('');
        $('#member_money').val('');
        $('.error_phone').text('');
        $(e).remove();
        $('.pt-discount').val(0);
    },
    print: function (code) {
        var stt_image = $('.toimg.' + code + '').find('input[name="stt"]').val();
        var base = $('.canvas').find('#' + stt_image + '').attr('src');
        $.ajax({
            url: laroute.route('admin.order.print-card-one'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                base: base
            }, success: function (res) {
                $('.content-print-card').empty();
                $('.content-print-card').append(res);
                jQuery('#print-card').print()
            }
        });
    },
    print_all: function () {
        var list_image = [];
        $.each($('.canvas'), function () {
            var $tds = $(this).find(".img-canvas");
            $.each($tds, function () {
                list_image.push($(this).attr('src'));
            });
        });
        $.ajax({
            url: laroute.route('admin.order.print-card-all'),
            method: "POST",
            data: {
                list_image: list_image
            },
            success: function (res) {
                $('.content-print-card').empty();
                $('.content-print-card').append(res);
                jQuery('#print-card').print();
            }

        });
    },
    send_mail: function () {
        var customer_id = $('#customer_id').val();
        if (customer_id == 1) {
            $('#modal-enter-email').modal('show');
        } else {
            $.ajax({
                url: laroute.route('admin.order.check-email-customer'),
                dataTye: 'JSON',
                method: 'POST',
                data: {
                    customer_id: customer_id
                }, success: function (res) {
                    if (res.email_null == 1) {
                        $('#modal-enter-email').modal('show');
                    }
                    if (res.email_success == 1) {
                        $('#enter_email').val(res.email).attr('disabled', true);
                        $('#modal-enter-email').modal('show');
                    }
                }
            });
        }
    },
    submit_send_email: function () {
        $('#submit_email').validate({
            rules: {
                enter_email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                enter_email: {
                    required: json['Hãy nhập email'],
                    email: json['Email không hợp lệ']
                },
            },
            submitHandler: function () {
                ///list image
                var list_image = [];
                $.each($('.canvas'), function () {
                    var $tds = $(this).find(".img-canvas");
                    $.each($tds, function () {
                        list_image.push($(this).attr('src'));
                    });
                });
                $.ajax({
                    url: laroute.route('admin.order.submit-send-email'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        email: $('#enter_email').val(),
                        customer_id: $('#customer_id').val(),
                        list_image: list_image
                    }, success: function (res) {
                        if (res.success == 1) {
                            swal(json["Gửi email thành công"], "", "success");
                            $('#modal-enter-email').modal('hide');
                        }
                    }
                });
            }
        });
    },
    changeAmountReceipt:function (obj) {
        var amount_receipt_cash = 0;
        if ($('#amount_receipt_cash').val() != undefined && $('#amount_receipt_cash').val() != '') {
            amount_receipt_cash = $('#amount_receipt_cash').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_atm = 0;
        if ($('#amount_receipt_atm').val() != undefined && $('#amount_receipt_atm').val() != '') {
            amount_receipt_atm = $('#amount_receipt_atm').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_visa = 0;
        if ($('#amount_receipt_visa').val() != undefined && $('#amount_receipt_visa').val() != '') {
            amount_receipt_visa = $('#amount_receipt_visa').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_receipt_money = 0;
        if ($('#amount_receipt_money').val() != undefined && $('#amount_receipt_money').val() != '') {
            amount_receipt_money = $('#amount_receipt_money').val().replace(new RegExp('\\,', 'g'), '');
        }
        var amount_all = Number(amount_receipt_cash) + Number(amount_receipt_atm) + Number(amount_receipt_visa) + Number(amount_receipt_money);

        $('#amount_all').val(formatNumber(amount_all.toFixed(decimal_number)));
        $('.cl_amount_all').text(formatNumber(amount_all.toFixed(decimal_number)));

        var rest = $('#receipt_amount').val().replace(new RegExp('\\,', 'g'), '');
        if (rest - amount_all > 0) {
            $('#amount_rest').val(formatNumber((rest - amount_all).toFixed(decimal_number)));
            $('.cl_amount_rest').text(formatNumber((rest - amount_all).toFixed(decimal_number)));
            if ($(obj).val() == '') {
                if (rest - amount_all < 0) {
                    $('#amount_return').val(formatNumber((amount_all - rest).toFixed(decimal_number)));
                    $('.cl_amount_return').text(formatNumber((amount_all - rest).toFixed(decimal_number)));
                } else {
                    $('#amount_return').val(0);
                    $('.cl_amount_return').text(0);
                }
            }
        } else {
            $('#amount_rest').val(0);
            $('#amount_return').val(formatNumber((amount_all - rest).toFixed(decimal_number)));
            $('.cl_amount_rest').text(0);
            $('.cl_amount_return').text(formatNumber((amount_all - rest).toFixed(decimal_number)));
        }
        discountCustomerInput();
    },
    getPromotionGift:function () {
        $.getJSON(laroute.route('translate'), function (json) {
            //Lấy total quantity sp, dv, thẻ dv
            var arrParam = [];
            $.each($('#table_add').find('.tr_table, .table_add'), function () {
                var objectType = $(this).find("input[name='object_type']").val();
                var objectCode = $(this).find("input[name='object_code']").val();
                var quantity = $(this).find("input[name='quantity']").val();

                arrParam.push({
                    objectType: objectType,
                    objectCode: objectCode,
                    quantity: quantity
                });
            });

            //Check promotion gift
            $.ajax({
                url: laroute.route('admin.order.check-gift'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    customer_id: $('#customer_id').val(),
                    arrParam: arrParam
                },
                async: false,
                success: function (res) {
                    $('.promotion_gift').remove();
                    if (res.gift > 0) {
                        $.map(res.arr_gift, function (a) {
                            stt_tr++;
                            var zero = 0;
                            var tpl = $('#table-gift-tpl').html();
                            tpl = tpl.replace(/{stt}/g, stt_tr);
                            tpl = tpl.replace(/{name}/g, a.gift_object_name + ' (' + json['quà tặng'] + ')');
                            tpl = tpl.replace(/{id}/g, a.gift_object_id);
                            tpl = tpl.replace(/{code}/g, a.gift_object_code);
                            tpl = tpl.replace(/{type_hidden}/g, a.gift_object_type + '_gift');
                            tpl = tpl.replace(/{price}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{price_hidden}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{amount}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{amount_hidden}/g, zero.toFixed(decimal_number));
                            tpl = tpl.replace(/{quantity}/g, a.quantity_gift);
                            tpl = tpl.replace(/{quantity_hid}/g, zero.toFixed(decimal_number));
                            $('#table_add > tbody').append(tpl);
                        });
                    }
                }
            });
        });
    },
    removeGift: function (obj) {
        $(obj).closest('.tr_table').remove();
    },
};
$(document).ready(function () {
    $.getJSON(laroute.route('translate'), function (json) {
        $('#search').keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
            }
        });
        $('#search').bind("enterKey", function () {
            var id = $('#customer_id').val();
            var type = $('.type').find('.active').attr('data-name');
            // var type_load = $('.type').find('.active').attr('data-name');

            var search = $('#search').val();
            mApp.block("#m_blockui_1_content", {
                overlayColor: "#000000",
                type: "loader",
                state: "success",
                message: json["Đang tải..."]
            });
            $.ajax({
                url: laroute.route('admin.order.search'),
                data: {
                    type: type,
                    search: search,
                    id: id,
                    customer_id: $('#customer_id').val()
                },
                method: 'POST',
                dataType: "JSON",
                success: function (response) {
                    if (response.type != 'member_card') {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            if (a.is_sale == 0) {
                                var tpl = $('#list-tpl').html();
                                tpl = tpl.replace(/{name}/g, a.name);
                                tpl = tpl.replace(/{price}/g, a.price);
                                tpl = tpl.replace(/{id}/g, a.id);
                                tpl = tpl.replace(/{code}/g, a.code);
                                if (a.avatar !== null && a.avatar !== '') {
                                    tpl = tpl.replace(/{img}/g, a.avatar);
                                } else {
                                    tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                                }
                                tpl = tpl.replace(/{price_hidden}/g, a.price);
                                tpl = tpl.replace(/{type}/g, a.type);
                                $('.append').append(tpl);
                            } else {
                                var tpl = $('#list-promotion-tpl').html();
                                tpl = tpl.replace(/{name}/g, a.name);
                                tpl = tpl.replace(/{price}/g, a.price);
                                tpl = tpl.replace(/{id}/g, a.id);
                                tpl = tpl.replace(/{code}/g, a.code);
                                if (a.avatar !== null && a.avatar !== '') {
                                    tpl = tpl.replace(/{img}/g, a.avatar);
                                } else {
                                    tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                                }
                                tpl = tpl.replace(/{price_hidden}/g, a.promotion_price);
                                tpl = tpl.replace(/{type}/g, a.type);
                                $('.append').append(tpl);
                            }
                        });
                    } else {
                        mApp.unblock("#m_blockui_1_content");
                        $('.append').empty();
                        $.map(response.list, function (a) {
                            var tpl = $('#list-card-tpl').html();
                            tpl = tpl.replace(/{card_name}/g, a.card_name);
                            tpl = tpl.replace(/{card_code}/g, a.card_code);
                            tpl = tpl.replace(/{id_card}/g, a.customer_service_card_id);
                            if (a.image != null) {
                                tpl = tpl.replace(/{img}/g, a.image);
                            } else {
                                tpl = tpl.replace(/{img}/g, '/uploads/admin/icon/default-placeholder.png');
                            }
                            if (a.count_using != json['Không giới hạn']) {
                                tpl = tpl.replace(/{quantity}/g, json['Còn '] + a.count_using);
                                tpl = tpl.replace(/{quantity_app}/g, a.count_using);
                            } else {
                                tpl = tpl.replace(/{quantity}/g, 'KGH');
                                tpl = tpl.replace(/{quantity_app}/g, json['Không giới hạn']);
                            }
                            $('.append').append(tpl);
                            $.each($('#table_add tbody tr'), function () {
                                var codeHidden = $(this).find("input[name='object_code']");
                                var value_code = codeHidden.val();
                                var code = a.card_code;
                                if (value_code == code) {
                                    var quantity = $(this).find("input[name='quantity']").val();
                                    var quantity_card = $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val();
                                    if (quantity_card != json['Không giới hạn']) {
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val(quantity_card - quantity);
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').empty();
                                        $('.card_check_' + a.customer_service_card_id + '').find('.quantity').append(json['Còn '] + $('.card_check_' + a.customer_service_card_id + '').find('.quantity_card').val() + ' (lần)');
                                    }
                                }
                            });
                        });
                    }

                }
            })
        });

        order.loadDefault();

        $('#customer-search').select2({
            placeholder: json['Nhập số điện thoại khách hàng cần tìm...'],
            ajax: {
                url: laroute.route('admin.order.search-customer'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1
                    };
                    return query;
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.results,
                        pagination: {
                            more: (params.page * 5) < data.count_filtered
                        }
                    };

                }
            },
            minimumInputLength: 0,
            allowClear: true,
        }).on('select2:select', function (event) {
            $('#customer_id_modal').val(event.params.data.id);
            $('#full_name').val(event.params.data.name).attr('disabled', 'disabled');
            $('#phone').val(event.params.data.phone).attr('disabled', 'disabled');
            $('#customer_avatar').val(event.params.data.image);
            $('#member_money').val(event.params.data.money);
            $('#address').val(event.params.data.address);
        }).on('select2:unselect', function (event) {
            $('#customer_id_modal').val('');
            $('#full_name').val('').removeAttr('disabled', 'disabled');
            $('#phone').val('').removeAttr('disabled', 'disabled');
            $('#member_money').val('');
            $('#address').val('');
        });
        $('#receipt_type').select2({
            placeholder: json['Chọn hình thức thanh toán']
        }).on('select2:select', function (event) {
            if (event.params.data.id == 'cash') {
                $('.cash').empty();
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tiền mặt']);
                tpl = tpl.replace(/{money}/g, '*');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_cash');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_cash');
                $('.cash').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_cash', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
            if (event.params.data.id == 'transfer') {
                $('.transfer').empty();
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tiền chuyển khoản']);
                tpl = tpl.replace(/{money}/g, '*');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_atm');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_atm');
                $('.transfer').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_atm', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
            if (event.params.data.id == 'visa') {
                $('.visa').empty()
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tiền chuyển Visa']);
                tpl = tpl.replace(/{money}/g, '*');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_visa');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_visa');
                $('.visa').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_visa', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
            if (event.params.data.id == 'member_money') {
                var money = $('#member_money').val();
                $('.member_money').empty();
                var tpl = $('#type-receipt-tpl').html();
                tpl = tpl.replace(/{label}/g, json['Tài khoản thành viên']);
                tpl = tpl.replace(/{money}/g, json['(Còn '] + formatNumber(money) + ')');
                tpl = tpl.replace(/{name_cash}/g, 'amount_receipt_money');
                tpl = tpl.replace(/{id_cash}/g, 'amount_receipt_money');
                $('.member_money').append(tpl);

                new AutoNumeric.multiple('#amount_receipt_money', {
                    currencySymbol : '',
                    decimalCharacter : '.',
                    digitGroupSeparator : ',',
                    decimalPlaces: decimal_number,
                    eventIsCancelable: true
                });
            }
        }).on('select2:unselect', function (event) {
            var amount_this = 0;

            if (event.params.data.id == 'cash') {
                amount_this = $('#amount_receipt_cash').val().replace(new RegExp('\\,', 'g'), '');
                $('.cash').empty();
            }
            if (event.params.data.id == 'transfer') {
                amount_this = $('#amount_receipt_atm').val().replace(new RegExp('\\,', 'g'), '');
                $('.transfer').empty();
            }
            if (event.params.data.id == 'visa') {
                amount_this = $('#amount_receipt_visa').val().replace(new RegExp('\\,', 'g'), '');
                $('.visa').empty();
            }
            if (event.params.data.id == 'member_money') {
                amount_this = $('#amount_receipt_money').val().replace(new RegExp('\\,', 'g'), '');
                $('.member_money').empty();
            }
            var amount_rest = $('#amount_rest').val().replace(new RegExp('\\,', 'g'), '');
            var amount_return = $('#amount_return').val().replace(new RegExp('\\,', 'g'), '');
            var all = $('#amount_all').val().replace(new RegExp('\\,', 'g'), '');

            $('#amount_all').val(formatNumber((all - amount_this).toFixed(decimal_number)));
            $('.cl_amount_all').text(formatNumber((all - amount_this).toFixed(decimal_number)));

            if (((amount_rest) + (amount_this) - (amount_return)) > 0) {
                $('#amount_rest').val(formatNumber(((amount_rest) + (amount_this) - (amount_return)).toFixed(decimal_number)));
                $('.cl_amount_rest').text(formatNumber(((amount_rest) + (amount_this) - (amount_return)).toFixed(decimal_number)));
            } else {
                $('#amount_rest').val(0);
                $('.cl_amount_rest').text(0);
            }

            if (amount_return - amount_this > 0) {
                $('#amount_return').val(formatNumber((amount_return - amount_this)));
                $('.cl_amount_return').text(formatNumber((amount_return - amount_this)));
            } else {
                $('#amount_return').val(0);
                $('.cl_amount_return').text(0);
            }
        });

        $('#refer_id').select2({
            placeholder: json['Chọn người giới thiệu'],
            allowClear: true
        });

        $('.btn-add').click(function () {
            var continute = true;
            var customer_id = $('#customer_id').val();
            var table_subbmit = [];
            $.each($('#table_add').find(".tr_table"), function () {
                var $tds = $(this).find("input,select");
                var $check_amount = $(this).find("input[name='amount']");
                if ($check_amount.val() < 0) {
                    $('.error-table').text(json['Tổng tiền không hợp lệ']);
                    continute = false;
                }
                $.each($tds, function () {
                    table_subbmit.push($(this).val());
                });
            });
            var voucher_bill = $('#voucher_code_bill').val();
            var total_bill = $('input[name="total_bill"]').val();
            var discount_bill = $('input[name="discount_bill"]').val();
            var amount_bill = $('input[name="amount_bill_input"]').val();
            // var loc_total = total_bill.replace(/\D+/g, '');
            // var loc_discount = discount_bill.replace(/\D+/g, '');
            if (continute == true) {
                $.ajax({
                    url: laroute.route('admin.order.submitAdd'),
                    data: {
                        customer_id: customer_id,
                        total_bill: total_bill,
                        discount_bill: discount_bill,
                        amount_bill: amount_bill,
                        table_add: table_subbmit,
                        voucher_bill: voucher_bill,
                        refer_id: $('#refer_id').val()
                    },
                    method: 'POST',
                    dataType: "JSON",
                    success: function (response) {
                        if (response.table_error == 1) {
                            $('.error-table').text(json['Vui lòng chọn dịch vụ/thẻ dịch vụ/sản phẩm']);
                        }
                        if (response.error == true) {
                            swal(json["Thêm đơn hàng thành công"], "", "success");
                            window.location.reload();
                        }
                    }
                })
            }
        });
    });

    new AutoNumeric.multiple('#discount-modal, #discount-bill, #tranport_charge', {
        currencySymbol : '',
        decimalCharacter : '.',
        digitGroupSeparator : ',',
        decimalPlaces: decimal_number
    });
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function loyalty(orderId) {
    var flag = true;
    $.ajax({
        url: laroute.route('admin.order.loyalty'),
        method: "POST",
        async: false,
        data: {order_id: orderId},
        success: function (res) {
        }
    });
    return flag;
}

/*
Giảm tiền theo hạng thành viên
 */
function discountCustomer(moneyTotal, pt) {
    var result = 0;
    result = moneyTotal * (pt / 100);
    return formatNumber((result));
}

function discountCustomerInput() {
    $.getJSON(laroute.route('translate'), function (json) {
        //Tổng tiền
        var moneyTotal = $("input[name=total_bill]").val();
        $('#total-money-discount').val();
        //Phần trăm giảm.
        var pt = $('.pt-discount').val();
        var moneyDiscountCustomer = discountCustomer(moneyTotal, pt);

        $('.span_member_level_discount').text(formatNumber(moneyDiscountCustomer));
        $('#member_level_discount').val(moneyDiscountCustomer.replace(new RegExp('\\,', 'g'), ''));

        ////Thành tiền.
        //Tiền giảm theo m
        // Member level.
        var memberLevelDiscount = $('#member_level_discount').val().replace(new RegExp('\\,', 'g'), '');
        //Giảm giá
        var discountBill = $('#discount_bill').val().replace(new RegExp('\\,', 'g'), '');
        var amountBill = moneyTotal - memberLevelDiscount - discountBill;
        $('.amount_bill').empty();
        $('.amount_bill').append(formatNumber(Number(amountBill).toFixed(decimal_number)) + ' ' + json['đ']);
        $('.amount_bill').append('<input type="hidden" name="amount_bill_input" class="form-control amount_bill_input" value="' + amountBill.toFixed(decimal_number) + '">');
    });
}

// setInterval(function () {
//     discountCustomerInput()
// }, 800/* in milliseconds 5p */);
