$('#autotable').PioTable({
    baseUrl: laroute.route('admin.bank.list')
});
var bank = {
    search: function () {
        $(".btn-search").trigger("click");
    },

    edit : function () {
        $('#form-bank').validate({
            rules: {
                bank_name: {
                    required: true,
                    maxlength: 255
                },
            },
            messages: {
                bank_name: {
                    required: 'Yêu cầu nhập tên ngân hàng',
                    maxlength: 'Tên ngân hàng vượt quá 255 ký tự'
                },
            },
        });
        if (!$('#form-bank').valid()) {
            return true;
        } else {
            $.ajax({
                url: laroute.route('admin.bank.update'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#form-bank').serialize(),
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            location.reload();
                        });
                    }
                },
            });
        }
    },

    addFormBankNew: function () {
        $.ajax({
            url: laroute.route('admin.bank.add-form-bank-new'),
            method: 'POST',
            dataType: 'JSON',
            data: $('#form-bank').serialize(),
            success: function (res) {
                $('.append-form-bank').empty();
                $('.append-form-bank').append(res.view);
                $('#bank_add_popup').modal('show');
            },
        });
    },

    addBank : function () {
        $('#form-bank').validate({
            rules: {
                bank_name: {
                    required: true,
                    maxlength: 255
                },
                bank_icon:{
                    required: true,
                }
            },
            messages: {
                bank_name: {
                    required: 'Yêu cầu nhập tên ngân hàng',
                    maxlength: 'Tên ngân hàng vượt quá 255 ký tự'
                },
                bank_icon:{
                    required: 'Yêu cầu chọn ảnh ngân hàng',
                }
            },
        });
        if (!$('#form-bank').valid()) {
            return  true;
        } else {
            $.ajax({
                url: laroute.route('admin.bank.add'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#form-bank').serialize(),
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            location.reload();
                        });
                    }
                },
            });
        }
    },

    detailBank : function (id) {
        $.ajax({
            url: laroute.route('admin.bank.detail-form-bank'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                id : id
            },
            success: function (res) {
                $('.append-form-bank').empty();
                $('.append-form-bank').append(res.view);
                $('#bank_detail_popup').modal('show');
            },
        });
    },

    editBank : function (id) {
        $.ajax({
            url: laroute.route('admin.bank.edit-form-bank'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                id : id
            },
            success: function (res) {
                $('.append-form-bank').empty();
                $('.append-form-bank').append(res.view);
                $('#bank_edit_popup').modal('show');
            },
        });
    },

    remove: function (obj, id) {
        $.getJSON(laroute.route('translate'), function (json) {
            $(obj).closest('tr').addClass('m-table__row--danger');

            swal({
                title: 'Thông báo',
                text: "Bạn có muốn xóa không?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Xóa',
                cancelButtonText: 'Hủy',
                onClose: function () {
                    $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    $.post(laroute.route('admin.bank.remove', {id: id}), function (data) {
                        swal(
                            'Xóa thành công',
                            '',
                            'success'
                        );
                        $('#autotable').PioTable('refresh');
                    });
                }
            });
        });
    },
}

function uploadImageDetail(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_detail')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_image_detail').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_product_detail.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#bank_icon').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}