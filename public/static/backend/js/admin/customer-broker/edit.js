$(document).ready(function () {
    // $.getJSON(laroute.route('translate'), function (json) {
        $('#contact_province_id').select2({
            placeholder: json["Chọn Tỉnh/Thành"],
        }).on('select2:select', function (event) {
            $.ajax({
                url: laroute.route('admin.customer.load-district'),
                dataType: 'JSON',
                data: {
                    id_province: event.params.data.id
                },
                method: 'POST',
                success: function (res) {
                    $('.contact_district_id').empty();
                    $('#contact_province_id').append('<option value=""> ' + json["Chọn quận/huyện"] + '</option>');
                    $.map(res.optionDistrict, function (a) {
                        $('.contact_district_id').append('<option value="' + a.id + '">' + a.type + ' ' + a.name + '</option>');
                    });
                    $('#contact_province_id').trigger('change');
                }
            });
        });
        $('#contact_district_id').select2({
            placeholder: json["Chọn quận/huyện"],
            ajax: {
                url: laroute.route('admin.customer-broker.load-district'),
                data: function (params) {
                    return {
                        id_province: $('#contact_province_id').val(),
                        search: params.term,
                        page: params.page || 1
                    };
                },
                dataType: 'JSON',
                method: 'POST',
                processResults: function (res) {
                    res.page = res.page || 1;
                    return {
                        results: res.optionDistrict.map(function (item) {
                            return {
                                id: item.id,
                                text: item.name
                            };
                        }),
                        pagination: {
                            more: res.pagination
                        }
                    };
                },
            }
        });

        $('#residence_province_id').select2({
            placeholder: json["Chọn Tỉnh/Thành"],
        }).on('select2:select', function (event) {
            $.ajax({
                url: laroute.route('admin.customer-broker.load-district'),
                dataType: 'JSON',
                data: {
                    id_province: event.params.data.id
                },
                method: 'POST',
                success: function (res) {
                    $('.residence_district_id').empty();
                    $('#residence_province_id').append('<option value=""> ' + json["Chọn quận/huyện"] + '</option>');
                    $.map(res.optionDistrict, function (a) {
                        $('.residence_district_id').append('<option value="' + a.id + '">' + a.type + ' ' + a.name + '</option>');
                    });
                    $('#residence_province_id').trigger('change');
                }
            });
        });

        $('#residence_district_id').select2({
            placeholder: json["Chọn quận/huyện"],
            ajax: {
                url: laroute.route('admin.customer-broker.load-district'),
                data: function (params) {
                    return {
                        id_province: $('#residence_province_id').val(),
                        search: params.term,
                        page: params.page || 1
                    };
                },
                dataType: 'JSON',
                method: 'POST',
                processResults: function (res) {
                    res.page = res.page || 1;
                    return {
                        results: res.optionDistrict.map(function (item) {
                            return {
                                id: item.id,
                                text: item.name
                            };
                        }),
                        pagination: {
                            more: res.pagination
                        }
                    };
                },
            }
        });


        // $('#customer_refer_id').select2({
        //     placeholder: json["Chọn người giới thiệu"],
        //     ajax: {
        //         url: laroute.route('admin.customer.search-customer-refer'),
        //         dataType: 'json',
        //         delay: 250,
        //         type: 'POST',
        //         data: function (params) {
        //             var query = {
        //                 search: params.term,
        //                 page: params.page || 1
        //             };
        //             return query;
        //         },
        //         processResults: function (response) {
        //             console.log(response);
        //             response.page = response.page || 1;
        //             return {
        //                 results: response.search.results,
        //                 pagination: {
        //                     more: response.pagination
        //                 }
        //             };
        //         },
        //         cache: true,
        //         delay: 250
        //     },
        //     allowClear: true
        //     // minimumInputLength: 3
        // });
        //
        // $('#customer_source_id').select2({
        //     placeholder: json["Chọn nguồn khách hàng"],
        // });
        // $('#day').select2({
        //     placeholder: json["Ngày"],
        // });
        // $('#month').select2({
        //     placeholder: json["Tháng"],
        // });
        // $('#year').select2({
        //     placeholder: json["Năm"],
        // });
        // $.ajax({
        //     url: laroute.route('admin.customer.load-bithday'),
        //     dataType: 'JSON',
        //     data: {
        //         id: $('#customer_id').val()
        //     },
        //     method: 'POST',
        //     success: function (res) {
        //         $('#day').val(res.day);
        //         $('#month').val(res.month);
        //         $('#year').val(res.year);
        //     }
        // });
        // $('#customer_group_id').select2({
        //     placeholder: json["Hãy chọn nhóm khách hàng"],
        // });


        $('.btn-add-phone2').click(function () {
            $('.phone2').show(350);
            $('.btn-add-phone2').hide(350);
        });
        $('.delete-phone').click(function () {
            $('.phone2').hide(350);
            $('.btn-add-phone2').show(350);
            $('#phone2').val('');
        });

        $('#bank_account_no').ForceNumericOnly();
        $('#phone2').ForceNumericOnly();
        $('#ic_no').ForceNumericOnly();
    // });
});


function onmouseoverAddNew() {
    $('.dropdow-add-new').show();
}


function onmouseoutAddNew() {
    $('.dropdow-add-new').hide();
}


$('.m_selectpicker').selectpicker();

function uploadImageICFront(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            $('#blah_ic_front_image')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile_ic_front_image').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_ic_back-image.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#ic_front_image_upload').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function uploadImageICBack(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            $('#blah_ic_back_image')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_ic_front-image.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#ic_back_image_upload').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

var autoGeneratePassword = 0;
var customer = {
    resetPass: function (id) {
        $.ajax({
            url: laroute.route('admin.customer-broker.show-reset-pass'),
            method: 'POST',
            data: {
                customer_id: id
            },
            success: function (res) {
                $('#kt_modal_reset_password').find('.modal-body .kt-scroll').html(res);
                $('#kt_modal_reset_password').modal();
                $('#hidden-password').val(id);
            }
        });
    },
    show_password: function (inputId) {
        if ($(inputId).prop('type') == 'password') {
            $(inputId).prop('type', 'text');
        } else {
            $(inputId).prop('type', 'password');
        }
    },
    autoGeneratePassword: function (o) {
        if ($(o).is(':checked')) {
            $('.append-password').empty();
            var tpl = $('#tpl-auto-password').html();
            $('.append-password').append(tpl);

            var password = customer.randomPassword();
            $('#password').val(password);
            autoGeneratePassword = 1;
        } else {
            $('#password').val('');
            $('.append-password').empty();
            var tpl = $('#tpl-not-auto-password').html();
            $('.append-password').append(tpl);
            autoGeneratePassword = 0;
        }
    },
    randomPassword: function () {
        var char = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < 1; i++) {
            char += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        var number = Math.floor(Math.random() * 10);
        var rand = Math.random().toString(36).slice(-6);
        var password = char + '' + number + '' + rand;
        return password;
    },
    createPasswordNew: function () {
        // khong co ky tu dac biet
        // $.validator.addMethod("validatePassword", function (value, element) {
        //     return this.optional(element) || /^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{8,}$/i.test(value);
        // });
        /// co ky tu dac biet
        // $.validator.addMethod("validatePassword", function (value, element) {
        //     return this.optional(element) || /^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/i.test(value);
        // });
        var form = $('#form-reset-password');
        if (autoGeneratePassword == 0) {
            form.validate({
                rules: {
                    password2: {
                        required: true,
                        minlength: 8,
                        maxlength: 20,
                        // validatePassword: true,
                    },
                },
                messages: {
                    password2: {
                        required: 'Vui lòng nhập mật khẩu',
                        minlength: 'Mật khẩu tối thiểu phải có 8 ký tự',
                        maxlength: 'Mật khẩu tối đa 20 ký tự',
                        // validatePassword: 'Mật khẩu phải bao gồm cả chữ và số'
                    },
                },
            });
            if (form.valid()) {
                $.ajax({
                    url: laroute.route('admin.customer-broker.submit-reset-pass'),
                    method: "POST",
                    dataType: "JSON",
                    data: {
                        customer_id: $('#hidden-password').val(),
                        password: $('#password2').val()
                    },
                    success: function (res) {
                        console.log(res);
                        if (res.error === false) {
                            swal.fire(res.message, "", "success");
                            $('#kt_modal_reset_password').modal('hide');
                            $('#modal-reset-password-success').html(res.item);
                            $('#kt_modal_reset_password_success').modal();
                        } else {
                            swal.fire(res.message, "", "error");
                        }
                    }
                });
            }
        } else {
            form.validate({
                rules: {
                    password2: {
                        required: true,
                        minlength: 8,
                        maxlength: 20,
                        validatePassword: true,
                    },
                },
                messages: {
                    password2: {
                        required: 'Vui lòng nhập mật khẩu',
                        minlength: 'Mật khẩu tối thiểu phải có 8 ký tự',
                        maxlength: 'Mật khẩu tối đa 20 ký tự',
                        validatePassword: 'Mật khẩu phải bao gồm cả chử và số'
                    },
                },
            });
            if (form.valid()) {
                $.ajax({
                    url: laroute.route('admin.customer-broker.submit-reset-pass'),
                    method: "POST",
                    dataType: "JSON",
                    data: {
                        customer_id: $('#hidden-password').val(),
                        password: $('#password').val()
                    },
                    success: function (res) {
                        if (res.error == false) {
                            swal.fire(res.message, "", "success")
                            $('#kt_modal_reset_password').modal('hide');
                            $('#modal-reset-password-success').html(res.item);
                            $('#kt_modal_reset_password_success').modal();
                        }else {
                            swal.fire(res.message, "", "error");
                        }
                    }
                });
            }
        }
    },
    editInfoCustomer: function () {
        // $.getJSON(laroute.route('translate'), function (json) {
        var form =  $('#form-edit');
            // $.validator.addMethod("validateCode", function (value, element) {
            //     return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
            // });
            $.validator.addMethod('validatePhone', function (value, element ) {
                return this.optional(element) || /0[0-9\s.-]{9,12}/.test(value);
            });
            var is_actived = 0;
            if ($('#is_actived').is(':checked')) {
                is_actived = 1;
            }
            form.validate({
                rules: {
                    bank_account_no: {
                        required: true,
                        number: true,
                        minlength: 7,
                        maxlength: 14,
                        digits: true
                    },
                    bank_name: {
                        required: true,
                        maxlength: 250
                    },
                    birthday: {
                        required: true,
                    },
                    gender: {
                        required: true,
                    },
                    phone2: {
                        required: true,
                        number: true,
                        minlength: 9,
                        maxlength: 11,
                        digits: true,
                        validatePhone: true
                    },
                    ic_no: {
                        required: true,
                        number: true,
                        minlength: 9,
                        maxlength: 12,
                        digits: true
                    },
                    ic_place: {
                        required: true,
                        maxlength: 250
                    },
                    // blah_ic_front_image: {
                    //     required: true,
                    // },
                    // blah_ic_back_image: {
                    //     required: true,
                    // },
                    contact_province_id: {
                        required: true,
                    },
                    contact_district_id: {
                        required: true,
                    },
                    contact_address: {
                        required: true,
                        maxlength: 250
                    },
                    residence_province_id: {
                        required: true,
                    },
                    residence_district_id: {
                        required: true,
                    },
                    residence_address: {
                        required: true,
                        maxlength: 250
                    },
                },
                messages: {
                    bank_account_no: {
                        required: 'Hãy nhập số tài khoản',
                        number: 'Chỉ được nhập số nguyên',
                        minlength: 'Tối thiểu 7 ký tự',
                        maxlength: 'Tối đa 14 ký tự',
                        digits: 'Chỉ được nhập số nguyên',
                    },
                    bank_name: {
                        required: 'Hãy nhập tên ngân hàng',
                        maxlength: 'Tối đa tên ngân hàng chỉ được 255 ký tự',
                    },
                    birthday: {
                        required: 'Hãy chọn ngày sinh',
                    },
                    gender: {
                        required: 'Hãy chọn giới tính',
                    },
                    phone2: {
                        required: 'Hãy nhập số điện thoại',
                        number: 'Chỉ được nhập số',
                        minlength: 'Tối thiểu 9 ký tự',
                        maxlength: 'Tối đa 11 ký tự',
                        digits: 'Chỉ được nhập số nguyên',
                        validatePhone: 'Vui lòng nhập đúng định dạng số điện thoại'
                    },
                    ic_no: {
                        required: 'Hãy nhập số CMND',
                        number: 'Chỉ được nhập số',
                        minlength: 'Tối thiểu 9 ký tự',
                        maxlength: 'Tối đa 12 ký tự',
                        digits: 'Chỉ được nhập số nguyên',
                    },
                    ic_place: {
                        required: 'Hãy nhập nơi cấp CMND',
                        maxlength: 'Tối đa chỉ được nhập 250 ký tự',
                    },
                    // blah_ic_front_image: {
                    //     required: 'Hãy up ảnh CMND mặt trước',
                    // },
                    // ic_back_image_upload: {
                    //     required: 'Hãy up ảnh CMND mặt sau',
                    // },
                    contact_province_id: {
                        required: 'Hãy chọn Tỉnh/Thành phố',
                    },
                    contact_district_id: {
                        required: 'Hãy chọn Quận/Huyện',
                    },
                    contact_address: {
                        required: 'Hãy nhập địa chỉ',
                        maxlength: 'Tối đa chỉ được 255 ký tự',
                    },
                    residence_province_id: {
                        required: 'Hãy chọn Tỉnh/Thành phố',
                    },
                    residence_district_id: {
                        required: 'Hãy chọn Quận/Huyện',
                    },
                    residence_address: {
                        required: 'Hãy nhập địa chỉ',
                        maxlength: 'Tối đa chỉ được 255 ký tự',
                    },
                }
            });
            if (form.valid()) {
                // if ($('#is_actived').is(':checked')) {
                //     is_actived = 1;
                // }
                // var is_actived = 0;

                var bank_account_no = $('#bank_account_no').val();
                var bank_name = $('#bank_name').val();
                var birthday = $('#birthday').val();
                var gender = $('#gender').val();
                var phone2 = $('#phone2').val();
                var ic_no = $('#ic_no').val();
                var ic_place = $('#ic_place').val();
                var ic_front_image_upload = $('#ic_front_image_upload').val();
                var ic_back_image_upload = $('#ic_back_image_upload').val();
                var contact_province_id = $('#contact_province_id').val();
                var contact_district_id = $('#contact_district_id').val();
                var contact_address = $('#contact_address').val();
                var residence_province_id = $('#residence_province_id').val();
                var residence_district_id = $('#residence_district_id').val();
                var residence_address = $('#residence_address').val();
                var customer_id_hidden = $('#customer_id_hidden').val();
                var full_name = $('#full_name').val();
                $.ajax({
                    url: laroute.route('admin.customer-broker.submitEdit'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        bank_account_no : bank_account_no,
                        bank_name : bank_name,
                        birthday : birthday,
                        gender : gender,
                        phone2 : phone2,
                        ic_no : ic_no,
                        ic_place : ic_place,
                        ic_front_image_upload : ic_front_image_upload,
                        ic_back_image_upload : ic_back_image_upload,
                        contact_province_id : contact_province_id,
                        contact_district_id : contact_district_id,
                        contact_address : contact_address,
                        residence_province_id : residence_province_id,
                        residence_district_id : residence_district_id,
                        residence_address : residence_address,
                        customer_id_hidden : customer_id_hidden,
                        full_name : full_name,
                        is_actived : is_actived
                    },
                    success: function (res) {
                        if (res.error == false) {
                            swal.fire(res.message, "", "success").then(function () {
                                window.location.href = laroute.route('admin.customer-broker.detail', {id:res.id});
                            });
                        } else {
                            swal.fire(res.message, '', "error");
                        }
                    },
                    error: function (res) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire('Chỉnh sửa thông tin nhà đầu tư thất bại', mess_error, "error");
                    }
                });
            }
        // });
    }
};

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function uploadFrontImage(input,index) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.blah_front_'+index)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile_front_'+index).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_serial_front.');
        $.ajax({
            url: laroute.route("config.upload"),
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.success == 1) {
                    $('.img_front_'+index).val(res.file);
                }

            }
        });
    }
}

function uploadBackImage(input,index) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.blah_back_'+index)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile_back_'+index).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_serial_back.');
        $.ajax({
            url: laroute.route("config.upload"),
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.success == 1) {
                    $('.img_back_'+index).val(res.file);
                }

            }
        });
    }
}

function dropzone(){
    Dropzone.options.dropzoneone = {
        paramName: 'file',
        maxFilesize: 500000, // MB
        maxFiles: 100,
        // acceptedFiles: ".jpeg,.jpg,.png,.gif,.xls,.xlsx,.pdf,.doc,.docx",
        addRemoveLinks: true,
        // headers: {
        //     "X-CSRF-TOKEN": $('input[name=_token]').val()
        // },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('customer-contract.upload-dropzone'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneone').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageShow').html();
                    tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                    tpl = tpl.replace(/{link_hidden}/g, response);
                    $('#upload-image').append(tpl);
                }
            });
            this.on('removedfile', function (file,response) {
                var name = file.upload.filename;
                $.ajax({
                    url: laroute.route('admin.service.delete-image'),
                    method: "POST",
                    data: {

                        filename: name
                    },
                    success: function () {
                        $("input[class='file_Name']").each(function () {
                            var $this = $(this);
                            if ($this.val() === name) {
                                $this.remove();
                            }
                        });

                    }
                });
            });
        }
    }
}

function removeImage(e){
    $(e).closest('.image-show-child').remove();
}

function submitContract(quantity,contractId,productId){
    var file_data = $('#getFile_front_0').prop('files')[0];
    var form_data = new FormData();

    for(var i =0 ; i< quantity;i++){
        var id = $("input[name=customer_contract_serial_id_"+i +"]").val();
        var no = $("input[name=product_serial_no_"+i +"]").val();
        var amount = $("input[name=product_serial_amount_"+i +"]").val();
        var img_front_hidden = $('.img_front_hidden_'+i).val();
        var img_front = $('.img_front_'+i).val();
        var img_back_hidden = $('.img_back_hidden_'+i).val();
        var img_back = $('.img_back_'+i).val();
        if (no.length > 100) {
            swal("Số serial vượt quá 100 ký tự",'','error');
            return ;
        }
        if(no != "" &&  amount!="" && img_front_hidden != '' && img_back_hidden != ''){
            if(id != ""){
                form_data.append("serial["+i+"][customer_contract_serial_id]",id);
            }

            form_data.append("serial["+i+"][product_serial_no]",no);
            form_data.append("serial["+i+"][product_serial_amount]",amount);
            form_data.append("serial["+i+"][img_front_hidden]",img_front_hidden);
            form_data.append("serial["+i+"][img_front]",img_front);
            form_data.append("serial["+i+"][img_back_hidden]",img_back_hidden);
            form_data.append("serial["+i+"][img_back]",img_back);
        }else{
            swal("Hãy điền đầy đủ thông tin và hình ảnh của Serial",'','error');
            return ;
        }
    }

    $.each($('#upload-image').find(".image-show-child"), function (index) {
        var imgService = $(this).find($('input[name=img-sv]')).val();
        var type = $(this).find($('input[name=type]')).val();
        form_data.append("contract_file["+index+"][image]",imgService);
        // contract_file.push({
        //     image: imgService,
        //     type: type
        // });
    });
    form_data.append("customer_contract_id",contractId);
    form_data.append("productId",productId);

    $.ajax({
        url: laroute.route("admin.customer-contract.update"),
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (res) {
            if (res.error == false) {
                swal(res.message,'','success').then(function (result) {
                    window.location.href = laroute.route('admin.customer-contract');
                });
            } else {
                swal(res.message,'','error');
            }
        }
    });
}

jQuery.fn.ForceNumericOnly =
    function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };