"use strict";

var vsetListContract = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.customer-broker.list-contract-customer-vset'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_contract'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'customer_contract_code',
                    title: 'Mã hợp đồng',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.customer_contract_code + '</span>';
                    }
                },
                {
                    field: 'product_name_vi',
                    title: 'Tên gói',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.product_name_vi + '</span>';
                    }
                },
                {
                    field: 'quantity',
                    title: 'Số lượng',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.quantity + '</span>';
                    }
                },
                {
                    field: 'price_standard',
                    title: 'Mệnh giá',
                    filterable: true, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.price_standard).toLocaleString()  + ' VNĐ </span>';
                    }

                },
                {
                    field: 'total',
                    title: 'Thành tiền',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.total_amount).toLocaleString() + ' VNĐ </span>';
                    }
                },
                {
                    field: 'process_status',
                    title: 'Lãi suất',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.interest_rate).toLocaleString() + ' % </span>';
                    }
                },
                {
                    field: 'investment_time',
                    title: 'Kỳ hạn đầu tư',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        if (row.investment_time == null) {
                            return '<span> N/A </span>';
                        } else {
                            return '<span>' + row.investment_time + ' tháng</span>';
                        }
                    }
                },
                {
                    field: 'withdraw_interest_time',
                    title: 'Kỳ hạn nhận lãi',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        if (row.withdraw_interest_time == null) {
                            return '<span> N/A </span>';
                        } else {
                            return '<span>' + row.withdraw_interest_time + ' tháng</span>';
                        }
                    }
                },
                {
                    field: 'is_active',
                    title: 'Trạng thái',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        if (row.is_active == 0 || row.is_deleted == 1) {
                            return '<span> Đã hết hạn </span>';
                        } else {
                            return '<span> Đang hoạt động </span>';
                        }
                    }
                },
                {
                    field: 'customer_contract_start_date',
                    title: 'Ngày ký hợp đồng',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        let d = new Date(row.customer_contract_start_date);
                        let day = d.getDate();
                        let month = d.getMonth() + 1;
                        let year = d.getFullYear();
                        let hour = d.getHours();
                        let minute = d.getMinutes();
                        let second =d.getSeconds();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        if (second < 10) {
                            second = "0" + second;
                        }
                        let date = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
                        return '<span>' + date + '</span>';
                    }
                },
                {
                    field: 'customer_contract_end_date',
                    title: 'Ngày hết hạn',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        let d = new Date(row.customer_contract_end_date);
                        let day = d.getDate();
                        let month = d.getMonth() + 1;
                        let year = d.getFullYear();
                        let hour = d.getHours();
                        let minute = d.getMinutes();
                        let second =d.getSeconds();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        if (second < 10) {
                            second = "0" + second;
                        }
                        if (isNaN(day) && isNaN(month) && isNaN(year) && isNaN(hour) && isNaN(minute) && isNaN(second)) {
                            return '<span>N/A</span>';
                        }

                        let date = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
                        return '<span>' + date + '</span>';
                    }
                },
            ],
        };
        vsetListContract.pioTable = $('#datatable_list_contract').mDatatable(options);
    },
    tab_list_contract:function () {
        $('#datatable_list_contract').mDatatable().search('');
    }
};

// setTimeout(function(){
//
// }, 1200);
vsetListContract.init();

var vsetListIntroduct = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.customer-broker.list-introduct'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_contract'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'customer_code',
                    title: 'Mã nhà đầu tư',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.customer_code + '</span>';
                    }
                },
                {
                    field: 'full_name',
                    title: 'Họ tên',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.full_name + '</span>';
                    }
                },
                {
                    field: 'phone_login',
                    title: 'Tài khoản',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.phone_login + '</span>';
                    }
                },
                {
                    field: 'email',
                    title: 'Email',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.email + '</span>';
                    }
                },
                {
                    field: 'sum_money_contract',
                    title: 'Đang đầu tư',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        if (row.sum_money_contract != null) {
                            return '<span class="m--font-bolder">' + parseInt(row.sum_money_contract).toLocaleString() + ' VNĐ </span>';
                        } else {
                            return '<span class="m--font-bolder">0 VNĐ </span>';

                        }
                    }
                },

            ],
        };
        vsetListIntroduct.pioTable = $('#datatable_list_introduct').mDatatable(options);
    },
    tab_list_introduct:function () {
        $('#datatable_list_introduct').mDatatable().search('');
    }
};

// setTimeout(function(){
//
// }, 1200);
vsetListIntroduct.init();