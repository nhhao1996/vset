var index = {
    changeStatus: function (id, obj) {
        var is_actived = 0;
        if ($(obj).is(':checked')) {
            is_actived = 1;
        }

        $.ajax({
            url: laroute.route('admin.new.change-status'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                new_id: id,
                is_actived: is_actived
            },
            success: function (res) {
                if (res.error == false) {
                    swal(res.message, "", "success");
                } else {
                    swal(res.message, '', "error");
                }
            }
        });
    },
    remove: function (id) {
        $.ajax({
            url: laroute.route('admin.new.destroy'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                new_id: id
            },
            success: function (res) {
                if (res.error == false) {
                    swal(res.message, "", "success");
                    $('#autotable').PioTable('refresh');
                } else {
                    swal(res.message, '', "error");
                }
            }
        });
    }
};

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.new.list')
});