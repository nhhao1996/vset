$('.select-2').select2();
var userGroupAuto = {
    addConditionA: function () {
        let flag = true;
        $('.condition-A').each(function () {
            var val = $(this).val();
            if (val == '') {
                flag = false;
            } else {
                if (flag == true) {
                    $(this).prop('disabled', true)
                }
            }
        });
        if (flag == true) {
            let tpl = $('#choose-condition-A').html();
            tpl = tpl.replace(/{option}/g, userGroupAuto.loadCondition('A'));
            $('.div-condition-A').append(tpl);
            $('.ss--select-2').select2();
        }
        var countCondition = 0;
        $('div-condition-A .chooses-condition-A').each(function () {
            countCondition++;
        });
        if (countCondition >= 8) {
            $('.btn-add-condition-A').hide();
        }
    },
    addConditionB: function () {
        let flag = true;
        $('.condition-B').each(function () {
            var val = $(this).val();
            if (val == '') {
                flag = false;
            } else {
                if (flag == true) {
                    $(this).prop('disabled', true)
                }
            }
        });
        if (flag == true) {
            let tpl = $('#choose-condition-B').html();
            tpl = tpl.replace(/{option}/g, userGroupAuto.loadCondition('B'));
            $('.div-condition-B').append(tpl);
            $('.ss--select-2').select2();
        }
        var countCondition = 0;
        $('.div-condition-B .chooses-condition-B').each(function () {
            countCondition++;
        });
        if (countCondition >= 8) {
            $('.btn-add-condition-B').hide();
        }
    },
    loadCondition: function (type) {
        var arrayCondition = [0];
        if (type == 'A') {
            //Danh sách điều kiện A.
            $('.condition-A').each(function () {
                var val = $(this).val();
                arrayCondition.push(val);
            });
        } else {
            //Danh sách điều kiện A.
            $('.condition-B').each(function () {
                var val = $(this).val();
                arrayCondition.push(val);
            });
        }
        var option = '';
        $.ajax({
            url: laroute.route('admin.customer-group-filter.get-condition'),
            method: "POST",
            data: {arrayCondition: arrayCondition},
            async: false,
            success: function (res) {

                $.each(res, function (key, value) {
                    option += "<option value='" + value.id + "'>" + value.name + "</option>";
                })

            }
        });
        return option;
    },
    removeConditionA: function (t) {
        $(t).closest('.A-condition-1').remove();
        $('.btn-add-condition-A').show();
    },
    removeConditionB: function (t) {
        $(t).closest('.B-condition-1').remove();
        $('.btn-add-condition-B').show();
    },
    chooseConditionA: function (t) {
        var idCondition = $(t).val();
        var divContentCondition = $(t).parents('.A-condition-1').find('.div-content-condition');
        var tpl;
        divContentCondition.empty();
        if (idCondition == '') {
            divContentCondition.empty();
        } else if (idCondition == 1) {
            tpl = $('#tpl-customer-group-define').html();
        } else if (idCondition == 2) {
            tpl = $('#tpl-day-appointment').html();
        } else if (idCondition == 3) {
            tpl = $('#tpl-status_appointment').html();
        } else if (idCondition == 4) {
            tpl = $('#tpl-time_appointment').html();
        } else if (idCondition == 5) {
            tpl = $('#tpl-not_appointment').html();
        } else if (idCondition == 6) {
            tpl = $('#tpl-use_service').html();
        } else if (idCondition == 7) {
                tpl = $('#tpl-use_service').html();
        } else if (idCondition == 8) {
                tpl = $('#tpl-use_product').html();
        } else if (idCondition == 9) {
                tpl = $('#tpl-use_product').html();
        }


        divContentCondition.append(tpl);
        userGroupAuto.select2();
    },
    chooseConditionB: function (t) {
        var idCondition = $(t).val();
        var divContentCondition = $(t).parents('.B-condition-1').find('.div-content-condition');
        var tpl;
        divContentCondition.empty();
        if (idCondition == '') {
            divContentCondition.empty();
        } else if (idCondition == 1) {
            tpl = $('#tpl-customer-group-define').html();
        } else if (idCondition == 2) {
            tpl = $('#tpl-day-appointment').html();
        } else if (idCondition == 3) {
            tpl = $('#tpl-status_appointment').html();
        } else if (idCondition == 4) {
            tpl = $('#tpl-time_appointment').html();
        } else if (idCondition == 5) {
            tpl = $('#tpl-not_appointment').html();
        } else if (idCondition == 6) {
            tpl = $('#tpl-use_service').html();
        } else if (idCondition == 7) {
            tpl = $('#tpl-use_service').html();
        } else if (idCondition == 8) {
            tpl = $('#tpl-use_product').html();
        } else if (idCondition == 9) {
            tpl = $('#tpl-use_product').html();
        }
        divContentCondition.append(tpl);
        userGroupAuto.select2();
    },
    select2: function () {
        $('.ss--select-2').select2();
        $(".inputmask").inputmask({
            "mask": "9",
            "repeat": 10,
            "greedy": false
        });
    },
    save: function (type) {
        var name = $('#name').val();
        var errorName = $('.error-name');
        var andOrA = $('#A-or-and').val();
        var andOrB = $('#B-or-and').val();
        var arrayConditionA = [];
        var arrayConditionB = [];
        var flag = false;
        $('.A-condition-1').each(function () {
            var condition = $(this).find('.condition-A').val();
            var value = $(this).find('.chooses-condition-A').val();
            if (condition != '' && value != '') {
                var temp = {condition: condition, value: value};
                arrayConditionA.push(temp);
                flag = true;
            }
        });

        $('.B-condition-1').each(function () {
            var condition = $(this).find('.condition-B').val();
            var value = $(this).find('.chooses-condition-A').val();
            if (condition != '' && value != '') {
                var temp = {condition: condition, value: value};
                arrayConditionB.push(temp);
            }
        });

        $.getJSON(laroute.route('translate'), function (json) {
            if (name == '') {
                errorName.text(json['Vui lòng nhập tên nhóm khách hàng']);
            } else {
                errorName.text('');
                if (flag == false) {
                    swal(json["Vui lòng chọn ít nhất 1 điều kiện bao gồm những người đáp ứng"], "", "error");
                } else {
                    $.ajax({
                        url: laroute.route('admin.customer-group-filter.store-customer-group-auto'),
                        method: "POST",
                        data: {
                            name: name,
                            andOrA: andOrA,
                            andOrB: andOrB,
                            arrayConditionA: arrayConditionA,
                            arrayConditionB: arrayConditionB,
                        },
                        success: function (res) {
                            if (res.error == false) {
                                swal.fire(json["Thêm thành công!"], "", "success").then(function (result) {
                                    if (result.dismiss == 'esc' || result.dismiss == 'backdrop') {
                                        if (type == 0) {
                                            window.location.href = laroute.route('admin.customer-group-filter');
                                        } else {
                                            location.reload();
                                        }
                                    }
                                    if (result.value == true) {
                                        if (type == 0) {
                                            window.location.href = laroute.route('admin.customer-group-filter');
                                        } else {
                                            location.reload();
                                        }                                }
                                });
                            }
                        },
                        error: function (res) {
                            if (res.responseJSON != undefined) {
                                var mess_error = '';
                                $.map(res.responseJSON.errors, function (a) {
                                    mess_error = mess_error.concat(a + '<br/>');
                                });
                                swal.fire(json['Thêm thất bại!'], mess_error, "error");
                            }
                        }
                    });
                }
            }
        });
    },
};

$("#A_2").inputmask({
    "mask": "9",
    "repeat": 10,
    "greedy": false
});
$("#B_2").inputmask({
    "mask": "9",
    "repeat": 10,
    "greedy": false
});
userGroupAuto.addConditionA();
userGroupAuto.addConditionB();




