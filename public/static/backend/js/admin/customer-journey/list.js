var customerJourney = {

    remove: function (obj, id) {

        $(obj).closest('tr').addClass('m-table__row--danger');
        $.getJSON(laroute.route('translate'), function (json) {
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.customer-journey.remove', {id: id}), function () {
                    swal(
                        'Xóa thành công.',
                        '',
                        'success'
                    );
                    $('#autotable').PioTable('refresh');
                });
            }
        });
    });
    },
    changeStatus: function (obj, id, action) {
        $.post(laroute.route('admin.customer-journey.change-status'), {id: id, action: action}, function (data) {
            $('#autotable').PioTable('refresh');
        }, 'JSON');
    },
    addClose: function () {
        let name = $('input[name="name"]');
        let description = $('textarea[name="description"]');
        let is_inactive = $('#is_actived_add');
        let isActive = 0;
        if (is_inactive.is(':checked')) {
            isActive = 1;
        }
        let type = "in";
        var error = 0;
        $('.error-name').text('');
        $('.error-description').text('');
        $(".error-name").css("color", "red");
        if (name.val() == "") {
            $('.error-name').text('Vui lòng nhập tên hành trình khách hàng');
            error = 1;
        } else if(name.length > 255) {
            $('.error-name').text('Tên hành trình vượt quá 255 ký tự');
            error = 1;
        }
        if (description.val() == "") {
            $('.error-description').text('Vui lòng nhập mô tả hành trình khách hàng');
            error = 1;
        }

        if (error == 0){
            $.ajax({
                url: laroute.route('admin.customer-journey.add'),
                data: {
                    name: name.val(),
                    description: description.val(),
                    is_inactive: isActive
                },
                method: "POST",
                dataType: "JSON",
                success: function (data) {
                    $('.error-name').text('');
                    if (data.success == 1) {
                        swal(
                            'Thêm nguồn hành trình hàng thành công',
                            '',
                            'success'
                        );
                        $('#modalAdd').modal('hide');
                        is_inactive.val('1');
                        $('#autotable').PioTable('refresh');
                    }

                    if (data.success == 0) {
                        if (data.message_name != ''){
                            $('.error-name').text(data.message_name);
                        }
                    }
                }
            });
        }
    },
    edit: function (id) {
        $('.error-group-name').text('');
        $.ajax({
            url: laroute.route('admin.customer-journey.edit'),
            data: {
                customerJourneyId: id,
            },
            method: "GET",
            dataType: 'JSON',
            success: function (data) {
                $('#modalEdit').modal('show');
                $('input[name="customer_journey_id_edit"]').val(data['customer_journey_id']);
                $('input[name="name_edit"]').val(data['name']);
                $('textarea[name="description_edit"]').val(data['description']);

                // if (data['customer_source_type'] == "in") {
                //     $('.type-in').prop('checked', true);
                // } else {
                //     $('.type-out').prop('checked', true);
                // }
                if (data['is_active'] == 1) {
                    $('#is_active_edit').prop('checked', true);
                } else {
                    $('#is_active_edit').prop('checked', false);
                }

            }
        })
    },
    submitEdit: function () {
        let id = $('input[name="customer_journey_id_edit"]').val();
        let name = $('input[name="name_edit"]').val();
        let description = $('textarea[name="description_edit"]').val();
        let isActive = 0;
        if ($('#is_active_edit').is(':checked')) {
            isActive = 1;
        }
        var error = 0;
        $('.error-name').text('');
        $('.error-description').text('');
        $(".error-name").css("color", "red");
        if (name == "") {
            $('.error-name').text('Vui lòng nhập tên hành trình khách hàng');
            error = 1;
        } else if(name.length > 255) {
            $('.error-name').text('Tên hành trình vượt quá 255 ký tự');
            error = 1;
        }
        if (description == "") {
            $('.error-description').text('Vui lòng nhập mô tả hành trình khách hàng');
            error = 1;
        }

        if (error == 0) {
            $.ajax({
                    url: laroute.route('admin.customer-journey.edit-submit'),
                    data: {
                        id: id,
                        name: name,
                        description: description,
                        is_active: isActive,
                    },
                    method: "POST",
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.success == 0) {
                            if (data.message_name != ''){
                                $('.error-group-name').text(data.message_name);
                            }

                        }
                        if (data.success == 1) {
                            swal(
                                'Cập nhật hành trình khách hàng thành công',
                                '',
                                'success'
                            );
                            $('#modalEdit').modal('hide');
                            $('#autotable').PioTable('refresh');
                        } else if (data.success == 2) {
                            swal({
                                title: 'Hành trình khách hàng đã tồn tại',
                                text: "Bạn có muốn kích hoạt lại không?",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Có',
                                cancelButtonText: 'Không'
                            }).then(function (willDelete) {
                                if (willDelete.value == true) {
                                    $.ajax({
                                        url: laroute.route('admin.customer-journey.edit-submit'),
                                        data: {
                                            id: id,
                                            name: name,
                                            description: description,
                                            is_active: isActive,
                                        },
                                        method: "POST",
                                        dataType: 'JSON',
                                        success: function (data) {
                                            if (data.success = 3) {
                                                swal(
                                                    'Kích hoạt hành trình khách hàng thành công',
                                                    '',
                                                    'success'
                                                );
                                                $('#autotable').PioTable('refresh');
                                                $('#modalEdit').modal('hide');
                                            }
                                        }
                                    });
                                }
                            });
                        }

                    }
                }
            );
        }

    },
    clearAdd: function () {
        $('.error-customer-source-name').text('');
        // $('#modalAdd #customer_source_type').val('in');
        $('#modalAdd #is_actived').val('1');
        $('#modalAdd #customer_source_name').val('');
        $('#modalAdd #customer_source_description').val('');

    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('select[name="is_actived"]').val('').trigger('change');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    }
};

$(document).ready(function () {
    $('#modalAdd').on('show.bs.modal', function (e) {
        $('#modalAdd input, #modalAdd textarea').val('');
        $('#is_actived_add').prop('checked',true)
    })
})

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.customer-journey.list')
});
$('select[name="is_actived"]').select2();