$('#all').PioTable({
    baseUrl: laroute.route('admin.withdraw-request.list-all')
});
$('#one').PioTable({
    baseUrl: laroute.route('admin.withdraw-request.list')
});
$('#two').PioTable({
    baseUrl: laroute.route('admin.withdraw-request.list-commission')
});
$('#three').PioTable({
    baseUrl: laroute.route('admin.withdraw-request.list-stock')
});
var customerContract = {
    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');

        swal({
            title: "Thông báo",
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Xóa",
            cancelButtonText: "Hủy",
            onClose: function () {
                // remove hightlight row
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.withdraw-request.remove', {id: id}), function () {
                    swal(
                        "Xóa thành công",
                        '',
                        'success'
                    );
                    $('#autotable').PioTable('refresh');
                });
            }
        });
    },
}

$(document).ready(function () {
    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });
})

var withrawRequest = {
    listWithrawRequest : function (page) {
        $.ajax({
            url: laroute.route('admin.withdraw-request.get-list-withdraw-request'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                page:page,
                // withdraw_request_group_type : $('#withdraw_request_group_type').val(),
                // withdraw_request_group_id : $('#withdraw_request_group_id').val(),
                customer_contract_id : $('#customer_contract_id').val(),
                customer_id : $('#customer_id').val(),
            },
            success: function (res) {
                $('.list_withraw_request').empty();
                $('.list_withraw_request').append(res.view);
            },
        });
    },

    listCustomerContractInterestMonth : function(page) {
        $.ajax({
            url: laroute.route('admin.withdraw-request.get-list-customer-contract-interest-month'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                page:page,
                customer_contract_id : $('#customer_contract_id').val(),
                customer_id : $('#customer_id').val(),
            },
            success: function (res) {
                $('.customer_contract_interest_by_month').empty();
                $('.customer_contract_interest_by_month').append(res.view);
            },
        });
    },

    listCustomerContractInterestDate : function(page) {
        $.ajax({
            url: laroute.route('admin.withdraw-request.get-list-customer-contract-interest-date'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                page:page,
                customer_contract_id : $('#customer_contract_id').val(),
                customer_id : $('#customer_id').val(),
            },
            success: function (res) {
                $('.customer_contract_interest_by_date').empty();
                $('.customer_contract_interest_by_date').append(res.view);
            },
        });
    },

    changeStatus : function (status) {
        var text = '';
        if (status == 'inprocess') {
            text = 'Bạn có chắc muốn xác nhận yêu cầu rút tiền';
        } else if (status == 'cancel') {
            text = 'Bạn có chắc không xác nhận yêu cầu rút tiền';
        } else if(status == 'done') {
            text = 'Bạn có chắc muốn hoàn thành yêu cầu rút tiền';
        }

        if (status != 'cancel'){
            swal({
                title: "Thông báo",
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Đồng ý",
                cancelButtonText: "Hủy",
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.withdraw-request.change-status'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            withdraw_request_status:status,
                            withdraw_request_id : $('#withdraw_request_id').val(),
                            available_balance_after : $('#available_balance_after').val()
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            });
        } else {
            swal.fire({
                title: text,
                type: 'question',
                input: 'textarea',
                inputPlaceholder: 'Nhập lý do',
                confirmButtonText: "Đồng ý",
                cancelButtonText: "Hủy",
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'Yêu cầu nhập lý do!'
                    } else if (value.length > 255){
                        return 'Lý do vượt quá 255 ký tự!'
                    }
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.withdraw-request.change-status'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            withdraw_request_status:status,
                            withdraw_request_id : $('#withdraw_request_id').val(),
                            available_balance_after : $('#available_balance_after').val(),
                            reason: result.value
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            })
        }

    },

    changeStatusGroup : function (status) {
        var text = '';
        if (status == 'inprocess') {
            text = 'Bạn có chắc muốn xác nhận yêu cầu rút tiền';
        } else if (status == 'cancel') {
            text = 'Bạn có chắc không xác nhận yêu cầu rút tiền';
        } else if(status == 'done') {
            text = 'Bạn có chắc muốn hoàn thành yêu cầu rút tiền';
        }

        if (status != 'cancel') {
            swal({
                title: "Thông báo",
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Xác nhận",
                cancelButtonText: "Hủy",
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.withdraw-request.change-status-group'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            withdraw_request_status:status,
                            withdraw_request_group_id : $('#withdraw_request_group_id').val(),
                            available_balance_after : $('#available_balance_after').val(),
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            });
        } else {
            swal.fire({
                title: text,
                type: 'question',
                input: 'textarea',
                inputPlaceholder: 'Nhập lý do',
                confirmButtonText: "Đồng ý",
                cancelButtonText: "Hủy",
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'Yêu cầu nhập lý do!'
                    } else if (value.length > 255){
                        return 'Lý do vượt quá 255 ký tự!'
                    }
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.withdraw-request.change-status-group'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            withdraw_request_status:status,
                            withdraw_request_group_id : $('#withdraw_request_group_id').val(),
                            available_balance_after : $('#available_balance_after').val(),
                            reason: result.value
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            })
        }
    },

    changeStatusStock : function (status) {
        var text = '';
        if (status == 'inprocess') {
            text = 'Bạn có chắc muốn xác nhận yêu cầu rút tiền';
        } else if (status == 'cancel') {
            text = 'Bạn có chắc không xác nhận yêu cầu rút tiền';
        } else if(status == 'done') {
            text = 'Bạn có chắc muốn hoàn thành yêu cầu rút tiền';
        }

        if (status != 'cancel') {
            swal({
                title: "Thông báo",
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Xác nhận",
                cancelButtonText: "Hủy",
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.withdraw-request.change-status-stock'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            withdraw_request_status:status,
                            withdraw_request_group_id : $('#withdraw_request_group_id').val(),
                            available_balance_after : $('#available_balance_after').val(),
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            });
        } else {
            swal.fire({
                title: text,
                type: 'question',
                input: 'textarea',
                inputPlaceholder: 'Nhập lý do',
                confirmButtonText: "Đồng ý",
                cancelButtonText: "Hủy",
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'Yêu cầu nhập lý do!'
                    } else if (value.length > 255){
                        return 'Lý do vượt quá 255 ký tự!'
                    }
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.withdraw-request.change-status-stock'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            withdraw_request_status:status,
                            withdraw_request_group_id : $('#withdraw_request_group_id').val(),
                            reason: result.value
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            })
        }
    }
}