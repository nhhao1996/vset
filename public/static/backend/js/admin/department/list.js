function clearModalAdd() {
    $('#modalAdd #department_name').val('');
    $('#modalAdd #is_inactive').val('1');
    $('#modalAdd .department-name').text('');
}

var Department = {
    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');
        // $.getJSON(laroute.route('translate'), function (json) {
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                // remove hightlight row
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.department.remove', {id: id}), function () {
                    swal(
                        'Xóa thành công.',
                        '',
                        'success'
                    );

                    // window.location.reload();

                    $('#autotable').PioTable('refresh');
                });
            }
        });
        // });
    },
    changeStatus: function (obj, id, action) {
        $.ajax({
            url: laroute.route('admin.department.change-status'),
            method: "POST",
            data: {
                id: id, action: action
            },
            dataType: "JSON"
        }).done(function (data) {
            $('#autotable').PioTable('refresh');
        });
    },
    clearAdd: function () {
        clearModalAdd();
    },
    add: function () {
        $('#form-department').validate({
            rules: {
                department_name: {
                    required: true,
                    maxlength: 255
                },
            },
            messages: {
                department_name: {
                    required: 'Yêu cầu nhập tên phòng ban',
                    maxlength: 'Tên phòng ban vượt quá 255 ký tự'
                },

            },
        });
        if (!$('#form-department').valid()) {
            return  true;
        } else {
            $.ajax({
                url: laroute.route('admin.department.add'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#form-department').serialize(),
                success: function (res) {
                    if (res.error == true) {
                        swal('Tạo phòng ban thất bại','','error');
                    } else {
                        swal('Tạo phòng ban thành công','','success').then(function () {
                            location.reload();
                        });
                    }
                },
            });
        }
    },
    edit: function (id) {
        $.ajax({
            url: laroute.route('admin.department.edit'),
            data: {
                id: id
            },
            method: "POST",
            dataType: 'JSON',
            success: function (data) {
                $('#modalEdit #hidden-id').val(id);
                $('#modalEdit #departName-edit').val(data['departmentName']);
                if (data['isInactive'] == 1) {
                    $('#modalEdit #is-inactive-edit').prop('checked', true);
                } else {
                    $('#modalEdit #is-inactive-edit').prop('checked', false);
                }
                $('#modalEdit').modal('show');

            }
        })
    },
    submitEdit: function () {
        $('#modalEdit .departName').css('color', 'red');
        let id = $('#modalEdit #hidden-id').val();
        let departName = $('#modalEdit #departName-edit');
        let check = 0;
        console.log(departName);
        if ($('#modalEdit #is-inactive-edit').is(':checked')) {
            check = 1;
        }
        if (departName.val() != "") {
            $.ajax({
                    url: laroute.route('admin.department.submit-edit'),
                    data: {
                        id: id,
                        departName: departName.val(),
                        isActived: check,
                        parameter: 0
                    },
                    method: "POST",
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.status == 0){
                            $('#modalEdit .departName').text('Phòng ban đã tồn tại');
                        }
                        if (data.status == 4){
                            $('#modalEdit .departName').text('Tên phòng ban vược quá 255 ký tự');
                        }
                        if (data.status == 1) {
                            swal(
                                'Cập nhật phòng ban thành công',
                                '',
                                'success'
                            );
                            $('#modalEdit').modal('hide');
                            $('#autotable').PioTable('refresh');
                            $('#modalEdit .departName').text('');
                        }else if (data.status == 2) {
                            swal({
                                title: 'Phòng ban đã tồn tại',
                                text: "Bạn có muốn kích hoạt lại không?",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Có',
                                cancelButtonText: 'Không',
                            }).then(function (willDelete) {
                                if (willDelete.value == true) {
                                    $.ajax({
                                        url: laroute.route('admin.department.submit-edit'),
                                        data: {
                                            id: id,
                                            departName: departName.val(),
                                            isActived: check,
                                            parameter: 1
                                        },
                                        method: "POST",
                                        dataType: 'JSON',
                                        success: function (data) {
                                            if (data.status = 3) {
                                                swal(
                                                    'Kích hoạt phòng ban thành công',
                                                    '',
                                                    'success'
                                                );
                                                $('#autotable').PioTable('refresh');
                                                $('#modalEdit').modal('hide');
                                            }
                                        }
                                    });
                                }
                            });
                        }

                    }
                }
            );
        } else {
            $('#modalEdit .departName').text('Vui lòng nhập tên phòng ban');
        }
    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('');
        $('.m_selectpicker').selectpicker('refresh');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    }
};
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.department.list')
});

$('.m_selectpicker').select2();