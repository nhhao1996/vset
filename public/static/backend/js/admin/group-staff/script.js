var unique = $('#unique').val();
var groupStaff = {
    init: function () {
        $.ajaxSetup({
            async: false,
        });
        groupStaff.loadItemSelected();
        $(document).ready(function() {
            $('#customer').select2({
                placeholder: 'Khách hàng'
            });
            $('#group_customer').select2({
                placeholder: 'Chọn nhóm khách hàng',
                language: {
                    noResults: function() {
                        return 'Không tìm thấy group khách hàng ';
                    },
                },
            });
        });
    },
    loadItem: function (page = 1) {
        var keyword =  $('#modal_staff .keyword').val();
        var isActived =  $('#modal_staff .is_actived').val();
        var group_staff_id =  $('#group_staff_id').val();
        $.ajax({
            url: laroute.route('admin.group-staff.loadStaff'),
            method: "POST",
            data: {
                unique: unique,
                page: page,
                keyword: keyword,
                isActived: isActived,
                group_staff_id: group_staff_id,
            },
            success: function (res) {
                $('#div_list_item').html(res);
            }
        });
    },
    checkedAllItem: function (o) {
        var type = 'unchecked';
        if ($(o).is(':checked')) {
            type = 'checked';
            $('#modal_staff .checkbox_item').prop('checked', true);
        } else {
            $('#modal_staff .checkbox_item').prop('checked', false);
        }

        var arrItemId = [];
        $('#modal_staff .checkbox_item').each(function () {
            var itemId = $(this).val();
            arrItemId.push(itemId);
        });
        groupStaff.checkedItem(type, arrItemId);
    },
    checkedOneItem: function (o, id) {
        var type = 'unchecked';
        if ($(o).is(':checked')) {
            type = 'checked';
        }
        var arrItemId = [];
        arrItemId.push(id);
        groupStaff.checkedItem(type, arrItemId);
    },
    checkedItem: function (type, array_item) {
        $.ajax({
            url: laroute.route('admin.group-staff.checkedItem'),
            method: "POST",
            data: {
                unique: unique,
                type: type,
                array_item: array_item,
            },
            success: function (res) {}
        });
    },
    showModalItem: function () {
        $('#modal_staff .keyword').val('');
        $('#modal_staff .is_actived').val('');
        $.ajax({
            url: laroute.route('admin.group-staff.removeSessionTemp'),
            method: "POST",
            data: {
                unique: unique,
            },
            success: function (res) {
                groupStaff.loadItem();
            }
        });
    },
    submitAddItem: function () {
        $('#modal_staff').modal('hide');
        $.ajax({
            url: laroute.route('admin.group-staff.submitAddItem'),
            method: "POST",
            data: {
                unique: unique,
            },
            success: function (res) {
                groupStaff.loadItemSelected();
            }
        });
    },
    loadItemSelected: function (page = 1) {
        var keyword =  $('#form_data .keyword').val();
        var isActived =  $('#form_data .is_actived').val();
        var show =  $('#show').val();
        var group_staff_id =  $('#group_staff_id').val();
        $.ajax({
            url: laroute.route('admin.group-staff.loadStaff'),
            method: "POST",
            data: {
                unique: unique,
                page: page,
                isItemSelected: 1,
                keyword: keyword,
                isActived: isActived,
                show: show,
                group_staff_id: group_staff_id,
            },
            success: function (res) {
                $('#div_list_item_selected').html(res);
            }
        });
    },
    removeItem: function (id, page = 1) {
        var keyword =  $('#form_data .keyword').val();
        var isActived =  $('#form_data .is_actived').val();
        $.ajax({
            url: laroute.route('admin.group-staff.removeItem'),
            method: "POST",
            data: {
                unique: unique,
                id: id,
            },
            success: function (res) {
                groupStaff.loadItemSelected(page);
            }
        });
    },
    store: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var group_name =  $('#group_name').val();
            $.ajax({
                url: laroute.route('admin.group-staff.store'),
                method: "POST",
                data: {
                    unique: unique,
                    group_name: group_name,
                },
                success: function (res) {
                    if (res.error == 0) {
                        swal.fire(json['Thêm thành công!'], "", "success").then(function (result) {
                            if (result.value == true) {
                                window.location.href = laroute.route('admin.group-staff');
                            }
                        });
                    } else {
                        var mess_error = '';
                        $.map(res.array_error, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Thêm thất bại!'], mess_error, "error");
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Thêm thất bại!'], mess_error, "error");
                    }
                }
            });
        });
    },
    update: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var group_name =  $('#group_name').val();
            var group_staff_id =  $('#group_staff_id').val();
            var is_active =  0;
            if ($('#is_active').is(':checked')) {
                is_active = 1;
            }
            $.ajax({
                url: laroute.route('admin.group-staff.update'),
                method: "POST",
                data: {
                    unique: unique,
                    group_name: group_name,
                    group_staff_id: group_staff_id,
                    is_active: is_active,
                },
                success: function (res) {
                    if (res.error == 0) {
                        swal.fire(json['Chỉnh sửa thành công!'], "", "success").then(function (result) {
                            if (result.value == true) {
                                window.location.href = laroute.route('admin.group-staff');
                            }
                        });
                    } else {
                        var mess_error = '';
                        $.map(res.array_error, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Chỉnh sửa thất bại!'], mess_error, "error");
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Chỉnh sửa thất bại!'], mess_error, "error");
                    }
                }
            });
        });
    },
    onChangeType: function (type) {
        var customer = $('#customer');
        var groupCustomer = $('#group_customer');
        customer.val('').trigger('change');
        groupCustomer.val('').trigger('change');
        if (type == 'all') {
            customer.prop('disabled', true);
            groupCustomer.prop('disabled', true);
        } else if (type == 'group') {
            customer.prop('disabled', true);
            groupCustomer.prop('disabled', false);
        } else if (type == 'detail') {
            customer.prop('disabled', false);
            groupCustomer.prop('disabled', true);
        }
    },
    updateAuthorization: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var group_staff_id =  $('#group_staff_id').val();
            var group_customer =  $('#group_customer').val();
            var customer =  $('#customer').val();
            var type = $('input[name=type]:checked').val();
            $.ajax({
                url: laroute.route('admin.group-staff.updateAuthorization'),
                method: "POST",
                data: {
                    unique: unique,
                    group_customer: group_customer,
                    customer: customer,
                    type: type,
                    group_staff_id: group_staff_id,
                },
                success: function (res) {
                    if (res.error == 0) {
                        swal.fire(json['Chỉnh sửa thành công!'], "", "success").then(function (result) {
                            if (result.value == true) {
                                window.location.href = laroute.route('admin.group-staff');
                            }
                        });
                    } else {
                        var mess_error = '';
                        $.map(res.array_error, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Chỉnh sửa thất bại!'], mess_error, "error");
                    }
                },
            });
        });
    }
};
groupStaff.init();