
$(document).ready(function () {
    // $('#btnLuu').click(function () {
    //     $.getJSON(laroute.route('translate'), function (json) {
    //         var form = $('#formEdit');
    //         form.validate({
    //             rules: {
    //                 name: {
    //                     required: true
    //                 },
    //                 point:{
    //                     required:true,
    //                     maxlength: 9,
    //                 },
    //                 discount: {
    //                     required: true,
    //                     max: 100,
    //                     min: 0,
    //                 }
    //             },
    //             messages: {
    //                 name: {
    //                     required: json['Hãy nhập cấp độ']
    //                 },
    //                 point:{
    //                     required:json['Hãy nhập số điểm quy đổi'],
    //                     maxlength:json['Số điểm không hợp lệ vui lòng kiểm tra lại'],
    //                 },
    //                 discount: {
    //                     required: json['Hãy nhập % giảm giá'],
    //                     max: json['% giảm giá không hợp lệ ( 0% - 100% )'],
    //                     min: json['% giảm giá không hợp lệ ( 0% - 100% )'],
    //                 }
    //             },
    //         });
    //         if (!form.valid()) {
    //             return false;
    //         }
    //         var id=$('#hhidden').val();
    //         var name=$('#h_name').val();
    //         var point=$('#h_point').val();
    //         var discount = $('#discount').val();
    //         var is_actived = 0;
    //         if($('#h_is_actived').is(':checked'))
    //         {
    //             is_actived=1;
    //         }
    //         $.ajax({
    //             url: laroute.route('admin.member-level.submitedit'),
    //             data: {
    //                 id: id ,
    //                 name: name,
    //                 point: point,
    //                 discount: discount,
    //                 is_actived: is_actived,
    //                 description: $('#description').val()
    //             },
    //             type: "POST",
    //             dataType: 'JSON',
    //             success: function (response ) {
    //
    //                     if(response.status==''){
    //                         $("#editForm").modal("hide");
    //                         swal(json["Cập nhật cấp độ thành viên thành công"], "", "success");
    //                         $('.error-name').text('');
    //                         $('#autotable').PioTable('refresh');
    //                     }else{
    //                         swal(json["Cập nhật cấp độ thành viên thất bại"], "", "success");
    //                     }
    //             },
    //         });
    //     });
    //
    // });
    // $('#h_point').mask('000,000,000,000', {reverse: true});
    // $('#discount').mask('000', {reverse: true});
});
var member_level = {

    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                // remove hightlight row
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.member-level.remove', {id: id}), function () {
                    swal(
                        'Xóa thành công',
                        '',
                        'success'
                    );
                    // window.location.reload();
                    $('#autotable').PioTable('refresh');
                });
            }
        });
    },
    add:function (close) {
        $('#type_add').val(close);
        $('#form').validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 50,
                },
                name_en: {
                    required: true,
                    maxlength: 50,
                },
                range_content: {
                    required: true,
                    maxlength: 100,
                },
                range_content_en: {
                    required: true,
                    maxlength: 100,
                },
                point:{
                    required:true,
                    maxlength: 9,
                },
                description: {
                    required:true,
                },
                description_en: {
                    required:true,
                }
                // discount: {
                //     required: true,
                //     max: 100,
                //     min: 0,
                // }
            },
            messages: {
                name: {
                    required: 'Hãy nhập cấp độ',
                    maxlength: 'Tên cấp độ vượt quá 50 ký tự',
                },
                name_en: {
                    required: 'Hãy nhập cấp độ',
                    maxlength: ' Tên cấp độ vượt quá 50 ký tự',
                },
                range_content: {
                    required: 'Hãy nhập tiêu đề',
                    maxlength: 'Tiêu đề vượt quá 100 ký tự',
                },
                range_content_en: {
                    required: 'Hãy nhập tiêu đề',
                    maxlength: 'Tiêu đề vượt quá 100 ký tự',
                },
                point:{
                    required:'Hãy nhập số điểm quy đổi',
                    maxlength:'Số điểm không hợp lệ vui lòng kiểm tra lại',
                },
                description: {
                    required:'Hãy nhập nội dung',
                },
                description_en: {
                    required:'Hãy nhập nội dung',
                }
                // discount: {
                //     required: 'Hãy nhập % giảm giá',
                //     max: '% giảm giá không hợp lệ ( 0% - 100% )',
                //     min: '% giảm giá không hợp lệ ( 0% - 100% )',
                // }
            },
        });

        if (!$('#form').valid()){
            return false;
        } else {
            var input=$('#type_add');
            var is_actived = 0;
            if($('#is_actived').is(':checked'))
            {
                is_actived=1;
            }

            $.ajax({
                method: 'POST',
                url: laroute.route('admin.member-level.submitadd'),
                // data: {
                //     name: $('#name').val(),
                //     point:$('#point').val(),
                //     discount: $('#discount').val(),
                //     is_actived: is_actived,
                //     description: $('#description').val(),
                //     member_image: $('#member_image').val(),
                // },
                data: $('#form').serialize()+'&is_actived='+is_actived,
                success: function (res) {
                    if(res.error == false)
                    {
                        swal.fire(res.message,'','success').then(function () {
                            window.location.href = laroute.route('admin.member-level');
                        });
                    }
                    else{
                        swal.fire(res.message,'','error');
                    }
                }
            })
        }
    },

    edit:function (close) {
        $('#type_add').val(close);
        $('#form').validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 50,
                },
                name_en: {
                    required: true,
                    maxlength: 50,
                },
                range_content: {
                    required: true,
                    maxlength: 100,
                },
                range_content_en: {
                    required: true,
                    maxlength: 100,
                },
                point:{
                    required:true,
                    maxlength: 9,
                },
                description: {
                    required:true,
                },
                description_en: {
                    required:true,
                }
                // discount: {
                //     required: true,
                //     max: 100,
                //     min: 0,
                // }
            },
            messages: {
                name: {
                    required: 'Hãy nhập cấp độ',
                    maxlength: 'Tên cấp độ vượt quá 50 ký tự',
                },
                name_en: {
                    required: 'Hãy nhập cấp độ',
                    maxlength: ' Tên cấp độ vượt quá 50 ký tự',
                },
                range_content: {
                    required: 'Hãy nhập tiêu đề',
                    maxlength: 'Tiêu đề vượt quá 100 ký tự',
                },
                range_content_en: {
                    required: 'Hãy nhập tiêu đề',
                    maxlength: 'Tiêu đề vượt quá 100 ký tự',
                },
                point:{
                    required:'Hãy nhập số điểm quy đổi',
                    maxlength:'Số điểm không hợp lệ vui lòng kiểm tra lại',
                },
                description: {
                    required:'Hãy nhập nội dung',
                },
                description_en: {
                    required:'Hãy nhập nội dung',
                }
                // discount: {
                //     required: 'Hãy nhập % giảm giá',
                //     max: '% giảm giá không hợp lệ ( 0% - 100% )',
                //     min: '% giảm giá không hợp lệ ( 0% - 100% )',
                // }
            },
        });

        if (!$('#form').valid()){
            return false;
        } else {
            var input=$('#type_add');
            var is_actived = 0;
            if($('#is_actived').is(':checked'))
            {
                is_actived=1;
            }

            $.ajax({
                method: 'POST',
                url: laroute.route('admin.member-level.submitedit'),
                data: $('#form').serialize()+'&is_actived='+is_actived,
                success: function (res) {
                    if(res.error == false)
                    {
                        swal.fire(res.message,'','success').then(function () {
                            window.location.href = laroute.route('admin.member-level');
                        });
                    }
                    else{
                        swal.fire(res.message,'','error');
                    }
                }
            })
        }
    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('');
        $('.m_selectpicker').selectpicker('refresh');
        $(".btn-search").trigger("click");

    }
};
$(".m_selectpicker").selectpicker();
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.member-level.list')
});

function onKeyDownInput(o) {
    $(o).on('keydown', function (e) {
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110])
        || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true))
        && (!0 === e.ctrlKey || !0 === e.metaKey)
        || 35 <= e.keyCode && 40 >= e.keyCode
        || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode)
        && e.preventDefault()
    });
}

function uploadImageMain(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_main')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_image_main').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_member.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("admin.member-level.uploadAction"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#member_image').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}