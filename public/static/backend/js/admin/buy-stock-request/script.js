$('#autotable').PioTable({
    baseUrl: laroute.route('admin.buy-stock-request.list')
});
$(document).ready(function () {
    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });

})
var BuyStockRequest = {
    dropzone:function () {
        Dropzone.options.dropzoneone = {
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            // headers: {
            //     "X-CSRF-TOKEN": $('input[name=_token]').val()
            // },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictRemoveFile: 'Xóa',
            dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
            dictInvalidFileType: 'Tệp không hợp lệ',
            dictCancelUpload: 'Hủy',
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                var random = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (let z = 0; z < 10; z++) {
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
            },
            init: function () {
                this.on("success", function (file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text', laroute.route('admin.extend-contract.upload-dropzone'));

                    if (file.status === "success") {
                        //Xóa image trong dropzone
                        $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                            file.previewElement.remove();
                        });
                        $('#dropzoneone').removeClass('dz-started');
                        //Append vào div image
                        let tpl = $('#imageShow').html();
                        tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                        tpl = tpl.replace(/{link_hidden}/g, response);
                        $('#upload-image').append(tpl);
                    }
                });
                this.on('removedfile', function (file,response) {
                    var name = file.upload.filename;
                    $.ajax({
                        url: laroute.route('admin.service.delete-image'),
                        method: "POST",
                        data: {

                            filename: name
                        },
                        success: function () {
                            $("input[class='file_Name']").each(function () {
                                var $this = $(this);
                                if ($this.val() === name) {
                                    $this.remove();
                                }
                            });

                        }
                    });
                });
            }
        };

        Dropzone.options.dropzoneone = {
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            // headers: {
            //     "X-CSRF-TOKEN": $('input[name=_token]').val()
            // },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictRemoveFile: 'Xóa',
            dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
            dictInvalidFileType: 'Tệp không hợp lệ',
            dictCancelUpload: 'Hủy',
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                var random = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (let z = 0; z < 10; z++) {
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
            },
            init: function () {
                this.on("success", function (file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text', laroute.route('admin.extend-contract.upload-dropzone'));

                    if (file.status === "success") {
                        //Xóa image trong dropzone
                        $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                            file.previewElement.remove();
                        });
                        $('#dropzoneone').removeClass('dz-started');
                        //Append vào div image
                        let tpl = $('#imageShowCash').html();
                        tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                        tpl = tpl.replace(/{link_hidden}/g, response);
                        $('#upload-image').append(tpl);
                    }
                });
                this.on('removedfile', function (file,response) {
                    var name = file.upload.filename;
                    $.ajax({
                        url: laroute.route('admin.service.delete-image'),
                        method: "POST",
                        data: {

                            filename: name
                        },
                        success: function () {
                            $("input[class='file_Name']").each(function () {
                                var $this = $(this);
                                if ($this.val() === name) {
                                    $this.remove();
                                }
                            });

                        }
                    });
                });
            }
        },

            Dropzone.options.dropzoneonecash = {
                paramName: 'file',
                maxFilesize: 10, // MB
                maxFiles: 20,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                // headers: {
                //     "X-CSRF-TOKEN": $('input[name=_token]').val()
                // },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dictRemoveFile: 'Xóa',
                dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
                dictInvalidFileType: 'Tệp không hợp lệ',
                dictCancelUpload: 'Hủy',
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                    var random = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    for (let z = 0; z < 10; z++) {
                        random += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
                },
                init: function () {
                    this.on("success", function (file, response) {
                        var a = document.createElement('span');
                        a.className = "thumb-url btn btn-primary";
                        a.setAttribute('data-clipboard-text', laroute.route('admin.extend-contract.upload-dropzone'));

                        if (file.status === "success") {
                            //Xóa image trong dropzone
                            $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                                file.previewElement.remove();
                            });
                            $('#dropzoneonecash').removeClass('dz-started');
                            //Append vào div image
                            let tpl = $('#imageShowCash').html();
                            tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                            tpl = tpl.replace(/{link_hidden}/g, response);
                            $('#upload-image-cash').append(tpl);
                        }
                    });
                    this.on('removedfile', function (file,response) {
                        var name = file.upload.filename;
                        $.ajax({
                            url: laroute.route('admin.service.delete-image'),
                            method: "POST",
                            data: {

                                filename: name
                            },
                            success: function () {
                                $("input[class='file_Name']").each(function () {
                                    var $this = $(this);
                                    if ($this.val() === name) {
                                        $this.remove();
                                    }
                                });

                            }
                        });
                    });
                }
            }

    },
    remove_img: function (e) {
        $(e).closest('.image-show-child').remove();
    },
    confirmFail : function (id) {
        // 1 : Tiền không đủ để thanh toán
        // 2 : Chưa nhập đủ thông tin nhà đầu tư
        if (id == 1) {
            swal('Không đủ tiền để xác nhận yêu cầu mua cổ phiếu','','error');
        } else if(id == 2) {
            swal('Yêu cầu nhập đủ thông tin Số điện thoại, CMND, Địa chỉ của nhà đầu tư để có thể xác nhận yêu cầu mua cổ phiếu','','error');
        } else if(id == 3) {
            swal('Số cổ phiếu còn lại từ nhà phát hành không đủ','','error');
        } else if(id == 4) {
            swal('Số cổ phiếu còn lại của người bán ở chợ không đủ','','error');
        }
    },
    cancelConfirm: function(status){
        text = 'Bạn có chắc muốn huỷ yêu cầu mua cổ phiếu này?';
        swal.fire({
            title: 'Không xác nhận yêu cầu',
            type: 'question',
            html:
                '<input id="reason_vi" class="swal2-input" placeholder="Nhập lý do không xác nhận (VI)">' +
                '<input id="reason_en" class="swal2-input" placeholder="Nhập lý do không xác nhận (EN)">',
            preConfirm: () => {
                if($('#reason_vi').val() == ''){
                    swal.showValidationError("Yêu cầu nhập lý do không xác nhận (VI)");
                } else if($('#reason_en').val() == ''){
                    swal.showValidationError("Yêu cầu nhập lý do không xác nhận (EN)");
                } else if($('#reason_vi').val().length > 255){
                    swal.showValidationError("Lý do không xác nhận (VI) vượt quá 255 ký tự");
                } else if($('#reason_en').val().length > 255){
                    swal.showValidationError("Lý do không xác nhận (EN) vượt quá 255 ký tự");
                }
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.buy-stock-request.cancel-confirm'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        process_status:status,
                        stock_order_id : $('#stock_order_id').val(),
                        reason_vi: $('#reason_vi').val(),
                        reason_en: $('#reason_en').val(),
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal(res.message,'','error').then(function () {
                                location.reload(true);
                            });
                        } else {
                            swal(res.message,'','success').then(function () {
                                location.reload(true);
                            });
                        }
                    },
                });
            }
        });
    },
    allowsConfirm : function(){
        var stock_order_id = $('#stock_order_id').val();

        var total = $('#total').val();
        var customer_id = $('#customer_id').val();

        // var stock_order_code = $('#stock_order_code').val();
        swal({
            title: "Xác nhận đơn hàng",
            text: "Bạn có muốn xác nhận đơn hàng này?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Xác nhận",
            cancelButtonText: "Hủy",
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.buy-stock-request.allows-confirm-and-create-receipt'),
                    method: 'POST',
                    data: {
                        'stock_order_id':stock_order_id,
                        'customer_id': customer_id,
                        'order_id': stock_order_id,
                        'total': total,
                        'source' : $('#source').val(),
                        'quantity' : $('#quantity').val(),
                        'payment_method_id' : $('#payment_method_id').val(),
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal.fire(res.message,'','error').then(function () {
                                location.reload();
                            });
                        } else {
                            swal(
                                res.message,
                                '',
                                'success'
                            ).then(function () {
                                location.reload();
                            });

                        }

                    }
                });
            }
        });
    },
    getListReceiptDetail : function (page) {
        $('#page').val(page);
        $.ajax({
            url: laroute.route('admin.buy-stock-request.get-list-receipt-detail'),
            method: "POST",
            data: {
                page: page,
                order_id : $('#stock_order_id').val()
            },
            success: function (res) {
                $('.receipt-detail-list').empty();
                $('.receipt-detail-list').append(res.view);
                $('td#payment_method_name_td').text($('#payment_method_name').val())
            }
        });
    },
    showPopupReceiptDetail: function () {
        $.ajax({
            url: laroute.route('admin.buy-stock-request.show-popup-receipt-detail'),
            method: "POST",
            data: {
                stock_order_id : $('#stock_order_id').val(),
                order_id : $('#stock_order_id').val(),
                customer_id : $('#customer_id').val(),
                payment_method_id : $('#payment_method_id').val(),
                payment_method_name : $('#payment_method_name').val(),
                stock_bonus : $('#stock_bonus').val(),
                total : $('#total').val(),
            },
            success: function (res) {
                $('#append-create-receipt').empty();
                $('#append-create-receipt').append(res.view);
                new AutoNumeric.multiple('.name', {
                    currencySymbol: '',
                    decimalCharacter: '.',
                    digitGroupSeparator: ',',
                    decimalPlaces: 2
                });
                $('#dropzoneonecash').dropzone();
                $('#dropzoneone').dropzone();
                $('#modalMakeReceipt').modal('show');
            }
        });
    },
    addReceiptDetail : function (method = 0) {
        if(method != 4){
            $.ajax({
                url: laroute.route('admin.buy-stock-request.show-popup-receipt-detail'),
                method: "POST",
                data: {
                    stock_order_id : $('#stock_order_id').val(),
                    order_id : $('#stock_order_id').val(),
                    customer_id : $('#customer_id').val(),
                    payment_method_id : $('#payment_method_id').val(),
                    payment_method_name : $('#payment_method_name').val(),
                    stock_bonus : $('#stock_bonus').val(),
                    total : $('#total').val(),
                },
                success: function (res) {
                    $('#append-create-receipt').empty();
                    $('#append-create-receipt').append(res.view);
                    new AutoNumeric.multiple('.name', {
                        currencySymbol: '',
                        decimalCharacter: '.',
                        digitGroupSeparator: ',',
                        decimalPlaces: 2
                    });
                    $('#dropzoneonecash').dropzone();
                    $('#dropzoneone').dropzone();
                }
            });
        }

        swal({
            title: "Xác nhận đơn hàng",
            text: "Bạn có muốn xác nhận đơn hàng này?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Xác nhận",
            cancelButtonText: "Hủy",
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.buy-stock-request.add-receipt-detail'),
                    method: "POST",
                    data: $('#receipt-detail-add').serialize()+
                        '&receipt_type='+$('#payment_method_type_select_box').val()+
                        '&source='+$('#source').val()+
                        '&quantity='+$('#quantity').val()+
                        '&total='+$('#total').val()+
                        '&customer='+$('#customer_id').val(),
                    success: function (res) {
                        if (res.error == true) {
                            swal.fire(res.message,'','error').then(function () {
                                location.reload();
                            });
                        } else {
                            swal(
                                res.message,
                                '',
                                'success'
                            ).then(function () {
                                location.reload();
                            });

                        }

                    }
                });
            }
        });
    },
    printBill:function () {
        var form = $('#print-bill');

        // Validate ngày tạo cmnd nhỏ hơn hiện tại
        $.validator.addMethod("maxDate", function(value, element) {
            var curDate = moment(new Date()).format('DD-MM-YYYY');
            var inputDate = moment(value,'DD-MM-YYYY').format('DD-MM-YYYY');
            if (inputDate <= curDate){
                return true;
            } else {
                return false;
            }
        });

        // Validate số điện thoại
        $.validator.addMethod('validatePhone', function (value, element ) {
            return this.optional(element) || /0[0-9\s.-]{9,12}/.test(value);
        });

        // Validate giá tiền
        $.validator.addMethod("checkMoney", function (value, element) {
            var valueItem = value.replace(new RegExp('\\,', 'g'), '');
            if (valueItem < 0) {
                return false;
            }
            return true;
        });

        form.validate({
            rules: {
                investment_unit: {
                    required: true
                },
                user_name: {
                    required: true
                },
                cmnd: {
                    required: true,
                    digits: true,
                    maxlength: 12
                },
                created: {
                    required: true,
                    // maxDate : true,
                },
                issued_by: {
                    required: true
                },
                address: {
                    required: true
                },
                phone: {
                    required: true,
                    validatePhone : true
                },
                reason: {
                    required: true
                },
                order_code: {
                    required: true
                },
                payment_amount: {
                    required: true,
                    checkMoney : true
                },
                payment_amount_text: {
                    required: true
                },
                name_collector: {
                    required: true
                },
                phone_collector: {
                    required: true,
                    validatePhone : true
                }
            },
            messages: {
                investment_unit: {
                    required: 'Yêu cầu nhập đơn vị thanh toán'
                },
                user_name: {
                    required: 'Yêu cầu nhập họ tên người nộp tiền'
                },
                cmnd: {
                    required: 'Yêu cầu nhập CMND',
                    digits: 'CMND sai định dạng',
                    maxlength: 'CMND vượt quá 12 số'
                },
                created: {
                    required: 'Yêu cầu nhập ngày cấp CMND',
                    maxDate : 'Ngày tạo CMND phải nhỏ hơn ngày hiện tại'
                },
                issued_by: {
                    required: 'Yêu cầu nhập nơi cấp CMND'
                },
                address: {
                    required: 'Yêu cầu nhập địa chỉ thường trú'
                },
                phone: {
                    required: 'Yêu cầu nhập số điện thoại',
                    validatePhone : 'Số điện thoại sai định dạng'
                },
                reason: {
                    required: 'Yêu cầu nhập lý do nộp tiền'
                },
                order_code: {
                    required: 'Yêu cầu nhập mã đơn hàng'
                },
                payment_amount: {
                    required: 'Yêu cầu nhập số tiền thanh toán',
                    checkMoney : 'Số tiền thanh toán sai định dạng'
                },
                payment_amount_text: {
                    required: 'Yêu cầu nhập số tiền thanh toán bằng chữ'
                },
                name_collector: {
                    required: 'Yêu cầu nhập họ tên người thu tiền'
                },
                phone_collector: {
                    required: 'Yêu cầu nhập số điện thoại người thu tiền',
                    validatePhone : 'Số điện thoại sai định dạng'
                }
            },
        });

        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                url: laroute.route('admin.buy-bonds-request.print-bill'),
                method: "POST",
                // data: $('#receipt-detail-add').serialize()+'&receipt_type='+$('#payment_method_type_select_box').val(),
                data: $('#print-bill').serialize(),
                success: function (res) {
                    // var divToPrint=document.getElementById('modalMakeReceipt');

                    var newWin = window.open('', 'Print-Window');

                    newWin.document.open();

                    newWin.document.write('<html><body onload="window.print()">' + res.view + '</body></html>');
                    newWin.document.close();
                }
            });
        }
    },
    createBill:function () {
        $.ajax({
            url: laroute.route('admin.buy-stock-request.create-bill'),
            method: "POST",
            data: {},
            success: function (res) {
                $('.bill').empty();
                $('.bill').append(res);
                $('#modalMakeReceipt').modal('hide');
                $('#create-bill').modal('show');
            }
        });
    }
}