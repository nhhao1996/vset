// todo: Global---------------------
String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

var that;
function setupInputNumber(){
    new AutoNumeric.multiple('.number-float', {
        currencySymbol: '',
        decimalCharacter: '.',
        digitGroupSeparator: ',',
        decimalPlaces: 2
    });
    new AutoNumeric.multiple('.number-int', {
        currencySymbol: '',
        // decimalCharacter: '.',
        // digitGroupSeparator: ',',
        // decimalPlaces: 2
    });

}

function setupDatePicker() {
    $(".daterange-picker")
      .daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        // maxDate: moment().endOf("day"),
        // startDate: moment().startOf("day"),
        // endDate: moment().add(1, 'days'),
        locale: {
          format: "DD/MM/YYYY",
          applyLabel: "Đồng ý",
          cancelLabel: "Thoát",
          customRangeLabel: "Tùy chọn ngày",
          daysOfWeek: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
          monthNames: [
            "Tháng 1 năm",
            "Tháng 2 năm",
            "Tháng 3 năm",
            "Tháng 4 năm",
            "Tháng 5 năm",
            "Tháng 6 năm",
            "Tháng 7 năm",
            "Tháng 8 năm",
            "Tháng 9 năm",
            "Tháng 10 năm",
            "Tháng 11 năm",
            "Tháng 12 năm",
          ],
          firstDay: 1,
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
      })
      .on("apply.daterangepicker", function (ev, picker) {
        $(this).val(
          picker.startDate.format("DD/MM/YYYY") +
            " - " +
            picker.endDate.format("DD/MM/YYYY")
        );
      });
  }
// !--Global-----------------------------
var DividendHandler={
    init(){
        // alert('hello');
        setupDateYearPicker();
        that = this;
        setupInputNumber();
       
      


        
    },
    showPopup(){
        $("#frmAddDividend").trigger("reset");
        $("#modal-add").modal("show");
    },
    setValue(name,item){
        $(`[name='${name}']`).val(item[`${name}`]);
    },
    resetForm(){
        const arrInput = [
                        "stock_history_divide_id"
                        ,"date_publish"
                        ,"stock_money_rate"
                        ,"stock_bonus_after"
                        ,"stock_bonus_before"
                        ];
        arrInput.forEach(e=>$(`[name="${e}"]`))

    },
    showPopupEdit(id){

        $("#modal-edit").modal("show");
        const item = LIST.find(e=> e.stock_history_divide_id == id);
        // item['date_publish'] = item['date_publish'].replace("00:00:00","").trim();
        // const idDatePublish = '#datepublish-'+id;
        item['date_publish'] = $("#datepublish-"+id).text();
        this.setValue("stock_history_divide_id",item);
        this.setValue("date_publish",item); 
        this.setValue("stock_money_rate",item);
        this.setValue("stock_bonus_after",item);
        this.setValue("stock_bonus_before",item);

    },
    showPopupUpload(id){
        $("[name='stock_history_divide_id']").val(id);
        $("#upload-image-cash").html("");
        console.log(LISTFILE);
        let arrFile = LISTFILE.filter(e=>e.stock_history_divide_id == id);
        arrFile.forEach(e=>{
            let temFile = `<div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx">
                                <p data-file="${e.stock_history_divide_file_id}">${e.display_name}</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>`;
            $("#upload-image-cash").append(temFile);


        });
        console.log(arrFile);


        $("#uploadFile").modal("show");
    },
    saveFile(){
        let data = $('#list-file-dividend').serialize();
        let arrName = [];
        $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
            arrName.push(file.name);

        });
        arrName = JSON.stringify(arrName);

        // todo: Save các file bị remove----------------
        let files = $("#upload-image-cash [data-file]");
        let arrFileId = [];
        files.toArray().forEach(e=>arrFileId.push($(e).attr("data-file")));
        arrFileId = JSON.stringify(arrFileId);


        $.ajax({
            url: laroute.route("admin.dividend.saveFile"),
            method: "POST",
            dataType: "JSON",
            data: $(`#list-file-dividend`).serialize() + "&arrName="+arrName + "&arrFileId="+arrFileId,

            success: function (data) {
                if (data.error == 1) {
                    swal("", data.message, "error");
                } else {
                    Swal("", data.message, "success").then(function (result) {
                        if (result) window.location.reload();
                    });
                }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
        });

    },
    edit(){
        if(!this.validate("frmEditDividend")) return;
        $.ajax({
            url: laroute.route("admin.dividend.edit"),
            method: "POST",
            dataType: "JSON",
            data: $(`#frmEditDividend`).serialize(),

            success: function (data) {
                if (data.error == 1) {
                    swal("", data.message, "error");
                } else {
                    Swal("", data.message, "success").then(function (result) {
                        if (result) window.location.reload();
                    });
                }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
        });

    },
    validate(frm){
        // todo: Add custom validate----------
        
        $.validator.addMethod(
            "requestInput", 
            function(value, element) {               
                let money = $("[name='stock_money_rate']").val().trim();
                
                let stock1 =$("[name='stock_bonus_before']").val().trim();
                let stock2 = $("[name='stock_bonus_after']").val().trim();
                if(money == '' && stock1 == '' && stock2 == '') return false;
                // todo: Nhập cho cổ phiếu
                else if(money.isEmpty()){
                    if(stock1.isEmpty() && !stock2.isEmpty() || (!stock1.isEmpty() && stock2.isEmpty()))  {
                      
                        return false;
                    }
                }  
                return true;          
            },
            "Yêu cầu nhập tiền mặt hoặc cổ phiếu"
            
        );
        // !--------Add custom validate-------
        $("#"+frm).validate({
            errorClass: "col-12 error",
            rules: {
                date_publish:{
                    required:true
                },
                stock_money_rate:{
                    requestInput:true,
                }
                   
               
            },
            messages: {
                stock_money_rate:{
                    required:'Yêu cầu nhập tỷ lệ % cổ tức',
                },
                date_publish:{
                    required:"Yêu cầu nhập ngày phát cổ tức"
                }

             
            },
            submitHandler: function (form) { // for demo
                // alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#"+frm).valid()


    },
    add(){      
        if(!this.validate("frmAddDividend")) return;
        $.ajax({
            url: laroute.route("admin.dividend.add"),
            method: "POST",
            dataType: "JSON",
            data: $(`#frmAddDividend`).serialize(),
      
            success: function (data) {
              if (data.error == 1) {
                swal("", data.message, "error");
              } else {
                Swal("", data.message, "success").then(function (result) {
                  if (result) window.location.reload();
                });
              }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
          });


    },
    search(e){
        console.log("e la gi: ", e);       
        const data= {
            "year":e.value.substring(1)

        };
        $.ajax({
            url: laroute.route("admin.dividend.list"),
            method: "POST",            
            data: data,
      
            success: function (data) {
              if (data.error == 1) {
                swal("", data.message, "error");
              } else {
                  $(".list-dividend").html(data);

               
              }
            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }
          });      
    }
}
$(document).ready(function(){    
});