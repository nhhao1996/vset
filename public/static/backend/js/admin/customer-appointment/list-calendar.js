function removeDuplicateUsingSet(arr) {
    // let unique_array = Array.from(new Set(arr));
    // return unique_array;
    var uniqueNames = [];
    $.each(arr, function (i, el) {
        if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
    });
    return uniqueNames;
}

$(document).ready(function () {
    $.getJSON(laroute.route('translate'), function (json) {
        $('#phone1').keydown(function () {
            $.ajax({
                url: laroute.route('admin.customer_appointment.search-phone'),
                dataType: 'JSON',
                method: 'POST',
                data: {
                    phone: $(this).val()
                },
                success: function (res) {
                    var arr = [];
                    $.map(res.list_phone, function (a) {
                        arr.push(a.phone);
                    });
                    $('#phone1').autocomplete({
                        source: arr,
                        change: function () {
                            var phone = $(this).val();
                            $.ajax({
                                url: laroute.route('admin.customer_appointment.cus-phone'),
                                dataType: 'JSON',
                                method: 'post',
                                data: {
                                    phone: phone
                                },
                                success: function (res) {
                                    if (res.success == 1) {
                                        $('#customer_hidden').val(res.cus.customer_id);
                                        $('#full_name').val(res.cus.full_name);
                                        $('#full_name').attr('disabled', true);
                                    }
                                    if (res.phone_new == 1) {
                                        $('#customer_hidden').val('');
                                        $('#full_name').val('');
                                        $('#full_name').attr('disabled', false);
                                    }
                                }
                            })
                        }
                    });
                }
            })
        });
        $('#list-date-show').datepicker();
        $('#search').datepicker({
            format: 'dd/mm/yyyy',
            language: 'vi',
            // startDate: '0d'
        });
        $('#block_search').click(function () {
            $('.display').show(350);
        });
        $('.delete-phone').click(function () {
            $('.display').hide(350);
            $('#search_name').val('');
        });
        $('#search').change(function () {

        });
        $('#search_name').bind("enterKey", function () {
            var search = $(this).val();
            var day = $('#day_hidden').val();
            $('.append').empty();
            $.ajax({
                url: laroute.route('admin.customer_appointment.search-name'),
                data: {
                    search: search,
                    day: day
                },
                method: 'POST',
                dataType: "JSON",
                success: function (res) {
                    $('#timeline-list').empty();
                    $('#timeline-list').append(res);
                    $('.detail_btn_' + $('.id_appointment').val() + '').trigger('click');
                    if ($('#null_day').text() != '') {
                        $('#m-info-cus').empty();
                    }
                }
            });
        });
        $('#search_name').keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
            }
        });
        $('.remove').click(function () {
            $(this).closest('.tr_detail').remove();
        });
        $(".quantity").TouchSpin({
            initval: 1,
            min: 1
        });
        $('.status').click(function () {
            $('.status').attr('class', 'btn btn-default status');
            $(this).attr('class', 'btn btn-primary active status');
        });
        $('#time_detail').select2({
            placeholder: json['Chọn giờ hẹn'],
        });
        if ($('#type_hide').val() == 'direct') {
            $('#time_detail').prepend('<option selected value="' + $('#time_hide').val() + '">' + $('#time_hide').val() + '</option>');
        } else {
            $('#time_detail').val($('#time_hide').val()).trigger('change');
        }

        // $('#appointment_source_id').select2({
        //     placeholder: 'Chọn nguồn lịch hẹn',
        // });
        $('#appointment_source_id').selectpicker();
        let $element = $('#appointment_source_id')
        let val = $element.find("option:contains('" + json['gọi điện'] + "')").val()
        $("#appointment_source_id").val(val).trigger('change');

        $('.btn_add').click(function () {
            $('#form-add').validate({
                rules: {
                    full_name: {
                        required: true
                    },
                    phone1: {
                        required: true,
                        minlength: 10,
                        maxlength: 11,
                        number: true
                    },
                    time: {
                        required: true
                    },
                    date: {
                        required: true
                    },
                    quantity_customer: {
                        min: 1,
                        required: true,
                        number: true,
                        max: 10,
                    }
                },
                messages: {
                    full_name: {
                        required: json['Hãy nhập tên khách hàng']
                    },
                    phone1: {
                        required: json['Hãy nhập số điện thoại'],
                        minlength: json['Số điện thoại tối thiểu 10 số'],
                        maxlength: json['Số điện thoại tối đa 11 số'],
                        number: json['Số điện thoại không hợp lệ']
                    },
                    time: {
                        required: json['Hãy chọn giờ hẹn']
                    },
                    date: {
                        required: json['Hãy chọn ngày hẹn']
                    },
                    quantity_customer: {
                        min: json['Số lượng khách hàng tối thiểu 1'],
                        required: json['Hãy nhập số lượng khách hàng'],
                        number: json['Số lượng khách hàng không hợp lệ'],
                        max: json['Số lượng khách hàng tối đa 10'],
                    }
                },
                submitHandler: function () {
                    // mApp.block("#m_blockui_1_content", {
                    //     overlayColor: "#000000",
                    //     type: "loader",
                    //     state: "success",
                    //     message: "Đang tải..."
                    // });
                    var full_name = $('#full_name').val();
                    var phone1 = $('#phone1').val();
                    var type = $('.source').find('.active input[name="customer_appointment_type"]').val();
                    var date = $('#date').val();
                    var time = $('#time').val();
                    var customer_hidden = $('#customer_hidden').val();
                    var description = $('#description').val();
                    var customer_quantity = $('#quantity_customer').val();
                    var appointment_source_id = $('#appointment_source_id').val();
                    // var customer_refer = $('#search_refer').val();
                    var status = $('.active').find(' input[name="status"]').val();
                    var table_quantity = [];
                    for (let i = 0; i < customer_quantity; i++) {
                        var stt = i + 1;
                        var input = $('#customer_order_' + stt + '').val();
                        var sv = '';
                        if ($('#service_id_' + stt + '').val() != '') {
                            sv = $('#service_id_' + stt + '').val();
                        }
                        var room = $('#room_id_' + stt + '').val();
                        var staff = $('#staff_id_' + stt + '').val();
                        var arr = {
                            stt: input,
                            sv: sv,
                            staff: staff,
                            room: room
                        };
                        table_quantity.push(arr);
                    }
                    //kiểm tra khách hàng đã có lịch hẹn ngày hôm nay chưa
                    if (customer_hidden != '') {
                        $.ajax({
                            url: laroute.route('admin.customer_appointment.check-number-appointment'),
                            method: 'POST',
                            dataType: 'JSON',
                            data: {
                                customer_id: customer_hidden,
                                date: date,
                                time: time,
                            },
                            success: function (res) {
                                mApp.unblock("#m_blockui_1_content");
                                console.log(res);
                                if (res.time_error == 1) {
                                    $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                                }
                                if (res.status == 0) {
                                    addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                        customer_hidden, description, table_quantity, status);
                                }
                                if (res.status == 1) {
                                    if (res.number < 3) {
                                        swal({
                                            title: json["Khách đã có lịch hẹn hôm nay lúc"] + " " + res.time,
                                            text: "",
                                            type: "warning",
                                            showCancelButton: !0,
                                            confirmButtonText: json["THÊM MỚI"],
                                            cancelButtonText: json["CẬP NHẬT"]
                                        });
                                        $('.swal2-confirm').click(function () {
                                            addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                                customer_hidden, description, table_quantity, status);
                                        });
                                        $('.swal2-cancel').click(function () {
                                            updateLoad(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        });
                                    } else {
                                        swal({
                                            title: json["Khách hàng đã đặt tối đa 3 lịch hẹn trong hôm nay"],
                                            text: "",
                                            type: "warning",
                                            confirmButtonText: json["Cập nhật lịch gần nhất"],
                                            confirmButtonClass: "btn btn-focus m-btn m-btn--pill m-btn--air"
                                        });
                                        $('.swal2-confirm').click(function () {
                                            updateLoad(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        })
                                    }
                                }
                            }
                        });
                    } else {
                        addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                            customer_hidden, description, table_quantity, status);
                    }
                }
            });
        });
        $('.btn_new').click(function () {
            $('#form-add').validate({
                rules: {
                    full_name: {
                        required: true
                    },
                    phone1: {
                        required: true,
                        minlength: 10,
                        maxlength: 11,
                        number: true
                    },
                    time: {
                        required: true
                    },
                    date: {
                        required: true
                    },
                    quantity_customer: {
                        min: 1,
                        required: true,
                        number: true,
                        max: 10,
                    }
                },
                messages: {
                    full_name: {
                        required: json['Hãy nhập tên khách hàng']
                    },
                    phone1: {
                        required: json['Hãy nhập số điện thoại'],
                        minlength: json['Số điện thoại tối thiểu 10 số'],
                        maxlength: json['Số điện thoại tối đa 11 số'],
                        number: json['Số điện thoại không hợp lệ']
                    },
                    time: {
                        required: json['Hãy chọn giờ hẹn']
                    },
                    date: {
                        required: json['Hãy chọn ngày hẹn']
                    },
                    quantity_customer: {
                        min: json['Số lượng khách hàng tối thiểu 1'],
                        required: json['Hãy nhập số lượng khách hàng'],
                        number: json['Số lượng khách hàng không hợp lệ'],
                        max: json['Số lượng khách hàng tối đa 10']
                    }
                },
                submitHandler: function () {
                    mApp.block("#m_blockui_1_content", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var full_name = $('#full_name').val();
                    var phone1 = $('#phone1').val();
                    var type = $('.source').find('.active input[name="customer_appointment_type"]').val();
                    var date = $('#date').val();
                    var time = $('#time').val();
                    var customer_hidden = $('#customer_hidden').val();
                    var description = $('#description').val();
                    var customer_quantity = $('#quantity_customer').val();
                    var appointment_source_id = $('#appointment_source_id').val();
                    // var customer_refer = $('#search_refer').val();
                    var status = $('.active').find(' input[name="status"]').val();
                    var table_quantity = [];
                    for (let i = 0; i < customer_quantity; i++) {
                        var stt = i + 1;
                        var input = $('#customer_order_' + stt + '').val();
                        var sv = '';
                        if ($('#service_id_' + stt + '').val() != '') {
                            sv = $('#service_id_' + stt + '').val();
                        }
                        var room = $('#room_id_' + stt + '').val();
                        var staff = $('#staff_id_' + stt + '').val();
                        var arr = {
                            stt: input,
                            sv: sv,
                            staff: staff,
                            room: room
                        };
                        table_quantity.push(arr);
                    }
                    //kiểm tra khách hàng đã có lịch hẹn ngày hôm nay chưa
                    if (customer_hidden != '') {
                        $.ajax({
                            url: laroute.route('admin.customer_appointment.check-number-appointment'),
                            method: 'POST',
                            dataType: 'JSON',
                            data: {
                                customer_id: customer_hidden,
                                date: date,
                                time: time,
                            },
                            success: function (res) {
                                mApp.unblock("#m_blockui_1_content");
                                if (res.time_error == 1) {
                                    $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                                }
                                if (res.status == 0) {
                                    addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                        customer_hidden, description, table_quantity, status);
                                }
                                if (res.status == 1) {
                                    if (res.number < 3) {
                                        swal({
                                            title: json["Khách đã có lịch hẹn hôm nay lúc"] + " " + res.time,
                                            text: "",
                                            type: "warning",
                                            showCancelButton: !0,
                                            confirmButtonText: json["THÊM MỚI"],
                                            cancelButtonText: json["CẬP NHẬT"]
                                        });
                                        $('.swal2-confirm').click(function () {
                                            addReset(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                                customer_hidden, description, table_quantity, status);
                                        });
                                        $('.swal2-cancel').click(function () {
                                            updateReset(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        });
                                    } else {
                                        swal({
                                            title: json["Khách hàng đã đặt tối đa 3 lịch hẹn trong hôm nay"],
                                            text: "",
                                            type: "warning",
                                            confirmButtonText: json["Cập nhật lịch gần nhất"],
                                            confirmButtonClass: "btn btn-focus m-btn m-btn--pill m-btn--air"
                                        });
                                        $('.swal2-confirm').click(function () {
                                            updateReset(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        })
                                    }
                                }
                            }
                        });
                    } else {
                        addReset(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                            customer_hidden, description, table_quantity, status);
                    }
                }
            });
        });
    });

});

var click_detail = {
    close: function () {
        $('.detail').css('display', 'none');
    },
    save: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            var time = $('#time_detail').val();
            var day = $('#search').val();
            var search_name = $('#search_name').val();
            $.ajax({
                url: laroute.route('admin.customer_appointment.submit-comfirm'),
                data: {
                    id: id,
                    time: time,
                    day: day,
                    search: search_name
                },
                method: 'POST',
                dataType: "JSON",
                success: function (res) {
                    swal(json["Xác nhận lịch hẹn thành công"], "", "success");
                    $('#timeline-list').empty();
                    $('#timeline-list').append(res);
                    $('.detail_btn_' + id + '').trigger('click');
                }
            });
        });
    },
    detail_click: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('admin.customer_appointment.detail-click'),
                data: {
                    id: id
                },
                method: 'POST',
                dataType: "JSON",
                success: function (res) {
                    $('#m-info-cus').empty();
                    $('#m-info-cus').append(res);
                    $('#time_detail').select2({
                        placeholder: json['Hãy chọn giờ hẹn']
                    });

                    $('#time_detail').val($('#time_hide').val()).trigger('change');

                }
            });
        });
    }
};
var customer_appointment = {
    search_time: function (e) {
        $('#search_name').val('');
        $(e).datepicker('hide');
        var daySearch = $('#day_hidden').val($(e).val());
        $('#day_head').empty();
        $('#today').attr('class', 'btn btn-info');
        $.ajax({
            url: laroute.route('admin.customer_appointment.search-time'),
            data: {
                day_search: daySearch.val()
            },
            method: 'POST',
            dataType: "JSON",
            success: function (res) {
                $('#timeline-list').empty();
                $('#timeline-list').append(res);
                $('.detail_btn_' + $('.id_appointment').val() + '').trigger('click');
                if ($('#null_day').text() != '') {
                    $('#m-info-cus').empty();
                }
            }
        });
    },
    click_modal: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('admin.customer_appointment.modalAddTimeline'),
                dataType: 'JSON',
                method: 'POST',
                data: {
                    date_now: $('#search').val()
                },
                success: function (res) {
                    $('.append_status').empty();
                    $('.date_app').empty();
                    $('.time_app').empty();
                    // $('#form-add')[0].reset();
                    $('#day_click').val(res.date_now);
                    $('#day_now').val(res.day_now);
                    $('#time_now').val(res.time_now);
                    $('#quantity_customer').val('1');
                    $('#quantity_hide').val(0);
                    $("#time").val('').trigger('change');
                    $('.error_time').text('');
                    $('#appointment').attr('class', 'btn btn-info color_button active');
                    $('#direct').attr('class', 'btn btn-default ');
                    var type = $('.source').find('.active input[name="customer_appointment_type"]').val();
                    if (type == 'appointment') {
                        var tpl = $('#append-status-other-tpl').html();
                        $('.append_status').append(tpl);
                        var tpl_date = $('#date-tpl').html();
                        $('.date_app').append(tpl_date);
                        var tpl_time = $('#time-tpl').html();
                        $('.time_app').append(tpl_time);
                    } else {
                        var tpl = $('#append-status-live-tpl').html();
                        $('.append_status').append(tpl);
                    }
                    ;
                    $('#date').datepicker({
                        startDate: '0d',
                        language: 'vi',
                        orientation: "bottom left", todayHighlight: !0,
                    }).on('changeDate', function (ev) {
                        $(this).datepicker('hide');
                    });
                    $('#date').val(res.date_now);
                    // $('#time').select2({
                    //     placeholder: 'Chọn giờ hẹn',
                    // });
                    $('#time').selectpicker();
                    $('#table_quantity > tbody').empty();
                    $.ajax({
                        url: laroute.route('admin.customer_appointment.option'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {},
                        success: function (res) {
                            for (let i = 0; i < $('#quantity_customer').val(); i++) {
                                var stts = $('#table_quantity tr').length;
                                var tpl = $('#table-quantity-tpl').html();
                                tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                                tpl = tpl.replace(/{stt}/g, i + 1);
                                $("#table_quantity > tbody").append(tpl);

                            }

                            $.each(res.optionService, function (index, element) {
                                $('.service_id').append('<option value="' + index + '">' + element + '</option>');
                            });
                            $('.service_id').select2({
                                placeholder: json['Chọn dịch vụ'],
                            }).on('select2:select', function (event) {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                $('#room_id_' + id + '').enable(true);
                                $('#staff_id_' + id + '').enable(true);

                            }).on('select2:unselect', function (event) {
                                if ($(this).val() == '') {
                                    var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                    $('#room_id_' + id + '').enable(false);
                                    $('#staff_id_' + id + '').enable(false);
                                    $('#room_id_' + id + '').val('').trigger('change');
                                    $('#staff_id_' + id + '').val('').trigger('change');
                                }
                            });

                            $.each(res.optionRoom, function (index, element) {
                                $('.room_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $.each(res.optionStaff, function (index, element) {
                                $('.staff_id').append('<option value="' + index + '">' + element + '</option>');

                            });

                            $('.room_id').select2({
                                placeholder: json['Chọn phòng'],
                            });
                            $('.staff_id').select2({
                                placeholder: json['Chọn nhân viên'],
                            });
                            $('.service_id').select2({
                                placeholder: json['Chọn dịch vụ'],
                            });
                        }
                    });
                    $('#modal-add').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }

            });
        });
    },
    new_click: function () {
        $('#new').attr('class', 'btn btn-info color_button active');
        $('#confirm').attr('class', 'btn btn-default');
    },
    confirm_click: function () {
        $('#confirm').attr('class', 'btn btn-info  color_button active');
        $('#new').attr('class', 'btn btn-default ');
    },
    appointment: function (e) {
        $.getJSON(laroute.route('translate'), function (json) {
            $('.date_app').empty();
            $('.time_app').empty();
            $(e).attr('class', 'btn btn-info color_button active');
            $('#direct').attr('class', 'btn btn-default ');
            $('.append_status').empty();
            var tpl = $('#append-status-other-tpl').html();
            $('.append_status').append(tpl);
            var tpl_date = $('#date-tpl').html();
            $('.date_app').append(tpl_date);
            var tpl_time = $('#time-tpl').html();
            $('.time_app').append(tpl_time);
            $('#date').datepicker({
                startDate: '0d',
                language: 'vi',
                orientation: "bottom left", todayHighlight: !0,
            });
            $('#date').val($('#day_click').val());
            // $('#time').select2({
            //     placeholder: 'Chọn giờ hẹn',
            // });
            $('#time').selectpicker();
            let name = json['gọi điện'];
            let $element = $('#appointment_source_id')
            let val = $element.find("option:contains('" + name + "')").val()
            $("#appointment_source_id").val(val).trigger('change');
        });
    },
    direct: function (e) {
        $('.date_app').empty();
        $('.time_app').empty();
        $(e).attr('class', 'btn btn-info color_button active');
        $('#appointment').attr('class', 'btn btn-default');
        $('.append_status').empty();
        var tpl = $('#append-status-live-tpl').html();
        $('.append_status').append(tpl);
        $('.date_app').append('<input id="date" name="date" class="form-control m-input" value="' + $('#day_now').val() + '" disabled="disabled">');
        $('.time_app').append('<input id="time" name="time" class="form-control m-input" value="' + $('#time_now').val() + '" disabled="disabled">');
        $("#appointment_source_id").val('1').trigger('change');
    },
    add_customer: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var quantity = $('#quantity_customer').val();
            $('#quantity_customer').val(parseInt(quantity) + 1);
            var quan_hide = $('#quantity_hide').val();
            if ($('#quantity_customer').val() > quan_hide) {
                $.ajax({
                    url: laroute.route('admin.customer_appointment.option'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {},
                    success: function (res) {
                        // for (let i = 0; i < quantity - quan_hide; i++) {
                        var stts = $('#table_quantity tr').length;
                        var tpl = $('#table-quantity-tpl').html();
                        tpl = tpl.replace(/{stt}/g, stts);
                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                        $("#table_quantity > tbody").append(tpl);
                        // }
                        $.each(res.optionService, function (index, element) {
                            $('.service_id').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $.each(res.optionRoom, function (index, element) {
                            $('.room_id').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $.each(res.optionStaff, function (index, element) {
                            $('.staff_id').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $('.service_id').select2({
                            placeholder: json['Chọn dịch vụ'],
                        }).on('select2:select', function (event) {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                            $('#room_id_' + id + '').enable(true);
                            $('#staff_id_' + id + '').enable(true);

                        }).on('select2:unselect', function (event) {
                            if ($(this).val() == '') {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                $('#room_id_' + id + '').enable(false);
                                $('#staff_id_' + id + '').enable(false);
                                $('#room_id_' + id + '').val('').trigger('change');
                                $('#staff_id_' + id + '').val('').trigger('change');
                            }

                        });
                        $('.staff_id').select2({
                            placeholder: json['Chọn nhân viên'],
                        });
                        $('.room_id').select2({
                            placeholder: json['Chọn phòng'],
                        });
                    }
                });
                $('#quantity_hide').val(quantity);
            }
            // $('#table_quantity > tbody').empty();
        });
    },
    change_quantity: function (e) {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#table_quantity > tbody').empty();
            $.ajax({
                url: laroute.route('admin.customer_appointment.option'),
                dataType: 'JSON',
                method: 'POST',
                data: {},
                success: function (res) {
                    for (let i = 0; i < $(e).val(); i++) {
                        var stts = $('#table_quantity tr').length;

                        var tpl = $('#table-quantity-tpl').html();
                        tpl = tpl.replace(/{stt}/g, i + 1);
                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                        $("#table_quantity > tbody").append(tpl);
                    }
                    $.each(res.optionService, function (index, element) {
                        $('.service_id').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionRoom, function (index, element) {
                        $('.room_id').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionStaff, function (index, element) {
                        $('.staff_id').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $('.service_id').select2({
                        placeholder: json['Chọn dịch vụ'],
                    }).on('select2:select', function (event) {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                        $('#room_id_' + id + '').enable(true);
                        $('#staff_id_' + id + '').enable(true);

                    }).on('select2:unselect', function (event) {
                        if ($(this).val() == '') {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                            $('#room_id_' + id + '').enable(false);
                            $('#staff_id_' + id + '').enable(false);
                            $('#room_id_' + id + '').val('').trigger('change');
                            $('#staff_id_' + id + '').val('').trigger('change');
                        }

                    });
                    $('.staff_id').select2({
                        placeholder: json['Chọn nhân viên'],
                    });
                    $('.room_id').select2({
                        placeholder: json['Chọn phòng'],
                    });
                }

            });
            $('#quantity_hide').val($(e).val() - 1);
        });
    },
    click_modal_edit: function (id) {
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                type: 'POST',
                url: laroute.route('admin.customer_appointment.detail'),
                data: {
                    id: id
                },
                dataType: 'JSON',
                success: function (res) {
                    $('.btn_receipt').remove();
                    $('.button_edit').remove();
                    $('#time_edit_new').select2({
                        placeholder: json['Chọn giờ hẹn']
                    });
                    $.map(res.time_new, function (time) {
                        $('#time_edit_new').append('<option value="' + time.time_new + '">' + time.time_new + '</option>');
                    });
                    $.map(res.item, function (a) {
                        $('#customer_id').val(a.customer_id);
                        $('.type_edit').empty();
                        var tpl_type = $('#type-modal-edit-tpl').html();
                        $('.type_edit').append(tpl_type);
                        if (a.status != 'finish') {
                            //begin append button edit
                            var button_edit = $('#button-edit-tpl').html();
                            $('.append_btn').append(button_edit);
                            //end
                            $('#quantity_customer_edit').attr("disabled", false);
                            $('.btn_add_cus').css('display', 'block');
                            $('.btn_edit').css('display', 'block');
                            $('.date_edit').empty();
                            $('.time_edit').empty();
                            $('.append_status_edit').empty();
                            $('.btn_receipt').remove();
                            $('#customer_appointment_id').val(a.customer_appointment_id);
                            $('#customer_appointment_type').val(a.customer_appointment_type);
                            $('#full_name_edit').val(a.full_name);
                            $('#phone1_edit').val(a.phone);
                            $('#branch').val(a.branch_name);
                            if (a.customer_appointment_type == 'appointment' || a.customer_appointment_type == 'booking') {
                                $('#appointment_type').attr('class', 'btn btn-info color_button');
                                $('#direct_type').remove();
                                var tpl = $('#date-edit-tpl').html();
                                $('.date_edit').append(tpl);
                                var tplt = $('#time-edit-tpl').html();
                                $('.time_edit').append(tplt);
                                $('#date_edit').val(a.date);
                                $('#date_edit').datepicker({
                                    startDate: '0d',
                                    language: 'vi',
                                    orientation: "bottom left", todayHighlight: !0,
                                }).on('changeDate', function (ev) {
                                    $(this).datepicker('hide');
                                });

                                $('#time_edit').select2({
                                    placeholder: json['Chọn giờ hẹn'],
                                });
                                // $('#time_edit').selectpicker();
                                let $element = $('#time_edit')
                                let val = $element.find("option:contains('" + a.time + "')").val()
                                $("#time_edit").val(val).trigger('change');
                                // $('#time_edit').val(a.time);
                                var tpl_stt_app = $('#status-appointment-edit-tpl').html();
                                $('.append_status_edit').append(tpl_stt_app);
                                if (a.status == "new") {
                                    $('#new_stt').attr('class', 'btn btn-info  active_edit color_button');
                                    $('#confirm_stt').attr('class', 'btn btn-default ');
                                    $('#wait_stt').attr('class', 'btn btn-default ');
                                    $('#cancel_stt').attr('class', 'btn btn-default ');
                                } else if (a.status == "confirm") {
                                    $('#new_stt').remove();
                                    $('#confirm_stt').attr('class', 'btn btn-info  active_edit color_button');
                                    $('#wait_stt').attr('class', 'btn btn-default ');
                                    $('#cancel_stt').attr('class', 'btn btn-default ');
                                } else if (a.status == "wait") {
                                    $('#new_stt').remove();
                                    $('#confirm_stt').remove();
                                    $('#wait_stt').attr('class', 'btn btn-info  active_edit color_button');
                                    $('#cancel_stt').attr('class', 'btn btn-default ');
                                }
                                if (a.status != 'finish') {
                                    let routess = laroute.route('admin.customer_appointment.receipt');
                                    if ($('#role-receipt-appointments').val() == 1) {
                                        $('.append_btn').append('<a href="' + routess.substring(0, 35) + '/' + a.customer_appointment_id + '" class="btn btn-info m-btn m-btn--icon m-btn--wide m-btn--md btn_receipt color_button son-mb m--margin-left-10"><span><i class="la la-check"></i><span>'+json['THANH TOÁN']+'</span></span></a>');
                                    }
                                }
                            } else {
                                $('#appointment_type').remove();
                                $('#direct_type').attr('class', 'btn btn-info  color_button');
                                $('.date_edit').append('<input class="form-control" id="date_edit" name="date_edit" disabled>');
                                $('.time_edit').append('<input class="form-control" id="time_edit" name="time_edit" disabled>');
                                $('#date_edit').val(a.date);
                                $('#time_edit').val(a.time);
                                var tpl_stt_direct = $('#status-direct-edit-tpl').html();
                                $('.append_status_edit').append(tpl_stt_direct);
                                if (a.status == 'wait') {
                                    $('#wait_stt').attr('class', 'btn btn-info  active_edit color_button');
                                    $('#cancel_stt').attr('class', 'btn btn-default ');
                                } else if (a.status == 'cancel') {
                                    $('#wait_stt').attr('class', 'btn btn-default ');
                                    $('#cancel_stt').attr('class', 'btn btn-info  active_edit color_button');
                                }
                                if (a.status != 'finish') {
                                    let routess = laroute.route('admin.customer_appointment.receipt');
                                    if ($('#role-receipt-appointments').val() == 1) {
                                        $('.append_btn').append('<a href="' + routess.substring(0, 35) + '/' + a.customer_appointment_id + '" class="btn btn-info m-btn m-btn--icon m-btn--wide m-btn--md btn_receipt color_button son-mb m--margin-left-10"><span><i class="la la-check"></i><span>'+json['THANH TOÁN']+'</span></span></a>');
                                    }
                                }
                            }
                            //Khác
                            $('#appointment_source_name').val(a.appointment_source_name);
                            $('#quantity_customer_edit').val(a.customer_quantity);
                            $('#description_edit').val(a.description);
                            $('#table_quantity_edit > tbody').empty();
                            $.ajax({
                                url: laroute.route('admin.customer_appointment.option'),
                                dataType: 'JSON',
                                method: 'POST',
                                data: {},
                                success: function (res1) {
                                    for (let i = 0; i < $('#quantity_customer_edit').val(); i++) {
                                        var stts = $('#table_quantity_edit tr').length;
                                        var tpl = $('#table-quantity-edit-tpl').html();
                                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                                        tpl = tpl.replace(/{stt}/g, i + 1);
                                        $("#table_quantity_edit > tbody").append(tpl);

                                    }

                                    $.each(res1.optionService, function (index, element) {
                                        $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');

                                    });
                                    $.each(res1.optionRoom, function (index, element) {
                                        $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');

                                    });
                                    $.each(res1.optionStaff, function (index, element) {
                                        $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');

                                    });
                                    $('.service_id_edit').select2({
                                        placeholder: json['Chọn dịch vụ'],
                                    }).on('select2:select', function (event) {
                                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                                        $('#room_id_edit_' + id + '').enable(true);
                                        $('#staff_id_edit_' + id + '').enable(true);

                                    }).on('select2:unselect', function (event) {
                                        if ($(this).val() == '') {
                                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                                            $('#room_id_edit_' + id + '').enable(false);
                                            $('#staff_id_edit_' + id + '').enable(false);
                                            $('#room_id_edit_' + id + '').val('').trigger('change');
                                            $('#staff_id_edit_' + id + '').val('').trigger('change');
                                        }

                                    });
                                    $('.room_id_edit').select2({
                                        placeholder: json['Chọn phòng'],
                                    });
                                    $('.staff_id_edit').select2({
                                        placeholder: json['Chọn nhân viên'],
                                    });

                                    $.each(res.data_sv, function (key, value) {
                                        if (a.status == 'new') {
                                            //Khi lịch hẹn mới thì hiện danh sách dịch vụ khách đã chọn.
                                            $('#service_id_edit_' + key + '').enable(true);
                                            if (value != '') {
                                                var selectedValues = value.split(',');

                                                //selected service
                                                $('#service_id_edit_' + key + '').val(selectedValues).trigger("change");
                                                $('#service_id_edit_' + key + '').val(selectedValues).enable("select2:clear");

                                                // $('#service_id_edit_' + key + '').val(selectedValues).prop("disabled", true);
                                                // $('#service_id_edit_' + key + ' option[value="'+selectedValues+'"]').prop('disabled',true);
                                                if ($('#service_id_edit_' + key + '').val() != '') {
                                                    $('#room_id_edit_' + key + '').enable(true);
                                                    $('#staff_id_edit_' + key + '').enable(true);
                                                }
                                            }
                                        } else {
                                            //Khi lịch hẹn đã xác nhận thì hiện danh sách dịch vụ khách đã chọn.
                                            if (value != '') {
                                                var selectedValues = value.split(',');

                                                //selected service
                                                $('#service_id_edit_' + key + '').val(selectedValues).trigger("change");
                                                $('#service_id_edit_' + key + '').val(selectedValues).enable("select2:clear");

                                                // $('#service_id_edit_' + key + '').val(selectedValues).prop("disabled", true);
                                                // $('#service_id_edit_' + key + ' option[value="'+selectedValues+'"]').prop('disabled',true);
                                                if ($('#service_id_edit_' + key + '').val() != '') {
                                                    $('#room_id_edit_' + key + '').enable(true);
                                                    $('#staff_id_edit_' + key + '').enable(true);
                                                }
                                            }
                                            $('#service_id_edit_' + key + '').enable(false);
                                            $('#room_id_edit_' + key + '').enable(false);
                                            $('#staff_id_edit_' + key + '').enable(false);

                                        }

                                    });
                                    $.map(res.data_group, function (a) {
                                        $('#staff_id_edit_' + a.customer_order + '').val(a.staff_id).trigger("change");
                                        $('#room_id_edit_' + a.customer_order + '').val(a.room_id).trigger("change");
                                    });

                                }
                            });
                            $('#quantity_hide_edit').val(a.customer_quantity);
                            $('#time_edit').prop("disabled", true);
                            if (a.status == 'new') {
                                $('#date_edit').prop('disabled', false);
                                $('#time_edit_new').prop("disabled", false);
                                $('#quantity_customer_edit').prop("disabled", false);
                            } else {
                                $('#date_edit').prop('disabled', true);
                                $('#time_edit_new').prop("disabled", true);
                                $('#quantity_customer_edit').prop("disabled", true);
                            }

                        } else {
                            if (a.customer_appointment_type == 'appointment' || a.customer_appointment_type == 'booking') {
                                $('#appointment_type').attr('class', 'btn btn-info color_button');
                                $('#direct_type').remove();
                            } else {
                                $('#direct_type').attr('class', 'btn btn-info color_button');
                                $('#appointment_type').remove();
                            }
                            $('.btn_edit').css('display', 'none');
                            $('.btn_add_cus').css('display', 'none');
                            $('.date_edit').empty();
                            $('.time_edit').empty();
                            $('.append_status_edit').empty();
                            $('.btn_receipt').remove();
                            $('#customer_appointment_id').val(a.customer_appointment_id);
                            $('#customer_appointment_type').val(a.customer_appointment_type);
                            $('#full_name_edit').val(a.full_name);
                            $('#phone1_edit').val(a.phone);
                            $('.date_edit').append('<input class="form-control" id="date_edit" name="date_edit" disabled>');
                            $('.time_edit').append('<input class="form-control" id="time_edit" name="time_edit" disabled>');
                            $('#date_edit').val(a.date);
                            $('#time_edit').val(a.time);
                            var tpl = $('#status-finish-edit-tpl').html();
                            $('.append_status_edit').append(tpl);
                            $('#appointment_source_name').val(a.appointment_source_name);
                            $('#quantity_customer_edit').val(a.customer_quantity);
                            $('#description_edit').val(a.description);
                            $('#quantity_customer_edit').attr("disabled", true);
                            $('#table_quantity_edit > tbody').empty();
                            $.ajax({
                                url: laroute.route('admin.customer_appointment.option'),
                                dataType: 'JSON',
                                method: 'POST',
                                data: {},
                                success: function (res1) {
                                    for (let i = 0; i < $('#quantity_customer_edit').val(); i++) {
                                        var stts = $('#table_quantity_edit tr').length;
                                        var tpl = $('#table-quantity-edit-tpl').html();
                                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                                        tpl = tpl.replace(/{stt}/g, i + 1);
                                        $("#table_quantity_edit > tbody").append(tpl);

                                    }
                                    $.each(res1.optionService, function (index, element) {
                                        $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');

                                    });
                                    $.each(res1.optionRoom, function (index, element) {
                                        $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');

                                    });
                                    $.each(res1.optionStaff, function (index, element) {
                                        $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');

                                    });
                                    $('.service_id_edit').select2({
                                        placeholder: json['Chọn dịch vụ'],

                                    }).on('select2:select', function (event) {
                                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                                        $('#room_id_edit_' + id + '').enable(true);
                                        $('#staff_id_edit_' + id + '').enable(true);

                                    }).on('select2:unselect', function (event) {
                                        if ($(this).val() == '') {
                                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                                            $('#room_id_edit_' + id + '').enable(false);
                                            $('#staff_id_edit_' + id + '').enable(false);
                                            $('#room_id_edit_' + id + '').val('').trigger('change');
                                            $('#staff_id_edit_' + id + '').val('').trigger('change');
                                        }

                                    });
                                    $('.room_id_edit').select2({
                                        placeholder: json['Chọn phòng'],

                                    });
                                    $('.staff_id_edit').select2({
                                        placeholder: json['Chọn nhân viên'],

                                    });

                                    $.each(res.data_sv, function (key, value) {
                                        if (value != '') {
                                            var selectedValues = value.split(',');
                                            $('#service_id_edit_' + key + '').val(selectedValues).trigger("change");
                                            $('#service_id_edit_' + key + '').val(selectedValues).prop("disabled", true);
                                            $('#service_id_edit_' + key + ' option[value="' + selectedValues + '"]').prop('disabled', true);


                                            if ($('#service_id_edit_' + key + '').val() != '') {
                                                $('#room_id_edit_' + key + '').enable(true);
                                                $('#staff_id_edit_' + key + '').enable(true);
                                            }
                                        }
                                        $('#service_id_edit_' + key + '').enable(false);
                                        $('#room_id_edit_' + key + '').enable(false);
                                        $('#staff_id_edit_' + key + '').enable(false);
                                    });
                                    $.map(res.data_group, function (a) {
                                        $('#staff_id_edit_' + a.customer_order + '').val(a.staff_id).trigger("change");
                                        $('#room_id_edit_' + a.customer_order + '').val(a.room_id).trigger("change");
                                    });

                                }
                            });
                            $('#time_edit_new').prop("disabled", true);
                        }

                    });
                    $('#modal-edit').modal('show');
                    if (res.same_branch != 1) {
                        $('.button_edit').remove();
                        $('.btn_receipt').remove();
                    }
                }
            });
        });
    },
    status_edit: function (e) {
        var $this = $(e).find('input[name="status"]').val();
        if ($this == 'wait') {
            $('#wait_stt').attr('class', 'btn btn-info  active_edit active color_button');
            $('#new_stt').attr('class', 'btn btn-default ');
            $('#confirm_stt').attr('class', 'btn btn-default');
            $('#cancel_stt').attr('class', 'btn btn-default')
        } else if ($this == 'cancel') {
            $('#cancel_stt').attr('class', 'btn btn-info active_edit active color_button')
            $('#new_stt').attr('class', 'btn btn-default');
            $('#confirm_stt').attr('class', 'btn btn-default');
            $('#wait_stt').attr('class', 'btn btn-default ');
        } else if ($this == 'new') {
            $('#new_stt').attr('class', 'btn btn-info  active_edit active color_button');
            $('#confirm_stt').attr('class', 'btn btn-default ');
            $('#wait_stt').attr('class', 'btn btn-default');
            $('#cancel_stt').attr('class', 'btn btn-default')
        } else if ($this == 'confirm') {
            $('#new_stt').attr('class', 'btn btn-default');
            $('#confirm_stt').attr('class', 'btn btn-info active_edit active color_button');
            $('#wait_stt').attr('class', 'btn btn-default ');
            $('#cancel_stt').attr('class', 'btn btn-default')
        }
    },
    add_customer_edit: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var quantity = $('#quantity_customer_edit').val();
            $('#quantity_customer_edit').val(parseInt(quantity) + 1);
            var quan_hide = $('#quantity_hide_edit').val();
            $('#quantity_hide_edit').val($('#quantity_customer_edit').val());
            $.ajax({
                url: laroute.route('admin.customer_appointment.option'),
                dataType: 'JSON',
                method: 'POST',
                data: {},
                success: function (res) {
                    // for (let i = 0; i < $('#quantity_customer_edit').val() - quan_hide; i++) {
                    var stts = $('#table_quantity_edit tr').length;
                    var tpl = $('#table-quantity-edit-tpl').html();
                    tpl = tpl.replace(/{stt}/g, stts);
                    tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                    $("#table_quantity_edit > tbody").append(tpl);
                    // }
                    $.each(res.optionService, function (index, element) {
                        $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionRoom, function (index, element) {
                        $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionStaff, function (index, element) {
                        $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $('.service_id_edit').select2({
                        placeholder: json['Chọn dịch vụ'],

                    }).on('select2:select', function (event) {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                        $('#room_id_edit_' + id + '').enable(true);
                        $('#staff_id_edit_' + id + '').enable(true);

                    }).on('select2:unselect', function (event) {
                        if ($(this).val() == '') {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                            $('#room_id_edit_' + id + '').enable(false);
                            $('#staff_id_edit_' + id + '').enable(false);
                            $('#room_id_edit_' + id + '').val('').trigger('change');
                            $('#staff_id_edit_' + id + '').val('').trigger('change');
                        }

                    });
                    $('.staff_id_edit').select2({
                        placeholder: json['Chọn nhân viên'],
                        allowClear: true
                    });
                    $('.room_id_edit').select2({
                        placeholder: json['Chọn phòng'],
                        allowClear: true
                    });
                }
            });
        });
    },
    change_quantity_edit: function (e) {
        $.getJSON(laroute.route('translate'), function (json) {
            if ($(e).val() > $('#quantity_hide_edit').val()) {
                // $('#table_quantity > tbody').empty();
                $.ajax({
                    url: laroute.route('admin.customer_appointment.option'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {},
                    success: function (res) {
                        for (let i = 0; i < $(e).val() - $('#quantity_hide_edit').val(); i++) {
                            var stts = $('#table_quantity_edit tr').length;
                            var tpl = $('#table-quantity-edit-tpl').html();
                            tpl = tpl.replace(/{stt}/g, stts);
                            tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                            $("#table_quantity_edit > tbody").append(tpl);
                        }
                        $.each(res.optionService, function (index, element) {
                            $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $.each(res.optionRoom, function (index, element) {
                            $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $.each(res.optionStaff, function (index, element) {
                            $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $('.service_id_edit').select2({
                            placeholder: json['Chọn dịch vụ'],
                        }).on('select2:select', function (event) {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                            $('#room_id_edit_' + id + '').enable(true);
                            $('#staff_id_edit_' + id + '').enable(true);

                        }).on('select2:unselect', function (event) {
                            if ($(this).val() == '') {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                                $('#room_id_edit_' + id + '').enable(false);
                                $('#staff_id_edit_' + id + '').enable(false);
                                $('#room_id_edit_' + id + '').val('').trigger('change');
                                $('#staff_id_edit_' + id + '').val('').trigger('change');
                            }

                        });
                        $('.staff_id_edit').select2({
                            placeholder: json['Chọn nhân viên'],
                        });
                        $('.room_id_edit').select2({
                            placeholder: json['Chọn phòng'],
                        });
                        $('#quantity_hide_edit').val($(e).val());
                    }
                });
            } else {
                $('#quantity_hide_edit').val($(e).val());
                $('#table_quantity_edit > tbody').empty();
                $.ajax({
                    url: laroute.route('admin.customer_appointment.option'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {},
                    success: function (res) {
                        for (let i = 0; i < $(e).val(); i++) {
                            var stts = $('#table_quantity_edit tr').length;
                            var tpl = $('#table-quantity-edit-tpl').html();
                            tpl = tpl.replace(/{stt}/g, stts);
                            tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                            $("#table_quantity_edit > tbody").append(tpl);
                        }
                        $.each(res.optionService, function (index, element) {
                            $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $.each(res.optionRoom, function (index, element) {
                            $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $.each(res.optionStaff, function (index, element) {
                            $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');
                        });
                        $('.service_id_edit').select2({
                            placeholder: json['Chọn dịch vụ'],
                        }).on('select2:select', function (event) {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                            $('#room_id_edit_' + id + '').enable(true);
                            $('#staff_id_edit_' + id + '').enable(true);

                        }).on('select2:unselect', function (event) {
                            if ($(this).val() == '') {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                                $('#room_id_edit_' + id + '').enable(false);
                                $('#staff_id_edit_' + id + '').enable(false);
                                $('#room_id_edit_' + id + '').val('').trigger('change');
                                $('#staff_id_edit_' + id + '').val('').trigger('change');
                            }

                        });
                        $('.staff_id_edit').select2({
                            placeholder: json['Chọn nhân viên'],
                        });
                        $('.room_id_edit').select2({
                            placeholder: json['Chọn phòng'],
                        });
                    }
                });
            }
        });
    },
    submit_edit: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#form-edit').validate({
                rules: {
                    time_edit: {
                        required: true
                    },
                    date_edit: {
                        required: true
                    },
                    quantity_customer_edit: {
                        min: 1,
                        required: true,
                        number: true,
                        max: 10
                    }
                },
                messages: {
                    time_edit: {
                        required: json['Hãy chọn giờ hẹn']
                    },
                    date_edit: {
                        required: json['Hãy chọn ngày hẹn']
                    },
                    quantity_customer_edit: {
                        min: json['Số lượng khách hàng tối thiểu 1'],
                        required: json['Hãy nhập số lượng khách hàng'],
                        number: json['Số lượng khách hàng không hợp lệ'],
                        max: json['Số lượng khách hàng tối đa 10']
                    }
                },
                submitHandler: function () {
                    mApp.block("#m_blockui_2_content", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var time_edit = $('#time_edit').val();
                    var date_edit = $('#date_edit').val();
                    var customer_appointment_id = $('#customer_appointment_id').val();
                    var customer_appointment_type = $('#customer_appointment_type').val();
                    var description = $('#description_edit').val();
                    var customer_quantity = $('#quantity_customer_edit').val();
                    var status = $('.active_edit ').find('input[name="status"]').val();
                    var table_quantity = [];
                    for (let i = 0; i < customer_quantity; i++) {
                        var stt = i + 1;
                        var input = $('#customer_order_edit_' + stt + '').val();
                        var sv = '';
                        if ($('#service_id_edit_' + stt + '').val() != '') {
                            sv = $('#service_id_edit_' + stt + '').val();
                        }
                        var room = $('#room_id_edit_' + stt + '').val();
                        var staff = $('#staff_id_edit_' + stt + '').val();
                        var arr = {
                            stt: input,
                            sv: sv,
                            staff: staff,
                            room: room
                        };
                        table_quantity.push(arr);
                    }
                    $.ajax({
                        url: laroute.route('admin.customer_appointment.submitModalEdit'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {
                            date: date_edit,
                            time: time_edit,
                            customer_appointment_id: customer_appointment_id,
                            customer_appointment_type: customer_appointment_type,
                            description: description,
                            customer_quantity: customer_quantity,
                            status: status,
                            table_quantity: table_quantity,
                            time_edit_new: $('#time_edit_new').val(),
                            customer_id: $('#customer_id').val()
                        },
                        success: function (res) {
                            mApp.unblock("#m_blockui_2_content");
                            if (res.time_error == 1) {
                                $('.error_time_edit').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                            }
                            if (res.status == 1) {
                                swal(json["Cập nhật lịch hẹn thành công"], "", "success");
                                $('#modal-edit').modal('hide');
                                window.location.reload();
                            }
                        }
                    })
                }
            });
        });
    },
    out_modal: function () {
        window.location.reload();
    }
};
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.customer_appointment.search-time')
});


function arr_diff(a1, a2) {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}


function addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                 customer_hidden, description, table_quantity, status) {
    $.getJSON(laroute.route('translate'), function (json) {
        $.ajax({
            url: laroute.route('admin.customer_appointment.submitModalAdd'),
            data: {
                full_name: full_name,
                phone1: phone1,
                customer_appointment_type: type,
                appointment_source_id: appointment_source_id,
                customer_quantity: customer_quantity,
                date: date,
                time: time,
                customer_hidden: customer_hidden,
                description: description,
                table_quantity: table_quantity,
                status: status
            },
            method: 'POST',
            dataType: "JSON",
            success: function (response) {
                if (response.status == 'phone_exist') {
                    $('.error-phone1').text(json['Số điện thoại đã tồn tại']);
                }
                if (response.time_error == 1) {
                    $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                }
                if (response.status == 1) {
                    window.location.reload();
                    swal(json["Thêm lịch hẹn thành công"], "", "success");
                }

            }
        });
    });

}

function updateLoad(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity) {
    $.getJSON(laroute.route('translate'), function (json) {
        $.ajax({
            url: laroute.route('admin.customer_appointment.update-number-appointment'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                customer_id: customer_hidden,
                date: date,
                time: time,
                type: type,
                status: status,
                appointment_source_id: appointment_source_id,
                description: description,
                customer_quantity: customer_quantity,
                table_quantity: table_quantity
            },
            success: function (res) {
                if (res.status == 1) {
                    window.location.reload();
                    swal(json["Cập nhật lịch hẹn thành công"], "", "success");
                }
            }
        })
    });

}

function addReset(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                  customer_hidden, description, table_quantity, status) {
    $.getJSON(laroute.route('translate'), function (json) {
        $.ajax({
            url: laroute.route('admin.customer_appointment.submitModalAdd'),
            data: {
                full_name: full_name,
                phone1: phone1,
                customer_appointment_type: type,
                appointment_source_id: appointment_source_id,
                customer_quantity: customer_quantity,
                date: date,
                time: time,
                customer_hidden: customer_hidden,
                description: description,
                table_quantity: table_quantity,
                status: status
            },
            method: 'POST',
            dataType: "JSON",
            success: function (response) {
                if (response.status == 'phone_exist') {
                    $('.error-phone1').text(json['Số điện thoại đã tồn tại']);
                }
                if (response.time_error == 1) {
                    $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                }
                if (response.status == 1) {
                    swal(json["Thêm lịch hẹn thành công"], "", "success");
                    $('#full_name').val('');
                    $('#phone1').val('');
                    $('#time').val('').trigger('change');
                    $('#quantity_customer').val(1);
                    $('#description').val('');
                    $("#table_quantity > tbody").empty();
                    $.ajax({
                        url: laroute.route('admin.customer_appointment.option'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {},
                        success: function (res) {
                            for (let i = 0; i < $('#quantity_customer').val(); i++) {
                                var stts = $('#table_quantity tr').length;
                                var tpl = $('#table-quantity-tpl').html();
                                tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                                tpl = tpl.replace(/{stt}/g, i + 1);
                                $("#table_quantity > tbody").append(tpl);

                            }
                            $.each(res.optionService, function (index, element) {
                                $('.service_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $.each(res.optionRoom, function (index, element) {
                                $('.room_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $.each(res.optionStaff, function (index, element) {
                                $('.staff_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $('.service_id').select2({
                                placeholder: json['Chọn dịch vụ'],

                            }).on('select2:select', function (event) {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                $('#room_id_' + id + '').enable(true);
                                $('#staff_id_' + id + '').enable(true);

                            }).on('select2:unselect', function (event) {
                                if ($(this).val() == '') {
                                    var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                    $('#room_id_' + id + '').enable(false);
                                    $('#staff_id_' + id + '').enable(false);
                                    $('#room_id_' + id + '').val('').trigger('change');
                                    $('#staff_id_' + id + '').val('').trigger('change');
                                }

                            });
                            $('.room_id').select2({
                                placeholder: json['Chọn phòng'],
                            });
                            $('.staff_id').select2({
                                placeholder: json['Chọn nhân viên'],
                            });

                        }
                    });
                }
            }


        });
    });

}

function updateReset(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity) {
    $.getJSON(laroute.route('translate'), function (json) {
        $.ajax({
            url: laroute.route('admin.customer_appointment.update-number-appointment'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                customer_id: customer_hidden,
                date: date,
                time: time,
                type: type,
                status: status,
                appointment_source_id: appointment_source_id,
                description: description,
                customer_quantity: customer_quantity,
                table_quantity: table_quantity
            },
            success: function (res) {
                if (res.status == 1) {
                    swal(json["Cập nhật lịch hẹn thành công"], "", "success");
                    $('#full_name').val('');
                    $('#phone1').val('');
                    $('#time').val('').trigger('change');
                    $('#quantity_customer').val(1);
                    $('#description').val('');
                    $("#table_quantity > tbody").empty();
                    $.ajax({
                        url: laroute.route('admin.customer_appointment.option'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {},
                        success: function (res) {
                            for (let i = 0; i < $('#quantity_customer').val(); i++) {
                                var stts = $('#table_quantity tr').length;
                                var tpl = $('#table-quantity-tpl').html();
                                tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                                tpl = tpl.replace(/{stt}/g, i + 1);
                                $("#table_quantity > tbody").append(tpl);

                            }
                            $.each(res.optionService, function (index, element) {
                                $('.service_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $.each(res.optionRoom, function (index, element) {
                                $('.room_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $.each(res.optionStaff, function (index, element) {
                                $('.staff_id').append('<option value="' + index + '">' + element + '</option>');

                            });
                            $('.service_id').select2({
                                placeholder: json['Chọn dịch vụ'],

                            }).on('select2:select', function (event) {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                $('#room_id_' + id + '').enable(true);
                                $('#staff_id_' + id + '').enable(true);

                            }).on('select2:unselect', function (event) {
                                if ($(this).val() == '') {
                                    var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                    $('#room_id_' + id + '').enable(false);
                                    $('#staff_id_' + id + '').enable(false);
                                    $('#room_id_' + id + '').val('').trigger('change');
                                    $('#staff_id_' + id + '').val('').trigger('change');
                                }

                            });
                            $('.room_id').select2({
                                placeholder: json['Chọn phòng'],
                            });
                            $('.staff_id').select2({
                                placeholder: json['Chọn nhân viên'],
                            });

                        }
                    });
                }
            }
        })
    });

}
