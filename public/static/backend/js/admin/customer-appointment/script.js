

$(document).ready(function () {
    $('#autotable').PioTable({
        baseUrl: laroute.route('admin.customer-appointment.list')
    });
    $('#refresh').click(function () {
        $('#autotable').PioTable('refresh');
    });

    $.getJSON(laroute.route('translate'), function (json) {
        $('#month').click(function () {
            $('.list-calendar').css('display', 'block');
            $('.list').css('display', 'none');
        });

    
    
        $('#search').click(function () {
            $('.table-list').find('tbody tr').empty();
        });
        $('#phone1').keydown(function () {
            $.ajax({
                url: laroute.route('admin.customer_appointment.search-phone'),
                dataType: 'JSON',
                method: 'POST',
                data: {
                    phone: $(this).val()
                },
                success: function (res) {
                    var arr = [];
                    $.map(res.list_phone, function (a) {
                        arr.push(a.phone);
                    });
                    $('#phone1').autocomplete({
                        source: arr,
                        change: function () {
                            var phone = $(this).val();
                            $.ajax({
                                url: laroute.route('admin.customer_appointment.cus-phone'),
                                dataType: 'JSON',
                                method: 'post',
                                data: {
                                    phone: phone
                                },
                                success: function (res) {
                                    console.log(res);
                                    if (res.success == 1) {
                                        $('#customer_hidden').val(res.cus.customer_id);
                                        $('#full_name').val(res.cus.full_name);
                                        $('#full_name').attr('disabled',true);
                                    }
                                    if (res.phone_new == 1) {
                                        $('#customer_hidden').val('');
                                        $('#full_name').val('');
                                        $('#full_name').attr('disabled',false);
                                    }
                                }
                            })
                        }
                    });
                }
            })
        });
        $('#customer_id').select2({
            placeholder: json['Nhập thông tin khách hàng'],
            ajax: {
                url: laroute.route('admin.customer_appointment.search'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1
                    };
                    return query;
                }
            },
            minimumInputLength: 1,
            allowClear: true,
        }).on('select2:unselect', function (e) {
            $('#customer_hidden').val('');
            $('#full_name').val('');
            $('#phone1').val('');
        });

        $('#search_refer').select2({
            placeholder: json['Nhập thông tin người giới thiệu'],
            ajax: {
                url: laroute.route('admin.customer_appointment.search'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {
                    var query = {
                        search: params.term,
                        page: params.page || 1
                    };
                    return query;
                },
                processResults: function (response) {
                    console.log(response);
                    response.page = response.page || 1;
                    return {
                        results: response.search.results,
                        pagination: {
                            more: response.pagination
                        }
                    };
                },
                cache: true,
                delay: 250,
    
    
            },
            // minimumInputLength: 1,
            // minimumResultsForSearch:0,
            allowClear: true,
            language: "vi",
        });
        $('#service_id').select2({
            placeholder: json['Chọn dịch vụ'],
            // ajax: {
            //     url: laroute.route('admin.customer_appointment.search-service'),
            //     dataType: 'json',
            //     delay: 250,
            //     type: 'POST',
            //     data: function (params) {
            //         var query = {
            //             search: params.term,
            //             page: params.page || 1
            //         };
            //         return query;
            //     }
            // },
            // minimumInputLength: 1,
        });
        $('#service_id').on('select2:select', function (event) {
            $('#service_id').val('').trigger('change');
            $('#table').css('display', 'block');
            var check = true;
            $.each($('#table_service tbody tr'), function () {
                let codeHidden = $(this).find("td input[name='service_name_hidden']");
                let codeExists = codeHidden.val();
                var code = event.params.data.id;
                if (codeExists == code) {
                    check = false;
                    let quantitySv = codeHidden.parents('tr').find('input[name="quantity"]').val();
                    // console.log(quantitySv);
                    let numbers = parseInt(quantitySv) + 1;
                    codeHidden.parents('tr').find('input[name="quantity"]').val(numbers);
                }
            });
            if (check == true) {
                $.ajax({
                    url: laroute.route('admin.customer_appointment.load-time'),
                    dataType: 'JSON',
                    data: {
                        id: event.params.data.id
                    },
                    method: 'POST',
                    success: function (res) {
                        var tpl = $('#service-tpl').html();
                        var stts = $('#table_service tr').length;
                        tpl = tpl.replace(/{stt}/g, stts);
                        tpl = tpl.replace(/{service_name}/g, event.params.data.text);
                        tpl = tpl.replace(/{service_name_id}/g, event.params.data.id);
                        tpl = tpl.replace(/{time}/g, res.time);
                        $('#table_service > tbody').append(tpl);
                        $(".quantity").TouchSpin({
                            initval: 1,
                            min: 1
                        });
                        $('.remove_service').click(function () {
                            $(this).closest('.service_tb').remove();
                            //alert('ok');
                        });
                    }
                });
    
    
            }
    
    
        });
        $('.btn_add').click(function () {
            $('#form-add').validate({
                rules: {
                    full_name: {
                        required: true
                    },
                    phone1: {
                        required: true,
                        minlength: 10,
                        maxlength: 11,
                        number: true
                    },
                    time: {
                        required: true
                    },
                    date: {
                        required: true
                    },
                    quantity_customer: {
                        min: 1,
                        required: true,
                        number: true,
                        max: 10,
                    }
                },
                messages: {
                    full_name: {
                        required: json['Hãy nhập tên khách hàng']
                    },
                    phone1: {
                        required: json['Hãy nhập số điện thoại'],
                        minlength: json['Số điện thoại tối thiểu 10 số'],
                        maxlength: json['Số điện thoại tối đa 11 số'],
                        number: json['Số điện thoại không hợp lệ']
                    },
                    time: {
                        required: json['Hãy chọn giờ hẹn']
                    },
                    date: {
                        required: json['Hãy chọn ngày hẹn']
                    },
                    quantity_customer: {
                        min: json['Số lượng khách hàng tối thiểu 1'],
                        required: json['Hãy nhập số lượng khách hàng'],
                        number: json['Số lượng khách hàng không hợp lệ'],
                        max: json['Số lượng khách hàng tối đa 10'],
                    }
                },
                submitHandler: function () {
                    // mApp.block("#m_blockui_1_content", {
                    //     overlayColor: "#000000",
                    //     type: "loader",
                    //     state: "success",
                    //     message: json["Đang tải..."]
                    // });
                    var full_name = $('#full_name').val();
                    var phone1 = $('#phone1').val();
                    var type = $('.source').find('.active input[name="customer_appointment_type"]').val();
                    var date = $('#date').val();
                    var time = $('#time').val();
                    var customer_hidden = $('#customer_hidden').val();
                    var description = $('#description').val();
                    var customer_quantity = $('#quantity_customer').val();
                    var appointment_source_id = $('#appointment_source_id').val();
                    // var customer_refer = $('#search_refer').val();
                    var status = $('.active').find(' input[name="status"]').val();
                    var table_quantity = [];
                    for (let i = 0; i < customer_quantity; i++) {
                        var stt = i + 1;
                        var input = $('#customer_order_' + stt + '').val();
                        var sv = '';
                        if ($('#service_id_' + stt + '').val() != '') {
                            sv = $('#service_id_' + stt + '').val();
                        }
                        var room = $('#room_id_' + stt + '').val();
                        var staff = $('#staff_id_' + stt + '').val();
                        var arr = {
                            stt: input,
                            sv: sv,
                            staff: staff,
                            room: room
                        };
                        table_quantity.push(arr);
                    }
                    //kiểm tra khách hàng đã có lịch hẹn ngày hôm nay chưa
                    if (customer_hidden != '') {
                        $.ajax({
                            url: laroute.route('admin.customer_appointment.check-number-appointment'),
                            method: 'POST',
                            dataType: 'JSON',
                            data: {
                                customer_id: customer_hidden,
                                date: date,
                                time: time,
                            },
                            success: function (res) {
                                mApp.unblock("#m_blockui_1_content");
                                console.log(res);
                                if (res.time_error == 1) {
                                    $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                                }
                                if (res.status == 0) {
                                    addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                        customer_hidden, description, table_quantity, status);
                                }
                                if (res.status == 1) {
                                    if (res.number < 3) {
                                        swal({
                                            title: json["Khách đã có lịch hẹn hôm nay lúc"] + " " + res.time,
                                            text: "",
                                            type: "warning",
                                            showCancelButton: !0,
                                            confirmButtonText: json["THÊM MỚI"],
                                            cancelButtonText: json["CẬP NHẬT"]
                                        });
                                        $('.swal2-confirm').click(function () {
                                            addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                                customer_hidden, description, table_quantity, status);
                                        });
                                        $('.swal2-cancel').click(function () {
                                            updateLoad(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        });
                                    } else {
                                        swal({
                                            title: json["Khách hàng đã đặt tối đa 3 lịch hẹn trong hôm nay"],
                                            text: "",
                                            type: "warning",
                                            confirmButtonText: json["Cập nhật lịch gần nhất"],
                                            confirmButtonClass: "btn btn-focus m-btn m-btn--pill m-btn--air"
                                        });
                                        $('.swal2-confirm').click(function () {
                                            updateLoad(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        })
                                    }
                                }
                            }
                        });
                    } else {
                        addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                            customer_hidden, description, table_quantity, status);
                    }
                }
            });
        });
        $('.btn_new').click(function () {
            $('#form-add').validate({
                rules: {
                    full_name: {
                        required: true
                    },
                    phone1: {
                        required: true,
                        minlength: 10,
                        maxlength: 11,
                        number: true
                    },
                    time: {
                        required: true
                    },
                    date: {
                        required: true
                    },
                    quantity_customer: {
                        min: 1,
                        required: true,
                        number: true,
                        max: 10,
                    }
                },
                messages: {
                    full_name: {
                        required: json['Hãy nhập tên khách hàng']
                    },
                    phone1: {
                        required: json['Hãy nhập số điện thoại'],
                        minlength: json['Số điện thoại tối thiểu 10 số'],
                        maxlength: json['Số điện thoại tối đa 11 số'],
                        number: json['Số điện thoại không hợp lệ']
                    },
                    time: {
                        required: json['Hãy chọn giờ hẹn']
                    },
                    date: {
                        required: json['Hãy chọn ngày hẹn']
                    },
                    quantity_customer: {
                        min: json['Số lượng khách hàng tối thiểu 1'],
                        required: json['Hãy nhập số lượng khách hàng'],
                        number: json['Số lượng khách hàng không hợp lệ'],
                        max: json['Số lượng khách hàng tối đa 10']
                    }
                },
                submitHandler: function () {
                    mApp.block("#m_blockui_1_content", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: json["Đang tải..."]
                    });
                    var full_name = $('#full_name').val();
                    var phone1 = $('#phone1').val();
                    var type = $('.source').find('.active input[name="customer_appointment_type"]').val();
                    var date = $('#date').val();
                    var time = $('#time').val();
                    var customer_hidden = $('#customer_hidden').val();
                    var description = $('#description').val();
                    var customer_quantity = $('#quantity_customer').val();
                    var appointment_source_id = $('#appointment_source_id').val();
                    // var customer_refer = $('#search_refer').val();
                    var status = $('.active').find(' input[name="status"]').val();
                    var table_quantity = [];
                    for (let i = 0; i < customer_quantity; i++) {
                        var stt = i + 1;
                        var input = $('#customer_order_' + stt + '').val();
                        var sv = '';
                        if ($('#service_id_' + stt + '').val() != '') {
                            sv = $('#service_id_' + stt + '').val();
                        }
                        var room = $('#room_id_' + stt + '').val();
                        var staff = $('#staff_id_' + stt + '').val();
                        var arr = {
                            stt: input,
                            sv: sv,
                            staff: staff,
                            room: room
                        };
                        table_quantity.push(arr);
                    }
                    //kiểm tra khách hàng đã có lịch hẹn ngày hôm nay chưa
                    if (customer_hidden != '') {
                        $.ajax({
                            url: laroute.route('admin.customer_appointment.check-number-appointment'),
                            method: 'POST',
                            dataType: 'JSON',
                            data: {
                                customer_id: customer_hidden,
                                date: date,
                                time: time,
                            },
                            success: function (res) {
                                mApp.unblock("#m_blockui_1_content");
                                if (res.time_error == 1) {
                                    $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                                }
                                if (res.status == 0) {
                                    addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                        customer_hidden, description, table_quantity, status);
                                }
                                if (res.status == 1) {
                                    if (res.number < 3) {
                                        swal({
                                            title: json["Khách đã có lịch hẹn hôm nay lúc"] + " " + res.time,
                                            text: "",
                                            type: "warning",
                                            showCancelButton: !0,
                                            confirmButtonText: json["THÊM MỚI"],
                                            cancelButtonText: json["CẬP NHẬT"]
                                        });
                                        $('.swal2-confirm').click(function () {
                                            addReset(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                                                customer_hidden, description, table_quantity, status);
                                        });
                                        $('.swal2-cancel').click(function () {
                                            updateReset(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        });
                                    } else {
                                        swal({
                                            title: json["Khách hàng đã đặt tối đa 3 lịch hẹn trong hôm nay"],
                                            text: "",
                                            type: "warning",
                                            confirmButtonText: json["Cập nhật lịch gần nhất"],
                                            confirmButtonClass: "btn btn-focus m-btn m-btn--pill m-btn--air"
                                        });
                                        $('.swal2-confirm').click(function () {
                                            updateReset(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity);
                                        })
                                    }
                                }
                            }
                        });
                    } else {
                        addReset(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                            customer_hidden, description, table_quantity, status);
                    }
                }
            });
        });
    
        $('#appointment_source_id').selectpicker();
        let $element = $('#appointment_source_id')
        let val = $element.find("option:contains('" + json['gọi điện'] + "')").val()
        $("#appointment_source_id").val(val).trigger('change');
    });
    
});

$.getJSON(laroute.route('translate'), function (json) {
var customer_appointment = {
    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');

        swal({
            title: json['Thông báo'],
            text: json["Bạn có muốn xóa không?"],
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                // remove hightlight row
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.customer.remove', {id: id}), function () {
                    swal(
                        'Xóa thành công',
                        '',
                        'success'
                    );
                    // window.location.reload();
                    $('#autotable').PioTable('refresh');
                });
            }
        });

    },
    changeStatus: function (obj, id, action) {
        $.ajax({
            url: laroute.route('admin.service_category.change-status'),
            method: "POST",
            data: {
                id: id, action: action
            },
            dataType: "JSON"
        }).done(function (data) {
            $('#autotable').PioTable('refresh');
        });
    },
    add_refer: function (close) {
        $('#type_add').val(close);
        $('#form_refer').validate({
            rules: {
                full_name_refer: {
                    required: true
                },
                phone1_refer: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 15
                }
            },
            messages: {
                full_name_refer: {
                    required: json['Hãy nhập tên người giới thiệu']
                },
                phone1_refer: {
                    required: json['Hãy nhập số điện thoại'],
                    number: json['Số điện thoại không hợp lệ'],
                    minlength: json['Số điện thoại tối thiểu 10 số'],
                    maxlength: json['Số điện thoại tối đa 11 số']
                }
            },
            submitHandler: function () {
                var full_name = $('#full_name_refer').val();
                var phone1 = $('#phone1_refer').val();
                var input = $('#type_add');
                $.ajax({
                    url: laroute.route('admin.customer_appointment.add-refer'),
                    data: {
                        full_name: full_name,
                        phone1: phone1,
                        close: input.val()
                    },
                    method: 'POST',
                    dataType: "JSON",
                    success: function (response) {
                        // console.log(response.id_add);
                        if (response.status == 1) {
                            if (response.close != 0) {
                                $("#refer").modal("hide");
                            }
                            $('#form_refer')[0].reset();
                            swal(json["Thêm người giới thiệu thành công"], "", "success");

                            $('#autotable').PioTable('refresh');
                            // $('#div-refer').append('<input class="form-control" type="hidden" readonly ' +
                            //     'name="customer_refer" id="customer_refer" value=' + response.id_add + '>');
                            // $('#div-refer').append('<input class="form-control m--margin-top-10" type="text" readonly ' +
                            //     'name="customer_refer_text" id="customer_refer_text" value="' + response.data['full_name'] + '">');
                        } else {
                            $('.error-phone').text(json['Số điện thoại đã tồn tại']);
                        }
                    }
                });
            }
        });


    },
    new_click: function () {
        $('#new').attr('class', 'btn btn-info  color_button active');
        $('#confirm').attr('class', 'btn btn-default');
    },
    confirm_click: function () {
        $('#confirm').attr('class', 'btn btn-info  color_button active');
        $('#new').attr('class', 'btn btn-default ');
    },
    appointment: function (e) {
        $('.date_app').empty();
        $('.time_app').empty();
        $(e).attr('class', 'btn btn-info  color_button active');
        $('#direct').attr('class', 'btn btn-default ');
        $('.append_status').empty();
        var tpl = $('#append-status-other-tpl').html();
        $('.append_status').append(tpl);
        var tpl_date = $('#date-tpl').html();
        $('.date_app').append(tpl_date);
        var tpl_time = $('#time-tpl').html();
        $('.time_app').append(tpl_time);
        $('#date').datepicker({
            startDate: '0d',
            language: 'vi',
            orientation: "bottom left", todayHighlight: !0,
        });
        $('#date').val($('#day_click').val());
        // $('#time').select2({
        //     placeholder: json['Chọn giờ hẹn'],
        // });
        $('#time').selectpicker();
        let name = json['gọi điện'];
        let $element = $('#appointment_source_id')
        let val = $element.find("option:contains('" + name + "')").val()
        $("#appointment_source_id").val(val).trigger('change');
    },
    direct: function (e) {
        $('.date_app').empty();
        $('.time_app').empty();
        $(e).attr('class', 'btn btn-info  color_button active');
        $('#appointment').attr('class', 'btn btn-default ');
        $('.append_status').empty();
        var tpl = $('#append-status-live-tpl').html();
        $('.append_status').append(tpl);
        $('.date_app').append('<input id="date" name="date" class="form-control m-input" value="' + $('#day_now').val() + '" disabled="disabled">');
        $('.time_app').append('<input id="time" name="time" class="form-control m-input" value="' + $('#time_now').val() + '" disabled="disabled">');
        $("#appointment_source_id").val('1').trigger('change');
    },
    add_customer: function () {
        var quantity = $('#quantity_customer').val();
        $('#quantity_customer').val(parseInt(quantity) + 1);
        var quan_hide = $('#quantity_hide').val();
        if ($('#quantity_customer').val() > quan_hide) {
            $.ajax({
                url: laroute.route('admin.customer_appointment.option'),
                dataType: 'JSON',
                method: 'POST',
                data: {},
                success: function (res) {
                    for (let i = 0; i < quantity - quan_hide; i++) {
                        var stts = $('#table_quantity tr').length;
                        var tpl = $('#table-quantity-tpl').html();
                        tpl = tpl.replace(/{stt}/g, stts);
                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                        $("#table_quantity > tbody").append(tpl);
                    }
                    $.each(res.optionService, function (index, element) {
                        $('.service_id').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionRoom, function (index, element) {
                        $('.room_id').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionStaff, function (index, element) {
                        $('.staff_id').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $('.service_id').select2({
                        placeholder: json['Chọn dịch vụ'],
                    }).on('select2:select', function (event) {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                        $('#room_id_' + id + '').enable(true);
                        $('#staff_id_' + id + '').enable(true);

                    }).on('select2:unselect', function (event) {
                        if ($(this).val() == '') {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                            $('#room_id_' + id + '').enable(false);
                            $('#staff_id_' + id + '').enable(false);
                            $('#room_id_' + id + '').val('').trigger('change');
                            $('#staff_id_' + id + '').val('').trigger('change');
                        }

                    });
                    $('.staff_id').select2({
                        placeholder: json['Chọn nhân viên'],
                    });
                    $('.room_id').select2({
                        placeholder: json['Chọn phòng'],
                    });
                }
            });
            $('#quantity_hide').val(quantity);
        }
        // $('#table_quantity > tbody').empty();


    },
    change_quantity: function (e) {

        $('#table_quantity > tbody').empty();
        $.ajax({
            url: laroute.route('admin.customer_appointment.option'),
            dataType: 'JSON',
            method: 'POST',
            data: {},
            success: function (res) {
                for (let i = 0; i < $(e).val(); i++) {
                    var stts = $('#table_quantity tr').length;

                    var tpl = $('#table-quantity-tpl').html();
                    tpl = tpl.replace(/{stt}/g, i + 1);
                    tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                    $("#table_quantity > tbody").append(tpl);
                }
                $.each(res.optionService, function (index, element) {
                    $('.service_id').append('<option value="' + index + '">' + element + '</option>');
                });
                $.each(res.optionRoom, function (index, element) {
                    $('.room_id').append('<option value="' + index + '">' + element + '</option>');
                });
                $.each(res.optionStaff, function (index, element) {
                    $('.staff_id').append('<option value="' + index + '">' + element + '</option>');
                });
                $('.service_id').select2({
                    placeholder: json['Chọn dịch vụ'],
                }).on('select2:select', function (event) {
                    var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                    $('#room_id_' + id + '').enable(true);
                    $('#staff_id_' + id + '').enable(true);

                }).on('select2:unselect', function (event) {
                    if ($(this).val() == '') {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                        $('#room_id_' + id + '').enable(false);
                        $('#staff_id_' + id + '').enable(false);
                        $('#room_id_' + id + '').val('').trigger('change');
                        $('#staff_id_' + id + '').val('').trigger('change');
                    }

                });
                $('.staff_id').select2({
                    placeholder: json['Chọn nhân viên'],
                });
                $('.room_id').select2({
                    placeholder: json['Chọn phòng'],
                });
            }

        });
        $('#quantity_hide').val($(e).val() - 1);
    },
    status_edit: function (e) {
        var $this = $(e).find('input[name="status"]').val();
        if ($this == 'wait') {
            $('#wait_stt').attr('class', 'btn btn-info  active_edit active color_button');
            $('#new_stt').attr('class', 'btn btn-default ');
            $('#confirm_stt').attr('class', 'btn btn-default ');
            $('#cancel_stt').attr('class', 'btn btn-default');
        } else if ($this == 'cancel') {
            $('#cancel_stt').attr('class', 'btn btn-info active_edit active color_button')
            $('#new_stt').attr('class', 'btn btn-default');
            $('#confirm_stt').attr('class', 'btn btn-default');
            $('#wait_stt').attr('class', 'btn btn-default');
        } else if ($this == 'new') {
            $('#new_stt').attr('class', 'btn btn-info active_edit active color_button');
            $('#confirm_stt').attr('class', 'btn btn-default');
            $('#wait_stt').attr('class', 'btn btn-default');
            $('#cancel_stt').attr('class', 'btn btn-default');
        } else if ($this == 'confirm') {
            $('#new_stt').attr('class', 'btn btn-default');
            $('#confirm_stt').attr('class', 'btn btn-info active_edit active color_button');
            $('#wait_stt').attr('class', 'btn btn-default');
            $('#cancel_stt').attr('class', 'btn btn-default')
        }

    },
    add_customer_edit: function () {
        var quantity = $('#quantity_customer_edit').val();
        $('#quantity_customer_edit').val(parseInt(quantity) + 1);
        var quan_hide = $('#quantity_hide_edit').val();
        $('#quantity_hide_edit').val($('#quantity_customer_edit').val());
        $.ajax({
            url: laroute.route('admin.customer_appointment.option'),
            dataType: 'JSON',
            method: 'POST',
            data: {},
            success: function (res) {
                // for (let i = 0; i < $('#quantity_customer_edit').val() - quan_hide; i++) {
                var stts = $('#table_quantity_edit tr').length;
                var tpl = $('#table-quantity-edit-tpl').html();
                tpl = tpl.replace(/{stt}/g, stts);
                tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                $("#table_quantity_edit > tbody").append(tpl);
                // }
                $.each(res.optionService, function (index, element) {
                    $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');
                });
                $.each(res.optionRoom, function (index, element) {
                    $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');
                });
                $.each(res.optionStaff, function (index, element) {
                    $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');
                });
                $('.service_id_edit').select2({
                    placeholder: json['Chọn dịch vụ'],
                }).on('select2:select', function (event) {
                    var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                    $('#room_id_edit_' + id + '').enable(true);
                    $('#staff_id_edit_' + id + '').enable(true);

                }).on('select2:unselect', function (event) {
                    if ($(this).val() == '') {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                        $('#room_id_edit_' + id + '').enable(false);
                        $('#staff_id_edit_' + id + '').enable(false);
                        $('#room_id_edit_' + id + '').val('').trigger('change');
                        $('#staff_id_edit_' + id + '').val('').trigger('change');
                    }

                });
                $('.staff_id_edit').select2({
                    placeholder: json['Chọn nhân viên'],
                    allowClear: true
                });
                $('.room_id_edit').select2({
                    placeholder: json['Chọn phòng'],
                    allowClear: true
                });
            }
        });


    },
    change_quantity_edit: function (e) {
        if ($(e).val() > $('#quantity_hide_edit').val()) {
            // $('#table_quantity > tbody').empty();
            $.ajax({
                url: laroute.route('admin.customer_appointment.option'),
                dataType: 'JSON',
                method: 'POST',
                data: {},
                success: function (res) {
                    for (let i = 0; i < $(e).val() - $('#quantity_hide_edit').val(); i++) {
                        var stts = $('#table_quantity_edit tr').length;
                        var tpl = $('#table-quantity-edit-tpl').html();
                        tpl = tpl.replace(/{stt}/g, stts);
                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                        $("#table_quantity_edit > tbody").append(tpl);
                    }
                    $.each(res.optionService, function (index, element) {
                        $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionRoom, function (index, element) {
                        $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionStaff, function (index, element) {
                        $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $('.service_id_edit').select2({
                        placeholder: json['Chọn dịch vụ'],
                    }).on('select2:select', function (event) {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                        $('#room_id_edit_' + id + '').enable(true);
                        $('#staff_id_edit_' + id + '').enable(true);

                    }).on('select2:unselect', function (event) {
                        if ($(this).val() == '') {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                            $('#room_id_edit_' + id + '').enable(false);
                            $('#staff_id_edit_' + id + '').enable(false);
                            $('#room_id_edit_' + id + '').val('').trigger('change');
                            $('#staff_id_edit_' + id + '').val('').trigger('change');
                        }

                    });
                    $('.staff_id_edit').select2({
                        placeholder: json['Chọn nhân viên'],
                    });
                    $('.room_id_edit').select2({
                        placeholder: json['Chọn phòng'],
                    });
                    $('#quantity_hide_edit').val($(e).val());
                }
            });

        } else {
            $('#quantity_hide_edit').val($(e).val());
            $('#table_quantity_edit > tbody').empty();
            $.ajax({
                url: laroute.route('admin.customer_appointment.option'),
                dataType: 'JSON',
                method: 'POST',
                data: {},
                success: function (res) {
                    for (let i = 0; i < $(e).val(); i++) {
                        var stts = $('#table_quantity_edit tr').length;
                        var tpl = $('#table-quantity-edit-tpl').html();
                        tpl = tpl.replace(/{stt}/g, stts);
                        tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                        $("#table_quantity_edit > tbody").append(tpl);
                    }
                    $.each(res.optionService, function (index, element) {
                        $('.service_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionRoom, function (index, element) {
                        $('.room_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $.each(res.optionStaff, function (index, element) {
                        $('.staff_id_edit').append('<option value="' + index + '">' + element + '</option>');
                    });
                    $('.service_id_edit').select2({
                        placeholder: json['Chọn dịch vụ'],
                    }).on('select2:select', function (event) {
                        var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                        $('#room_id_edit_' + id + '').enable(true);
                        $('#staff_id_edit_' + id + '').enable(true);

                    }).on('select2:unselect', function (event) {
                        if ($(this).val() == '') {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order_edit"]').val();
                            $('#room_id_edit_' + id + '').enable(false);
                            $('#staff_id_edit_' + id + '').enable(false);
                            $('#room_id_edit_' + id + '').val('').trigger('change');
                            $('#staff_id_edit_' + id + '').val('').trigger('change');
                        }

                    });
                    $('.staff_id_edit').select2({
                        placeholder: json['Chọn nhân viên'],
                    });
                    $('.room_id_edit').select2({
                        placeholder: json['Chọn phòng'],
                    });
                }
            });
        }


    },
    submit_edit: function () {
        $('#form-edit').validate({
            rules: {
                time_edit: {
                    required: true
                },
                date_edit: {
                    required: true
                },
                quantity_customer_edit: {
                    min: 1,
                    required: true,
                    number: true,
                    max:10
                }
            },
            messages: {
                time_edit: {
                    required: json['Hãy chọn giờ hẹn']
                },
                date_edit: {
                    required: json['Hãy chọn ngày hẹn']
                },
                quantity_customer_edit: {
                    min: json['Số lượng khách hàng tối thiểu 1'],
                    required: json['Hãy nhập số lượng khách hàng'],
                    number: json['Số lượng khách hàng không hợp lệ'],
                    max:json['Số lượng khách hàng tối đa 10']
                }
            },
            submitHandler: function () {
                mApp.block("#m_blockui_2_content", {
                    overlayColor: "#000000",
                    type: "loader",
                    state: "success",
                    message: json["Đang tải..."]
                });
                var time_edit = $('#time_edit').val();
                var date_edit = $('#date_edit').val();
                var customer_appointment_id = $('#customer_appointment_id').val();
                var customer_appointment_type = $('#customer_appointment_type').val();
                var description = $('#description_edit').val();
                var customer_quantity = $('#quantity_customer_edit').val();
                var status = $('.active_edit ').find('input[name="status"]').val();
                var table_quantity = [];
                for (let i = 0; i < customer_quantity; i++) {
                    var stt = i + 1;
                    var input = $('#customer_order_edit_' + stt + '').val();
                    var sv = '';
                    if ($('#service_id_edit_' + stt + '').val() != '') {
                        sv = $('#service_id_edit_' + stt + '').val();
                    }
                    var room = $('#room_id_edit_' + stt + '').val();
                    var staff = $('#staff_id_edit_' + stt + '').val();
                    var arr = {
                        stt: input,
                        sv: sv,
                        staff: staff,
                        room: room
                    };
                    table_quantity.push(arr);
                }

                $.ajax({
                    url: laroute.route('admin.customer_appointment.submitModalEdit'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        date: date_edit,
                        time: time_edit,
                        customer_appointment_id: customer_appointment_id,
                        customer_appointment_type: customer_appointment_type,
                        description: description,
                        customer_quantity: customer_quantity,
                        status: status,
                        table_quantity: table_quantity,
                        time_edit_new: $('#time_edit_new').val(),
                        customer_id: $('#customer_id').val()
                    },
                    success: function (res) {
                        mApp.unblock("#m_blockui_2_content");
                        if (res.time_error == 1) {
                            $('.error_time_edit').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
                        }
                        if (res.status == 1) {
                            swal(json["Cập nhật lịch hẹn thành công"], "", "success");
                            $('#modal-edit').modal('hide');
                            window.location.reload();
                        } else {
                            swal(json["Cập nhật lịch hẹn thất bại"], res.message, "error");
                        }
                    }
                })
            }
        });

    },
    out_modal: function () {
        window.location.reload();
    }
    // remove_tb: function (e) {
    //     $(e).closest('.tr_quantity').remove();
    // }

};
});


function arr_diff(a1, a2) {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}

function onmouseoverAddNew() {
    $('.dropdow-add-new').show();
}

function onmouseoutAddNew() {
    $('.dropdow-add-new').hide();
}

// function onmouseoverAddNew() {
//     $('.dropdow-add-new').show();
// }
function onKeyDownInput(o) {
    $.getJSON(laroute.route('translate'), function (json) {
    if ($(o).val().charAt(0) != "0" && $(o).val().length > 0) {
        $('.error-phone1').text(json["Cập nhật lịch hẹn thất bại"]);
    } else {
        $('.error-phone1').text('');
        $(o).on('keydown', function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110])
            || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true))
            && (!0 === e.ctrlKey || !0 === e.metaKey)
            || 35 <= e.keyCode && 40 >= e.keyCode
            || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode)
            && e.preventDefault()
        });
    }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function addLoad(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                 customer_hidden, description, table_quantity, status) {
    $.getJSON(laroute.route('translate'), function (json) {
    $.ajax({
        url: laroute.route('admin.customer_appointment.submitModalAdd'),
        data: {
            full_name: full_name,
            phone1: phone1,
            customer_appointment_type: type,
            appointment_source_id: appointment_source_id,
            customer_quantity: customer_quantity,
            date: date,
            time: time,
            customer_hidden: customer_hidden,
            description: description,
            table_quantity: table_quantity,
            status: status
        },
        method: 'POST',
        dataType: "JSON",
        success: function (response) {
            if (response.status == 'phone_exist') {
                $('.error-phone1').text(json['Số điện thoại đã tồn tại']);
            }
            if (response.time_error == 1) {
                $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
            }
            if (response.status == 1) {
                window.location.reload();
                swal(json["Thêm lịch hẹn thành công"], "", "success");
            } else {
                swal(json["Thêm lịch hẹn thất bại"], response.message, "error");
            }

        }
    });
    });
}

function updateLoad(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity) {
    $.getJSON(laroute.route('translate'), function (json) {
    $.ajax({
        url: laroute.route('admin.customer_appointment.update-number-appointment'),
        method: 'POST',
        dataType: 'JSON',
        data: {
            customer_id: customer_hidden,
            date: date,
            time: time,
            type: type,
            status: status,
            appointment_source_id: appointment_source_id,
            description: description,
            customer_quantity: customer_quantity,
            table_quantity: table_quantity
        },
        success: function (res) {
            if (res.status == 1) {
                window.location.reload();
                swal(json["Cập nhật lịch hẹn thành công"], "", "success");
            }
        }
    })
});
}

function addReset(full_name, phone1, type, appointment_source_id, customer_quantity, date, time,
                  customer_hidden, description, table_quantity, status) {
    $.getJSON(laroute.route('translate'), function (json) {
    $.ajax({
        url: laroute.route('admin.customer_appointment.submitModalAdd'),
        data: {
            full_name: full_name,
            phone1: phone1,
            customer_appointment_type: type,
            appointment_source_id: appointment_source_id,
            customer_quantity: customer_quantity,
            date: date,
            time: time,
            customer_hidden: customer_hidden,
            description: description,
            table_quantity: table_quantity,
            status: status
        },
        method: 'POST',
        dataType: "JSON",
        success: function (response) {
            if (response.status == 'phone_exist') {
                $('.error-phone1').text(json['Số điện thoại đã tồn tại']);
            }
            if (response.time_error == 1) {
                $('.error_time').text(json['Ngày hẹn, giờ hẹn không hợp lệ']);
            }
            if (response.status == 1) {
                swal(json["Thêm lịch hẹn thành công"], "", "success");
                $('#full_name').val('');
                $('#phone1').val('');
                $('#time').val('').trigger('change');
                $('#quantity_customer').val(1);
                $('#description').val('');
                $("#table_quantity > tbody").empty();
                $.ajax({
                    url: laroute.route('admin.customer_appointment.option'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {},
                    success: function (res) {
                        for (let i = 0; i < $('#quantity_customer').val(); i++) {
                            var stts = $('#table_quantity tr').length;
                            var tpl = $('#table-quantity-tpl').html();
                            tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                            tpl = tpl.replace(/{stt}/g, i + 1);
                            $("#table_quantity > tbody").append(tpl);

                        }
                        $.each(res.optionService, function (index, element) {
                            $('.service_id').append('<option value="' + index + '">' + element + '</option>');

                        });
                        $.each(res.optionRoom, function (index, element) {
                            $('.room_id').append('<option value="' + index + '">' + element + '</option>');

                        });
                        $.each(res.optionStaff, function (index, element) {
                            $('.staff_id').append('<option value="' + index + '">' + element + '</option>');

                        });
                        $('.service_id').select2({
                            placeholder: json['Chọn dịch vụ'],

                        }).on('select2:select', function (event) {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                            $('#room_id_' + id + '').enable(true);
                            $('#staff_id_' + id + '').enable(true);

                        }).on('select2:unselect', function (event) {
                            if ($(this).val() == '') {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                $('#room_id_' + id + '').enable(false);
                                $('#staff_id_' + id + '').enable(false);
                                $('#room_id_' + id + '').val('').trigger('change');
                                $('#staff_id_' + id + '').val('').trigger('change');
                            }

                        });
                        $('.room_id').select2({
                            placeholder: json['Chọn phòng'],
                        });
                        $('.staff_id').select2({
                            placeholder: json['Chọn nhân viên'],
                        });

                    }
                });
            } else {
                swal(json["Thêm lịch hẹn thất bại"], response.message, "error");
            }
        }


    });
});
}

function updateReset(customer_hidden, date, time, type, status, appointment_source_id, description, customer_quantity, table_quantity) {
    $.getJSON(laroute.route('translate'), function (json) {
    $.ajax({
        url: laroute.route('admin.customer_appointment.update-number-appointment'),
        method: 'POST',
        dataType: 'JSON',
        data: {
            customer_id: customer_hidden,
            date: date,
            time: time,
            type: type,
            status: status,
            appointment_source_id: appointment_source_id,
            description: description,
            customer_quantity: customer_quantity,
            table_quantity: table_quantity
        },
        success: function (res) {
            if (res.status == 1) {
                swal(json["Cập nhật lịch hẹn thành công"], "", "success");
                $('#full_name').val('');
                $('#phone1').val('');
                $('#time').val('').trigger('change');
                $('#quantity_customer').val(1);
                $('#description').val('');
                $("#table_quantity > tbody").empty();
                $.ajax({
                    url: laroute.route('admin.customer_appointment.option'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {},
                    success: function (res) {
                        for (let i = 0; i < $('#quantity_customer').val(); i++) {
                            var stts = $('#table_quantity tr').length;
                            var tpl = $('#table-quantity-tpl').html();
                            tpl = tpl.replace(/{name}/g, json['Khách '] + stts);
                            tpl = tpl.replace(/{stt}/g, i + 1);
                            $("#table_quantity > tbody").append(tpl);

                        }
                        $.each(res.optionService, function (index, element) {
                            $('.service_id').append('<option value="' + index + '">' + element + '</option>');

                        });
                        $.each(res.optionRoom, function (index, element) {
                            $('.room_id').append('<option value="' + index + '">' + element + '</option>');

                        });
                        $.each(res.optionStaff, function (index, element) {
                            $('.staff_id').append('<option value="' + index + '">' + element + '</option>');

                        });
                        $('.service_id').select2({
                            placeholder: json['Chọn dịch vụ'],

                        }).on('select2:select', function (event) {
                            var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                            $('#room_id_' + id + '').enable(true);
                            $('#staff_id_' + id + '').enable(true);

                        }).on('select2:unselect', function (event) {
                            if ($(this).val() == '') {
                                var id = $(this).closest('.tr_quantity').find('input[name="customer_order"]').val();
                                $('#room_id_' + id + '').enable(false);
                                $('#staff_id_' + id + '').enable(false);
                                $('#room_id_' + id + '').val('').trigger('change');
                                $('#staff_id_' + id + '').val('').trigger('change');
                            }

                        });
                        $('.room_id').select2({
                            placeholder: json['Chọn phòng'],
                        });
                        $('.staff_id').select2({
                            placeholder: json['Chọn nhân viên'],
                        });

                    }
                });
            }
        }
    })
});
}



