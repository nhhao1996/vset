var customerSource = {

    remove: function (obj, id) {

        $(obj).closest('tr').addClass('m-table__row--danger');
        $.getJSON(laroute.route('translate'), function (json) {
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.customer-source.remove', {id: id}), function () {
                    swal(
                        'Xóa thành công.',
                        '',
                        'success'
                    );
                    $('#autotable').PioTable('refresh');
                });
            }
        });
    });
    },
    changeStatus: function (obj, id, action) {
        $.post(laroute.route('admin.customer-source.change-status'), {id: id, action: action}, function (data) {
            $('#autotable').PioTable('refresh');
        }, 'JSON');
    },
    addClose: function () {
        let customer_source_name = $('input[name="customer_source_name"]');
        let customer_source_description = $('textarea[name="customer_source_description"]');
        let is_inactive = $('#is_actived_add');
        let isActive = 0;
        if (is_inactive.is(':checked')) {
            isActive = 1;
        }
        let type = "in";
        if ($('#type-out').is(":checked")) {
            type = "out";
        }
        $(".error-customer-source-name").css("color", "red");
        if (customer_source_name.val() == "") {
            $('.error-customer-source-name').text('Vui lòng nhập tên nguồn khách hàng');
        }
        if (customer_source_name.val() != "") {
            $.ajax({
                url: laroute.route('admin.customer-source.add'),
                data: {
                    customer_source_name: customer_source_name.val(),
                    customer_source_description: customer_source_description.val(),
                    customer_source_type: type,
                    is_inactive: isActive
                },
                method: "POST",
                dataType: "JSON",
                success: function (data) {
                    $.getJSON(laroute.route('translate'), function (json) {
                        $('.error-customer-source-name').text('');
                        if (data.success == 1) {
                            swal(
                                'Thêm nguồn khách hàng thành công',
                                '',
                                'success'
                            );
                            $('#modalAdd').modal('hide');
                            is_inactive.val('1');
                            $('#autotable').PioTable('refresh');
                        }

                        if (data.success == 0) {
                            if (data.message_name != ''){
                                $('.error-customer-source-name').text(data.message_name);
                            }
                        }
                    });
                }
            });

        }
    },
    edit: function (id) {
        $('.error-group-name').text('');
        $.ajax({
            url: laroute.route('admin.customer-source.edit'),
            data: {
                customerSourceId: id,
            },
            method: "GET",
            dataType: 'JSON',
            success: function (data) {
                $('#modalEdit').modal('show');
                $('input[name="customer_source_id_edit"]').val(data['customer_source_id']);
                $('input[name="customer_source_name_edit"]').val(data['customer_source_name']);
                $('textarea[name="customer_source_description_edit"]').val(data['customer_source_description']);

                // if (data['customer_source_type'] == "in") {
                //     $('.type-in').prop('checked', true);
                // } else {
                //     $('.type-out').prop('checked', true);
                // }
                if (data['is_actived'] == 1) {
                    $('#is_actived_edit').prop('checked', true);
                } else {
                    $('#is_actived_edit').prop('checked', false);
                }

            }
        })
    },
    submitEdit: function () {
        let id = $('input[name="customer_source_id_edit"]').val();
        let customer_source_name = $('input[name="customer_source_name_edit"]').val();
        let customer_source_description = $('textarea[name="customer_source_description_edit"]').val();
        let isActive = 0;
        if ($('#is_actived_edit').is(':checked')) {
            isActive = 1;
        }
        $(".error-group-name").css("color", "red");
        if (customer_source_name != "") {
            $.ajax({
                    url: laroute.route('admin.customer-source.edit-submit'),
                    data: {
                        id: id,
                        customer_source_name: customer_source_name,
                        customer_source_description: customer_source_description,
                        is_actived: isActive,
                        parameter: 0
                    },
                    method: "POST",
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.success == 0) {
                            if (data.message_name != ''){
                                $('.error-group-name').text(data.message_name);
                            }

                        }
                        if (data.success == 1) {
                            swal(
                                'Cập nhật nguồn khách hàng thành công',
                                '',
                                'success'
                            );
                            $('#modalEdit').modal('hide');
                            $('#autotable').PioTable('refresh');
                        } else if (data.success == 2) {
                            swal({
                                title: 'Nguồn khách hàng đã tồn tại',
                                text: "Bạn có muốn kích hoạt lại không?",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Có',
                                cancelButtonText: 'Không'
                            }).then(function (willDelete) {
                                if (willDelete.value == true) {
                                    $.ajax({
                                        url: laroute.route('admin.customer-source.edit-submit'),
                                        data: {
                                            id: id,
                                            customer_source_name: customer_source_name,
                                            customer_source_description: customer_source_description,
                                            is_actived: isActive,
                                            parameter: 1
                                        },
                                        method: "POST",
                                        dataType: 'JSON',
                                        success: function (data) {
                                            if (data.success = 3) {
                                                swal(
                                                    'Kích hoạt nguồn khách hàng thành công',
                                                    '',
                                                    'success'
                                                );
                                                $('#autotable').PioTable('refresh');
                                                $('#modalEdit').modal('hide');
                                            }
                                        }
                                    });
                                }
                            });
                        }

                    }
                }
            );
        } else {
            $.getJSON(laroute.route('translate'), function (json) {
            $('.error-group-name').text('Vui lòng nhập tên nguồn khách hàng');
            });
        }
    },
    clearAdd: function () {
        $('.error-customer-source-name').text('');
        // $('#modalAdd #customer_source_type').val('in');
        $('#modalAdd #is_actived').val('1');
        $('#modalAdd #customer_source_name').val('');
        $('#modalAdd #customer_source_description').val('');

    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('select[name="is_actived"]').val('').trigger('change');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    }
};

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.customer-source.list')
});
$('select[name="is_actived"]').select2();