var interest = {
    init: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var arrRange = {};
            arrRange[json["Hôm nay"]] = [moment(), moment()];
            arrRange[json["Hôm qua"]] = [moment().subtract(1, "days"), moment().subtract(1, "days")];
            arrRange[json["7 ngày trước"]] = [moment().subtract(6, "days"), moment()];
            arrRange[json["30 ngày trước"]] = [moment().subtract(29, "days"), moment()];
            arrRange[json["Trong tháng"]] = [moment().startOf("month"), moment().endOf("month")];
            arrRange[json["Tháng trước"]] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")];
            $("#time").daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                buttonClasses: "m-btn btn",
                applyClass: "btn-primary",
                cancelClass: "btn-danger",

                maxDate: moment().endOf("day"),
                // startDate: moment().startOf("day"),
                // endDate: moment().add(1, 'days'),
                startDate: moment().subtract(6, "days"),
                endDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY',
                    "applyLabel": json["Đồng ý"],
                    "cancelLabel": json["Thoát"],
                    "customRangeLabel": json["Tùy chọn ngày"],
                    daysOfWeek: [
                        json["CN"],
                        json["T2"],
                        json["T3"],
                        json["T4"],
                        json["T5"],
                        json["T6"],
                        json["T7"]
                    ],
                    "monthNames": [
                        json["Tháng 1 năm"],
                        json["Tháng 2 năm"],
                        json["Tháng 3 năm"],
                        json["Tháng 4 năm"],
                        json["Tháng 5 năm"],
                        json["Tháng 6 năm"],
                        json["Tháng 7 năm"],
                        json["Tháng 8 năm"],
                        json["Tháng 9 năm"],
                        json["Tháng 10 năm"],
                        json["Tháng 11 năm"],
                        json["Tháng 12 năm"]
                    ],
                    "firstDay": 1
                },
                ranges: arrRange
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format("DD/MM/YYYY");
                var end = picker.endDate.format("DD/MM/YYYY");
                $(this).val(start + " - " + end);
                interest.onchangeTime();
            });
        });
        $("#time-hidden").daterangepicker({
            startDate: moment().subtract(6, "days"),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('#time').val($("#time-hidden").val());
        interest.filter();
    },
    onchangeTime: function () {
        var time = $('#time').val();
        if (time != '') {
            $('#year').val('');
        }
        interest.filter();
    },
    onchangeYear: function (o) {
        var year = $(o).val();
        if (year == '') {
            $('#time').val($("#time-hidden").val());
        } else {
            $('#time').val('');
        }
        interest.filter();
    },
    filter: function () {
        var time = $('#time').val();
        var year = $('#year').val();
        var data = {
            time: time,
            year: year,
        };
        $.ajax({
            url: laroute.route('admin.report.filterInterest'),
            method: "POST",
            dataType: 'json',
            data: data,
            sync: false,
            success: function (res) {
                interest.chartLine(res.chart_line);
                interest.chartQuantity(res.chart_quantity);
                $('#total_money').empty();
                $('#total_money').html(res.totalInterest + ' VNĐ');
            }
        });

    },
    //Hàm định dạng số.
    formatNumber: function (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    },
    chartLine: function (dataValue) {
        google.charts.load('current', {'packages':['line']});
        google.charts.setOnLoadCallback(drawChart);
        // var dataValue = [
        //     ["day", "TRÁI PHIẾU", "TIẾT KIỆM"],
        //     ["22/12", 0, 0],
        //     ["23/12", 0, 0],
        //     ["24/12", 0, 0],
        // ];
        var color = ['#4fc4cb', '#F3D85E', '#4EF1FF', '#01DF01', '#A901DB', '#FB5454', '#DF7401'];

        function drawChart() {
            var data = google.visualization.arrayToDataTable(dataValue);
            var options = {
                title: 'Số tiền',
                titleTextStyle: {
                    color: "#454545",
                    fontSize: 13,
                    bold: true
                },
                legend: {
                    position: 'right',
                    textStyle: {
                        color: '#454545',
                        fontSize: 13,
                        bold: true
                    }
                },
                titlePosition: 'out',
                hAxis: {
                    title: '',
                    titleTextStyle:
                        {
                            color: '#333',
                            fontSize: 12
                        },
                    // viewWindow: {
                    //     max:31,
                    //     min:1
                    // }
                },
                vAxis: {
                    viewWindowMode: "explicit",
                    viewWindow:{ min: 0 },
                    format:'###,### VNĐ',
                },
                colors: color,
                chartArea: {
                    height: "700px",
                    width: "80%",
                    left: 30
                }
            };
            var formatter1 = new google.visualization.NumberFormat({pattern:'###,### VNĐ' });
            formatter1.format(data, 1);

            var chart = new google.charts.Line(document.getElementById('interest'));
            chart.draw(data, google.charts.Line.convertOptions(options));
        }
    },
    chartQuantity: function (dataValue) {
        /**
         * Data mẫu
         * [
         *      {month: '01', order: 10},
         *      {month: '02', order: 12},
         * ]
         */
        AmCharts.makeChart("quantity", {
            "type": "serial",
            "addClassNames": true,
            "theme": "light",
            "autoMargins": false,
            "marginLeft": 100,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "balloon": {
                "adjustBorderColor": false,
                "horizontalPadding": 10,
                "verticalPadding": 8,
                "color": "#ffffff"
            },
            "dataProvider": dataValue,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:12px;'>[[category]]<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                "fillAlphas": 1,
                "title": "",
                "type": "column",
                "valueField": "quantity",
                "dashLengthField": "dashLengthColumn",
                "bullet": "bubble",
            }],
            "categoryField": "product_category",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });
        $('.amcharts-chart-div').find('a').remove();
    }
};
interest.init();
expectedInterestPayment.init();