$(document).ready(function (){
    $('#select_type').change(function (){
        if ($('#select_type').val() != 'week'){
            $('#week').val('week_now').change();
        }
        $('.block_hide').hide();
        $('.select_week').hide();
        var block = $('#select_type').val();
        $('.'+block+'_block').show();
    });

    $('#week').change(function (){
       if($('#week').val() == 'select_week') {
           $('.select_week').show();
       } else {
           $('.select_week').hide();
       }
    });
});

var expectedInterestPayment = {
    // init: function () {
    //     expectedInterestPayment.filter();
    // },
    init: function () {
        // $.getJSON(laroute.route('translate'), function (json) {
            var arrRange = {};
            arrRange["Hôm nay"] = [moment(), moment()];
            arrRange["Ngày mai"] = [moment().add(1, "days"), moment().add(1, "days")];
            arrRange["7 ngày sau"] = [moment(), moment().add(6, "days")];
            arrRange["30 ngày sau"] = [moment(), moment().add(29, "days")];
            arrRange["Trong tháng"] = [moment().startOf("months"), moment().endOf("months")];
            arrRange["Tháng sau"] = [moment().add(1, "months").startOf("months"), moment().add(1, "months").endOf("months")];

            $("#time-expected").daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                buttonClasses: "m-btn btn",
                applyClass: "btn-primary",
                cancelClass: "btn-danger",
                showWeekNumbers:true,
                minDate: moment(),
                maxDate: moment().add(2, "year"),
                locale: {
                    format: 'MM/YYYY',
                    "applyLabel": "Đồng ý",
                    "cancelLabel": "Thoát",
                    "customRangeLabel": "Tùy chọn ngày",
                    daysOfWeek: [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Tháng 1 năm",
                        "Tháng 2 năm",
                        "Tháng 3 năm",
                        "Tháng 4 năm",
                        "Tháng 5 năm",
                        "Tháng 6 năm",
                        "Tháng 7 năm",
                        "Tháng 8 năm",
                        "Tháng 9 năm",
                        "Tháng 10 năm",
                        "Tháng 11 năm",
                        "Tháng 12 năm"
                    ],
                    "firstDay": 1
                },
                ranges: arrRange
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format("DD/MM/YYYY");
                var end = picker.endDate.format("DD/MM/YYYY");
                $(this).val(start + " - " + end);
                // expectedInterestPayment.onchangeTime();
                expectedInterestPayment.filter();
            });
        // });
        // $("#time-expected-hidden").daterangepicker({
        //     startDate: moment(),
        //     endDate: moment().add(6, "days"),
        //     locale: {
        //         format: 'DD/MM/YYYY'
        //     }
        // });
        $("#time-hidden").daterangepicker({
            startDate: moment(),
            endDate: moment().add(6, "days"),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $("#month_start").datepicker( {
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            startDate: "0m",
            endDate: "+2y"
        });
        $("#month_end").datepicker( {
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            startDate: "0m",
            endDate: "+2y"
        });
        // $('#time-expected').val($("#time-expected-hidden").val());
        $('#time-expected').val($("#time-hidden").val());

        expectedInterestPayment.filter();
    },
    filter: function () {
        var year = $('#year').val();
        var data = {
            year: year,
        };
        $.getJSON(laroute.route('translate'), function (json) {
            $.ajax({
                url: laroute.route('admin.report.filterExpectedInterestPayment'),
                method: "POST",
                dataType: 'json',
                data: $('#form-chart').serialize(),
                sync: false,
                success: function (res) {
                    /**
                     * Data mẫu
                     * [
                     *      {month: '01', order: 10},
                     *      {month: '02', order: 12},
                     * ]
                     */
                    AmCharts.makeChart("chart", {
                        "type": "serial",
                        "addClassNames": true,
                        "theme": "light",
                        "autoMargins": false,
                        "marginLeft": 150,
                        "marginRight": 30,
                        "marginTop": 20,
                        "marginBottom": 100,
                        "balloon": {
                            "adjustBorderColor": false,
                            "horizontalPadding": 10,
                            "verticalPadding": 8,
                            "color": "#ffffff"
                        },
                        "dataProvider": res.value.chart,
                        "valueAxes": [{
                            "axisAlpha": 0,
                            "position": "left",
                            "unit" : " VNĐ",
                            // "autoGridCount" : false,
                            // "gridCount" : 15,

                        }],
                        "startDuration": 1,
                        "graphs": [{
                            "alphaField": "alpha",
                            "balloonText": "<span style='font-size:12px;'>"
                            +" [[category]]<br><span style='font-size:20px;'>[[value]] VNĐ</span> [[additional]]</span>",
                            "fillAlphas": 1,
                            "title": "",
                            "type": "column",
                            "valueField": "value",
                            "dashLengthField": "dashLengthColumn",
                            "bullet": "bubble",
                        }],
                        "categoryField": res.type,
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "tickLength": 0,
                            "labelRotation": -55,
                        },

                    });
                    $('.amcharts-chart-div').find('a').remove();
                }
            });
        });
    },
};
expectedInterestPayment.init();

$(document).ready(function (){
    $(".example").daterangepicker({
        single: true,
        standalone: true
    });
})