var sales = {
    init: function () {
        $("#month_start").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            startDate: "-2y",
            endDate: new Date(),
        });

        $("#month_end").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            startDate: "-2y",
            endDate: new Date(),
        });

        $("#month_start_sale").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            // startDate: "-2y",
            endDate: new Date(),
        });

        $("#month_end_sale").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            // startDate: "-2y",
            endDate: new Date(),
        });

        $("#month_start_group").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            // startDate: "-2y",
            endDate: new Date(),
        });

        $("#month_end_group").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            // startDate: "-2y",
            endDate: new Date(),
        });
    },

    filter: function () {
        $.ajax({
            url: laroute.route('admin.report.salesFilter'),
            method: "POST",
            dataType: 'json',
            data: $('#sale-form').serialize(),
            sync: false,
            success: function (res) {
                if (res.error == false) {
                    AmCharts.makeChart("sales", {
                        "type": "serial",
                        "addClassNames": true,
                        "theme": "light",
                        "autoMargins": false,
                        "marginLeft": 150,
                        "marginRight": 30,
                        "marginTop": 20,
                        "marginBottom": 100,
                        "balloon": {
                            "adjustBorderColor": false,
                            "horizontalPadding": 10,
                            "verticalPadding": 8,
                            "color": "#ffffff"
                        },
                        "dataProvider": res.amcharts,
                        "valueAxes": [{
                            "axisAlpha": 0,
                            "position": "left",
                            "unit" : " VNĐ"
                        }],
                        "startDuration": 1,
                        "graphs": [{
                            "alphaField": "alpha",
                            "balloonText": "<span style='font-size:12px;'>"
                                // + title
                                // + "[[category]]"
                                // + titleYear
                                + "<br><span style='font-size:20px;'>[[value]] VNĐ</span> [[additional]]</span>",
                            "fillAlphas": 1,
                            "title": "Tháng",
                            "type": "column",
                            "valueField": "money",
                            "dashLengthField": "dashLengthColumn",
                            "bullet": "bubble",
                        }],
                        "categoryField": "month",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "tickLength": 0,
                            "labelRotation": 0,
                        }
                    });
                    $('.amcharts-chart-div').find('a').remove();
                } else {
                    $('#sales').empty();
                }
            }
        });
    },

    filterSale:function (pageClick = null) {
        if (pageClick == null) {
            var page = 1;
        } else {
            var page = pageClick;
        }

        $.ajax({
            url: laroute.route('admin.report.sumSalesFilter'),
            method: "POST",
            dataType: 'json',
            data: $('#filter-sale').serialize()+'&page='+page,
            sync: false,
            success: function (res) {
                if (res.error == false) {
                    if (pageClick == null){
                        am4core.useTheme(am4themes_animated);
                        var chart = am4core.create("chartSale", am4charts.PieChart);
                        chart.radius = am4core.percent(50);

                        chart.data = res.chart;
// Add and configure Series
                        var pieSeries = chart.series.push(new am4charts.PieSeries());
                        pieSeries.dataFields.value = "money";
                        pieSeries.dataFields.category = "name";
                        pieSeries.slices.template.stroke = am4core.color("#fff");
                        pieSeries.slices.template.strokeWidth = 1;
                        pieSeries.slices.template.strokeOpacity = 1;
                        pieSeries.labels.template.text = "{category}\n{value.percent.formatNumber('#.###')} %";
                        pieSeries.slices.template.tooltipText = "{category}: {value.percent.formatNumber('###.000')}% ({value.value} VND)";
// This creates initial animation
                        pieSeries.hiddenState.properties.opacity = 1;
                        pieSeries.hiddenState.properties.endAngle = -90;
                        pieSeries.hiddenState.properties.startAngle = -90;
                        $('.money-sale').text(res.total+' VND');
                    }
                    $('.table-sale').empty();
                    $('.table-sale').append(res.view);
                } else {
                    $('.table-sale').empty();
                    $('#chartSale').empty();
                }
            }
        });
    },
    filterGroup:function (pageClick = null) {
        if (pageClick == null) {
            var page = 1;
        } else {
            var page = pageClick;
        }
        $.ajax({
            url: laroute.route('admin.report.sumGroupFilter'),
            method: "POST",
            dataType: 'json',
            data: $('#filter-group').serialize()+'&page='+page,
            sync: false,
            success: function (res) {
                if (res.error == false) {
                    if (pageClick == null){
                        am4core.useTheme(am4themes_animated);
                        var chart = am4core.create("chartGroup", am4charts.PieChart);
                        chart.radius = am4core.percent(50);

                        chart.data = res.chart;

// Add and configure Series
                        var pieSeries = chart.series.push(new am4charts.PieSeries());
                        pieSeries.dataFields.value = "money";
                        pieSeries.dataFields.category = "name";
                        pieSeries.slices.template.stroke = am4core.color("#fff");
                        pieSeries.slices.template.strokeWidth = 1;
                        pieSeries.slices.template.strokeOpacity = 1;
                        pieSeries.labels.template.text = "{category}\n{value.percent.formatNumber('#.###')} %";
                        pieSeries.slices.template.tooltipText = "{category}: {value.percent.formatNumber('###.000')}% ({value.value} VND)";
// This creates initial animation
                        pieSeries.hiddenState.properties.opacity = 1;
                        pieSeries.hiddenState.properties.endAngle = -90;
                        pieSeries.hiddenState.properties.startAngle = -90;
                        $('.money-sale-group').text(res.total+' VND');
                    }

                    $('.table-group').empty();
                    $('.table-group').append(res.view);
                } else {
                    $('.table-group').empty();
                    $('#chartGroup').empty();
                }
            }
        });
    }
}

sales.init();
sales.filter();
sales.filterSale();
sales.filterGroup();