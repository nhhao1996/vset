var shareLink = {
    init: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var arrRange = {};
            arrRange[json["Hôm nay"]] = [moment(), moment()];
            arrRange[json["Hôm qua"]] = [moment().subtract(1, "days"), moment().subtract(1, "days")];
            arrRange[json["7 ngày trước"]] = [moment().subtract(6, "days"), moment()];
            arrRange[json["30 ngày trước"]] = [moment().subtract(29, "days"), moment()];
            arrRange[json["Trong tháng"]] = [moment().startOf("month"), moment().endOf("month")];
            arrRange[json["Tháng trước"]] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")];
            $("#time").daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                buttonClasses: "m-btn btn",
                applyClass: "btn-primary",
                cancelClass: "btn-danger",

                maxDate: moment().endOf("day"),
                // startDate: moment().startOf("day"),
                // endDate: moment().add(1, 'days'),
                startDate: moment().subtract(6, "days"),
                endDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY',
                    "applyLabel": json["Đồng ý"],
                    "cancelLabel": json["Thoát"],
                    "customRangeLabel": json["Tùy chọn ngày"],
                    daysOfWeek: [
                        json["CN"],
                        json["T2"],
                        json["T3"],
                        json["T4"],
                        json["T5"],
                        json["T6"],
                        json["T7"]
                    ],
                    "monthNames": [
                        json["Tháng 1 năm"],
                        json["Tháng 2 năm"],
                        json["Tháng 3 năm"],
                        json["Tháng 4 năm"],
                        json["Tháng 5 năm"],
                        json["Tháng 6 năm"],
                        json["Tháng 7 năm"],
                        json["Tháng 8 năm"],
                        json["Tháng 9 năm"],
                        json["Tháng 10 năm"],
                        json["Tháng 11 năm"],
                        json["Tháng 12 năm"]
                    ],
                    "firstDay": 1
                },
                ranges: arrRange
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format("DD/MM/YYYY");
                var end = picker.endDate.format("DD/MM/YYYY");
                $(this).val(start + " - " + end);
                shareLink.filter();
            });
        });
        $("#time-hidden").daterangepicker({
            startDate: moment().subtract(6, "days"),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('#time').val($("#time-hidden").val());
        shareLink.filter();
    },

    filter: function () {
        $.ajax({
            url: laroute.route('admin.report.shareLinkFilter'),
            method: "POST",
            dataType: 'json',
            data: {
                time : $('#time').val(),
                source : $('#source').val()
            },
            sync: false,
            success: function (res) {
                if (res.error == false) {
                    // var data = google.visualization.arrayToDataTable([
                    //     ['day', 'Tất cả', 'Khác','Facebook','Zalo','Twitter','Wechat'],
                    //     ['1/2/2021',  10,6,5,4,3,2],
                    //     ['2/2/2021',  15,6,5,4,3,2],
                    //     ['3/2/2021',  20,6,5,4,3,2],
                    //     ['4/2/2021',  5,6,5,4,3,2]
                    // ]);
                    var data = google.visualization.arrayToDataTable(res.chart);
                    var width = 0;
                    if (res.chart.length >= 10) {
                        width = 2000;
                    } else if(res.chart.length >= 15) {
                        width = 3000;
                    } else if (res.chart.length >= 20) {
                        width = 4000;
                    }
                    var totalRow = 0;
                    if (res.totalValue <= 4) {
                        totalRow = 4;
                    } else {
                        totalRow = res.totalValue;
                    }
                    var options = {
                        title: '',
                        height : 600,
                        curveType: 'function',
                        vAxis: {
                            // viewWindowMode: "explicit",
                            viewWindow:{ min: 0, max: totalRow },
                            minValue:0,
                            gridlines: {
                                count: totalRow,
                            }
                        },
                        viewWindowMode: "explicit",
                        format:'###,### Nhà đầu tư',
                        legend: { position: 'right' },
                    };

                    if (width != 0) {
                        options.width = width;
                    }

                    var chart = new google.visualization.ColumnChart(document.getElementById('share-link'));

                    chart.draw(data, options);
                }else {
                    $('#share-link').empty();
                }
            }
        });
    }
}

// google.charts.load('43', {'packages':['corechart']});
google.charts.load('current', {'packages':['corechart', 'bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    shareLink.filter();
}


shareLink.init();