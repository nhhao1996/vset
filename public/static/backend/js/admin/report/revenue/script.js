var revenue = {
    init: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var arrRange = {};
            arrRange[json["Hôm nay"]] = [moment(), moment()];
            arrRange[json["Hôm qua"]] = [moment().subtract(1, "days"), moment().subtract(1, "days")];
            arrRange[json["7 ngày trước"]] = [moment().subtract(6, "days"), moment()];
            arrRange[json["30 ngày trước"]] = [moment().subtract(29, "days"), moment()];
            arrRange[json["Trong tháng"]] = [moment().startOf("month"), moment().endOf("month")];
            arrRange[json["Tháng trước"]] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")];
            $("#time").daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                buttonClasses: "m-btn btn",
                applyClass: "btn-primary",
                cancelClass: "btn-danger",

                maxDate: moment().endOf("day"),
                // startDate: moment().startOf("day"),
                // endDate: moment().add(1, 'days'),
                startDate: moment().subtract(6, "days"),
                endDate: moment(),
                locale: {
                    format: 'DD/MM/YYYY',
                    "applyLabel": json["Đồng ý"],
                    "cancelLabel": json["Thoát"],
                    "customRangeLabel": json["Tùy chọn ngày"],
                    daysOfWeek: [
                        json["CN"],
                        json["T2"],
                        json["T3"],
                        json["T4"],
                        json["T5"],
                        json["T6"],
                        json["T7"]
                    ],
                    "monthNames": [
                        json["Tháng 1 năm"],
                        json["Tháng 2 năm"],
                        json["Tháng 3 năm"],
                        json["Tháng 4 năm"],
                        json["Tháng 5 năm"],
                        json["Tháng 6 năm"],
                        json["Tháng 7 năm"],
                        json["Tháng 8 năm"],
                        json["Tháng 9 năm"],
                        json["Tháng 10 năm"],
                        json["Tháng 11 năm"],
                        json["Tháng 12 năm"]
                    ],
                    "firstDay": 1
                },
                ranges: arrRange
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format("DD/MM/YYYY");
                var end = picker.endDate.format("DD/MM/YYYY");
                $(this).val(start + " - " + end);
                revenue.onchangeTime();
            });
        });
        $("#time-hidden").daterangepicker({
            startDate: moment().subtract(6, "days"),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('#time').val($("#time-hidden").val());
        revenue.filter();
    },
    onchangeTime: function () {
        var time = $('#time').val();
        if (time != '') {
            $('#year').val('');
        }
        revenue.filter();
    },
    onchangeYear: function (o) {
        var year = $(o).val();
        if (year == '') {
            $('#time').val($("#time-hidden").val());
        } else {
            $('#time').val('');
        }
        revenue.filter();
    },
    filter: function () {
        var time = $('#time').val();
        var year = $('#year').val();
        var type = $('#type').val();
        var data = {
            time: time,
            year: year,
            type: type,
        };
        $.getJSON(laroute.route('translate'), function (json) {
            var title = '';
            var titleYear = '';
            if (year != '') {
                title = json['Tháng'] + ' ';
                titleYear = '/' + year;
            }
            $.ajax({
                url: laroute.route('admin.report.filterRevenue'),
                method: "POST",
                dataType: 'json',
                data: data,
                sync: false,
                success: function (res) {
                    /**
                     * Data mẫu
                     * [
                     *      {month: '01', order: 10},
                     *      {month: '02', order: 12},
                     * ]
                     */
                    AmCharts.makeChart("revenue", {
                        "type": "serial",
                        "addClassNames": true,
                        "theme": "light",
                        "autoMargins": false,
                        "marginLeft": 150,
                        "marginRight": 30,
                        "marginTop": 20,
                        "marginBottom": 100,
                        "balloon": {
                            "adjustBorderColor": false,
                            "horizontalPadding": 10,
                            "verticalPadding": 8,
                            "color": "#ffffff"
                        },
                        "dataProvider": res.chart,
                        "valueAxes": [{
                            "axisAlpha": 0,
                            "position": "left",
                            "unit" : " VNĐ"
                        }],
                        "startDuration": 1,
                        "graphs": [{
                            "alphaField": "alpha",
                            "balloonText": "<span style='font-size:12px;'>"
                            // + title
                            + "[[category]]"
                            // + titleYear
                            + "<br><span style='font-size:20px;'>[[value]] VNĐ</span> [[additional]]</span>",
                            "fillAlphas": 1,
                            "title": "Tháng",
                            "type": "column",
                            "valueField": "order",
                            "dashLengthField": "dashLengthColumn",
                            "bullet": "bubble",
                        }],
                        "categoryField": "month",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "tickLength": 0,
                            "labelRotation": -55,
                        }
                    });
                    $('.amcharts-chart-div').find('a').remove();
                    var total = revenue.formatNumber(res.total);
                    var text = total + ' VNĐ';
                    $('#total_money').text(text);
                    var dataPie = [];
                    $.each(res.payment_method, function (key, value) {
                        if (key != 0) {
                            // let temp = [value[0], value[1]];
                            let temp = {};
                            temp['name'] = value[0];
                            temp['value'] = value[1];
                            dataPie.push(temp);
                        }

                    });
                    revenue.paymentMethod(dataPie);
                }
            });
        });
    },
    //Hàm định dạng số.
    formatNumber: function (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    },
    // Biểu đồ theo phương thức thanh toán
    paymentMethod: function (dataValue) {
        // google.charts.load("current", {packages: ["corechart"]});
        // google.charts.setOnLoadCallback(drawChart);
        // function drawChart() {
        //     var data = google.visualization.arrayToDataTable(dataValue);
        //
        //     var options = {
        //         title: '',
        //         pieHole: 0.4,
        //         legend: {position: 'bottom'},
        //         colors: ['#3468be', '#f06eaa', '#e3d500','#0abb87'],
        //         chartArea: {left: 0, top: 7, right: 0, width: '50%', height: '75%'},
        //         enableInteractivity: false,
        //     };
        //
        //     var chart = new google.visualization.PieChart(
        //         document.getElementById('pie_chart_payment_method'));
        //     chart.draw(data, options);
        // }

        am4core.useTheme(am4themes_animated);
        var chart = am4core.create("pie_chart_payment_method", am4charts.PieChart);
        chart.radius = am4core.percent(50);

// Add data
//                     chart.data = [
//                         {
//                             "name": "Lithuania",
//                             "money": 10
//                         },
//                         {
//                             "name": "Czechia",
//                             "money": 10
//                         },
//                     ];

        chart.data = dataValue;

// Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "name";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 1;
        pieSeries.slices.template.strokeOpacity = 1;
        pieSeries.labels.template.text = "{category}\n{value.percent.formatNumber('#.###')}%";
        pieSeries.slices.template.tooltipText = "{category}: {value.percent.formatNumber('###.000')}% ({value.value} VND)";

// This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;
    }
};
revenue.init();