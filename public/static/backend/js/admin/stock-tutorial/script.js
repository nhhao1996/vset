var stockTutorial = {
    update : function () {
        $.ajax({
            url: laroute.route('admin.stock-tutorial.update'),
            data: $('#stock-tutorial').serialize(),
            method: 'POST',
            dataType: "JSON",
            success: function (res) {
                if(res.error == false){
                    swal(res.message,'','success').then(function () {
                        window.location.href = laroute.route('admin.stock-tutorial');
                    });
                } else {
                    swal(res.message,'','error');
                }
            }
        });
    },
}

$(document).ready(function () {
    uploadImgCk = function (file,lang) {
        let out = new FormData();
        out.append('file', file, file.name);

        $.ajax({
            method: 'POST',
            url: laroute.route('admin.stock-tutorial.upload'),
            contentType: false,
            cache: false,
            processData: false,
            data: out,
            success: function (img) {
                if ((file.type).indexOf('image') != -1){
                    $("#stock_tutorial_content").summernote('insertImage', img['file']);
                } else {
                    $("#stock_tutorial_content").summernote('pasteHTML', '<a href="'+img['file']+'">'+file.name+'</a>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus + " " + errorThrown);
            }
        });
    };
})