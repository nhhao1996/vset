$('#autotable').PioTable({
    baseUrl: laroute.route('admin.customer-contract.list-interest-approved-change'),
});
// $('document').ready(function () {
//     $('.checkAll').click(function () {
//
//     });
// })

function check() {
    if ($('.checkAll input').is(':checked')) {
        $('.checkItem input').prop('checked',true);
    } else {
        $('.checkItem input').prop('checked',false);
    }
}

var customerContract = {
    approved: function () {
        var listInterest = [];
        $.each($('table').find(".checkItem"), function () {
            if ($(this).find($('input')).is(':checked')) {
                listInterest.push($(this).find($('input')).attr('data-id'));
            }


        });

        if (listInterest.length == 0) {
            swal(
                'Chưa chọn lãi để xác nhận',
                '',
                'error'
            )
        } else {
            $.ajax({
                url: laroute.route('admin.customer-contract.confirm-contract-interest'),
                method: 'POST',
                data: {
                    listInterest: listInterest,
                },
                success: function (response) {
                    if (response.error == 1) {
                        swal(
                            response.message,
                            '',
                            'error'
                        )
                    } else {
                        swal(
                            response.message,
                            '',
                            'success'
                        ).then(function () {
                            location.reload();
                        });

                    }

                }
            });
        }
    },
}

$(document).ready(function () {
    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        // maxDate: moment().endOf("day"),
        // startDate: moment().startOf("day"),
        // endDate: moment().add(1, 'days'),
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });
})