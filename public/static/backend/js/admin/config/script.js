var config = {
    addKey: function () {
        $('.list-keyhot').append(function () {
            return '<div id="key'+sum+'">' +
                        ' <input type="text" name="key['+sum+']" class="form-control mb-2 w-50 d-inline" placeholder="Nhập từ khóa">'+
                        '<button type="button" style="margin-left: 5px !important;" onclick="config.removeKey('+sum+')' +
                '"\n' +
                        ' class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"\n' +
                        '  title="Xóa"><i class="la la-trash"></i></button>\n' +
                    ' </div>'
        });
        sum++;
    },

    removeKey: function ($id) {
        $('#key'+$id).remove();
    },
    
    updateKey : function () {
        $.ajax({
            url: laroute.route('admin.config.edit-post-config-general'),
            method: 'POST',
            dataType: 'JSON',
            data: $('#form-update').serialize(),
            success: function (res) {
                if (!res.error) {
                    swal(res.message, '', 'success').then(function () {
                        window.location.href = laroute.route('admin.config.config-general');
                    });
                } else {
                    swal(res.message, '', 'error');
                }
            }
        });
    },
    
    updateBrand : function () {
        $.ajax({
            url: laroute.route('admin.config.edit-post-config-general'),
            method: 'POST',
            dataType: 'JSON',
            data: $('#form-update').serialize(),
            success: function (res) {
                if (!res.error) {
                    swal(res.message, '', 'success').then(function () {
                        window.location.href = laroute.route('admin.config.config-general');
                    });
                } else {
                    swal(res.message, '', 'error');
                }
            }
        });
    }
}

$(document).ready(function () {
    $('input[name="auto_apply_branch"]').change(function () {
        var check = $('input[name="auto_apply_branch"]:checked').val();
        if (check == 1){
            $('.input-number').prop('disabled',false);
        } else {
            $('.input-number').prop('disabled',true);
        }
    });
});

function uploadAvatar(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#image');
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_config-general.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#logo').val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}