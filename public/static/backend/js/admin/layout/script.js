//== Class definition
$('#sm-form').click(function () {
    let keyword = $('#m_typeahead_2').val();
    if (keyword != '') {
        $('#form-search').submit();
    }
});
var $input = $("#m_typeahead_2");
$input.typeahead({minLength: 1});
var Layout = {
    searchTypeahead: function (o) {
        var $this = $(o).val();
        $.ajax({
            url: laroute.route('admin.layout.search-dashboard'),
            dataType: 'JSON',
            method: 'POST',
            data: {
                keyword: $this
            },
            success: function (data) {
                $input.typeahead({
                    hint: true,
                    highlight: true,
                    source: data,
                    autoSelect: false,
                    items: 15,
                    minLength: 1
                });
                $('.dropdown-item').click(
                    function () {
                        var current = $input.typeahead("getActive");
                        if (current) {
                            $('#idSearchDashboard').val(current.id);
                            $('#nameSearchDashboard').val(current.name);
                            if ($('#idSearchDashboard').val() != '' && $('#nameSearchDashboard').val() != '') {
                                $('#form-search-hhidden').submit();
                            }
                        }
                    }
                );
            }
        });
    },
    clickDetail: function (id,name) {
        console.log(id)
        console.log(name)
        $('#idSearchDashboard').val(id);
        $('#nameSearchDashboard').val(name);
        if ($('#idSearchDashboard').val() != '' && $('#nameSearchDashboard').val() != '') {
            $('#form-search-hhidden').submit();
        }
    }
};

// $input.change(function () {
//     var current = $input.typeahead("getActive");
//     console.log(current.id);
// if (current) {
//     $('#idSearchDashboard').val(current.id);
//     $('#nameSearchDashboard').val(current.name);
//     if ($('#idSearchDashboard').val() != '' && $('#nameSearchDashboard').val() != '') {
//         $('#form-search-hhidden').submit();
//     }
// }
// });
//


var Paginate = {
    pageClickCustomer: function (page) {
        $.ajax({
            url: laroute.route('admin.layout.search.paging-customer'),
            method: "POST",
            data: {
                page: page,
                keyword: $('#keyword').val()
            },
            success: function (data) {
                $('.table-content-customer').empty();
                $('.table-content-customer').append(data);
            }
        });
    },
    pageClickCustomerAppointment: function (page) {
        $.ajax({
            url: laroute.route('admin.layout.search.paging-customer-appointment'),
            method: "POST",
            data: {
                page: page,
                keyword: $('#keyword').val()
            },
            success: function (data) {
                $('.table-content-customer-appointment').empty();
                $('.table-content-customer-appointment').append(data);
            }
        });
    },
    pageClickOrder: function (page) {
        $.ajax({
            url: laroute.route('admin.layout.search.paging-order'),
            method: "POST",
            data: {
                page: page,
                keyword: $('#keyword').val()
            },
            success: function (data) {
                $('.table-content-order').empty();
                $('.table-content-order').append(data);
            }
        });
    }
};

$('#m_quicksearch_input').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});

$('#m_quicksearch_input').bind("enterKey", function (e) {
    if ($('#m_quicksearch_input').val() != '') {
        $('#form-search').submit();
    }
});

