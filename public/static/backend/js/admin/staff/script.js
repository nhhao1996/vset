$(document).ready(function () {
    // $.getJSON(laroute.route('translate'), function (json) {
        $('#day').select2({
            placeholder: 'Ngày',
            allowClear: true
        });
        $('#month').select2({
            placeholder: 'Tháng',
            allowClear: true
        });
        $('#year').select2({
            placeholder: 'Năm',
            allowClear: true
        });
        // $('#staff_title_id').select2({
        //     placeholder: 'Hãy chọn chức vụ'
        // });
        // $('#department_id').select2({
        //     placeholder: 'Hãy chọn phòng ban'
        // });
        $('#is_admin').select2();
        $('.btn_add').click(function () {
            $.validator.addMethod("validateEmail", function (value, element) {
                return this.optional(element) || /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i.test(value);
            });
            $.validator.addMethod("validatePassword", function (value, element) {
                return this.optional(element) || /^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/i.test(value);
            });
            $.validator.addMethod("checkPhone", function (value, element) {
                var patt = new RegExp("((0|84)+(89|90|93|70|79|77|76|78)+([0-9]{7})\\b)|((0|84)+(86|96|97|98|32|33|34|35|36|37|38|39)+([0-9]{7})\\b)|((0|84)+(88|91|94|83|84|85|81|82)+([0-9]{7})\\b)|((0|84)+(99|59)+([0-9]{7})\\b)|((0|84)+(92|56|58)+([0-9]{7})\\b)");
                return patt.test(value);
            });
            $('#form-add').validate({
                rules: {
                    full_name: {
                        required: true,
                        maxlength: 100
                    },
                    phone1: {
                        required: true,
                        // number: true,
                        // minlength: 10,
                        // maxlength: 15,
                        checkPhone : true
                    },
                    // department_id: {
                    //     required: true
                    // },
                    // staff_title_id: {
                    //     required: true
                    // },
                    address: {
                        required: true,
                        maxlength: 255
                    },
                    user_name: {
                        required: true,
                        maxlength: 100
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 20,
                        validatePassword : true
                    },
                    repass: {
                        minlength: 8,
                        equalTo: "#password"
                    },
                    email : {
                        maxlength: 100,
                        validateEmail: true
                    },
                    // is_admin:{
                    //     required: true,
                    // }

                },
                messages: {
                    full_name: {
                        required: "Hãy nhập họ tên",
                        maxlength: 'Họ tên tối đa 100 ký tự'
                    },
                    phone1: {
                        required: 'Hãy nhập số điện thoại',
                        // number: 'Số điện thoại không hợp lệ',
                        // minlength: 'Tối thiểu 10 số',
                        // maxlength: 'Tối đa 15 số',
                        checkPhone : 'Số điện thoại không hợp lệ'
                    },
                    // department_id: {
                    //     required: 'Hãy chọn phòng ban'
                    // },
                    // staff_title_id: {
                    //     required: 'Hãy chọn chức vụ'
                    // },
                    address: {
                        required: 'Hãy nhập địa chỉ',
                        maxlength: 'Địa chỉ tối đa 255 ký tự'
                    },
                    user_name: {
                        required: 'Hãy nhập tên tài khoản',
                        maxlength: 'Tên tài khoản tối đa 100 ký tự'
                    },
                    password: {
                        required: 'Hãy nhập mật khẩu',
                        minlength: 'Tối thiểu 8 kí tự',
                        maxlength: 'Mật khẩu tối đa 20 ký tự',
                        validatePassword : 'Hãy nhập password từ 8 đến 20 ký tự bao gồm chữ và số'
                    },
                    repass: {
                        minlength: 'Tối thiểu 8 kí tự',
                        equalTo: "Nhập lại mật khẩu không đúng"
                    },
                    email : {
                        maxlength: 'Email tối đa 100 ký tự',
                        validateEmail: 'Email sai định dạng'
                    },
                    // is_admin:{
                    //     required: 'Hãy chọn quyền hạn',
                    // }
                },
            });

            if(!$('#form-add').valid()) {
                return false;
            } else {
                var full_name = $('#full_name').val();
                var phone = $('#phone1').val();
                var gender = $('input[name="gender"]:checked').val();
                // var staff_title_id = $('#staff_title_id').val();
                // var department_id = $('#department_id').val();
                var address = $('#address').val();
                var email = $('#email').val();
                var day = $('#day').val();
                var month = $('#month').val();
                var year = $('#year').val();
                var user_name = $('#user_name').val();
                // var is_admin = $('#is_admin').val();
                var is_admin = 0;
                var password = $('#password').val();
                var staff_avatar = $('#staff_avatar').val();
                var roleGroup = $('#role-group-id').val();
                if (email != '') {
                    if (!isValidEmailAddress(email)) {
                        $('.error_email').text('Email không hợp lệ');
                        return false;
                    } else {
                        $('.error_email').text('');
                        $.ajax({
                            url: laroute.route('admin.staff.submitAdd'),
                            dataType: 'JSON',
                            method: 'POST',
                            data: {
                                full_name: full_name,
                                phone: phone,
                                gender: gender,
                                // staff_title_id: staff_title_id,
                                // department_id: department_id,
                                address: address,
                                email: email,
                                day: day,
                                month: month,
                                year: year,
                                user_name: user_name,
                                is_admin: is_admin,
                                password: password,
                                staff_avatar: staff_avatar,
                                roleGroup: roleGroup
                            },
                            success: function (res) {
                                if (res.error_birthday == 1) {
                                    $('.error_birthday').text('Ngày sinh không hợp lệ');
                                } else {
                                    $('.error_birthday').text('');
                                }
                                if (res.error_user == 1) {
                                    $('.error_user').text('Tài khoản đã tồn tại');
                                } else {
                                    $('.error_user').text('');
                                }
                                if (res.success == 1) {
                                    swal("Thêm nhân viên thành công", "", "success").then(function () {
                                        window.location.reload();
                                    });
                                } else {
                                    swal("Thêm nhân viên thất bại", "", "error");
                                }
                            }
                        });
                    }
                } else {
                    $.ajax({
                        url: laroute.route('admin.staff.submitAdd'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {
                            full_name: full_name,
                            phone: phone,
                            gender: gender,
                            // staff_title_id: staff_title_id,
                            // department_id: department_id,
                            address: address,
                            email: email,
                            day: day,
                            month: month,
                            year: year,
                            user_name: user_name,
                            is_admin: is_admin,
                            password: password,
                            staff_avatar: staff_avatar,
                            roleGroup: roleGroup
                        },
                        success: function (res) {
                            if (res.error_birthday == 1) {
                                $('.error_birthday').text('Ngày sinh không hợp lệ');
                            } else {
                                $('.error_birthday').text('');
                            }
                            if (res.error_user == 1) {
                                $('.error_user').text('Tài khoản đã tồn tại');
                            } else {
                                $('.error_user').text('');
                            }
                            if (res.success == 1) {
                                swal("Thêm nhân viên thành công", "", "success").then(function () {
                                    window.location.reload();
                                });
                            }else {
                                swal("Thêm nhân viên thất bại", "", "error");
                            }
                        }
                    });
                }
            }
        });
    // });
    // $.getJSON(laroute.route('translate'), function (json) {
    $('.btn_add_close').click(function () {
        $.validator.addMethod("validateEmail", function (value, element) {
            return this.optional(element) || /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i.test(value);
        });
        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/i.test(value);
        });
        $.validator.addMethod("checkPhone", function (value, element) {
            var patt = new RegExp("((0|84)+(89|90|93|70|79|77|76|78)+([0-9]{7})\\b)|((0|84)+(86|96|97|98|32|33|34|35|36|37|38|39)+([0-9]{7})\\b)|((0|84)+(88|91|94|83|84|85|81|82)+([0-9]{7})\\b)|((0|84)+(99|59)+([0-9]{7})\\b)|((0|84)+(92|56|58)+([0-9]{7})\\b)");
            return patt.test(value);
        });
        $('#form-add').validate({
            rules: {
                full_name: {
                    required: true,
                    maxlength: 100
                },
                phone1: {
                    required: true,
                    // number: true,
                    // minlength: 10,
                    // maxlength: 15,
                    checkPhone : true
                },
                // department_id: {
                //     required: true
                // },
                // staff_title_id: {
                //     required: true
                // },
                address: {
                    required: true,
                    maxlength: 255
                },
                user_name: {
                    required: true,
                    maxlength: 100
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 20,
                    validatePassword : true
                },
                repass: {
                    minlength: 8,
                    equalTo: "#password"
                },
                email : {
                    maxlength: 100,
                    validateEmail: true
                },
                // is_admin:{
                //     required: true,
                // }

            },
            messages: {
                full_name: {
                    required: "Hãy nhập họ tên",
                    maxlength: 'Họ tên tối đa 100 ký tự'
                },
                phone1: {
                    required: 'Hãy nhập số điện thoại',
                    // number: 'Số điện thoại không hợp lệ',
                    // minlength: 'Tối thiểu 10 số',
                    // maxlength: 'Tối đa 15 số',
                    checkPhone : 'Số điện thoại không hợp lệ'
                },
                // department_id: {
                //     required: 'Hãy chọn phòng ban'
                // },
                // staff_title_id: {
                //     required: 'Hãy chọn chức vụ'
                // },
                address: {
                    required: 'Hãy nhập địa chỉ',
                    maxlength: 'Địa chỉ tối đa 255 ký tự'
                },
                user_name: {
                    required: 'Hãy nhập tên tài khoản',
                    maxlength: 'Tên tài khoản tối đa 100 ký tự'
                },
                password: {
                    required: 'Hãy nhập mật khẩu',
                    minlength: 'Tối thiểu 8 kí tự',
                    maxlength: 'Mật khẩu tối đa 20 ký tự',
                    validatePassword : 'Hãy nhập password từ 8 đến 20 ký tự bao gồm chữ và số'
                },
                repass: {
                    minlength: 'Tối thiểu 8 kí tự',
                    equalTo: "Nhập lại mật khẩu không đúng"
                },
                email : {
                    maxlength: 'Email tối đa 100 ký tự',
                    validateEmail: 'Email sai định dạng'
                },
                // is_admin:{
                //     required: 'Hãy chọn quyền hạn',
                // }
            },
        });

        if (!$('#form-add').valid()) {
            return false;
        } else {
            var full_name = $('#full_name').val();
            var phone = $('#phone1').val();
            var gender = $('input[name="gender"]:checked').val();
            // var staff_title_id = $('#staff_title_id').val();
            // var department_id = $('#department_id').val();
            var address = $('#address').val();
            var email = $('#email').val();
            var day = $('#day').val();
            var month = $('#month').val();
            var year = $('#year').val();
            var user_name = $('#user_name').val();
            // var is_admin = $('#is_admin').val();
            var is_admin = 0;
            var password = $('#password').val();
            var staff_avatar = $('#staff_avatar').val();
            var roleGroup = $('#role-group-id').val();
            // $.getJSON(laroute.route('translate'), function (json) {
            if (email != '') {
                if (!isValidEmailAddress(email)) {
                    $('.error_email').text('Email không hợp lệ');
                    return false;
                } else {
                    $('.error_email').text('');
                    $.ajax({
                        url: laroute.route('admin.staff.submitAdd'),
                        dataType: 'JSON',
                        method: 'POST',
                        data: {
                            full_name: full_name,
                            phone: phone,
                            gender: gender,
                            // staff_title_id: staff_title_id,
                            // department_id: department_id,
                            address: address,
                            email: email,
                            day: day,
                            month: month,
                            year: year,
                            user_name: user_name,
                            is_admin: is_admin,
                            password: password,
                            staff_avatar: staff_avatar,
                            roleGroup: roleGroup
                        },
                        success: function (res) {
                            if (res.error_birthday == 1) {
                                $('.error_birthday').text('Ngày sinh không hợp lệ');
                            } else {
                                $('.error_birthday').text('');
                            }
                            if (res.error_user == 1) {
                                $('.error_user').text('Tài khoản đã tồn tại');
                            } else {
                                $('.error_user').text('');
                            }
                            if (res.success == 1) {
                                swal("Thêm nhân viên thành công", "", "success").then(function () {
                                    window.location = laroute.route('admin.staff');
                                });
                            }else {
                                swal("Thêm nhân viên thất bại", "", "error");
                            }
                        }
                    });
                }
            } else {
                $.ajax({
                    url: laroute.route('admin.staff.submitAdd'),
                    dataType: 'JSON',
                    method: 'POST',
                    data: {
                        full_name: full_name,
                        phone: phone,
                        gender: gender,
                        // staff_title_id: staff_title_id,
                        // department_id: department_id,
                        address: address,
                        email: email,
                        day: day,
                        month: month,
                        year: year,
                        user_name: user_name,
                        is_admin: is_admin,
                        password: password,
                        staff_avatar: staff_avatar,
                        roleGroup: roleGroup
                    },
                    success: function (res) {
                        if (res.error_birthday == 1) {
                            $('.error_birthday').text('Ngày sinh không hợp lệ');
                        } else {
                            $('.error_birthday').text('');
                        }
                        if (res.error_user == 1) {
                            $('.error_user').text('Tài khoản đã tồn tại');
                        } else {
                            $('.error_user').text('');
                        }
                        if (res.success == 1) {
                            swal("Thêm nhân viên thành công", "", "success").then(function () {
                                window.location = laroute.route('admin.staff');
                            });
                        }else {
                            swal("Thêm nhân viên thất bại", "", "error");
                        }

                    }
                });
            }
        }
    });
    // });
});

var staff = {
    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');
        $.getJSON(laroute.route('translate'), function (json) {
            swal({
                title: json['Thông báo'],
                text: json["Bạn có muốn xóa không?"],
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: json['Xóa'],
                cancelButtonText: json['Hủy'],
                onClose: function () {
                    // remove hightlight row
                    $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    $.post(laroute.route('admin.staff.remove', {id: id}), function () {
                        swal(
                            json['Xóa thành công'],
                            '',
                            'success'
                        );
                        // window.location.reload();
                        $('#autotable').PioTable('refresh');
                    });
                }
            });
        });
    },
    changeStatus: function (obj, id, action) {
        $.ajax({
            url: laroute.route('admin.staff.change-status'),
            method: "POST",
            data: {
                id: id, action: action
            },
            dataType: "JSON"
        }).done(function (data) {
            $('#autotable').PioTable('refresh');
        });
    },
    refresh: function () {
        $('input[name="search"]').val('');
        $('.m_selectpicker').val('');
        $('.m_selectpicker').selectpicker('refresh');
        $(".btn-search").trigger("click");
    }
};
var staffTitle = {
    add: function (parameter) {
        var staffTitleName = $('#modalAdd #staff_title_name');
        var staffTitleDescription = $('#modalAdd #staff_title_description');
        var errorStaffTitleName = $('#modalAdd .error-staff_title_name');
        $.getJSON(laroute.route('translate'), function (json) {
            if (staffTitleName.val() == '') {
                errorStaffTitleName.text(json['Vui lòng nhập tên chức vụ']);
            } else {
                errorStaffTitleName.text('');
                $.ajax({
                    url: laroute.route('admin.staff-title.submitadd'),
                    method: "POST",
                    data: {
                        staffTitleName: staffTitleName.val(),
                        staffTitleDescription: staffTitleDescription.val(),
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            if (parameter == 0) {
                                $('#modalAdd').modal('hide');
                            }
                            swal(
                                json['Thêm chức vụ thành công'],
                                '',
                                'success'
                            );
                            staffTitleName.val('');
                            staffTitleDescription.val('');
                            $('#staff_title_id > option').remove();
                            $('#staff_title_id').append('<option></option>');
                            $.each(data.optionStaffTitle, function (index, element) {
                                $('#staff_title_id').append('<option value="' + index + '">' + element + '</option>')
                            });
                            $('#autotable').PioTable('refresh');
                        } else {
                            errorStaffTitleName.text(json['Chức vụ đã tồn tại']);
                        }
                    }
                });
            }
        });
    },
};
var Department = {
    add: function () {
        $(".department-name").css("color", "red");
        let departmentName = $('#department_name');
        let check = 0;
        $.getJSON(laroute.route('translate'), function (json) {
            if ($('#is_inactive').is(':checked')) {
                check = 1;
            }
            if (departmentName.val() != "") {
                $.ajax({
                    url: laroute.route('admin.department.add'),
                    data: {
                        departmentName: departmentName.val(),
                        isInActive: check
                    },
                    method: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        
                        if (data.status == 1) {
                            swal(
                                json['Thêm phòng ban thành công'],
                                '',
                                'success'
                            );
                            clearModalAdd();
                            $(".department-name").text('');
                            $('#autotable').PioTable('refresh');

                            $('#department_id > option').remove();
                            $('#department_id').append('<option></option>');
                            $.each(data.optionDepartment, function (index, element) {
                                $('#department_id').append('<option value="' + index + '">' + element + '</option>')
                            });
                        }
                        if (data.status == 0) {
                            $(".department-name").text(json['Phòng ban đã tồn tại']);
                        }
                    }
                });

            } else {
                $('.department-name').text(json['Vui lòng nhập tên phòng ban']);
            }
        });
    },
    addClose: function () {
        $(".department-name").css("color", "red");
        let departmentName = $('#department_name');
        let check = 0;
        $.getJSON(laroute.route('translate'), function (json) {
            if ($('#is_inactive').is(':checked')) {
                check = 1;
            }
            if (departmentName.val() != "") {
                $.ajax({
                    url: laroute.route('admin.department.add'),
                    data: {
                        departmentName: departmentName.val(),
                        isInActive: check
                    },
                    method: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        if (data.status == 1) {
                            $('#autotable').PioTable('refresh');
                            swal(
                                json['Thêm phòng ban thành công'],
                                '',
                                'success'
                            );
                            $('#modalAddPartment').modal('hide');
                            clearModalAdd();
                            $("#department_name").text('');

                            $('#department_id > option').remove();
                            $('#department_id').append('<option></option>');
                            $.each(data.optionDepartment, function (index, element) {
                                $('#department_id').append('<option value="' + index + '">' + element + '</option>')
                            });
                        }
                        if (data.status == 0) {
                            $(".department-name").text(json['Phòng ban đã tồn tại']);
                        }
                    }
                });

            } else {
                $('.department-name').text(json['Vui lòng nhập tên phòng ban']);
            }
        });
    },
};

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.staff.list')
});
$('.m_selectpicker').selectpicker();

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function uploadImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: laroute.route("admin.staff.uploads"),
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.success == 1) {
                    $('#staff_avatar').val(res.file);
                }

            }
        });
    }
}
$('.js-example-data-ajax').select2({
    placeholder: "Chọn nhóm quyền",
});
function onmouseoverAddNew() {
    $('.dropdow-add-new').show();
}

function onmouseoutAddNew() {
    $('.dropdow-add-new').hide();
}

function clearModalAdd() {
    $('#modalAddPartment #department_name').val('');
    $('#modalAddPartment #is_inactive').val('1');
    $('#modalAddPartment .department-name').text('');
}

