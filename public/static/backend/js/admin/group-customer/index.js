$('#autotable').PioTable({
    baseUrl: laroute.route('admin.group-customer.list')
});
var groupCustomer = {
    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                // remove hightlight row
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.group-customer.destroy', {id: id}), function (result) {
                    if (result.error == false) {
                        swal(
                            'Xóa thành công',
                            '',
                            'success'
                        );
                        $('#autotable').PioTable('refresh');
                    } else {
                        swal(
                            result.message,
                            '',
                            'error'
                        );
                        $('#autotable').PioTable('refresh');
                    }
                });
            }
        });
    },
};