var unique = $('#unique').val();
var groupCustomer = {
    init: function () {
        $.ajaxSetup({
            async: false,
        });
        groupCustomer.loadCustomerSelected();
    },
    loadCustomer: function (page = 1) {
        var keyword =  $('#modal_customer .keyword').val();
        var isActived =  $('#modal_customer .is_actived').val();
        $.ajax({
            url: laroute.route('admin.group-customer.loadCustomer'),
            method: "POST",
            data: {
                unique: unique,
                page: page,
                keyword: keyword,
                isActived: isActived,
            },
            success: function (res) {
                $('#div_list_customer').html(res);
            }
        });
    },
    checkedAllItem: function (o) {
        var type = 'unchecked';
        if ($(o).is(':checked')) {
            type = 'checked';
            $('#modal_customer .checkbox_item').prop('checked', true);
        } else {
            $('#modal_customer .checkbox_item').prop('checked', false);
        }

        var arrItemId = [];
        $('#modal_customer .checkbox_item').each(function () {
            var itemId = $(this).val();
            arrItemId.push(itemId);
        });
        groupCustomer.checkedItem(type, arrItemId);
    },
    checkedOneItem: function (o, id) {
        var type = 'unchecked';
        if ($(o).is(':checked')) {
            type = 'checked';
        }
        var arrItemId = [];
        arrItemId.push(id);
        groupCustomer.checkedItem(type, arrItemId);
    },
    checkedItem: function (type, array_item) {
        $.ajax({
            url: laroute.route('admin.group-customer.checkedCustomer'),
            method: "POST",
            data: {
                unique: unique,
                type: type,
                array_item: array_item,
            },
            success: function (res) {}
        });
    },
    showModalItem: function () {
        $('#modal_customer .keyword').val('');
        $('#modal_customer .is_actived').val('');
        $.ajax({
            url: laroute.route('admin.group-customer.removeSessionTemp'),
            method: "POST",
            data: {
                unique: unique,
            },
            success: function (res) {
                groupCustomer.loadCustomer();
            }
        });
    },
    submitAddItem: function () {
        $('#modal_customer').modal('hide');
        $.ajax({
            url: laroute.route('admin.group-customer.submitAddItem'),
            method: "POST",
            data: {
                unique: unique,
            },
            success: function (res) {
                groupCustomer.loadCustomerSelected();
            }
        });
    },
    loadCustomerSelected: function (page = 1) {
        var keyword =  $('#form_data .keyword').val();
        var isActived =  $('#form_data .is_actived').val();
        var show =  $('#show').val();
        $.ajax({
            url: laroute.route('admin.group-customer.loadCustomer'),
            method: "POST",
            data: {
                unique: unique,
                page: page,
                isCustomerSelected: 1,
                keyword: keyword,
                isActived: isActived,
                show: show,
            },
            success: function (res) {
                $('#div_list_customer_selected').html(res);
            }
        });
    },
    removeItem: function (id, page = 1) {
        var keyword =  $('#form_data .keyword').val();
        var isActived =  $('#form_data .is_actived').val();
        $.ajax({
            url: laroute.route('admin.group-customer.removeItem'),
            method: "POST",
            data: {
                unique: unique,
                id: id,
            },
            success: function (res) {
                groupCustomer.loadCustomerSelected(page);
            }
        });
    },
    store: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var group_name =  $('#group_name').val();
            $.ajax({
                url: laroute.route('admin.group-customer.store'),
                method: "POST",
                data: {
                    unique: unique,
                    group_name: group_name,
                },
                success: function (res) {
                    if (res.error == 0) {
                        swal.fire(json['Thêm thành công!'], "", "success").then(function (result) {
                            if (result.value == true) {
                                window.location.href = laroute.route('admin.group-customer');
                            }
                        });
                    } else {
                        var mess_error = '';
                        $.map(res.array_error, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Thêm thất bại!'], mess_error, "error");
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Thêm thất bại!'], mess_error, "error");
                    }
                }
            });
        });
    },
    update: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var group_name =  $('#group_name').val();
            var group_customer_id =  $('#group_customer_id').val();
            $.ajax({
                url: laroute.route('admin.group-customer.update'),
                method: "POST",
                data: {
                    unique: unique,
                    group_name: group_name,
                    group_customer_id: group_customer_id,
                },
                success: function (res) {
                    if (res.error == 0) {
                        swal.fire(json['Chỉnh sửa thành công!'], "", "success").then(function (result) {
                            if (result.value == true) {
                                window.location.href = laroute.route('admin.group-customer');
                            }
                        });
                    } else {
                        var mess_error = '';
                        $.map(res.array_error, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Chỉnh sửa thất bại!'], mess_error, "error");
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire(json['Chỉnh sửa thất bại!'], mess_error, "error");
                    }
                }
            });
        });
    },

};
groupCustomer.init();