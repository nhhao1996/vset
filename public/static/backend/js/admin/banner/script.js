$('#autotable').PioTable({
    baseUrl: laroute.route('admin.banner.list')
});

function uploadImageDesktop(input,lang) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img_desktop_main_'+lang)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_img_desktop_main_'+lang).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_banner_desktop_main_'+lang+'.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#img_desktop_upload_'+lang).val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function uploadImageMobile(input,lang) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img_mobile_main_'+lang)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_img_mobile_main_'+lang).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_banner_mobile_main_'+lang+'.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#img_mobile_upload_'+lang).val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

var banner = {
    addBanner: function (close) {
        $('#form-banner').validate({
            rules: {
                name: {
                    required: true,
                    maxlength:255
                },
            },
            messages: {
                name: {
                    required: 'Yêu cầu nhập tên popup khuyến mãi',
                    maxlength: "Tên popup khuyến mãi vượt quá 255 ký tự"
                },
            },
        });

        if (!$('#form-banner').valid()) {
            return false;
        } else {

            var is_actived = 0 ;
            if ($('#is_actived').is(':checked')) {
                is_actived = 1;
            }

            $.ajax({
                url: laroute.route('admin.banner.submit-add'),
                method: "POST",
                dataType: "JSON",
                data: $('#form-banner').serialize()+'&is_actived='+is_actived,
                success: function (data) {
                    if (data.error == true) {
                        swal('', data.message, "error");
                    } else {
                        swal('', data.message, "success").then(function () {
                            if (close == 1) {
                                window.location.href = laroute.route('admin.banner');
                            } else {
                                window.location.href = laroute.route('admin.banner.add');
                            }
                        });
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire('', mess_error, "error");
                    }
                }
            });
        }
    },

    editBanner: function () {
        $('#form-banner').validate({
            rules: {
                name: {
                    required: true,
                    maxlength:255
                },
            },
            messages: {
                name: {
                    required: 'Yêu cầu nhập tên popup khuyến mãi',
                    maxlength: "Tên popup khuyến mãi vượt quá 255 ký tự"
                },
            },
        });

        if (!$('#form-banner').valid()) {
            return false;
        } else {

            var is_actived = 0 ;
            if ($('#is_actived').is(':checked')) {
                is_actived = 1;
            }

            $.ajax({
                url: laroute.route('admin.banner.submit-edit'),
                method: "POST",
                dataType: "JSON",
                data: $('#form-banner').serialize()+'&is_actived='+is_actived,
                success: function (data) {
                    if (data.error == true) {
                        swal('', data.message, "error");
                    } else {
                        swal('', data.message, "success").then(function () {
                            window.location.href = laroute.route('admin.banner');
                        });
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire('', mess_error, "error");
                    }
                }
            });
        }
    },
}