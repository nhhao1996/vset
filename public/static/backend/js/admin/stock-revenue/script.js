$(document).ready(function (){
    $('#filter_type').change(function (){
        if ($('#filter_type').val() != 'week'){
        }
        $('.block_hide').hide();
        var block = $('#filter_type').val();
        $('.'+block+'_block').show();
    });
    $('input[type="radio"]').click(function(){
        stockRevenue.filter();
    });
});
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
var stockRevenue = {
    colorOption: ['#f28f43', '#999999', '#2f7ed8', '#8bbc21',
        '#910000','#492970', '#f28f43', '#77a1e5'],
    init: function () {
            var arrRange = {};
            arrRange["Hôm nay"] = [moment(), moment()];
            arrRange["Ngày mai"] = [moment().add(1, "days"), moment().add(1, "days")];
            arrRange["7 ngày sau"] = [moment(), moment().add(6, "days")];
            arrRange["30 ngày sau"] = [moment(), moment().add(29, "days")];
            arrRange["Trong tháng"] = [moment().startOf("months"), moment().endOf("months")];
            arrRange["Tháng sau"] = [moment().add(1, "months").startOf("months"), moment().add(1, "months").endOf("months")];
        $("#time").daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            buttonClasses: "m-btn btn",
            applyClass: "btn-primary",
            cancelClass: "btn-danger",
            startDate: "-6m",
            "dateLimit": {
                "month": 6
            },
            locale: {
                format: 'DD/MM/YYYY',
                "applyLabel": "Đồng ý",
                "cancelLabel": "Thoát",
                "customRangeLabel": "Tùy chọn ngày",
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                ],
                "monthNames": [
                    "Tháng 1 năm",
                    "Tháng 2 năm",
                    "Tháng 3 năm",
                    "Tháng 4 năm",
                    "Tháng 5 năm",
                    "Tháng 6 năm",
                    "Tháng 7 năm",
                    "Tháng 8 năm",
                    "Tháng 9 năm",
                    "Tháng 10 năm",
                    "Tháng 11 năm",
                    "Tháng 12 năm"
                ],
                "firstDay": 1
            },
            // ranges: {daterangepicker
            //     'Hôm nay': [moment(), moment()],
            //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
            //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
            //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
            //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
            //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            // }
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            stockRevenue.filter();
        });

        $("#month_start").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            startDate: "-24m",
            endDate: "0m"
        });

        $("#month_end").datepicker({
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months",
            language: "vi",
            startDate: "-24m",
            endDate: "0m"
        });
        // });
        $("#time-hidden").daterangepicker({
            startDate: moment().add(-1, "months"),
            endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('#time').val($("#time-hidden").val());

        stockRevenue.filter();
    },
    filter: function () {
        var selectedVal = "";
        var selected = $("#radio input[type='radio']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
        }
        window.type_chart = selectedVal;
        $.ajax({
            url: laroute.route('admin.report.stock-revenue.filter'),
            method: "POST",
            dataType: 'json',
            data: {
                'type_chart' : selectedVal,
                'filter_type' : $('#filter_type').val(),
                'time' : $('#time').val(),
                'month_start' : $('#month_start').val(),
                'month_end' : $('#month_end').val(),
            },
            sync: false,
            success: function (res) {
                $('#chart').html('');$('#text_description').html('');$('#payment_method_chart').html('');
                showChart(res.cate,res.chart);
                showChatPaymentMethod(res.payment_method_chart);
                let htmlString = "";
                let time = $('#filter_type').val() == 'day' ? $('#time').val() : ($('#month_start').val()+'-'+$('#month_end').val());
                let description = res.text_description;
                console.log(description);
                switch (selectedVal) {
                    case "revenue":
                        htmlString+= `
                            <div class="form-group mt-4"><b>Doanh thu đầu tư cổ phiếu</b>&nbsp;&nbsp;&nbsp;Thời gian: ${time}</div>
                            <div class="form-group">Tổng số Cổ phiếu bán: ${description != null ? formatNumber(Number(description.total_quantity).toFixed(decimal_number)) : 0} CP</div>
                            <div class="form-group money">Giá trị phát hành cao nhất: ${description != null ? formatNumber(Number(description.max_publisher).toFixed(decimal_number)) : 0} VNĐ</div>
                            <div class="form-group money">Giá trị phát hành thấp nhất: ${description != null ? formatNumber(Number(description.min_publisher).toFixed(decimal_number)) : 0} VNĐ</div>
                            <div class="form-group money">Giá trị phát hành trung bình: ${description != null ? formatNumber(Number(description.avg_publisher).toFixed(decimal_number)) : 0} VNĐ</div>
                            <div class="form-group money">Tổng giá trị cổ phiếu bán được: ${description != null ? formatNumber(Number(description.total_revenue).toFixed(decimal_number)) : 0} VNĐ</div>
                        `;
                        break;
                    case "count-transaction":
                        htmlString+= `
                            <div class="form-group mt-4"><b>Số lượng giao dịch của nhà phát hành</b>&nbsp;&nbsp;&nbsp;Thời gian: ${time}</div>
                            <div class="form-group">Tổng số giao dịch: ${description != null ? description.total_transaction : 0} GD</div>
                            <div class="form-group">Tổng số cổ phiếu giao dịch: ${description != null ? formatNumber(Number(description.total_quantity).toFixed(decimal_number)) : 0} CP</div>
                            <div class="form-group money">Tổng giá trị giao dịch: ${description != null ? formatNumber(Number(description.total_revenue).toFixed(decimal_number)) : 0} VNĐ</div>
                        `;
                        break;
                    case "market-transaction":
                        htmlString+= `
                            <div class="form-group mt-4"><b>Giao dịch mua bán trên chợ</b>&nbsp;&nbsp;&nbsp;Thời gian: ${time}</div>
                            <div class="form-group">Tổng số giao dịch: ${description != null ? description.count_transaction : 0} GD</div>
                            <div class="form-group">Tổng số cổ phiếu giao dịch: ${description != null ? formatNumber(Number(description.total_quantity).toFixed(decimal_number)) : 0} CP</div>
                            <div class="form-group money">Tổng giá trị giao dịch: ${description != null ? formatNumber(Number(description.total_revenue).toFixed(decimal_number)) : 0} VNĐ</div>
                            <div class="form-group money">Tổng doanh thu từ thu phí: ${description != null ? formatNumber(Number(description.total_fee_revenue).toFixed(decimal_number)) : 0} VNĐ</div>
                        `;
                        break;
                    case "transfer-transaction":
                        htmlString+= `
                            <div class="form-group mt-4"><b>Giao dịch chuyển nhượng</b>&nbsp;&nbsp;&nbsp;Thời gian: ${time}</div>
                            <div class="form-group">Tổng số giao dịch chuyển nhượng: ${description != null ? description.paysuccess_request : 0} GD</div>
                            <div class="form-group">Tổng số cổ phiếu chuyển nhượng: ${description != null ? formatNumber(Number(description.total_quantity).toFixed(decimal_number)) : 0} CP</div>
                            <div class="form-group money">Tổng doanh thu từ thu phí: ${description != null ? formatNumber(Number(description.total_fee_revenue).toFixed(decimal_number)) : 0} VNĐ</div>
                        `;
                        break;
                }
                $('#text_description').html(htmlString);
                // AutoNumeric.multiple('.money',{
                //     currencySymbol : '',
                //     decimalCharacter : '.',
                //     digitGroupSeparator : ',',
                //     decimalPlaces: decimal_number
                // });
            }
        });
    },
};

function showChatPaymentMethod(data) {
    Highcharts.chart('payment_method_chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<b>{point.name}: {point.percentage:.2f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                colors: stockRevenue.colorOption,
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            data: data
        }]
    });
};
function showChart(cate,chart){
    let text = window.type_chart != 'count-transaction' ? 'Số tiền (VNĐ)' : 'Số lượng giao dịch';
    let units = window.type_chart != 'count-transaction';
    Highcharts.chart('chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: cate
        },
        yAxis: {
            allowDecimals: units,
            unit : [' VNĐ'],
            min: 0,
            title: {
                text: text
            },
        },
        colors: stockRevenue.colorOption,
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            pointFormatter: function() {
                var abc = '';
                var value;
                var stackTotalValue;
                if(units){
                    value = formatNumber(Number(this.y).toFixed(decimal_number)) + ' VNĐ';
                    stackTotalValue = formatNumber(Number(this.stackTotal).toFixed(decimal_number)) + ' VNĐ';
                }
                else{
                    value = this.y;
                    stackTotalValue = this.stackTotal;
                }
                return '<span>' + this.series.name + '</span>: <b>' + value + '</b><br>'
                     + '<span>' + 'Total: ' + '</span>: <b>' + stackTotalValue + '</b>';
            },
        },
        plotOptions: {
            column: {
                stacking: 'normal',
            }
        },
        series: chart
    });
}
stockRevenue.init();

$(document).ready(function (){
    $(".example").daterangepicker({
        single: true,
        standalone: true
    });
})