var support = {
    editStatus:function () {
        var status = $("select[name=support_request_status]").val();

        $.ajax({
            url: laroute.route('admin.support.update-status'),
            dataType: 'JSON',
            method: 'POST',
            data: {
                status: status,
                id:$("#support_request_id_hidden").val()
            },
            success: function (res) {
                if (res.success == 1) {
                    swal.fire(res.message, "", "success").then(function () {
                        window.location.href = laroute.route('admin.support');
                    });
                } else {
                    swal.fire(res.message, '', "error");
                }
            },
            error: function (res) {
                var mess_error = '';
                $.map(res.responseJSON.errors, function (a) {
                    mess_error = mess_error.concat(a + '<br/>');
                });
                swal.fire('Chỉnh sửa thông tin hỗ trợ thất bại', mess_error, "error");
            }
        });
    }
}