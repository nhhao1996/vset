var sendEmail = {
    save: function() {
        $('.position_error').empty();
        var form = $('#form-update');
        form.validate({
            rules: {
                // email_name: {
                //     required: true,
                //     maxlength: 250
                // },
                email: {
                    required: true,
                    maxlength: 250
                },
                emailstaff:{
                    required: true,
                    maxlength: 250
                }
            },
            messages: {
                // email_name: {
                //     required: name_required,
                //     maxlength: name_max
                // },
                email: {
                    required: email_required,
                    maxlength: email_max
                },
                emailstaff: {
                    required: email_required,
                    maxlength: email_max
                }
            },
        });
        if (!form.valid()) {
            return false;
        }

        var continute = true;
        var arrEmail = [];
        var numberField = 0;
        var email_staff = $('#email_staff').val();
        var phone_staff = $('#phone_staff').val();
        $.each($('.field_cc').find('.field_email_cc_form'), function () {
            var email = $(this).find($('.email_cc')).val();
            var active = 0;
            if ($(this).find($('.email_cc_check')).is(':checked')) {
                active = 1;
            }

            if (email == '') {
                $('.position_error_' + numberField).text(email_cc_required);
                continute = false;
            } else if (email.length > 250 ) {
                $('.position_error_' + numberField).text(email_cc_max);
                continute = false;
            } else if (checkEmail(email) == false){
                $('.position_error_' + numberField).text('Email không hợp lệ');
                continute = false;
            }
            numberField++;

            arrEmail.push({
                email: email,
                is_actived : active,
            });
        });


        if (email_staff == '') {
            $('.position_error_email_staff').text(email_cc_required);
            continute = false;
        } else if (email_staff.length > 250 ) {
            $('.position_error_email_staff').text(email_cc_max);
            continute = false;
        } else if (checkEmail(email_staff) == false) {
            $('.position_error_email_staff').text('Email không hợp lệ');
            continute = false;
        }

        if (phone_staff == '') {
            $('.position_error_phone_staff').text(email_cc_required);
            continute = false;
        } else if (phone_staff.length > 12 ) {
            $('.position_error_phone_staff').text('Số điện thoại vượt quá 12 số');
            continute = false;
        } else if(checkPhone(phone_staff) == false) {
            $('.position_error_phone_staff').text('Số điện thoại không hợp lệ');
            continute = false;
        }

        if (continute == false) {
            return true;
        } else {
            var is_actived = 0;
            if ($(".is_actived").is(':checked')) {
                is_actived = 1;
            }

            $.ajax({
                url: laroute.route('admin.send-email.update'),
                method: "POST",
                dataType: "JSON",
                data: {
                    email_cc : arrEmail,
                    email_staff : email_staff,
                    phone_staff : phone_staff
                },
                success: function (res) {
                    if (res.error == false) {
                        swal.fire(res.message, "", "success").then(function (result) {
                            window.location.href = laroute.route('admin.send-email');
                        });
                    } else {
                        swal.fire(res.message, '', "error");
                    }
                },
                // error: function (res) {
                //     var mess_error = '';
                //     $.map(res.responseJSON.errors, function (a) {
                //         mess_error = mess_error.concat(a + '<br/>');
                //     });
                //     swal.fire(json.product_model.STORE_FAILED, mess_error, "error");
                // }
            });
        }

    },
    addField: function () {
        var tpl = $('#field_email_cc').html();
        tpl = tpl.replace(/{number}/g, number);
        $('.field_cc').append(tpl);
        number ++;
    },

    deleteField :function (number) {
        $('.field_email_cc_'+number).remove();
    },

    cancel: function () {
        Swal.fire({
            title: cancel,
            html: yes_text,
            buttonsStyling: false,

            confirmButtonText: yes,
            confirmButtonClass: "btn btn-sm btn-default btn-bold btn_yes ",

            showCancelButton: true,
            cancelButtonText: no,
            cancelButtonClass: "btn btn-sm btn-bold btn-brand btn_cancel "
        }).then(function (result) {
            if (result.value) {
                window.location.href = laroute.route('admin.send-email');
            }
        });
    }
};

function checkPhone (value) {
    var patt = new RegExp("((0|84)+(89|90|93|70|79|77|76|78)+([0-9]{7})\\b)|((0|84)+(86|96|97|98|32|33|34|35|36|37|38|39)+([0-9]{7})\\b)|((0|84)+(88|91|94|83|84|85|81|82)+([0-9]{7})\\b)|((0|84)+(99|59)+([0-9]{7})\\b)|((0|84)+(92|56|58)+([0-9]{7})\\b)");
    return patt.test(value);
};
function checkEmail (value) {
    return /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i.test(value);
};