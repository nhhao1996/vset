$('#autotable').PioTable({
    baseUrl: laroute.route('admin.manager-extend-contract.list')
});

$(document).ready(function () {
    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        // maxDate: moment().endOf("day"),
        // startDate: moment().startOf("day"),
        // endDate: moment().add(1, 'days'),
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });
})

var managerExtendContract = {

    checkCreateContract : function () {
        $.ajax({
            url: laroute.route('admin.manager-extend-contract.check-create-contract'),
            dataType: 'JSON',
            data: $('#form-product').serialize(),
            method: 'POST',
            success: function (res) {
                if (res.error == false){
                    swal.fire('', res.message, "success").then(function () {
                        window.location.href = laroute.route('admin.manager-extend-contract');
                    });
                } else {
                    swal.fire('', res.message, "error");
                }
            }
        });
    },
    checkExtendContract : function (id,code) {
        $.ajax({
            url: laroute.route('admin.manager-extend-contract.checkExtendContract'),
            dataType: 'JSON',
            data: {
                customer_contract_id: id,
                customer_contract_code: code,
            },
            method: 'POST',
            success: function (res) {
                if (res.error == false){
                    window.location.href = laroute.route('admin.manager-extend-contract.extendContract',{id:res.id});
                } else {
                    swal.fire('', res.message, "error");
                }
            }
        });
    },

    checkValue : function () {
        var product_id = $('#product_id').val();
        var investment_time_id = $('#investment_time_id').val();
        var quantity = $('#quantity').val();
        var withdraw_interest_time_id = $('#withdraw_interest_time_id').val();
        var customer_contract_id = $('#customer_contract_id').val();
        var total_old = $('.total_old').val();
        $.ajax({
            url: laroute.route('admin.manager-extend-contract.checkValue'),
            dataType: 'JSON',
            data: {
                product_id : product_id,
                investment_time_id : investment_time_id,
                quantity : quantity,
                withdraw_interest_time_id : withdraw_interest_time_id,
                customer_contract_id : customer_contract_id,
                total_old : total_old
            },
            method: 'POST',
            success: function (res) {
                var interest_rate_text = 0;
                var bonus_text = 0;
                var total_amount_new = 0;
                var total_new = 0;
                var money_payment = 0;
                var total_new_submit = 0;

                if(res.error == false) {
                     interest_rate_text = formatNumber(Math.ceil(res.interest_rate_text).toFixed(2));
                     bonus_text = formatNumber(Math.ceil(res.bonus_text).toFixed(0));
                     total_amount_new = formatNumber(Math.ceil(res.total_amount_new).toFixed(0));
                     total_new = formatNumber(Math.ceil(res.total_new).toFixed(0));
                     total_new_submit = res.total_new;
                     money_payment = formatNumber(Math.ceil(res.money_payment).toFixed(0));
                }
                $('.interest_rate_text').html(interest_rate_text);
                $('.bonus_text').html(bonus_text);
                $('.total_amount_new').html(total_amount_new);
                $('.total_new').html(total_new);
                $('#total_new_submit').val(total_new_submit);
                $('.money_payment').html(money_payment);
            }
        });
    },

    createBill:function () {
        $.ajax({
            url: laroute.route('admin.manager-extend-contract.create-bill'),
            method: "POST",
            data: {},
            success: function (res) {
                $('.bill').empty();
                $('.bill').append(res);
                $('#modalMakeReceipt').modal('hide');
                $('#create-bill').modal('show');
            }
        });
    },

    printBill:function () {
        var form = $('#print-bill');
        // Validate ngày tạo cmnd nhỏ hơn hiện tại
        $.validator.addMethod("maxDate", function(value, element) {
            var curDate = moment(new Date()).format('DD-MM-YYYY');
            var inputDate = moment(value,'DD-MM-YYYY').format('DD-MM-YYYY');
            console.log(value,inputDate , curDate);
            if (inputDate < curDate){
                return true;
            } else {
                return false;
            }
        });

        // Validate số điện thoại
        $.validator.addMethod('validatePhone', function (value, element ) {
            return this.optional(element) || /0[0-9\s.-]{9,12}/.test(value);
        });

        // Validate giá tiền
        $.validator.addMethod("checkMoney", function (value, element) {
            var valueItem = value.replace(new RegExp('\\,', 'g'), '');
            if (valueItem < 0) {
                return false;
            }
            return true;
        });

        form.validate({
            rules: {
                investment_unit: {
                    required: true
                },
                user_name: {
                    required: true
                },
                cmnd: {
                    required: true,
                    digits: true,
                    maxlength: 12
                },
                created: {
                    required: true,
                    maxDate : true,
                },
                issued_by: {
                    required: true
                },
                address: {
                    required: true
                },
                phone: {
                    required: true,
                    validatePhone : true
                },
                reason: {
                    required: true
                },
                order_code: {
                    required: true
                },
                payment_amount: {
                    required: true,
                    checkMoney : true
                },
                payment_amount_text: {
                    required: true
                },
                name_collector: {
                    required: true
                },
                phone_collector: {
                    required: true,
                    validatePhone : true
                }
            },
            messages: {
                investment_unit: {
                    required: 'Yêu cầu nhập đơn vị thanh toán'
                },
                user_name: {
                    required: 'Yêu cầu nhập họ tên người nộp tiền'
                },
                cmnd: {
                    required: 'Yêu cầu nhập CMND',
                    digits: 'CMND sai định dạng',
                    maxlength: 'CMND vượt quá 12 số'
                },
                created: {
                    required: 'Yêu cầu nhập ngày cấp CMND',
                    maxDate : 'Ngày tạo CMND phải nhỏ hơn ngày hiện tại'
                },
                issued_by: {
                    required: 'Yêu cầu nhập nơi cấp CMND'
                },
                address: {
                    required: 'Yêu cầu nhập địa chỉ thường trú'
                },
                phone: {
                    required: 'Yêu cầu nhập số điện thoại',
                    validatePhone : 'Số điện thoại sai định dạng'
                },
                reason: {
                    required: 'Yêu cầu nhập lý do nộp tiền'
                },
                order_code: {
                    required: 'Yêu cầu nhập mã đơn hàng'
                },
                payment_amount: {
                    required: 'Yêu cầu nhập số tiền thanh toán',
                    checkMoney : 'Số tiền thanh toán sai định dạng'
                },
                payment_amount_text: {
                    required: 'Yêu cầu nhập số tiền thanh toán bằng chữ'
                },
                name_collector: {
                    required: 'Yêu cầu nhập họ tên người thu tiền'
                },
                phone_collector: {
                    required: 'Yêu cầu nhập số điện thoại người thu tiền',
                    validatePhone : 'Số điện thoại sai định dạng'
                }
            },
        });

        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                url: laroute.route('admin.manager-extend-contract.print-bill'),
                method: "POST",
                // data: $('#receipt-detail-add').serialize()+'&receipt_type='+$('#payment_method_type_select_box').val(),
                data: $('#print-bill').serialize(),
                success: function (res) {
                    // var divToPrint=document.getElementById('modalMakeReceipt');

                    var newWin = window.open('', 'Print-Window');

                    newWin.document.open();

                    newWin.document.write('<html><body onload="window.print()">' + res.view + '</body></html>');
                    newWin.document.close();

                }
            });
        }
    },
}

function dropzone(){
    Dropzone.options.dropzoneone = {
        paramName: 'file',
        maxFilesize: 500000, // MB
        maxFiles: 100,
        acceptedFiles: ".jpeg,.jpg,.png,",
        addRemoveLinks: true,
        // headers: {
        //     "X-CSRF-TOKEN": $('input[name=_token]').val()
        // },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('admin.manager-extend-contract.uploadDropzoneAction'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneone').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageShow').html();
                    tpl = tpl.replace(/{link}/g, response.file);
                    tpl = tpl.replace(/{link_hidden}/g, response.file);
                    tpl = tpl.replace(/{numberImage}/g, numberImage);
                    numberImage++;
                    $('#upload-image').append(tpl);
                }
            });
            this.on('removedfile', function (file,response) {
                var name = file.upload.filename;
                $.ajax({
                    url: laroute.route('admin.service.delete-image'),
                    method: "POST",
                    data: {

                        filename: name
                    },
                    success: function () {
                        $("input[class='file_Name']").each(function () {
                            var $this = $(this);
                            if ($this.val() === name) {
                                $this.remove();
                            }
                        });

                    }
                });
            });
        }
    }
}
function removeImage(e){
    $(e).closest('.image-show-child').remove();
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}