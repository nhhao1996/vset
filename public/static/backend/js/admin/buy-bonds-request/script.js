$('#autotable').PioTable({
    baseUrl: laroute.route('admin.buy-bonds-request.list')
});

var BuyBondsRequest = {
    clearAdd: function () {
        $('#amount').val('');
    },
    add: function (param) {
        var amount = $('#amount');
        var total = $('#total');
        var customer_id = $('#customer_id');
        var withdraw_request_id = $('#withdraw_request_id');
        var order_id = $('#order_id');
        var error = $('.error-name');
        var staff_id = $('#staff_id').val();
        swal({
            title: "Xác nhận đơn hàng",
            text: "Bạn có muốn xác nhận đơn hàng này?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Xác nhận",
            cancelButtonText: "Hủy",
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.buy-bonds-request.createReceipt'),
                    method: 'POST',
                    data: {
                        customer_id: customer_id.val(),
                        withdraw_request_id: withdraw_request_id.val(),
                        order_id: order_id.val(),
                        total: total.val(),
                        staff_id : staff_id
                    },
                    success: function (response) {
                        if (response.error == 1) {
                            error.text('Thêm phiếu thu thất bại');
                        } else if (response.error == true) {
                            swal.fire(response.message,'','error').then(function () {
                                location.reload();
                            });
                        } else {
                            swal(
                                'Xác nhận thành công',
                                '',
                                'success'
                            ).then(function () {
                                if (param == 0) {
                                    $('#modalAdd').modal('hide');
                                } else {
                                    amount.val('');
                                    error.text('');
                                }
                                location.reload();
                                $('#autotable').PioTable('refresh');
                            });

                        }

                    }
                });
            }
        });
    },
    //!add

    addWallet : function(){
        var amount = $('#amount');
        var total = $('#total');
        var customer_id = $('#customer_id');
        var withdraw_request_id = $('#withdraw_request_id');
        var order_id = $('#order_id');
        var payment_method_id = $('.payment_method_id');
        var staff_id = $('#staff_id');
        var error = $('.error-name');
        swal({
            title: "Xác nhận đơn hàng",
            text: "Bạn có muốn xác nhận đơn hàng này?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Xác nhận",
            cancelButtonText: "Hủy",
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.buy-bonds-request.createWallet'),
                    method: 'POST',
                    data: {
                        customer_id: customer_id.val(),
                        withdraw_request_id: withdraw_request_id.val(),
                        order_id: order_id.val(),
                        total: total.val(),
                        payment_method_id: payment_method_id.val(),
                        staff_id: staff_id.val(),
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal.fire(res.message,'','error').then(function () {
                                location.reload();
                            });
                        } else {
                            swal(
                                res.message,
                                '',
                                'success'
                            ).then(function () {
                                location.reload();
                            });

                        }

                    }
                });
            }
        });
    },

    remove: function (obj, id) {
        // hightlight row
        $(obj).closest('tr').addClass('m-table__row--danger');

        swal({
            title: "Thông báo",
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Xóa",
            cancelButtonText: "Hủy",
            onClose: function () {
                // remove hightlight row
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.buy-bonds-request.remove', {id: id}), function () {
                    swal(
                        "Xóa thành công",
                        '',
                        'success'
                    );
                    // window.location.reload();
                    $('#autotable').PioTable('refresh');
                });
            }
        });
    },
    dropzone:function () {
        Dropzone.options.dropzoneone = {
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            // headers: {
            //     "X-CSRF-TOKEN": $('input[name=_token]').val()
            // },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictRemoveFile: 'Xóa',
            dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
            dictInvalidFileType: 'Tệp không hợp lệ',
            dictCancelUpload: 'Hủy',
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                var random = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (let z = 0; z < 10; z++) {
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
            },
            init: function () {
                this.on("success", function (file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text', laroute.route('admin.buy-bonds-request.upload-dropzone'));

                    if (file.status === "success") {
                        //Xóa image trong dropzone
                        $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                            file.previewElement.remove();
                        });
                        $('#dropzoneone').removeClass('dz-started');
                        //Append vào div image
                        let tpl = $('#imageShow').html();
                        tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                        tpl = tpl.replace(/{link_hidden}/g, response);
                        $('#upload-image-cash').append(tpl);
                    }
                });
                this.on('removedfile', function (file,response) {
                    var name = file.upload.filename;
                    $.ajax({
                        url: laroute.route('admin.service.delete-image'),
                        method: "POST",
                        data: {

                            filename: name
                        },
                        success: function () {
                            $("input[class='file_Name']").each(function () {
                                var $this = $(this);
                                if ($this.val() === name) {
                                    $this.remove();
                                }
                            });

                        }
                    });
                });
            }
        };

        Dropzone.options.dropzoneonecash = {
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            // headers: {
            //     "X-CSRF-TOKEN": $('input[name=_token]').val()
            // },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictRemoveFile: 'Xóa',
            dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
            dictInvalidFileType: 'Tệp không hợp lệ',
            dictCancelUpload: 'Hủy',
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                var random = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (let z = 0; z < 10; z++) {
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
            },
            init: function () {
                this.on("success", function (file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text', laroute.route('admin.buy-bonds-request.upload-dropzone'));

                    if (file.status === "success") {
                        //Xóa image trong dropzone
                        $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                            file.previewElement.remove();
                        });
                        $('#dropzoneonecash').removeClass('dz-started');
                        //Append vào div image
                        let tpl = $('#imageShowCash').html();
                        tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                        tpl = tpl.replace(/{link_hidden}/g, response);
                        $('#upload-image-cash').append(tpl);
                    }
                });
                this.on('removedfile', function (file,response) {
                    var name = file.upload.filename;
                    $.ajax({
                        url: laroute.route('admin.service.delete-image'),
                        method: "POST",
                        data: {

                            filename: name
                        },
                        success: function () {
                            $("input[class='file_Name']").each(function () {
                                var $this = $(this);
                                if ($this.val() === name) {
                                    $this.remove();
                                }
                            });

                        }
                    });
                });
            }
        },

        Dropzone.options.dropzoneone = {
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            // headers: {
            //     "X-CSRF-TOKEN": $('input[name=_token]').val()
            // },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dictRemoveFile: 'Xóa',
            dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
            dictInvalidFileType: 'Tệp không hợp lệ',
            dictCancelUpload: 'Hủy',
            renameFile: function (file) {
                var dt = new Date();
                var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                var random = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                for (let z = 0; z < 10; z++) {
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
            },
            init: function () {
                this.on("success", function (file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text', laroute.route('admin.buy-bonds-request.upload-dropzone'));

                    if (file.status === "success") {
                        //Xóa image trong dropzone
                        $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                            file.previewElement.remove();
                        });
                        $('#dropzoneone').removeClass('dz-started');
                        //Append vào div image
                        let tpl = $('#imageShowCash').html();
                        tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                        tpl = tpl.replace(/{link_hidden}/g, response);
                        $('#upload-image').append(tpl);
                    }
                });
                this.on('removedfile', function (file,response) {
                    var name = file.upload.filename;
                    $.ajax({
                        url: laroute.route('admin.service.delete-image'),
                        method: "POST",
                        data: {

                            filename: name
                        },
                        success: function () {
                            $("input[class='file_Name']").each(function () {
                                var $this = $(this);
                                if ($this.val() === name) {
                                    $this.remove();
                                }
                            });

                        }
                    });
                });
            }
        }

    },
    remove_img: function (e) {
        $(e).closest('.image-show-child').remove();
    },

    getListReceiptDetail : function (page) {
        $('#page').val(page);
        $.ajax({
            url: laroute.route('admin.buy-bonds-request.get-list-receipt-detail'),
            method: "POST",
            data: {
                page: page,
                order_id : $('#order_id').val()
            },
            success: function (res) {
                $('.receipt-detail-list').empty();
                $('.receipt-detail-list').append(res.view);
            }
        });
    },

    addReceiptDetail : function () {
        var form = $('#receipt-detail-add');
        var check  = $('#payment_method_type_select_box').val();
        $.validator.addMethod("checkMoney", function (value, element) {
            var valueItem = value.replace(new RegExp('\\,', 'g'), '');
            if (valueItem < 0) {
                return false;
            }
            return true;
        });
        form.removeData('validator');
        if (check == 'cash') {
            form.validate({
                rules: {
                    amount: {
                        required: true,
                        checkMoney : true
                    },
                },
                messages: {
                    amount: {
                        required: 'Yêu cầu số tiền thanh toán',
                        checkMoney: 'Số tiền thanh toán phải lớn hơn hoặc bằng 0'
                    },
                },
                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                },
            });
        } else if (check == 'transfer') {
            form.validate({
                rules: {
                    amount_transfer: {
                        required: true,
                        checkMoney : true
                    },
                },
                messages: {
                    amount_transfer: {
                        required: 'Yêu cầu số tiền thanh toán',
                        checkMoney: 'Số tiền thanh toán phải lớn hơn hoặc bằng 0'
                    },
                },
                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                },
            });
        } else if (check == 'interest') {
            form.validate({
                rules: {
                    withdraw_request_group_id_interest: {
                        required: true,
                    },
                },
                messages: {
                    withdraw_request_group_id_interest: {
                        required: 'Yêu cầu chọn số tiền để thanh toán',
                    },
                },
                errorPlacement: function(error, element) {
                    error.insertBefore('.interest');
                },
            });
        } else if (check == 'bonus') {
            form.validate({
                rules: {
                    withdraw_request_group_id_bonus: {
                        required: true
                    },
                },
                messages: {
                    withdraw_request_group_id_bonus: {
                        required: 'Yêu cầu chọn số tiền để thanh toán'
                    },
                },
                errorPlacement: function(error, element) {
                    // error.insertBefore(element);
                    error.insertBefore('.bonus');
                },
            });
        }

        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                url: laroute.route('admin.buy-bonds-request.add-receipt-detail'),
                method: "POST",
                data: $('#receipt-detail-add').serialize()+'&receipt_type='+$('#payment_method_type_select_box').val()+'&staff_id='+$('#staff_id').val(),
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            // BuyBondsRequest.getListReceiptDetail(1);
                            // $('#modalMakeReceipt').modal('hide');
                            location.reload();
                        });
                    }
                }
            });
        }
    },

    showPopupReceiptDetail: function () {
        $.ajax({
            url: laroute.route('admin.buy-bonds-request.show-popup-receipt-detail'),
            method: "POST",
            data: {
                order_id : $('#order_id').val(),
                customer_id : $('#customer_id').val()
            },
            success: function (res) {
                console.log(res);
                $('#append-create-receipt').empty();
                $('#append-create-receipt').append(res.view);
                $(function() {
                    $('.'+$('#payment_method_type_select_box').val()).show();
                });
                var payment_method = $('.payment_method_id').val();
                if (payment_method == 1){
                    $('#payment_method_type_select_box').val('transfer').trigger('change');
                    $('.print-bill').addClass('d-none');
                } else if(payment_method == 2) {
                    $('#payment_method_type_select_box').val('interest').trigger('change');
                    $('.print-bill').addClass('d-none');
                } else if(payment_method == 3) {
                    $('#payment_method_type_select_box').val('bonus').trigger('change');
                    $('.print-bill').addClass('d-none');
                }

                $('<input type="radio">').prop('disabled',true);
                $('select').prop('disabled',true);

                new AutoNumeric.multiple('.name', {
                    currencySymbol: '',
                    decimalCharacter: '.',
                    digitGroupSeparator: ',',
                    decimalPlaces: 2
                });
                $('#dropzoneone').dropzone();
                $('#dropzoneonecash').dropzone();
                // BuyBondsRequest.dropzone;
                $('#modalMakeReceipt').modal('show');
                $('.noselector').hide();
                $(function() {
                    $('#payment_method_type_select_box').change(function(){
                        $('.noselector').hide();
                        $('.' + $(this).val()).show();
                    });
                });
                //check niếu chọn value nào thì show box đó lên
            }
        });
    },

    confirmFail : function (id) {
        // 1 : Tiền lãi không đủ để thanh toán
        // 2 : Chưa nhập đủ thông tin nhà đầu tư
        if (id == 1) {
            swal('Không đủ tiền để xác nhận yêu cầu mua gói đầu tư - tiết kiệm','','error');
        } else if(id == 2) {
            swal('Yêu cầu nhập đủ thông tin Số điện thoại, CMND, Địa chỉ của nhà đầu tư để có thể xác nhận yêu cầu mua gói đầu tư - tiết kiệm','','error');
        }
    },

    printBill:function () {
        var form = $('#print-bill');

        // Validate ngày tạo cmnd nhỏ hơn hiện tại
        $.validator.addMethod("maxDate", function(value, element) {
            var curDate = moment(new Date()).format('DD-MM-YYYY');
            var inputDate = moment(value,'DD-MM-YYYY').format('DD-MM-YYYY');
            if (inputDate <= curDate){
                return true;
            } else {
                return false;
            }
        });

        // Validate số điện thoại
        $.validator.addMethod('validatePhone', function (value, element ) {
            return this.optional(element) || /0[0-9\s.-]{9,12}/.test(value);
        });

        // Validate giá tiền
        $.validator.addMethod("checkMoney", function (value, element) {
            var valueItem = value.replace(new RegExp('\\,', 'g'), '');
            if (valueItem < 0) {
                return false;
            }
            return true;
        });

        form.validate({
            rules: {
                investment_unit: {
                    required: true
                },
                user_name: {
                    required: true
                },
                cmnd: {
                    required: true,
                    digits: true,
                    maxlength: 12
                },
                created: {
                    required: true,
                    // maxDate : true,
                },
                issued_by: {
                    required: true
                },
                address: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 12,
                    digits: true,
                    validatePhone: true
                },
                reason: {
                    required: true
                },
                order_code: {
                    required: true
                },
                payment_amount: {
                    required: true,
                    checkMoney : true
                },
                payment_amount_text: {
                    required: true
                },
                name_collector: {
                    required: true
                },
                phone_collector: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 12,
                    digits: true,
                    validatePhone: true
                }
            },
            messages: {
                investment_unit: {
                    required: 'Yêu cầu nhập đơn vị thanh toán'
                },
                user_name: {
                    required: 'Yêu cầu nhập họ tên người nộp tiền'
                },
                cmnd: {
                    required: 'Yêu cầu nhập CMND/ CCCD',
                    digits: 'CMND/ CCCD sai định dạng',
                    maxlength: 'CMND/ CCCD vượt quá 12 số'
                },
                created: {
                    required: 'Yêu cầu nhập ngày cấp CMND/ CCCD',
                    // maxDate : 'Ngày tạo CMND phải nhỏ hơn ngày hiện tại'
                },
                issued_by: {
                    required: 'Yêu cầu nhập nơi cấp CMND/ CCCD'
                },
                address: {
                    required: 'Yêu cầu nhập địa chỉ thường trú'
                },
                phone: {
                    required: 'Yêu cầu nhập số điện thoại',
                    number: 'Số điện thoại sai định dạng',
                    minlength: 'Số điện thoại tối thiểu 10 ký tự',
                    maxlength: 'Số điện thoại tối đa 12 ký tự',
                    digits: 'Số điện thoại sai định dạng',
                    validatePhone: 'Số điện thoại sai định dạng',
                },
                reason: {
                    required: 'Yêu cầu nhập lý do nộp tiền'
                },
                order_code: {
                    required: 'Yêu cầu nhập mã đơn hàng'
                },
                payment_amount: {
                    required: 'Yêu cầu nhập số tiền thanh toán',
                    checkMoney : 'Số tiền thanh toán sai định dạng'
                },
                payment_amount_text: {
                    required: 'Yêu cầu nhập số tiền thanh toán bằng chữ'
                },
                name_collector: {
                    required: 'Yêu cầu nhập họ tên người thu tiền'
                },
                phone_collector: {
                    required: 'Yêu cầu nhập số điện thoại',
                    number: 'Số điện thoại sai định dạng',
                    minlength: 'Số điện thoại tối thiểu 10 ký tự',
                    maxlength: 'Số điện thoại tối đa 12 ký tự',
                    digits: 'Số điện thoại sai định dạng',
                    validatePhone: 'Số điện thoại sai định dạng',
                }
            },
        });

        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                url: laroute.route('admin.buy-bonds-request.print-bill'),
                method: "POST",
                // data: $('#receipt-detail-add').serialize()+'&receipt_type='+$('#payment_method_type_select_box').val(),
                data: $('#print-bill').serialize(),
                success: function (res) {
                    // var divToPrint=document.getElementById('modalMakeReceipt');

                    var newWin = window.open('', 'Print-Window');

                    newWin.document.open();

                    newWin.document.write('<html><body onload="window.print()">' + res.view + '</body></html>');
                    newWin.document.close();
                }
            });
        }
    },
    createBill:function () {
        $.ajax({
            url: laroute.route('admin.buy-bonds-request.create-bill'),
            method: "POST",
            data: {},
            success: function (res) {
                $('.bill').empty();
                $('.bill').append(res);
                $('#modalMakeReceipt').modal('hide');
                $('#create-bill').modal('show');
            }
        });
    },

    cancelOrder:function () {
        swal.fire({
            title: 'Không xác nhận yêu cầu',
            type: 'question',

            html:
                '<input id="reason_vi" class="swal2-input" placeholder="Nhập lý do không xác nhận (VI)">' +
                '<input id="reason_en" class="swal2-input" placeholder="Nhập lý do không xác nhận (EN)">',
            preConfirm: () => {
                if($('#reason_vi').val() == ''){
                    swal.showValidationError("Yêu cầu nhập lý do không xác nhận (VI)");
                } else if($('#reason_en').val() == ''){
                    swal.showValidationError("Yêu cầu nhập lý do không xác nhận (EN)");
                } else if($('#reason_vi').val().length > 255){
                    swal.showValidationError("Lý do không xác nhận (VI) vượt quá 255 ký tự");
                } else if($('#reason_en').val().length > 255){
                    swal.showValidationError("Lý do không xác nhận (EN) vượt quá 255 ký tự");
                }
            }
        }).then(function (result) {
            if (result.value) {
                var staff_id = $('#staff_id').val()
                $.ajax({
                    url: laroute.route('admin.buy-bonds-request.cancel-order'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        order_id : $('#order_id').val(),
                        reason_vi: $('#reason_vi').val(),
                        reason_en: $('#reason_en').val(),
                        staff_id : staff_id
                    },
                    success: function (res) {
                        if (res.error == true) {
                            swal(res.message,'','error').then(function () {
                                location.reload();
                            });
                        } else {
                            swal(res.message,'','success').then(function () {
                                location.reload();
                            });
                        }
                    },
                });
            }
        })
    }
}

$(document).ready(function () {
    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });

})