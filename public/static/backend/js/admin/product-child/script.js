var productChild = {
    init: function () {
        $(".daterange-picker").daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            buttonClasses: "m-btn btn",
            applyClass: "btn-primary",
            cancelClass: "btn-danger",
            maxDate: moment().endOf("day"),
            startDate: moment().startOf("day"),
            endDate: moment().add(1, 'days'),
            locale: {
                format: 'DD/MM/YYYY',
                "applyLabel": "Đồng ý",
                "cancelLabel": "Thoát",
                "customRangeLabel": "Tùy chọn ngày",
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                ],
                "monthNames": [
                    "Tháng 1 năm",
                    "Tháng 2 năm",
                    "Tháng 3 năm",
                    "Tháng 4 năm",
                    "Tháng 5 năm",
                    "Tháng 6 năm",
                    "Tháng 7 năm",
                    "Tháng 8 năm",
                    "Tháng 9 năm",
                    "Tháng 10 năm",
                    "Tháng 11 năm",
                    "Tháng 12 năm"
                ],
                "firstDay": 1
            },
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "7 ngày trước": [moment().subtract(6, "days"), moment()],
                "30 ngày trước": [moment().subtract(29, "days"), moment()],
                "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
                "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
        });
        productChild.tab('new');
        productChild.tab('sale');
        productChild.tab('best_seller');
        $('#autotable_new').PioTable({
            baseUrl: laroute.route('admin.product-child.list-tab')
        });
        $('#autotable_sale').PioTable({
            baseUrl: laroute.route('admin.product-child.list-tab')
        });
        $('#autotable_best_seller').PioTable({
            baseUrl: laroute.route('admin.product-child.list-tab')
        });
        $(document).on('keyup', ".input-percent-sale", function () {
            var n = parseInt($(this).val().replace(/\D/g, ''), 10);
            if (typeof n == 'number' && Number.isInteger(n)) {
                if (n > 100) {
                    $(this).val(0);
                }
            } else {
                $(this).val(0);
            }
            $(this).val(parseInt($(this).val()));
        });

    },
    notEnterInput: function (thi) {
        $(thi).val('');
    },
    tab: function (type_tab) {
        $.ajax({
            url: laroute.route('admin.product-child.list-tab'),
            method: "POST",
            async: false,
            data: {type_tab: type_tab},
            success: function (res) {
                if (type_tab === 'new'){
                    $('.table-content-new').empty();
                    $('.table-content-new').prepend(res);
                } else if (type_tab === 'sale') {
                    $('.table-content-sale').empty();
                    $('.table-content-sale').prepend(res);
                } else if (type_tab === 'best_seller') {
                    $('.table-content-best-seller').empty();
                    $('.table-content-best-seller').prepend(res);
                }
            }
        });
    },
    popAdd: function (type_tab) {
        $.ajax({
            url: laroute.route('admin.product-child.get-option-add-tab'),
            method: "POST",
            data: {type_tab: type_tab},
            success: function (res) {
                $('#append-popup').empty();
                $('#append-popup').prepend(res);
                $('#modal_add').modal('show');
                $('.ss--select-2').select2();
            }
        });
    },
    selectedProductChild: function (type_tab, th) {
        $.ajax({
            url: laroute.route('admin.product-child.selected-product-child'),
            method: "POST",
            data: {
                id: $(th).val(),
                type_tab: type_tab
            },
            success: function (res) {
                var flag = true;
                $.each($('.product_child_id'), function () {
                    if ($(this).val() == res.product_child_id) {
                        flag = false;
                        return false;
                    }
                });
                if (res.product_child_id !== undefined && flag === true) {
                    if (type_tab != 'sale') {
                        let tpl = $('#product-childs').html();
                        tpl = tpl.replace(/{product_child_id}/g, res.product_child_id);
                        tpl = tpl.replace(/{product_child_name}/g, res.product_child_name);
                        tpl = tpl.replace(/{price}/g, productChild.formatNumber(res.price));
                        tpl = tpl.replace(/{unit}/g, res.unit_name);
                        tpl = tpl.replace(/{cost}/g, productChild.formatNumber(res.cost));
                        $('.tbody-table-product').append(tpl);
                    } else {
                        let tpl = $('#product-childs-sale').html();
                        tpl = tpl.replace(/{product_child_id}/g, res.product_child_id);
                        tpl = tpl.replace(/{product_child_name}/g, res.product_child_name);
                        tpl = tpl.replace(/{price}/g, productChild.formatNumber(res.price));
                        tpl = tpl.replace(/{unit}/g, res.unit_name);
                        tpl = tpl.replace(/{cost}/g, productChild.formatNumber(res.cost));
                        $('.tbody-table-product').append(tpl);
                    }
                }
                $('.input-percent-sale').mask('000', {reverse: true});
                productChild.resetStt();
            }
        });
    },
    removeTr: function (o) {
        $(o).closest('tr').remove();
        let table = $('#table-product > tbody tr').length;
        let a = 1;
        $.each($('.stt'), function () {
            $(this).text(a++);
        });
    },
    resetStt: function () {
        var stt = 1;
        $.each($('.stt'), function () {
            $(this).text(stt++);
        });
    },
    submitAdd: function (type_tab) {
        $.ajax({
            url: laroute.route('admin.product-child.submit-add-product-child'),
            method: "POST",
            data: {
                type_tab: type_tab,
                productChildId: productChild.eachGetProductChildId(type_tab)
            },
            success: function (res) {

                if (res.error === false) {
                    swal(
                        res.message,
                        '',
                        'success'
                    );
                    productChild.tab(type_tab);
                    $('#modal_add').modal('hide');
                } else {
                    swal(
                        'Thêm thất bại',
                        '',
                        'error'
                    );
                }
            }
        });
    },
    eachGetProductChildId: function (type_tab) {
        var result = [];
        if (type_tab != 'sale') {
            $.each($('.product_child_id'), function () {
                result.push($(this).val());
            });
        } else {
            $.each($('.product_child_id'), function () {
                let id = $(this).val();
                let percentSale = $(this).closest('tr').find('.input-percent-sale').val();
                var temp = {id: id, percentSale: percentSale};
                result.push(temp);
            });
        }
        return result;
    },
    tabCurrent: function (type_tab) {
        $.ajax({
            url: laroute.route('admin.product-child.tab-current'),
            method: "POST",
            data: {
                type_tab: type_tab,
            },
            success: function (res) {
            }
        });
    },
    removeList: function (obj, type_tab, product_child_id) {
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route('admin.product-child.remove-list'),
                    method: "POST",
                    data: {
                        type_tab: type_tab,
                        product_child_id: product_child_id,
                    },
                    success: function (res) {
                        if (res.error === false) {
                            swal(
                                res.message,
                                '',
                                'success'
                            );
                            productChild.tab(type_tab);
                        } else {
                            swal(
                                'Xóa không thành công',
                                '',
                                'error'
                            );
                        }
                    }
                });
            }
        });
    },
    formatNumber: function (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }
};
productChild.init();
