function refresh() {
    $('#branch').val('').trigger('change');
    $('#status').val('').trigger('change');
    $('#staff').val('').trigger('change');
    $('input[name=search_keyword]').val('');
    $('#time').val('');
    filter();
}

$('#branch').select2().on('select2:select', function () {
    filter();
});
$('#status').select2().on('select2:select', function () {
    filter();
});
$('#staff').select2().on('select2:select', function () {
    filter();
});
$('input[name=search_keyword]').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});
$('input[name=search_keyword]').bind("enterKey", function (e) {
    filter();
});
// Chọn ngày.
$('#time').on('apply.daterangepicker', function (ev, picker) {
    var start = picker.startDate.format("DD/MM/YYYY");
    var end = picker.endDate.format("DD/MM/YYYY");
    $(this).val(start + " - " + end);
    filter();
});
$.getJSON(laroute.route('translate'), function (json) {
    var arrRange = {};
    arrRange[json["Hôm nay"]] = [moment(), moment()];
    arrRange[json["Hôm qua"]] = [moment().subtract(1, "days"), moment().subtract(1, "days")];
    arrRange[json["7 ngày trước"]] = [moment().subtract(6, "days"), moment()];
    arrRange[json["30 ngày trước"]] = [moment().subtract(29, "days"), moment()];
    arrRange[json["Trong tháng"]] = [moment().startOf("month"), moment().endOf("month")];
    arrRange[json["Tháng trước"]] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")];
    $("#time").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",

        maxDate: moment().endOf("day"),
        startDate: moment().startOf("day"),
        endDate: moment().add(1, 'days'),
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": json["Đồng ý"],
            "cancelLabel": json["Thoát"],
            "customRangeLabel": json["Tùy chọn ngày"],
            daysOfWeek: [
                json["CN"],
                json["T2"],
                json["T3"],
                json["T4"],
                json["T5"],
                json["T6"],
                json["T7"]
            ],
            "monthNames": [
                json["Tháng 1 năm"],
                json["Tháng 2 năm"],
                json["Tháng 3 năm"],
                json["Tháng 4 năm"],
                json["Tháng 5 năm"],
                json["Tháng 6 năm"],
                json["Tháng 7 năm"],
                json["Tháng 8 năm"],
                json["Tháng 9 năm"],
                json["Tháng 10 năm"],
                json["Tháng 11 năm"],
                json["Tháng 12 năm"]
            ],
            "firstDay": 1
        },
        ranges: arrRange
    }).on('apply.daterangepicker', function (ev) {
    });
});

function filter() {
    let keyWord = $('input[name=search_keyword]').val();
    let status = $('#status').val();
    let branch = $('#branch').val();
    let staff = $('#staff').val();
    let time = $('#time').val();

    $.ajax({
        url: laroute.route('admin.service-card.sold.filter'),
        method: "POST",
        data: {
            cardType: 'service',
            keyWord: keyWord,
            status: 1,
            branch: branch,
            staff: staff,
            time: time
        },
        success: function (data) {
            $('.list-card').empty();
            $('.list-card').append(data);
        }
    })
}

// $('#autotable').PioTable({
//     baseUrl: laroute.route('admin.service-card.sold.detail-paginate')
// });

function pageClickDetailCardSold(val) {
    $.ajax({
        url: laroute.route('admin.service-card.sold.detail-paginate'),
        method: "POST",
        data: {
            page: val,
            code: $('#code').val()
        },
        success: function (data) {
            $('.list-history').empty();
            $('.list-history').append(data);
        }
    });
}

function pageClick(page) {
    $.ajax({
        url: laroute.route('admin.service-card.sold.paginate'),
        method: "POST",
        data: {
            page: page,
            cardType: 'service'
        },
        success: function (data) {
            $('.list-card').empty();
            $('.list-card').append(data);

        }
    });
}

function pageClickFilter(page) {
    let keyWord = $('input[name=search_keyword]').val();
    let status = $('#status').val();
    let branch = $('#branch').val();
    let staff = $('#staff').val();
    let time = $('#time').val();

    $.ajax({
        url: laroute.route('admin.service-card.sold.paging-search'),
        method: "POST",
        data: {
            cardType: 'service',
            keyWord: keyWord,
            status: 1,
            branch: branch,
            staff: staff,
            time: time,
            page: page
        },
        success: function (data) {
            $('.list-card').empty();
            $('.list-card').append(data);
        }
    });
}
function notEnterInput(thi) {
    $(thi).val('');
}
$('.cancelBtn').removeClass('btn-danger');
$('.cancelBtn').addClass('btn-metal ss--btn');

$('.applyBtn').removeClass('btn-primary');
$('.applyBtn').addClass('ss--button-cms-piospa ss--btn');

if ($('tbody tr').length == 0) {
    $('.null-data').text('0');
}
$(".date-picker-expire").datepicker({
    format: "dd/mm/yyyy",
    startDate: '+1d',
    language: 'vi',
});
$("#number-using-not-limit").click(function () {
    $.getJSON(laroute.route('translate'), function (json) {
        let value = $(this).prop('checked');
        if (value) {
            $('#number_using').attr('disabled', 'disabled');
            $('#expired_date').attr('disabled', 'disabled');
            $('#count_using').attr('disabled', 'disabled');
            $('#minus_using').val(json['Không giới hạn']);
        } else {
            $('#number_using').removeAttr('disabled');
            $('#expired_date').removeAttr('disabled');
            $('#count_using').removeAttr('disabled');
            $('#minus_using').val($('#number_using').val() - $('#count_using').val());
        }
    });
});
$(document).on('keyup', '#number_using', function () {
    $.getJSON(laroute.route('translate'), function (json) {
        var checkNumb = $.isNumeric($('#number_using').val());
        var checkCount = $.isNumeric($('#count_using').val());
        if (checkNumb && checkCount) {
            let result = $(this).val() - $('#count_using').val();
            if (result > -1) {
                $('#count_using_div').next('span').remove();
                $('#minus_using').val(result);
            } else {
                $('#count_using_div').next('span').remove();
                $('#count_using_div').after('<span class="form-control-feedback text-danger">' + json['Số lần sử dụng phải ít hơn số thẻ dịch vụ'] + '</span>');
                $('#minus_using').val('0');
            }
        }
    });
});
$(document).on('keyup', '#count_using', function () {
    $.getJSON(laroute.route('translate'), function (json) {
        var checkNumb = $.isNumeric($('#number_using').val());
        var checkCount = $.isNumeric($('#count_using').val());
        if (checkNumb && checkCount) {
            let result = $('#number_using').val() - $(this).val();
            if (result > -1) {
                $('#count_using_div').next('span').remove();
                $('#minus_using').val(result);
            } else {
                $('#count_using_div').next('span').remove();
                $('#count_using_div').after('<span class="form-control-feedback text-danger">' + json['Số lần sử dụng phải ít hơn số thẻ dịch vụ'] + '</span>');
                $('#minus_using').val('0');
            }
        }
    });
});
var serviceCard = {
    save: function () {
        $.getJSON(laroute.route('translate'), function (json) {
            var form = $('#form-submit');
            form.validate({
                rules: {
                    number_using: {
                        number: true,
                        min: 1,
                        required: true
                    },
                    count_using: {
                        number: true,
                        min: 0,
                        required: true
                    },
                    note: {
                        required: true
                    },
                    expired_date: {
                        required: true
                    }
                },
                messages: {
                    number_using: {
                        number: json['Vui lòng chỉ nhập số'],
                        min: json['Vui lòng chỉ nhập số và tối thiểu là 1'],
                        required: json['Vui lòng nhập số lần sử dụng']
                    },
                    count_using: {
                        number: json['Vui lòng chỉ nhập số'],
                        min: json['Vui lòng chỉ nhập số và tối thiểu là 0'],
                        required: json['Vui lòng nhập số lần đã sử dụng']
                    },
                    note: {
                        required: json['Vui lòng nhập ghi chú']
                    },
                    expired_date: {
                        required: json['Vui lòng chọn hạn sử dụng']
                    }
                }
            });
            if (!form.valid()) {
                return;
            }

            form.submit();
        });

    }
};
