var sttBranch = 0;

$(document).ready(function () {
    // $.getJSON(laroute.route('translate'), function (json) {
        // $(".quantity").TouchSpin({
        //     initval: 1,
        //     min: 1,
        //     buttondown_class: "btn btn-default down btn-ct",
        //     buttonup_class: "btn btn-default up btn-ct"
        //
        // });

        $('.summernote').summernote({
            height: 150,
            placeholder: 'Nhập mô tả chi tiết...',
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });
        $('.note-btn').attr('title', '');

        $('#select2').select2({
            placeholder: 'Chọn đơn vị tính'
        });
        $('#branch_id').select2({
            placeholder: "Chọn chi nhánh",
        });

        $('#product_id').select2({
            ajax: {
                url: laroute.route('admin.search.product'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {

                    var query = {
                        search: params.term,
                        page: params.page || 1
                    };
                    return query;
                }
            },
            placeholder: 'Chọn sản phẩm sử dụng',
            minimumInputLength: 1,
        });

        $('#service_category_id').select2({
            placeholder: "Chọn nhóm dịch vụ",
        });

        $('#price_standard').change(function () {
            $('.old_price').empty();
            $('.old_price ').append($(this).val());
            $('.old_price ').append('<input type="hidden"  id="old_tb" name="old_tb" value=' + $(this).val() + '>');
        });
        $('#new_price').change(function () {
            $('.new_price').empty();
            $('.new_price ').append('<input  class="form-control new btn-sm" style="text-align: right;" id="new_tb" name="new_tb" value=' + $(this).val().replace(new RegExp('\\,', 'g'), '') + '>');
        });
        $('#checkAll').click(function () {
            $('#table_branch > tbody').empty();
            //Check vào all sẽ disable select 2
            if ($('input[name="checkAll"]').is(':checked')) {
                $('select[name="branch_id[]"]').prop("disabled", true);
            } else {
                $('select[name="branch_id[]"]').prop("disabled", false);
            }
            if ($('#checkAll').is(':checked')) {
                $('#branch_id > option').prop("selected", "selected");
                $('#branch_id').trigger("change");
                $('select[name="branch_id"] > option').each(function () {
                    sttBranch ++;
                    var old = $('#price_standard').val();
                    var new_p = $('#new_price').val();
                    var loc_old = old.replace(new RegExp('\\,', 'g'), '');
                    var loc_new = new_p.replace(new RegExp('\\,', 'g'), '');

                    // var stts = $('#table_branch tr').length;
                    var tpl = $('#branch-tpl').html();
                    tpl = tpl.replace(/{stt}/g, sttBranch);
                    tpl = tpl.replace(/{branch_name}/g, $(this).text());
                    tpl = tpl.replace(/{branch_id}/g, $(this).val());
                    tpl = tpl.replace(/{old_price}/g, old);
                    tpl = tpl.replace(/{new_price}/g, loc_new);
                    $('#table_branch > tbody').append(tpl);

                    new AutoNumeric.multiple('.new_' + sttBranch + '', {
                        currencySymbol: '',
                        decimalCharacter: '.',
                        digitGroupSeparator: ',',
                        decimalPlaces: decimal_number
                    });

                    $('.remove_branch').click(function () {
                        $(this).closest('.branch_tb').remove();
                    });

                });
            } else {
                $('#table_branch > tbody').empty();
                $('select[name="branch_id"] > option').removeAttr('selected');
                $('select[name="branch_id"]').val(null).trigger('change');

            }
        });
        $('#branch_id').on('select2:select', function (event) {
            sttBranch ++;
            // var stts = $('#table_branch tr').length;
            var tpl = $('#branch-tpl').html();
            tpl = tpl.replace(/{stt}/g, sttBranch);
            tpl = tpl.replace(/{branch_name}/g, event.params.data.text);
            tpl = tpl.replace(/{branch_id}/g, event.params.data.id);
            tpl = tpl.replace(/{old_price}/g, $('#price_standard').val());
            tpl = tpl.replace(/{new_price}/g, $('#new_price').val());
            $('#table_branch > tbody').append(tpl);

            new AutoNumeric.multiple('.new_' + sttBranch + '', {
                currencySymbol: '',
                decimalCharacter: '.',
                digitGroupSeparator: ',',
                decimalPlaces: decimal_number
            });

            $('.remove_branch').click(function () {
                $(this).closest('.branch_tb').remove();
            });

        });
        $('#branch_id').on('select2:unselect', function (event) {
            $('.branch_tb').remove(":contains(" + event.params.data.text + ")");
        });
        $('#check_all_branch').click(function () {
            $('.check:checkbox').prop('checked', this.checked);

        });
        if ($('input[name="check_product"]').is(':checked')) {
            $('select[name="product_id[]"]').prop("disabled", false);
        } else {
            $('select[name="product_id[]"]').prop("disabled", true);
        }
        $('#check_product').click(function () {
            if ($('input[name="check_product"]').is(':checked')) {
                $('select[name="product_id[]"]').prop("disabled", false);
            } else {
                $('select[name="product_id[]"]').prop("disabled", true);
            }
        });

        $('#product_id').on('select2:select', function (event) {
            // console.log(event.params.data.id);
            $(this).val('').trigger('change')
            var check = true;
            $.each($('#table_product tbody tr'), function () {
                let codeHidden = $(this).find("input[name='product_hidden']");
                let value_id = codeHidden.val();
                let code = event.params.data.id;
                if (value_id == code) {
                    check = false;
                    let quantitySv = codeHidden.parents('tr').find('input[name="quantity"]').val();
                    let numbers = parseInt(quantitySv) + 1;
                    codeHidden.parents('tr').find('input[name="quantity"]').val(numbers);
                    // codeHidden.parents('tr').find('.quantity').empty();
                    //codeHidden.parents('tr').find('.discount-tr-'+type_check+'-'+code+'').append('<a class="abc m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary" href="javascript:void(0)" onclick="order.modal_discount('+amount+','+code+','+id_type+')"><i class="la la-plus"></i></a>');
                }
            });
            if (check == true) {
                var stts = $('#table_product tr').length;
                var tpl = $('#product-tpl').html();
                tpl = tpl.replace(/{stt}/g, stts);
                tpl = tpl.replace(/{product_name}/g, event.params.data.text);
                tpl = tpl.replace(/{product_id}/g, event.params.data.id);
                tpl = tpl.replace(/{id_unit}/g, event.params.data.id);
                $("#table_product > tbody").append(tpl);

                var id = $(this).val();

                $.ajax({
                    url: laroute.route('admin.service.getUnit'),
                    method: "POST",
                    data: {id: id},
                    dataType: "JSON",
                    success: function (data) {
                        // $('.unit').empty();
                        $.each(data, function (index, element) {
                            $('.unit').append('<option></option>');
                            $('.unit').append('<option value="' + index + '">' + element + '</option>');
                        });
                    }
                });
                $(".in_quantity").TouchSpin({
                    initval: 1,
                    min: 1,
                    buttondown_class: "btn btn-default down btn-ct",
                    buttonup_class: "btn btn-default up btn-ct"
                });
                $.getJSON(laroute.route('translate'), function (json) {
                    $('.unit').select2({
                        placeholder:'Chọn đơn vị tính'
                    });
                });
            }


            $('.remove_product').click(function () {
                $(this).closest('.pro_tb').remove();
            });
        });
        $('#product_id').on('select2:unselect', function (event) {
            $('.pro_tb').remove(":contains(" + event.params.data.text + ")");
        });
        if ($('input[name="is_actived"]').is(':checked')) {
            $('#h_is_actived').val(1);
        } else {
            $('#h_is_actived').val(0);
        }
        $('#h_is_actived').click(function () {
            if ($('input[name="is_actived"]').is(':checked')) {
                $('#h_is_actived').val(1);
            } else {
                $('#h_is_actived').val(0);
            }
        });


        $('.btn3').click(function () {
            $.getJSON(laroute.route('translate'), function (json) {
                var form = $('#formAdd');

                form.validate({
                    rules: {
                        group_name: {
                            required: true,
                        }
                    },
                    messages: {
                        group_name: {
                            required: "Hãy nhập tên nhóm khách hàng",
                        }
                    }, rules: {
                        service_category_id: {
                            required: true
                        },
                        service_name: {
                            required: true
                        },
                        service_code: {
                            required: true
                        },
                        price_standard: {
                            required: true
                        },
                        time: {
                            required: true
                        },
                        branch_id: {
                            required: true
                        },
                        description1: {
                            maxlength: 250
                        }
                    },
                    messages: {
                        service_category_id: {
                            required: 'Hãy chọn nhóm dịch vụ'
                        },
                        service_name: {
                            required: 'Hãy nhập tên dịch vụ'
                        },
                        service_code: {
                            required: 'Hãy nhập mã dịch vụ'
                        },
                        price_standard: {
                            required: 'Hãy nhập giá dịch vụ'
                        },
                        time: {
                            required: 'Hãy nhập thời gian sử dụng'
                        },
                        branch_id: {
                            required: "Hãy chọn chi nhánh"
                        },
                        description1: {
                            maxlength: 'Mô tả ngắn tối đa 250 kí tự'
                        }
                    },
                });

                if (!form.valid()) {
                    return false;
                }

                $('.black-title').css('color', 'black');
                var continues = true;
                var standard = $('#price_standard').val();
                var loc = standard.replace(new RegExp('\\,', 'g'), '');
                var active = $('#h_is_actived').val();
                // $('.new_hid').val($('.new').val().replace(/\D+/g, ''));
                var branch_table = [];
                $.each($('#table_branch').find(".branch_tb"), function () {
                    var $check = $(this).find("td input.new");
                    var $tds = $(this).find("td input");
                    if ($check.val() == "") {
                        $check.parents('td').find('.error_new_price').text('Hãy nhập giá chi nhánh');
                        continues = false;
                    }
                    $.each($tds, function () {
                        branch_table.push($(this).val());
                    });

                });
                var product_table = [];
                $.each($('#table_product tr input[name="product_hidden"]').parentsUntil("tbody"), function () {
                    var $tds = $(this).find("td input,td select");
                    var $check_quantity = $(this).find("td input.in_quantity ");
                    if ($check_quantity.val() == "") {
                        $check_quantity.parents('td').find('.error_quantity').text('Hãy nhập số lượng sản phẩm');
                        continues = false;
                    }
                    $.each($tds, function () {
                        product_table.push($(this).val());
                    });
                });
                var check_image = $('.image-show').find('input[name="img-sv"]');
                var img = [];
                $.each(check_image, function () {
                    img.push($(this).val());
                });
                var service_avatar = $('#service_avatar').val();
                if (continues == true) {
                    $.ajax({
                        url: laroute.route('admin.service.submitAdd'),
                        data: {
                            service_category_id: $('#service_category_id').val(),
                            service_name: $('#service_name').val(),
                            service_code: $('#service_code').val(),
                            time: $('#time').val(),
                            price_standard: loc,
                            description: $('#description1').val(),
                            detail_description: $('.summernote').summernote('code'),
                            branch_id: $('#branch_hidden').val(),
                            product_id: $('#product_hidden').val(),
                            is_actived: active,
                            branch_tb: branch_table,
                            product_tb: product_table,
                            image: img,
                            service_avatar: service_avatar,
                            type_refer_commission: $('.refer').find('.active input[name="type_refer_commission"]').val(),
                            refer_commission_value: $('#refer_commission_value').val().replace(new RegExp('\\,', 'g'), ''),
                            type_staff_commission: $('.staff').find('.active input[name="type_staff_commission"]').val(),
                            staff_commission_value: $('#staff_commission_value').val().replace(new RegExp('\\,', 'g'), '')
                        },
                        method: 'POST',
                        dataType: "JSON",
                        success: function (response) {
                            if (response.status == 1) {
                                $('#formAdd')[0].reset();
                                swal("Thêm dịch vụ thành công", "", "success");
                                window.location = laroute.route('admin.service');
                            }
                            if (response.status == 0) {
                                $('.error_service_name').text('Tên dịch vụ đã tồn tại');
                                $('.error_service_name').css('color', 'red');
                            }
                            if (response.error_refer_commission == 1) {
                                swal(response.message, "", "error");
                            }
                            if (response.error_staff_commission == 1) {
                                swal(response.message, "", "error");
                            }
                            if (response.branch_null == 1) {
                                $('.error_branch_tb').text('Vui lòng chọn chi nhánh');
                            } else {
                                $('.error_branch_tb').text('');
                            }

                        }
                    })
                }
            });
        });
        $('.btn_new').click(function () {
            $.getJSON(laroute.route('translate'), function (json) {
                $('#formAdd').validate({
                    rules: {
                        service_category_id: {
                            required: true
                        },
                        service_name: {
                            required: true
                        },
                        service_code: {
                            required: true
                        },
                        price_standard: {
                            required: true
                        },
                        time: {
                            required: true
                        },
                        branch_id: {
                            required: true
                        },
                        description1: {
                            maxlength: 250
                        }
                    },
                    messages: {
                        service_category_id: {
                            required: 'Hãy chọn nhóm dịch vụ'
                        },
                        service_name: {
                            required: 'Hãy nhập tên dịch vụ'
                        },
                        service_code: {
                            required: 'Hãy nhập mã dịch vụ'
                        },
                        price_standard: {
                            required: 'Hãy nhập giá dịch vụ'
                        },
                        time: {
                            required: 'Hãy nhập thời gian sử dụng'
                        },
                        branch_id: {
                            required: "Hãy chọn chi nhánh"
                        },
                        description1: {
                            maxlength: 'Mô tả ngắn tối đa 250 kí tự'
                        }
                    },
                    submitHandler: function () {

                        $('.black-title').css('color', 'black');
                        var continues = true;
                        var standard = $('#price_standard').val();
                        var loc = standard.replace(new RegExp('\\,', 'g'), '');
                        var active = $('#h_is_actived').val();
                        // $('.new_hid').val($('.new').val().replace(/\D+/g, ''));
                        var branch_table = [];
                        $.each($('#table_branch').find(".branch_tb"), function () {
                            var $check = $(this).find("td input.new");
                            var $tds = $(this).find("td input");
                            if ($check.val() == "") {
                                $check.parents('td').find('.error_new_price').text('Hãy nhập giá chi nhánh');
                                continues = false;
                            }
                            $.each($tds, function () {
                                branch_table.push($(this).val());
                            });

                        });
                        var product_table = [];
                        $.each($('#table_product tr input[name="product_hidden"]').parentsUntil("tbody"), function () {
                            var $tds = $(this).find("td input,td select");
                            var $check_quantity = $(this).find("td input.in_quantity ");
                            if ($check_quantity.val() == "") {
                                $check_quantity.parents('td').find('.error_quantity').text('Hãy nhập số lượng sản phẩm');
                                continues = false;
                            }
                            $.each($tds, function () {
                                product_table.push($(this).val());
                            });
                        });
                        var check_image = $('.image-show').find('input[name="img-sv"]');
                        var img = [];
                        $.each(check_image, function () {
                            img.push($(this).val());
                        });
                        var service_avatar = $('#service_avatar').val();
                        if (continues == true) {
                            $.ajax({
                                url: laroute.route('admin.service.submitAdd'),
                                data: {
                                    service_category_id: $('#service_category_id').val(),
                                    service_name: $('#service_name').val(),
                                    service_code: $('#service_code').val(),
                                    time: $('#time').val(),
                                    price_standard: loc,
                                    description: $('#description1').val(),
                                    detail_description: $('.summernote').summernote('code'),
                                    branch_id: $('#branch_hidden').val(),
                                    product_id: $('#product_hidden').val(),
                                    is_actived: active,
                                    branch_tb: branch_table,
                                    product_tb: product_table,
                                    image: img,
                                    service_avatar: service_avatar,
                                    type_refer_commission: $('.refer').find('.active input[name="type_refer_commission"]').val(),
                                    refer_commission_value: $('#refer_commission_value').val().replace(new RegExp('\\,', 'g'), ''),
                                    type_staff_commission: $('.staff').find('.active input[name="type_staff_commission"]').val(),
                                    staff_commission_value: $('#staff_commission_value').val().replace(new RegExp('\\,', 'g'), '')
                                },
                                method: 'POST',
                                dataType: "JSON",
                                success: function (response) {
                                    if (response.status == 1) {
                                        $('#formAdd')[0].reset();
                                        swal("Thêm dịch vụ thành công", "", "success");
                                        window.location.reload();
                                    }
                                    if (response.status == 0) {
                                        $('.error_service_name').text('Tên dịch vụ đã tồn tại');
                                        $('.error_service_name').css('color', 'red');
                                    }
                                    if (response.error_refer_commission == 1) {
                                        swal(response.message, "", "error");
                                    }
                                    if (response.error_staff_commission == 1) {
                                        swal(response.message, "", "error");
                                    }
                                    if (response.branch_null == 1) {
                                        $('.error_branch_tb').text('Vui lòng chọn chi nhánh');
                                    } else {
                                        $('.error_branch_tb').text('');
                                    }
                                }
                            })
                        }
                    }
                });
            });
        });
    // });
});
var service = {
    remove: function (obj, id) {
        // $.getJSON(laroute.route('translate'), function (json) {
            // hightlight row
            $(obj).closest('tr').addClass('m-table__row--danger');

            swal({
                title: 'Thông báo',
                text: "Bạn có muốn xóa không?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Xóa',
                cancelButtonText: 'Hủy',
                onClose: function () {
                    // remove hightlight row
                    $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    $.post(laroute.route('admin.service.remove', {id: id}), function () {
                        swal(
                            'Xóa thành công',
                            '',
                            'success'
                        );
                        // window.location.reload();
                        $('#autotable').PioTable('refresh');
                    });
                }
            });
        // });
    },
    changeStatus: function (obj, id, action) {
        $.ajax({
            url: laroute.route('admin.service.change-status'),
            method: "POST",
            data: {
                id: id, action: action
            },
            dataType: "JSON"
        }).done(function (data) {
            $('#autotable').PioTable('refresh');
        });
    },
    edit: function (id) {
        $.ajax({
            type: 'POST',
            url: laroute.route('admin.service_category.edit'),
            data: {
                id: id
            },
            dataType: 'JSON',
            success: function (response) {
                $('#editForm').modal("show");
                $('#hhidden').val(response["service_category_id"]);
                $('#h_name').val(response["name"]);
                $('#h_description').val(response["description"]);
                $('#h_is_actived').val(response["is_actived"]);
                $('.error-name').text('');
            }


        });
    },
    add_service_category: function (close) {
        $.getJSON(laroute.route('translate'), function (json) {
            $('#type_add').val(close);
            $('#form').validate({
                rules: {
                    name: {
                        required: true,
                    }
                },
                messages: {
                    name: {
                        required: json['Hãy nhập nhóm dịch vụ']
                    }
                }, submitHandler: function () {
                    var name = $('#name');
                    var des = $('#description');
                    var is_actived = $('#is_actived');
                    var input = $('#type_add');
                    $.ajax({
                        url: laroute.route('admin.service_category.submitAdd'),
                        data: {
                            name: name.val(),
                            description: des.val(),
                            is_actived: is_actived.val(),
                            close: input.val()
                        },
                        method: 'POST',
                        dataType: "JSON",
                        success: function (response) {
                            if (response.status == 1) {
                                if (response.close != 0) {
                                    $("#add").modal("hide");
                                }
                                $('#form')[0].reset();
                                $('.error-name').text('');
                                swal("Thêm nhóm dịch vụ thành công", "", "success");
                                $('#service_category_id > option').remove();
                                $.each(response.optionCategory, function (index, element) {
                                    $('#service_category_id').append('<option value="' + index + '">' + element + '</option>')
                                });
                                $('#autotable').PioTable('refresh');
                            } else {
                                $('.error-name').text('Nhóm dịch vụ đã tồn tại');
                                $('.error-name').css('color', 'red');

                            }
                        }
                    })
                }
            });
        });
    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('#created_at').val('');
        $('.m_selectpicker').val('');
        $('.m_selectpicker').selectpicker('refresh');
        $(".btn-search").trigger("click");
    },
    image_dropzone: function () {
        $('#addImage').modal('show');
        $('#up-ima').empty();
        $('.dropzone')[0].dropzone.files.forEach(function (file) {
            file.previewElement.remove();
        });
        $('.dropzone').removeClass('dz-started');
        // Dropzone.options.dropzoneone={
        //     init: function () {
        //         this.options.maxFiles = this.options.maxFiles + 5;
        //     }
        // }


    },
    remove_avatar: function () {
        $('.avatar').empty();
        var tpl = $('#avatar-tpl').html();
        $('.avatar').append(tpl);
        $('.image-format').text('');
        $('.image-size').text('');
        $('.image-capacity').text('');
    },
    remove_img: function (e) {
        $(e).closest('.image-show-child').remove();
    },
    description: function () {
        $('#modal-description').modal('show');
    },
    refer_commission: function (obj) {
        if (obj == 'money') {
            $('#refer_money').attr('class', 'btn btn-info color_button active');
            $('#refer_percent').attr('class', 'btn btn-default');
        } else {
            $('#refer_percent').attr('class', 'btn btn-info color_button active');
            $('#refer_money').attr('class', 'btn btn-default');
        }
    },
    staff_commission: function (obj) {
        if (obj == 'money') {
            $('#staff_money').attr('class', 'btn btn-info color_button active');
            $('#staff_percent').attr('class', 'btn btn-default');
        } else {
            $('#staff_percent').attr('class', 'btn btn-info color_button active');
            $('#staff_money').attr('class', 'btn btn-default');
        }
    },
};
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.service.list')
});

// $.getJSON(laroute.route('translate'), function (json) {
    var arrRange = {};
    arrRange["Hôm nay"] = [moment(), moment()];
    arrRange["Hôm qua"] = [moment().subtract(1, "days"), moment().subtract(1, "days")];
    arrRange["7 ngày trước"] = [moment().subtract(6, "days"), moment()];
    arrRange["30 ngày trước"] = [moment().subtract(29, "days"), moment()];
    arrRange["Trong tháng"] = [moment().startOf("month"), moment().endOf("month")];
    arrRange["Tháng trước"] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")];

    $("#created_at").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        // buttonClasses: "m-btn btn",
        // applyClass: "btn-primary",
        // cancelClass: "btn-danger",

        maxDate: moment().endOf("day"),
        startDate: moment().startOf("day"),
        endDate: moment().add(1, 'days'),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD/MM/YYYY',
            // "applyLabel": "Đồng ý",
            // "cancelLabel": "Thoát",
            "customRangeLabel": 'Tùy chọn ngày',
            daysOfWeek: [
                json["CN"],
                json["T2"],
                json["T3"],
                json["T4"],
                json["T5"],
                json["T6"],
                json["T7"]
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        ranges: arrRange
    });
// });

function onmouseoverAddNew() {
    $('.dropdow-add-new').show();
}

function onmouseoutAddNew() {
    $('.dropdow-add-new').hide();
}

// function readURL(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//
//         reader.onload = function (e) {
//             $('#blah')
//                 .attr('src', e.target.result);
//         };
//
//         reader.readAsDataURL(input.files[0]);
//     }
// }
function uploadImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imageAvatar = $('#service_avatar');
        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#getFile').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());
        $.getJSON(laroute.route('translate'), function (json) {
            if (Math.round(fsize / 1024) <= 10240) {
                $('.error_img').text('');
                $.ajax({
                    url: laroute.route("admin.service.uploads"),
                    method: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (res) {
                        if (res.success == 1) {
                            $('#service_avatar').val(res.file);
                            $('.delete-img').css('display', 'block');

                        }

                    }
                });
            } else {
                $('.error_img').text('Hình ảnh vượt quá dung lượng cho phép');
            }
        });
    }
}

$('.btn-save-image').click(function () {
    var arrayImage = new Array();
    $('.file_Name').each(function () {
        arrayImage.push($(this).val());
    });
    // $('.image-show').empty();
    for (let i = 0; i < arrayImage.length; i++) {
        let $_tpl = $('#imgeShow').html();
        let tpl = $_tpl;
        tpl = tpl.replace(/{link}/g, 'temp_upload/' + arrayImage[i]);
        tpl = tpl.replace(/{link_hidden}/g, arrayImage[i]);
        $('.image-show').append(tpl);
        $('.delete-img-sv').css('display', 'block');
    }

    $('#addImage').modal('hide');

});
