function clearModalAdd() {
    $('#modalAdd #category-name').val('');
    $('#modalAdd #description').val('');
    $('#modalAdd .error-category-name').text('');
    $('.is_actived').prop('checked', true);
}

var productCategory = {
    remove: function (obj, id) {

        $(obj).closest('tr').addClass('m-table__row--danger');
        $.getJSON(laroute.route('translate'), function (json) {
        swal({
            title: json['Thông báo'],
            text: json["Bạn có muốn xóa không?"],
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: json['Xóa'],
            cancelButtonText: json['Hủy'],
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.product-category.remove', {id: id}), function () {
                    swal(
                        json['Xóa thành công.'],
                        '',
                        'success'
                    );
                    $('#autotable').PioTable('refresh');
                });
            }
        });
    });
    },
    changeStatus: function (obj, id, action) {
        $.post(laroute.route('admin.product-category.change-status'), {id: id, action: action}, function (data) {
            $('#autotable').PioTable('refresh');
        }, 'JSON');
    },
    clearModalAdd: function () {
        clearModalAdd();
    },
    add: function () {
        let categoryName = $('#modalAdd #category-name');
        let description = $('#modalAdd #description');
        let errorCategoryName = $('#modalAdd .error-category-name');
        let check = 0;
        if ($('.is_actived').is(':checked')) {
            check = 1;
        }
        errorCategoryName.css('color', 'red');
        if (categoryName.val() != "") {
            $.ajax({
                url: laroute.route('admin.product-category.add'),
                data: {
                    categoryName: categoryName.val(),
                    description: description.val(),
                    isActived: check
                },
                method: "POST",
                success: function (data) {
                    $.getJSON(laroute.route('translate'), function (json) {
                    if (data.status == 1) {
                        swal(
                            json['Thêm danh mục thành công'],
                            '',
                            'success'
                        );
                        $('#autotable').PioTable('refresh');
                        clearModalAdd();
                    } else {
                        errorCategoryName.text(json['Danh mục đã tồn tại']);
                    }
                });
                }
            });
        } else {
            $.getJSON(laroute.route('translate'), function (json) {
            errorCategoryName.text(json['Vui lòng nhập tên danh mục']);
            });
        }
    },
    addClose: function () {
        let categoryName = $('#modalAdd #category-name');
        let description = $('#modalAdd #description');
        let isActived = $('#modalAdd #is_actived');
        let errorCategoryName = $('#modalAdd .error-category-name');
        let check = 0;
        if ($('.is_actived').is(':checked')) {
            check = 1;
        }
        errorCategoryName.css('color', 'red');
        if (categoryName.val() != "") {
            $.ajax({
                url: laroute.route('admin.product-category.add'),
                data: {
                    categoryName: categoryName.val(),
                    description: description.val(),
                    isActived: check
                },
                method: "POST",
                success: function (data) {
                    $.getJSON(laroute.route('translate'), function (json) {
                    if (data.status == 1) {
                        swal(
                            json['Thêm danh mục thành công'],
                            '',
                            'success'
                        );
                        $('#modalAdd').modal('hide');
                        $('#autotable').PioTable('refresh');
                        clearModalAdd();
                    } else {
                        errorCategoryName.text(json['Danh mục đã tồn tại']);
                    }
                });
                }
            });
        } else {
            $.getJSON(laroute.route('translate'), function (json) {
            errorCategoryName.text(json['Vui lòng nhập tên danh mục']);
            });
        }
    },
    edit: function (id) {
        let categoryName = $('#modalEdit #category-name');
        let description = $('#modalEdit #description');
        let idHidden = $('#modalEdit #idHidden');
        let isActive = $('#modalEdit .is_actived');
        $('#modalEdit .error-category-name').text('');
        $.ajax({
            url: laroute.route('admin.product-category.edit'),
            data: {
                id: id
            },
            method: "POST",
            dataType: 'JSON',
            success: function (data) {
                $('#modalEdit').modal('show');
                categoryName.val(data.categoryName);
                description.val(data.description);
                if (data.isActived == 1) {
                    isActive.prop('checked', true);
                } else {
                    isActive.prop('checked', false);
                }
                idHidden.val(data.categoryId);
            }
        })
    },
    submitEdit: function () {
        let categoryName = $('#modalEdit #category-name');
        let description = $('#modalEdit #description');
        let isActived = $('#modalEdit .is_actived');
        let check = 0;
        if (isActived.is(':checked')) {
            check = 1;
        }
        let idHidden = $('#modalEdit #idHidden');
        let errorCategoryName = $('#modalEdit .error-category-name');
        errorCategoryName.css("color", "red");
        if (categoryName.val() != "") {
            $.ajax({
                    url: laroute.route('admin.product-category.submit-edit'),
                    data: {
                        id: idHidden.val(),
                        categoryName: categoryName.val(),
                        description: description.val(),
                        isActived: check,
                        parameter: 0
                    },
                    method: "POST",
                    dataType: "JSON",
                    success: function (data) {
                        $.getJSON(laroute.route('translate'), function (json) {
                        if (data.status == 0) {
                            errorCategoryName.text(json['Danh mục đã tồn tại']);
                        }
                        if(data.status==1){
                            swal(
                                json['Cập nhật danh mục thành công'],
                                '',
                                'success'
                            );
                            $('#modalEdit').modal('hide');
                            $('#autotable').PioTable('refresh');
                            errorCategoryName.text('');
                        }else if (data.status == 2) {
                            swal({
                                title: json['Danh mục sản phẩm đã tồn tại'],
                                text: json["Bạn có muốn kích hoạt lại không?"],
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: json['Có'],
                                cancelButtonText: json['Không'],
                            }).then(function (willDelete) {
                                if (willDelete.value == true) {
                                    $.ajax({
                                        url: laroute.route('admin.product-category.submit-edit'),
                                        data: {
                                            id: idHidden.val(),
                                            categoryName: categoryName.val(),
                                            description: description.val(),
                                            isActived: check,
                                            parameter: 1
                                        },
                                        method: "POST",
                                        dataType: 'JSON',
                                        success: function (data) {
                                            if (data.status = 3) {
                                                swal(
                                                    json['Kích hoạt danh mục sản phẩm thành công'],
                                                    '',
                                                    'success'
                                                );
                                                $('#autotable').PioTable('refresh');
                                                $('#modalEdit').modal('hide');
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });

                    }
                }
            );
        } else {
            $.getJSON(laroute.route('translate'), function (json) {
            errorCategoryName.text(json['Vui lòng nhập tên danh mục']);
            });
        }
    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('');
        $('.m_selectpicker').selectpicker('refresh');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    }
};
$('#autotable').PioTable({
    baseUrl: laroute.route('admin.product-category.list')
});
$('select[name="is_actived"]').select2();

