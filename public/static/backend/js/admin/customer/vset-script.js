"use strict";
function formatDate(strDate){
    var date = new Date(strDate);
    var dateStr =
        ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
        ("00" + date.getDate()).slice(-2) + "-" +
        date.getFullYear() + " " +
        ("00" + date.getHours()).slice(-2) + ":" +
        ("00" + date.getMinutes()).slice(-2) + ":" +
        ("00" + date.getSeconds()).slice(-2);
    return dateStr;
}
var vsetScript = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.customer.list-history-customer-vset'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_history'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'order_code',
                    title: 'Mã đơn hàng',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    template: function (row) {
                        return '<span>' + row.order_code + '</span>';
                    }

                },
                {
                    field: 'product_name_vi',
                    title: 'Tên gói',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span>' + row.product_name_vi + '</span>';
                    }
                },
                {
                    field: 'price_standard',
                    title: 'Giá',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.price_standard).toLocaleString()  + ' VNĐ </span>';
                    }

                },
                {
                    field: 'payment_method_name_vi',
                    title: 'Hình thức giao dịch',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {

                        return '<span class="m--font-bolder">' + row.payment_method_name_vi +'</span>';
                    }

                },
                {
                    field: 'quantity',
                    title: 'Số lượng',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {

                        return '<span class="m--font-bolder">' + row.quantity +'</span>';
                    }

                },
                {
                    field: 'process_status',
                    title: 'Trạng thái',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        if (row.process_status == 'new') {
                            return '<span class="m--font-bolder">Mới</span>';
                        } else if (row.process_status == 'paysuccess') {
                            return '<span class="m--font-bolder">Thanh toán thành công</span>';
                        } else if (row.process_status == 'confirmed') {
                            return '<span class="m--font-bolder">Xác nhận</span>';
                        } else if (row.process_status == 'pay-half') {
                            return '<span class="m--font-bolder">Thanh toán 1 phần</span>';
                        }
                    }

                },
                {
                    field: 'total',
                    title: 'Thành tiền',
                    filterable: false, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.total).toLocaleString() + ' VNĐ </span>';
                    }
                },
                {
                    field: 'created_at',
                    title: 'Ngày đặt hàng',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        let d = new Date(row.created_at);
                        let day = d.getDate();
                        let month = d.getMonth() + 1;
                        let year = d.getFullYear();
                        let hour = d.getHours();
                        let minute = d.getMinutes();
                        let second =d.getSeconds();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        if (second < 10) {
                            second = "0" + second;
                        }
                        let date = day + "-" + month + "-" + year + " " + hour + ":" + minute + ":" + second;
                        return '<span>' + date + '</span>';
                    }
                }
            ],
        };
        vsetScript.pioTable = $('#datatable_list_history').mDatatable(options);
    },
    tab_list_history:function () {
        $('#datatable_list_history').mDatatable().search('');
    },
};

// setTimeout(function(){
//
// }, 1200);
vsetScript.init();











var vsetStockHistoryScript = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.customer.list-stock-history-customer-vset'),
                        params: {
                            customer_id: $('#customer_id_hidden').val(),
                            is_filter:"0"
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_history'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'stock_order_code',
                    title: 'Mã giao dịch',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    template: function (row) {
                        return '<span>' + row.stock_order_code + '</span>';
                    }

                },
                {
                    field: 'action',
                    title: 'Hành động',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span>' + row.action + '</span>';
                    }
                },
                {
                    field: 'transaction_object',
                    title: 'Bên giao dịch',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + row.transaction_object + '  </span>';
                    }

                },
                {
                    field: 'quantity',
                    title: 'Số cổ phiếu',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {

                        return '<span class="m--font-bolder">' + row.quantity +'</span>';
                    }

                },
                {
                    field: 'total',
                    title: 'Tổng giá trị',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {

                        return '<span class="m--font-bolder">' + parseInt(row.total).toLocaleString() +'</span>';
                    }

                },       
                {
                    field: 'process_status',
                    title: 'Trạng thái',
                    filterable: true, // disable or enablePOP filtering
                    template: function (row) {
                        if (row.process_status == 'new') {
                            return '<span class="m--font-bolder">Mới</span>';
                        } else if (row.process_status == 'paysuccess') {
                            return '<span class="m--font-bolder">Thanh toán thành công</span>';
                        } else if (row.process_status == 'confirmed') {
                            return '<span class="m--font-bolder">Xác nhận</span>';
                        } else if (row.process_status == 'pay-half') {
                            return '<span class="m--font-bolder">Thanh toán 1 phần</span>';
                        }
                        else if (row.process_status == 'cancel') {
                            return '<span class="m--font-bolder">Đã hủy</span>';
                        }
                    }

                },
                {
                    field: 'created_at',
                    title: 'Ngày giao dịch',
                    filterable: false, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + formatDate(row.created_at) + ' </span>';
                    }
                },
  
            ],
        };
        vsetScript.pioTable = $('#datatable_list_stock_history').mDatatable(options);
    },
    tab_list_stock_history:function () {
        $('#datatable_list_stock_history').mDatatable().search('');
    },
};

vsetStockHistoryScript.init();


var vsetListIntroduct = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.customer.list-introduct'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_contract'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'customer_code',
                    title: 'Mã nhà đầu tư',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.customer_code + '</span>';
                    }
                },
                {
                    field: 'full_name',
                    title: 'Họ tên',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.full_name + '</span>';
                    }
                },
                {
                    field: 'phone_login',
                    title: 'Tài khoản',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.phone_login + '</span>';
                    }
                },
                {
                    field: 'email',
                    title: 'Email',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.email + '</span>';
                    }
                },
                {
                    field: 'sum_money_contract',
                    title: 'Đang đầu tư',
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        if (row.sum_money_contract != null) {
                            return '<span class="m--font-bolder">' + parseInt(row.sum_money_contract).toLocaleString() + ' VNĐ </span>';
                        } else {
                            return '<span class="m--font-bolder">0 VNĐ </span>';

                        }
                    }
                },

            ],
        };
        vsetListIntroduct.pioTable = $('#datatable_list_introduct').mDatatable(options);
    },
    tab_list_introduct:function () {
        $('#datatable_list_introduct').mDatatable().search('');
    }
};

// setTimeout(function(){
//
// }, 1200);
vsetListIntroduct.init();


var vsetContractCommission = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.customer.list-contract-commission'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_history'),
                delay: 100,
            },

// columns definition
            columns: [
                // {
                //     field: 'customer_contract_code',
                //     title: 'Mã hợp đồng',
                //     filterable: true, // disable or enablePOP filtering,
                //     template: function (row) {
                //         return '<span>' + row.customer_contract_code + '</span>';
                //     }
                //
                // },
                // {
                //     field: 'product_name_vi',
                //     title: 'Tên gói',
                //     filterable: true, // disable or enablePOP filtering,
                //     template: function (row) {
                //         return '<span>' + row.product_name_vi + '</span>';
                //     }
                //
                // },
                // {
                //     field: 'commission',
                //     title: 'Tiền hoa hồng',
                //     filterable: false, // disable or enablePOP filtering
                //     template: function (row) {
                //         return '<span class="m--font-bolder">' + parseInt(row.commission).toLocaleString() + ' VNĐ </span>';
                //     }
                // },
                {
                    field: 'total_money',
                    title: 'Tiền đã nhận',
                    filterable: false, // disable or enablePOP filtering
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseInt(row.total_money).toLocaleString() + ' VNĐ </span>';
                    }
                },
                {
                    field: 'commission_rate',
                    title: 'Tỉ lệ thưởng',
                    filterable: false, // disable or enablePOP filtering
                    template: function (row) {
                        if(row.commission_rate != null) {
                            return '<span class="m--font-bolder">' + parseInt(row.commission_rate).toLocaleString() + ' % </span>';
                        } else {
                            return '<span class="m--font-bolder"> N/A </span>';
                        }

                    }
                },
                {
                    field: 'type',
                    title: 'Loại',
                    filterable: false, // disable or enablePOP filtering
                    template: function (row) {
                        if(row.type == 'bonus') {
                            return '<span> Thưởng </span>';
                        } else if (row.type == 'refer') {
                            return '<span> Thưởng giới thiệu </span>';
                        } else if (row.type == 'register') {
                            return '<span> Thưởng đăng ký </span>';
                        }
                    }
                },
                {
                    field: 'created_at',
                    title: 'Ngày được tặng',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        let d = new Date(row.created_at);
                        let day = d.getDate();
                        let month = d.getMonth();
                        let year = d.getFullYear();
                        let hour = d.getHours();
                        let minute = d.getMinutes();
                        let second =d.getSeconds();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        if (second < 10) {
                            second = "0" + second;
                        }
                        let date = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
                        // return '<span>' + row.created_at + '</span>';
                        return '<span>' + date + '</span>';
                    }
                }
            ],
        };
        vsetContractCommission.pioTable = $('#datatable_list_contract_commission').mDatatable(options);
    },
    tab_list_contract_commission:function () {
        $('#datatable_list_contract_commission').mDatatable().search('');
    },
};

vsetContractCommission.init();

// todo: vsetListStockContract---------------------------------
// todo: vsetListStockContract--------------
var vsetListStockContract = {
    pioTable: null,
    init: function () {
        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        headers: {},
                        url: laroute.route('admin.stock-contract.list-datatable'),
                        params: {
                            customer_id: $('#customer_id_hidden').val()
                        },
                        map: function (raw) {
// sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 5,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: 0
            },

// layout definition
            layout: {
                theme: "default",
                class: "",
                scroll: !0,
                height: "auto",
                footer: 0
            },
// column sorting
            sortable: !0,
            toolbar: {
                placement: ["bottom"], items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    }
                }
            },
            search: {
                input: $('#search_contract'),
                delay: 100,
            },

// columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                    template: function (row, index, datatable) {
                        return (index + 1 + (datatable.getCurrentPage()) * datatable.getPageSize()) - datatable.getPageSize();
                    }
                },
                {
                    field: 'stock_contract_code',
                    title: 'Mã hợp đồng',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering,
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.stock_contract_code + '</span>';
                    }
                },

                {
                    field: 'quantity',
                    title: 'Số lượng',
                    width: 150,
                    filterable: true, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span>' + row.quantity + '</span>';
                    }
                },
                {
                    field: 'wallet_stock_money',
                    title: 'Tổng tiền cổ phiếu',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseFloat(row.wallet_stock_money).toLocaleString() + ' VNĐ </span>';
                    }
                },
                {
                    field: 'wallet_dividend_money',
                    title: 'Tổng tiền cổ tức',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        return '<span class="m--font-bolder">' + parseFloat(row.wallet_dividend_money).toLocaleString() + ' VNĐ </span>';
                    }
                },


                {
                    field: 'is_active',
                    title: 'Trạng thái',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        if (row.is_active == 0 || row.is_deleted == 1) {
                            return '<span> Đã hết hạn </span>';
                        } else {
                            return '<span> Đang hoạt động </span>';
                        }
                    }
                },
                {
                    field: 'customer_contract_start_date',
                    title: 'Ngày ký hợp đồng',
                    filterable: false, // disable or enablePOP filtering
                    textAlign: 'center',
                    template: function (row) {
                        let d = new Date(row.created_at);
                        let day = d.getDate();
                        let month = d.getMonth() + 1;
                        let year = d.getFullYear();
                        let hour = d.getHours();
                        let minute = d.getMinutes();
                        let second =d.getSeconds();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        if (minute < 10) {
                            minute = "0" + minute;
                        }
                        if (second < 10) {
                            second = "0" + second;
                        }
                        let date = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
                        return '<span>' + date + '</span>';
                    }
                }
            ],
        };
        vsetListStockContract.pioTable = $('#datatable_list_stock_contract').mDatatable(options);
    },
    tab_list_stock_contract:function () {
        $('#datatable_list_stock_contract').mDatatable().search('');
    }
};
vsetListStockContract.init();