var product = {
    onMouseOverAddNew: function () {
        $('.dropdow-add-new').show();
    },
    onMouseOutAddNew: function () {
        $('.dropdow-add-new').hide();
    },
    remove: function (obj, id) {
        // $.getJSON(laroute.route('translate'), function (json) {
            $(obj).closest('tr').addClass('m-table__row--danger');

            swal({
                title: 'Thông báo',
                text: "Bạn có muốn xóa không?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Xóa',
                cancelButtonText: 'Hủy',
                onClose: function () {
                    $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    $.post(laroute.route('admin.product.remove', {id: id}), function (data) {
                        swal(
                            'Xóa thành công',
                            '',
                            'success'
                        );
                        $('#autotable').PioTable('refresh');

                    });
                }
            });
        // });
    },

    addProduct: function (close) {
        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^[a-z|A-Z|0-9]*$/i.test(value);
        });

        $('#form-product').validate({
            rules: {
                product_code: {
                    required: true,
                    maxlength:255,
                    validatePassword:true
                },
                product_category_id : {
                    required: true,
                },
                product_name_vi : {
                    required: true,
                    maxlength:255,
                },
                product_name_en : {
                    required: true,
                    maxlength:255,
                },
                product_short_name_vi : {
                    maxlength:255,
                },
                product_short_name_en : {
                    maxlength:255,
                },
                price_standard: {
                    required: true,
                    number: true
                },
                interest_rate_standard : {
                    required: true,
                    number: true
                },
                withdraw_fee_rate_before : {
                    required: true,
                    number: true
                },
                withdraw_fee_rate_ok : {
                    required: true,
                    number: true
                },
                withdraw_fee_interest_rate : {
                    required: true,
                    number: true
                },
                withdraw_min_amount : {
                    required: true,
                    number: true
                },
                // withraw_min_bonus : {
                //     required: true,
                //     number: true
                // },
                // min_allow_sale : {
                //     required: true,
                //     number: true
                // },
                type_staff_commission: {
                    required: true,
                },
                type_refer_commission : {
                    required: true,
                },
                staff_commission_value : {
                    required: true,
                    number: true
                },
                refer_commission_value : {
                    required: true,
                    number: true
                },
                published_at: {
                    required: true
                },
                bonus_extend:{
                    required: true,
                    number: true
                }
            },
            messages: {
                product_code: {
                    required: 'Yêu cầu nhập mã gói',
                    maxlength: "Mã gói vượt quá 255 ký tự",
                    validatePassword : 'Mã gói không cho phép nhập các ký tự đặc biệt , có dấu hoặc khoảng trắng'
                },
                product_category_id : {
                    required: 'Yêu cầu chọn danh mục',
                },
                product_name_vi : {
                    required: 'Yêu cầu nhập tên gói tiếng việt',
                    maxlength:'Tên gói vượt quá 255 ký tự',
                },
                product_name_en : {
                    required: 'Yêu cầu nhập tên gói tiếng anh',
                    maxlength:'Tên gói vượt quá 255 ký tự',
                },
                product_short_name_vi : {
                    maxlength:'Mô tả ngắn gói tiếng việt vượt quá 255 ký tự',
                },
                product_short_name_en : {
                    maxlength:'Mô tả ngắn gói tiếng anh vượt quá 255 ký tự',
                },
                price_standard: {
                    required: 'Yêu cầu nhập giá trị gói',
                    number: 'Giá trị gói sai định dạng'
                },
                interest_rate_standard : {
                    required: 'Yêu cầu nhập tỉ lệ lãi suất chuẩn',
                    number: 'Tỉ lệ lãi suất chuẩn sai định dạng'
                },
                withdraw_fee_rate_before : {
                    required: 'Yêu cầu nhập tỉ lệ rút tiền trước kì hạn',
                    number: 'Tỉ lệ rút tiền trước kì hạn sai định dạng'
                },
                withdraw_fee_rate_ok : {
                    required: 'Yêu cầu nhập tỉ lệ rút tiền trước đúng kì hạn',
                    number: 'Tỉ lệ rút tiền trước đúng kì hạn sai định dạng'
                },
                withdraw_fee_interest_rate : {
                    required: 'Yêu cầu nhập tỉ lệ phí rút tiền lãi',
                    number: 'Phí rút tiền lãi sai định dạng'
                },
                withdraw_min_amount : {
                    required: 'Yêu cầu nhập số tiền tối thiểu có thể rút lãi',
                    number: 'Số tiền tối thiểu có thể rút lãi sai định dạng'
                },
                // min_allow_sale : {
                //     required: 'Yêu cầu nhập giá thấp nhất có thể bán',
                //     number: 'Giá thấp nhất có thể bán sai định dạng sai định dạng'
                // },
                type_staff_commission: {
                    required: 'Yêu cầu chọn loại hoa hồng cho nhân viên phục vụ',
                },
                type_refer_commission : {
                    required: 'Yêu cầu chọn kiểu hoa hồng cho khách giới thiệu',
                },
                staff_commission_value : {
                    required: 'Yêu cầu nhập mức hoa hồng cho nhân viên phục vụ',
                    number: 'Mức hoa hồng cho nhân viên phục vụ sai định dạng'
                },
                refer_commission_value : {
                    required: 'Yêu cầu nhập mức hoa hồng cho khách giới thiệu',
                    number: 'Mức hoa hồng cho khách giới thiệu sai định dạng'
                },
                published_at: {
                    required: 'Yêu cầu chọn ngày phát hành'
                },
                bonus_extend:{
                    required: 'Yêu cầu nhập mức thưởng khi gia hạn hợp đồng',
                    number: 'Thưởng khi gia hạn hợp đồng sai định dạng'
                },
                // withraw_min_bonus : {
                //     required: 'Yêu cầu nhập số tiền thưởng tối thiểu có thể rút',
                //     number: 'Số tiền thưởng tối thiểu có thể rút sai định dạng'
                // },

            },
        });

        if (!$('#form-product').valid()) {
            return false;
        } else {

            var is_actived = 0 ;
            if ($('#is_actived').is(':checked')) {
                is_actived = 1;
            }

            // var imageContract = [];
            // var arrImageContract = [];
            // $.each($('#upload-image').find(".image-show-child"), function (i,index) {
            //     var imgService = $(this).find($('input[name=img-sv]')).val();
            //     var type = $(this).find($('input[name=type]')).val();
            //     imageContract.push(imgService);
            //     arrImageContract.push({
            //         type : 'desktop',
            //         image : imgService
            //     });
            // });
            //
            // var imageContract = [];
            // $.each($('#upload-image-mobile').find(".image-show-child-mobile"), function (i,index) {
            //     var imgService = $(this).find($('input[name=img-sv]')).val();
            //     var type = $(this).find($('input[name=type]')).val();
            //     arrImageContract.push({
            //         type : 'desktop',
            //         image : imgService
            //     });
            // });


            $.ajax({
                url: laroute.route('admin.product.submit-add'),
                method: "POST",
                dataType: "JSON",
                data: $('#form-product').serialize()+'&is_actived='+is_actived,
                success: function (data) {
                    if (data.error == true) {
                        swal('', data.message, "error");
                    } else {
                        swal('', data.message, "success").then(function () {
                            if (close == 1) {
                                window.location.href = laroute.route('admin.product.detail',{id:data.product_id,tab:'product_interest_tab'});
                            } else {
                                window.location.href = laroute.route('admin.product.add');
                            }
                        });
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire('', mess_error, "error");
                    }
                }
            });
        }
    },

    editProduct : function() {
        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^[a-z|A-Z|0-9]*$/i.test(value);
        });
        $('#form-product').validate({
            rules: {
                // product_code: {
                //     required: true,
                //     maxlength:255,
                //     validatePassword:true
                // },
                product_category_id : {
                    required: true,
                },
                product_name_vi : {
                    required: true,
                    maxlength:255,
                },
                product_name_en : {
                    required: true,
                    maxlength:255,
                },
                product_short_name_vi : {
                    maxlength:255,
                },
                product_short_name_en : {
                    maxlength:255,
                },
                // price_standard: {
                //     required: true,
                //     number: true
                // },
                interest_rate_standard : {
                    required: true,
                    number: true
                },
                withdraw_fee_rate_before : {
                    required: true,
                    number: true
                },
                withdraw_fee_rate_ok : {
                    required: true,
                    number: true
                },
                withdraw_fee_interest_rate : {
                    required: true,
                    number: true
                },
                withdraw_min_amount : {
                    required: true,
                    number: true,
                },
                // min_allow_sale : {
                //     required: true,
                //     number: true
                // },
                type_staff_commission: {
                    required: true,
                },
                type_refer_commission : {
                    required: true,
                },
                staff_commission_value : {
                    required: true,
                    number: true
                },
                refer_commission_value : {
                    required: true,
                    number: true
                },
                published_at: {
                    required: true
                },
                bonus_extend:{
                    required: true,
                    number: true
                },
                // withraw_min_bonus : {
                //     required: true,
                //     number: true
                // },

            },
            messages: {
                // product_code: {
                //     required: 'Yêu cầu nhập mã gói',
                //     maxlength: "Mã gói vượt quá 255 ký tự",
                //     validatePassword : 'Mã gói không cho phép nhập các ký tự đặc biệt , có dấu hoặc khoảng trắng'
                // },
                product_category_id : {
                    required: 'Yêu cầu chọn danh mục',
                },
                product_name_vi : {
                    required: 'Yêu cầu nhập tên gói tiếng việt',
                    maxlength:'Tên gói vượt quá 255 ký tự',
                },
                product_name_en : {
                    required: 'Yêu cầu nhập tên gói tiếng anh',
                    maxlength:'Tên gói vượt quá 255 ký tự',
                },
                product_short_name_vi : {
                    maxlength:'Mô tả ngắn gói tiếng việt vượt quá 255 ký tự',
                },
                product_short_name_en : {
                    maxlength:'Mô tả ngắn gói tiếng anh vượt quá 255 ký tự',
                },
                // price_standard: {
                //     required: 'Yêu cầu nhập giá trị gói',
                //     number: 'Giá trị gói sai định dạng'
                // },
                interest_rate_standard : {
                    required: 'Yêu cầu nhập tỉ lệ lãi suất chuẩn',
                    number: 'Tỉ lệ lãi suất chuẩn sai định dạng'
                },
                withdraw_fee_rate_before : {
                    required: 'Yêu cầu nhập tỉ lệ rút tiền trước kì hạn',
                    number: 'Tỉ lệ rút tiền trước kì hạn sai định dạng'
                },
                withdraw_fee_rate_ok : {
                    required: 'Yêu cầu nhập tỉ lệ rút tiền trước đúng kì hạn',
                    number: 'Tỉ lệ rút tiền trước đúng kì hạn sai định dạng'
                },
                withdraw_fee_interest_rate : {
                    required: 'Yêu cầu nhập tỉ lệ phí rút tiền lãi',
                    number: 'Phí rút tiền lãi sai định dạng'
                },
                withdraw_min_amount : {
                    required: 'Yêu cầu nhập số tiền tối thiểu có thể rút lãi',
                    number: 'Số tiền tối thiểu có thể rút lãi sai định dạng',
                },
                // min_allow_sale : {
                //     required: 'Yêu cầu nhập giá thấp nhất có thể bán',
                //     number: 'Giá thấp nhất có thể bán sai định dạng sai định dạng'
                // },
                type_staff_commission: {
                    required: 'Yêu cầu chọn loại hoa hồng cho nhân viên phục vụ',
                },
                type_refer_commission : {
                    required: 'Yêu cầu chọn kiểu hoa hồng cho khách giới thiệu',
                },
                staff_commission_value : {
                    required: 'Yêu cầu nhập mức hoa hồng cho nhân viên phục vụ',
                    number: 'Mức hoa hồng cho nhân viên phục vụ sai định dạng'
                },
                refer_commission_value : {
                    required: 'Yêu cầu nhập mức hoa hồng cho khách giới thiệu',
                    number: 'Mức hoa hồng cho khách giới thiệu sai định dạng'
                },
                published_at: {
                    required: 'Yêu cầu chọn ngày phát hành'
                },
                bonus_extend:{
                    required: 'Yêu cầu nhập mức thưởng khi gia hạn hợp đồng',
                    number: 'Thưởng khi gia hạn hợp đồng sai định dạng'
                },
                // withraw_min_bonus : {
                //     required: 'Yêu cầu nhập số tiền thưởng tối thiểu có thể rút',
                //     number: 'Số tiền thưởng tối thiểu có thể rút sai định dạng'
                // },
            },
        });

        if (!$('#form-product').valid()) {
            return false;
        } else {

            var is_actived = 0 ;
            if ($('#is_actived').is(':checked')) {
                is_actived = 1;
            }

            $.ajax({
                url: laroute.route('admin.product.submit-edit'),
                method: "POST",
                dataType: "JSON",
                data: $('#form-product').serialize()+'&is_actived='+is_actived,
                success: function (data) {
                    if (data.error == true) {
                        swal('', data.message, "error");
                    } else {
                        swal('', data.message, "success").then(function () {
                            window.location.href = laroute.route('admin.product');
                        });
                    }
                },
                error: function (res) {
                    if (res.responseJSON != undefined) {
                        var mess_error = '';
                        $.map(res.responseJSON.errors, function (a) {
                            mess_error = mess_error.concat(a + '<br/>');
                        });
                        swal.fire('', mess_error, "error");
                    }
                }
            });
        }
    },

    changeStatus: function (obj, id, action) {
        $.ajax({
            url: laroute.route('admin.product.change-status'),
            method: "POST",
            data: {
                id: id, action: action
            },
            dataType: "JSON"
        }).done(function (data) {
            $('#autotable').PioTable('refresh');
        });
    },
    edit: function (id) {
        $.ajax({
            url: laroute.route('admin.product.edit'),
            method: "POST",
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                $('#modalEdit').modal("show");
                $('#modalEdit #product_model_id').val(data.product_model_id);
                $('#modalEdit #product_id').val(data.product_id);
                $('#modalEdit #product_category_id').val(data.product_category_id);
                $('#modalEdit #product_name').val(data.product_name);
                $('#modalEdit #product_short_name').val(data.product_short_name);
                $('#modalEdit #unit_id').val(data.unit_id);
                $('#modalEdit #cost').val(data.cost);
                $('#modalEdit #price_standard').val(data.price_standard);
                $('#modalEdit #is_sales').val(data.is_sales);
                $('#modalEdit #is_promo').val(data.is_promo);
                $('#modalEdit #type').val(data.type);
                $('#modalEdit #description').val(data.description);
                $('#modalEdit #supplier_id').val(data.supplier_id);
                $('#modalEdit #is_actived').val(data.is_actived);
            }
        });
    },
    submitEdit: function () {
        $('#formEdit').validate({
            rules: {
                product_name: {required: true},
                cost: {required: true, min: 0},
            },
            messages: {
                product_name: "Vui lòng nhập tên sản phẩm",
                cost: {
                    required: "Vui lòng nhập giá sản phẩm",
                    min: "Giá phải lớn hơn 0"
                }
            },
            submitHandler: function () {
                $.ajax({
                    url: laroute.route('admin.product.submit-edit'),
                    method: "POST",
                    dataType: "JSON",
                    data: {
                        product_id: $('#modalEdit #product_id').val(),
                        product_category_id: $('#modalEdit #product_category_id').val(),
                        product_name: $('#modalEdit #product_name').val(),
                        product_short_name: $('#modalEdit #product_short_name').val(),
                        unit_id: $('#modalEdit #unit_id').val(),
                        cost: $('#modalEdit #cost').val(),
                        price_standard: $('#modalEdit #price_standard').val(),
                        is_sales: $('#modalEdit #is_sales').val(),
                        is_promo: $('#modalEdit #is_promo').val(),
                        type: $('#modalEdit #type').val(),
                        description: $('#modalEdit #description').val(),
                        supplier_id: $('#modalEdit #supplier_id').val(),
                        is_actived: $('#modalEdit #is_actived').val()
                    },
                    success: function (data) {

                        swal("Cập nhật sản phẩm thành công", "", "success");
                        $('#modalEdit').modal('hide');
                        $('#autotable').PioTable('refresh');

                    }
                });
            }
        });
    },
    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('').trigger('change');
        $('#created_at').val('');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    },
    notEnterInput: function (thi) {
        $(thi).val('');
    },
    
    getListProductBonus: function (page) {
        $('#page_product_bonus').val(page);
        $.ajax({
            url: laroute.route('admin.product.get-list-product-bonus'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                page:page,
                id : $('#product_id').val()
            },
            success: function (res) {
                $('.bonus_value').empty();
                $('.bonus_value').append(res.view);
            },
        });
    },

    getListInterestRate: function (page) {
        $('#page_interest_rate').val(page);
        $.ajax({
            url: laroute.route('admin.product.get-list-interest-rate'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                page:page,
                id : $('#product_id').val()
            },
            success: function (res) {
                $('.interest_rate').empty();
                $('.interest_rate').append(res.view);
            },
        });
    },

    removeProductBonus: function (obj, id) {
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.product.remove-product-bonus', {id: id}), function (data) {
                    swal(
                        'Xóa thành công',
                        '',
                        'success'
                    ).then(function () {
                        product.getListProductBonus(1);
                    });
                });
            }
        });
    },

    removeProductInterest: function (obj, id) {
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.post(laroute.route('admin.product.remove-product-interest', {id: id}), function (data) {
                    swal(
                        'Xóa thành công',
                        '',
                        'success'
                    ).then(function () {
                        product.getListInterestRate(1);
                    });
                });
            }
        });
    },

    addProductBonus:function () {
        $('#product_bonus_add').validate({
            rules: {
                payment_method_id: {
                    required: true
                },
                investment_time_id: {
                    required: true
                },
                bonus_rate: {
                    required: true
                },
            },
            messages: {
                payment_method_id: {
                    required: 'Yêu cầu chọn hình thức thanh toán'
                },
                investment_time_id: {
                    required: 'Yêu cầu chọn kỳ hạn đầu tư'
                },
                bonus_rate: {
                    required: 'Yêu cầu nhập tỉ lệ tiền thưởng'
                },
            },
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
        });

        if (!$('#product_bonus_add').valid()) {
            return false;
        } else {
            var is_active = 0;
            if ($('.is_actived_product_bonus').is(":checked")) {
                is_active = 1;
            }
            $.ajax({
                url: laroute.route('admin.product.add-product-bonus'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#product_bonus_add').serialize()+'&is_actived='+is_active,
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            $('#product_bonus_popup').modal('hide');
                            $('#product_bonus_add').resetForm();
                            product.getListProductBonus(1);
                        });
                    }
                },
            });
        }
    },

    detailProductBonus : function (id) {
        $.ajax({
            url: laroute.route('admin.product.detail-product-bonus'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                id : id,
                idProduct : $('#product_id').val()
            },
            success: function (res) {
                $('#append-product-bonus').empty();
                $('#append-product-bonus').append(res.view);
                $('.select2').select2();
                $('#product_bonus_popup_detail').modal('show');
            },
        });
    },
    editProductBonus : function (id) {
            $.ajax({
                url: laroute.route('admin.product.edit-product-bonus'),
                method: 'POST',
                dataType: 'JSON',
                data: {
                    id : id,
                    idProduct : $('#product_id').val()
                },
                success: function (res) {
                    $('#append-product-bonus').empty();
                    $('#append-product-bonus').append(res.view);
                    new AutoNumeric.multiple('.number-money', {
                        currencySymbol: '',
                        decimalCharacter: '.',
                        digitGroupSeparator: ',',
                        decimalPlaces: 2
                    });
                    $('.select2').select2();
                    $('#product_bonus_popup_edit').modal('show');
                },
            });
    },

    editProductBonusPost : function() {
        $('#product_bonus_edit').validate({
            rules: {
                payment_method_id: {
                    required: true
                },
                investment_time_id: {
                    required: true
                },
                bonus_rate: {
                    required: true
                },
            },
            messages: {
                payment_method_id: {
                    required: 'Yêu cầu chọn hình thức thanh toán'
                },
                investment_time_id: {
                    required: 'Yêu cầu chọn kỳ hạn đầu tư'
                },
                bonus_rate: {
                    required: 'Yêu cầu nhập tỉ lệ tiền thưởng'
                },
            },
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
        });

        if (!$('#product_bonus_edit').valid()) {
            return false;
        } else {
            var is_active = 0;
            if ($('.is_actived_product_bonus_edit').is(":checked")) {
                is_active = 1;
            }
            $.ajax({
                url: laroute.route('admin.product.edit-product-bonus-post'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#product_bonus_edit').serialize()+'&is_actived='+is_active,
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            $('#product_bonus_popup_edit').modal('hide');
                            product.getListProductBonus(1);
                        });
                    }
                },
            });
        }
    },

    addProductInterest:function () {
        $('#product_interest_add_form').validate({
            rules: {
                term_time_type: {
                    required: true
                },
                investment_time_id: {
                    required: true
                },
                withdraw_interest_time_id: {
                    required: true
                },
                interest_rate: {
                    required: true
                },
                commission_rate: {
                    required: true
                },
                month_interest: {
                    required: true
                },
                total_interest: {
                    required: true
                },
            },
            messages: {
                term_time_type: {
                    required: 'Yêu cầu chọn loại lãi suất'
                },
                investment_time_id: {
                    required: 'Yêu cầu chọn kỳ hạn đầu tư'
                },
                withdraw_interest_time_id: {
                    required: 'Yêu cầu chọn kỳ hạn rút lãi'
                },
                interest_rate: {
                    required: 'Yêu cầu nhập lãi suất'
                },
                commission_rate: {
                    required: 'Yêu cầu nhập tỉ lệ hoa hồng'
                },
                month_interest: {
                    required: 'Yêu cầu nhập tổng lãi suất hàng tháng'
                },
                total_interest: {
                    required: 'Yêu cầu nhập tổng lãi suất'
                },
            },
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
        });

        if (!$('#product_interest_add_form').valid()) {
            return false;
        } else {
            var is_default_display = 0;
            if ($('.is_default_display_add').is(":checked")) {
                is_default_display = 1;
            }

            var is_actived = 0;
            if ($('.is_actived_add').is(":checked")) {
                is_actived = 1;
            }

            product_id = $('#product_id').val();
            $.ajax({
                url: laroute.route('admin.product.add-product-interest'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#product_interest_add_form').serialize()+'&is_actived='+is_actived+'&is_default_display='+is_default_display+'&product_id='+product_id,
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            $('#product_interest_popup').modal('hide');
                            $('#product_interest_add').resetForm();
                            product.getListInterestRate(1);
                        });
                    }
                },
            });
        }
    },

    detailProductInterest : function (id) {
        $.ajax({
            url: laroute.route('admin.product.detail-product-interest'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                id : id,
                idProduct : $('#product_id').val()
            },
            success: function (res) {
                $('#append-product-interest').empty();
                $('#append-product-interest').append(res.view);
                $('.select2').select2();
                $('#product_interest_popup_detail').modal('show');
            },
        });
    },
    editProductInterest : function (id) {
        $.ajax({
            url: laroute.route('admin.product.edit-product-interest'),
            method: 'POST',
            dataType: 'JSON',
            data: {
                id : id,
                idProduct : $('#product_id').val()
            },
            success: function (res) {
                $('#append-product-interest').empty();
                $('#append-product-interest').append(res.view);
                new AutoNumeric.multiple('.number-money', {
                    currencySymbol: '',
                    decimalCharacter: '.',
                    digitGroupSeparator: ',',
                    decimalPlaces: 2
                });
                $('.select2').select2();
                $('#product_interest_popup_edit').modal('show');
            },
        });
    },

    editProductInterestPost : function () {
        $('#product_interest_edit_form').validate({
            rules: {
                term_time_type: {
                    required: true
                },
                investment_time_id: {
                    required: true
                },
                withdraw_interest_time_id: {
                    required: true
                },
                interest_rate: {
                    required: true
                },
                commission_rate: {
                    required: true
                },
                month_interest: {
                    required: true
                },
                total_interest: {
                    required: true
                },
            },
            messages: {
                term_time_type: {
                    required: 'Yêu cầu chọn loại lãi suất'
                },
                investment_time_id: {
                    required: 'Yêu cầu chọn kỳ hạn đầu tư'
                },
                withdraw_interest_time_id: {
                    required: 'Yêu cầu chọn kỳ hạn rút lãi'
                },
                interest_rate: {
                    required: 'Yêu cầu nhập lãi suất'
                },
                commission_rate: {
                    required: 'Yêu cầu nhập tỉ lệ hoa hồng'
                },
                month_interest: {
                    required: 'Yêu cầu nhập tổng lãi suất hàng tháng'
                },
                total_interest: {
                    required: 'Yêu cầu nhập tổng lãi suất'
                },
            },
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
        });

        if (!$('#product_interest_edit_form').valid()) {
            return false;
        } else {
            var is_default_display = 0;
            if ($('.is_default_display_edit').is(":checked")) {
                is_default_display = 1;
            }

            var is_actived = 0;
            if ($('.is_actived_edit').is(":checked")) {
                is_actived = 1;
            }
            product_id = $('#product_id').val();

            $.ajax({
                url: laroute.route('admin.product.edit-product-interest-post'),
                method: 'POST',
                dataType: 'JSON',
                data: $('#product_interest_edit_form').serialize()+'&is_actived='+is_actived+'&is_default_display='+is_default_display+'&product_id='+product_id,
                success: function (res) {
                    if (res.error == true) {
                        swal(res.message,'','error');
                    } else {
                        swal(res.message,'','success').then(function () {
                            $('#product_interest_popup_edit').modal('hide');
                            $('#product_interest_edit_form').resetForm();
                            product.getListInterestRate(1);
                        });
                    }
                },
            });
        }
    },

    calculateInterest : function (investment_time_id,withdraw_interest_time_id,term_time_type) {
        var investment_time = $('.investment_time_month_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).val();
        var interest_rate = $('.interest_rate_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).val();
        var price_standard = $('.price_standard_hidden').val();
        if (interest_rate != '' && interest_rate > 0) {
            var month_interest = (interest_rate/(100*12))*price_standard;
            var total_interest = investment_time * month_interest;
            $('.month_interest_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).val(Math.ceil(month_interest));
            $('.text_month_interest_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).text(formatNumber(Math.ceil(month_interest).toFixed(2)));
            $('.total_interest_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).val(Math.ceil(total_interest));
            $('.text_total_interest_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).text(formatNumber(Math.ceil(total_interest).toFixed(2)));
        }
    },

    calculateWithdraw : function (withdraw_interest_time_id,term_time_type) {
        var investment_time = $('.investment_time_month'+'_'+withdraw_interest_time_id+'_'+term_time_type).val();
        var interest_rate = $('.interest_rate'+'_'+withdraw_interest_time_id+'_'+term_time_type).val();
        var price_standard = $('.price_standard_hidden').val();
        if (interest_rate != '' && interest_rate > 0) {
            var month_interest = (interest_rate/(100*12))*price_standard;
            $('.month_interest'+'_'+withdraw_interest_time_id+'_'+term_time_type).val(Math.ceil(month_interest));
            $('.text_month_interest'+'_'+withdraw_interest_time_id+'_'+term_time_type).text(formatNumber(Math.ceil(month_interest).toFixed(2)));
        }
    },

    submitListInterestWithraw : function () {
        $.ajax({
            url: laroute.route('admin.product.submit-list-investment-withdraw'),
            method: 'POST',
            data: $('#list-investment-withdraw').serialize(),
            success: function (res) {
                if (res.error == true) {
                    swal(res.message,'','error');
                } else {
                    swal(res.message,'','success').then(function () {
                        location.reload();
                    });
                }
            },
        });
    },

    submitListInterestWithrawNotType : function () {
        $.ajax({
            url: laroute.route('admin.product.submit-list-investment-withdraw-not-type'),
            method: 'POST',
            data: $('#list-investment-withdraw').serialize(),
            success: function (res) {
                if (res.error == true) {
                    swal(res.message,'','error');
                } else {
                    swal(res.message,'','success').then(function () {
                        location.reload();
                    });
                }
            },
        });
    },

    submitBonusProduct : function() {
        $.ajax({
            url: laroute.route('admin.product.submit-list-product-bonus'),
            method: 'POST',
            data: $('#list-product-bonus').serialize(),
            success: function (res) {
                if (res.error == true) {
                    swal(res.message,'','error');
                } else {
                    swal(res.message,'','success').then(function () {
                        location.reload();
                    });
                }
            },
        });
    },

    changDisplay : function (investment_time_id,withdraw_interest_time_id,term_time_type) {
        $('.is_default_display_check').prop( "checked", false );
        $('.is_default_display_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).prop( "checked", true );

        var is_display = 0 ;
        if ($('.is_default_display_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).is(':checked')) {
            is_display = 1;
        }

        if (is_display == 1) {
            count = 0;
            // $.each($('.investment-withdraw').find(".investment_time_"+investment_time_id+'_'+term_time_type),function () {
            $.each($('.investment-withdraw').find(".m-switch"),function () {
                var check = $(this).find($('.is_default_display_check')).is(':checked');
                if (check == true) {
                    count++;
                }

                if (count > 1) {
                    swal('Kì hạn đầu tư đã có hiển thị chính','','error');
                    $('.is_default_display_'+investment_time_id+'_'+withdraw_interest_time_id+'_'+term_time_type).prop("checked", false).trigger("change");
                    return false;
                }
            })
        }
    }
};

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$('#autotable').PioTable({
    baseUrl: laroute.route('admin.product.list')
});

$(document).ready(function () {
    var value = $('#product_category_id').val();
    // tiết kiệm
    if (value == 2){
        $('#withdraw_min_time').show();
    } else {
        $('#withdraw_min_time').hide();
    }

    $('#product_category_id').change(function () {
        var value = $('#product_category_id').val();
        // tiết kiệm
        if (value == 2){
            $('#withdraw_min_time').show();
        } else {
            $('#withdraw_min_time').hide();
        }
    })

    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });

    $('.is_default_display_check_term').change(function () {
        if ($('.is_default_display_check_term').is(':checked')) {
            $('.is_default_display_check').prop('checked',false);
            $('.is_default_display_check_term').prop('checked',true);
        };
    });
})

function uploadImageMain(input,lang) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_main_'+lang)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_image_main_'+lang).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_product_main_'+lang+'.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#image_main_upload_'+lang).val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function uploadImageDetail(input,lang) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_detail_'+lang)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_image_detail_'+lang).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_product_detail_'+lang+'.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#image_detail_upload_'+lang).val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}


function uploadImageMainMobile(input,lang) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_main_mobile_'+lang)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_image_main_mobile_'+lang).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_product_main_mobile_'+lang+'.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#image_main_upload_mobile_'+lang).val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function uploadImageDetailMobile(input,lang) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_detail_mobile_'+lang)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        var file_data = $('#id_image_detail_mobile_'+lang).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('link', '_product_detail_mobile_'+lang+'.');
        var fsize = input.files[0].size;
        var fileInput = input,
            file = fileInput.files && fileInput.files[0];
        var img = new Image();

        img.src = window.URL.createObjectURL(file);

        img.onload = function () {
            var imageWidth = img.naturalWidth;
            var imageHeight = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);

            $('.image-size').text(imageWidth + "x" + imageHeight + "px");

        };
        $('.image-capacity').text(Math.round(fsize / 1024) + 'kb');

        $('.image-format').text(input.files[0].name.split('.').pop().toUpperCase());

        if (Math.round(fsize / 1024) <= 10240) {
            $.ajax({
                url: laroute.route("config.upload"),
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (res) {
                    if (res.success == 1) {
                        $('#image_detail_upload_mobile_'+lang).val(res.file);
                    }
                }
            });
        } else {
            swal("Hình ảnh vượt quá dung lượng cho phép", "", "error");
        }
    }
}

function dropzone(){
    Dropzone.options.dropzoneone = {
        paramName: 'file',
        maxFilesize: 500000, // MB
        maxFiles: 100,
        acceptedFiles: ".jpeg,.jpg,.png,",
        addRemoveLinks: true,
        // headers: {
        //     "X-CSRF-TOKEN": $('input[name=_token]').val()
        // },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('customer-contract.upload-dropzone'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneone').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageShow').html();
                    tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                    tpl = tpl.replace(/{link_hidden}/g, response);
                    tpl = tpl.replace(/{numberImage}/g, numberImage);
                    numberImage++;
                    $('#upload-image').append(tpl);
                }
            });
            this.on('removedfile', function (file,response) {
                var name = file.upload.filename;
                $.ajax({
                    url: laroute.route('admin.service.delete-image'),
                    method: "POST",
                    data: {

                        filename: name
                    },
                    success: function () {
                        $("input[class='file_Name']").each(function () {
                            var $this = $(this);
                            if ($this.val() === name) {
                                $this.remove();
                            }
                        });

                    }
                });
            });
        }
    }

    Dropzone.options.dropzoneoneimage = {
        paramName: 'file',
        maxFilesize: 500000, // MB
        maxFiles: 100,
        acceptedFiles: ".jpeg,.jpg,.png,",
        addRemoveLinks: true,
        // headers: {
        //     "X-CSRF-TOKEN": $('input[name=_token]').val()
        // },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('customer-contract.upload-dropzone'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneoneimage')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneoneimage').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageShowMobile').html();
                    tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                    tpl = tpl.replace(/{link_hidden}/g, response);
                    tpl = tpl.replace(/{numberImageMobile}/g, numberImageMobile);
                    numberImageMobile++;
                    $('#upload-image-mobile').append(tpl);
                }
            });
            this.on('removedfile', function (file,response) {
                var name = file.upload.filename;
                $.ajax({
                    url: laroute.route('admin.service.delete-image'),
                    method: "POST",
                    data: {

                        filename: name
                    },
                    success: function () {
                        $("input[class='file_Name']").each(function () {
                            var $this = $(this);
                            if ($this.val() === name) {
                                $this.remove();
                            }
                        });

                    }
                });
            });
        }
    }
}

$(document).ready(function () {
    $('input[name="type_bonus_refer"]').change(function () {
        if ($(this).val() == 'money'){
            $('.voucher_refer').prop('disabled',true);
            $('.money_refer').prop('disabled',false);
        } else {
            $('.money_refer').prop('disabled',true);
            $('.voucher_refer').prop('disabled',false);
        }
    });
    if ($('.is_active_register').is(':checked')){
        $('.add_service_register').prop('disabled',false);
    } else {
        $('.add_service_register').prop('disabled',true);
    }
    $('.is_active_register').change(function () {
        if ($('.is_active_register').is(':checked')){
            $('.add_service_register').prop('disabled',false);
        } else {
            $('.add_service_register').prop('disabled',true);
        }
    });

});

function removeImage(e){
    $(e).closest('.image-show-child').remove();
}

function removeImageMobile(e){
    $(e).closest('.image-show-child-mobile').remove();
}

var linkSource = {

    deleteSearch: function(){
        $('#service_name').val('');
        $('#service_category').val('').trigger('change');
        linkSource.searchPopup(1);
    },

    removeService : function (obj,id,type) {
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa dịch vụ không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route("admin.product.deleteService"),
                    method: "POST",
                    data: {
                        service_id : id
                    },
                    success: function (res) {
                        $('.body_tr_register #tr_'+id).remove();
                    }
                });
            }
        });
    },

    showPopup: function(type) {
        linkSource.searchPopup(1);
    },

    searchPopup: function(page){
        $('#popup_service #service_name').val('');
        $.ajax({
            url: laroute.route("admin.product.getListService"),
            method: "POST",
            data: $('#search_service').serialize()+'&page='+page,
            success: function (res) {
                $('.table-service').empty();
                $('.table-service').append(res.view);
                $('#popup_service').modal('show');
            }
        });
    },

    addListService: function () {
        var listService = [];
        $.each($('.table_service_popup').find(".service_id"), function () {
            if ($(this).is(':checked')){
                listService.push($(this).val());
            }
        });
        if (listService.length == 0) {
            swal.fire('Vui lòng chọn dịch vụ để thêm', "", "error")
        } else {
            $.ajax({
                url: laroute.route("admin.product.addListService"),
                method: "POST",
                data: {
                    listService : listService
                },
                success: function (res) {
                    $('.body_tr_register').append(res.view);
                }
            });
        }
    },

    saveConfig: function (type) {
        var is_active = 0;
        if ($('.is_active_register').is(':checked')){
            is_active = 1;
        }

        data = $('.form-product-bonus-config').serialize()+'&is_active='+is_active+'&date_action_register='+$('.date_action_register').val()+'&product_id='+$('#product_id').val()

        $.ajax({
            url: laroute.route("admin.product.saveConfig"),
            method: "POST",
            data: data,
            success: function (res) {
                if (res.error == false){
                    swal.fire(res.message,'','success');
                } else {
                    swal.fire(res.message,'','error');
                }
            }
        });
    },
}