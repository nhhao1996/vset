$('#autotable').PioTable({
    baseUrl: laroute.route('admin.log-email.list')
});

$(".daterange-picker").daterangepicker({
    autoUpdateInput: false,
    autoApply: true,
    buttonClasses: "m-btn btn",
    applyClass: "btn-primary",
    cancelClass: "btn-danger",
    locale: {
        format: 'DD/MM/YYYY',
        "applyLabel": "Đồng ý",
        "cancelLabel": "Thoát",
        "customRangeLabel": "Tùy chọn ngày",
        daysOfWeek: [
            "CN",
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7"
        ],
        "monthNames": [
            "Tháng 1 năm",
            "Tháng 2 năm",
            "Tháng 3 năm",
            "Tháng 4 năm",
            "Tháng 5 năm",
            "Tháng 6 năm",
            "Tháng 7 năm",
            "Tháng 8 năm",
            "Tháng 9 năm",
            "Tháng 10 năm",
            "Tháng 11 năm",
            "Tháng 12 năm"
        ],
        "firstDay": 1
    },
    // ranges: {
    //     'Hôm nay': [moment(), moment()],
    //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
    //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
    //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
    //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
    //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
    // }
}).on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
});