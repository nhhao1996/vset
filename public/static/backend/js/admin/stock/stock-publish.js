$(document).ready(function(){
  StockPublishRespository.init();
});
var that = null;
var StockPublishRespository = {
    init(){
        // this.onSubmit();
        this.onInputAmount();
        this.onPublishStock();
        this.onBtnPublishPoup();
        this.onBtnStopPublish();
        this.onBtnHistory();
        this.onChangeFilterPublishDay()
        that = this;

    },
    onSubmit(){
       $("#btnPublish").on('click',function(){
        that.publishStock();
        //    if(!that.validate("popup-stock-publish")){
            //    return;             
        //    }else{
        //        that.publishStock();
              
        //    }
           
       });

    },
    onInputAmount(e){
        $("[name='quantity']").on('keyup',function(){           
            that.handleTotalAmount(e);

        });
        $("[name='money']").on('keyup',function(){              
            that.handleTotalAmount(e);

        });
        
        
    },    
    handleTotalAmount(e){   
        // todo: Mõi lần nhập số lượng phát hành
        let quantity = $("[name='quantity']") .val().trim();
        let money = $("[name='money']") .val().trim();
        quantity = quantity!==''?parseInt(quantity):0;
        money = money!==''?parseInt(money):0;
        let total = quantity*money;
        $("#total_amount").val(total);
    },

    validate(frmForm){
        $("#"+frmForm).validate({
            rules: {
                quantity: {
                    required: true,
                    min:0
                },
                    money: {
                    required: true,                   
                },
                quantity_sale: {
                    required: true,                   
                }
               
            },
            messages: {
                quantity:{
                    required:'Số lượng phát hành không được rỗng',
                    min:"Số lượng phát hành phải lớn hơn 0"
                },
                quantity_sale: {
                    required: "Đã bán không được rỗng"
                },
                "money": {
                    required: "Giá phát hành không được rỗng",
                    min: "Giá phát hành phải lớn hơn 0"
                }
            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#frm-stock-publish").valid()

    },
    onPublishStock(){
        $("#btnPublish").on("click", function(){
            $('#modal-publish-stock').modal('show');     
        });        

    },
    onBtnPublishPoup() {
        $("#btnPublishPopup").on("click", function () {
            const quantity = $("[name='quantity']").val().trim().replace(",","");
            const price = $("[name='money']").val().trim();
            const quantityMin = $("#quantity_min").val().trim().replace(",","");
            const numQuantity = parseInt(quantity);
            const numQuantityMin = parseInt(quantityMin);
            console.log('quantity, quantityMin: ', numQuantity, numQuantityMin);
            if (quantity == '') {
                Swal.fire('', 'Số lượng phát hành không dược rỗng', 'error');
                return;
            } else if (price == '') {
                Swal.fire('', 'Giá tiêu chuẩn không dược rỗng', 'error');
                return;
            } else if (quantityMin == '') {
                Swal.fire('', 'Số lượng bán tối thiểu không được rỗng', 'error');
                return;
            } else if (numQuantityMin > numQuantity) {
                Swal.fire('', 'Số lượng mua tối thiểu phải nhỏ hơn số cổ phiếu phát hành', 'error');
                return;
            }
            that.releaseStock("popup-stock-publish");
            //    that.validate("popup-stock-publish");
            // });

        });
    },
    releaseStock(){
        $.ajax({
            url: laroute.route('admin.stock.publish'),
            method: "POST",
            dataType: "JSON",
            data:$(`#frmStockPublish`).serialize(),

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    },
    publishStock(){
        $.ajax({
            url: laroute.route('admin.stock.publish'),
            method: "POST",
            dataType: "JSON",
            data:$(`#frmStockPublish`).serialize(),

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    },
    handleStopPublish(){
        let data = {
            stock_id:$("[name='stock_id']").val(),
            is_active:0
        };
        $.ajax({
            url: laroute.route('admin.editStockPublish'),
            method: "POST",
            dataType: "JSON",
            data:data,

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    },

    onBtnStopPublish(){
        $("#btnStopPublish").on('click', function(){
            swal({
                title: 'Ngưng phát hành cổ phiếu',
                text: "Bạn có muốn ngưng phát hành cổ phiếu?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Xác nhận',
                cancelButtonText: 'Hủy',
            }).then(function (result) {
                if (result.value) {
                    that.handleStopPublish();                 
                }
              })
        });        

    },
    onBtnHistory(){
        $("#btnHistory").on('click', function(){
            $("#modal-stock-publish-history").modal("show");           
        });
    },
    search(){
        let date = $("#publish_at").val();
        let href = $("#btnExportStockPublish").attr("href").replace(/\?.*/i,'');
        href  = href + "?publish_at="+date;
        $("#btnExportStockPublish").attr("href",href);


        $.ajax({
            url: laroute.route('admin.listStockPublish'),
            method: "POST",          
            data:$(`.frmFilter`).serialize(),

            success: function (data) {
                $(".table-history").html("");
                $(".table-history").html(data);
                // if(data.error == 1){
                //     swal('', data.message, "error");
                // }
                // else{
                //     Swal('',data.message,'success').then(function(result){
                //         if(result) window.location.reload();
                //     });          

                // }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });
    },
    onChangeFilterPublishDay(){
        $("#publish_at").on('change', function(){
            let date = $($this).val();
            console.log('date la gi: ', date);
        });
    }


};