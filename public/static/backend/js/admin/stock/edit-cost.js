// todo: Config------------------
new AutoNumeric.multiple(".number-fix", {
  currencySymbol: "",
  decimalCharacter: ".",
  digitGroupSeparator: ",",
  decimalPlaces: 2,
});
// !--------Config---------------

$(document).ready(function () {
  StockPublishRespository.init();
});
var that = null;
var StockPublishRespository = {
  init() {
    // this.onSubmit();
    this.onBtnSave();
    that = this;
  },
  validate() {
    $("#frm").validate({
      rules: {

        fee_withraw_stock: { required: true,min:0,max:100},
        fee_withraw_dividend: { required: true,min:0,max:100},
        fee_convert_bond_stock: { required: true,min:0,max:100},
        fee_convert_saving_stock: { required: true,min:0,max:100},
        fee_stock_sell_market: { required: true,min:0,max:100},
        fee_transfer_stock: { required: true,min:0,max:100}
      },
      messages: {
        fee_withraw_stock:
          {
            required:"Yêu cầu nhập Phí rút cổ phiếu",
            min:"Phí rú cổ phiếu tối thiểu là 0",
            max:'Phí rú cổ phiếu tối đa là 100'
          },
        fee_withraw_dividend:
            {
                required:"Yêu cầu nhập Phí rút cổ tức ",
                min:"Phí rút cổ tức tối thiểu là 0",
              max:"Phí rút cổ tức tối đa là 100"
            },
        fee_convert_bond_stock:
            {
                required:"Yêu cầu nhập Phí chuyển đổi hợp đồng gói đầu tư sang cổ phiếu ",
                min:"Phí chuyển đổi hợp đồng gói đầu tư sang cổ phiếu tối thiểu là 0",
              max:"Phí chuyển đổi hợp đồng gói đầu tư sang cổ phiếu tối đa là 100"
            },
        fee_convert_saving_stock:
            {
                required:"Yêu cầu nhập Phí chuyển đổi hợp đồng tiết kiệm sang cổ phiếu ",
                min:"Phí chuyển đổi hợp đồng tiết kiếm ang cổ phiếu tối thiểu là 0",
              max:"Phí chuyển đổi hợp đồng tiết kiếm ang cổ phiếu tối thiểu là 100"
            },
        fee_stock_sell_market:
            {
                required:"Yêu cầu nhập Phí bán cổ phiếu qua chợ",
              min:"Phí cổ phiếu qua chợ tối thiểu là 0",
              max:"Phí cổ phiếu qua chợ tối đa là 100"
            },
        fee_transfer_stock:
            {
                required:"Yêu cầu nhập Phí chuyển nhượng cổ phiếu ",
                min:"Phí chuyển nhượng tối thiểu là 0",
                max:"Phí chuyển nhượng tối đa là 100"
            },
      },
      submitHandler: function (form) {
        // for demo
        alert("valid form submitted"); // for demo
        return false; // for demo
      },
    });
    return $("#frm").valid();
  },
  releaseStock() {
    $.ajax({
      url: laroute.route("admin.stock.publish"),
      method: "POST",
      dataType: "JSON",
      data: $(`#popup-stock-publish`).serialize(),

      success: function (data) {
        if (data.error == 1) {
          swal("", data.message, "error");
        } else {
          Swal("", data.message, "success").then(function (result) {
            if (result) window.location.reload();
          });
        }
      },
      // error: function (res) {
      //     console.log('error la gi: ', res);
      //     var mess_error = '';
      //     jQuery.each(res.responseJSON.errors, function (key, val) {
      //         mess_error = mess_error.concat(val + '<br/>');
      //     });
      //     swal.fire('', mess_error, "error");
      // }
    });
  },
  publishStock(frm) {
    $.ajax({
      url: laroute.route("admin.stock.publish"),
      method: "POST",
      dataType: "JSON",
      data: $(`fsd`).serialize(),

      success: function (data) {
        if (data.error == 1) {
          swal("", data.message, "error");
        } else {
          Swal("", data.message, "success").then(function (result) {
            if (result) window.location.reload();
          });
        }
      },
      // error: function (res) {
      //     console.log('error la gi: ', res);
      //     var mess_error = '';
      //     jQuery.each(res.responseJSON.errors, function (key, val) {
      //         mess_error = mess_error.concat(val + '<br/>');
      //     });
      //     swal.fire('', mess_error, "error");
      // }
    });
  },
  handleStopPublish() {
    let data = {
      stock_id: $("[name='stock_id']").val(),
      is_active: 0,
    };
    $.ajax({
      url: laroute.route("admin.editStockPublish"),
      method: "POST",
      dataType: "JSON",
      data: data,

      success: function (data) {
        if (data.error == 1) {
          swal("", data.message, "error");
        } else {
          Swal("", data.message, "success").then(function (result) {
            if (result) window.location.reload();
          });
        }
      },
      // error: function (res) {
      //     console.log('error la gi: ', res);
      //     var mess_error = '';
      //     jQuery.each(res.responseJSON.errors, function (key, val) {
      //         mess_error = mess_error.concat(val + '<br/>');
      //     });
      //     swal.fire('', mess_error, "error");
      // }
    });
  },

  onBtnStopPublish() {

    $("#btnStopPublish").on("click", function () {
      swal({
        title: 'Thông báo',
        text: "Bạn có muốn xóa không?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Xóa',
        cancelButtonText: 'Hủy',
      }).then(function (result) {
        if (result.value) {
          that.handleStopPublish();
        }
      });
    });
  },
  handleSubmit() {
    $.ajax({
      url: laroute.route("admin.stock.editPost"),
      method: "POST",
      dataType: "JSON",
      data: $(`#frm`).serialize(),

      success: function (data) {
        if (data.error == 1) {
          swal("", data.message, "error");
        } else {
            swal("", data.message, "success").then(function () {
                window.location.href = laroute.route('admin.stock.getCost');
          });
        }
      },
      // error: function (res) {
      //     console.log('error la gi: ', res);
      //     var mess_error = '';
      //     jQuery.each(res.responseJSON.errors, function (key, val) {
      //         mess_error = mess_error.concat(val + '<br/>');
      //     });
      //     swal.fire('', mess_error, "error");
      // }
    });
  },
  onBtnSave() {
    $("#btnSave").on("click", function () {
      // alert('Luu');
      if (that.validate()) {
        that.handleSubmit();
      }
    });
  },
};
