var BonusHandler = {
    stockFrom:"",
    init(){
        this.onActive();
    },
    onActive(){
        $("[name='is_active']").click(function(){
            const checked = $(this).prop("checked");
            const pathName = window.location.pathname;
            const param = pathName.match(/([a-z]+)$/i);
            let key = "";

            switch(param[0]) {
                case 'order':
                    key = 'stock_amount_bonus';
                    break;
                case 'method':
                    key = 'stock_payment_method_bonus';
                    break;
                case 'referral':
                    key = 'stock_refer_bonus';
                    break;

                default:
            }
                    //
            swal({
                title: 'Bạn chắc chắn muốn thay đổi trạng thái cấu hình thưởng?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Đồng ý',
                cancelButtonText: 'Huỷ',
                onClose: function () {
                    // remove hightlight row
                    // $(obj).closest('tr').removeClass('m-table__row--danger');
                }
            }).then(function (result) {
                if (result.value) {
                    var stock_bonus_conifg_id = $("[name='stock_bonus_config_id']").val();
                    $.post(laroute.route('admin.stock.config-bonus.edit', {stock_bonus_config_id: key, is_active:checked}), function () {
                        swal(
                            'Cập nhật thành công !',
                            '',
                            'success'
                        ).then(function(result){
                            if(result.value)  window.location.reload();
                        });



                        // $('#autotable').PioTable('refresh');
                    });
                }
            });
        });
    },
    onDelete($id){
        swal({
            title: 'Bạn chắc chắn muốn xóa dòng này?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Huỷ',
            onClose: function () {
                // remove hightlight row
                // $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                var stock_bonus_conifg_id = $("[name='stock_bonus_config_id']").val();
                $.post(laroute.route('admin.stock.config-bonus.order.remove',
                    {stock_bonus_amount_id:$id}), function () {
                    swal(
                        'Đã xóa thành công!',
                        '',
                        'success'
                    ).then(function(result){
                        if(result.value) window.location.reload();
                    });

                });
            }
        });
    },
    getValue(name){
        return $(`[name="${name}"]`).val().trim();

    },
    setValute(name="",val=""){
        return $(`[name="${name}"]`).val(val);

    },
    add(){
        let from = this.getValue('stock_from').replace(",","");
        let to = this.getValue("stock_to").replace(",","");
        let rate = this.getValue("rate");
        if(from == ""){
            Swal.fire("Yêu cầu nhập Từ","","error");
            return;
        }
        if(to== ""){
            Swal.fire("Yêu cầu nhập Đến","","error");
            return;
        }
        if(parseInt(to) < parseInt(from)){
            Swal.fire("Yêu cầu Đến không được nhỏ hơn Từ","","error");
            return;
        }
        if(this.getValue("rate") == ""){
            Swal.fire("Yêu cầu nhập Tỷ lệ thưởng","","error");
            return;
        }
        let data = $("#frmAdd").serialize();
        if(this.stockFrom!==""){
            data += '&stock_from='+from;
        }
        $.ajax({
            url: laroute.route('admin.stock.config-bonus.order.add'),
            method: "POST",
            data:data ,
            success: function (res) {
                console.log(res);
                if(res.error){
                    Swal.fire("Thêm thành công","","success").then(function(result){
                        if(result.value) window.location.reload(true);
                    });

                }
                else{
                    Swal.fire("Thêm thất bại","","error");
                }

            }
        });
    },
    validate(){
        let times = 1;
        $("#frmAdd").validate({
            errorPlacement: function(error,element){
                // error.insertAfter(element);
                if(times==1) alert(error.html());
                times++;

                // if(!isShow){
                //     alert(error.html());
                //     isShow=true;
                // }else{
                //     isShow = !isShow;
                // }

            },
            rules: {
                "stock_from": {
                    required: true,
                },
                "stock_to": {
                    required: true,
                },
                "rate":{
                    required: true,
                }
            },
            messages: {
                "stock_from": {
                    required: "Yêu cầu nhập Từ"
                },
                "stock_to": {
                    required: "Yêu cầu nhập Đến"
                },
                "rate": {
                    required: "Yêu cầu nhập Tỷ lệ thưởng"
                },
            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#frmAdd").valid();
    },
    showPopupAdd(){
        let lastStockTo = $(".stock_to").last().text().trim().replace(",","");
        if(lastStockTo == "") return;
        $("[name='stock_from']").val(parseInt($lastBonus['stock_to']) + 1).prop("disabled","disabled");
        $('#stock2').val('');
        $('#rate').val('');
        this.stockFrom = lastStockTo;

    },
    showPopupEdit(...item){
        this.setValute('stock_from',item[0])
        this.setValute('stock_to',item[1]);
        this.setValute('rate',item[2]);
        this.setValute('stock_bonus_amount_id',item[3]);
        console.log('what item is: ', item);

    },
    edit(){
        let from = this.getValue('stock_from').replace(",","");
        let to = this.getValue("stock_to").replace(",","");
        let rate = $('#rate_change_edit_amount').val();
        if(from == ""){
            Swal.fire("Yêu cầu nhập Từ","","error");
            return;
        }
        if(to== ""){
            Swal.fire("Yêu cầu nhập Đến","","error");
            return;
        }
        if(parseInt(to) < parseInt(from)){
            Swal.fire("Yêu cầu Đến không được nhỏ hơn Từ","","error");
            return;
        }
        if(this.getValue("rate") == ""){
            Swal.fire("Yêu cầu nhập Tỷ lệ thưởng","","error");
            return;
        }
        $.ajax({
            url: laroute.route('admin.stock.config-bonus.order.edit'),
            method: "POST",
            data:{
                stock_bonus_amount_id:$("[name='stock_bonus_amount_id']").val(),
                rate: $('#rate_change_edit_amount').val()
            },
            success: function (res) {
                console.log(res);
                if(res.error){
                    Swal.fire(res.message,"","success").then(function(result){
                        if(result.value) window.location.reload(true);
                    });

                }
                else{
                    Swal.fire("Thêm thất bại","","error");
                }

            }
        });
    },
}
var BonusPaymentHandler = {
    stockFrom:"",
    init(){
        // this.onActive();
    },

    edit(){
        // let from = this.getValue('stock_from');
        // let to = this.getValue("stock_to");
        // let rate = this.getValue("rate");
        // if(from == ""){
        //     Swal.fire("Yêu cầu nhập Từ","","error");
        //     return;
        // }
        // if(to== ""){
        //     Swal.fire("Yêu cầu nhập Đến","","error");
        //     return;
        // }
        // if(parseInt(to) < parseInt(from)){
        //     Swal.fire("Yêu cầu Đến không được nhỏ hơn Từ","","error");
        //     return;
        // }
        // if(this.getValue("rate") == ""){
        //     Swal.fire("Yêu cầu nhập Tỷ lệ thưởng","","error");
        //     return;
        // }
        // let data = $("#frmAdd").serialize();
        // if(this.stockFrom!==""){
        //     data += '&stock_from='+this.stockFrom;
        // }
        if(!this.validate()) return;
        $.ajax({
            url: laroute.route('admin.stock.config-bonus.payment.submitEdit'),
            method: "POST",
            data:$("#frmEdit").serialize() +'&stock_payment_method_bonus='+$("#is_active").prop('checked'),
            success: function (res) {
                if(!res.error){
                    Swal.fire(res.message,"","success").then(function(result){
                        if(result.value) {
                            window.location.href=laroute.route('admin.stock.config-bonus.payment');
                        }
                    });

                }
                else{
                    Swal.fire(res.message,"","error");
                }

            }
        });
    },
    validate(){

        $("#frmEdit").validate({
            rules: {
                "cash_rate": {
                    required: true,
                },
                "transfer_rate": {
                    required: true,
                },
                "stock_rate":{
                    required: true,
                },
                "dividend_rate":{
                    required: true,
                },
                "transfer_rate":{
                    required: true,
                }
            },
            messages: {
                "cash_rate": {
                    required: "Yêu cầu nhập tỷ lệ thưởng tiền mặt"
                },
                "transfer_rate": {
                    required: "Yêu cầu nhập tỷ lệ thưởng chuyển khoản"
                },
                "stock_rate": {
                    required: "Yêu cầu nhập tỷ lệ thưởng ví cổ phiếu"
                },
                "dividend_rate": {
                    required: "Yêu cầu nhập tỷ lệ thưởng ví cổ tức"
                },
            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#frmEdit").valid();
    },
    showPopupAdd(){
        let lastStockTo = $(".stock_to").last().text().trim();
        if(lastStockTo == "") return;
        $("[name='stock_from']").val(parseInt($lastBonus['stock_to']) + 1).prop("disabled","disabled");
        this.stockFrom = lastStockTo;

    },
}
var BonusTransferHandler = {
    stockFrom:"",
    init(){
    },
    onDelete($id){
        swal({
            title: 'Bạn chắc chắn muốn xóa dòng này?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Huỷ',
            onClose: function () {
                // remove hightlight row
                // $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                var stock_bonus_conifg_id = $("[name='stock_bonus_config_id']").val();
                $.post(laroute.route('admin.stock.config-bonus.order.remove',
                    {stock_bonus_amount_id:$id}), function () {
                    swal(
                        'Đã xóa thành công!',
                        '',
                        'success'
                    ).then(function(result){
                        if(result.value) window.location.reload();
                    });

                });
            }
        });
    },
    getValue(name){
        console.log(name);
        return $(`[name="${name}"]`).val().trim();

    },
    setValue(name="",val=""){
        return $(`[name="${name}"]`).val(val);

    },
    validate(){
        let times = 1;
        $("#frmAdd").validate({
            errorPlacement: function(error,element){
                // error.insertAfter(element);
                if(times==1) alert(error.html());
                times++;

                // if(!isShow){
                //     alert(error.html());
                //     isShow=true;
                // }else{
                //     isShow = !isShow;
                // }

            },
            rules: {
                "stock_from": {
                    required: true,
                },
                "stock_to": {
                    required: true,
                },
                "rate":{
                    required: true,
                }
            },
            messages: {
                "stock_from": {
                    required: "Yêu cầu nhập Từ"
                },
                "stock_to": {
                    required: "Yêu cầu nhập Đến"
                },
                "rate": {
                    required: "Yêu cầu nhập Tỷ lệ thưởng"
                },
            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#frmAdd").valid();
    },
    showPopupEdit(...item){


        // this.setValue('product_name_vi',item[1])
        console.log('what item is: ', item);
        this.setValue('product_name_vi',item[0]);
        $("[name='bonus_rate']").val(item[1]);
        this.setValue("product_id",item[2]);

    },
    edit(){
        if(this.getValue("bonus_rate") == ""){
            Swal.fire("Yêu cầu nhập Tỷ lệ thưởng","","error");
            return;
        }
        $.ajax({
            url: laroute.route('admin.stock.config-bonus.transfer.submitEdit'),
            method: "POST",
            data:{
                stock_bonus_amount_id:$("[name='stock_bonus_amount_id']").val(),
                bonus_rate:this.getValue("bonus_rate"),
                product_id:this.getValue(("product_id"))
            },
            success: function (res) {
                console.log(res);
                if(!res.error){
                    Swal.fire(res.message,"","success").then(function(result){
                        if(result.value) window.location.reload(true);
                    });

                }
                else{
                    Swal.fire("Cập nhật thất bại","","error");
                }

            }
        });
    },
}
var BonusReferalHandler = {
    stockFrom:"",
    init(){
        this.onActive();
    },
    onDelete($id){
        swal({
            title: 'Bạn chắc chắn muốn xóa dòng này?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Huỷ',
            onClose: function () {
                // remove hightlight row
                // $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                var stock_bonus_conifg_id = $("[name='stock_bonus_config_id']").val();
                $.post(laroute.route('admin.stock.config-bonus.referral.remove',
                    {stock_bonus_refer_id:$id}), function () {
                    swal(
                        'Đã xóa thành công!',
                        '',
                        'success'
                    ).then(function(result){
                        if(result.value) window.location.reload();
                    });

                });
            }
        });
    },
    getValue(name){
        console.log(name);
        return $(`[name="${name}"]`).val().trim();

    },
    setValute(name="",val=""){
        return $(`[name="${name}"]`).val(val);

    },
    add(){
        let from = this.getValue('stock_from').replace(",","");
        let to = this.getValue("stock_to").replace(",","");
        let rate = this.getValue("rate");
        if(from == ""){
            Swal.fire("Yêu cầu nhập Từ","","error");
            return;
        }
        if(to== ""){
            Swal.fire("Yêu cầu nhập Đến","","error");
            return;
        }
        if(parseInt(to) < parseInt(from)){
            Swal.fire("Yêu cầu Đến không được nhỏ hơn Từ","","error");
            return;
        }
        if(this.getValue("rate") == ""){
            Swal.fire("Yêu cầu nhập Tỷ lệ thưởng","","error");
            return;
        }
        let data = $("#frmAdd").serialize();
        if(this.stockFrom!==""){
            data += '&stock_from='+from;
        }
        $.ajax({
            url: laroute.route('admin.stock.config-bonus.referral.submitAdd'),
            method: "POST",
            data:data ,
            success: function (res) {
                console.log(res);
                if(!res.error){
                    Swal.fire("Thêm thành công","","success").then(function(result){
                        if(result.value) window.location.reload(true);
                    });

                }
                else{
                    Swal.fire("Thêm thất bại","","error");
                }

            }
        });
    },
    validate(){
        let times = 1;
        $("#frmAdd").validate({
            errorPlacement: function(error,element){
                // error.insertAfter(element);
                if(times==1) alert(error.html());
                times++;

                // if(!isShow){
                //     alert(error.html());
                //     isShow=true;
                // }else{
                //     isShow = !isShow;
                // }

            },
            rules: {
                "stock_from": {
                    required: true,
                },
                "stock_to": {
                    required: true,
                },
                "rate":{
                    required: true,
                }
            },
            messages: {
                "stock_from": {
                    required: "Yêu cầu nhập Từ"
                },
                "stock_to": {
                    required: "Yêu cầu nhập Đến"
                },
                "rate": {
                    required: "Yêu cầu nhập Tỷ lệ thưởng"
                },
            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#frmAdd").valid();
    },
    showPopupAdd(){
        let lastStockTo = $(".stock_to").last().text().trim();
        if(lastStockTo == "") return;
        $("[name='stock_from']").val(parseInt($lastBonus['stock_to']) + 1).prop("disabled","disabled");
        $('#stock2').val('');
        $('#rate').val('');
        this.stockFrom = lastStockTo;

    },
    showPopupEdit(...item){
        console.log('item la gi: ', item);
        this.setValute('stock_from',item[0])
        this.setValute('stock_to',item[1]);
        this.setValute('rate',item[2]);
        this.setValute('stock_bonus_refer_id',item[3]);
        console.log('what item is: ', item);

    },
    edit(){
        let from = this.getValue('stock_from').replace(",","");
        let to = this.getValue("stock_to").replace(",","");
        let rate = this.getValue("rate");
        if(from == ""){
            Swal.fire("Yêu cầu nhập Từ","","error");
            return;
        }
        if(to== ""){
            Swal.fire("Yêu cầu nhập Đến","","error");
            return;
        }
        if(parseInt(to) < parseInt(from)){
            Swal.fire("Yêu cầu Đến không được nhỏ hơn Từ","","error");
            return;
        }
        if(this.getValue("rate") == ""){
            Swal.fire("Yêu cầu nhập Tỷ lệ thưởng","","error");
            return;
        }
        $.ajax({
            url: laroute.route('admin.stock.config-bonus.referral.submitEdit'),
            method: "POST",
            data:{
                stock_bonus_refer_id:$("[name='stock_bonus_refer_id']").val(),
                rate:$("#modalEdit [name='rate']").val()
            },
            success: function (res) {
                console.log(res);
                if(!res.error){
                    Swal.fire(res.message,"","success").then(function(result){
                        if(result.value) window.location.reload(true);
                    });

                }
                else{
                    Swal.fire("Thêm thất bại","","error");
                }

            }
        });
    },
}
$(document).ready(function(){
    BonusHandler.init();
    });