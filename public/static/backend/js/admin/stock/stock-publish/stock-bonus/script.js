// todo: Format Number--------------
function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
// !--------Format number-----------
var StockBonusHandler = {
    init(){

    },
    showPopupEdit(id){        
        let data = LIST.find(e=>e.stock_bonus_id == id);
        $("[name='percent']").val(data["percent"]);
        $("[name='stock_bonus_id']").val(data["stock_bonus_id"]);
        $("#popup").modal("show");
    },
    validate(){
        $("#frmEditStockPublishBonus").validate({
            rules: {
                "percent": {
                    required: true,
                    min: 0,
                    max: 100

                }

            },
            messages: {
                "percent": {
                    required: "Yêu cầu nhập tỷ lệ %",
                    min: "Tỷ lệ % tối thiểu là 0",
                    max: "Tỷ lệ % tối đa là 100"
                }
            }
        });
        return $("#frmEditStockPublishBonus").valid();

    },

    submitEditStockPublish(){
        if(!this.validate()) return;
        $.ajax({
            url: laroute.route('admin.stock-publish.bonus.submitEdit'),
            method: "POST",
            dataType: "JSON",
            data:$(`#frmEditStockPublishBonus`).serialize(),

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    }
}