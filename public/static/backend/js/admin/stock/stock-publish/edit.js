// todo: Format Number--------------
function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
// !--------Format number-----------

$(document).ready(function(){
  StockPublishRespository.init();
});
var that = null;
var StockPublishRespository = {
    init(){
        // this.onSubmit();
        this.onInputAmount();
        this.onPublishStock();
        this.onBtnPublishPoup();
        this.onBtnSave();
        that = this;

    },
    onSubmit(){
       $("#btnPublish").on('click',function(){
           if(!that.validate("popup-stock-publish")){
               return;             
           }else{
               that.publishStock();
              
           }
           
       });

    },
    onInputAmount(e){
        $("[name='quantity']").on('keyup',function(){           
            that.handleTotalAmount(e);

        });
        $("[name='money']").on('keyup',function(){              
            that.handleTotalAmount(e);

        });
        
        
    },    
    handleTotalAmount(e){   
        // todo: Mõi lần nhập số lượng phát hành  
        let quantity = $("[name='quantity']") .val().replace(",","").trim();
        let money = $("[name='money']") .val().replace(",","").trim();
        quantity = quantity!==''?parseInt(quantity):0;
        money = money!==''?parseFloat(money):0;
        let total = quantity*money;
        $("#total_amount").val(total);
     
        

    },

    validate(frmForm){
        $("#"+frmForm).validate({
            rules: {
                quantity: {
                    required: true,
                    min:0
                    // minlength: 5
                },
                    money: {
                    required: true,                   
                },
                quantity_sale: {
                    required: true,                   
                }
               
            },
            messages: {
                quantity:{
                    required:'Số lượng phát hành không được rỗng',
                    min:"Số lượng phát hành phải lớn hơn 0"
                },
                quantity_sale: {
                    required: "Đã bán không được rỗng"
                },
                "money": {
                    required: "Giá phát hành không được rỗng",
                    min: "Giá phát hành phải lớn hơn 0"
                }
            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });
        return $("#"+frmForm).valid()

    },
    onPublishStock(){
        $("#btnPublish").on("click", function(){
            $('#popup_publish').modal('show');});

    },
    onBtnPublishPoup(){
        $("#btnPublishPopup").on("click", function(){
            const quantity = $("[name='quantity']").val().trim();            
            const price = $("[name='money']").val().trim();
            if(quantity == '') {
                Swal.fire('','Số lượng phát hành không dược rỗng','error');
                return;
             }else if (price == ''){
                Swal.fire('','Giá tiêu chuẩn không dược rỗng', 'error');
                return;
             } 
             that.releaseStock("popup-stock-publish");           
        //    that.validate("popup-stock-publish");
        });

    },
    releaseStock(){
        $.ajax({
            url: laroute.route('admin.stock.publish'),
            method: "POST",
            dataType: "JSON",
            data:$(`#popup-stock-publish`).serialize(),

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    },
    publishStock(frm){
        $.ajax({
            url: laroute.route('admin.stock.publish'),
            method: "POST",
            dataType: "JSON",
            data:$(`fsd`).serialize(),

            success: function (data) {
                if(data.error == 1){
                    swal('', data.message, "error");
                }
                else{
                    Swal('',data.message,'success').then(function(result){
                        if(result) window.location.reload();
                    });
                    
                   

                }

            },
            // error: function (res) {
            //     console.log('error la gi: ', res);
            //     var mess_error = '';
            //     jQuery.each(res.responseJSON.errors, function (key, val) {
            //         mess_error = mess_error.concat(val + '<br/>');
            //     });
            //     swal.fire('', mess_error, "error");
            // }

        });

    },
    onBtnSave(){
        $("#btnSave").on("click", function(){
            if(!that.validate("frm")) return;
            let is_active = $("[name='is_active']").prop("checked")
            is_active = is_active?1:0;
            $.ajax({
                url: laroute.route('admin.editStockPublish'),
                method: "POST",
                dataType: "JSON",
                data:$(`#frm`).serialize()+"&is_active="+is_active,
    
                success: function (data) {
                    if(data.error == 1){
                        swal('', data.message, "error");
                    }
                    else{
                        Swal('',data.message,'success').then(function(result){
                            window.location.href = laroute.route("admin.stock-publish.list") ;                           
                        });
                        
                       
    
                    }
    
                },
                // error: function (res) {
                //     console.log('error la gi: ', res);
                //     var mess_error = '';
                //     jQuery.each(res.responseJSON.errors, function (key, val) {
                //         mess_error = mess_error.concat(val + '<br/>');
                //     });
                //     swal.fire('', mess_error, "error");
                // }
    
            });
        });
    }


};