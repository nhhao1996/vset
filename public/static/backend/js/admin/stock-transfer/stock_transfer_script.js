// todo: Stock Transfer------------
var StockTransfer = {
    moveTab(type){      
        $(".tab").removeClass("color_button");
        $("#"+type).addClass("color_button");
        // const type = type=="traiphieu"?1:2;
        // const data = {product_category_id:type};

        // $.ajax({
        //     url: laroute.route('admin.stock-transfer.listAction'),
        //     method: "POST",
        //     dataType: "JSON",
        //     data: $('#form-product').serialize()+'&is_actived='+is_actived,
        //     success: function (data) {
        //         if (data.error == true) {
        //             swal('', data.message, "error");
        //         } else {
        //             swal('', data.message, "success").then(function () {
        //                 if (close == 1) {
        //                     window.location.href = laroute.route('admin.product');
        //                 } else {
        //                     window.location.href = laroute.route('admin.product.add');
        //                 }
        //             });
        //         }
        //     },
        //     error: function (res) {
        //         if (res.responseJSON != undefined) {
        //             var mess_error = '';
        //             $.map(res.responseJSON.errors, function (a) {
        //                 mess_error = mess_error.concat(a + '<br/>');
        //             });
        //             swal.fire('', mess_error, "error");
        //         }
        //     }
        // });
    },
    handleSubmit(){
          
        $.ajax({
            url: laroute.route('admin.stock-transfer.confirmTransfer'),
            method: "POST",
            dataType: "JSON",
            data:{
                stock_tranfer_contract_id:$("#stock_tranfer_contract_id").val()
            },
            success: function (data) {
                if (data.error == 1) {
                    swal('', data.message, "error");
                } else {
                    swal('', data.message, "success").then(function (res) {
                        if (res.value) {
                            window.location.reload(true);                            
                        }
                    });
                }
            },
            error: function (res) {
                if (res.responseJSON != undefined) {
                    var mess_error = '';
                    $.map(res.responseJSON.errors, function (a) {
                        mess_error = mess_error.concat(a + '<br/>');
                    });
                    swal.fire('', mess_error, "error");
                }
            }
        });
    },

    refresh: function () {
        $('input[name="search_keyword"]').val('');
        $('.m_selectpicker').val('').trigger('change');
        $('#created_at').val('');
        $(".btn-search").trigger("click");
    },
    search: function () {
        $(".btn-search").trigger("click");
    },
    notEnterInput: function (thi) {
        $(thi).val('');
    },

};
// !--------Stock Transfer--------

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}



$(document).ready(function () {
    var value = $('#product_category_id').val();
    // tiết kiệm
    if (value == 2){
        $('#withdraw_min_time').show();
    } else {
        $('#withdraw_min_time').hide();
    }

    $('#product_category_id').change(function () {
        var value = $('#product_category_id').val();
        // tiết kiệm
        if (value == 2){
            $('#withdraw_min_time').show();
        } else {
            $('#withdraw_min_time').hide();
        }
    })

    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });

    $('.is_default_display_check_term').change(function () {
        if ($('.is_default_display_check_term').is(':checked')) {
            $('.is_default_display_check').prop('checked',false);
            $('.is_default_display_check_term').prop('checked',true);
        };
    });
})



function dropzone(){
    Dropzone.options.dropzoneone = {
        paramName: 'file',
        maxFilesize: 500000, // MB
        maxFiles: 100,
        acceptedFiles: ".jpeg,.jpg,.png,",
        addRemoveLinks: true,
        // headers: {
        //     "X-CSRF-TOKEN": $('input[name=_token]').val()
        // },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.onBtnCancel();
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('customer-contract.upload-dropzone'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneone')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneone').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageShow').html();
                    tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                    tpl = tpl.replace(/{link_hidden}/g, response);
                    tpl = tpl.replace(/{numberImage}/g, numberImage);
                    numberImage++;
                    $('#upload-image').append(tpl);
                }
            });
            this.on('removedfile', function (file,response) {
                var name = file.upload.filename;
                $.ajax({
                    url: laroute.route('admin.service.delete-image'),
                    method: "POST",
                    data: {

                        filename: name
                    },
                    success: function () {
                        $("input[class='file_Name']").each(function () {
                            var $this = $(this);
                            if ($this.val() === name) {
                                $this.remove();
                            }
                        });

                    }
                });
            });
        }
    }

    Dropzone.options.dropzoneoneimage = {
        paramName: 'file',
        maxFilesize: 500000, // MB
        maxFiles: 100,
        acceptedFiles: ".jpeg,.jpg,.png,",
        addRemoveLinks: true,
        // headers: {
        //     "X-CSRF-TOKEN": $('input[name=_token]').val()
        // },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('customer-contract.upload-dropzone'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneoneimage')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneoneimage').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageShowMobile').html();
                    tpl = tpl.replace(/{link}/g, 'temp_upload/' + response);
                    tpl = tpl.replace(/{link_hidden}/g, response);
                    tpl = tpl.replace(/{numberImageMobile}/g, numberImageMobile);
                    numberImageMobile++;
                    $('#upload-image-mobile').append(tpl);
                }
            });
            this.on('removedfile', function (file,response) {
                var name = file.upload.filename;
                $.ajax({
                    url: laroute.route('admin.service.delete-image'),
                    method: "POST",
                    data: {

                        filename: name
                    },
                    success: function () {
                        $("input[class='file_Name']").each(function () {
                            var $this = $(this);
                            if ($this.val() === name) {
                                $this.remove();
                            }
                        });

                    }
                });
            });
        }
    }
}

$(document).ready(function () {

    // $('input[name="type_bonus_refer"]').change(function () {
    //     if ($(this).val() == 'money'){
    //         $('.voucher_refer').prop('disabled',true);
    //         $('.money_refer').prop('disabled',false);
    //     } else {
    //         $('.money_refer').prop('disabled',true);
    //         $('.voucher_refer').prop('disabled',false);
    //     }
    // });
});

function removeImage(e){
    $(e).closest('.image-show-child').remove();
}

function removeImageMobile(e){
    $(e).closest('.image-show-child-mobile').remove();
}

var linkSource = {

    deleteSearch: function(){
        $('#service_name').val('');
        $('#service_category').val('').trigger('change');
        linkSource.searchPopup(1);
    },

    removeService : function (obj,id,type) {
        $(obj).closest('tr').addClass('m-table__row--danger');
        swal({
            title: 'Thông báo',
            text: "Bạn có muốn xóa dịch vụ không?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Xóa',
            cancelButtonText: 'Hủy',
            onClose: function () {
                $(obj).closest('tr').removeClass('m-table__row--danger');
            }
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: laroute.route("admin.product.deleteService"),
                    method: "POST",
                    data: {
                        service_id : id
                    },
                    success: function (res) {
                        $('.body_tr_register #tr_'+id).remove();
                    }
                });
            }
        });
    },
    onBtnCancel(){
        $("#btnCancel").on('change', function(){
            alert('hello');

        });


    }

}