$('#autotable').PioTable({
    baseUrl: laroute.route('admin.transfer-request.list')
});

$(document).ready(function () {
    $(".select").select2();
    $(".daterange-picker").daterangepicker({
        autoUpdateInput: false,
        autoApply: true,
        buttonClasses: "m-btn btn",
        applyClass: "btn-primary",
        cancelClass: "btn-danger",
        locale: {
            format: 'DD/MM/YYYY',
            "applyLabel": "Đồng ý",
            "cancelLabel": "Thoát",
            "customRangeLabel": "Tùy chọn ngày",
            daysOfWeek: [
                "CN",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7"
            ],
            "monthNames": [
                "Tháng 1 năm",
                "Tháng 2 năm",
                "Tháng 3 năm",
                "Tháng 4 năm",
                "Tháng 5 năm",
                "Tháng 6 năm",
                "Tháng 7 năm",
                "Tháng 8 năm",
                "Tháng 9 năm",
                "Tháng 10 năm",
                "Tháng 11 năm",
                "Tháng 12 năm"
            ],
            "firstDay": 1
        },
        // ranges: {
        //     'Hôm nay': [moment(), moment()],
        //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
        //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
        //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
        //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
        //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        // }
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
    });

})
var TransferRequest = {
    confirmFail : function (id) {
        switch (id) {
            case 1: swal('Thông tin nhà đầu tư chuyển không đủ để xác nhận','','error'); break;
            case 2: swal('Thông tin nhà đầu tư nhận không đủ để xác nhận','','error'); break;
            case 3: swal('Số cổ phiếu của nhà đầu tư chuyển không đủ để chuyển','','error'); break;
            case 4: swal('Số tiền của nhà đầu tư chuyển không đủ để trả phí','','error'); break;
        }
    },
    changeStatus : function (status) {
        var text = '';
        if (status == 'new') {
            text = 'Bạn có chắc muốn xác nhận yêu cầu chuyển nhượng';
        } else if (status == 'cancel') {
            text = 'Bạn có chắc muốn huỷ yêu cầu chuyển nhượng';
        }

        if (status != 'cancel') {
            swal({
                title: "Thông báo",
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Xác nhận",
                cancelButtonText: "Hủy",
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.transfer-request.change-status'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: $('#transfer-request-detail').serialize()+'&process_status='+status,
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload(true);
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload(true);
                                });
                            }
                        },
                    });
                }
            });
        } else {
            swal.fire({
                title: 'Không xác nhận yêu cầu',
                type: 'question',

                html:
                    '<input id="reason_vi" class="swal2-input" placeholder="Nhập lý do không xác nhận (VI)">' +
                    '<input id="reason_en" class="swal2-input" placeholder="Nhập lý do không xác nhận (EN)">',
                preConfirm: () => {
                    if($('#reason_vi').val() == ''){
                        swal.showValidationError("Yêu cầu nhập lý do không xác nhận (VI)");
                    } else if($('#reason_en').val() == ''){
                        swal.showValidationError("Yêu cầu nhập lý do không xác nhận (EN)");
                    } else if($('#reason_vi').val().length > 255){
                        swal.showValidationError("Lý do không xác nhận (VI) vượt quá 255 ký tự");
                    } else if($('#reason_en').val().length > 255){
                        swal.showValidationError("Lý do không xác nhận (EN) vượt quá 255 ký tự");
                    }
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.transfer-request.change-status'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            process_status:status,
                            stock_order_id : $('[name="stock_order_id"]').val(),
                            reason_vi: $('#reason_vi').val(),
                            reason_en: $('#reason_en').val(),
                        },
                        success: function (res) {
                            console.log(res);
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload(true);
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload(true);
                                });
                            }
                        },
                    });
                }
            })
        }
    }
}