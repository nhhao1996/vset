//== Class Definition
var SnippetLogin = function() {

    var login = $('#m_login');

    var showErrorMsg = function(form, type, msg) {

        console.log(msg);

        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert"> '+msg+'\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    }

    //== Private Functions

    var displaySignUpForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signin');

        login.addClass('m-login--signup');
        // login.find('.m-login__signup').animateClass('flipInX animated');
    }

    var displaySignInForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        // login.find('.m-login__signin').animateClass('flipInX animated');
    }

    var displayForgetPasswordForm = function() {
        login.removeClass('m-login--signin');
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    }

    var handleFormSwitch = function() {
        $('#m_login_forget_password').click(function(e) {
            e.preventDefault();
            displayForgetPasswordForm();
        });

        $('#m_login_forget_password_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });

        $('#m_login_signup').click(function(e) {
            e.preventDefault();
            displaySignUpForm();
        });

        $('#m_login_signup_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });
    }

    var handleSignInFormSubmit = function() {
        $('#m_login_signin_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    user_name: {
                        required: true,
                        // email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages:{
                    user_name: {
                        required: 'Yêu cầu nhập tài khoản đăng nhập',
                        // email: 'Tài khoản email sai định dạng'
                    },
                    password: {
                        required: 'Yêu cầu nhập mật khẩu đăng nhập'
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "user_name") {
                        error.insertAfter(".error_user_name");
                    } else if (element.attr("name") == "password") {
                        error.insertAfter(".error_password");
                    } else {
                        error.insertAfter(element);
                    }
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function(response, status, xhr, $form) {
                	if (response.error)
            		{
	                	// similate 2s delay
	                	setTimeout(function() {
		                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
		                    showErrorMsg(form, 'danger', response.message);
	                    }, 2000);
            		}
                	else
            		{
                		window.location = response.url;
            		}
                }
            });
        });
    }

    var handleSignUpFormSubmit = function() {
        $('#m_login_signup_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true
                    },
                    agree: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function(response, status, xhr, $form) {
                	// similate 2s delay
                	setTimeout(function() {
	                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
	                    form.clearForm();
	                    form.validate().resetForm();

	                    // display signup form
	                    displaySignInForm();
	                    var signInForm = login.find('.m-login__signin form');
	                    signInForm.clearForm();
	                    signInForm.validate().resetForm();

	                    showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
	                }, 2000);
                }
            });
        });
    }

    var handleForgetPasswordFormSubmit = function() {
        $('#m_login_forget_password_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: 'Yêu cầu nhập địa chỉ email',
                        email: 'Email sai định dạng'
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "email") {
                        error.insertAfter(".error-email");
                    } else {
                        error.insertAfter(element);
                    }
                },
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: laroute.route('sendEmailResetPassword'),
                // type: 'POST',
                success: function(response, status, xhr, $form) { 
                	// similate 2s delay
                	setTimeout(function() {
                		btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove 
	                    form.clearForm(); // clear form
	                    form.validate().resetForm(); // reset validation states

	                    // display signup form
	                    displaySignInForm();
	                    var signInForm = login.find('.m-login__signin form');
	                    signInForm.clearForm();
	                    signInForm.validate().resetForm();

	                    if (response.error == 1) {
                            showErrorMsg(signInForm, 'error', response.message);
                        } else {
                            showErrorMsg(signInForm, 'success', 'Hệ thống đã gửi email xác nhận. Vui lòng kiểm tra lại để tiến hành đổi mật khẩu mới!');
                        }

                	}, 2000);
                }
            });
        });
    }

    //== Public Functions
    return {
        // public functions
        init: function() {
            handleFormSwitch();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
            handleForgetPasswordFormSubmit();
        }
    };
}();

var forgetPassword = {
    showErrorMsg : function(form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert"> '+msg+'\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        // alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    },
    changePassword : function (token) {
        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}$/i.test(value);
        });
        $('#form-change-password').validate({
            rules: {
                password: {
                    required: true,
                    validatePassword : true
                },
                re_password: {
                    required: true,
                    equalTo: ".new-password"
                }
            },
            messages : {
                password: {
                    required: 'Yêu cầu nhập mật khẩu mới',
                    validatePassword : 'Hãy nhập password từ 8 đến 20 ký tự bao gồm chữ và số'
                },
                re_password: {
                    required: 'Yêu cầu nhập mật khẩu xác nhận',
                    equalTo : 'Mật khẩu xác nhận không giống với mật khẩu mới'
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter("."+element.attr("name")+"-error");
            },

        });

        if (!$('#form-change-password').valid()) {
            return false;
        } else {
            $.ajax({
                url: laroute.route('submit-reset-password'),
                method: "POST",
                data: $('#form-change-password').serialize(),
                success: function (res) {
                    if (res.error == false) {
                        // swal.fire(res.message, "", "success").then(function (result) {
                        //     window.location.href = laroute.route('login');
                        // });
                        var changPassword = $('.change-password-view')
                        var signInForm = changPassword.find('#form-change-password');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();
                        console.log(res.message);
                        forgetPassword.showErrorMsg(signInForm, 'success', res.message);
                        setTimeout(function(){
                            window.location.href = laroute.route('login');
                        },3000);
                    }else {
                        swal.fire(res.message, "", "error");
                    }
                }
            });
        }
    }
}

//== Class Initialization
jQuery(document).ready(function() {
    SnippetLogin.init();
});