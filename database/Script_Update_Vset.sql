ALTER TABLE  `config` ADD COLUMN `value_vi` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `key`;

ALTER TABLE  `config` ADD COLUMN `value_en` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `value_vi`;

ALTER TABLE  `config_notification` MODIFY COLUMN `send_type` enum('immediately','before','after','in_time') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'immediately' COMMENT 'Loại gửi' AFTER `display_sort`;

ALTER TABLE  `customer_code` MODIFY COLUMN `type` enum('otp_verify','otp_pass','email_verify','bank_verify','ic_verify','wd_interest_verify','wd_saving_verify','wd_commission_verify','order_verify','change_phone_login','change_info','confirm_withraw','withdraw_saving','wd_bond_verify','update_info','register_update','extend_contract','withdraw_stock_verify','withdraw_dividend_verify','publish_order','market_order','transfer_order','market_sell') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại CODE: verify_phone, verify_email,...' AFTER `customer_id`;

ALTER TABLE  `customer_contract` ADD COLUMN `staff_id` int(11) NULL DEFAULT NULL COMMENT 'id nhân viên chăm sóc' AFTER `commission`;

ALTER TABLE  `customer_contract` ADD COLUMN `is_fully_withdraw` int(11) NULL DEFAULT NULL COMMENT 'Đã rút gốc' AFTER `is_active`;

ALTER TABLE  `customer_contract` ADD COLUMN `is_end` tinyint(1) NULL DEFAULT 0 COMMENT 'Kiểm tra hợp đồng yêu cầu kết thúc' AFTER `is_fully_withdraw`;

ALTER TABLE  `customer_contract_interest_by_month` MODIFY COLUMN `interest_rate_by_month` decimal(16, 7) NOT NULL COMMENT 'Tỉ lệ lãi xuất theo tháng (interest_rate/12)*total_amount.' AFTER `interest_month`;

ALTER TABLE  `customer_contract_interest_by_month` MODIFY COLUMN `withdraw_date_planning` datetime(0) NOT NULL COMMENT 'Ngày rút tiền theo kế hoạch:Last withdraw_request + withdraw _interest_timeNếu Last withdraw_request = null thì lấy customer_contract.customer_contract_start_date thế vô.' AFTER `interest_banlance_amount`;

ALTER TABLE  `customer_contract_interest_by_time` MODIFY COLUMN `interest_rate_by_time` decimal(16, 12) NOT NULL COMMENT 'Tỉ lệ lãi xuất theo thời điểm' AFTER `interest_time`;

ALTER TABLE  `customers` MODIFY COLUMN `customer_group_id` int(11) NULL DEFAULT 1 COMMENT 'Nhóm khách hàng ' AFTER `customer_code`;

ALTER TABLE  `customers` ADD COLUMN `utm_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Nguồn đăng ký' AFTER `customer_group_id`;

ALTER TABLE  `customers` ADD COLUMN `is_referal` tinyint(1) NULL DEFAULT NULL COMMENT 'Kiểm tra đăng ký từ link share' AFTER `is_test`;

ALTER TABLE  `customers` ADD COLUMN `customer_journey_id` int(11) NULL DEFAULT 1 COMMENT 'Hành trình khách hàng' AFTER `is_referal`;

ALTER TABLE  `jobs_email_log` MODIFY COLUMN `email_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `obj_id`;

ALTER TABLE  `notification` MODIFY COLUMN `user_id` int(11) NULL DEFAULT NULL COMMENT 'ID user' AFTER `notification_detail_id`;

ALTER TABLE  `notification` MODIFY COLUMN `notification_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg' COMMENT 'Avatar của thông báo' AFTER `user_id`;

ALTER TABLE  `notification` ADD COLUMN `notification_title_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Title' AFTER `notification_avatar`;

ALTER TABLE  `notification` ADD COLUMN `notification_title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'title en' AFTER `notification_title_vi`;

ALTER TABLE  `notification` ADD COLUMN `notification_message_vi` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Nội dung thông báo' AFTER `notification_title_en`;

ALTER TABLE  `notification` ADD COLUMN `notification_message_en` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Nội dung thông báo en' AFTER `notification_message_vi`;

ALTER TABLE  `notification_detail` ADD COLUMN `content_vi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Noi dung thong bao' AFTER `background`;

ALTER TABLE  `notification_detail` ADD COLUMN `content_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Nội dung thông báo EN' AFTER `content_vi`;

ALTER TABLE  `notification_detail` ADD COLUMN `action_name_vi` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên hiển thị của action' AFTER `content_en`;

ALTER TABLE  `notification_detail` ADD COLUMN `action_name_en` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên hiển thị của action en' AFTER `action_name_vi`;

ALTER TABLE  `notification_template` ADD COLUMN `title_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'tiêu đề thông báo' AFTER `notification_type_id`;

ALTER TABLE  `notification_template` ADD COLUMN `title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL AFTER `title_vi`;

ALTER TABLE  `notification_template` ADD COLUMN `description_vi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'thông tin nổi bật của thông báo' AFTER `title_short`;

ALTER TABLE  `notification_template` ADD COLUMN `description_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL AFTER `description_vi`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `title_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tiêu đề (VI)' AFTER `key`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tiêu đề (EN)' AFTER `title_vi`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `message_vi` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung tin nhắn (VI)' AFTER `title_en`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `message_en` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Nội dung tin nhắn (EN)' AFTER `message_vi`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `detail_content_vi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Nội dung detail (VI)' AFTER `detail_background`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `detail_content_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Nội dung detail (EN)' AFTER `detail_content_vi`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `detail_action_name_vi` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên button tương tác' AFTER `detail_content_en`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `detail_action_name_en` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên button tương tác' AFTER `detail_action_name_vi`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `type_bonus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'money : thưởng tiền, voucher : thưởng dịch vụ' AFTER `detail_action_params`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `money` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Thưởng tiền' AFTER `type_bonus`;

ALTER TABLE  `notification_template_auto` ADD COLUMN `service_id` int(11) NULL DEFAULT NULL COMMENT 'Thưởng dịch vụ' AFTER `money`;

ALTER TABLE  `orders` ADD COLUMN `contract_code_extend` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã hợp đồng cũ yêu cầu gia hạn' AFTER `is_extend`;

ALTER TABLE  `orders` ADD COLUMN `staff_id` int(11) NULL DEFAULT NULL COMMENT 'id nhân viên chăm sóc' AFTER `contract_code_extend`;

ALTER TABLE  `orders` ADD COLUMN `reason_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'lý do vi' AFTER `staff_id`;

ALTER TABLE  `orders` ADD COLUMN `reason_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'lý do en' AFTER `reason_vi`;

ALTER TABLE  `otp_log` MODIFY COLUMN `otp_type` enum('register','forget_password','update_bank','update_ic','withdraw_interest','withdraw_saving','withdraw_commission','order_verify','change_password','extend_contract','change_phone_login','change_info_success','confirm_withraw','withdraw_bond','update_info','register_update','withdraw_stock','withdraw_dividend','publish_order','market_order','transfer_order','market_sell') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `otp`;

ALTER TABLE  `pages` MODIFY COLUMN `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên trang' AFTER `id`;

ALTER TABLE  `pages` MODIFY COLUMN `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Route của trang' AFTER `name`;

ALTER TABLE  `products` ADD COLUMN `product_avatar_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình đại diện gói (vi)' AFTER `description_en`;

ALTER TABLE  `products` ADD COLUMN `product_avatar_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình đại diện gói (en)' AFTER `product_avatar_vi`;

ALTER TABLE  `products` ADD COLUMN `product_avatar_mobile_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình đại diện gói (mobile) (vi)' AFTER `product_avatar_en`;

ALTER TABLE  `products` ADD COLUMN `product_avatar_mobile_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình đại diện gói (mobile) (en)' AFTER `product_avatar_mobile_vi`;

ALTER TABLE  `products` ADD COLUMN `product_image_detail_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình chi tiết gói (vi)' AFTER `product_avatar_mobile_en`;

ALTER TABLE  `products` ADD COLUMN `product_image_detail_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình chi tiết gói (en)' AFTER `product_image_detail_vi`;

ALTER TABLE  `products` ADD COLUMN `product_image_detail_mobile_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình chi tiết gói (mobile) (vi)' AFTER `product_image_detail_en`;

ALTER TABLE  `products` ADD COLUMN `product_image_detail_mobile_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hình chi tiết gói (mobile) (en)' AFTER `product_image_detail_mobile_vi`;

ALTER TABLE  `products` ADD COLUMN `withraw_min_bonus` decimal(16, 3) NULL DEFAULT NULL AFTER `withdraw_fee_interest_rate`;

ALTER TABLE  `products` ADD COLUMN `extended_commission_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'hoa hồng khi gia hạn (của chính tk đó))' AFTER `type_refer_commission`;

ALTER TABLE  `products` ADD COLUMN `month_extend` int(11) NULL DEFAULT NULL COMMENT 'Số tháng còn lại để được báo gia hạn hợp đồng' AFTER `extended_commission_value`;

ALTER TABLE  `products` ADD COLUMN `bonus_extend` decimal(16, 3) NULL DEFAULT 0 COMMENT 'Thưởng thêm khi gian hạn họp đồng' AFTER `updated_by`;

ALTER TABLE  `service_categories` ADD COLUMN `name_vi` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL AFTER `service_category_id`;

ALTER TABLE  `service_categories` ADD COLUMN `slug_vi` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL AFTER `name_en`;

ALTER TABLE  `service_categories` ADD COLUMN `description_vi` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL AFTER `slug_en`;

ALTER TABLE  `service_serial` ADD COLUMN `customer_id` int(11) NULL DEFAULT NULL AFTER `service_voucher_template`;

ALTER TABLE  `service_serial` ADD COLUMN `type` enum('buy','gift') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'buy' COMMENT 'Loại : buy là mua , gift là quà tặng' AFTER `customer_id`;

ALTER TABLE  `services` ADD COLUMN `service_name_vi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `service_id`;

ALTER TABLE  `sms_log` MODIFY COLUMN `sms_type` enum('birthday','new_appointment','cancel_appointment','remind_appointment','paysuccess','new_customer','service_card_nearly_expired','service_card_over_number_used','service_card_expires','change_info','confirm_withraw','confirm_extend') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Loại tin nhắn' AFTER `sms_status`;

ALTER TABLE  `withdraw_request_group` MODIFY COLUMN `withdraw_request_group_type` enum('interest','bonus','saving','bond') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'interest' AFTER `withdraw_request_group_id`;

CREATE TABLE  `account_statement`  (
  `account_statement_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NULL DEFAULT NULL COMMENT 'Id nhà đầu tư',
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Họ và tên',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Số điện thoại',
  `date_satement` datetime(0) NULL DEFAULT NULL COMMENT 'Ngày sao kê',
  PRIMARY KEY (`account_statement_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `account_statement_map_contract`  (
  `account_statement_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_statement_id` int(11) NULL DEFAULT NULL,
  `customer_contract_id` int(11) NULL DEFAULT NULL COMMENT 'Id hợp đồng',
  `customer_contract_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Mã hợp đồng',
  `total_amount` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền đầu tư',
  `total_before` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền lãi các tháng trước',
  `money_in` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền lãi ',
  `money_out` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền lãi đã sử dụng',
  PRIMARY KEY (`account_statement_map_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `appendix_contract`  (
  `appendix_contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_contract_id` int(11) NULL DEFAULT NULL COMMENT 'id hợp đồng',
  `customers_change_id` int(11) NULL DEFAULT NULL COMMENT 'id yêu cầu thay đổi',
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'đường dẫn chưa file phụ lục',
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`appendix_contract_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `appendix_contract_map`  (
  `appendix_contract_map_id` int(255) NOT NULL AUTO_INCREMENT,
  `appendix_contract_id` int(11) NULL DEFAULT NULL COMMENT 'Id bảng appendix_contract',
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Đường dẫn file',
  `created_at` date NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`appendix_contract_map_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `banner`  (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên banner',
  `img_desktop_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ảnh tiếng việt desktop',
  `img_desktop_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ảnh tiếng anh desktop',
  `img_mobile_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ảnh tiếng việt mobile',
  `img_mobile_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ảnh tiếng anh mobile',
  `from` datetime(0) NULL DEFAULT NULL COMMENT 'thời gian bắt đầu',
  `to` datetime(0) NULL DEFAULT NULL COMMENT 'thời gian kết thúc',
  `is_active` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `config_module`  (
  `config_module_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `route_site` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_active` tinyint(1) NULL DEFAULT NULL COMMENT 'Kích hoạt',
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `update_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`config_module_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Danh sách các module sẽ bị ẩn' ROW_FORMAT = Dynamic;

CREATE TABLE  `customer_contract_archive`  (
  `customer_contract_archive_id` int(16) NOT NULL AUTO_INCREMENT,
  `customer_contract_origin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Hợp đồng gốc',
  `customer_contract_from` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Hợp đồng gia hạn đằng trước đó',
  `customer_contract_extended` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Hợp đồng gia hạn',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`customer_contract_archive_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `customer_journey`  (
  `customer_journey_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên hành trình',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'Mô tả',
  `is_active` tinyint(1) NULL DEFAULT 1 COMMENT 'Trạng thái',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`customer_journey_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `customers_change`  (
  `customer_change_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `type` enum('before','after') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Loại hiển thị',
  `full_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Họ và Tên',
  `birthday` date NULL DEFAULT NULL COMMENT 'Ngày sinh',
  `gender` enum('male','female','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Giới tính',
  `phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'số điện thoại1_tài khoản đăng nhập',
  `phone2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'số điện thoại2_Thông tin liên hệ',
  `phone_verified` tinyint(1) NULL DEFAULT 0 COMMENT 'Đã xác thực số điện thoại',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'email1',
  `email_verified` tinyint(1) NULL DEFAULT 0 COMMENT 'Đã xác thực email',
  `password` char(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ic_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'số chứng minh nhân dân',
  `ic_date` date NULL DEFAULT NULL COMMENT 'Ngày cấp chứng minh nhân dân',
  `ic_place` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Nơi cấp minh nhân dân',
  `ic_front_image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Hình chứng minh nhân dân mặt trước',
  `ic_back_image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Hình chứng minh nhân dân mặt sau',
  `contact_province_id` int(11) NULL DEFAULT NULL COMMENT 'Liên hệ _ Tỉnh/Thành phố',
  `contact_district_id` int(11) NULL DEFAULT NULL COMMENT 'Liên hệ _ Quận/Huyện',
  `contact_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Liên hệ _ Đia chi',
  `residence_province_id` int(11) NULL DEFAULT NULL COMMENT 'Thường trú _ Tỉnh/Thành phố',
  `residence_district_id` int(11) NULL DEFAULT NULL COMMENT 'Thường trú _ Quận/Huyện',
  `residence_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Thường trú _ Đia chi',
  `bank_id` int(11) NULL DEFAULT NULL COMMENT 'Ngân hàng',
  `bank_account_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Số tài khoản ngân hàng',
  `bank_account_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên tài khoản ngân hàng',
  `bank_account_branch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Chi nhánh ngân hàng',
  `customer_source_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Nguồn khách hàng',
  `customer_refer_id` int(11) NULL DEFAULT NULL COMMENT 'mã khách giới thiệu',
  `customer_refer_code` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `customer_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Link avatar',
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Ghi chú',
  `zalo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'zalo',
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `facebook` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'facebook',
  `customer_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã khách hàng',
  `customer_group_id` int(11) NULL DEFAULT NULL COMMENT 'Nhóm khách hàng ',
  `utm_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Nguồn đăng ký',
  `point` double(16, 2) NULL DEFAULT 0 COMMENT 'Điểm ',
  `member_level_id` int(11) NULL DEFAULT 1 COMMENT 'Mức độ thành viên',
  `FbId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'FacebookID',
  `ZaloId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'ZaloID',
  `is_updated` tinyint(1) NULL DEFAULT 0 COMMENT 'Cập nhật thông tin khi đăng ký tài khoản trên app',
  `site_id` int(11) NULL DEFAULT NULL,
  `branch_id` int(11) NULL DEFAULT NULL,
  `postcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `date_last_visit` datetime(0) NULL DEFAULT NULL COMMENT 'ngày đến gần nhất',
  `is_logout` tinyint(1) NULL DEFAULT 0 COMMENT 'đánh dấu 1 nếu muốn user logut',
  `is_actived` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'Đánh dấu hoạt động',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Đánh dấu xóa ',
  `created_by` int(11) NOT NULL COMMENT 'Người tạo',
  `updated_by` int(11) NOT NULL COMMENT 'Ngừoi thay đổi gần nhất',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT 'Thời gian tạo',
  `is_test` tinyint(1) NULL DEFAULT 0 COMMENT 'Tài khoản test',
  `is_referal` tinyint(1) NULL DEFAULT NULL,
  `status` enum('new','success','cancel') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Trạng thái xét duyệt',
  `reason_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Lý do huy',
  `reason_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `customer_journey_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`customer_change_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Danh sách thông tin nhà đầu tư' ROW_FORMAT = Dynamic;

CREATE TABLE  `group_customer`  (
  `group_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên nhóm khách hàng',
  `is_active` tinyint(1) NULL DEFAULT 1 COMMENT 'Trạng thái nhóm',
  `is_deleted` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`group_customer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `group_customer_map_customer`  (
  `group_customer_map_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_customer_id` int(11) NULL DEFAULT NULL COMMENT 'id nhóm khách hàng',
  `customer_id` int(11) NULL DEFAULT NULL COMMENT 'id khách hàng',
  PRIMARY KEY (`group_customer_map_customer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `group_staff`  (
  `group_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên nhóm sale',
  `type` enum('all','group','detail') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'all' COMMENT 'Loại quyền. All : xem tất cả , group : Xem theo group khách hàng, detail : Xem chi tiết theo từng khách hàng',
  `is_active` tinyint(1) NULL DEFAULT NULL COMMENT 'Trạng thái nhóm',
  `is_deleted` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`group_staff_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `group_staff_map_customer`  (
  `group_staff_map_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_staff_id` int(11) NULL DEFAULT NULL COMMENT 'id nhóm sale',
  `obj_id` int(11) NULL DEFAULT NULL COMMENT 'ID nhóm khi type của nhóm sale là group, id là id của khách hàng khi type của nhóm sale là detail',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`group_staff_map_customer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `group_staff_map_staff`  (
  `group_staff_map_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_staff_id` int(11) NULL DEFAULT NULL COMMENT 'id nhóm sale',
  `staff_id` int(11) NULL DEFAULT NULL COMMENT 'id sale',
  PRIMARY KEY (`group_staff_map_staff_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `link_source`  (
  `link_source_id` int(11) NOT NULL AUTO_INCREMENT,
  `source` enum('default','facebook','zalo','twitter','wechat') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Chia sẽ từ nguồn',
  `source_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên nguồn',
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`link_source_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng quản lý danh sách nguồn đăng ký ' ROW_FORMAT = Dynamic;

CREATE TABLE  `link_source_map_customer`  (
  `map_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_source_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`map_customer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng quản lý danh sách dịch vụ tặng cho nhà đầu tư đăng ký' ROW_FORMAT = Dynamic;

CREATE TABLE  `link_source_map_refer`  (
  `map_refer_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_source_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`map_refer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng quản lý danh sách dịch vụ tặng thưởng khi nhà đầu tư giới thiệu' ROW_FORMAT = Dynamic;

CREATE TABLE  `notification_customer_group`  (
  `notification_customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_template_id` int(11) NULL DEFAULT NULL,
  `member_level_id` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`notification_customer_group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `notification_reward`  (
  `notification_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_template_id` int(11) NULL DEFAULT NULL,
  `key_reward` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`notification_reward_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `notification_reward_map`  (
  `notification_reward_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_reward_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`notification_reward_map_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `orders_fail`  (
  `order_id` int(16) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `customer_id` int(16) NOT NULL,
  `product_id` int(11) NULL DEFAULT NULL COMMENT 'mã gói',
  `quantity` int(11) NULL DEFAULT NULL COMMENT 'số lượng gói',
  `price_standard` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Giá trị gói',
  `interest_rate_standard` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tỉ lệ lãi xuất chuẩn ',
  `term_time_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1-> có kỳ hạn , 0 - > không có kỳ hạn ',
  `investment_time_id` int(11) NULL DEFAULT NULL COMMENT 'Kỳ hạn đầu tư ->là id',
  `withdraw_interest_time_id` int(11) NULL DEFAULT NULL COMMENT 'Kỳ hạn rút lãi -> là id',
  `interest_rate` decimal(16, 3) NOT NULL COMMENT 'Lãi xuất',
  `commission_rate` int(11) NOT NULL COMMENT 'Tỉ lệ hoa hồng ',
  `month_interest` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng lãi xuất hàng tháng',
  `total_interest` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng lãi xuất',
  `bonus_rate` decimal(16, 3) NOT NULL COMMENT 'Tỉ lệ tặng thêm',
  `total` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền :price_standard x quantity',
  `bonus` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền tặng : price_standard x bonus_rate x quantity',
  `total_amount` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng thành tiền = total + tiền tặng',
  `payment_method_id` int(16) NULL DEFAULT NULL COMMENT 'Hình thức thanh toán',
  `process_status` enum('not_call','confirmed','ordercomplete','ordercancle','paysuccess','payfail','new','pay-half') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'new' COMMENT 'Trạng thái đơn hàng',
  `payment_date` datetime(0) NULL DEFAULT NULL COMMENT 'Ngày thanh toán: (khi order status chuyển về payment_success thì cập nhật payment date)',
  `customer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Họ và Tên nhà đầu tư',
  `customer_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Sđt nhà đầu tư',
  `customer_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Email nhà đầu tư',
  `customer_residence_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ thường trú nhà đầu tư',
  `customer_ic_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'số chứng minh nhân dân khách hàng',
  `refer_id` int(11) NULL DEFAULT NULL COMMENT 'Mã người môi giới',
  `commission` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền hoa hồng : price_standard x commission_rate x quantity',
  `order_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mô tả cú pháp chuyển tiền nếu có',
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `branch_id` int(11) NULL DEFAULT NULL,
  `tranport_charge` int(11) NULL DEFAULT NULL,
  `discount` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tổng tiền giảm giá',
  `customer_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `order_source_id` int(11) NULL DEFAULT 1,
  `transport_id` int(11) NULL DEFAULT NULL,
  `voucher_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `discount_member` decimal(16, 0) NULL DEFAULT NULL,
  `is_apply` tinyint(4) NULL DEFAULT 0 COMMENT 'Đơn hàng từ app đã chuyển chi nhánh chưa',
  `total_saving` decimal(16, 3) NULL DEFAULT NULL COMMENT 'ETL - Giảm giá khuyến mãi',
  `total_tax` decimal(16, 3) NULL DEFAULT NULL,
  `is_extend` tinyint(1) NULL DEFAULT NULL,
  `contract_code_extend` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Mã hợp đồng cũ yêu cầu gia hạn',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `product_bonus_service`  (
  `product_bonus_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL COMMENT 'id gói',
  `service_id` int(11) NULL DEFAULT NULL COMMENT 'id dịch vụ',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`product_bonus_service_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `product_bonus_service_config`  (
  `product_bonus_service_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL,
  `is_active` tinyint(1) NULL DEFAULT NULL,
  `start` datetime(0) NULL DEFAULT NULL COMMENT 'Thời gian bắt đầu',
  `end` datetime(0) NULL DEFAULT NULL COMMENT 'Thời gian kết thúc',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`product_bonus_service_config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `product_bonus_service_log`  (
  `product_bonus_service_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL COMMENT 'id đơn hàng',
  `service_id` int(11) NULL DEFAULT NULL COMMENT 'id dịch vụ',
  `service_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`product_bonus_service_log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `product_images`  (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('mobile','desktop') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'desktop',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_image_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `refer_source`  (
  `refer_source_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id nguồn',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'default , facebook, zalo, twitter, wechat',
  `source_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên nguồn',
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`refer_source_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `refer_source_customer`  (
  `refer_source_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `refer_source_id` int(11) NULL DEFAULT NULL COMMENT 'id nguồn giới thiệu',
  `type` enum('register','refer') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'người đăng ký hay người giới thiệu',
  `type_bonus` enum('money','voucher') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'loại thưởng',
  `money` decimal(16, 3) NULL DEFAULT NULL COMMENT 'tiền thưởng',
  `is_active` tinyint(1) NULL DEFAULT NULL COMMENT 'trạng thái',
  `start` datetime(0) NULL DEFAULT NULL COMMENT 'thời gian bắt đầu',
  `end` datetime(0) NULL DEFAULT NULL COMMENT 'thời gian kết thúc',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`refer_source_customer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `refer_source_map_service`  (
  `refer_source_map_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `refer_source_customer_id` int(11) NULL DEFAULT NULL COMMENT 'Id bảng refer_source Nguồn',
  `service_id` int(11) NULL DEFAULT NULL COMMENT 'id dịch vụ',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`refer_source_map_service_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `sms_message`  (
  `sms_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tiêu đề từng key',
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key của message',
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'nội dung tin nhắn',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`sms_message_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE  `wallet`  (
  `wallet_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_contract_id` int(11) NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL COMMENT 'id khách hàng',
  `type` enum('bonus','register','refer') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Loại thưởng',
  `total_money` decimal(16, 3) NULL DEFAULT NULL COMMENT 'Tiền thưởng',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`wallet_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `config_notification`;
CREATE TABLE `config_notification` (
  `key` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Key để phân biệt notification',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên nội dung cấu hình',
  `config_notification_group_id` int(11) NOT NULL COMMENT 'Nhóm cấu hình',
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Active cho phép gửi',
  `display_sort` int(11) NOT NULL DEFAULT 100 COMMENT 'Vị trí hiển thị trong cùng 1 nhóm',
  `send_type` enum('immediately','before','after','in_time') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'immediately' COMMENT 'Loại gửi',
  `schedule_unit` enum('day','hour','minute') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Đơn vị cộng thêm. Dùng hàm Cartbon->add(2, ''hour'') để tính thời gian chính xác',
  `value` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Giá trị sẽ + - hoặc đúng thời điểm',
  `created_at` datetime DEFAULT NULL COMMENT 'Ngày tạo',
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày cạp nhật',
  `updated_by` int(11) DEFAULT NULL COMMENT 'Người cập nhật',
  PRIMARY KEY (`key`) USING BTREE,
  KEY `config_notification_group_id` (`config_notification_group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='Cấu hình push notification tự động';

INSERT INTO `config_notification` (`key`, `name`, `config_notification_group_id`, `is_active`, `display_sort`, `send_type`, `schedule_unit`, `value`, `created_at`, `updated_at`, `updated_by`) VALUES
('contract_extension',	'Gia hạn hợp đồng thành công',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('custom_contract_confirm',	'Xác nhận lãi hàng tháng',	0,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 14:02:19',	NULL),
('customer_birthday',	'Chúc mừng sinh nhật khách hàng',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-11-05 14:53:58',	25),
('customer_ranking',	'Chúc mừng thăng hạng',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:55:55',	NULL),
('customer_W',	'Chúc mừng khách hàng mới',	1,	0,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-25 17:34:47',	NULL),
('happy_new_year',	'Chúc mừng năm mới',	1,	1,	100,	'immediately',	NULL,	'2021-01-01',	NULL,	'2021-04-19 02:56:51',	1),
('labor_day',	'Chúc mừng ngày Quốc tế Lao động',	1,	1,	100,	'immediately',	NULL,	'2021-04-19',	NULL,	'2021-04-19 02:57:40',	NULL),
('liberation_south',	'Chúc mừng kỷ niệm ngày Giải phóng miền Nam, thống nhất Đất nước',	1,	1,	100,	'immediately',	NULL,	'2021-04-22',	NULL,	'2021-04-22 08:25:12',	NULL),
('merry_christmas',	'Chúc mừng Giáng sinh',	1,	1,	100,	'immediately',	NULL,	'2021-04-06',	NULL,	'2021-04-06 09:00:28',	NULL),
('national_day',	'Chúc mừng ngày Quốc Khánh 2/9',	1,	1,	100,	'immediately',	NULL,	'2021-05-10',	NULL,	'2021-05-10 07:37:15',	NULL),
('noti_maintenance',	'Thông báo bảo trì',	1,	1,	100,	'immediately',	NULL,	'2021-04-06',	NULL,	'2021-04-06 09:00:28',	NULL),
('order_bond_status_A',	'Giao dịch được cập nhật trạng thái Xác nhận! (Trái phiếu)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_bond_status_C',	'Giao dịch được cập nhật trạng thái đã hủy! (Trái phiếu)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_bond_status_S',	'Giao dịch đã được thanh toán thành công (Trái phiếu)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_bond_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch (Trái phiếu)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_saving_status_A',	'Giao dịch được cập nhật trạng thái Xác nhận! (Tiết kiệm)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_saving_status_C',	'Giao dịch được cập nhật trạng thái đã hủy! (Tiết kiệm)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_saving_status_S',	'Giao dịch đã được thanh toán thành công (Tiết kiệm)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_saving_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch (Tiết kiệm)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_service_status_A',	'Giao dịch được cập nhật trạng thái Xác nhận! (Dịch vụ)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_service_status_C',	'Giao dịch được cập nhật trạng thái đã hủy! (Dịch vụ)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_service_status_S',	'Giao dịch đã được thanh toán thành công (Dịch vụ)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('order_service_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch (Dịch vụ)',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-24 19:29:11',	NULL),
('report_interest_day',	'Báo cáo chốt lãi mỗi ngày',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-11-10 14:00:29',	NULL),
('request_contract_extension',	'Yêu cầu gia hạn thành công',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('request_extend_contract',	'Tái đầu tư để nhận được nhiều ưu đãi hấp dẫn',	1,	1,	100,	'immediately',	NULL,	'2020-11-11',	NULL,	'2020-11-10 14:00:29',	NULL),
('vietnamese_women_day',	'Chúc mừng ngày Phụ Nữ Việt Nam',	1,	1,	100,	'immediately',	NULL,	'2021-04-06',	NULL,	'2021-04-06 09:00:28',	NULL),
('warning_extend',	'Cảnh báo hợp đồng sắp hết hạn',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-11-22 19:26:38',	NULL),
('withdraw_bond_create',	'Yêu cầu rút trái phiếu đã được tạo!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_bond_status_A',	'Yêu cầu rút trái phiếu đã được xác nhận!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2021-03-22 19:09:44',	NULL),
('withdraw_bond_status_C',	'Yêu cầu rút trái phiếu bị hủy',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2021-03-22 19:17:27',	NULL),
('withdraw_bond_status_D',	'Yêu cầu rút trái phiếu đã được hoàn thành!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2021-03-22 19:09:06',	NULL),
('withdraw_commission_create',	'Yêu cầu rút ví thưởng đã được tạo!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_commission_status_A',	'Yêu cầu rút ví thưởng được Xác nhận!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:58:52',	NULL),
('withdraw_commission_status_C',	'Yêu cầu rút ví thưởng bị hủy',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-22 21:33:07',	1),
('withdraw_commission_status_D',	'Yêu cầu rút ví thưởng đã được hoàn thành!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_interest_create',	'Yêu cầu rút lãi đã được tạo!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_interest_status_A',	'Yêu cầu rút lãi được Xác nhận!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:58:26',	NULL),
('withdraw_interest_status_C',	'Yêu cầu rút lãi bị hủy!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:02',	NULL),
('withdraw_interest_status_D',	'Yêu cầu rút lãi đã được hoàn thành!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_saving_create',	'Yêu cầu rút tiết kiệm đã được tạo!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_saving_status_A',	'Yêu cầu rút tiết kiệm được Xác nhận!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:58:37',	NULL),
('withdraw_saving_status_C',	'Yêu cầu rút tiết kiệm bị hủy!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('withdraw_saving_status_D',	'Yêu cầu rút tiết kiệm đã được hoàn thành!',	1,	1,	100,	'immediately',	NULL,	NULL,	NULL,	'2020-09-20 11:59:13',	NULL),
('woman_day',	'Chúc mừng ngày quốc tế phụ nữ',	1,	1,	100,	'immediately',	NULL,	'2021-04-06',	NULL,	'2021-04-06 09:00:28',	NULL);

DROP TABLE IF EXISTS `notification_template_auto`;
CREATE TABLE `notification_template_auto` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID tự tăng',
  `key` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Key đùng để xác định notification',
  `title_vi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tiêu đề (VI)',
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tiêu đề (EN)',
  `message_vi` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung tin nhắn (VI)',
  `message_en` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Nội dung tin nhắn (EN)',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Hình của notification',
  `has_detail` tinyint(1) DEFAULT 0 COMMENT 'Có trang chi tiết không',
  `detail_background` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Background detail',
  `detail_content_vi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Nội dung detail (VI)',
  `detail_content_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Nội dung detail (EN)',
  `detail_action_name_vi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên button tương tác',
  `detail_action_name_en` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên button tương tác',
  `detail_action` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Action khi click ở app',
  `detail_action_params` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Param bổ sung',
  `type_bonus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'money : thưởng tiền, voucher : thưởng dịch vụ',
  `money` decimal(16,3) DEFAULT NULL COMMENT 'Thưởng tiền',
  `service_id` int(11) DEFAULT NULL COMMENT 'Thưởng dịch vụ',
  `created_at` datetime DEFAULT NULL COMMENT 'Ngày tạo',
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày cập nhật',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `key` (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Template cấu hình nội dung gửi Notification';

INSERT INTO `notification_template_auto` (`id`, `key`, `title_vi`, `title_en`, `message_vi`, `message_en`, `avatar`, `has_detail`, `detail_background`, `detail_content_vi`, `detail_content_en`, `detail_action_name_vi`, `detail_action_name_en`, `detail_action`, `detail_action_params`, `type_bonus`, `money`, `service_id`, `created_at`, `updated_at`) VALUES
(1,	'order_bond_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch',	'VSETGROUP thank you for your transaction',	'Giao dịch số [order_code] đã được tiếp nhận và đang chờ xử lý',	'Transaction number [order_code] has been received and is pending',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">VSETGROUP cám ơn [gender] [name] lên Đầu Tư Trái Phiếu trên ứng dụng VSBOND.<br>\r\nGiao dịch số <b> [order_code] </b> đã được tiếp nhận và đang chờ xử lý</p>',	'<p style = \"font-size: 14px\"> VSETGROUP thanks [gender] [name] to Bond Investment on VSBOND application.<br>\r\nTransaction number <b> [order_code] </b> has been received and is pending </p>',	'Chi tiết giao dịch',	'Transaction details',	'order_detail',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 19:55:44'),
(5,	'order_bond_status_C',	'Giao dịch được cập nhật trạng thái đã hủy!',	'The transaction is updated with canceled status!',	'Giao dịch số [order_code] đã bị hủy',	'Transaction number [order_code] has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">Giao dịch số <b> [order_code] </b> đã bị hủy .Lý do : [reason_cancel]</p>',	'<p style = \"font-size: 14px\"> Transaction number <b> [order_code] </b> has been canceled .Reason : [reason_cancel]</p>',	'Chi tiết giao dịch ',	'Transaction details',	'order_detail',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:35:24',	'2021-04-27 01:47:21'),
(21,	'customer_ranking',	'Chúc mừng thăng hạng',	'Happy promotion',	'Chúc mừng bạn đã thăng hạng [name] với những quyền lợi mới.Hãy tận hưởng nào ',	'Congratulations on your [name] promotion with new perks. Enjoy',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">VSETGROUP chúc mừng [gender] [name] đã thăng hạng [ranking_name] với những quyền lợi mới.Hãy tận hưởng nào </p>',	'<p style = \"font-size: 14px\"> VSETGROUP congratulates [gender] [name] for ranking [ranking_name] with new benefits. Enjoy </p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2021-04-12 03:33:18'),
(22,	'customer_birthday',	'Chúc mừng sinh nhật khách hàng',	'Happy birthday to customers',	'Chúc [gender] [name] có một ngày sinh nhật vui vẻ và thành công trong công việc ',	'Happy [gender] [name] have a happy birthday and success in work',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">VSETGROUP chúc [gender] [name] có một ngày sinh nhật vui vẻ và thành công trong công việc </p>',	'<p style = \"font-size: 14px\"> VSETGROUP wish [gender] [name] have a happy and successful birthday at work </p>',	NULL,	NULL,	'birthday',	NULL,	NULL,	NULL,	NULL,	NULL,	'2020-11-19 19:56:10'),
(23,	'customer_W',	'Chúc mừng khách hàng mới',	'Happy new customers',	'Chúc mừng [name] đã trở thành thành viên mới, hãy tận hưởng những dịch vụ tốt nhất của chúng tôi.',	'Congratulations [name] on becoming a new member, enjoy our best services.',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">VSETGROUP chúc mừng [gender] [name] đã trở thành thành viên mới, hãy tận hưởng những dịch vụ tốt nhất của chúng tôi.<br>\r\nCám ơn Quý Khách!</p>',	'<p style = \"font-size: 14px\"> VSETGROUP congratulations [gender] [name] on being a new member, please enjoy our best services.<br>\r\nThank you very much! </p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2020-11-19 19:56:26'),
(24,	'order_bond_status_S',	'Giao dịch đã được thanh toán thành công',	'The transaction has been paid successfully',	'Giao dịch số [order_code] đã được thanh toán thành công',	'Transaction number [order_code] has been paid successfully',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">Giao dịch số <b> [order_code] </b> đã được thanh toán thành công<br>\r\nCám ơn Quý Khách!</p>',	'<p style = \"font-size: 14px\"> Transaction number <b> [order_code] </b> has been successfully paid <br>\r\nThank you very much! </p>',	'Xem ví của tôi',	'See my wallet',	'wallet-bond',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	NULL,	'2020-11-19 19:56:45'),
(25,	'order_bond_status_A',	'Giao dịch được cập nhật trạng thái Xác nhận!',	'Transaction is updated with Confirmation status!',	'Giao dịch số [order_code] đã được Xác nhận',	'Transaction number [order_code] has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nVSETGROUP cám ơn [gender] [name] lên Đầu Tư Trái Phiếu trên ứng dụng VSBOND.<br>\r\n<br>\r\nQuý khách vui lòng chuyển khoản [so tien] theo thông tin sau:<br>\r\n- STK: 0911007999999 <br>\r\n- Ngân hàng: Vietcombank Sài Gòn<br>\r\n- Chi nhánh: Gò Vấp<br>\r\n- Nội dung chuyển khoản: THANH TOAN HD [ma so hop dong]<br>\r\nNhân viên tư vấn sẽ liên hệ quý khách trong thời gian sớm nhất!<br>\r\n<br>\r\nTổng đài hỗ trợ : 0902739999<br>\r\nCám ơn Quý Khách!</p>',	'<p style = \"font-size: 14px\">\r\nVSETGROUP thanks [gender] [name] to Bond Investment on VSBOND application. <br>\r\n<br>\r\nPlease transfer [so tien] according to the following information: <br>\r\n- STK: 0911007999999 <br>\r\n- Bank: Vietcombank Saigon <br>\r\n- Branch: Go Vap <br>\r\n- Content of transfer: THANH TOAN HD [number of contract] <br>\r\nConsulting staff will contact you as soon as possible! <br>\r\n<br>\r\nSupport hotline: 0902739999 <br>\r\nThank you very much! </p>',	'Chi tiết giao dịch',	'Transaction details',	'order_detail',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:35:24',	'2020-11-19 19:57:20'),
(29,	'order_saving_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch',	'VSETGROUP thank you for your transaction',	'Giao dịch số [order_code] đã được tiếp nhận và đang chờ xử lý',	'Transaction number [order_code] has been received and is pending',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\"VSETGROUP cám ơn [gender] [name] lên Đầu Tư Tiết Kiệm trên ứng dụng VSBOND.<br>\r\nGiao dịch số <b> [order_code] </b> đã được tiếp nhận và đang chờ xử lý</p>',	'<p style = \"font-size: 14px\" VSETGROUP thank [gender] [name] for Saving Investment on VSBOND app.<br>\r\nTransaction number <b> [order_code] </b> has been received and is pending </p>',	'Chi tiết giao dịch',	'Transaction details',	'order_detail',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 19:57:36'),
(30,	'order_saving_status_A',	'Giao dịch được cập nhật trạng thái Xác nhận!',	'Transaction is updated with Confirmation status!',	'Giao dịch số [order_code] đã được Xác nhận',	'Transaction number [order_code] has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nVSETGROUP cám ơn [gender] [name] lên Đầu Tư Tiết Kiệm trên ứng dụng VSBOND. <br>\r\n<br>\r\nQuý khách vui lòng chuyển khoản [so tien] theo thông tin sau:<br>\r\n- STK: 0911007999999 <br>\r\n- Ngân hàng: Vietcombank Sài Gòn <br>\r\n- Chi nhánh: Gò Vấp<br>\r\n- Nội dung chuyển khoản: THANH TOAN HD [ma so hop dong]<br>\r\nNhân viên tư vấn sẽ liên hệ quý khách trong thời gian sớm nhất!<br>\r\n<br>\r\nTổng đài hỗ trợ : 0902739999<br>\r\nCám ơn Quý Khách!</p>',	'<p style = \"font-size: 14px\">\r\nVSETGROUP thanks [gender] [name] to Saving Investment on the VSBOND app. <br>\r\n<br>\r\nPlease transfer [so tien] according to the following information: <br>\r\n- STK: 0911007999999 <br>\r\n- Bank: Vietcombank Saigon <br>\r\n- Branch: Go Vap <br>\r\n- Content of transfer: THANH TOAN HD [number of contract] <br>\r\nConsulting staff will contact you as soon as possible! <br>\r\n<br>\r\nSupport hotline: 0902739999 <br>\r\nThank you very much! </p>',	'Chi tiết giao dịch',	'Transaction details',	'order_detail',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:35:24',	'2020-11-19 19:57:50'),
(31,	'order_saving_status_C',	'Giao dịch được cập nhật trạng thái đã hủy! ',	'The transaction is updated with canceled status!',	'Giao dịch số [order_code] đã bị hủy',	'Transaction number [order_code] has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">Giao dịch số <b> [order_code] </b> đã bị hủy .Lý do : [reason_cancel]</p>',	'<p style = \"font-size: 14px\"> Transaction number <b> [order_code] </b> has been canceled .Reason : [reason_cancel]</p>',	'Chi tiết giao dịch',	'Transaction details',	'order_detail',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:35:24',	'2021-04-27 01:47:21'),
(32,	'order_saving_status_S',	'Giao dịch đã được thanh toán thành công',	'The transaction has been paid successfully',	'Giao dịch số [order_code] đã được thanh toán thành công',	'Transaction number [order_code] has been paid successfully',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\"Giao dịch số <b> [order_code] </b> đã được thanh toán thành công<br>\r\nCám ơn Quý Khách!</p>',	'<p style = \"font-size: 14px\" \"transaction number <b> [order_code] </b> has been successfully paid <br>\r\nThank you very much! </p>',	'Xem ví của tôi',	'See my wallet',	'wallet-saving',	'{\"order_id\":\"[:order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	NULL,	'2020-11-19 19:58:49'),
(33,	'order_service_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch',	'VSETGROUP thank you for your transaction',	'Giao dịch số [order_service_code] đã được tiếp nhận và đang chờ xử lý',	'Transaction number [order_service_code] has been received and is pending',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\"VSETGROUP cám ơn [gender] [name] đã mua [service_name] trên ứng dụng VSBOND.<br>\r\nGiao dịch số <b> [order_service_code] </b> đã được tiếp nhận và đang chờ xử lý</p>',	'<p style = \"font-size: 14px\" VSETGROUP thank [gender] [name] for purchasing [service_name] on the VSBOND app.<br> Transaction number <b> [order_service_code] </b> has been received and is pending </p>',	'Xem chi tiết',	'See details',	'order_service_detail',	'{\"order_service_id\":\"[:order_service_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 19:59:11'),
(36,	'order_service_status_A',	'Giao dịch được cập nhật trạng thái Xác nhận!',	'Transaction is updated with Confirmation status!',	'Giao dịch số [order_service_code] đã được xác nhận',	'Transaction number [order_service_code] has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nVSETGROUP cám ơn [gender] [name] đã mua gói dịch vụ [service_name] trên ứng dụng VSBOND. <br>\r\n<br>\r\nQuý khách vui lòng chuyển khoản [so tien] theo thông tin sau:<br>\r\n- STK: 0911007999999 <br>\r\n- Ngân hàng: Vietcombank Sai Gòn <br>\r\n- Chi nhánh: Gò Vấp<br>\r\n- Nội dung chuyển khoản: THANH TOAN DV [ma giao dich]<br>\r\nNhân viên tư vấn sẽ liên hệ quý khách trong thời gian sớm nhất!<br>',	'<p style = \"font-size: 14px\">\r\nVSETGROUP thank [gender] [name] for purchasing the service package [service_name] on the VSBOND app. <br>\r\n<br>\r\nPlease transfer [so tien] according to the following information: <br>\r\n- STK: 0911007999999 <br>\r\n- Bank: Vietcombank Sai Gon <br>\r\n- Branch: Go Vap <br>\r\n- Content of transfer: THANH TOAN DV [code of transaction] <br>\r\nConsulting staff will contact you as soon as possible! <br>',	'Xem chi tiết',	'Xem chi tiết',	'order_service_detail',	'{\"order_service_id\":\"[:order_service_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:00:31'),
(37,	'order_service_status_C',	'Giao dịch được cập nhật trạng thái đã hủy!',	'The transaction is updated with canceled status!',	'Giao dịch số [order_service_code] đã bị hủy',	'Transaction number [order_service_code] has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">Giao dịch số <b> [order_code] </b> đã bị hủy</p>d',	'<p style = \"font-size: 14px\"> Transaction number <b> [order_code] </b> has been canceled </p> d',	'Xem chi tiết',	'See details',	'order_service_detail',	'{\"order_service_id\":\"[:order_service_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:01:11'),
(38,	'order_service_status_S',	'Giao dịch đã được thanh toán thành công',	'The transaction has been paid successfully',	'Giao dịch số [order_service_code] đã được thanh toán thành công',	'Transaction number [order_service_code] has been paid successfully',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\"Giao dịch số <b> [order_service_code] </b> đã được thanh toán thành công<br>\r\nCám ơn Quý Khách!</p>',	'<p style = \"font-size: 14px\" Transaction number <b> [order_service_code] </b> has been paid successfully <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'service_detail',	'{\"order_service_id\":\"[:order_service_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:02:08'),
(39,	'withdraw_interest_status_A',	'Yêu cầu rút lãi được Xác nhận!',	'Request to withdraw interest is Confirmed!',	'Giao dịch rút lãi số [withdraw_request_code] đã được xác nhận',	'The [withdraw_request_code] digital withdrawal transaction has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút lãi cho hợp đồng số <b> [contract_code] </b> của [gender] [name] đã được xác nhận. <br>\r\n<br>\r\nVSETGROUP sẽ chuyển khoản [so tien] theo thông tin sau:<br>\r\n- Thời gian chuyển khoản trong [so ngay] sau khi giao dịch xác nhận<br>\r\n- STK: [stk cus] <br>\r\n- Ngân hàng: [ngân hàng] <br>\r\n- Chi nhánh [chi nhanh] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nInterest withdrawal request for contract number <b> [contract_code] </b> of [gender] [name] has been confirmed. <br>\r\n<br>\r\nVSETGROUP will transfer [amount] according to the following information: <br>\r\n- Transfer time in [compared with immediately] after the transaction confirmed <br>\r\n- STK: [stk cus] <br>\r\n- Bank: [bank] <br>\r\n- Branch [quick branch] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:03:16'),
(40,	'withdraw_saving_status_A',	'Yêu cầu rút tiết kiệm được Xác nhận!',	'Savings withdrawal request Confirmed!',	'Giao dịch rút tiết kiệm số [withdraw_request_code] đã được xác nhận',	'The [withdraw_request_code] digital withdrawal transaction has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút tiết kiệm cho hợp đồng số <b> [contract_code] </b> của [gender] [name] đã được xác nhận. <br>\r\n<br>\r\nVSETGROUP sẽ chuyển khoản [so tien] theo thông tin sau:<br>\r\n- Thời gian chuyển khoản trong [so ngay] sau khi giao dịch xác nhận<br>\r\n- STK: [stk cus] <br>\r\n- Ngân hàng: [ngân hàng] <br>\r\n- Chi nhánh [chi nhanh] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe savings withdrawal request for contract number <b> [contract_code] </b> of [gender] [name] has been confirmed. <br>\r\n<br>\r\nVSETGROUP will transfer [amount] according to the following information: <br>\r\n- Transfer time in [compared with immediately] after the transaction confirmed <br>\r\n- STK: [stk cus] <br>\r\n- Bank: [bank] <br>\r\n- Branch [quick branch] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:04:14'),
(41,	'withdraw_commission_status_A',	'Yêu cầu rút ví thưởng được Xác nhận!',	'Bonus wallet withdrawal request Confirmed!',	'Giao dịch rút ví thưởng [withdraw_request_code] đã được xác nhận',	'The [withdraw_request_code] bonus wallet withdrawal transaction has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút thưởng của [gender] [name] đã được xác nhận. <br>\r\n<br>\r\nVSETGROUP sẽ chuyển khoản [so tien] theo thông tin sau:<br>\r\n- Thời gian chuyển khoản trong [so ngay] sau khi giao dịch xác nhận<br>\r\n- STK: [stk cus] <br>\r\n- Ngân hàng: [ngân hàng] <br>\r\n- Chi nhánh [chi nhanh] <br>\r\nCám ơn Qúy Khách! </p> ',	'<p style = \"font-size: 14px\">\r\n[Gender] [name] withdrawal request has been confirmed. <br>\r\n<br>\r\nVSETGROUP will transfer [amount] according to the following information: <br>\r\n- Transfer time in [compared with immediately] after the transaction confirmed <br>\r\n- STK: [stk cus] <br>\r\n- Bank: [bank] <br>\r\n- Branch [quick branch] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:05:36'),
(42,	'withdraw_interest_status_C',	'Yêu cầu rút lãi bị hủy!',	'Interest withdrawal request is canceled!',	'Giao dịch rút lãi số [withdraw_request_code] đã bị hủy',	'The [withdraw_request_code] digital withdrawal transaction has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút lãi cho hợp đồng số <b> [contract_code] </b> của [gender] [name] đã bị hủy. <br>\r\nLý do : [reason] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nInterest withdrawal request for contract number <b> [contract_code] </b> of [gender] [name] has been canceled. <br>\r\nReason: [reason] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:06:37'),
(43,	'withdraw_saving_status_C',	'Yêu cầu rút tiết kiệm bị hủy!',	'Savings withdrawal request is canceled!',	'Giao dịch rút tiết kiệm số [withdraw_request_code] đã bị hủy',	'The [withdraw_request_code] digital withdrawal transaction has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút tiết kiệm cho hợp đồng số <b> [contract_code] </b> của [gender] [name] đã được bị hủy. <br>\r\nLý do : [reason] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe savings withdrawal request for contract number <b> [contract_code] </b> of [gender] [name] has been canceled. <br>\r\nReason: [reason] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:07:19'),
(45,	'withdraw_commission_status_C',	'Yêu cầu rút ví thưởng bị hủy',	'Bonus wallet withdrawal request has been canceled',	'Giao dịch rút ví thưởng [withdraw_request_code] đã bị hủy',	'The [withdraw_request_code] bonus wallet withdrawal transaction has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút thưởng của [gender] [name] đã bị hủy. <br>\r\nLý do : [reason] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\n[Gender] [name] withdrawal request has been canceled. <br>\r\nReason: [reason] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:08:04'),
(46,	'custom_contract_confirm',	'Xác nhận lãi hàng tháng',	'Monthly interest confirmation',	'Xác nhận lãi hàng tháng của hợp đồng [customer_contract_code] đã được xác nhận',	'Confirm the monthly interest of the contract [customer_contract_code] has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nXác nhận lãi hàng tháng của hợp đồng <b> [customer_contract_code] </b> đã được xác nhận</p>',	'<p style = \"font-size: 14px\">\r\nMonthly contractual interest confirmation <b> [customer_contract_code] </b> has been verified </p>',	NULL,	NULL,	'',	'',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:09:07'),
(47,	'withdraw_interest_create',	'Yêu cầu rút lãi đã được tạo!',	'Interest withdrawal request has been created!',	'Giao dịch rút lãi số [withdraw_request_code] đã được tạo',	'Interest withdrawal [withdraw_request_code] number has been created',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút lãi cho hợp đồng số <b> [contract_code] </b> với số tiền [withdraw_request_amount] của [gender] [name] đã được tạo. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nInterest withdrawal request for contract number <b> [contract_code] </b> with an amount [withdraw_request_amount] of [gender] [name] has been created. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:13:02'),
(49,	'withdraw_saving_create',	'Yêu cầu rút tiết kiệm đã được tạo!',	'Savings withdrawal request has been created!',	'Giao dịch rút tiết kiệm số [withdraw_request_code] đã được tạo',	'Withdraw savings of [withdraw_request_code] was created',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút tiết kiệm cho hợp đồng số <b> [contract_code] </b> với số tiền [withdraw_request_amount] của [gender] [name] đã được tạo. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nSavings withdrawal request for contract number <b> [contract_code] </b> with a [withdraw_request_amount] amount of [gender] [name] has been created. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:13:57'),
(50,	'withdraw_commission_create',	'Yêu cầu rút ví thưởng đã được tạo!',	'Bonus wallet withdrawal request has been created!',	'Giao dịch rút ví thưởng [withdraw_request_code] đã được tạo',	'Bonus transaction withdrawal [withdraw_request_code] has been created',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút ví thưởng với số tiền [withdraw_request_amount] của [gender] [name] đã được tạo. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nA request to withdraw bonus wallet with [withdraw_request_amount] amount of [gender] [name] has been created. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:16:11'),
(51,	'withdraw_interest_status_D',	'Yêu cầu rút lãi đã được hoàn thành!',	'Interest withdrawal request has been completed!',	'Giao dịch rút lãi số [withdraw_request_code] đã được hoàn thành',	'Interest withdrawal [withdraw_request_code] has been completed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút lãi cho hợp đồng số <b> [contract_code] </b> với số tiền [withdraw_request_amount] của [gender] [name] đã được hoàn thành. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe withdrawal request for contract number <b> [contract_code] </b> with an amount [withdraw_request_amount] of [gender] [name] has been completed. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'Xem chi tiết',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:18:01'),
(53,	'withdraw_saving_status_D',	'Yêu cầu rút tiết kiệm đã được hoàn thành!',	'Savings withdrawal request has been completed!',	'Giao dịch rút tiết kiệm số [withdraw_request_code] đã được hoàn thành',	'Withdraw savings of [withdraw_request_code] has been completed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút tiết kiệm cho hợp đồng số <b> [contract_code] </b> với số tiền [withdraw_request_amount] của [gender] [name] đã được hoàn thành. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe savings withdrawal request for contract number <b> [contract_code] </b> with [withdraw_request_amount] amount of [gender] [name] has been completed. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:19:15'),
(54,	'withdraw_commission_status_D',	'Yêu cầu rút ví thưởng đã được hoàn thành!',	'Bonus wallet withdrawal request has been completed!',	'Giao dịch rút ví thưởng [withdraw_request_code] đã được hoàn thành',	'Bonus withdrawal transaction [withdraw_request_code]  has been completed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút ví thưởng với số tiền [withdraw_request_amount] của [gender] [name] đã được hoàn thành. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe request to withdraw bonus wallet with [withdraw_request_amount] amount of [gender] [name] has been completed. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:20:55'),
(55,	'happy_new_year',	'Chúc mừng năm mới',	'Happy New Year',	'Chúc mừng năm mới',	'Happy New Year',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\nKính gửi quý khách<br>\n\nTrước tiên xin thay mặt toàn thể cán bộ công nhân viên trong Tập đoàn VSETGROUP xin chân thành cảm ơn Quý khách hàng đã và đang tín nhiệm, quan tâm, ủng hộ các sản phẩm cũng như dịch vụ do chúng tôi cung cấp trong suốt thời gian qua.<br>\n\nChúng tôi luôn quan niệm rằng sự ủng hộ, yêu mến và niềm tin đến từ phía Quý khách đã dành cho công ty trong suốt thời gian qua chính là thành công lớn nhất của công ty. Để đáp lại tấm thịnh tình này chúng tôi hiểu rằng cần phải nâng cao chất lượng dịch vụ và sản phẩm tốt hơn nữa để không phụ niềm tin sự ủng hộ của Quý khách hàng.<br>\n\nMột lần nữa, Tập đoàn VSETGROUP xin được gửi tới Quý khách hàng những lời chúc mừng năm mới tốt đẹp nhất. Chúc Quý khách hàng và gia đình đón Xuân Mới ... tràn ngập niềm vui và Hạnh phúc, An Khang Thịnh Vượng.\n</p>',	'<p style = \"font-size: 14px\">\nTo customer: <br>\n\nFirst of all, on behalf of all employees of VSETGROUP Group, we would like to sincerely thank customers for their trust, interest and support for the products and services we provide during the time. past time. <br>\n\nWe always think that the support, love and trust from you for the company during the past time is the greatest success of the company. In response to this tribute, we understand that we need to improve the quality of our services and products even better so as not to trust our customers\' support.<br>\n\nOnce again, VSETGROUP Group would like to send our customers the best New Year greetings. Wishing you and your family to welcome New Spring ... filled with joy and happiness, An Khang Thinh Vuong.\n</p>',	NULL,	NULL,	NULL,	NULL,	'',	NULL,	NULL,	'2019-10-04 17:31:54',	'2021-04-01 09:43:07'),
(56,	'woman_day',	'Chúc mừng ngày quốc tế phụ nữ',	'Happy Women\'s Day',	'Chúc mừng ngày quốc tế phụ nữ',	'Happy Women\'s Day',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nNhân ngày Quốc tế phụ nữ 8/3, VSETGROUP xin gửi đến quý Khách hàng, Nhà đầu tư lời chúc sức khỏe, chúc các chị em luôn hạnh phúc, xinh đẹp và có thật nhiều niềm vui, thành công trong cuộc sống.\r\nThân ái\r\n</p>',	'<p style = \"font-size: 14px\">\r\nOn the occasion of International Women\'s Day 8/3, VSETGROUP would like to send our best wishes to all customers and investors, wish you all happiness, beauty and lots of happiness and success in life.\r\nLove\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:22:45'),
(57,	'liberation_south',	'Chúc mừng kỷ niệm ngày Giải phóng miền Nam, thống nhất Đất nước',	'Congratulations on the day of the Liberation of the South and the reunification of the country',	'Chúc mừng kỷ niệm ngày Giải phóng miền Nam, thống nhất Đất nước',	'Congratulations on the day of the Liberation of the South and the reunification of the country',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nNhân dịp ngày lễ 30/4 đang đến, VSETGROUP chúc cho toàn thể quý Khách hàng, quý Nhà đầu tư có sức khỏe dồi dào, hoàn thành tốt công việc và nhiệm vụ được giao, tạo nên nhiều giá trị ý nghĩa cho cuộc sống.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nOn the occasion of the upcoming 30/4 holiday, VSETGROUP wishes all customers and investors good health, successfully complete their assigned tasks and tasks, creating meaningful values for life.\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:23:55'),
(58,	'labor_day',	'Chúc mừng ngày Quốc tế Lao động',	'Congratulates the International Labor Day',	'Chúc mừng ngày Quốc tế Lao động',	'Congratulates the International Labor Day',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nNhân dịp ngày Quốc tế Lao động 1/5, VSETGROUP xin kính chúc quý đối tác, khách hàng có một kỳ nghỉ lễ ý nghĩa, ấm áp, hạnh phúc.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nOn the occasion of the International Labor Day May 1, VSETGROUP would like to wish all partners and customers a meaningful, warm and happy holiday.\r\n</p>',	NULL,	NULL,	NULL,	NULL,	'',	NULL,	NULL,	'2019-10-04 17:31:54',	'2021-04-01 09:43:07'),
(59,	'national_day',	'Chúc mừng ngày Quốc Khánh 2/9',	'Congratulations Independence Day 2/9',	'Chúc mừng ngày Quốc Khánh 2/9',	'Congratulations Independence Day 2/9',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nHòa chung vào không khí hào hùng của ngày Quốc Khánh 02/09, VSETGroup xin gửi tới Quý khách hàng lời cảm ơn, lời chúc tốt đẹp nhất cho sự hợp tác và quan hệ lâu dài với VSETGroup. Trân trọng cảm ơn Quý khách hàng đã lựa chọn, tin tưởng và đồng hành cùng chúng tôi.<br>\r\n\r\nKính chúc Quý khách có những giây phút vui vẻ và hạnh phúc bên gia đình và những người thân yêu!\r\n</p>',	'<p style = \"font-size: 14px\">\r\nJoining in the heroic atmosphere of the National Day September 2, VSETGroup would like to send our customers thanks, best wishes for the long-term cooperation and relationship with VSETGroup. Sincerely thank customers for choosing, trusting and accompanying us<br>\r\n\r\nWe wish you happy and happy moments with your family and loved ones!\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:25:31'),
(60,	'vietnamese_women_day',	'Chúc mừng ngày Phụ Nữ Việt Nam',	'Happy Vietnamese Women\'s Day',	'Chúc mừng ngày Phụ Nữ Việt Nam',	'Happy Vietnamese Women\'s Day',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nVSET kính chúc Quý khách có một ngày 20 tháng 10 ý nghĩa, vui vẻ và ngập tràn hạnh phúc. Chân thành cảm ơn quý khách đã đồng hành cùng chúng tôi trong suốt thời gian qua.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nVSET wishes you a meaningful, happy and happy October 20 day. Thank you very much for accompanying us during the past time.\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:26:07'),
(61,	'merry_christmas',	'Chúc mừng Giáng sinh',	'Merry Christmas',	'Chúc mừng Giáng sinh',	'Merry Christmas',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nQuý khách hàng thân mến! <br>\r\nVậy là một mùa giáng sinh nữa lại về, đánh dấu cho sự hợp tác tốt đẹp của chúng ta trong nhiều năm qua. VSETGROUP rất cảm ơn sự đồng hành của quý khách hàng đã giúp chúng tôi làm nên những thành công của ngày hôm nay. Quý khách hàng luôn là niềm động lực và niềm tin để chúng tôi tiếp tục cố gắng. Chúc quý Khách hàng, Nhà đầu tư một giáng sinh an lành và ấm áp. Mong được tiếp tục đồng hành với quý khách hàng trên chặn đường phía trước.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nDear customers! <br>\r\nSo another Christmas season is coming, marking our good cooperation over the years. VSETGROUP would like to thank our customers for their companionship helping us to make today\'s successes. Customers are always the motivation and belief for us to keep trying. We wish customers and investors a merry and warm Christmas. Looking forward to continuing to accompany our customers on the way ahead.\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2021-04-06 08:36:22'),
(63,	'request_contract_extension',	'Yêu cầu gia hạn thành công',	'Successful renewal request',	'Yêu cầu gia hạn thành công',	'Successful renewal request',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nQuý khách đã yêu cầu gia hạn hợp đồng thành công. Mã hợp đồng gia hạn: [MaHopDong]. Chúng tôi sẽ xử lý yêu cầu của quý khách trong thời gian sớm nhất. Mọi chi tiết xin liện hệ bộ phận CSKH. Hotline: 090739999\r\n</p>',	'<p style = \"font-size: 14px\">\r\nYou have successfully requested to renew the contract. Contract renewal code: [MaHopDong]. We will process your request as soon as possible. For more information, please contact the Customer Care department. Hotline: 090739999\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:27:08'),
(64,	'contract_extension',	'Gia hạn hợp đồng thành công',	'Successful contract renewal',	'Gia hạn hợp đồng thành công',	'Successful contract renewal',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nQuý khách đã gia hạn hợp đồng thành công. Mã hợp đồng gia hạn: [MaHopDong]. Quý khách có thể kiểm tra hợp đồng của mình tại trang xem thông tin hợp đồng. Mọi thắc mắc xin liên hệ bộ phận CSKH. Hotline: 090739999. \r\nVSET xin trân trọng cảm ơn.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nYou have successfully renewed the contract. Contract renewal code: [MaHopDong]. You can check your contract at the contract information page. Any questions please contact the Customer Care department. Hotline: 090739999.\r\nVSET would like to thank you.\r\n</p>',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:27:55'),
(65,	'warning_extend',	'Cảnh báo hợp đồng sắp hết hạn',	'Contract warning is about to expire',	'Hợp đồng [customer_contract_code] sắp hết hạn.',	'The contract [customer_contract_code] is about to expire.',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nHợp đồng [customer_contract_code] sắp hết hạn, ngày hết hạn: [customer_contract_end_date]. Quý khách có muốn gia hạn hợp đồng để nhận được thêm nhiều ưu đãi. Mọi thắc mắc xin liên hệ bộ phận CSKH để biết thêm chi tiết. Hotline: 090739999.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nThe contract [customer_contract_code] is about to expire, expiration date: [customer_contract_end_date]. Do you want to extend the contract to get more incentives. If you have any questions, please contact the Customer Care department for more details. Hotline: 090739999.\r\n</p>',	'Gia hạn ngay',	'Extend now',	'customer_contract_detail',	'{\"customer_contract_code\":\"[:customer_contract_code]\",\"type\":\"[:type]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-23 20:05:56'),
(66,	'request_extend_contract',	'Tái đầu tư để nhận được nhiều ưu đãi hấp dẫn',	'Re-invest to receive many attractive incentives',	'Tái đầu tư để nhận được nhiều ưu đãi hấp dẫn',	'Re-invest to receive many attractive incentives',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nQuý khách có thể sử dụng số tiền lãi rảnh rỗi để thực hiện tái đầu tư để nhận được những ưu đãi từ VSET. Đầu tư thông minh, nhận quà cực lớn.<br>\r\nĐể biết thêm thông tin chi tiết về chương trình ưu đãi, quý khách vui lòng liên hệ bộ phận CSKH. Hotline: 090739999.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nYou can use your free interest to reinvest to receive special offers from VSET. Smart investment, receive huge gifts. <br>\r\nFor more information about the incentive program, please contact the Customer Care department. Hotline: 090739999.\r\n</p>',	'Đầu tư ngay',	'Invest now',	'customer_contract_detail',	'{\"customer_contract_code\":\"[:customer_contract_code]\",\"type\":\"[:type]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-26 23:45:29'),
(67,	'report_interest_day',	'Báo cáo chốt lãi mỗi ngày',	'Daily interest report',	'Báo cáo chốt lãi mỗi ngày',	'Daily interest report',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nThông báo chốt lãi của quý khách vào ngày [day_report].\r\nLãi đầu ngày : [interest_start] VNĐ <<br>\r\nLãi cuối ngày : [interest_end] VNĐ <br>\r\nSố tiền lãi đã sử dụng : [interest_using] VNĐ\r\n<br>\r\nĐể biết thêm thông tin chi tiết về chương trình ưu đãi, quý khách vui lòng liên hệ bộ phận CSKH. Hotline: 090739999.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nYour interest on the date [day_report].<br>\r\nInterest at the beginning of the day: [interest_start] VND <br>\r\nInterest at the end of the day: [interest_end] VND <br>\r\nThe amount of interest used: [interest_using] VND\r\n<br>\r\nFor more information about the incentive program, please contact the Customer Care department. Hotline: 090739999.\r\n</p>',	'',	'',	'',	'',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-26 23:45:29'),
(68,	'noti_maintenance',	'Thông báo bảo trì',	'Maintenance notice',	'Thông báo bảo trì hệ thống',	'System maintenance notice',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	0,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nXin thông báo đến quý khách hàng. Hệ thống VSET sẽ được tạm dừng để bảo trì hệ thống từ 25/01/2021 đến 26/01/2021 .<br>\r\nĐể biết thêm thông tin chi tiết quý khách vui lòng liên hệ bộ phận CSKH. Hotline: 090739999.\r\n</p>',	'<p style = \"font-size: 14px\">\r\nPlease inform our customers. The VSET system will be paused for system maintenance from 25/01/2021 to 26/01/2021 .<br>\r\nFor more information, please contact the Customer Care department. Hotline: 090739999.\r\n</p>',	'',	'',	'',	'',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-12-21 20:12:24'),
(69,	'withdraw_bond_create',	'Yêu cầu rút trái phiếu đã được tạo!',	'Bond withdrawal request has been created!',	'Giao dịch rút trái phiếu số [withdraw_request_code] đã được tạo',	'Withdraw bond of [withdraw_request_code] was created',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút trái phiếu cho hợp đồng số <b> [contract_code] </b> với số tiền [withdraw_request_amount] của [gender] [name] đã được tạo. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nBond withdrawal request for contract number <b> [contract_code] </b> with a [withdraw_request_amount] amount of [gender] [name] has been created. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:13:57'),
(70,	'order_stock_status_W',	'VSETGROUP cảm ơn bạn đã giao dịch',	'VSETGROUP thank you for your transaction',	'Giao dịch số [stock_order_code] đã được tiếp nhận và đang chờ xử lý',	'Transaction number [stock_order_code] has been received and is pending',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">VSETGROUP cám ơn [gender] [name] đã đầu tư trên ứng dụng VSBOND.<br>\r\nGiao dịch số <b> [stock_order_code] </b> đã được tiếp nhận và đang chờ xử lý</p>',	'<p style = \"font-size: 14px\">VSETGROUP thank [gender] [name] for investing in the VSBOND application. <br>\r\nTransaction number <b> [stock_order_code] </b> has been received and is pending</p>',	'Chi tiết giao dịch',	'Transaction details',	'stock_order_detail',	'{\"stock_order_id\":\"[:stock_order_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2021-04-15 16:12:03'),
(71,	'withdraw_bond_status_A',	'Yêu cầu rút trái phiếu được Xác nhận!',	'Bond withdrawal request Confirmed!',	'Giao dịch rút trái phiếu số [withdraw_request_code] đã được xác nhận',	'The [withdraw_request_code] digital bond withdrawal transaction has been confirmed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút trái phiếu cho hợp đồng số <b> [contract_code] </b> của [gender] [name] đã được xác nhận. <br>\r\n<br>\r\nVSETGROUP sẽ chuyển khoản [so tien] theo thông tin sau:<br>\r\n- Thời gian chuyển khoản trong [so ngay] sau khi giao dịch xác nhận<br>\r\n- STK: [stk cus] <br>\r\n- Ngân hàng: [ngân hàng] <br>\r\n- Chi nhánh [chi nhanh] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe bond withdrawal request for contract number <b> [contract_code] </b> of [gender] [name] has been confirmed. <br>\r\n<br>\r\nVSETGROUP will transfer [amount] according to the following information: <br>\r\n- Transfer time in [compared with immediately] after the transaction confirmed <br>\r\n- STK: [stk cus] <br>\r\n- Bank: [bank] <br>\r\n- Branch [quick branch] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:04:14'),
(72,	'withdraw_bond_status_C',	'Yêu cầu rút trái phiếu bị hủy!',	'Bond withdrawal request is canceled!',	'Giao dịch rút trái phiếu số [withdraw_request_code] đã bị hủy',	'The [withdraw_request_code] bond withdrawal transaction has been canceled',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút trái phiếu cho hợp đồng số <b> [contract_code] </b> của [gender] [name] đã được bị hủy. <br>\r\nLý do : [reason] <br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe bond withdrawal request for contract number <b> [contract_code] </b> of [gender] [name] has been canceled. <br>\r\nReason: [reason] <br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:07:19'),
(74,	'withdraw_bond_status_D',	'Yêu cầu rút trái phiếu đã được hoàn thành!',	'Bond withdrawal request has been completed!',	'Giao dịch rút trái phiếu số [withdraw_request_code] đã được hoàn thành',	'Withdraw bond of [withdraw_request_code] has been completed',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	1,	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',	'<p style=\"font-size:14px\">\r\nYêu cầu rút trái phiếu cho hợp đồng số <b> [contract_code] </b> với số tiền [withdraw_request_amount] của [gender] [name] đã được hoàn thành. <br>\r\n<br>\r\nCám ơn Qúy Khách! </p>',	'<p style = \"font-size: 14px\">\r\nThe bond withdrawal request for contract number <b> [contract_code] </b> with [withdraw_request_amount] amount of [gender] [name] has been completed. <br>\r\n<br>\r\nThank you very much! </p>',	'Xem chi tiết',	'See details',	'withdraw_request_detail',	'{\"withdraw_request_id\":\"[:withdraw_request_id]\",\"user_id\":\"[:user_id]\",\"brand_url\":\"[:brand_url]\",\"brand_name\":\"[:brand_name]\",\"brand_id\":[:brand_id]}',	NULL,	NULL,	NULL,	'2019-10-04 17:31:54',	'2020-11-19 20:19:15');

DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `admin_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_menu_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên menu',
  `admin_menu_category_id` int(11) DEFAULT NULL,
  `admin_menu_route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'đường dẩn',
  `admin_menu_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên icon',
  `admin_menu_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_menu_position` int(11) DEFAULT NULL COMMENT 'Vị trí ',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`admin_menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='Danh sách menu';

INSERT INTO `admin_menu` (`admin_menu_id`, `admin_menu_name`, `admin_menu_category_id`, `admin_menu_route`, `admin_menu_icon`, `admin_menu_img`, `admin_menu_position`, `created_at`, `updated_at`) VALUES
(1,	'Danh sách nhà đầu tư',	3,	'admin.customer',	'fa fa-user-tie',	NULL,	NULL,	NULL,	'2021-04-02 10:12:49'),
(2,	'Danh sách gói',	4,	'admin.product',	'fa fa-cubes',	NULL,	NULL,	NULL,	'2021-04-03 01:55:09'),
(3,	'Danh sách nhân viên',	3,	'admin.staff',	'fa fa-user-cog',	NULL,	NULL,	NULL,	'2021-04-02 10:12:57'),
(4,	'Danh sách yêu cầu mua trái phiếu - tiết kiệm',	2,	'admin.buy-bonds-request',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-27 06:41:13'),
(5,	'Danh sách hợp đồng',	2,	'admin.customer-contract',	'fa fa-shopping-cart',	NULL,	NULL,	NULL,	'2021-04-27 06:41:13'),
(6,	'Danh sách yêu cầu rút tiền',	2,	'admin.withdraw-request',	'fa fa-reply-all',	NULL,	NULL,	NULL,	'2021-04-02 10:04:30'),
(7,	'Danh sách thông tin hướng dẫn',	NULL,	'admin.support-management',	'fa fa-info-circle',	NULL,	14,	NULL,	'2020-10-01 19:10:31'),
(8,	'Danh sách nhóm hướng dẫn',	NULL,	'admin.faq-group.index',	'fa fa-life-ring',	NULL,	15,	NULL,	'2020-10-08 08:36:20'),
(9,	'Danh sách hướng dẫn',	9,	'admin.faq.index',	'fa fa-tasks',	NULL,	15,	NULL,	'2021-04-03 02:24:37'),
(10,	'Danh sách hỗ trợ',	9,	'admin.support',	'fa fa-life-ring',	NULL,	NULL,	NULL,	'2021-04-03 02:24:36'),
(11,	'Quyền lợi cấp độ đầu tư',	3,	'admin.member-level',	'fa fa-award',	NULL,	NULL,	NULL,	'2021-04-24 04:22:43'),
(12,	'Danh sách phòng ban',	NULL,	'admin.department',	'fa fa-sitemap',	NULL,	3,	NULL,	'2020-12-23 11:40:29'),
(13,	'Danh sách chức vụ',	NULL,	'admin.staff-title',	'fa fa-suitcase',	NULL,	3,	NULL,	'2020-12-23 11:40:29'),
(14,	'Danh sách dịch vụ',	4,	'voucher',	'fa fa-cubes',	NULL,	NULL,	NULL,	'2021-04-03 01:55:13'),
(15,	'Danh sách ngân hàng',	3,	'admin.bank',	'fa fa-university ',	NULL,	NULL,	NULL,	'2021-04-03 02:22:26'),
(16,	'Danh sách nhóm thông báo',	NULL,	'admin.customer-group-filter',	'fa fa-university ',	NULL,	NULL,	NULL,	'2021-04-03 02:02:07'),
(17,	'Danh sách serial dịch vụ',	2,	'admin.service-serial',	'fa fa-shopping-cart',	NULL,	NULL,	NULL,	'2021-04-27 07:06:16'),
(18,	'Danh sách yêu cầu mua dịch vụ',	2,	'admin.order-service',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-27 07:06:20'),
(19,	'Cấu hình thông báo tự động',	6,	'config',	'fa fa-university ',	NULL,	16,	NULL,	'2021-04-03 02:02:16'),
(20,	'Danh sách nhóm quyền',	5,	'admin.role-group',	'fa fa-users',	NULL,	NULL,	NULL,	'2021-04-03 01:57:44'),
(21,	'Quản lý phân quyền',	NULL,	'admin.authorization',	'fa fa-users-cog',	NULL,	NULL,	NULL,	'2021-04-27 07:14:00'),
(22,	'Danh sách log email',	8,	'admin.log-email',	'fa fa-history',	NULL,	NULL,	NULL,	'2021-04-03 02:21:41'),
(23,	'Danh sách log thông báo',	8,	'admin.log-noti',	'fa fa-history',	NULL,	NULL,	NULL,	'2021-04-03 02:21:43'),
(24,	'Danh sách nhà đầu tư tiềm năng',	NULL,	'admin.potential-customers',	'fa fa-user-tie',	NULL,	21,	NULL,	'2020-12-23 11:32:23'),
(25,	'Danh sách nhà môi giới',	NULL,	'admin.customer-broker',	'fa fa-user-tie',	NULL,	22,	NULL,	'2020-12-23 11:32:23'),
(26,	'Báo cáo doanh thu - thanh toán',	1,	'admin.report.revenue',	'fa fa-chart-pie',	NULL,	NULL,	NULL,	'2021-04-02 09:54:08'),
(27,	'Báo cáo trả lãi dự kiến',	NULL,	'admin.report.expectedInterestPayment',	'fa fa-chart-pie',	NULL,	NULL,	NULL,	'2021-05-05 08:14:03'),
(28,	'Báo cáo lãi suất',	1,	'admin.report.interest',	'fa fa-chart-pie',	NULL,	NULL,	NULL,	'2021-04-02 09:53:58'),
(29,	'Cấu hình email cc',	7,	'admin.send-email',	'fa fa-list',	NULL,	NULL,	NULL,	'2021-04-03 02:19:22'),
(30,	'Danh sách nguồn khách hàng',	NULL,	'admin.customer-source',	'fa fa-users',	NULL,	NULL,	NULL,	'2021-04-02 09:53:38'),
(31,	'Danh sách yêu cầu gia hạn hợp đồng',	2,	'admin.extend-contract',	'fa fa-shopping-cart',	NULL,	NULL,	NULL,	'2021-04-02 10:04:44'),
(32,	'Danh sách nguồn khách hàng',	NULL,	'admin.customer-source',	'fa fa-users',	NULL,	NULL,	NULL,	'2021-03-09 12:49:44'),
(33,	'Danh sách popup khuyến mãi',	7,	'admin.banner',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-03 02:19:31'),
(34,	'Cấu hình thưởng khi share link',	7,	'admin.refer-source',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-03 02:19:26'),
(35,	'Danh sách yêu cầu thay đổi thông tin',	2,	'admin.customer-change-request',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-02 10:04:51'),
(37,	'Danh sách nhóm khách hàng',	5,	'admin.group-customer',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-16 09:23:24'),
(38,	'Danh sách nhóm nhân viên',	5,	'admin.group-staff',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-03 01:58:05'),
(39,	'Báo cáo doanh thu nhân viên',	1,	'admin.report.sales',	'fa fa-chart-pie',	NULL,	NULL,	NULL,	'2021-04-02 09:53:52'),
(40,	'Báo cáo nhà đầu tư đăng ký từ referral link',	1,	'admin.report.shareLink',	'fa fa-chart-pie',	NULL,	NULL,	NULL,	'2021-04-02 09:53:52'),
(41,	'Danh sách thông báo thưởng',	6,	'admin.notification',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-03 02:02:50'),
(42,	'Danh sách sao kê hàng tháng',	3,	'admin.account-statement',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-03 01:42:31'),
(43,	'Danh sách hành trình khách hàng',	NULL,	'admin.customer-journey',	'fa fa-server',	NULL,	NULL,	NULL,	'2021-04-16 09:20:18'),
(44,	'Danh sách kì hạn đầu tư',	4,	'admin.config-term-investment',	'fa fa-cubes',	NULL,	NULL,	NULL,	'2021-04-03 01:55:09');

DROP TABLE IF EXISTS `admin_menu_category`;
CREATE TABLE `admin_menu_category` (
  `menu_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên nhóm menu',
  `menu_category_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên icon',
  `position` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`menu_category_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='Danh sách nhóm menu';

INSERT INTO `admin_menu_category` (`menu_category_id`, `menu_category_name`, `menu_category_icon`, `position`, `created_at`, `updated_at`) VALUES
(1,	'Báo cáo ',	'fa fa-chart-pie',	1,	NULL,	NULL),
(2,	'Danh sách yêu cầu',	'fa fa-shopping-cart',	2,	NULL,	'2021-04-02 10:05:59'),
(3,	'Quản lý tài khoản',	'fa fa-users',	3,	NULL,	'2021-04-02 10:12:40'),
(4,	'Quản lý sản phẩm',	'fa fa-cubes',	4,	NULL,	'2021-04-03 01:55:01'),
(5,	'Quản lý phân quyền',	'fa fa-users-cog',	5,	NULL,	NULL),
(6,	'Quản lý thông báo',	'fa fa-university',	6,	NULL,	'2021-04-03 01:59:09'),
(7,	'Quản lý cấu hình',	'fa fa-list',	7,	NULL,	NULL),
(8,	'Quản lý log',	'fa fa-history',	8,	NULL,	NULL),
(9,	'Quản lý thông tin',	'fa fa-life-ring',	9,	NULL,	NULL);

DROP TABLE IF EXISTS `phone_service`;
CREATE TABLE `phone_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID tự tăng',
  `telco` enum('vina','mobi','viettel','gphone','vnm','australia') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nhà mạng',
  `service_num` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Đầu số',
  `created_at` datetime DEFAULT NULL COMMENT 'Ngày tạo',
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày cập nhật',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `telco_service_num` (`telco`,`service_num`) USING BTREE,
  KEY `service_num` (`service_num`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Đầu số di động';

INSERT INTO `phone_service` (`id`, `telco`, `service_num`, `created_at`, `updated_at`) VALUES
(1,	'mobi',	'070',	'2019-08-28 16:13:01',	'2019-08-27 19:13:01'),
(2,	'mobi',	'079',	'2019-08-28 16:13:18',	'2019-08-27 19:13:18'),
(3,	'mobi',	'077',	'2019-08-28 16:13:28',	'2019-08-27 19:13:28'),
(4,	'mobi',	'076',	'2019-08-28 16:13:35',	'2019-08-27 19:13:35'),
(5,	'mobi',	'078',	'2019-08-28 16:13:41',	'2019-08-27 19:13:41'),
(6,	'mobi',	'089',	'2019-08-28 16:13:51',	'2019-08-27 19:13:51'),
(7,	'mobi',	'090',	'2019-08-28 16:13:57',	'2019-08-27 19:13:57'),
(8,	'mobi',	'093',	'2019-08-28 16:14:05',	'2019-08-27 19:14:05'),
(9,	'vina',	'083',	'2019-08-28 16:14:12',	'2019-08-27 19:14:12'),
(10,	'vina',	'084',	'2019-08-28 16:14:18',	'2019-08-27 19:14:18'),
(11,	'vina',	'085',	'2019-08-28 16:14:24',	'2019-08-27 19:14:24'),
(12,	'vina',	'081',	'2019-08-28 16:14:29',	'2019-08-27 19:14:29'),
(13,	'vina',	'082',	'2019-08-28 16:14:35',	'2019-08-27 19:14:35'),
(14,	'vina',	'088',	'2019-08-28 16:14:41',	'2019-08-27 19:14:41'),
(15,	'vina',	'091',	'2019-08-28 16:14:47',	'2019-08-27 19:14:47'),
(16,	'vina',	'094',	'2019-08-28 16:14:55',	'2019-08-27 19:14:55'),
(17,	'viettel',	'032',	'2019-08-28 16:15:03',	'2019-08-27 19:15:03'),
(18,	'viettel',	'033',	'2019-08-28 16:15:13',	'2019-08-27 19:15:13'),
(19,	'viettel',	'034',	'2019-08-28 16:15:18',	'2019-08-27 19:15:18'),
(20,	'viettel',	'035',	'2019-08-28 16:15:23',	'2019-08-27 19:15:23'),
(21,	'viettel',	'036',	'2019-08-28 16:15:34',	'2019-08-27 19:15:34'),
(22,	'viettel',	'037',	'2019-08-28 16:15:40',	'2019-08-27 19:15:40'),
(23,	'viettel',	'038',	'2019-08-28 16:15:44',	'2019-08-27 19:15:44'),
(24,	'viettel',	'039',	'2019-08-28 16:15:48',	'2019-08-27 19:15:48'),
(25,	'viettel',	'086',	'2019-08-28 16:15:58',	'2019-08-27 19:15:58'),
(26,	'viettel',	'096',	'2019-08-28 16:16:05',	'2019-08-27 19:16:05'),
(27,	'viettel',	'097',	'2019-08-28 16:16:13',	'2019-08-27 19:16:13'),
(28,	'viettel',	'098',	'2019-08-28 16:16:20',	'2019-08-27 19:16:20'),
(29,	'vnm',	'056',	'2019-08-28 16:16:28',	'2019-08-27 19:16:28'),
(30,	'vnm',	'058',	'2019-08-28 16:16:36',	'2019-08-27 19:16:36'),
(31,	'vnm',	'092',	'2019-08-28 16:16:45',	'2019-08-27 19:16:45'),
(32,	'gphone',	'059',	'2019-08-28 16:16:52',	'2019-08-27 19:16:52'),
(33,	'gphone',	'099',	'2019-08-28 16:17:00',	'2019-08-27 19:17:00'),
(34,	'vina',	'087',	'2021-04-20 14:06:45',	'2021-04-20 07:06:45'),
(35,	'vnm',	'052',	'2019-08-28 16:16:45',	'2019-08-27 19:16:45');

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value_vi` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value_en` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES (1, 'Thiết lập thứ hàng thành viên', 'reset_member_ranking', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (2, 'Trạng thái thiết lập thứ hạng', 'actived_loyalty', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (3, 'Từ khóa hot', 'hot_search', 'Kem dưỡng da;Kem Massage;', 'Kem dưỡng da;Kem Massage;', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (4, 'Chi nhánh tự áp dụng khi đặt hàng', 'auto_apply_branch', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (5, 'Giữ điểm khi hủy lịch hẹn', 'save_point_appointment_cancel', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (6, 'Giữ điểm khi hủy đơn hàng', 'save_point_order_cancel', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (7, 'Logo', 'logo', '2', '2', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (8, 'Short logo', 'short_logo', '', '', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (9, 'Giá tiền lẻ', 'decimal_number', '2', '2', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (10, 'Thông tin Ngân hàng Vset', 'bank_vset', '{\r\n    \"company_name\":\"CÔNG TY CỔ PHẦN TẬP ĐOÀN VSETGROUP\",\r\n    \"bank_number\":\"0911007999999\",\r\n    \"bank_info\":\"Ngân hàng Vietcombank - Chi nhánh Tân Sơn Nhất\",\r\n    \"bg_color\":\"D6E9DC\",\r\n    \"warning\":\"Vui lòng chuyển khoản đúng nội dung để xác nhận hợp đồng của quý khách!\"\r\n}', '{\r\n    \"company_name\":\"VSETGROUP GROUP CORPORATION\",\r\n    \"bank_number\":\"0911007999999\",\r\n    \"bank_info\":\"Vietcombank - Tan Son Nhat Branch\",\r\n    \"bg_color\":\"D6E9DC\",\r\n    \"warning\":\"Please transfer the correct content to confirm your contract!\"\r\n}', '2021-03-26 00:02:50');
INSERT INTO `config` VALUES (11, 'Số ngày rút tiền tiết kiệm trước kì hạn', 'withdraw_saving_before', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (12, 'Số ngày rút tiền tiết kiệm đúng kì hạn', 'withdraw_saving_ok', '7', '7', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (13, 'Số ngày rút lãi', 'withdraw_interest', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (14, 'Số ngày rút thưởng', 'withdraw_bonus', '1', '1', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (15, 'Địa chỉ công ty', 'address_company', '{\r\n \"address\" : [\"Đường 123/456 Số 1, Đường CMT8, Quận 7 , TPHCM\",\"12/3/8 Số 345, Đường 678, Quận 1, TPHCM\"]\r\n}', '{\r\n \"address\" : [\"Street 123/456 Number 1, Street CMT8, District 7 , HCM City\",\"12/3/8 Number 345, Street 678, District 1, HCM City\"]\r\n}', '2021-03-29 23:12:21');
INSERT INTO `config` VALUES (16, 'Cảnh báo gia hạn trước khi hết hạn hợp đồng (số ngày)', 'warning_extend', '15', '15', '2021-04-24 10:32:50');
INSERT INTO `config` VALUES (17, 'Hotline', 'hotline', '01653385959', '01653385959', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (18, 'Link chat app', 'chat_app', 'https://m.me/TestVsetGroup', 'https://m.me/TestVsetGroup', '2021-03-25 14:18:24');
INSERT INTO `config` VALUES (19, 'Email nhân viên', 'email_staff', 'ngandtt@pioapps.vn', 'ngandtt@pioapps.vn', '2021-04-12 12:08:04');
INSERT INTO `config` VALUES (20, 'Số điện thoại nhân viên', 'phone_staff', '0967313406', '0967313406', '2021-04-12 12:08:04');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tên trang',
  `route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Route của trang',
  `is_actived` tinyint(1) NULL DEFAULT 1,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 139 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, 'Danh sách nhà đầu tư', 'admin.customer', 1, NULL, NULL);
INSERT INTO `pages` VALUES (2, 'Chi tiết nhà đầu tư', 'admin.customer.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (3, 'Chỉnh sửa nhà đầu tư', 'admin.customer.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (4, 'Danh sách gói', 'admin.product', 1, NULL, NULL);
INSERT INTO `pages` VALUES (5, 'Thêm gói', 'admin.product.add', 1, NULL, NULL);
INSERT INTO `pages` VALUES (6, 'Chi tiết gói', 'admin.product.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (7, 'Chỉnh sửa gói', 'admin.product.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (8, 'Xóa gói', 'admin.product.remove', 1, NULL, NULL);
INSERT INTO `pages` VALUES (9, 'Danh sách nhân viên', 'admin.staff', 1, NULL, NULL);
INSERT INTO `pages` VALUES (10, 'Thêm nhân viên', 'admin.staff.add', 1, NULL, NULL);
INSERT INTO `pages` VALUES (11, 'Chỉnh sửa nhân viên', 'admin.staff.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (12, 'Xóa nhân viên', 'admin.staff.remove', 1, NULL, NULL);
INSERT INTO `pages` VALUES (13, 'Danh sách phòng ban', 'admin.department', 0, NULL, NULL);
INSERT INTO `pages` VALUES (14, 'Thêm phòng ban', 'admin.department.add', 0, NULL, NULL);
INSERT INTO `pages` VALUES (15, 'Chỉnh sửa phòng ban', 'admin.department.edit', 0, NULL, NULL);
INSERT INTO `pages` VALUES (16, 'Xóa phòng ban', 'admin.department.remove', 0, NULL, NULL);
INSERT INTO `pages` VALUES (17, 'Danh sách chức vụ', 'admin.staff-title', 0, NULL, NULL);
INSERT INTO `pages` VALUES (18, 'Thêm chức vụ', 'admin.staff-title.add', 0, NULL, NULL);
INSERT INTO `pages` VALUES (19, 'Chỉnh sửa chức vụ', 'admin.staff-title.edit', 0, NULL, NULL);
INSERT INTO `pages` VALUES (20, 'Xóa chức vụ', 'admin.staff-title.remove', 0, NULL, NULL);
INSERT INTO `pages` VALUES (21, 'Danh sách hợp đồng', 'admin.customer-contract', 1, NULL, NULL);
INSERT INTO `pages` VALUES (22, 'Export hợp đồng', 'admin.customer-contract.export', 1, NULL, NULL);
INSERT INTO `pages` VALUES (23, 'Chi tiết hợp đồng', 'admin.customer-contract.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (24, 'Chỉnh sửa hợp đồng', 'admin.customer-contract.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (25, 'Danh sách yêu cầu mua trái phiếu - tiết kiệm', 'admin.buy-bonds-request', 1, NULL, NULL);
INSERT INTO `pages` VALUES (26, 'Chi tiết yêu cầu mua trái phiếu tiết kiệm', 'admin.buy-bonds-request.view', 1, NULL, NULL);
INSERT INTO `pages` VALUES (27, 'Xác nhận mua trái phiếu - tiết kiệm (Chuyển khoản)', 'admin.buy-bonds-request.createReceipt', 1, NULL, NULL);
INSERT INTO `pages` VALUES (28, 'Xác nhận mua trái phiếu - tiết kiệm (Ví lãi / Ví thưởng)', 'admin.buy-bonds-request.createWallet', 1, NULL, NULL);
INSERT INTO `pages` VALUES (29, 'Danh sách yêu cầu mua dịch vụ', 'admin.order-service', 1, NULL, NULL);
INSERT INTO `pages` VALUES (30, 'Chi tiết yêu cầu mua dịch vụ', 'admin.order-service.view', 1, NULL, NULL);
INSERT INTO `pages` VALUES (31, 'Xác nhận mua dịch vụ (Chuyển khoản)', 'admin.order-service.createReceipt', 1, NULL, NULL);
INSERT INTO `pages` VALUES (32, 'Xác nhận mua dịch vụ (Ví lãi / Ví thưởng)', 'admin.order-service.createWallet', 1, NULL, NULL);
INSERT INTO `pages` VALUES (33, 'Danh sách serial dịch vụ', 'admin.service-serial', 1, NULL, NULL);
INSERT INTO `pages` VALUES (34, 'Chi tiết serial dịch vụ', 'admin.service-serial.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (35, 'Chỉnh sửa serial dịch vụ', 'admin.service-serial.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (36, 'Danh sách yêu cầu rút tiền', 'admin.withdraw-request', 1, NULL, NULL);
INSERT INTO `pages` VALUES (37, 'Chi tiết yêu cầu rút tiền', 'admin.withdraw-request.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (38, 'Xác nhận/Không xác nhận yêu cầu rút tiền', 'admin.withdraw-request.change-status', 1, NULL, NULL);
INSERT INTO `pages` VALUES (39, 'Danh sách hỗ trợ', 'admin.support', 1, NULL, NULL);
INSERT INTO `pages` VALUES (40, 'Chi tiết hỗ trợ', 'admin.support.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (41, 'Chỉnh sửa hỗ trợ', 'admin.support.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (42, 'Xóa hỗ trợ', 'admin.support.remove', 1, NULL, NULL);
INSERT INTO `pages` VALUES (43, 'Danh sách dịch vụ', 'voucher', 1, NULL, NULL);
INSERT INTO `pages` VALUES (44, 'Tạo dịch vụ', 'voucher.create', 1, '0000-00-00 00:00:00', NULL);
INSERT INTO `pages` VALUES (45, 'Chi tiết dịch vụ', 'voucher.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (46, 'Chỉnh sửa dịch vụ', 'voucher.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (47, 'Xóa dịch vụ', 'voucher.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (48, 'Danh sách ngân hàng', 'admin.bank', 1, NULL, NULL);
INSERT INTO `pages` VALUES (49, 'Thêm ngân hàng', 'admin.bank.add-form-bank-new', 1, NULL, NULL);
INSERT INTO `pages` VALUES (50, 'Chi tiết ngân hàng', 'admin.bank.detail-form-bank', 1, NULL, NULL);
INSERT INTO `pages` VALUES (51, 'Chỉnh sửa ngân hàng', 'admin.bank.edit-form-bank', 1, NULL, NULL);
INSERT INTO `pages` VALUES (52, 'Xóa ngân hàng', 'admin.bank.remove', 1, NULL, NULL);
INSERT INTO `pages` VALUES (53, 'Danh sách thông tin hướng dẫn', 'admin.support-management', 0, NULL, NULL);
INSERT INTO `pages` VALUES (54, 'Chi tiết thông tin hướng dẫn', 'admin.support-management.view', 0, NULL, NULL);
INSERT INTO `pages` VALUES (55, 'Danh sách nhóm hướng dẫn', 'admin.faq-group.index', 1, NULL, NULL);
INSERT INTO `pages` VALUES (56, 'Thêm nhóm hướng dẫn', 'admin.faq-group.create', 1, NULL, NULL);
INSERT INTO `pages` VALUES (57, 'Chi tiết nhóm hướng dẫn', 'admin.faq-group.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (58, 'Chỉnh sửa nhóm hướng dẫn', 'admin.faq-group.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (59, 'Xóa nhóm hướng dẫn', 'admin.faq-group.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (60, 'Danh sách hướng dẫn', 'admin.faq.index', 1, NULL, NULL);
INSERT INTO `pages` VALUES (61, 'Tạo hướng dẫn', 'admin.faq.create', 1, NULL, NULL);
INSERT INTO `pages` VALUES (62, 'Chi tiết hướng dẫn', 'admin.faq.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (63, 'Chỉnh sửa hướng dẫn', 'admin.faq.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (64, 'Xóa hướng dẫn', 'admin.faq.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (65, 'Danh sách cấu hình thông báo tự động', 'config', 0, NULL, NULL);
INSERT INTO `pages` VALUES (66, 'Chỉnh sửa cấu hình thông báo tự động', 'config.edit', 0, NULL, NULL);
INSERT INTO `pages` VALUES (67, 'Thay đổi trạng thái cấu hình thông báo tự động', 'config.change-status', 0, NULL, NULL);
INSERT INTO `pages` VALUES (68, 'Danh sách nhóm quyền', 'admin.role-group', 1, NULL, NULL);
INSERT INTO `pages` VALUES (69, 'Tạo nhóm quyền', 'admin.role-group.submitadd', 1, NULL, NULL);
INSERT INTO `pages` VALUES (70, 'Chỉnh sửa nhóm quyền', 'admin.role-group.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (71, 'Danh sách quyền', 'admin.authorization', 0, NULL, NULL);
INSERT INTO `pages` VALUES (72, 'Chỉnh sửa quyền', 'admin.authorization.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (73, 'Danh sách log email', 'admin.log-email', 1, NULL, NULL);
INSERT INTO `pages` VALUES (74, 'Danh sách log thông báo', 'admin.log-noti', 1, NULL, NULL);
INSERT INTO `pages` VALUES (75, 'Chi tiết log thông báo', 'admin.log-noti.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (76, 'Danh sách nhà đầu tư tiềm năng', 'admin.potential-customers', 0, NULL, NULL);
INSERT INTO `pages` VALUES (77, 'Chi tiết nhà đầu tư tiềm năng', 'admin.potential-customers.detail', 0, NULL, NULL);
INSERT INTO `pages` VALUES (78, 'Chỉnh sửa nhà đầu tư tiềm năng', 'admin.potential-customers.edit', 0, NULL, NULL);
INSERT INTO `pages` VALUES (79, 'Danh sách nhà môi giới', 'admin.customer-broker', 0, NULL, NULL);
INSERT INTO `pages` VALUES (80, 'Chi tiết nhà môi giới', 'admin.customer-broker.detail', 0, NULL, NULL);
INSERT INTO `pages` VALUES (81, 'Chỉnh sửa nhà môi giới', 'admin.customer-broker.edit', 0, NULL, NULL);
INSERT INTO `pages` VALUES (82, 'Cấu hình email cc', 'admin.send-email', 1, NULL, NULL);
INSERT INTO `pages` VALUES (83, 'Danh sách loại dịch vụ', 'admin.service_category', 1, NULL, NULL);
INSERT INTO `pages` VALUES (84, 'Tạo loại dịch vụ', 'admin.service_category.submitAdd', 1, NULL, NULL);
INSERT INTO `pages` VALUES (85, 'Chỉnh sửa loại dịch vụ', 'admin.service_category.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (86, 'Xóa loại dịch vụ', 'admin.service_category.remove', 1, NULL, NULL);
INSERT INTO `pages` VALUES (87, 'Danh sách yêu cầu gia hạn hợp đồng', 'admin.extend-contract', 1, NULL, NULL);
INSERT INTO `pages` VALUES (88, 'Chi tiết yêu cầu gia hạn hợp đồng', 'admin.extend-contract.view', 1, NULL, NULL);
INSERT INTO `pages` VALUES (89, 'Xác nhận gia hạn hợp đồng (Chuyển khoản)', 'admin.extend-contract.createReceipt', 1, NULL, NULL);
INSERT INTO `pages` VALUES (90, 'Xác nhận gia hạn hợp đồng (Ví lãi / Ví thưởng)', 'admin.extend-contract.createWallet', 1, NULL, NULL);
INSERT INTO `pages` VALUES (91, 'Danh sách nguồn khách hàng', 'admin.customer-source', 1, NULL, NULL);
INSERT INTO `pages` VALUES (92, 'Thêm nguồn khách hàng', 'admin.customer-source.add', 1, NULL, NULL);
INSERT INTO `pages` VALUES (93, 'Sửa nguồn khách hàng', 'admin.customer-source.edit-submit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (94, 'Xóa nguồn khách hàng', 'admin.customer-source.remove', 1, NULL, NULL);
INSERT INTO `pages` VALUES (95, 'Export danh sách nhà đầu tư', 'admin.customer.export', 1, NULL, NULL);
INSERT INTO `pages` VALUES (96, 'Danh sách popup khuyến mãi', 'admin.banner', 1, NULL, NULL);
INSERT INTO `pages` VALUES (97, 'Tạo popup khuyến mãi', 'addmin.banner.add', 1, NULL, NULL);
INSERT INTO `pages` VALUES (98, 'Danh sách nguồn tặng thưởng', 'admin.link-source', 1, NULL, NULL);
INSERT INTO `pages` VALUES (99, 'Chi tiết cấu hình nguồn tặng thưởng', 'admin.link-source.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (100, 'Danh sách yêu cầu thay đổi thông tin', 'admin.customer-change-request', 1, NULL, NULL);
INSERT INTO `pages` VALUES (101, 'Danh sách nguồn share link', 'admin.refer-source', 1, NULL, NULL);
INSERT INTO `pages` VALUES (102, 'Chỉnh sửa cấu hình nguồn', 'admin.refer-source.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (106, 'Thêm nhóm khách hàng', 'admin.group-customer.create', 1, NULL, NULL);
INSERT INTO `pages` VALUES (107, 'Danh sách nhóm nhân viên', 'admin.group-staff', 1, NULL, NULL);
INSERT INTO `pages` VALUES (108, 'Thêm nhóm nhân viên', 'admin.group-staff.create', 1, NULL, NULL);
INSERT INTO `pages` VALUES (109, 'Xóa nhóm nhân viên', 'admin.group-staff.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (110, 'Chỉnh sửa nhóm nhân viên', 'admin.group-staff.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (112, 'Phân quyền cho nhóm nhân viên', 'admin.group-staff.authorization', 1, NULL, NULL);
INSERT INTO `pages` VALUES (113, 'Chi tiết nhóm khách hàng', 'admin.group-customer.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (114, 'Chỉnh sửa nhóm khách hàng', 'admin.group-customer.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (115, 'Xóa nhóm khách hàng', 'admin.group-customer.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (116, 'Xóa nhóm khách hàng', 'admin.group-customer.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (117, 'Chi tiết nhóm nhân viên', 'admin.group-staff.show', 1, NULL, NULL);
INSERT INTO `pages` VALUES (118, 'Chỉnh sửa nhóm nhân viên', 'admin.group-staff.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (119, 'Xóa nhóm nhân viên', 'admin.group-staff.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (120, 'Danh sách sao kê hàng tháng', 'admin.account-statement', 1, NULL, NULL);
INSERT INTO `pages` VALUES (121, 'Chi tiết sao kê hàng tháng', 'admin.account-statement.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (122, 'Duyệt yêu cầu thay đổi thông tin nhà đầu tư', 'admin.customer-change-request.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (123, 'Danh sách thông báo thưởng', 'admin.notification', 1, NULL, NULL);
INSERT INTO `pages` VALUES (124, 'Tạo thông báo thưởng', 'admin.notification.create', 1, NULL, NULL);
INSERT INTO `pages` VALUES (125, 'Chi tiết thông báo thưởng', 'admin.notification.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (126, 'Chỉnh sửa thông báo thưởng', 'admin.notification.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (127, 'Xóa thông báo thưởng', 'admin.notification.destroy', 1, NULL, NULL);
INSERT INTO `pages` VALUES (128, 'Danh sách sao kê hàng tháng', 'admin.account-statement', 1, NULL, NULL);
INSERT INTO `pages` VALUES (129, 'Chi tiết sao kê hàng tháng', 'admin.account-statement.detail', 1, NULL, NULL);
INSERT INTO `pages` VALUES (130, 'Danh sách hành trình khách hàng', 'admin.customer-journey', 1, NULL, NULL);
INSERT INTO `pages` VALUES (131, 'Tạo hành trình khách hàng', 'admin.customer-journey.add', 1, NULL, NULL);
INSERT INTO `pages` VALUES (132, 'Chỉnh sửa hành trình khách hàng', 'admin.customer-journey.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (133, 'Chính sửa cấu hình thông báo tự động', 'config.edit', 1, NULL, NULL);
INSERT INTO `pages` VALUES (134, 'Báo cáo doanh thu - thanh toán', 'admin.report.revenue', 1, NULL, NULL);
INSERT INTO `pages` VALUES (135, 'Báo cáo trả lãi dự kiến', 'admin.report.expectedInterestPayment', 1, NULL, NULL);
INSERT INTO `pages` VALUES (136, 'Báo cáo lãi suất', 'admin.report.interest', 1, NULL, NULL);
INSERT INTO `pages` VALUES (137, 'Báo cáo doanh thu nhân viên', 'admin.report.sales', 1, NULL, NULL);
INSERT INTO `pages` VALUES (138, 'Báo cáo nhà đầu tư đăng ký từ referral link', 'admin.report.shareLink', 1, NULL, NULL);

DROP TABLE IF EXISTS `payment_method`;
CREATE TABLE `payment_method`  (
  `payment_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method_name_vi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên hình thức thanh toán',
  `payment_method_name_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_type` enum('auto','manual') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại hình thức thanh toán',
  `payment_method_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Hình icon đại diện phương thức thanh toán',
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'trạng thái ',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`payment_method_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'Danh sách phương thức thanh toán' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_method
-- ----------------------------
INSERT INTO `payment_method` VALUES (1, 'Chuyển khoản', 'Transfer', 'manual', '', 1, 1, 1, '2020-08-20 04:09:11', '2020-08-27 03:36:51');
INSERT INTO `payment_method` VALUES (2, 'Ví lãi', 'Interest Wallet', 'auto', '', 1, 1, 1, '2020-08-20 04:09:27', '2020-08-27 03:37:07');
INSERT INTO `payment_method` VALUES (3, 'Ví Thưởng', 'Commission Wallet', 'auto', '', 1, 1, 1, '2020-08-20 04:09:42', '2020-08-27 03:37:22');
INSERT INTO `payment_method` VALUES (4, 'Tiền mặt', 'Cash', 'manual', '', 1, 1, 1, '2021-03-01 15:30:12', '2021-03-01 15:30:15');

DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq`  (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_group` int(11) NULL DEFAULT NULL COMMENT 'nhóm ? nếu type khác faq thì null',
  `faq_type` enum('faq','privacy_policy','terms_use') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'faq : hỏi đáp, policy : chính sach bảo mật, terms : điều khoản sử dụng',
  `faq_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'title',
  `faq_title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `faq_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'nội dung',
  `faq_content_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `faq_position` int(10) UNSIGNED NULL DEFAULT 1 COMMENT 'thứ tự hiển thị',
  `is_actived` tinyint(1) NULL DEFAULT 1,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`faq_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'trung tâm cài đặt hỗ trợ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of faq
-- ----------------------------
INSERT INTO `faq` VALUES (1, 2, 'faq', '1. Giới thiệu về trái phiếu Vsetgroup:', '1.	INTRODUCTION ABOUT VSETGROUP BOND', '<p class=\"MsoNormal\"><span style=\"font-size: 14px;\">Trái phiếu doanh nghiệp (Chứng khoán nợ) là giao dịch giữa người đi vay và người cho vay. Khi mua trái phiếu doanh nghiệp tức là bạn đang cho doanh nghiệp vay một khoản tiền đến một ngày được xác định trước được gọi là ngày đáo hạn. Tất nhiên, bạn sẽ không cho vay miễn phí tiền của mình. Người đi vay phải trả cho bạn một khoản phí với lãi suất được xác định trước để đổi lấy việc được sử dụng tiền của bạn. Việc thanh toán tiền lãi được thực hiện dựa trên thông tin cam kết của đơn vị phát hành trái phiếu.</span></p><p class=\"MsoNormal\"><span style=\"font-size: 14px;\">Căn cứ theo Nghị định số 163/2018/NĐ-CP ngày 04/12/2018 của Chính Phủ về phát hành Trái phiếu Doanh nghiệp, và mục đích bổ sung vốn lưu động thực hiện cho chương trình kế hoạch dài hạn 2020-2025. Tập đoàn Vsetgroup thực hiện việc phát hành Trái Phiếu với:</span></p><p class=\"MsoNormal\"><span style=\"font-size: 14px;\">●      Tổng giá trị đợt phát hành là 163.750.000.000 vnđ (Một trăm sáu mươi ba tỷ bảy trăm năm mươi triệu đồng chẵn)</span></p><p class=\"MsoNormal\"><span style=\"font-size: 14px;\">●      Trái phiếu có kỳ hạn từ 1-5 năm, được hưởng lãi từ ngày phát hành tới ngày đáo hạn hoặc ngày mua trước hạn</span></p><p class=\"MsoNormal\"><span style=\"font-size: 14px;\">●      Tiền lãi được trả định kỳ theo tháng/quý/năm.</span></p><p class=\"MsoNormal\"><span style=\"font-size: 14px;\">●      Trái phiếu được phát hành dưới hình thức bút toán ghi sổ, là loại hình Trái phiếu có thể chuyển đổi thành cổ phiếu phổ thông theo quy định của Tập đoàn Vsetgroup.</span></p>', '<div><p class=\"MsoNormal\" style=\"line-height:normal\">Corporate Bond (Debt Security) is a transaction\r\nbetween a company (issuer) and investors (bondholders). When customers put\r\nmoney in a bond that means they agree to lend a loan to the business until the\r\ndue day called maturity date. Bondholders do not let businesses borrow money\r\nfor free, they receive a fixed income (coupon rate) according to the\r\ncommitments of the issuer.<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\" style=\"line-height:normal\"><o:p>&nbsp;</o:p></p>\r\n\r\n<p class=\"MsoNormal\" style=\"line-height:normal\">Based on The Decree 163/2018/NĐ-CP dated December,\r\n4<sup>th</sup>, 2018, on issuance of corporate bond and for the purpose of\r\nincreasing working capital for 2020- 2025 long- term plan, VSETGROUP releases\r\nbonds with criteria:<o:p></o:p></p>\r\n\r\n<p class=\"MsoNormal\" style=\"line-height:normal\"><o:p>&nbsp;</o:p></p>\r\n\r\n<ul style=\"margin-top:0in\" type=\"disc\">\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\">Total face value: <span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">163.750.000.000\r\n     vnđ (one hundred sixty three billion seven hundred fifty million Vietnam\r\n     Dong)</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">1- 5 year- term with coupon rate\r\n     calculated from purchase day until or before the maturity date</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Monthly/ quarterly/ annually interest\r\n     payment</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Book entry form which can be converted\r\n     into common share according to regulation of VSETGROUP&nbsp;</span><o:p></o:p></li>\r\n</ul></div>', 1, 1, 0, '2020-10-07 15:13:50', 1, '2020-10-08 22:58:45', 1);
INSERT INTO `faq` VALUES (2, 2, 'faq', '2. Cách tính lãi suất cho nhà đầu tư', '2.	HOW TO CALCULATE THE COUPON RATE FOR INVESTORS', '<p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">      </span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">Mức lãi suất cố định từ\r\n15-19,5%/năm<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">      </span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">Nhà đầu tư sẽ được trả lãi định\r\nkỳ theo tháng/quý/năm tùy theo mong muốn cũng như nhu cầu của nhà đầu tư để thực\r\nhiện mua Trái Phiếu có hình thức trả lãi phù hợp.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">      </span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">Với mệnh giá trái phiếu càng\r\ncao, và kỳ hạn càng lớn thì lãi suất nhà đầu tư nhận được sẽ càng lớn tương ứng:</span><span style=\"font-size: 14px;\"> </span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><table class=\"MsoNormalTable\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left: 41pt; border: none;\">\r\n <tbody><tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">Mệnh Giá Trái\r\n  Phiếu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border:solid black 1.0pt;\r\n  border-left:none;mso-border-left-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">Lãi suất/Năm<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border:solid black 1.0pt;\r\n  border-left:none;mso-border-left-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">Tiền lãi nhận/Tháng<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">5 Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">15%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">62.500<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">20 Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">16%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">266.667<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">50 Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">17%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">708.333<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">100 Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">17,5%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">1.458.333<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">200 triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">18%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">3.000.000<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">500 Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">19%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">7.916.667<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">1 Tỷ<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">19,5%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\" style=\"font-size:14px\">16.250.000<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n</tbody></table>', '<p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-Annually constant coupon rate is 15-19.5%.</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-Period of payment is variable in demand and\r\nwant of investors.</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-The higher value and longer term of the bond\r\nis, the higher the interest payment is.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"line-height:normal\"><o:p>&nbsp;</o:p></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><table class=\"MsoNormalTable\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-left: 41pt; border: none;\">\r\n <tbody><tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\">FACE VALUE<o:p></o:p></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border:solid black 1.0pt;\r\n  border-left:none;mso-border-left-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\">COUPON RATE/ YEAR<o:p></o:p></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border:solid black 1.0pt;\r\n  border-left:none;mso-border-left-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\">INTEREST PAYMENT/ MONTH<o:p></o:p></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">5 Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">15%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">62.500<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">20\r\n  Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">16%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">266.667<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">50\r\n  Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">17%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">708.333<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">100\r\n  Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">17,5%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">1.458.333<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">200\r\n  triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">18%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">3.000.000<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">500\r\n  Triệu<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">19%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">7.916.667<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"165\" valign=\"top\" style=\"width:123.75pt;border:solid black 1.0pt;\r\n  border-top:none;mso-border-top-alt:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">1 Tỷ<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"214\" valign=\"top\" style=\"width:160.5pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">19,5%<o:p></o:p></span></p>\r\n  </td>\r\n  <td width=\"221\" valign=\"top\" style=\"width:165.75pt;border-top:none;border-left:\r\n  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;\r\n  mso-border-top-alt:solid black 1.0pt;mso-border-left-alt:solid black 1.0pt;\r\n  padding:5.0pt 5.0pt 5.0pt 5.0pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center;line-height:normal;\r\n  mso-pagination:none;border:none;mso-padding-alt:31.0pt 31.0pt 31.0pt 31.0pt;\r\n  mso-border-shadow:yes\"><span lang=\"vi\">16.250.000<o:p></o:p></span></p>\r\n  </td>\r\n </tr>\r\n</tbody></table>', 2, 1, 0, '2020-10-07 15:20:13', 1, '2020-10-08 23:00:08', 1);
INSERT INTO `faq` VALUES (3, 2, 'faq', '3. Thời gian đầu tư là bao lâu?', '3.	HOW LONG IS THE INVESTMENT TERM?', '<p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">      </span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">Thời gian đầu tư được tính bằng\r\nkỳ hạn Trái Phiếu từ 1-5 năm<o:p></o:p></span></p><p>\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">      </span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">Trừ khi trái phiếu được mua lại\r\nhoặc hủy bỏ trước hạn, Trái phiếu sẽ được hoàn trả bằng 100% mệnh giá vào Ngày\r\nđáo hạn.<o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-The investment term is measured in bond\r\nterms from 1 to 5 years.</span><o:p></o:p></p><p>\r\n\r\n</p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-The bond will be returned 100% face value in\r\nthe maturity date unless it was sold or canceled before the date.</span><o:p></o:p></p>', 3, 1, 0, '2020-10-07 15:21:03', 1, '2020-10-08 23:00:47', 1);
INSERT INTO `faq` VALUES (4, 2, 'faq', '4. Đầu tư vào Trái Phiếu Tập đoàn Vsetgroup, bạn được gì?', '4.	WHAT BENEFIT CUSTOMERS GET FROM VSETGROUP BOND?', '<p class=\"MsoNormal\" style=\"margin-left: 0.5in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;\r\nline-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%;\">Trái phiếu mang tính ổn\r\nđịnh và có lãi suất cao hơn so với tiền gửi ngân hàng.</span><span lang=\"vi\" style=\"font-size:14px;line-height:115%\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 0.5in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;\r\nline-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%;\">Thu nhập từ trái phiếu\r\nlà tiền lãi, khoản thu cố định không phụ thuộc vào kết quả kinh doanh của doanh\r\nnghiệp</span><span lang=\"vi\" style=\"font-size:14px;line-height:115%\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 0.5in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;\r\nline-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%;\">Kết thúc kỳ hạn Trái Phiếu,\r\nkhách hàng được bảo toàn TOÀN BỘ vốn đã đầu tư lại được nhận thêm lãi suất cao.</span><span lang=\"vi\" style=\"font-size:14px;line-height:115%\"><o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px;line-height:115%\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">      </span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Tại VsetGroup bạn hoàn toàn có thể chuyển đổi trái phiếu thành cổ phiếu\r\ntrở thành cổ đông của công ty khi đạt yêu cầu sở hữu theo quy định của\r\nVsetGroup.</span><span lang=\"vi\" style=\"font-size:14px;line-height:115%\"><o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-Get a more stable and higher coupon rate\r\nthan a savings account.</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-Have fixed income from interest payment\r\nwhich is independent of business’s profit.</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-Keep the original capital and receive a sum\r\nof high interest.</span><o:p></o:p></p><p>\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">-Transfer easily bond to common shares and\r\nbecome a shareholder when meeting the requirements.</span><o:p></o:p></p>', 4, 1, 0, '2020-10-07 15:21:52', 1, '2020-10-08 23:01:29', 1);
INSERT INTO `faq` VALUES (5, 2, 'faq', '5. Công ty dùng vốn vào mục đích kinh doanh gì?', '5.	WHAT PURPOSE IS THE CAPITAL USED FOR?', '<p class=\"MsoNormal\"><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Dòng vốn mà Tập Đoàn VsetGroup huy động từ\r\nnguồn Trái Phiếu sẽ được dùng để bổ sung vốn lưu động, thực hiện cho các chương\r\ntrình mở rộng ngành nghề kinh doanh, đồng thời tập trung phát triển các lĩnh vực\r\nchủ lực trong giai đoạn 2020-2025.<o:p></o:p></span></p><p class=\"MsoNormal\"><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">05 dự án và tập đoàn sẽ phát triển trong\r\nkế hoạch mở rộng dài hạn của mình đó là:<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">DỰ ÁN XÂY DỰNG TRUNG TÂM\r\nĐĂNG KIỂM XE CƠ GIỚI VÀ TRUNG TÂM PHỤ TÙNG SỬA CHỮA Ô TÔ.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">DỰ ÁN XÂY DỰNG CHUỖI 50\r\nHỆ THỐNG LÀM ĐẸP VISSA BEAUTY SALON TẠI TP.HCM.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">DỰ ÁN XÂY DỰNG CHUỖI 3\r\nNHÀ HÀNG KARAOKE TIẾP KHÁCH DOANH NGHIỆP TẠI TP.HCM<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">DỰ ÁN XÂY DỰNG CHUỖI 5 CỬA\r\nHÀNG GIẦY DA VÀ PHỤ KIỆN CHO NAM TẠI TP.HCM<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">DỰ ÁN CÔNG TY TÀI CHÍNH\r\nVSET-CREDIT.<o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin-left:.5in;text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">●<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \"Times New Roman\";\">     \r\n</span></span><!--[endif]--><span lang=\"vi\" style=\"font-size:14px; line-height: 115%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">DỰ ÁN CHUỖI 100 CỬA HÀNG\r\nSIÊU THỊ CÂY XANH.<o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">&nbsp;VSETGROUP invests the capital mobilized\r\nfrom bonds into working capital to expand the business areas as well as focus\r\non development of main areas in the 2020- 2025 period.</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><o:p>&nbsp;</o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">5 projects VSETGROUP focus on in the long-\r\nterm expansion- plan:</span><o:p></o:p></p><p class=\"MsoNormal\" style=\"line-height:normal\"><o:p>&nbsp;</o:p></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><ul style=\"margin-top:0in\" type=\"disc\">\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Building Motor Vehicle Register Center\r\n     and Automobile Service &amp; Repair Center Project</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Chain of 50 “VISSA BEAUTY SALON” stores\r\n     in Ho Chi Minh city</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Chain of 3 Restaurants with Karaoke\r\n     service for corporate guests</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Chain of 5 shoes and accessories shops\r\n     for men in Ho Chi Minh city</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">“VSET- CREDIT” financial company project</span><o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"line-height: normal; vertical-align: baseline;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Chain of 100&nbsp; “Siêu thị cây xanh”\r\n     stores&nbsp;</span><o:p></o:p></li>\r\n</ul>', 5, 1, 0, '2020-10-07 15:22:14', 1, '2020-10-08 23:02:23', 1);

SET FOREIGN_KEY_CHECKS = 1;

DROP TABLE IF EXISTS `group_customer`;
CREATE TABLE `group_customer` (
  `group_customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL COMMENT 'Tên nhóm khách hàng',
  `is_active` tinyint(1) DEFAULT 1 COMMENT 'Trạng thái nhóm',
  `is_deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

INSERT INTO `group_customer` (`group_customer_id`, `group_name`, `is_active`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1,	'Mới',	1,	0,	'2021-05-26 13:50:25',	1,	'2021-05-26 13:50:25',	1);


DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE `product_categories` (
  `product_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name_vi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên loại gói tiếng việt',
  `category_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên lại gói tiếng anh ',
  `description_vi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả gói tiếng việt',
  `description_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả gói tiếng anh',
  `is_actived` tinyint(1) DEFAULT 1,
  `is_deleted` tinyint(1) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid category ETL',
  PRIMARY KEY (`product_category_id`) USING BTREE,
  UNIQUE KEY `category_name` (`category_name_vi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='danh sách lại gói';

INSERT INTO `product_categories` (`product_category_id`, `category_name_vi`, `category_name_en`, `description_vi`, `description_en`, `is_actived`, `is_deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`, `slug`, `category_uuid`) VALUES
(1,	'Đầu Tư Trái Phiếu',	'BOND',	NULL,	NULL,	1,	0,	1,	1,	'2020-08-04 21:08:16',	'2021-05-28 04:29:52',	NULL,	''),
(2,	'Đầu Tư Tiết Kiệm',	'Saving',	NULL,	NULL,	1,	0,	1,	1,	'2020-08-04 21:09:44',	'2021-05-28 04:29:52',	NULL,	'');

INSERT INTO `refer_source` (`refer_source_id`, `source`, `source_name`, `updated_at`, `updated_by`) VALUES
(1,	'default',	'Link copy',	NULL,	NULL),
(2,	'facebook',	'Facebook',	NULL,	NULL),
(3,	'zalo',	'Zalo',	NULL,	NULL),
(4,	'twitter',	'Twitter',	NULL,	NULL),
(5,	'wechat',	'Wechat',	NULL,	NULL);

DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên Ngân hàng',
  `bank_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Icon đại diện ngân hàng',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`bank_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Danh sách ngân hàng';

INSERT INTO `bank` (`bank_id`, `bank_name`, `bank_icon`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	'ACB',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20200923/1160083498923092020_product_detail.png',	1,	1,	'2020-09-23 11:23:10',	'2020-09-22 21:23:10'),
(2,	'SACOMBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20200926/6160108775526092020_product_detail.jpg',	2,	5,	'2020-09-26 09:36:01',	'2020-10-16 17:28:21'),
(3,	'VIETCOMBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20200926/5160108780026092020_product_detail.png',	2,	5,	'2020-09-26 09:38:15',	'2020-10-16 17:28:04'),
(4,	'PVCOMBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/0160286811317102020_product_detail.png',	1,	5,	'2020-10-02 21:09:03',	'2020-10-16 17:27:49'),
(5,	'VPBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/6160286829517102020_product_detail.jpg',	5,	5,	'2020-10-17 00:10:57',	'2020-10-16 17:27:36'),
(6,	'TECHCOMBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/0160286845317102020_product_detail.png',	5,	5,	'2020-10-17 00:14:17',	'2020-10-16 17:27:17'),
(7,	'VIETINBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/4160286849417102020_product_detail.png',	5,	5,	'2020-10-17 00:14:57',	'2020-10-16 17:26:59'),
(8,	'AGRIBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/5160286891317102020_product_detail.png',	5,	5,	'2020-10-17 00:22:02',	'2020-10-16 17:26:45'),
(9,	'MBBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/2160286895517102020_product_detail.jpg',	5,	5,	'2020-10-17 00:22:51',	'2020-10-16 17:26:32'),
(10,	'SHINHAN BANK VIETNAM',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/5160286912817102020_product_detail.jpg',	5,	5,	'2020-10-17 00:25:33',	'2020-10-16 17:25:33'),
(11,	'BIDV',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/6160286917817102020_product_detail.png',	5,	5,	'2020-10-17 00:26:19',	'2020-10-16 17:26:19'),
(12,	'ABBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/4160286939517102020_product_detail.png',	5,	5,	'2020-10-17 00:29:56',	'2020-10-16 17:29:56'),
(13,	'SAIGONBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/5160287001917102020_product_detail.png',	5,	5,	'2020-10-17 00:40:30',	'2020-10-16 17:40:30'),
(14,	'PGBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/7160287004217102020_product_detail.jpg',	5,	5,	'2020-10-17 00:40:48',	'2020-10-16 17:40:48'),
(15,	'VIB',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/8160287007717102020_product_detail.png',	5,	5,	'2020-10-17 00:41:25',	'2020-10-16 17:41:25'),
(16,	'DONGABANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/8160287010117102020_product_detail.png',	5,	5,	'2020-10-17 00:41:53',	'2020-10-16 17:41:53'),
(17,	'EXIMBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/6160287013317102020_product_detail.jpg',	5,	5,	'2020-10-17 00:42:36',	'2020-10-16 17:42:36'),
(18,	'VIETBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/1160287016917102020_product_detail.jpg',	5,	5,	'2020-10-17 00:43:05',	'2020-10-16 17:43:05'),
(19,	'HDBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/1160287040417102020_product_detail.jpg',	5,	5,	'2020-10-17 00:43:24',	'2020-10-16 17:46:46'),
(20,	'OCB',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/7160287022017102020_product_detail.jpg',	5,	5,	'2020-10-17 00:43:54',	'2020-10-16 17:43:54'),
(21,	'TPBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/9160287024517102020_product_detail.jpg',	5,	5,	'2020-10-17 00:44:10',	'2020-10-16 17:44:10'),
(22,	'SEABANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/6160287026417102020_product_detail.png',	5,	5,	'2020-10-17 00:44:30',	'2020-10-16 17:44:30'),
(23,	'HSBC',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/1160287029317102020_product_detail.png',	5,	5,	'2020-10-17 00:45:10',	'2020-10-16 17:45:10'),
(24,	'SCB',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/8160287033017102020_product_detail.jpg',	5,	5,	'2020-10-17 00:45:40',	'2020-10-16 17:45:40'),
(25,	'BAOVIETBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/9160287067217102020_product_detail.png',	5,	5,	'2020-10-17 00:49:22',	'2020-10-16 17:51:14'),
(26,	'SHB',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/1160287076617102020_product_detail.jpg',	5,	5,	'2020-10-17 00:52:56',	'2020-10-16 17:52:56'),
(27,	'LIENVIETPOSTBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/1160287079817102020_product_detail.png',	5,	5,	'2020-10-17 00:53:41',	'2020-10-16 17:53:52'),
(28,	'NAMABANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/8160287086517102020_product_detail.jpg',	5,	5,	'2020-10-17 00:54:37',	'2020-10-16 17:54:57'),
(29,	'OCEANBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/9160287091717102020_product_detail.jpg',	5,	5,	'2020-10-17 00:55:28',	'2020-10-16 17:55:28'),
(30,	'CITIBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/2160287104117102020_product_detail.jpg',	5,	5,	'2020-10-17 00:57:26',	'2020-10-16 17:57:26'),
(31,	'MARITIMEBANK',	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/store/20201017/6160287114317102020_product_detail.jpeg',	5,	5,	'2020-10-17 00:59:23',	'2020-10-16 17:59:23');

DROP TABLE IF EXISTS `check_version`;
CREATE TABLE `check_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` enum('android','ios') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Platform',
  `version` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên version',
  `release_date` date NOT NULL COMMENT 'Ngày release',
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Link cập nhật',
  `flag` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: Bắt buộc update; 0: không bắt update',
  `created_at` datetime DEFAULT NULL COMMENT 'Ngày tạo',
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Ngày cập nhật',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Quản lý version của app';

INSERT INTO `check_version` (`id`, `platform`, `version`, `release_date`, `link`, `flag`, `created_at`, `updated_at`) VALUES
(1,	'android',	'1.1.4',	'2021-02-01',	'https://play.google.com/store/apps/details?id=vset.flutter.wao.vset',	1,	'2019-06-29 11:34:24',	'2021-03-24 01:09:16'),
(2,	'ios',	'1.0.7',	'2021-02-01',	'https://apps.apple.com/vn/app/vsbond/id1534681985',	1,	'2019-06-29 11:34:30',	'2021-03-24 01:11:31');

DROP TABLE IF EXISTS `link_source`;
CREATE TABLE `link_source`  (
  `link_source_id` int(11) NOT NULL AUTO_INCREMENT,
  `source` enum('default','facebook','zalo','twitter','wechat') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Chia sẽ từ nguồn',
  `source_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Tên nguồn',
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`link_source_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng quản lý danh sách nguồn đăng ký ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of link_source
-- ----------------------------
INSERT INTO `link_source` VALUES (1, 'default', 'Mặc định', '2021-03-11 21:19:45', 1);
INSERT INTO `link_source` VALUES (2, 'facebook', 'Facebook', '2021-03-11 21:19:45', 1);
INSERT INTO `link_source` VALUES (3, 'zalo', 'Zalo', '2021-03-11 21:19:45', 1);
INSERT INTO `link_source` VALUES (4, 'twitter', 'Twitter', '2021-03-11 21:19:45', 1);
INSERT INTO `link_source` VALUES (5, 'wechat', 'Wechat', '2021-03-11 21:19:45', 1);

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value_vi` text COLLATE utf8_unicode_ci NOT NULL,
  `value_en` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

INSERT INTO `config` (`config_id`, `name`, `key`, `value_vi`, `value_en`, `updated_at`) VALUES
(1,	'Thiết lập thứ hàng thành viên',	'reset_member_ranking',	'1',	'1',	'2021-03-25 07:18:24'),
(2,	'Trạng thái thiết lập thứ hạng',	'actived_loyalty',	'1',	'1',	'2021-03-25 07:18:24'),
(3,	'Từ khóa hot',	'hot_search',	'Kem dưỡng da;Kem Massage;',	'Kem dưỡng da;Kem Massage;',	'2021-03-25 07:18:24'),
(4,	'Chi nhánh tự áp dụng khi đặt hàng',	'auto_apply_branch',	'1',	'1',	'2021-03-25 07:18:24'),
(5,	'Giữ điểm khi hủy lịch hẹn',	'save_point_appointment_cancel',	'1',	'1',	'2021-03-25 07:18:24'),
(6,	'Giữ điểm khi hủy đơn hàng',	'save_point_order_cancel',	'1',	'1',	'2021-03-25 07:18:24'),
(7,	'Logo',	'logo',	'2',	'2',	'2021-03-25 07:18:24'),
(8,	'Short logo',	'short_logo',	'',	'',	'2021-03-25 07:18:24'),
(9,	'Giá tiền lẻ',	'decimal_number',	'2',	'2',	'2021-03-25 07:18:24'),
(10,	'Thông tin Ngân hàng Vset',	'bank_vset',	'{\r\n    \"company_name\":\"CÔNG TY CỔ PHẦN TẬP ĐOÀN VSETGROUP\",\r\n    \"bank_number\":\"0911007999999\",\r\n    \"bank_info\":\"Ngân hàng Vietcombank - Chi nhánh Tân Sơn Nhất\",\r\n    \"bg_color\":\"D6E9DC\",\r\n    \"warning\":\"Vui lòng chuyển khoản đúng nội dung để xác nhận hợp đồng của quý khách!\"\r\n}',	'{\r\n    \"company_name\":\"VSETGROUP GROUP CORPORATION\",\r\n    \"bank_number\":\"0911007999999\",\r\n    \"bank_info\":\"Vietcombank - Tan Son Nhat Branch\",\r\n    \"bg_color\":\"D6E9DC\",\r\n    \"warning\":\"Please transfer the correct content to confirm your contract!\"\r\n}',	'2021-03-25 17:02:50'),
(11,	'Số ngày rút tiền tiết kiệm trước kì hạn',	'withdraw_saving_before',	'1',	'1',	'2021-03-25 07:18:24'),
(12,	'Số ngày rút tiền tiết kiệm đúng kì hạn',	'withdraw_saving_ok',	'7',	'7',	'2021-03-25 07:18:24'),
(13,	'Số ngày rút lãi',	'withdraw_interest',	'1',	'1',	'2021-03-25 07:18:24'),
(14,	'Số ngày rút thưởng',	'withdraw_bonus',	'1',	'1',	'2021-03-25 07:18:24'),
(15,	'Địa chỉ công ty',	'address_company',	'{\r\n \"address\" : [\"Đường 123/456 Số 1, Đường CMT8, Quận 7 , TPHCM\",\"12/3/8 Số 345, Đường 678, Quận 1, TPHCM\"]\r\n}',	'{\r\n \"address\" : [\"Street 123/456 Number 1, Street CMT8, District 7 , HCM City\",\"12/3/8 Number 345, Street 678, District 1, HCM City\"]\r\n}',	'2021-03-29 16:12:21'),
(16,	'Cảnh báo gia hạn trước khi hết hạn hợp đồng (số ngày)',	'warning_extend',	'15',	'15',	'2021-04-24 03:32:50'),
(17,	'Hotline',	'hotline',	'01653385959',	'01653385959',	'2021-03-25 07:18:24'),
(18,	'Link chat app',	'chat_app',	'https://m.me/TestVsetGroup',	'https://m.me/TestVsetGroup',	'2021-03-25 07:18:24'),
(19,	'Email nhân viên',	'email_staff',	'ngandtt@pioapps.vn',	'ngandtt@pioapps.vn',	'2021-04-12 05:08:04'),
(20,	'Số điện thoại nhân viên',	'phone_staff',	'0967313406',	'0967313406',	'2021-04-12 05:08:04');

SET NAMES utf8mb4;

INSERT INTO `config_module` VALUES (1, 'home_dashboard', 'dashboard_index', 'Dashboard Trang chủ', 0, NULL, NULL);
INSERT INTO `config_module` VALUES (2, 'bond_dashboard', 'bond_index', 'Dashboard Trái phiếu', 0, NULL, NULL);
INSERT INTO `config_module` VALUES (3, 'wallet_dashboard', 'wallet', 'Dashboard Ví của tôi', 0, NULL, NULL);
INSERT INTO `config_module` VALUES (4, 'contract_dashboard', 'contract', 'Dashboard Hợp đồng', 0, NULL, NULL);
INSERT INTO `config_module` VALUES (5, 'history_dashboard', 'history', 'Dashboard Lịch sử', 0, NULL, NULL);
INSERT INTO `config_module` VALUES (6, 'support_dashboard', 'notification', 'Dashboard Hỗ trợ', 0, NULL, NULL);

DROP TABLE IF EXISTS `member_levels`;
CREATE TABLE `member_levels` (
  `member_level_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `name_en` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `member_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `range_content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `range_content_en` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `next_level_id` int(11) DEFAULT NULL,
  `code` varchar(20) COLLATE utf8_bin NOT NULL,
  `point` int(11) NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_bin DEFAULT NULL,
  `description_en` text COLLATE utf8_bin DEFAULT NULL,
  `is_actived` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`member_level_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `member_levels` (`member_level_id`, `name`, `name_en`, `slug`, `member_image`, `range_content`, `range_content_en`, `next_level_id`, `code`, `point`, `discount`, `description`, `description_en`, `is_actived`, `created_by`, `updated_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1,	'Mới',	NULL,	'thanh-vien',	NULL,	NULL,	NULL,	NULL,	'member',	0,	0,	'<p class=\"card-text pb-2 mb-2 bo-dot\">Thành viên mới chưa có quyền lợi</p>',	NULL,	0,	1,	28,	'2019-11-14 13:53:33',	'2020-09-14 01:46:53',	0),
(2,	'Bạc',	'Bạc',	'bac',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/silver.png',	'Đầu tư từ 100 triệu đến 500 triệu',	'Đầu tư từ 100 triệu đến 500 triệu',	3,	'silver',	100000,	5,	'<p class=\"card-text pb-2 mb-2 bo-dot\"><span lang=\"VI\">1. <span style=\"font-size: 14px;\">Được chia lãi suất theo đúng cam kết từ </span><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: 14px;\">15-20%/năm</span></span><br><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">2. Được Tập đoàn tặng quà sinh nhật</span></span></span></span><br><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">3. Được đi du lịch 1 lần/năm</span></span><br></span></span><br><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 107%;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><span style=\"font-size: 14px;\">4. Tặng thẻ Bảo </span><span style=\"font-weight: bold; font-size: 14px;\">Hiểm Bảo Việt An Gia – Hạng Bạc</span><span style=\"font-size: 14px;\"> - Quyền lợi sử dụng lên đến </span><span style=\"font-weight: bold; color: rgb(255, 0, 0); font-size: 14px;\">93 triệu/ người/ năm</span></span></span></span><br><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 107%;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">5. Được giảm giá </span><span style=\"font-weight: bold; color: rgb(255, 0, 0); font-size: 14px;\">10%</span><span style=\"font-size: 14px;\"> các dịch vụ VsetGroup đang kinh doanh</span></span><br></span></span></p>',	'<p><span lang=\"VI\">1.&nbsp;</span><span style=\"font-size: 14px;\">Interest rate is divided according to commitment from <b style=\"color: rgb(255, 0, 0);\">15-20% /year</b></span><br><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">2.&nbsp;</span></span></span></span><span style=\"font-size: 14px;\">The Group gave a birthday gift</span><br><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">3.&nbsp;</span></span><span style=\"font-size: 14px;\">Traveling once a year</span><br></span></span><br><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><span style=\"font-size: 14px;\">4.&nbsp;</span></span></span></span><span style=\"font-size: 14px;\">Free <span style=\"font-weight: bold;\">Bao Viet An Gia Insurance - Silver Level</span> - Benefits up to <span style=\"font-weight: bold; color: rgb(255, 0, 0);\">93 million / person / year</span></span><br><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">5.&nbsp;</span></span></span></span><span style=\"font-size: 14px;\">Get a 10% discount on services VsetGroup is doing business</span><br></p>',	1,	1,	1,	'2020-10-15 15:54:03',	'2020-10-15 08:54:03',	0),
(3,	'Vàng',	'Vàng',	'vang',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/gold-1.png',	'Đầu tư từ 500 triệu đến 1 tỷ',	'Đầu tư từ 500 triệu đến 1 tỷ',	4,	'gold',	500000,	10,	'<p class=\"card-text pb-2 mb-2 bo-dot\"><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><span style=\"font-size: 14px;\">1. Được chia lãi suất theo đúng cam kết từ </span><b style=\"color: rgb(255, 0, 0);\"><span style=\"font-size: 14px;\">15-20%/năm</span></b></span></span></span><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 14px;\"><br><br>2. Được Tập đoàn tặng quà sinh nhật</span></span></span><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 14px;\"><br><br>3. Được đi du lịch 1 lần/năm</span><br></span></span><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">4. Tặng thẻ </span><span style=\"font-weight: bold; font-size: 14px;\">Bảo Hiểm Bảo Việt An Gia – Hạng Bạc</span><span style=\"font-size: 14px;\"> - Quyền lợi sử dụng lên đến </span><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: 14px;\">137 triệu/ người/ năm</span></span></span></span><span lang=\"VI\" style=\"line-height: 107%;\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><span style=\"font-size: 14px;\"><br><br>5. Được giảm giá </span><span style=\"font-weight: bold; color: rgb(255, 0, 0); font-size: 14px;\">20%</span><span style=\"font-size: 14px;\"> các dịch vụ VsetGroup đang kinh doanh</span></span><br></span></span></p>',	'<p><span style=\"font-size: 14px;\">1. To be divided in interest rates according to the commitment of <span style=\"font-weight: bold; color: rgb(255, 0, 0);\">15-20% / year</span></span></p><p><span style=\"font-size: 14px;\">2. Received a birthday gift from the Group</span></p><p><span style=\"font-size: 14px;\">3. To travel once a year</span></p><p><span style=\"font-size: 14px;\">4. Gifted <span style=\"font-weight: bold;\">Bao Viet An Gia Insurance Card - Silver Level</span> - Use benefits up to <span style=\"color: rgb(255, 0, 0); font-weight: bold;\">137 million / person / year</span></span></p><p><span style=\"font-size: 14px;\">5. Receive <span style=\"font-weight: bold; color: rgb(255, 0, 0);\">20%</span> discount for services VsetGroup is doing business</span><br></p>',	1,	1,	1,	'2020-10-15 16:07:03',	'2020-10-15 02:09:44',	0),
(4,	'Kim cương',	'Kim cương',	'kim-cuong',	'https://cms-traiphieu.vsetgroup.vn/static/backend/images/diamond.png',	'Đầu tư từ 1 tỷ trở lên',	'Đầu tư từ 1 tỷ trở lên',	NULL,	'diamond',	1000000,	15,	'<p class=\"card-text pb-2 mb-2 bo-dot\"><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><span style=\"font-size: 14px;\">1. Được chia lãi suất theo đúng cam kết từ&nbsp;</span><b style=\"color: rgb(255, 0, 0);\"><span style=\"font-size: 14px;\">15-20%/năm</span></b></span></span></span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 14px;\"><br><br>2. Được Tập đoàn tặng quà sinh nhật</span></span></span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 14px;\"><br><br>3. Được đi du lịch 1 lần/năm</span><br></span></span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><br><span style=\"font-size: 14px;\">4. Tặng thẻ&nbsp;</span><span style=\"font-weight: bold; font-size: 14px;\">Bảo Hiểm Bảo Việt An Gia – Hạng Bạc</span><span style=\"font-size: 14px;\">&nbsp;- Quyền lợi sử dụng lên đến <b style=\"color: rgb(255, 0, 0);\">230</b></span><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: 14px;\">&nbsp;triệu/ người/ năm</span></span></span></span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"line-height: 13.91px;\"><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 15.3333px;\"><span style=\"font-size: 14px;\"><br><br>5. Được giảm giá&nbsp;</span><span style=\"font-weight: bold; color: rgb(255, 0, 0); font-size: 14px;\">20% - 30%</span><span style=\"font-size: 14px;\">&nbsp;các dịch vụ VsetGroup đang kinh doanh</span></span></span></span><br></p>',	'<p><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">1.&nbsp;</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">Đượ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">c chia lãi su</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ấ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">t theo&nbsp;</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">đ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">úng cam k</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ế</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">t t</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ừ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">&nbsp;</span><b style=\"color: rgb(255, 0, 0);\"><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" red;=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">15-20%/n</span><span lang=\"VI\" style=\"font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">ă</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" red;=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">m</span></b><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\"><br><br><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">2.&nbsp;</span></span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">Đượ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">c T</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ậ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">p&nbsp;</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">đ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">oàn t</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ặ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">ng quà sinh nh</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ậ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">t&nbsp;</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\"><br><br><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">3.&nbsp;</span></span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">Đượ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">c&nbsp;</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">đ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">i du l</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ị</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">ch 1 l</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ầ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">n/n</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ă</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">m&nbsp;</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:#050505;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\"><br><br><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">4.&nbsp;</span></span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">Tặng thẻ&nbsp;<b>Bảo Hiểm Bảo Việt An Gia – Hạng Vàng</b>&nbsp;- Quyền lợi sử dụng lên đến</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">&nbsp;</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">&nbsp;</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(255, 0, 0); font-weight: bold;\">230 triệu/người/năm</span><b><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;=\"\" mso-fareast-font-family:calibri;mso-fareast-theme-font:minor-latin;color:red;=\"\" mso-ansi-language:vi;mso-fareast-language:en-us;mso-bidi-language:ar-sa\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\"><br></span></b><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",sans-serif;mso-fareast-font-family:calibri;mso-fareast-theme-font:=\"\" minor-latin;color:#050505;mso-ansi-language:vi;mso-fareast-language:en-us;=\"\" mso-bidi-language:ar-sa\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\"><br><span style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">5.&nbsp;</span></span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">Đượ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">c gi</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ả</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">m giá&nbsp;<b style=\"color: rgb(255, 0, 0);\">20%-30%</b></span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" red;=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">&nbsp;</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">các d</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ị</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">ch v</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">ụ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">&nbsp;VsetGroup&nbsp;</span><span lang=\"VI\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 11.5pt; line-height: 16.4067px; font-family: Calibri, sans-serif; color: rgb(5, 5, 5);\">đ</span><span lang=\"VI\" segoe=\"\" ui=\"\" historic\",=\"\" sans-serif;=\"\" color:=\"\" rgb(5,=\"\" 5,=\"\" 5);=\"\" background-image:=\"\" initial;=\"\" background-position:=\"\" background-size:=\"\" background-repeat:=\"\" background-attachment:=\"\" background-origin:=\"\" background-clip:=\"\" initial;\"=\"\" style=\"font-size: 11.5pt; line-height: 16.4067px;\">ang kinh doanh&nbsp;</span><br></p>',	1,	1,	1,	'2020-10-15 16:11:27',	'2020-10-15 09:11:27',	0);

UPDATE services
JOIN services as service_copy
ON service_copy.service_id = services.service_id
SET services.service_name_vi = service_copy.service_name , services.service_name_en = service_copy.service_name ,
services.description_en = service_copy.description


UPDATE notification
JOIN notification as notification_copy
ON notification_copy.notification_id = notification.notification_id
SET notification.notification_title_vi = notification_copy.notification_title , notification.notification_title_en = notification_copy.notification_title , notification.notification_message_vi = notification_copy.notification_message, notification.notification_message_en = notification_copy.notification_message

UPDATE notification_detail
JOIN notification_detail as notification_detail_copy
ON notification_detail_copy.notification_detail_id = notification_detail.notification_detail_id
SET
notification_detail.content_vi = notification_detail_copy.content ,
notification_detail.content_en = notification_detail_copy.content ,
notification_detail.action_name_vi = notification_detail_copy.action_name,
notification_detail.action_name_en = notification_detail_copy.action_name


UPDATE service_categories
JOIN service_categories as service_categories_copy
ON service_categories_copy.service_category_id = service_categories.service_category_id
SET
service_categories.name_vi = service_categories_copy.name ,
service_categories.name_en = service_categories_copy.name ,
service_categories.slug_vi = service_categories_copy.slug,
service_categories.slug_en = service_categories_copy.slug,
service_categories.description_vi = service_categories_copy.description,
service_categories.description_en = service_categories_copy.description


