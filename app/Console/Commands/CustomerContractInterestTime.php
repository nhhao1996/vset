<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Admin\Jobs\CustomerContractInterestTimeJob;

class CustomerContractInterestTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vset:customer-contract-interest-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new CustomerContractInterestTimeJob());
    }
}
