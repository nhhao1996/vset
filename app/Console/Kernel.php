<?php

namespace App\Console;

use App\Console\Commands\SendNotiNotEvent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\SendMailAuto;
use Modules\Admin\Jobs\AccountStatementJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        SendMailAuto::class,
        SendNotiNotEvent::class,
        SendMailAuto::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('command:sendmailauto')
//            ->everyMinute();

        $schedule->command('vset:customer-contract-interest-month')
            ->daily();

        $schedule->command('vset:customer-contract-interest-day')
            ->daily();

        $schedule->command('vset:account-statement')
            ->daily();

        $schedule->command('vset:divide-dividend')
            ->daily();
        $schedule->command('vset:receive-stock-bonus')
            ->daily();

        $schedule->command('vset:customer-contract-interest-time')
            ->everyFiveMinutes();

        $schedule->command('vset:send-noti')
            ->everyFiveMinutes();

        //add to crontab EDITOR=nano crontab -e
        // * * * * * cd /home/web/public/vset-backend.piotech.xyz && php artisan schedule:run >> /var/log/vset-backend.log
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
