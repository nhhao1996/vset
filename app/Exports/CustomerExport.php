<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                '1',
                '17/09/2019',
                'NGUYỄN VĂN A',
                '1',
                'Khách cũ',
                '0791234567',
                '01/01/1980',
                '72 Trần Trọng Cung',
                'THÊM BẰNG TAY'
            ],
            [
                '2',
                '17/09/2019',
                'NGUYỄN VĂN B',
                '0',
                'Khách cũ',
                '0312345678',
                '01/01/1980',
                '72 Trần Trọng Cung',
                'THÊM BẰNG TAY'
            ],

        ]);
    }

    public function headings(): array
    {
        $export = [
            'STT',
            'NGÀY SD DỊCH VỤ',
            'TÊN KHÁCH HÀNG',
            'GIỚI TÍNH',
            'NHÓM KHÁCH HÀNG',
            'SỐ ĐIỆN THOẠI',
            'NGÀY SINH',
            'ĐỊA CHỈ',
            'GHI CHÚ'
        ];
        return $export;
    }
}
