<?php

namespace App\Imports;

use App\User;
use Carbon\Carbon;
use Modules\Admin\Models\StockChartTable;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use DB;




class StockChartImport implements ToModel, WithStartRow,WithValidation
{
    public function startRow(): int
    {
        return 2;
    }
    public function rules(): array
    {
        return [
//            '0' => Rule::unique("stock_chart",'time'),
        ];
    }

    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        try{
           if(!$row[0] || !$row[1]){
               return null;
           }
//           todo: Check If exsit---------
            $date = Carbon::createFromFormat("Y-m-d H:i", $row[0]);//
            $time = $date->timestamp;
            $datetime = $date->format("Y-m-d H:i:s");
            $instance = DB::table("stock_chart")
                ->where('timestamp',$time)
                ->where("time",$datetime)
            ;
           if(!is_null($instance->first())){
//               dd('instanc: ', $instance);
               $instance->update(['value'=>$row[1]]);
               return;
           }

            $data = [
                "time" => $date,
                "value" => $row[1],
                "timestamp" => $date->timestamp,
                "created_at" => Carbon::now()
            ];
//            dd($data);
            return new StockChartTable($data);
        }
        catch(\Exception $e){
//            dd($e->getMessage());
            return null;
    }

    }
}