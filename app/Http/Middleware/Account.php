<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\Admin\Models\PageTable;

class Account
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->put('currentRouteName',  Route::currentRouteName());

        if (Auth::user()->is_admin == 1) {
            return $next($request);
        }

        $routes = new PageTable();
        $allRoute = $routes->getAllRoute();
        $allRoute[] = 'admin.authorization';
        $allRoute[] = 'admin.authorization.edit';
        $allRoute[] = 'admin.inventory-input.add';
        $allRoute[] = 'admin.inventory-input.edit';
        $allRoute[] = 'admin.inventory-output.add';
        $allRoute[] = 'admin.inventory-output.edit';
        $allRoute[] = 'admin.inventory-transfer.add';
        $allRoute[] = 'admin.inventory-transfer.edit';
        $allRoute[] = 'admin.inventory-checking.add';
        $allRoute[] = 'admin.inventory-checking.edit';
        $currentRouteName = Route::currentRouteName();

        $listRoute[] = 'admin.customer';
        $listRoute[] = 'admin.potential-customers';
        $listRoute[] = 'admin.customer-broker';
        $listRoute[] = 'admin.product';
        $listRoute[] = 'admin.staff';
        $listRoute[] = 'admin.department';
        $listRoute[] = 'admin.staff-title';
        $listRoute[] = 'admin.customer-contract';
        $listRoute[] = 'admin.order-service';
        $listRoute[] = 'admin.service-serial';
        $listRoute[] = 'admin.withdraw-request';
        $listRoute[] = 'admin.support';
        $listRoute[] = 'voucher';
        $listRoute[] = 'admin.bank';
        $listRoute[] = 'admin.role-group';
        $listRoute[] = 'admin.authorization';
        $listRoute[] = 'admin.log-email';
        $listRoute[] = 'admin.log-noti';
        $listRoute[] = 'admin.report.revenue';
        $listRoute[] = 'admin.report.expectedInterestPayment';
        $listRoute[] = 'admin.report.interest';

//        if (!in_array($currentRouteName, $request->session()->get('routeList'))) {
//            return redirect()->route('authorization.not-have-access');
//        }

        if (in_array($currentRouteName, $allRoute)) {
            if (!in_array($currentRouteName, $request->session()->get('routeList'))) {
                foreach ($request->session()->get('routeList') as $item){
                    if (in_array($item,$listRoute)){
                        return redirect()->route($item);
                    }
                }
                return redirect()->route('admin.nothing');
            }
        }
        return $next($request);
    }
}
