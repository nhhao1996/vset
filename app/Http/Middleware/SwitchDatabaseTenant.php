<?php

namespace App\Http\Middleware;

use App\Models\PiospaBrandTable;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\DB;
use MyCore\Helper\OpensslCrypt;

/**
 * Class SwitchDatabaseTenant
 * @package App\Http\Middleware
 * @author DaiDP
 * @since Sep, 2019
 */
class SwitchDatabaseTenant
{
    /**
     * Run the request filter.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        $configDB = $this->configDB($request);
//
//        if ($configDB != 200) {
//            abort($configDB);
//        }


        \Illuminate\Support\Facades\Config::set('config', [
            'logo' => DB::table('config')->select('value_vi')->where('key', 'logo')->first(),
            'short_logo' => DB::table('config')->select('value_vi')->where('key', 'short_logo')->first(),
            'decimal_number' => DB::table('config')->select('value_vi')->where('key', 'decimal_number')->first()
        ]);

        return $next($request);
    }

    /**
     * Parse connect string to array
     *
     * @param $str
     * @return array
     */
    protected function parseConnStr($str)
    {
        $arrPart = explode(';', $str);
        $arrParams = [];
        foreach ($arrPart as $item) {
            list($key, $val) = explode('=', $item, 2);
            $key = strtolower($key);

            $arrParams[$key] = $val;
        }

        return $arrParams;
    }

    protected function configDB($request)
    {
        $domain = $request->getHost();

        $brandCode = preg_replace('/(.+)' . DOMAIN_PIOSPA . '/', '$1', $domain);

        $mPiospaBrand = new PiospaBrandTable();
        $brand = $mPiospaBrand->getBrand($brandCode);

//        // Kiểm tra không tìm thấy cấu hình của tenant thì trả về lỗi 404
        if (empty($brand)) {
            return 404;
        }
//        if (!isset($brand['tenant_id']) || empty($brand['tenant_id'])) {
//            return 404;
//        }

        if (!isset($brand['brand_contr']) || empty($brand['brand_contr'])) {
            return 404;
        }

        $oConstr = new OpensslCrypt(env('OP_SECRET'), env('OP_SALT'));

        $conStr = $oConstr->decode($brand['brand_contr']);

        // Kiểm tra connect string không đủ thông tin bắt buộc thì trả về lỗi 404
        $arrParams = $this->parseConnStr($conStr);

        if (empty($arrParams['server'])
            || empty($arrParams['database'])
            || empty($arrParams['user'])) {
            return 404;
        }

        $idTenant = $brand['brand_contr'];

        session(['idTenant' => $idTenant]);
        // Thiết lập cấu hình database
        config([
            'database.connections.mysql' => [
                'driver' => 'mysql',
                'host' => $arrParams['server'],
                'port' => $arrParams['port'] ?? 3306,
                'database' => $arrParams['database'],
                'username' => $arrParams['user'],
                'password' => $arrParams['password'] ?? '',
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => env('DB_CHARSET', 'utf8mb4'),
                'collation' => env('DB_COLLATION', 'utf8mb4_unicode_ci'),
                'prefix' => env('DB_PREFIX', ''),
                'strict' => env('DB_STRICT_MODE', false),
                'engine' => env('DB_ENGINE', null),
                'timezone' => env('DB_TIMEZONE', '+07:00'),
            ]
        ]);
        \DB::purge('mysql'); // Clear cache config. See: https://stackoverflow.com/a/37705096
        
        \Illuminate\Support\Facades\Config::set('config', [
            'logo' => DB::table('config')->select('value_vi')->where('key', 'logo')->first(),
            'short_logo' => DB::table('config')->select('value_vi')->where('key', 'short_logo')->first(),
            'decimal_number' => DB::table('config')->select('value_vi')->where('key', 'decimal_number')->first()
        ]);

        return 200;
    }
}
