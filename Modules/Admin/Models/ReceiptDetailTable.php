<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 12/5/2018
 * Time: 2:37 PM
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
use Illuminate\Support\Facades\DB;
class ReceiptDetailTable extends Model
{
    use ListTableTrait;
    protected $table = 'receipt_details';
    protected $primaryKey = 'receipt_detail_id';
    protected $fillable = [
        'receipt_detail_id', 'receipt_id', 'cashier_id', 'receipt_type', 'amount', 'note', 'created_by', 'updated_by',
        'created_at', 'updated_at','card_code'
    ];

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->receipt_detail_id;
    }

    public function getItem($id)
    {
        $ds = $this->select(
            'receipt_detail_id',
            'receipt_id',
            'cashier_id',
            'receipt_type',
            'amount',
            'card_code',
            'created_at'
        )
            ->where('receipt_id',$id)->get();
        return $ds;
    }

    public function edit(array $data, $id)
    {
        return $this->where('receipt_detail_id',$id)->update($data);
    }

    public function sumAmmount($id)
    {
        $ds=$this->select('receipt_detail_id','receipt_id',
            DB::raw("SUM(amount) as number"))
            ->groupBy('receipt_id')->where('receipt_id',$id)->get();
        return $ds;
    }

    /**
     * Lấy ds tiền đã thanh toán
     *
     * @return mixed
     */
    public function getAllReceiptDetail()
    {
        return $this
            ->select(
                'receipt_id',
                DB::raw("SUM(amount) as number")
            )
            ->groupBy('receipt_id')
            ->get();
    }

    public function getListReceipt($data) {
        $page    = (int) ($data['page'] ? $data['page'] : 1);
        $display = 10;

        $oSelect = $this
            ->join('receipts','receipts.receipt_id','receipt_details.receipt_id')
            ->join('staffs','staffs.staff_id','receipt_details.staff_id')
            ->where('receipts.object_type',$data['object_type'])
            ->where('receipts.object_id',$data['order_id'])
            ->select(
                'staffs.full_name',
                'receipt_details.*'
            )
            ->orderBy('receipt_details.receipt_detail_id','DESC');

        if ($display > 10) {
            $page = intval(count($oSelect->get()) / $display);
        }
        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function createReceiptDetail($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function sumMoneyReceipt($receipt_id){
        $oSelect = $this
            ->where('receipt_id',$receipt_id)
            ->select(
                DB::raw("SUM(amount) as sum_receipt")
            )
            ->first();
        return $oSelect;
    }

    /**
     * Tất cả order theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByCondition($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            DB::raw('YEAR(receipt_details.created_at) as year'),
            DB::raw('MONTH(receipt_details.created_at) as month')
        )
            ->join(
            'receipts',
            'receipts.receipt_id',
            $this->table . '.receipt_id'
        )
            ->join(
            'orders',
            'orders.order_id',
            'receipts.object_id'
        )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            )
            ->join(
                'product_categories',
                'product_categories.product_category_id',
                'products.product_category_id'
            );
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where(DB::raw('YEAR(receipt_details.created_at)'), '=', $params['year']);
        }
        if (isset($params['object_type'])) {
            $select->where('receipts.object_type', '=', $params['object_type']);
        }
        if (isset($params['type'])) {
            $select->where('products.product_category_id', $params['type']);
        }
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    public function getReportRevenueByMethod($params){
        $select = $this->select(
            'receipt_details.receipt_type',
            DB::raw("count(*) as total")
        )
            ->join(
                'receipts',
                'receipts.receipt_id',
                $this->table . '.receipt_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'receipts.object_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                'orders.customer_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            )
            ->join(
                'product_categories',
                'product_categories.product_category_id',
                'products.product_category_id'
            )
            ->where('customers.is_test','<>',1)
            ->groupBy('receipt_details.receipt_type');

        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at']) && $params['created_at'] != '') {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year']) && $params['year'] != '') {
            $select->where(DB::raw('YEAR(receipt_details.created_at)'), '=', $params['year']);
        }
        if (isset($params['object_type']) && $params['object_type'] != '') {
            $select->where('receipts.object_type', '=', $params['object_type']);
        }
        if (isset($params['type']) && $params['type'] != '') {
            if($params['type'] == 3){
                $select->where('receipts.object_type', '=', 'order-service');
            } else {
                $select->where('receipts.object_type', '=', 'order');
                $select->where('products.product_category_id', $params['type']);
            }
        }

        return $select->get();
    }

    /**
     * VuND
     */
    public function getReportRevenue($params){
        $select = $this->select(
            DB::raw("sum(receipt_details.amount) as 'total'")
        )
            ->join(
                'receipts',
                'receipts.receipt_id',
                $this->table . '.receipt_id'
            )
            ->join(
                'customer_contract',
                'customer_contract.order_id',
                'receipts.object_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                'customer_contract.customer_id'
            )
            ->join(
                'products',
                'products.product_id',
                'customer_contract.product_id'
            )
            ->where('customers.is_test','<>',1)
            ->groupBy('date');

        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at']) && $params['created_at'] != '') {
            $select->addSelect(DB::raw("DATE_FORMAT(receipt_details.created_at, '%Y-%m-%d') as date"));
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year']) && $params['year'] != '') {
            $select->addSelect(DB::raw("DATE_FORMAT(receipt_details.created_at, '%Y-%m-%d') as date"));
            $select->where(DB::raw('YEAR(receipt_details.created_at)'), '=', $params['year']);
        }
        if (isset($params['object_type']) && $params['object_type'] != '') {
            $select->where('receipts.object_type', '=', $params['object_type']);
        }
        if (isset($params['type']) && $params['type'] != '') {
            if($params['type'] == 3){
                $select->where('receipts.object_type', '=', 'order-service');
            } else {
                $select->where('receipts.object_type', '=', 'order');
                $select->where('products.product_category_id', $params['type']);
            }
        }

        return $select->get();
    }

    /**
     * Tất cả order theo điều kiện không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function getByConditionNotTest($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            DB::raw('YEAR(receipt_details.created_at) as year'),
            DB::raw('MONTH(receipt_details.created_at) as month')
        )
            ->join(
                'receipts',
                'receipts.receipt_id',
                $this->table . '.receipt_id'
            )
            ->join(
                'customer_contract',
                'customer_contract.order_id',
                'receipts.object_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                'customer_contract.customer_id'
            )
            ->join(
                'products',
                'products.product_id',
                'customer_contract.product_id'
            )
            ->join(
                'product_categories',
                'product_categories.product_category_id',
                'products.product_category_id'
            );
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where(DB::raw('YEAR(receipt_details.created_at)'), '=', $params['year']);
        }
        if (isset($params['object_type'])) {
            $select->where('receipts.object_type', '=', $params['object_type']);
        }
        if (isset($params['type'])) {
            $select->where('products.product_category_id', $params['type']);
        }
        $select
            ->where('customers.is_test','<>',1)
            ->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }


    /**
     * Tất cả order theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByConditionType($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            DB::raw('YEAR(receipt_details.created_at) as year'),
            DB::raw('MONTH(receipt_details.created_at) as month')
        )
            ->join(
            'receipts',
            'receipts.receipt_id',
            $this->table . '.receipt_id'
        )
            ->join(
            'order_services',
            'order_services.order_service_id',
            'receipts.object_id'
        );
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where(DB::raw('YEAR(receipt_details.created_at)'), '=', $params['year']);
        }
        if (isset($params['object_type'])) {
            $select->where('receipts.object_type', '=', $params['object_type']);
        }
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    /**
     * Tất cả order theo điều kiện không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function getByConditionTypeNotTest($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            DB::raw('YEAR(receipt_details.created_at) as year'),
            DB::raw('MONTH(receipt_details.created_at) as month')
        )
            ->join(
                'receipts',
                'receipts.receipt_id',
                $this->table . '.receipt_id'
            )
            ->join(
                'order_services',
                'order_services.order_service_id',
                'receipts.object_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                'order_services.customer_id'
            );
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where(DB::raw('YEAR(receipt_details.created_at)'), '=', $params['year']);
        }
        if (isset($params['object_type'])) {
            $select->where('receipts.object_type', '=', $params['object_type']);
        }
        $select
            ->where('customers.is_test','<>',1)
            ->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }
}