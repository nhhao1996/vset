<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class CustomerDeviceTable extends Model
{
    use ListTableTrait;
    protected $table = "customer_device";
    protected $primaryKey = "customer_device_id";
    protected $fillable = [
        'customer_device_id',
        'customer_id',
        'imei',
        'model',
        'platform',
        'os_version',
        'app_version',
        'token',
        'date_created',
        'last_access',
        'date_modified',
        'modified_by',
        'created_by',
        'is_actived',
        'is_deleted',
        'endpoint_arn',
    ];

    public $timestamps = false;

    public function deleteDevice($customer_id,$data){
        $oSelect = $this->where('customer_id',$customer_id)->delete();
        return $oSelect;
    }
}