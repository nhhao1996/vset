<?php

namespace Modules\Admin\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerContactLogTable extends Model
{
    protected $table = 'customer_contract_log';
    protected $primaryKey = 'customer_contract_log_id';
    protected $fillable = [
        'customer_contract_log_id',
        'customer_contract_id',
        'customer_id',
        'year',
        'month',
        'day',
        'week',
        'week_year',
        'full_time',
        'interest'
    ];

    public $timestamps = false;

    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;

//    public $timestamps = false;

    /**
     * Thêm 1 lúc nhiều record
     * @param $data
     */
    public function addInsert($data)
    {
        $this->insert($data);
    }

    public function checkContractLog($data) {
        $oSelect = $this
            ->where('customer_contract_id',$data['customer_contract_id'])
            ->where('year',$data['year'])
            ->where('month',$data['month'])
            ->first();
        return $oSelect;
    }

    public function updateContractLog($data,$id){
        $oSelect = $this->where('customer_contract_log_id',$id)->update($data);
        return $oSelect;
    }

    /**
     * Danh sách theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByCondition($params = [])
    {
        $select = $this->select($this->table . '.*',DB::raw('SUM('.$this->table . '.interest)'.' as total_interest_log'))
        ->join(
            'customer_contract',
            'customer_contract.customer_contract_id',
            $this->table . '.customer_contract_id'
        )
        ->where('customer_contract.is_active', self::IS_ACTIVE)
        ->where('customer_contract.is_deleted', self::NOT_DELETE);
        if (isset($params['year'])) {
            $select->where('year', $params['year']);
        }
        if (isset($params['array_month'])) {
            $select->whereIn('month', $params['array_month']);
        }
        if (isset($params['select_type']) && $params['select_type'] == 'day') {
            $select->where('full_time','>=', $params['startTime']);
            $select->where('full_time','<=', $params['endTime']);
            $select->groupBy('full_time');
        }
        if (isset($params['select_type']) && $params['select_type'] == 'week') {
            $select->where('week', $params['week']);
            $select->where('year', $params['year']);
            $select->groupBy('full_time');
        }
        if (isset($params['select_type']) && $params['select_type'] == 'month') {
            $select->where(DB::raw("(DATE_FORMAT(full_time,'%m/%Y') )"),'>=', $params['month_start']);
            $select->where(DB::raw("(DATE_FORMAT(full_time,'%m/%Y') )"),'<=', $params['month_end']);
            $select->groupBy('full_time');
        }
        return $select->get();
    }

    /**
     * Danh sách theo điều kiện không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function getByConditionNotTest($params = [])
    {
        $select = $this
            ->select(
                $this->table . '.*',
                DB::raw('SUM('.$this->table . '.interest)'.' as total_interest_log'),
                'customer_contract.customer_contract_code',
                'customer_contract.customer_name',
                'customer_contract.customer_contract_start_date',
                'customer_contract.customer_contract_end_date'
            )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                $this->table . '.customer_id'
            )
            ->where('customer_contract.is_active', self::IS_ACTIVE)
            ->where('customer_contract.is_deleted', self::NOT_DELETE);
//        if (isset($params['year'])) {
//            $select->where($this->table . '.year', $params['year']);
//        }
        if (isset($params['array_month'])) {
            $select->whereIn($this->table . '.month', $params['array_month']);
        }
        if (isset($params['select_type']) && $params['select_type'] == 'day') {
            $select = $select->where($this->table . '.full_time','>=', $params['startTime']);
            $select = $select->where($this->table . '.full_time','<=', $params['endTime']);
            $select = $select->groupBy($this->table . '.full_time');
        }
        if (isset($params['select_type']) && $params['select_type'] == 'week') {
            if ($params['type_week'] == 'week_now' || $params['type_week'] == 'week_after') {
                $select->where($this->table . '.week_year', $params['week'].'/'.$params['year']);
                if (isset($params['fulltime'])) {
                    $select->where($this->table . '.full_time','>=', $params['fulltime']);
                }
                $select->groupBy($this->table . '.week_year');
            } else if ($params['type_week'] == 'month_after') {

                $select->where($this->table . '.full_time','>=', $params['week_start']);
                $select->where($this->table . '.full_time','<=', $params['week_end']);
//                $select->where($this->table . '.year', $params['year']);
                $select->groupBy($this->table . '.week_year');
            } else if ($params['type_week'] == 'select_week') {
                if (isset($params['week_before']) && isset($params['year_before'])) {
                    if (Carbon::now()->setISODate($params['year_before'],$params['week_before'])->startOfWeek()->format('Y-m-d 00:00:00') > Carbon::now()) {
                        $select->where($this->table . '.full_time','>=', Carbon::now()->setISODate($params['year_before'],$params['week_before'])->startOfWeek()->format('Y-m-d 00:00:00'));
                    } else {
                        $select->where($this->table . '.full_time','>=', Carbon::now());
                    }
//                    $select->where($this->table . '.year','>=', $params['year_before']);
                } else {
                    $select->where($this->table . '.full_time','>=',  Carbon::now());
                }

                if (isset($params['week_after']) && isset($params['year_after'])) {
                    if (Carbon::now()->setISODate($params['year_after'],$params['week_after'])->endOfWeek()->format('Y/m/d 23:59:59') > Carbon::now()) {
                        $select->where($this->table . '.full_time','<=', Carbon::now()->setISODate($params['year_after'],$params['week_after'])->endOfWeek()->format('Y-m-d 23:59:59'));
                    } else {
                        $select->where($this->table . '.full_time','<=', Carbon::now());
                    }
//                    $select->where($this->table . '.year','<=', $params['year_after']);
                } else {
                    $select->where($this->table . '.full_time','<=', Carbon::now());
                }

                $select->groupBy($this->table . '.week_year');
            }
        }
        if (isset($params['select_type']) && $params['select_type'] == 'month') {
            if ($params['month_start'] != null) {
                if (Carbon::parse($params['month_start']) > Carbon::now()){

                    $select->where($this->table .'.full_time','>=', $params['month_start']);
                } else {
                    $select->where($this->table .'.full_time','>=', Carbon::now());
                }

            }
            if ($params['month_end'] != null) {
                if (Carbon::parse($params['month_end']) > Carbon::now()){
                    $select->where($this->table .'.full_time','<=', $params['month_end']);
                } else {
                    $select->where($this->table .'.full_time','<=', Carbon::now());
                }
            }

            $select->groupBy($this->table . '.year',$this->table . '.month');
        }

        return $select
            ->where('customers.is_test','<>',1)
            ->orderBy($this->table .'.full_time')
            ->get();
    }

    /**
     * Danh sách theo điều kiện không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function listByConditionNotTest($params = [])
    {
        $select = $this
            ->select(
                $this->table . '.*',
//                DB::raw('SUM('.$this->table . '.interest)'.' as total_interest_log'),
                $this->table . '.interest',
                'customer_contract.customer_contract_code',
                'customer_contract.customer_name',
                'customer_contract.customer_contract_start_date',
                'customer_contract.customer_contract_end_date',
                'customer_contract.total_amount'
            )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                $this->table . '.customer_id'
            )
            ->where('customer_contract.is_active', self::IS_ACTIVE)
            ->where('customer_contract.is_deleted', self::NOT_DELETE);
//        if (isset($params['year'])) {
//            $select->where($this->table . '.year', $params['year']);
//        }
        if (isset($params['array_month'])) {
            $select->whereIn($this->table . '.month', $params['array_month']);
        }
        if (isset($params['select_type']) && $params['select_type'] == 'day') {
            $select = $select->where($this->table . '.full_time','>=', $params['startTime']);
            $select = $select->where($this->table . '.full_time','<=', $params['endTime']);
//            $select = $select->groupBy($this->table . '.full_time');
        }
        if (isset($params['select_type']) && $params['select_type'] == 'week') {
            if ($params['type_week'] == 'week_now' || $params['type_week'] == 'week_after') {
                $select->where($this->table . '.week_year', $params['week'].'/'.$params['year']);
                if (isset($params['fulltime'])) {
                    $select->where($this->table . '.full_time','>=', $params['fulltime']);
                }
//                $select->groupBy($this->table . '.week_year');
            } else if ($params['type_week'] == 'month_after') {

                $select->where($this->table . '.full_time','>=', $params['week_start']);
                $select->where($this->table . '.full_time','<=', $params['week_end']);
//                $select->where($this->table . '.year', $params['year']);
//                $select->groupBy($this->table . '.week_year');
            } else if ($params['type_week'] == 'select_week') {
                if (isset($params['week_before']) && isset($params['year_before'])) {
                    if (Carbon::now()->setISODate($params['year_before'],$params['week_before'])->startOfWeek()->format('Y-m-d 00:00:00') > Carbon::now()) {
                        $select->where($this->table . '.full_time','>=', Carbon::now()->setISODate($params['year_before'],$params['week_before'])->startOfWeek()->format('Y-m-d 00:00:00'));
                    } else {
                        $select->where($this->table . '.full_time','>=', Carbon::now());
                    }
//                    $select->where($this->table . '.year','>=', $params['year_before']);
                } else {
                    $select->where($this->table . '.full_time','>=',  Carbon::now());
                }

                if (isset($params['week_after']) && isset($params['year_after'])) {
                    if (Carbon::now()->setISODate($params['year_after'],$params['week_after'])->endOfWeek()->format('Y/m/d 23:59:59') > Carbon::now()) {
                        $select->where($this->table . '.full_time','<=', Carbon::now()->setISODate($params['year_after'],$params['week_after'])->endOfWeek()->format('Y-m-d 23:59:59'));
                    } else {
                        $select->where($this->table . '.full_time','<=', Carbon::now());
                    }
//                    $select->where($this->table . '.year','<=', $params['year_after']);
                } else {
                    $select->where($this->table . '.full_time','<=', Carbon::now());
                }

//                $select->groupBy($this->table . '.week_year');
            }
        }
        if (isset($params['select_type']) && $params['select_type'] == 'month') {
            if ($params['month_start'] != null) {
                if (Carbon::parse($params['month_start']) > Carbon::now()){

                    $select->where($this->table .'.full_time','>=', $params['month_start']);
                } else {
                    $select->where($this->table .'.full_time','>=', Carbon::now());
                }

            }
            if ($params['month_end'] != null) {
                if (Carbon::parse($params['month_end']) > Carbon::now()){
                    $select->where($this->table .'.full_time','<=', $params['month_end']);
                } else {
                    $select->where($this->table .'.full_time','<=', Carbon::now());
                }
            }

//            $select->groupBy($this->table . '.year',$this->table . '.month');
        }

        return $select
            ->where('customers.is_test','<>',1)
            ->orderBy($this->table .'.full_time')
            ->get();
    }
}