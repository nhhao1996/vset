<?php
/**
 * Created by PhpStorm.
 * User: SonVeratti
 * Date: 3/17/2018
 * Time: 1:26 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class InvestmentTimeTable extends Model
{
    use ListTableTrait;
    protected $table = 'investment_time';
    protected $primaryKey = 'investment_time_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'investment_time_id', 'product_category_id', 'investment_time_month', 'created_at', 'updated_at', 'created_by',
        'updated_by','is_deleted','is_actived'
    ];

    const IS_DELETED = 0;
    const IS_ACTIVED = 1;

    public function add(array $data)
    {
        $oTitle = $this->create($data);
        return $oTitle->investment_time_id;
    }

    public function _getList(&$filter = [])
    {
        $select = $this
            ->join('product_categories','product_categories.product_category_id',$this->table.'.product_category_id')
            ->select(
                $this->table.'.investment_time_id',
                'product_categories.category_name_vi',
                $this->table.'.investment_time_month',
                $this->table.'.is_deleted',
                $this->table.'.product_category_id'
            );
//            ->where($this->table.'.is_deleted', 0);

        if (isset($filter['product_category_id'])) {
            $select = $select->where('product_categories.product_category_id',$filter['product_category_id']);
            unset($filter['product_category_id']);
        }

        if (isset($filter['is_deleted'])) {
            $select = $select->where($this->table.'.is_deleted',$filter['is_deleted']);
            unset($filter['is_deleted']);
        }
        $select = $select->orderBy('product_categories.product_category_id','ASC')
            ->orderBy($this->table.'.investment_time_month','ASC');
        return $select;
    }

    public function getAllByCategoryProduct($product_category_id) {
        $oSelect = $this
            ->where('product_category_id',$product_category_id)
            ->where('is_deleted',self::IS_DELETED)
            ->where('is_actived',self::IS_ACTIVED)
//            ->orderBy('investment_time_month','ASC')
            ->get();
        return $oSelect;
    }

    public function checkUnique($product_category_id,$investment_time_month,$id = null){
        $oSelect = $this
//            ->where('is_deleted',0)
            ->where('product_category_id',$product_category_id)
            ->where('investment_time_month',$investment_time_month);
        if ($id != null) {
            $oSelect = $oSelect->where('investment_time_id','<>',$id);
        }

        return $oSelect->first();
    }

    public function getDetail($id) {
        $oSelect = $this->where('investment_time_id',$id)->first();
        return $oSelect;
    }

    public function updatedInvestment($data,$id){
        $oSelect = $this->where('investment_time_id',$id)->update($data);
        return $oSelect;
    }

    public function deleteInvestment($id) {
        $oSelect = $this->where('investment_time_id',$id)->delete();
        return $oSelect;
    }
}
//