<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ReferSourceMapServiceTable extends Model
{
    use ListTableTrait;
    protected $table = "refer_source_map_service";
    protected $primaryKey = "refer_source_map_service_id";
    protected $fillable = [
        'refer_source_map_service_id',
        'refer_source_customer_id',
        'service_id',
        'created_at',
        'created_by',
    ];

    public function getListAll(){
        $oSelect = $this->get();
        return $oSelect;
    }

    public function getListService($id){
        $oSelect = $this
            ->join('refer_source_customer','refer_source_customer.refer_source_customer_id','refer_source_map_service.refer_source_customer_id')
            ->join('refer_source','refer_source.refer_source_id','refer_source_customer.refer_source_id')
            ->join('services','services.service_id','refer_source_map_service.service_id')
            ->where('refer_source.refer_source_id',$id)
            ->get();
        return $oSelect;

    }

    public function deleteService($id){
        $oSelect = $this->where('refer_source_customer_id',$id)->delete();
        return $oSelect;
    }

    public function addService($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

}