<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
//Vset
class ProductBonusTable extends Model
{
    use ListTableTrait;
    protected $table = 'product_bonus';
    protected $primaryKey = 'product_bonus_id';

    protected $fillable = [
        'product_bonus_id',
        'product_id',
        'payment_method_id',
        'bonus_rate',
        'investment_time_id',
        'is_deleted',
        'is_actived',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;

    public function getListProductBonus($filter) {

        $page    = (int) ($filter['page'] ? $filter['page'] : 1);
        $display = 10;

        $oSelect = $this
            ->join('payment_method','payment_method.payment_method_id','product_bonus.payment_method_id')
            ->join('investment_time','investment_time.investment_time_id','product_bonus.investment_time_id')
            ->where('product_id',$filter['id'])
            ->orderBy('product_bonus.product_bonus_id','DESC')
            ->select(
                'product_bonus.*',
                'payment_method.payment_method_name_vi',
                'investment_time.investment_time_month'
            );

        if ($display > 10) {
            $page = intval(count($oSelect->get()) / $display);
        }
        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function removeProductBonus($id) {
        $oSelect = $this->where('product_bonus_id',$id)->delete();
        return $oSelect;
    }

    public function checkValue($data) {
        $oSelect = $this
            ->where('product_id',$data['product_id'])
            ->where('payment_method_id',$data['payment_method_id'])
            ->where('investment_time_id',$data['investment_time_id']);
        if (isset($data['product_bonus_id'])) {
            $oSelect = $oSelect->where('product_bonus_id','<>',$data['product_bonus_id']);
        }
        return $oSelect->get();
    }

    public function addProductBonus($data){
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelect = $this->where('product_bonus_id',$id)->first();
        return $oSelect;
    }

    public function updateProductBonus($id,$data) {
        $oSelect = $this
            ->where('product_bonus_id',$id)
            ->update($data);
    }

    public function getListProductBonusByProductId($product_id) {
        $oSelect = $this
            ->where('product_id',$product_id)->get();
        return $oSelect;
    }

    /**
     * Chi tiết thưởng trái phiếu / tiết kiệm
     *
     * @param $categoryId
     * @param $productId
     * @param $paymentMethodId
     * @param $interestTime
     * @return mixed
     */
    public function getBonus($categoryId, $productId, $paymentMethodId, $interestTime)
    {
        $ds = $this
            ->select(
                "{$this->table}.product_id",
                "{$this->table}.payment_method_id",
                "{$this->table}.bonus_rate",
                "{$this->table}.investment_time_id",
                "investment_time.investment_time_month"
            )
            ->leftJoin("investment_time", "investment_time.investment_time_id", "=", "{$this->table}.investment_time_id")
            ->where("{$this->table}.product_id", $productId)
            ->where("{$this->table}.payment_method_id", $paymentMethodId)
            ->where("{$this->table}.is_actived", self::IS_ACTIVE)
            ->where("{$this->table}.is_deleted", self::NOT_DELETE);

        if ($interestTime != 0) {
            $ds->where("investment_time.product_category_id", $categoryId)
                ->where("investment_time.investment_time_id", $interestTime)
                ->where("investment_time.is_actived", self::IS_ACTIVE)
                ->where("investment_time.is_deleted", self::NOT_DELETE);
        }

        return $ds->first();
    }
}