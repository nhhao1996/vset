<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;
//Vset
class PaymentMethodTable extends Model
{
    use ListTableTrait;
    protected $table = 'payment_method';
    protected $primaryKey = 'payment_method_id';
    protected $fillable = [
        'payment_method_id',
        'payment_method_name_vi',
        'payment_method_name_en',
        'payment_method_type',
        'payment_method_image',
        'is_active',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    const IS_ACTIVE = 1;

    public function getAll() {
        $oSelect = $this->where('is_active',self::IS_ACTIVE)->get();
        return $oSelect;
    }

    public function getAllByType($type) {
        $oSelect = $this
            ->where('payment_method_type',$type)
            ->where('is_active',self::IS_ACTIVE)
            ->get();
        return $oSelect;
    }

    /**
     * Dữ liệu của biểu đồ tròn PTTT theo ngày
     *
     * @param array $params
     * @return mixed
     */
    public function getPaymentMethodChart($params = []){
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        switch ($params['type_chart']) {
            case 'revenue':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders", "stock_orders.payment_method_id", "=", "{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
            case 'count-transaction':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders", "stock_orders.payment_method_id", "=", "{$this->table}.payment_method_id")
                ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
            case 'market-transaction':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders", "stock_orders.payment_method_id", "=", "{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","market")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
            case 'transfer-transaction':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders", "stock_orders.payment_method_id", "=", "{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","transfer")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
        }
        return $select->get()->toArray();
    }

    /**
     * Dữ liệu của biểu đồ tròn PTTT theo Tháng
     *
     * @param array $params
     * @return mixed
     */
    public function getPaymentMethodChartMonth($params = []){

        $lang    = \Illuminate\Support\Facades\App::getLocale();
        switch ($params['type_chart']) {
            case 'revenue':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
            case 'count-transaction':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
            case 'market-transaction':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","market")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
            case 'transfer-transaction':
                $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                )
                    ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","transfer")
                        ->where("stock_orders.process_status","=","paysuccess");
                $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                $select = $select->groupBy('stock_orders.payment_method_id');
                break;
        }
        return $select->get()->toArray();
    }

    /**
     * Dữ liệu của biểu đồ cột báo cáo doanh thu thanh toán của cổ phiếu
     *
     * @param array $params
     * @return mixed
     */
    public function getDataStockChart($params = []){
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        if (isset($params['filter_type']) && $params['filter_type'] == 'day') {
            switch ($params['type_chart']){
                case 'revenue':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%d/%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE(stock_orders.created_at)"));
                    $select = $select->orderBy(DB::raw("DATE(stock_orders.created_at)"),'ASC');
                    break;
                case 'count-transaction':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("(COUNT(stock_orders.total_money)) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%d/%m/%Y') as date")
                    )
                    ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                    ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE(stock_orders.created_at)"));
                    $select = $select->orderBy(DB::raw("DATE(stock_orders.created_at)"),'ASC');
                    break;
                case 'market-transaction':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%d/%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","market")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE(stock_orders.created_at)"));
                    $select = $select->orderBy(DB::raw("DATE(stock_orders.created_at)"),'ASC');
                    break;
                case 'transfer-transaction':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%d/%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","transfer")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE(stock_orders.created_at)"));
                    $select = $select->orderBy(DB::raw("DATE(stock_orders.created_at)"),'ASC');
                    break;
                default:
                    $select = $this->select(
                    "{$this->table}.payment_method_id",
                    "{$this->table}.payment_method_name_$lang as payment_method_name",
                    DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                    DB::raw("DATE_FORMAT(stock_orders.created_at,'%d/%m/%Y') as date")
                )
                    ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.process_status","=","paysuccess")
                        ->where("stock_orders.source","=","publisher");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE(stock_orders.created_at)"));
                    $select = $select->orderBy(DB::raw("DATE(stock_orders.created_at)"),'ASC');
                    break;
            }
        }
        if (isset($params['filter_type']) && $params['filter_type'] == 'month') {

            switch ($params['type_chart']){
                case 'revenue':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'] , $params['month_end']]);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"));
                    $select = $select->orderBy(DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"),'ASC');
                    break;
                case 'count-transaction':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("COUNT(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'] , $params['month_end']]);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"));
                    $select = $select->orderBy(DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"),'ASC');
                    break;
                case 'market-transaction':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","market")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'] , $params['month_end']]);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"));
                    $select = $select->orderBy(DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"),'ASC');
                    break;
                case 'transfer-transaction':
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.source","=","transfer")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'] , $params['month_end']]);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"));
                    $select = $select->orderBy(DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"),'ASC');
                    break;
                default:
                    $select = $this->select(
                        "{$this->table}.payment_method_id",
                        "{$this->table}.payment_method_name_$lang as payment_method_name",
                        DB::raw("SUM(CASE
                                        WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                        ELSE stock_orders.total_money
                                    END) as total_revenue"),
                        DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y') as date")
                    )
                        ->rightJoin("stock_orders","stock_orders.payment_method_id","=","{$this->table}.payment_method_id")
                        ->where("stock_orders.process_status","=","paysuccess")
                        ->where("stock_orders.source","=","publisher");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'] , $params['month_end']]);
                    $select = $select->groupBy('stock_orders.payment_method_id',DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"));
                    $select = $select->orderBy(DB::raw("DATE_FORMAT(stock_orders.created_at,'%m/%Y')"),'ASC');
                    break;
            }
        }
        return $select->get()->toArray();
    }

}