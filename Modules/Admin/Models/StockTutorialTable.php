<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockTutorialTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_tutorial";
    protected $primaryKey = "stock_tutorial_key";
    protected $fillable = [
        'stock_tutorial_key',
        'stock_tutorial_content',
        'is_active',
        'is_delete',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    public function getDetail($key) {
        $oSelect = $this->where('stock_tutorial_key',$key)->first();
        return $oSelect;
    }

    public function updateByKey($data,$key){
        $oSelect = $this->where('stock_tutorial_key',$key)->update($data);
        return $oSelect;
    }


}