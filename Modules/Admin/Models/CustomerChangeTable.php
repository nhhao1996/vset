<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class CustomerChangeTable extends Model
{
    use ListTableTrait;
    protected $table = 'customers_change';
    protected $primaryKey = 'customer_change_id';
    protected $fillable = [
        'customer_change_id','customer_id', 'full_name', 'birthday', 'gender', 'phone', 'phone2',
        'phone_verified', 'email', 'email_verified', 'password', 'ic_no', 'ic_date', 'ic_place', 'ic_front_image', 'contact_province_id',
        'contact_province_id', 'contact_district_id', 'contact_address', 'residence_province_id', 'residence_district_id', 'residence_address',
        'bank_id', 'bank_account_no', 'bank_account_name', 'bank_account_branch', 'customer_source_id', 'customer_refer_id', 'customer_refer_code',
        'customer_avatar', 'note', 'zalo', 'facebook', 'customer_code', 'customer_group_id', 'point', 'member_level_id', 'FbId', 'ZaloId', 'is_updated',
        'site_id', 'branch_id', 'postcode', 'date_last_visit', 'is_actived', 'is_deleted', 'created_by', 'updated_by', 'created_at', 'updated_at',
        'is_logout','is_test','type','status','reason_vi','reason_en'
    ];

    /**
     * @param array $filter
     * @return mixed
     */
    protected function _getList(&$filter = [])
    {
        $ds = $this
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=',  'customers_change.customer_source_id')
            ->join('customers','customers.customer_id',$this->table.'.customer_id')
            ->where('customers_change.type','after')
            ->select(
                'customers_change.customer_change_id',
                 'customers_change.customer_id as customer_id',
                 'customers_change.branch_id as branch_id',
                 'customers.full_name as full_name',
                 'customers.phone as phone',
                 'customers.customer_avatar as customer_avatar',
                 'customers_change.birthday as birthday',
                 'customers_change.gender as gender',
                 'customers_change.email as email',
                 'customers_change.phone2 as phone2',
                 'customers_change.phone as phone_login',
                 'customers_change.customer_code as customer_code',
//                 'customers_change.customer_avatar as customer_avatar',
                 'customers_change.created_at as created_at',
                 'customers_change.customer_refer_id',
                 'customers_change.is_test',
                 'customers_change.status',
                 'customers_change.created_at',
                'customer_sources.customer_source_name'
            );


        if (isset($filter['arr_refer_id'])){
            $ds = $ds->whereIn( 'customers_change.customer_id',$filter['arr_refer_id']);
            unset($filter['arr_refer_id']);
        }

        if (isset($filter['introduct_refer_id'])){
            $ds = $ds->where( 'customers_change.customer_refer_id',$filter['introduct_refer_id']);
            unset($filter['introduct_refer_id']);
        }

        $ds->where('customers_change.is_deleted',0)
            ->where( 'customers_change.customer_id', '<>', 1);
//            ->where( 'customers_change.customer_id', 1)
//            ->orderBy( 'customers_change.customer_id', 'desc');

        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $ds->where(function ($query) use ($search) {
                $query->where( 'customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere( 'customers.phone', 'like', '%' . $search . '%')
                    ->where( 'customers_change.is_deleted', 0);
            });
           
        }

        unset($filter['search']);
        if (isset($filter['type_customer']) && $filter['type_customer'] == 1){
            $ds = $ds->whereNotNull('customer_contract.customer_contract_id');
        } else if (isset($filter['type_customer']) && $filter['type_customer'] == 0) {
            $ds = $ds->whereNull('customer_contract.customer_contract_id');
        }

        if(isset($filter['customer_source_id'])) {
            $ds = $ds->where('customer_sources.customer_source_id',$filter['customer_source_id']);
            unset($filter['customer_source_id']);
        }

        if(isset($filter['status'])) {
            $ds = $ds->where($this->table.'.status',$filter['status']);
            unset($filter['status']);
        }

        if (isset($filter['created'])){
            $day = $filter['created'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');

            $ds = $ds
                ->where( 'customers_change.created_at','>=', $startTime)
                ->where( 'customers_change.created_at','<=', $endTime);
            unset($filter['created']);
        }

//        $ds = $ds->groupBy( 'customers_change.customer_id');
        unset($filter['type_customer']);
        // dd($ds -> toSql());
        return $ds->orderBy('customers_change.updated_at','DESC');
    }

    /**
     * export dữ liệu
     * @param array $filter
     * @return mixed
     */
    public function getListExport($filter)
    {

        $ds = $this
            ->leftJoin('customer_contract', 'customer_contract.customer_id', '=',  'customers_change.customer_id')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=',  'customers_change.customer_source_id')
            ->leftJoin('customers as broker', 'broker.customer_refer_id', '=',  'customers_change.customer_id')
            ->select(
                'customers_change.customer_change_id',
                'customers_change.customer_id as customer_id',
                'customers_change.branch_id as branch_id',
                'customers.full_name as full_name',
                'customers.phone as phone',
                'customers_change.birthday as birthday',
                'customers_change.gender as gender',
                'customers_change.email as email',
                'customers_change.phone2 as phone2',
                'customers_change.phone as phone_login',
                'customers_change.customer_code as customer_code',
                'customers_change.customer_avatar as customer_avatar',
                'customers_change.created_at as created_at',
                'customers_change.customer_refer_id',
                'customers_change.is_test',
                'customers_change.status',
                'customer_sources.customer_source_name',
                DB::raw("SUM(customer_contract.total_amount) as sum_money_contract"),
                 'customers_change.is_actived'
            );
        if(isset($filter['type_customer']) && $filter['type_customer'] == 'potential'){
            $ds->whereNotExists(function ($ds){
                $ds->select(DB::raw(1))
                    ->from('customer_contract')
                    ->whereRaw('customer_contract.customer_id = customers.customer_id');
            });
            unset($filter['type_customer']);
        }

        if(isset($filter['type_customer']) && $filter['type_customer'] == 'broker'){
            $ds = $ds->whereNotNull( 'customers_change.customer_refer_id')->groupBy( 'customers_change.customer_refer_id');
            unset($filter['type_customer']);
        } else {
            $ds = $ds->groupBy( 'customers_change.customer_id');
        }

        if (isset($filter['arr_refer_id'])){
            $ds = $ds->whereIn( 'customers_change.customer_id',$filter['arr_refer_id']);
            unset($filter['arr_refer_id']);
        }

        if (isset($filter['introduct_refer_id'])){
            $ds = $ds->where( 'customers_change.customer_refer_id',$filter['introduct_refer_id']);
            unset($filter['introduct_refer_id']);
        }

        $ds = $ds->where( 'customers_change.is_deleted', 0)
            ->where( 'customers_change.customer_id', '<>', 1);
//            ->where( 'customers_change.customer_id', 1)
//            ->orderBy( 'customers_change.customer_id', 'desc');

        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $ds->where(function ($query) use ($search) {
                $query->where( 'customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere( 'customers.phone', 'like', '%' . $search . '%')
                    ->where( 'customers_change.is_deleted', 0);
            });
        }

        unset($filter['search']);
        if (isset($filter['type_customer']) && $filter['type_customer'] == 1){
            $ds = $ds->whereNotNull('customer_contract.customer_contract_id');
        } else if (isset($filter['type_customer']) && $filter['type_customer'] == 0) {
            $ds = $ds->whereNull('customer_contract.customer_contract_id');
        }

        if(isset($filter['customer_source_id'])) {
            $ds = $ds->where('customer_sources.customer_source_id',$filter['customer_source_id']);
            unset($filter['customer_source_id']);
        }

        if (isset($filter['created'])){
            $day = $filter['created'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');

            $ds = $ds
                ->where( 'customers_change.created_at','>=', $startTime)
                ->where( 'customers_change.created_at','<=', $endTime);
            unset($filter['created']);
        }

        if (isset($filter['is_broker'])){
            $ds = $ds->whereNotNull('broker.customer_id');
            unset($filter['is_broker']);
        }

        if (isset($filter['is_test'])){
            $ds = $ds->where( 'customers_change.is_test',1);
            unset($filter['is_test']);
        }

        $ds = $ds->groupBy( 'customers_change.customer_id');
        unset($filter['type_customer']);
        return $ds->orderBy('customers_change.customer_change_id','DESC')->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->customer_id;
    }

    public function getCustomerSearch($data)
    {
        $select = $this->select('customer_id', 'full_name', 'phone1', 'customer_avatar', 'account_money', 'address')
//            ->where('full_name', 'like', '%' . $data . '%')
//            ->orWhere('phone1', 'like', '%' . $data . '%')
            ->where(function ($query) use ($data) {
                $query->where('full_name', 'like', '%' . $data . '%')
                    ->orWhere('phone1', 'like', '%' . $data . '%')
                    ->orWhere('phone', 'like', '%' . $data . '%');
            })
            ->where('is_deleted', 0)
            ->where('is_actived', 1);
//        if (Auth::user()->is_admin != 1) {
//            $select->where('branch_id', Auth::user()->branch_id);
//        }
        return $select->paginate(6);
    }

    /**
     * @param $id
     * @return
      */
    public function getItem($id)
    {
        $get = $this
//            ->leftJoin('customer_groups as group', 'group.customer_group_id', '=',  'customers_change.customer_group_id')
            ->leftJoin('customer_sources as source', 'source.customer_source_id', '=',  'customers_change.customer_source_id')
//            ->leftJoin('province', 'province.provinceid', '=',  'customers_change.province_id')
//            ->leftJoin('district', 'district.districtid', '=',  'customers_change.district_id')
//            ->leftJoin('member_levels', 'member_levels.member_level_id', '=',  'customers_change.member_level_id')
            ->select(
//                 'customers_change.customer_group_id as customer_group_id',
//                'group.group_name as group_name',
//                 'customers_change.full_name as full_name',
//                 'customers_change.customer_code as customer_code',
//                 'customers_change.gender as gender',
//                 'customers_change.phone as phone1',
//                'province.name as province_name',
//                'province.type as province_type',
//                'district.name as district_name',
//                'district.type as district_type',
//                 'customers_change.contact_address as address',
//                 'customers_change.email as email',
//                 'customers_change.customer_source_id as customer_source_id',
//                 'customers_change.birthday as birthday',
//                'source.customer_source_name',
//                 'customers_change.customer_refer_id',
//                 'customers_change.facebook as facebook',
//                 'customers_change.zalo as zalo',
//                 'customers_change.note as note',
//                 'customers_change.customer_id as customer_id',
//                 'customers_change.is_actived as is_actived',
//                 'customers_change.phone2 as phone2',
//                 'customers_change.customer_avatar as customer_avatar',
//                 'customers_change.created_at as created_at',
//                 'customers_change.account_money as account_money',
//                 'customers_change.province_id as province_id',
//                 'customers_change.district_id as district_id',
//                 'customers_change.point as point',
//                 'customers_change.member_level_id as member_level_id',
//                'member_levels.name as member_level_name',
//                 'customers_change.point as point',
//                'member_levels.discount as member_level_discount',
//                "{$this->table}.postcode"
                )
            ->where( 'customers_change.customer_id', $id);
//        if (Auth::user()->is_admin != 1) {
//            $get->where( 'customers_change.branch_id', Auth::user()->branch_id);
//        }
        return $get->first();
    }


    /**
     * @param $id
     */
    public function getItemRefer($id)
    {
        $get = $this->Join('customers as cs', 'cs.customer_refer_id', '=',  'customers_change.customer_id')
            ->select( 'customers_change.full_name as full_name_refer',  'customers_change.customer_id as customer_id')
            ->where('cs.customer_id', $id)->first();
        return $get;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $filters)
    {
        return $this->where($filters)->where('status','new')->update($data, $filters);
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $this->where('customer_id', $id)->update(['is_deleted' => 1]);
    }

    /**
     * @return mixed
     */
    public function getCustomerOption()
    {
        return $this->select('full_name', 'customer_code', 'customer_id', 'phone')
            ->where('is_actived', 1)
            ->where('is_deleted', 0)
            ->where('customer_id', '!=', 1)->get()->toArray();
    }

    /**
     * @param $phone
     * @param $id
     * @return mixed
     * Kiểm tra số điện thoại đã tồn tại chưa
     */
    public function testPhone($phone, $id)
    {
        return $this->where(function ($query) use ($phone) {
            $query->where('phone', '=', $phone)
                ->orWhere('phone2', '=', $phone);
        })->where('customer_id', '<>', $id)
            ->where('is_deleted', 0)->first();
    }

    public function searchPhone($phone)
    {
        $select = $this->select('customer_id', 'full_name', 'phone')
            ->where('phone', 'like', '%' . $phone . '%')
            ->where('is_actived', 1)
            ->where('is_deleted', 0);
        return $select->get();
    }

    /**
     * @param $phone
     * @return mixed
     * Lấy danh sách sđt khách hàng
     */
    public function getCusPhone($phone)
    {
        $select = $this->select('customer_id', 'full_name', 'phone')
            ->where('phone', $phone)
            ->where('is_actived', 1)
            ->where('is_deleted', 0);
        return $select->first();
    }

    public function getCusPhone2($phone)
    {
        $select = $this->select('customer_id', 'full_name', 'phone')
            ->where('phone', $phone)
            ->where('is_deleted', 0);
        return $select->first();
    }

    /**
     * @return mixed
     * Tổng số khách hàng từ năm hiện tại trở về trước
     */
    public function totalCustomer($yearNow)
    {

        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('YEAR(created_at)'), '<=', $yearNow)
            ->where('is_deleted', 0)->get();
        return $ds;
    }

    /**
     * @param $dayNow
     * @return mixed
     * Tổng số khách hàng đã tạo trong năm hiện tại
     */
    public function totalCustomerNow($yearNow)
    {

        $ds = $this->select(DB::raw('count(customer_id) as number'))
//            ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') = DATE_FORMAT('$yearNow','%Y-%m-%d')")
            ->whereRaw("YEAR(created_at)=$yearNow")
            ->where('is_deleted', 0)
            ->get();
        return $ds;
    }

    /**
     * @param $year
     * @param $branch
     * @return mixed
     * Tổng số khách hàng trong năm hiện tại trở về trước và chi nhánh
     */
    public function filterCustomerYearBranch($year, $branch)
    {

        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('YEAR(created_at)'), '<=', $year)->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    /**
     * @param $year
     * @param $branch
     * @return mixed
     * Tổng số khách hàng trong năm hiện tại và chi nhánh
     */
    public function filterNowCustomerBranch($year, $branch)
    {
        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('YEAR(created_at)'), '=', $year)->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    /**
     * @param $time
     * @param $branch
     * @return mixed
     * Tổng số KH từ thời gian endTime trở về trước và chi nhánh
     */
    public function filterTimeToTime($time, $branch)
    {

        $arr_filter = explode(" - ", $time);
        $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
        $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('DATE(created_at)'), '<=', $endTime)->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    /**
     * @param $time
     * @param $branch
     * @return mixed
     * Tổng số KH từ khoản thời gian start time và end time
     */
    public function filterTimeNow($time, $branch)
    {
        $arr_filter = explode(" - ", $time);
        $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
        $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->whereBetween(DB::raw('DATE(created_at)'), [$startTime, $endTime])
            ->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    public function searchCustomerEmail($data, $birthday, $gender)
    {
        $select = $this->leftJoin('branches', 'branches.branch_id', '=',  'customers_change.branch_id')
            ->select( 'customers_change.customer_id',
                 'customers_change.full_name',
                 'customers_change.phone',  'customers_change.birthday',
                 'customers_change.gender', 'branches.branch_name',  'customers_change.email')
            ->where( 'customers_change.is_actived', 1)
            ->where( 'customers_change.is_deleted', 0)
            ->where( 'customers_change.customer_id', '<>', 1);
        if ($data != null) {
            $select->where(function ($query) use ($data, $birthday, $gender) {
                $query->where( 'customers_change.full_name', 'like', '%' . $data . '%')
                    ->orWhere( 'customers_change.phone', 'like', '%' . $data . '%')
                    ->orWhere( 'customers_change.email', 'like', '%' . $data . '%');
            });
        }
        if ($birthday != null) {
            $select->where( 'customers_change.birthday', $birthday);
        }
        if ($gender != null) {
            $select->where( 'customers_change.gender', $gender);
        }

        return $select->get();
    }

    public function getBirthdays()
    {
        $select = $this->whereMonth('birthday', '=', date('m'))
            ->whereDay('birthday', '=', date('d'))
            ->where('is_deleted', 0)
            ->where('is_actived', 1)
            ->where('phone1', '<>', null)->get();
        return $select;
    }

    //search dashboard
    public function searchDashboard($keyword)
    {
        $select = $this->select(
            'customer_id',
            'full_name',
            'phone',
             'customers_change.email as email',
            'branches.branch_name as branch_name',
             'customers_change.updated_at as updated_at',
            'customer_avatar',
            'customer_id',
            'group_name',
            'customer_avatar'
        )
            ->leftJoin('branches', 'branches.branch_id', '=',  'customers_change.branch_id')
            ->leftJoin('customer_groups', 'customer_groups.customer_group_id', '=',  'customers_change.customer_group_id')
            ->where(function ($query) use ($keyword) {
                $query->where( 'customers_change.full_name', 'like', '%' . $keyword . '%')
                    ->orWhere( 'customers_change.phone', 'like', '%' . $keyword . '%')
                    ->orWhere( 'customers_change.email', 'like', '%' . $keyword . '%');
            })
            ->where( 'customers_change.is_deleted', 0)
            ->where( 'customers_change.is_actived', 1)
            ->where( 'customers_change.customer_id', '<>', 1)
            ->get();
        return $select;
    }

    /**
     * Báo cáo công nợ theo khách hàng
     *
     * @param $id_branch
     * @param $time
     * @param $top
     * @return mixed
     */
    public function reportCustomerDebt($id_branch, $time, $top)
    {
        $ds = $this
            ->leftJoin('customer_debt', 'customer_debt.customer_id', '=',  'customers_change.customer_id')
            ->leftJoin('branches', 'branches.branch_id', '=',  'customers_change.branch_id')
            ->select(
                 'customers_change.full_name',
                'customer_debt.debt_type',
                'customer_debt.status',
                'customer_debt.amount',
                'customer_debt.amount_paid'
            );
        if (isset($id_branch)) {
            $ds->where('branches.branch_id', $id_branch);
        }
        if (isset($time) && $time != "") {
            $arr_filter = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $ds->whereBetween('customer_debt.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
        }
        return $ds->get();
    }

    protected function getListCore(&$filters = [])
    {
        $oSelect = $this
            ->leftJoin(
                'branches', 'branches.branch_id', '=',  'customers_change.branch_id'
            )
            ->leftJoin(
                'customer_groups', 'customer_groups.customer_group_id', '=',
                 'customers_change.customer_group_id'
            )
            ->select(
                 'customers_change.customer_id as customer_id',
                 'customers_change.branch_id as branch_id',
                 'customers_change.full_name as full_name',
                 'customers_change.birthday as birthday',
                 'customers_change.gender as gender',
                 'customers_change.email as email',
                 'customers_change.phone as phone1',
                 'customers_change.customer_code as customer_code',
                'customer_groups.group_name as group_name',
                 'customers_change.customer_avatar as customer_avatar',
                 'customers_change.created_at as created_at',
                 'customers_change.updated_at as updated_at',
                 'customers_change.customer_group_id as customer_group_id',
                'branches.branch_name as branch_name',
                 'customers_change.is_actived'
            )
            ->where( 'customers_change.is_deleted', 0)
            ->where( 'customers_change.customer_id', '<>', 1)
            ->orderBy( 'customers_change.customer_id', 'desc');
        if (isset($filters['arrayUser'])) {
            $oSelect->whereIn($this->table . '.phone', $filters['arrayUser']);
            unset($filters['arrayUser']);
        }
        return $oSelect;
    }

    public function getCustomerInGroupAuto($arrayCondition)
    {
        $select = $this->select( 'customers_change.customer_id')->where(
             'customers_change.is_deleted', 0
        )->leftJoin(
            'customer_appointments',
            'customer_appointments.customer_id', '=',
             'customers_change.customer_id'
        );
        foreach ($arrayCondition as $key => $value) {
            if ($key == 1) {
                $select->leftJoin(
                    'customer_group_define_detail',
                    'customer_group_define_detail.phone', '=',
                     'customers_change.phone'
                )->orWhere('customer_group_define_detail.id', $value);
            } elseif ($key == 2) {
                $select->orWhere('customer_appointments.date', '>=' , $value);
            } elseif ($key == 3) {
               $select->orWhere('customer_appointments.status', '=' , $value);
            } elseif ($key == 4) {
               $select->orWhereBetween('customer_appointments.time', [$value['hour_from'], $value['hour_to']]);
            }
        }
        return $select->get();
    }

    public function getCustomerNotAppointment()
    {
        $select = $this->select( 'customers_change.customer_id', 'customer_appointments.customer_appointment_id')
            ->leftJoin(
                'customer_appointments',
                'customer_appointments.customer_id',
                 'customers_change.customer_id'
            )
            ->where( 'customers_change.is_deleted', 0)
            ->where( 'customers_change.is_actived', 1)
            ->get();
        return $select;
    }

    public function getCustomerUseService($arrService, $where)
    {
        $select = $this->select(
             'customers_change.customer_id',
            'orders.order_id',
            'order_details.object_type',
            'order_details.object_id',
            'orders.process_status'
        )
            ->leftJoin('orders','orders.customer_id', '=',  'customers_change.customer_id')
            ->leftJoin('order_details','order_details.order_id', '=', 'orders.order_id')
            ->where( 'customers_change.is_deleted', 0)
            ->where( 'customers_change.is_actived', 1);
        return $select->get();
    }

    /**
     * Lấy List full khách hàng
     *
     * @return mixed
     */
    public function getAllCustomer()
    {
        $ds = $this->select(
            'customer_id',
            'full_name',
            'phone',
            'gender',
            'member_level_id'
        )
            ->where('customer_id', '!=',1)
            ->where('is_deleted', 0)
            ->get();
        return $ds;
    }


    /**
     * Lấy List full khách hàng  for withdraw request
     *
     * @return mixed
     */
    public function getAllCustomerWithdraw()
    {
        $ds = $this->select(
            'customer_id',
            'full_name'
        )
            ->where('customer_id', '!=',1)
            ->where('is_deleted', 0)
            ->get();
        return $ds;
    }

    /** V_Set get detail customer
     * @param int $id
     * @return mixed
    */
    public function vSetGetDetailCustomerChange($id)
    {
        $oSelect = $this
            ->leftJoin('customers as customer_refer', 'customer_refer.customer_refer_code', '=', $this->table.'.customer_code')
            ->leftJoin('bank', 'bank.bank_id', '=', $this->table.'.bank_id')
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', '=', $this->table.'.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', '=', $this->table.'.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', '=', $this->table.'.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', '=', $this->table.'.residence_district_id')
            ->leftJoin('member_levels', 'member_levels.member_level_id', '=', $this->table.'.member_level_id')
            ->select(
                $this->table.'.*',
//                'customer_code.code',
                'bank.bank_name',
                'customer_refer.full_name as customer_refer_name',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'member_levels.name as member_levels_name',
                $this->table.'.contact_province_id',
                $this->table.'.contact_district_id',
                $this->table.'.contact_address',
                $this->table.'.residence_province_id',
                $this->table.'.residence_district_id',
                $this->table.'.residence_address',
                $this->table.'.is_test'
            )
            ->where( 'customers_change.customer_change_id','<',$id)
            ->orderBy('customers_change.customer_change_id','DESC')
            ->limit(1);

        return $oSelect->first();
    }
        /** V_Set get detail customer After
     * @param int $id
     * @return mixed
    */
    public function vSetGetDetailCustomerChangeAfter($id)
    {
        $oSelect = $this
            ->leftJoin('customers as customer_refer', 'customer_refer.customer_refer_code', '=', $this->table.'.customer_code')
            ->leftJoin('bank', 'bank.bank_id', '=', $this->table.'.bank_id')
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', '=', $this->table.'.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', '=', $this->table.'.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', '=', $this->table.'.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', '=', $this->table.'.residence_district_id')
            ->leftJoin('member_levels', 'member_levels.member_level_id', '=', $this->table.'.member_level_id')
            ->select(
                $this->table.'.*',
//                'customer_code.code',
                'bank.bank_name',
                'customer_refer.full_name as customer_refer_name',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'member_levels.name as member_levels_name',
                $this->table.'.contact_province_id',
                $this->table.'.contact_district_id',
                $this->table.'.contact_address',
                $this->table.'.residence_province_id',
                $this->table.'.residence_district_id',
                $this->table.'.residence_address',
                $this->table.'.is_test'
            )
            ->where( 'customers_change.customer_change_id',$id)
            ->orderBy("customers_change.customer_change_id","DESC");

        return $oSelect->first();
    }

    public function updateCustomer($id,$data) {
        $oSelect = $this->where('customer_id',$id)->update($data);
        return $oSelect;
    }

    public function getFullNameCustomerRefer($customer_refer_code) {
        $oSelect = $this->where('customer_code',$customer_refer_code)->select('full_name')->first();
        return $oSelect;
    }

    public function getCustomer($customer_id){
        $oSelect = $this
            ->leftJoin('member_levels','member_levels.member_level_id', 'customers_change.member_level_id')
            ->where( 'customers_change.customer_id',$customer_id)
            ->select( 'customers_change.*','member_levels.name as member_levels_name')->get();
        return $oSelect;
    }

    public function getDetail($customer_id){
        $oSelect = $this
            ->leftJoin('member_levels','member_levels.member_level_id', 'customers_change.member_level_id')
            ->where( 'customers_change.customer_id',$customer_id)
            ->select( 'customers_change.*','member_levels.name as member_levels_name')->first();
        return $oSelect;
    }

    public function getAll(){
        $oSelect = $this->where('is_actived',1)->where('is_deleted',0)->get();
        return $oSelect;
    }

    public function getAllByDay($day){
        $oSelect = $this
            ->where('is_actived',1)
            ->where('is_deleted',0)
            ->where(DB::raw("(DATE_FORMAT(birthday,'%Y-%m-%d') )"),$day)->get();
        return $oSelect;
    }

    public function getListReferCustomer($customer_id) {
        $oSelect = $this->where('customer_refer_id',$customer_id)->select('is_referal')->get();
        return $oSelect;
    }

    public function cancelChangeRequest($id,$reason_vi,$reason_en){
        $oSelect = $this->where('customer_change_id',$id)->update(['reason_vi' => $reason_vi,'reason_en' => $reason_en,'status' => 'cancel']);
        return $oSelect;
    }
}