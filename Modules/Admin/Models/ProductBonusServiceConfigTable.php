<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ProductBonusServiceConfigTable extends Model
{
    use ListTableTrait;
    protected $table = "product_bonus_service_config";
    protected $primaryKey = "product_bonus_service_config_id";
    protected $fillable = [
        'product_bonus_service_config_id',
        'product_id',
        'is_active',
        'start',
        'end',
        'created_at',
        'created_by',
    ];

    public function getConfig($product_id) {
        $oSelect = $this
            ->where('product_id',$product_id)
            ->first();
        return $oSelect;
    }

    public function updateConfig($id,$data) {
        $oSelect = $this->where('product_id',$id)->update($data);
        return $oSelect;
    }

    public function createdConfig($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

}