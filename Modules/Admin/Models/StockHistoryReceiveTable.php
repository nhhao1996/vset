<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 5/12/2021
 * Time: 9:47 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;

class StockHistoryReceiveTable extends Model
{
    protected $table = 'stock_history_receive';
    protected $primaryKey = 'stock_history_receive_id';
    protected $fillable = [
      'stock_history_receive_id',
      'stock_history_divide_id',
      'stock_id',
      'customer_id',
      'quantity',
      'type',
      'payment_method_id',
      'day',
      'month',
      'year',
      'dividend',
      'value',
      'created_at',
      'created_by',
      'updated_at',
      'updated_by',
    ];
    public function createHistory($data){
        $add = $this->create($data);
        return $add->stock_history_receive_id;
    }
}