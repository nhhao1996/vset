<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 11/8/2018
 * Time: 12:33 AM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class ProductInventoryTable extends Model
{
    use ListTableTrait;
    protected $table = 'product_inventorys';
    protected $primaryKey = 'product_inventory_id';
    public $timestamps = true;

    protected $fillable = ['product_inventory_id', 'product_id', 'product_code', 'warehouse_id', 'import', 'export', 'quantity', 'created_at', 'updated_at', 'created_by', 'updated_by'];

    protected function _getList()
    {
        $oSelect = $this->leftjoin('product_childs', 'product_childs.product_id', '=', 'product_inventorys.product_id')
            ->leftjoin('warehouses', 'warehouses.warehouse_id', '=', 'product_inventorys.warehouse_id')
            ->select(
                'product_inventorys.product_inventory_id as productInventoryId',
                'product_childs.product_code as productCode',
                'product_childs.product_child_name as productChildName',
                'warehouses.name as warehouseName',
                'product_inventorys.quantity as quantity'
            )
            ->where('product_childs.is_deleted', 0)
            ->groupBy('product_inventorys.product_inventory_id');
        return $oSelect;
    }

    protected function getItem($id)
    {
        return $this->where('product_inventory_id', $id)->first();
    }

    public function getListProductInventory()
    {
        $oSelect = $this
            ->leftjoin('product_childs', 'product_childs.product_id', '=', 'product_inventorys.product_id')
            ->leftjoin('warehouses', 'warehouses.warehouse_id', '=', 'product_inventorys.warehouse_id')
            ->select(
                'product_inventorys.product_inventory_id as productInventoryId',
                'product_childs.product_code as productCode',
                'product_childs.product_child_name as productChildName',
                'warehouses.name as warehouseName',
                'product_inventorys.quantity as quantity'

            )
            ->where('product_childs.is_deleted', 0)
            ->where('products.is_deleted', 0)->groupBy('product_childs.product_code')->paginate(10);
        return $oSelect;
    }

    /**
     * Insert product inventory to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $oInsert = $this->create($data);

        return $oInsert->product_inventory_id;
    }

    /**
     * Edit product inventory in database
     *
     * @param array $data , $id
     * @return number
     */
    public function edit(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);

    }

    /**
     * Check product inventory in database
     *
     * @param $productCode , $warehouseId
     * @return array
     */
    public function checkProductInventory($productCode, $warehouseId)
    {
        return $this->where('product_code', $productCode)->where('warehouse_id', $warehouseId)->first();
    }

    /*
     * get product inventory by warehouse id and product id.
     */
    public function getProductByWarehouseAndProductId($warehouseId, $productId)
    {
        $select = $this->leftJoin('product_childs', 'product_childs.product_child_id', '=', 'product_inventorys.product_id')
            ->leftJoin('units', 'units.unit_id', '=', 'product_childs.unit_id')
            ->select(
                'product_inventorys.product_id as productId',
                'product_childs.product_child_name as name',
                'product_inventorys.product_code as code',
                'units.unit_id as unitId',
                'units.name as unitName',
                'product_inventorys.quantity as quantitys',
                'product_childs.cost as cost'
            )
            ->where('product_inventorys.warehouse_id', $warehouseId)
            ->where('product_inventorys.product_id', $productId)
            ->where('product_childs.is_deleted', 0)
            ->first();
        return $select;
    }

    /*
     * get product inventory by warehouse id and product child code.
     */
    public function getProductByWarehouseAndProductCode($warehouseId, $code)
    {
        $select = $this->leftJoin('product_childs', 'product_childs.product_child_id', '=', 'product_inventorys.product_id')
            ->leftJoin('units', 'units.unit_id', '=', 'product_childs.unit_id')
            ->select(
                'product_inventorys.product_id as productId',
                'product_childs.product_child_name as name',
                'product_inventorys.product_code as code',
                'units.unit_id as unitId',
                'units.name as unitName',
                'product_inventorys.quantity as quantitys',
                'product_childs.cost as cost'
            )
            ->where('product_inventorys.warehouse_id', $warehouseId)
            ->where('product_inventorys.product_code', $code)
            ->first();
        return $select;
    }

    public function getProduct()
    {
        $select = $this->select(
            'product_inventorys.warehouse_id as warehouse_id',
            'product_inventorys.product_code as product_code',
            'product_inventorys.quantity as quantity'
        )
            ->leftJoin('warehouses', 'warehouses.warehouse_id', '=', 'product_inventorys.warehouse_id')
            ->leftJoin('product_childs', 'product_childs.product_child_id', '=', 'product_inventorys.product_id')
            ->leftJoin('products', 'products.product_id', '=', 'product_childs.product_id')
            ->where('warehouses.is_deleted', 0)
            ->where('products.is_deleted', 0)
            ->where('product_childs.is_deleted', 0)->get();
        return $select;
    }

    public function getProductInventoryByWarehouse($productCode)
    {
        $oSelect = $this->leftJoin('warehouses', 'warehouses.warehouse_id', '=', 'product_inventorys.warehouse_id')
            ->select(
                'warehouses.name as warehouseName',
                'warehouses.warehouse_id as warehouseId',
                'product_inventorys.quantity as quantity'
            )
            ->where('product_inventorys.product_code', $productCode)->get();
        return $oSelect;
    }

    public function getProductInventory()
    {
        return $this->select('product_code', 'warehouse_id', 'quantity')->get();
    }

    public function getQuantityProductInventoryByCode($code)
    {
        return $this->select(DB::raw('SUM(quantity) as quantityInventory'))
            ->leftJoin('warehouses', 'warehouses.warehouse_id', '=', 'product_inventorys.warehouse_id')
            ->where('warehouses.is_deleted', 0)
            ->where('product_code', $code)->first();
    }

    public function getProductWhereIn(array $warehouse)
    {
        return $this->select('warehouse_id', 'product_code', 'quantity')
            ->whereIn('warehouse_id', $warehouse)->get();
    }

    //Tìm kiểm sản phẩm tồn kho.
    public function getProductInventoryByCodeOrName($warehouse, $name, $code)
    {
        $select = $this->leftJoin('product_childs', 'product_childs.product_code', '=', 'product_inventorys.product_code')
            ->select(
                'product_childs.product_child_id as product_child_id',
                'product_childs.product_child_name as product_child_name'
            )
            ->where(function ($query) use ($name, $code) {
                $query->where('product_inventorys.product_code', 'like', '%' . $code . '%')
                    ->orWhere('product_childs.product_child_name', 'like', '%' . $name . '%');
            })
            ->where('product_inventorys.warehouse_id', $warehouse)->get();
        return $select;
    }

    //Tìm kiểm sản phẩm tồn kho theo kho.
    public function getProductInventoryByWarehouseId($warehouse)
    {
        $select = $this->leftJoin('product_childs', 'product_childs.product_code', '=', 'product_inventorys.product_code')
            ->select(
                'product_childs.product_child_id as product_child_id',
                'product_childs.product_child_name as product_child_name'
            )
            ->where('is_deleted',0)
            ->where('product_inventorys.warehouse_id', $warehouse)->get();
        return $select;
    }
}