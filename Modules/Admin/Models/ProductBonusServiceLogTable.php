<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ProductBonusServiceLogTable extends Model
{
    use ListTableTrait;
    protected $table = "product_bonus_service_log";
    protected $primaryKey = "product_bonus_service_log_id";
    protected $fillable = [
        "product_bonus_service_log_id",
        "order_id",
        "service_id",
        "service_name",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];

    public function getAllByOrder($orderId){
        $oSelect = $this
            ->where('order_id',$orderId)
            ->get();
        return $oSelect;
    }
}