<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;
use function Aws\filter;

class GroupStaffTable extends Model
{
    use ListTableTrait;
    protected $table = "group_staff";
    protected $primaryKey = "group_staff_id";
    public $timestamps = false;

    const NOT_DELETED = 0;

    protected $fillable = [
        'group_staff_id',
        'group_name',
        'type',
        'is_active',
        'is_deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function add($data)
    {
        $select = $this->create($data);
        return $select->{$this->primaryKey};
    }

    public function getItem($id)
    {
        $select = $this->select($this->fillable)
            ->where($this->primaryKey, $id)
            ->first();
        return $select;
    }

    public function edit($id, $data)
    {
        $this->where($this->primaryKey, $id)->update($data);
    }

    protected function _getList(&$filter = [])
    {
        $select = $this->select(
            $this->table . '.group_staff_id',
            $this->table . '.group_name',
            $this->table . '.is_active',
            DB::raw('count(group_staff_map_staff.group_staff_id) as total')
        )
            ->join(
                'group_staff_map_staff',
                'group_staff_map_staff.group_staff_id',
                $this->table . '.group_staff_id'
            )
            ->where($this->table . '.is_deleted', self::NOT_DELETED)
            ->groupBy($this->table . '.' . $this->primaryKey)
            ->orderBy($this->table . '.' . $this->primaryKey, 'DESC');
        if (isset($filter['search']) && $filter['search'] != null && $filter['search'] != '') {
            $select->where($this->table . '.group_name', 'like', '%' . $filter['search'] . '%');
        }
        unset($filter['search']);
        return $select;
    }

    public function getConfigGeneral($staff_id){
        $oSelect = $this
            ->join('group_staff_map_staff','group_staff_map_staff.group_staff_id',$this->table.'.group_staff_id')
            ->where($this->table.'.is_active',1)
            ->where($this->table.'.is_deleted',0)
            ->where('group_staff_map_staff.staff_id',$staff_id)
            ->select($this->table.'.*');
        return $oSelect->first();
    }

    public function deleteStaff($groupId){
        $oSelect = $this->where('group_staff_id',$groupId)->delete();
        return $oSelect;
    }
}