<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ProductBonusServiceTable extends Model
{
    use ListTableTrait;
    protected $table = "product_bonus_service";
    protected $primaryKey = "product_bonus_service_id";
    protected $fillable = [
        'product_bonus_service_id',
        'product_id',
        'service_id',
        'created_at',
        'created_by',
    ];

    public function getListService($id){
        $oSelect = $this
            ->join('services','services.service_id','product_bonus_service.service_id')
            ->where('product_id',$id)
            ->get();
        return $oSelect;
    }

    public function deleteService($product_id){
        $oSelect = $this->where('product_id',$product_id)->delete();
        return $oSelect;
    }

    public function createdService($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function checkService($product_id ,$filter = []){
        $oSelect = $this
            ->join('product_bonus_service_config','product_bonus_service_config.product_id','product_bonus_service.product_id')
            ->join('service_voucher_template','service_voucher_template.service_id','product_bonus_service.service_id')
            ->join('services','services.service_id','service_voucher_template.service_id')
            ->where('product_bonus_service_config.is_active',1)
            ->where('product_bonus_service.product_id',$product_id)
            ->where('product_bonus_service_config.start','<=', isset($filter['created_at']) && $filter['created_at'] != null ? $filter['created_at'] : Carbon::now())
            ->where('product_bonus_service_config.end','>',isset($filter['created_at']) && $filter['created_at'] != null ? $filter['created_at'] : Carbon::now())
            ->where('service_voucher_template.valid_from_date','<=',Carbon::now())
            ->where('service_voucher_template.valid_to_date','>',Carbon::now())
            ->select(
                'product_bonus_service.service_id',
                'service_voucher_template.service_voucher_template_id',
                'services.price_standard'
            )
            ->get();
        return $oSelect;

    }

}