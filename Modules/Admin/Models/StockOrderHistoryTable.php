<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/9/2021
 * Time: 3:56 PM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;


class StockOrderHistoryTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_orders";
    protected $primaryKey = "stock_order_id";

    protected $fillable = [
        'stock_order_id', 'stock_order_code', 'customer_id', 'obj_id', 'source', 'quantity',
        'quantity_min', 'money_standard', 'fee', 'total', 'total_money', 'retail_purchase', 'payment_method_id','process_status',
        'payment_date', 'customer_name', 'customer_phone', 'customer_email', 'customer_residence_address', 'customer_ic_no', 'order_description','is_deleted',
        'reason_vi', 'reason_en', 'created_by', 'updated_by', 'created_at',"updated_at"
    ];

    /**
     * Danh sách yêu cầu mua cổ phiếu hoặc chuyển nhượng nếu type_list là transfer-request
     *
     * @param array $filter
     * @return mixed
     */
    protected  function _getList(&$filters=[])
    {
         // todo: Customer bán cho người khác--------
        
         $customerId = $filters['customer_id'];
        
        //  todo: Bán ở chợ---------
         $selectSell = $this     
         ->select(
             "{$this->table}.stock_order_code",
             DB::raw("'bán' as action"),
             "C.customer_code as transaction_object",
             "{$this->table}.quantity",
             "{$this->table}.total",
             "{$this->table}.process_status",
             "{$this->table}.created_at"

         )
         ->leftJoin("stock_market as SM","SM.stock_market_id","{$this->table}.obj_id")
         ->leftJoin("customers as C","C.customer_id","{$this->table}.customer_id")
         ->where("SM.customer_id",$customerId)
         ->where("{$this->table}.source",'market')     ;

         //todo:Chuyển nhượng
         $selectTransfer = $this
         ->select(
             "{$this->table}.stock_order_code",
             DB::raw("'chuyển nhượng' as action"),
             "C.customer_code as transaction_object",
             "{$this->table}.quantity"  ,
             "{$this->table}.total",
             "{$this->table}.process_status",             
             "{$this->table}.created_at"

         )
         ->leftJoin("customers as C","C.customer_id","{$this->table}.customer_id")
         ->where("{$this->table}.source","transfer")
         ->where("{$this->table}.obj_id",$customerId);

   

        //  todo: Mua: Nhận chuyển nhượng, mua ở chợ, mua từ nhà phát hành        
        $selectBuy = $this
        ->select(
            "{$this->table}.stock_order_code",
            DB::raw("'Mua' as action"),
            DB::raw("
                    (CASE 
                        WHEN {$this->table}.source='publisher' THEN 'Nhà phát hành'
                        WHEN {$this->table}.source='market' THEN C1.customer_code
                        WHEN {$this->table}.source='transfer' THEN C2.customer_code                    
                    END) as transaction_object"),
            "{$this->table}.quantity"  ,
            "{$this->table}.total",
            "{$this->table}.process_status",             
            "{$this->table}.created_at"

        )
        ->leftJoin("stock_market as SM1", "SM1.stock_market_id","{$this->table}.obj_id")
        ->leftJoin("customers as C1", "C1.customer_id","SM1.customer_id")
        ->leftJoin("customers as C2", "C2.customer_id","{$this->table}.customer_id")
        ->where("{$this->table}.customer_id",$customerId);       

         $selectSell
         ->union($selectTransfer)
         ->union($selectBuy);

         $filters['stock_orders.customer_id']  = $filters['customer_id'];
         unset($filters['customer_id']);
         unset($filters['is_filter']);
         return $selectSell;  
   
    }
   
}