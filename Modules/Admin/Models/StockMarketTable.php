<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 8:46 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockMarketTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_market";
    protected $primaryKey = "stock_market_id";
    protected $fillable = [
      'stock_market_id','customer_id','quantity','quantity_min','money','fee','total','total_money','retail_purchase',
        'status','sell_date','reason_vi','reason_vi','reason_en','created_at','created_by','updated_at','updated_by'
    ];

    /**
     * Danh sách đang bán ở chợ hoặc mua cổ phiếu ở chợ hoặc đăng bán dựa vào status route
     *
     * @param $filter
     * @return mixed
     */
    protected  function _getList(&$filter)
    {
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        if($filter['status_route'] == 'selling'){
            $oSelect = $this
                ->leftJoin("customers","customers.customer_id","=","{$this->table}.customer_id")
                ->select(
                    "{$this->table}.stock_market_id",
                    "customers.full_name",
                    "{$this->table}.quantity",
                    "{$this->table}.money",
                    "{$this->table}.retail_purchase",
                    "{$this->table}.sell_date"
                )->where("{$this->table}.status","=","selling");
            if (isset($filter['search']) != "") {
                $search = $filter['search'];
                $oSelect->where(function ($query) use ($search) {
                    $query->where("customers.full_name", 'like', '%' . $search . '%');
                });
                unset($filter["search"]);
            }
            if(isset($filter["created_at"]) && $filter["created_at"] != ""){
                $arr_filter = explode(" - ",$filter["created_at"]);
                $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
                $oSelect->whereBetween("{$this->table}.sell_date", [$from , $to]);
            }
            if(isset($filter["sorted"]) && $filter["sorted"] != ""){
                if($filter["sorted"] == "0"){
                    $oSelect = $oSelect->orderBy("{$this->table}.money","DESC");
                    $oSelect = $oSelect->orderBy("{$this->table}.sell_date","DESC");
                }
                else{
                    $oSelect = $oSelect->orderBy("{$this->table}.money","ASC");
                    $oSelect = $oSelect->orderBy("{$this->table}.sell_date","DESC");
                }
            }
            else{
                $oSelect = $oSelect->orderBy("{$this->table}.sell_date","DESC");
            }
        }
        elseif($filter['status_route'] == 'buy-stock'){
            $oSelect = $this
                ->rightJoin("stock_orders","stock_orders.obj_id","=","{$this->table}.stock_market_id")
                ->leftJoin("customers as cus","cus.customer_id","=","stock_orders.customer_id")
                ->leftJoin("customers","customers.customer_id","=","{$this->table}.customer_id")
                ->select(
                    "{$this->table}.stock_market_id",
                    "stock_orders.stock_order_id",
                    "customers.full_name as seller_name",
                    "cus.full_name as buyer_name",
                    "stock_orders.quantity",
                    "stock_orders.money_standard",
                    "stock_orders.process_status",
                    "stock_orders.source",
                    "stock_orders.payment_date")
                ->where("stock_orders.process_status","=","paysuccess")
                ->where("stock_orders.source","=","market");
            if (isset($filter['search']) != "") {
                $search = $filter['search'];
                $oSelect->where(function ($query) use ($search) {
                    $query->where("cus.full_name", 'like', '%' . $search . '%');
                });
                unset($filter["search"]);
            }
            if(isset($filter["created_at"]) && $filter["created_at"] != ""){
                $arr_filter = explode(" - ",$filter["created_at"]);
                $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
                $oSelect->whereBetween("stock_orders.payment_date", [$from , $to]);
            }
            $oSelect->orderBy("stock_orders.payment_date","DESC");
        }
        else{
            $oSelect = $this
                ->leftJoin("customers","customers.customer_id","=","{$this->table}.customer_id")
                ->select(
                    "{$this->table}.stock_market_id",
                    "customers.full_name",
                    "{$this->table}.quantity",
                    "{$this->table}.money",
                    "{$this->table}.retail_purchase",
                    "{$this->table}.created_at"
                )->where("{$this->table}.status","=","new");
            if (isset($filter['search']) != "") {
                $search = $filter['search'];
                $oSelect->where(function ($query) use ($search) {
                    $query->where("customers.full_name", 'like', '%' . $search . '%');
                });
                unset($filter["search"]);
            }
            if(isset($filter["created_at"]) && $filter["created_at"] != ""){
                $arr_filter = explode(" - ",$filter["created_at"]);
                $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
                $oSelect->whereBetween("{$this->table}.created_at", [$from , $to]);
            }
        }

        unset($filter["sorted"]);
        unset($filter["status_route"]);
        return $oSelect;
    }

    /**
     * Chi tiết mua cổ phiếu ở chợ
     *
     * @param $stock_order_id
     * @return mixed
     */
    public function getDetailBuyStock($stock_order_id){
        $oSelect = $this
            ->rightJoin("stock_orders","stock_orders.obj_id","=","{$this->table}.stock_market_id")
            ->leftJoin("customers as cus","cus.customer_id","=","stock_orders.customer_id")
            ->leftJoin("customers","customers.customer_id","=","{$this->table}.customer_id")
            ->leftJoin('stock_transaction as sell', function($q)
            {
                $q->on('sell.stock_order_id', '=', 'stock_orders.stock_order_id')
                    ->on('sell.customer_id', '=', 'customers.customer_id');
            })
            ->leftJoin('stock_transaction as buy', function($q)
            {
                $q->on('buy.stock_order_id', '=', 'stock_orders.stock_order_id')
                    ->on('buy.customer_id', '=', 'cus.customer_id');
            })
            ->select(
                "{$this->table}.stock_market_id",
                "{$this->table}.quantity as sell_quantity",
                "stock_orders.stock_order_id",

                "customers.full_name as seller_name",
                "customers.phone as seller_phone",
                "sell.quantity_before as seller_quantity_before",
                "sell.quantity_after as seller_quantity_after",

                "cus.full_name as buyer_name",
                "cus.phone as buyer_phone",
                "buy.quantity_before as buyer_quantity_before",
                "buy.quantity_after as buyer_quantity_after",

                "stock_orders.quantity",
                "stock_orders.money_standard",
                "stock_orders.total",
                "stock_orders.fee",
                "stock_orders.process_status",
                "stock_orders.source",
                "stock_orders.payment_date"
            )
            ->where("stock_orders.process_status","=","paysuccess")
            ->where("stock_orders.stock_order_id","=",$stock_order_id);
        return $oSelect->first();
    }

    /**
     * Chi tiết đăng ký bán ở chợ
     *
     * @param $stock_market_id
     * @return mixed
     */
    public function getDetailRegisterToSell($stock_market_id){
        $oSelect = $this
            ->leftJoin("customers","customers.customer_id","=","{$this->table}.customer_id")
            ->leftJoin("stock_customer","stock_customer.customer_id","=","{$this->table}.customer_id")
            ->select(
                "{$this->table}.stock_market_id",
                "{$this->table}.status",
                "{$this->table}.customer_id",
                "customers.customer_code",
                "customers.phone",
                "customers.full_name",
                "customers.email",
                "stock_customer.stock",
                "{$this->table}.quantity",
                "{$this->table}.money",
                "{$this->table}.total"
            )
            ->where("{$this->table}.stock_market_id","=",$stock_market_id);
        return $oSelect->first();
    }

    /**
     * Lấy ID của người bán
     *
     * @param $stock_order_id
     * @return mixed
     */
    public function getSellerIdByStockOrderId($stock_order_id){
        $data = $this->select("{$this->table}.customer_id")
            ->leftJoin("stock_orders","stock_orders.obj_id","=","{$this->table}.stock_market_id")
            ->where("stock_orders.stock_order_id","=",$stock_order_id);
        return $data->first()->toArray();

    }

    /**
     * Lấy số lượng bán của 1 đơn hàng
     *
     * @param $stock_market_id
     * @return mixed
     */
    public function getQuantity($stock_market_id){
        $data = $this->select("quantity")->where("{$this->table}.stock_market_id","=",$stock_market_id)->first();
        return $data;
    }
    public function edit($data,$id){
        $this->where($this->primaryKey,$id)->update($data);

    }
    public function getDetailSellerMarket($stockMarketId){
        $data = $this->select(
            "customers.customer_id as seller_id",
            "customers.full_name as seller_name",
            "customers.customer_code as seller_code",
            "customers.phone as seller_phone"
        )
        ->leftJoin("customers","customers.customer_id","{$this->table}.customer_id")
        ->where("{$this->table}.stock_market_id",$stockMarketId);
        return $data->first();
    }
}