<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/29/2020
 * Time: 4:11 PM
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class CustomerContractInterestDateTable extends Model
{
    use ListTableTrait;
    protected $table = "customer_contract_interest_by_date";
    protected $primaryKey = "customer_contract_interest_by_date_id";

    /**
     * Danh sách lãi suất theo ngày
     *
     * @param array $filter
     * @return mixed
     */
    public function _getList($filter = [])
    {
        $ds = $this
            ->select(
                "{$this->table}.interest_year",
                "{$this->table}.interest_month",
                "{$this->table}.interest_day",
                "{$this->table}.interest_rate_by_day",
                "{$this->table}.interest_amount",
                "{$this->table}.interest_total_amount",
                "{$this->table}.interest_balance_amount",
                "{$this->table}.withdraw_date_planning"
            )
            ->where("customer_contract_id", $filter['customer_contract_id'])
            ->orderBy("{$this->table}.customer_contract_interest_by_date_id", "desc");

        return $ds;
    }
}