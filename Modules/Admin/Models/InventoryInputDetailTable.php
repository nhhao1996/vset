<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 11/13/2018
 * Time: 2:06 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class InventoryInputDetailTable extends Model
{
    protected $table = 'inventory_input_details';
    protected $primaryKey = 'inventory_input_detail_id';

    protected $fillable = ['inventory_input_detail_id', 'inventory_input_id', 'product_code', 'unit_id', 'quantity', 'current_price', 'quantity_recived', 'total', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    /**
     * Insert inventory input to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $inventoryInputDetail = $this->create($data);
        return $inventoryInputDetail->inventory_input_detail_id;
    }

    /*
     * get inventory input detail by inventory input id
     */
    public function getInventoryInputDetailByParentId($id)
    {
        $data = $this->leftJoin('product_childs', 'product_childs.product_code', '=', 'inventory_input_details.product_code')
            ->leftJoin('units', 'units.unit_id', '=', 'inventory_input_details.unit_id')
            ->select(
                'inventory_input_details.product_code as code',
                'inventory_input_details.unit_id as unitId',
                'inventory_input_details.quantity as quantity',
                'inventory_input_details.current_price as currentPrice',
                'inventory_input_details.quantity_recived as quantityRecived',
                'inventory_input_details.total as total',
                'product_childs.product_child_name as childName',
                'product_childs.price as price',
                'units.name as unitName'
            )
            ->where('inventory_input_details.inventory_input_id', $id)->get();
        return $data;
    }

    /*
     * edit inventory output detail.
     */
    public function editByInputIdAndProductCode(array $data, $inventoryInputId, $productCode)
    {
        return $this->where('inventory_input_id', $inventoryInputId)
            ->where('product_code', $productCode)->update($data);
    }

    /*
     * get history inventory input
     */
    public function getHistory($code)
    {
        $select = $this
            ->leftJoin('inventory_inputs', 'inventory_inputs.inventory_input_id', '=', 'inventory_input_details.inventory_input_id')
            ->leftJoin('warehouses', 'warehouses.warehouse_id', '=', 'inventory_inputs.warehouse_id')
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'inventory_inputs.created_by')
            ->select(
                'inventory_inputs.pi_code as code',
                'inventory_inputs.warehouse_id as warehouses',
                'warehouses.name as warehouse',
                'inventory_inputs.type as type',
                'inventory_input_details.quantity as quantity',
                'inventory_inputs.status as status',
                'staffs.full_name as user',
                'inventory_inputs.created_at as createdAt'
            )
            ->where('inventory_input_details.product_code', $code)
            ->where('inventory_inputs.status', 'success');
        if (Auth::user()->is_admin != 1) {
            $select->where('warehouses.branch_id', Auth::user()->branch_id);
        }
        return $select->get();
    }

    //Xóa với điều kiện id phiếu nhập và mã sản phẩm.
    public function removeByParentIdAndProductCode($parentId, $productCode)
    {
        return $this->where('inventory_input_id', $parentId)
            ->where('product_code', $productCode)->delete();
    }
}