<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
use DB;

class StockPublishHistoryTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_history";
    protected $primaryKey = "stock_history_id";

    protected $fillable = [
        "stock_history_id"
        ,"stock_id"
        ,"stock_config_id"
        ,"quantity"
        ,"quantity_min"
        ,"quantity_publish"
        ,"quantity_sale"
        ,"money"
        ,"publish_at"
        ,"money_publish"
        ,"is_active"
        ,"created_at"
        ,"created_by"
        ,"updated_at"
        ,"updated_by"
        
    ];
      /**
     * @param array $filter
     * @return mixed
     */

    public function add($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }
    public function edit($id, $data) {
        return $this->where($this->primaryKey, $id)->first()->update($data);
    }

}
