<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;

class CommissionLogTable extends Model
{
    protected $table  = 'commission_log';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'customer_id',
        'branch_id',
        'money',
        'type',
        'note',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by'
    ];

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->id;
    }

    /**
     * @param $customer_id
     * @return mixed
     */
    public function getLogByCustomer($customer_id)
    {
        $ds = $this
            ->leftJoin('branches', 'branches.branch_id', '=', 'commission_log.branch_id')
            ->select(
                'branches.branch_name',
                'commission_log.money',
                'commission_log.note',
                'commission_log.created_at',
                'commission_log.type'
            )
            ->where('commission_log.customer_id', $customer_id)->get();
        return $ds;
    }
}