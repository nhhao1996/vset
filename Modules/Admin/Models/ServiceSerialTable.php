<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ServiceSerialTable extends Model
{
    use ListTableTrait;
    protected $table = 'service_serial';
    protected $primaryKey = 'service_serial_id';
    protected $fillable = [
        'service_serial_id',
        'service_id',
        'service_voucher_template',
        'serial',
        'order_service_id',
        'service_serial_status',
        'created_at',
        'updated_at',
        'actived_at',
        'created_by',
        'updated_by',
    ];

    public function _getList($filter = [])
    {
        $oSelect = $this
            ->join('services','services.service_id','service_serial.service_id')
            ->join('customers','customers.customer_id','service_serial.customer_id')
            ->join('service_voucher_template','service_voucher_template.service_voucher_template_id','service_serial.service_voucher_template')
            ->leftJoin('order_services','order_services.order_service_id','service_serial.order_service_id');
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $oSelect->where(function ($query) use ($search) {
                $query->where('order_services.order_service_code', 'like', '%' . $search . '%')
                    ->orWhere('services.service_name_vi', 'like', '%' . $search . '%')
                    ->orWhere('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('service_serial.serial', 'like', '%' . $search . '%');
            });
            unset($filter['search']);
        }

        if (isset($filter['service_serial_status'])){
            $oSelect = $oSelect->where('service_serial.service_serial_status',$filter['service_serial_status']);
            unset($filter['service_serial_status']);
        }

        if (isset($filter['type'])){
            $oSelect = $oSelect->where('service_serial.type',$filter['type']);
            unset($filter['type']);
        }



        $oSelect = $oSelect->select(
                'service_serial.*',
                'services.service_name_vi as service_name',
                'service_voucher_template.valid_from_date',
                'service_voucher_template.valid_to_date',
                'order_services.order_service_code',
                'customers.full_name'
            );
        return $oSelect->orderBy('service_serial_id','DESC');
    }

    public function addServiceSerial($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelected = $this
            ->join('services','services.service_id','service_serial.service_id')
            ->join('service_voucher_template','service_voucher_template.service_voucher_template_id','service_serial.service_voucher_template')
            ->leftJoin('order_services','order_services.order_service_id','service_serial.order_service_id')
            ->where('service_serial_id',$id)
            ->select(
                'service_serial.*',
                'services.service_name_vi as service_name',
                'service_voucher_template.valid_from_date',
                'service_voucher_template.valid_to_date',
                'order_services.order_service_code'
            )->first();
        return $oSelected;
    }

    public function checkSerial($id,$serial) {
        $oSelect = $this->where('service_serial_id','<>',$id)->where('serial',$serial)->get();
        return $oSelect;
    }

    public function updateServiceSerrial($id,$data){
        $oSelect = $this->where('service_serial_id',$id)->update($data);
        return $oSelect;
    }
}