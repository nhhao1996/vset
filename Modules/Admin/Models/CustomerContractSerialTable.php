<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/29/2020
 * Time: 5:09 PM
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class CustomerContractSerialTable extends Model
{
    use ListTableTrait;
    protected $table = "customer_contract_serial";
    protected $primaryKey = "customer_contract_serial_id";

    /**
     * Danh sách số serial
     *
     * @param array $filter
     * @return mixed
     */
    public function _getList($filter = [])
    {
        $ds = $this
            ->select(
                "products.product_name_vi as product_name",
                "{$this->table}.product_serial_no",
                "{$this->table}.issued_date",
                "{$this->table}.product_serial_amount"
            )
            ->join("products", "products.product_id", "=", "{$this->table}.product_id")
            ->where("{$this->table}.customer_contract_id", $filter['customer_contract_id'])
            ->orderBy("{$this->table}.customer_contract_serial_id", "desc");

        return $ds;
    }
}