<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/12/2021
 * Time: 5:34 PM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockCustomerTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_customer";
    protected $primaryKey = "stock_customer_id";
    public $timestamps = FALSE;

    protected $fillable = [
        'stock_customer_id', 'customer_id', 'wallet_stock_money', 'wallet_dividend_money', 'stock','stock_bonus',
        'last_updated', 'created_at','created_by','updated_at','updated_by'
    ];

    public function getListStockCustomer(){
        $data = $this->select(
            'stock_customer_id', 'c.customer_id', 'wallet_stock_money', 'wallet_dividend_money', 'stock','c.phone', 'c.full_name'
        )
            ->leftJoin("customers as c", "c.customer_id","{$this->table}.customer_id")
            ->get()->toArray();
        return $data;
    }
    /**
     * Tạo ví cổ phiếu, cổ tức cho 1 customer
     *
     * @param $data
     * @return mixed
     */
    public function createStockCustomer($data){
        $add = $this->create($data);
        return $add->stock_customer_id;
    }

    /**
     * Cập nhật (+,-) số dư của customer
     *
     * @param $data
     * @param $customerId
     * @return mixed
     */
    public function updateStockWallet($data, $customerId){
        return $this->where("{$this->table}.customer_id",$customerId)->update($data);
    }
    
    /**
     * Cập nhật Stock Customer
     *
     * @param $data
     * @param $customerId
     * @return mixed
     */
    public function edit($data, $customerId){
        return $this->where("{$this->table}.{$this->primaryKey}",$customerId)->update($data);
    }
       /**
     * Get Stock Customer theo customer_id
     *
     * @param $data
     * @param $customerId
     * @return mixed
     */
    public function getStockByCustomer($customerId){
        $select =  $this->where("{$this->table}.customer_id",$customerId);
        return $select->first();
    }

    /**
     * Cập nhật (+,-) số dư của customer
     *
     * @param $data
     * @param $customerId
     * @return mixed
     */
    public function updateStockOrDividendWallet($data, $customerId){
        return $this->where("{$this->table}.customer_id",$customerId)->update($data);
    }

    /**
     * Lấy số tiền cổ phiếu của customer
     *
     * @param $customerId
     * @return array
     */
    public function getStockWalletMoney($customerId){
        $oSelect = $this->select(
            "wallet_stock_money"
        )->where("{$this->table}.customer_id","=",$customerId)->orderBy("stock_customer_id","DESC");
        if($oSelect->first() == null){
            return ['wallet_stock_money'=>'0'];
        }
        else{
            return $oSelect->first()->toArray();
        }
    }

    /**
     * Lấy số tiền cổ tức của customer
     *
     * @param $customerId
     * @return array
     */
    public function getDividendWalletMoney($customerId){
        $oSelect = $this->select(
            "wallet_dividend_money"
        )->where("{$this->table}.customer_id","=",$customerId)->orderBy("stock_customer_id","DESC");
        if($oSelect->first() == null){
            return ['wallet_dividend_money'=>'0'];
        }
        else{
            return $oSelect->first()->toArray();
        }
    }

    /**
     * Lấy số cổ phiếu của customer
     *
     * @param $customerId
     * @return array
     */
    public function getStock($customerId){
        $oSelect = $this->select(
            "stock"
        )->where("{$this->table}.customer_id","=",$customerId)->orderBy("stock_customer_id","DESC");
        if($oSelect->first() == null){
            return ['stock'=>'0'];
        }
        else{
            return $oSelect->first()->toArray();
        }
    }
    public function getStockBonus($customerId){
        $oSelect = $this->select(
            "stock_bonus"
        )->where("{$this->table}.customer_id","=",$customerId)->orderBy("stock_customer_id","DESC");
        if($oSelect->first() == null){
            return ['stock_bonus'=>'0'];
        }
        else{
            return $oSelect->first()->toArray();
        }
    }
    public function checkExistsCustomer($customerId){
        $oSelect = $this->select(
            "stock"
        )->where("{$this->table}.customer_id","=",$customerId)->orderBy("stock_customer_id","DESC");
        if($oSelect->first() == null){
            return false;
        }
        else{
            return true;
        }
    }
    public function getWalletByCustomerId($customerId){
        $oSelect = $this
            ->where('customer_id',$customerId)
            ->select(
                'wallet_stock_money',
                'wallet_dividend_money',
                'stock'
            )
            ->first();
        return $oSelect;
    }

    public function updateCustomer($customerId,$data){
        $oSelect = $this->where('customer_id',$customerId)->update($data);
        return$oSelect;
    }

//    Lấy thông tin tiền và mã hợp đồng cổ phiếu
    public function getInfoStockByCustomer($customerId){
        $select =  $this
            ->select(
                $this->table.'.*',
                'stock_contract.stock_contract_code'
            )
            ->leftJoin('stock_contract','stock_contract.customer_id',$this->table.'.customer_id')
            ->where("{$this->table}.customer_id",$customerId);
        return $select->first();
    }
}