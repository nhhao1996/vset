<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockNewsTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_news";
    protected $primaryKey = "stock_news_id";
    protected $fillable = [
        'stock_news_id',
        'stock_category_id',
        'stock_news_title_vi',
        'stock_news_title_en',
        'stock_news_content_vi',
        'stock_news_content_en',
        'stock_news_img',
        'is_active',
        'is_deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    public function _getList(&$filters = []) {
        $oSelect = $this
            ->join('stock_category','stock_category.stock_category_id',$this->table.'.stock_category_id');
        if (isset($filters['stock_news$stock_news_title_vi'])) {
            $oSelect = $oSelect->where($this->table.'.stock_news_title_vi', 'like', '%' . $filters['stock_news$stock_news_title_vi'] . '%');
        }

        if (isset($filters['stock_news$stock_news_title_en'])) {
            $oSelect = $oSelect->where($this->table.'.stock_news_title_en', 'like', '%' . $filters['stock_news$stock_news_title_en'] . '%');
        }

        if (isset($filters['stock_category$stock_category_title_vi'])) {
            $oSelect = $oSelect->where('stock_category.stock_category_title_vi', 'like', '%' . $filters['stock_category$stock_category_title_vi'] . '%');
        }

        if (isset($filters['stock_category$stock_category_title_en'])) {
            $oSelect = $oSelect->where('stock_category.stock_category_title_en', 'like', '%' . $filters['stock_category$stock_category_title_en'] . '%');
        }

        if (isset($filters['stock_news$is_active'])) {
            $oSelect = $oSelect->where($this->table.'.is_active', $filters['stock_news$is_active']);
        }

        unset($filters['stock_news$stock_news_title_vi']);
        unset($filters['stock_news$stock_news_title_en']);
        unset($filters['stock_category$stock_category_title_vi']);
        unset($filters['stock_category$stock_category_title_en']);
        unset($filters['stock_news$is_active']);
        $oSelect = $oSelect->select($this->table.'.*','stock_category.stock_category_title_vi','stock_category.stock_category_title_en')->orderBy('stock_news_id','DESC');
        return $oSelect;
    }

    public function checkUsing($stock_category_id){
        $oSelect = $this->where('stock_category_id',$stock_category_id)->get();
        return $oSelect;
    }

    public function checkName($category,$name,$id = null){
        $oSelect = $this
            ->where('stock_category_id',$category)
            ->where($name);
        if ($id != null){
            $oSelect = $oSelect->where('stock_news_id','<>',$id);
        }

        return $oSelect->get();
    }

    public function createNews($data){
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function updateNews($data,$id){
        $oSelect = $this->where('stock_news_id',$id)->update($data);
        return $oSelect;
    }

    public function getDetail($id){
        $oSelect = $this->where('stock_news_id',$id)->first();
        return $oSelect;
    }

    public function removeNews($id){
        $oSelect = $this->where('stock_news_id',$id)->delete();
        return $oSelect;
    }
}