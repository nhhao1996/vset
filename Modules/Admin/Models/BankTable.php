<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class BankTable extends Model
{
    use ListTableTrait;
    protected $table = "bank";
    protected $primaryKey = "bank_id";
    protected $fillable = [
        'bank_id',
        'bank_name',
        'bank_icon',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public function _getList(&$filters = []) {
        $oSelect = $this;
        if (isset($filters['search_keyword'])) {
            $oSelect = $oSelect->where('bank_name', 'like', '%' . $filters['search_keyword'] . '%')
            ->orWhere('bank_id', 'like', '%' . $filters['search_keyword'] . '%');
        }

        unset($filters['search_keyword']);
        $oSelect = $oSelect->orderBy('bank_id','DESC');
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelect = $this->where('bank_id',$id)->first();
        return $oSelect;
    }

    public function updateBank($id,$data) {
        $oSelect = $this->where('bank_id',$id)->update($data);
        return $oSelect;
    }

    public function addBank($data) {
        $oSelelct = $this->insertGetId($data);
        return $oSelelct;
    }

    public function deleteBank($id) {
        $oSelect = $this->where('bank_id',$id)->delete();
        return $oSelect;
    }
    public  function checkByname($data){
        $oSelect = BankTable::select('bank_name')->where('bank_name', $data)->count();
        return $oSelect;
    }
    public  function checkByUpdate($data){
        $oSelect = BankTable::select('bank_name' ,'bank_id')
            ->where('bank_name', $data['bank_name'])
            ->whereNotIn('bank_id', [$data['bank_id']])
            ->count();
        return $oSelect;
    }

    public function getAll(){
        return $this->get();
    }
}