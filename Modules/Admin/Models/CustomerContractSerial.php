<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerContractSerial extends Model
{
    protected $table = 'customer_contract_serial';
    protected $primaryKey = 'customer_contract_serial_id';
    protected $fillable = ["customer_contract_serial_id", "customer_contract_id", "product_id", "product_serial_no", "issued_date", "product_serial_amount", "issued_by", "product_serial_front", "product_serial_back", "created_by", "updated_by", "created_at", "updated_at"];

    public function getSerialbyProductId($id,$customer_contract_id){
        $oSelect = $this->select(
            "{$this->table}.*",
            "s.full_name as staff_name"
        )
                ->leftJoin("staffs as s","s.staff_id","=","{$this->table}.issued_by")
                ->where("product_id",$id)
                ->where("customer_contract_id",$customer_contract_id);

        return $oSelect->get();
    }

    public function edit($id,$data){
        $this->where("customer_contract_serial_id",$id)->update($data);
    }

    public function add($data){
        $result = $this->create($data);
        return $result;
    }
}
