<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockHistoryDivideFileTable extends Model
{
    use ListTableTrait;
    protected $table = 'stock_history_divide_file';
    protected $primaryKey = 'stock_history_divide_file_id';
    protected $fillable = [
        "stock_history_divide_file_id",
        "file",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];
      /**
     * @param array $data
     * @return mixed
     */
      public function getListFile(){
          return $this->select()->get();
}
    public function add(array $data)
    {
        $add = $this->insert($data);       
        return $add;
    }
    public function remove($arrFileIds, $id){

        return $this
            ->where("{$this->table}.stock_history_divide_id",$id)
            ->whereNotIn($this->primaryKey,$arrFileIds)->delete();
    }


}