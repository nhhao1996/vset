<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 4/29/2020
 * Time: 11:29 AM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerContractInterestByDateTable extends Model
{
    protected $table = "customer_contract_interest_by_date";
    protected $primaryKey = "customer_contract_interest_by_date_id";
    
    protected $fillable = [
        'customer_contract_interest_by_date_id',
        'customer_contract_id',
        'customer_id',
        'interest_year',
        'interest_month',
        'interest_day',
        'interest_rate_by_day',
        'interest_amount',
        'interest_total_amount',
        'interest_balance_amount',
        'withdraw_date_planning',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    const IS_ACTIVED = 1;
    const IS_NOT_DELETED = 0;
    /**
     * Thêm 1 record
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $select = $this->create($data);
        return $select->primaryKey;
    }

    /**
     * Thêm nhiều record
     * @param array $data
     */
    public function addInsert(array $data)
    {
        $select = $this->insert($data);
    }

    /**
     * Các customer contract đã tồn tại trong năm, tháng.
     * @param $arrContractId
     * @param $year
     * @param $month
     *
     * @return mixed
     */
    public function contractExist($arrContractId, $year, $month)
    {
        $select = $this->select($this->table . '.*')
            ->whereIn($this->table . '.customer_contract_id', $arrContractId)
            ->where($this->table . '.interest_year', $year)
            ->where($this->table . '.interest_month', $month);
        return $select->get();
    }

    /**
     * Record cuối cùng của customer contract
     * @param $customerContractId
     *
     * @return mixed
     */
    public function lastCustomerContractInterest($customerContractId)
    {
        $select = $this->select($this->table . '.*')
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $select;
    }

    /**
     * Tổng tiền tất cả các tháng confirmed_by # null
     * @param $customerContractId
     *
     * @return mixed
     */
    public function getConfirmByNotNullOfContract($customerContractId)
    {
        $select = $this->select(DB::raw("SUM(interest_amount) as interest_amount"))
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->whereNotNull($this->table . '.confirmed_by')
            ->where($this->table . '.confirmed_by', '<>', 0);
        return $select->first();
    }

    /**
     * Xóa hết record trong ngày
     * @param $year
     * @param $month
     * @param $day
     */
    public function removeByMonthYearDay($year, $month, $day)
    {
        $this->where($this->table . '.interest_year', $year)
            ->where($this->table . '.interest_month', $month)
            ->where($this->table . '.interest_day', $day)
            ->delete();
    }

    /**
     * Record cuối cùng của contract trong ngày
     * @param $customerContractId
     * @param $year
     * @param $month
     * @param $day
     *
     * @return mixed
     */
    public function getByMonthYearDay($customerContractId, $year, $month, $day)
    {
        $select = $this->where($this->table . '.customer_contract_id', $customerContractId)
            ->where($this->table . '.interest_year', $year)
            ->where($this->table . '.interest_month', $month)
            ->where($this->table . '.interest_day', $day)
            ->orderBy($this->table . '.customer_contract_interest_by_date_id', 'desc')
            ->first();
        return $select;
    }

    public function getListAll($filter) {
        $page    = (int) ($filter['page'] ? $filter['page'] : 1);
        $display = 10;

        $oSelect = $this
            ->where('customer_contract_id',$filter['customer_contract_id'])
//            ->where('customer_id',$filter['customer_id'])
            ->orderBy('customer_contract_interest_by_date_id','DESC');

        if ($display > 10) {
            $page = intval(count($oSelect->get()) / $display);
        }
        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function sumContractStatus($customer_contract_id){
        $oSelect = $this->where('customer_contract_id',$customer_contract_id)
            ->orderBy($this->table . '.customer_contract_interest_by_date_id', 'desc')
            ->select('interest_balance_amount');

        return $oSelect->first();
    }

    /**
     * Tất cả record theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByCondition($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        //interest_month nằm trong array tháng truyền vào
        if (isset($params['array_month'])) {
            $select->whereIn($this->table . '.interest_month', $params['array_month']);
        }
        $select->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    /**
     * Tất cả record theo điều kiện không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function getByConditionNotTest($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                'orders.customer_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        //interest_month nằm trong array tháng truyền vào
        if (isset($params['array_month'])) {
            $select->whereIn($this->table . '.interest_month', $params['array_month']);
        }
        $select
            ->where('customers.is_test','<>', 1)
            ->where('orders.is_deleted', self::IS_NOT_DELETED)
            ->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    /**
     * Số lượng hợp đồng
     * @param array $params
     *
     * @return mixed
     */
    public function getByContractQuantity($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Trạng thái của order
        if (isset($params['product_category_id'])) {
            $select->where('products.product_category_id', $params['product_category_id']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        $select->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy('orders.order_id');
        return $select->get()->count();
    }

    /**
     * Số lượng hợp đồng không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function getByContractQuantityNotTest($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->leftJoin(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->leftJoin(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->leftJoin(
                'customers',
                'customers.customer_id',
                'orders.customer_id'
            )
            ->leftJoin(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Trạng thái của order
        if (isset($params['product_category_id'])) {
            $select->where('products.product_category_id', $params['product_category_id']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        $select
            ->where('customers.is_test','<>',1);
//            ->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy('orders.order_id');
        return $select->get()->count();
    }

    public function getInfoInterestByDate($customer_contract_id){
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
//            ->where('interest_year',$year)
//            ->where('interest_month',$month)
//            ->where('interest_day',$day)
            ->orderBy('customer_contract_interest_by_date_id','DESC')
            ->first();
        return $oSelect;
    }

    public function updateInterestDate($id,$data){
        $oSelect = $this->where('customer_contract_interest_by_date_id',$id)->update($data);
        return $oSelect;
    }

    public function getLasted($customerContractId){
        $oSelect = $this->where('customer_contract_id',$customerContractId)
            ->orderBy($this->table . '.customer_contract_interest_by_date_id', 'desc')
            ->select(
                'customer_contract_interest_by_date_id',
                'customer_contract_id',
                'customer_id',
                'interest_year',
                'interest_month',
                'interest_day',
                'interest_rate_by_day',
                'interest_amount',
                'interest_total_amount',
                'interest_balance_amount',
                'withdraw_date_planning'
            );

        return $oSelect->first();
    }

}