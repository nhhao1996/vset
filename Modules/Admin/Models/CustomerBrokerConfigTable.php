<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class CustomerBrokerConfigTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_broker_config';
    protected $primaryKey = 'key';
    protected $fillable = [
        'key',
        'object',
        'is_voucher',
        'is_money',
        'value'
    ];

    public function updateConfig($data,$key)
    {
        $oSelect = $this->where('key',$key)->update($data);
        return $oSelect;
    }
}