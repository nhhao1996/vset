<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 9/29/2018
 * Time: 11:58 AM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ProductCategoryTable extends Model
{
    use ListTableTrait;
    protected $table = 'product_categories';
    protected $primaryKey = 'product_category_id';

    protected $fillable = ['product_category_id', 'category_name_vi', 'category_name_en', 'description_vi', 'description_en', 'is_actived', 'is_deleted', 'created_by', 'created_at','updated_at','slug'];

    protected function _getList()
    {
        return $this->select('product_category_id', 'category_name', 'description', 'is_actived', 'is_deleted', 'created_by', 'updated_by', 'created_at', 'updated_at')
            ->where('is_deleted', 0)->orderBy($this->primaryKey, 'desc');
    }

    //Add product category
    public function add(array $data)
    {
        $productCategory = $this->create($data);
        return $productCategory->product_category_id;
    }

    /*
     * Delete product category
     */
    public function remove($id)
    {
        return $this->where($this->primaryKey, $id)->update(['is_deleted' => 1]);
    }

    /*
     * Edit product category
     */
    public function edit(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);
    }

    /*
     * get item
     */

    public function getItem($id)
    {
        return $this->where($this->primaryKey, $id)->first();
    }

    /*
     * get all product category
     */
    public function getAll()
    {
        return $this
            ->where('is_deleted', 0)
            ->where('is_actived', 1)
            ->get();
    }
}