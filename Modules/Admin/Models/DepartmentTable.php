<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 9/24/2018
 * Time: 10:20 AM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class DepartmentTable extends Model
{
    use ListTableTrait;
    protected $table = 'departments';
    protected $primaryKey = 'department_id';

    protected $fillable = ['department_id', 'department_name', 'is_inactive', 'is_deleted', 'created_by', 'updated_by', 'created_at', 'updated_at','slug'];

    protected function _getList()
    {
        return $this->select('department_id', 'department_name', 'is_inactive', 'created_at')
            ->where('is_deleted', 0)->orderBy($this->primaryKey, 'desc');
    }

    /**
     * Insert department to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $oStaffDepartment = $this->create($data);
        return $oStaffDepartment->id;
    }

    /**
     * Edit department to database
     *
     * @param array $data , $id
     * @return number
     */
    public function edit(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);
    }

    /**
     * Remove department to database
     *
     * @param number $id
     */
    public function remove($id)
    {
        $this->where($this->primaryKey, $id)->update(['is_deleted' => 1]);
    }

    public function getItem($id)
    {
        return $this->where($this->primaryKey, $id)->first();
    }
    /*
     * function get
     */
    public function getStaffDepartmentOption(){
        return $this->select('department_id','department_name')->where('is_deleted',0)->get()->toArray();
    }
    /*
     * check unique department
     */
    public function check($name){
        return $this->where('slug',str_slug($name))->where('is_deleted', 0)->first();
    }
    /*
    * check unique department edit
    */
    public function checkEdit($id,$name){
        return $this->where('department_id','<>',$id)->where('slug',str_slug($name))->where('is_deleted', 0)->first();
    }
    /*
     * test is deleted
     */
    public function testIsDeleted($name)
    {
        return $this->where('slug', str_slug($name))->where('is_deleted', 1)->first();
    }
    /*
     * edit by department name
     */
    public function editByName($name){
        return $this->where('slug', str_slug($name))->update(['is_deleted' => 0]);
    }
    public  function checkByname($data){
        $oSelect = DepartmentTable::select('department_name')->where('department_name', $data)->count();
        return $oSelect;
    }
}