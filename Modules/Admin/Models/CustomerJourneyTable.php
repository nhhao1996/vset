<?php
/**
 * Created by PhpStorm.
 * User: WAO
 * Date: 13/03/2018
 * Time: 1:10 CH
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class CustomerJourneyTable extends Model
{
    use ListTableTrait;

    /*
     * table service_package
     */
    protected $table = 'customer_journey';
    protected $primaryKey = 'customer_journey_id';

    /*
     * fill table
     * $var array
     */
    protected $fillable = [
        'customer_journey_id',
        'name',
        'description',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function _getList(&$filters = [])
    {
        $select = $this;

        if (isset($filters['search_type'])){
            $select = $select->where($filters['search_type'],'like', '%' . $filters['search_keyword'] . '%');
        }

        if (isset($filters['is_active'])){
            $select->where('is_active',$filters['is_active']);
        }

        unset($filters['is_active']);
        unset($filters['search_type']);
        unset($filters['search_keyword']);

            $select = $select->orderBy('customer_journey_id','DESC');
        return $select;
    }

    public function getAll(){
        $oSelect = $this
            ->where('is_active',1)
            ->get();
        return $oSelect;
    }

    public function testCustomerJourney($name,$id = null){
        $oSelect = $this
            ->where('name',$name);

        if($id != null) {
            $oSelect = $oSelect->where('customer_journey_id','<>',$id);
        }

        $oSelect = $oSelect->get();
        return $oSelect;
    }

    public function add($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function getItem($id){
        $oSelect = $this->where('customer_journey_id',$id)->first();
        return $oSelect;
    }

    public function edit($data,$id) {
        $oSelect = $this->where('customer_journey_id',$id)->update($data);
        return $oSelect;
    }

}