<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockConfigTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_config";
    protected $primaryKey = "stock_config_id";
    protected $fillable = [
        'stock_config_id', 
        'stock_config_code',
        'name_vi',
        'name_en',
        'fee_stock_sell_market',
        'fee_transfer_stock',
        'fee_withraw_stock',
        'fee_withraw_dividend',
        'fee_convert_bond_stock',
        'fee_convert_saving_stock',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    /**
     * Lấy phí người bán ở chợ
     *
     * @return mixed
     */
    public function getFeeStockSellMarket(){
        $data = $this->select("fee_stock_sell_market")->first();
        return $data;
    }

    /**
     * @param array $filter
     * @return mixed
     */
    protected function _getList(&$filter = [])
    {
        $ds = $this            
            ->select(
                $this->table.".*",
                'sf.stock_config_file_id',
                'sf.file'
            )
        ;
        return $ds;
    }
   public function getDetail(){
       return $this->select($this->table . ".*")
       ->orderBy($this->primaryKey, 'DESC')
       ->first();
   }
    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {      
        // dd("data la gi: ", $data); 
        return  $this->where('stock_config_id', $id)->first()->update($data);       
    }



}