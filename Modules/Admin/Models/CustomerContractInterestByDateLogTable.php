<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 4/29/2020
 * Time: 11:29 AM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerContractInterestByDateLogTable extends Model
{
    protected $table = "customer_contract_interest_by_date_log";
    protected $primaryKey = "customer_contract_interest_by_date_log_id";
    
    protected $fillable = [
        'customer_contract_interest_by_date_log_id',
        'customer_contract_id',
        'customer_id',
        'interest_year',
        'interest_month',
        'interest_day',
        'interest_rate_by_day',
        'interest_amount',
        'interest_total_amount',
        'interest_balance_amount',
        'withdraw_date_planning',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * Record cuối cùng của customer contract
     * @param $customerContractId
     *
     * @return mixed
     */
    public function lastCustomerContractInterest($customerContractId)
    {
        $select = $this->select($this->table . '.*')
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $select;
    }

    /**
     * Xoá log ngày cũ
     * @param $customer_contract_id
     */
    public function removeLogDate($customer_contract_id){
        return $this
            ->where($this->table . '.customer_contract_id', $customer_contract_id)
            ->delete();
    }

    /**
     * Xoá log ngày cũ
     * @param $customer_contract_id
     */
    public function addLogDate($data){
        return $this
            ->insertGetId($data);
    }

    public function getInfoInterestByDate($customer_contract_id){
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $oSelect;
    }

}