<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockBonusReferTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_bonus_refer";
    protected $primaryKey = "stock_bonus_refer_id";
    public $timestamps = false;

    protected $fillable = [
        "stock_bonus_refer_id"
        ,"stock_from"
        ,"stock_to"
        ,"rate"
        ,"created_at"
        ,"created_by"
        ,"updated_at"
        ,"updated_by"
    ];


    /**
     * Lấy danh sách stock_bonus
     *
     * @param $stockId
     * @return mixed
     */
    public function _getList(array &$filters=[]){
        return $this->select("{$this->table}.*");
    }
    public function add($data){
        return $this->insert($data);
    }
    public function edit($data,$id){
        $select =  $this->where($this->primaryKey,$id)->first();
        return $select->update($data);
    }


    public function remove($data){
        return $this->where($data)->first()->delete();
    }

    public function getRateWithQuantity($quantity)
    {
        $data = $this->select("rate")
            ->where("stock_from", "<=", $quantity)
            ->where("stock_to", ">=", $quantity)->first();
        if($data == null){
            $data['rate'] = 0;
        }
        return $data;
    }
    public function getLast(){
        return $this->select()
            ->orderBy('stock_to','DESC')
            ->orderBy($this->primaryKey,'DESC')
            ->first();
    }

}