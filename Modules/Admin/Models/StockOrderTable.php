<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/9/2021
 * Time: 3:56 PM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;


class StockOrderTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_orders";
    protected $primaryKey = "stock_order_id";

    protected $fillable = [
        'stock_order_id', 'stock_order_code', 'customer_id', 'obj_id', 'source', 'quantity',
        'quantity_min', 'money_standard', 'fee', 'total', 'total_money', 'retail_purchase', 'payment_method_id','process_status',
        'payment_date', 'customer_name', 'customer_phone', 'customer_email', 'customer_residence_address', 'customer_ic_no', 'order_description','is_deleted',
        'reason_vi', 'reason_en', 'created_by', 'updated_by', 'created_at',"updated_at"
    ];

    /**
     * Danh sách yêu cầu mua cổ phiếu hoặc chuyển nhượng nếu type_list là transfer-request
     *
     * @param array $filter
     * @return mixed
     */
    protected  function _getList(&$filter=[])
    {
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        if(isset($filter['type_list']) == 'transfer-request'){
            $oSelect = $this
                ->leftJoin("customers as cus_transfer","cus_transfer.customer_id","=","{$this->table}.obj_id")
                ->leftJoin("customers as cus_receiver","cus_receiver.customer_id","=","{$this->table}.customer_id")
                ->select(
                    "{$this->table}.stock_order_id",
                    "{$this->table}.stock_order_code",
                    "cus_transfer.full_name as transfer_name",
                    "cus_receiver.full_name as receiver_name",
                    "{$this->table}.quantity",
                    "{$this->table}.total",
                    "{$this->table}.process_status",
                    "{$this->table}.source",
                    "{$this->table}.created_at"
                )->where("{$this->table}.source","=","transfer");
            if (isset($filter['search']) != "") {
                $search = $filter['search'];
                $oSelect->where(function ($query) use ($search) {
                    $query->where("cus_transfer.full_name", 'like', '%' . $search . '%')
                        ->orWhere("cus_receiver.full_name", 'like', '%' . $search . '%')
                        ->orWhere("cus_transfer.phone", 'like', '%' . $search . '%')
                        ->orWhere("cus_receiver.phone", 'like', '%' . $search . '%')
                        ->orWhere("{$this->table}.stock_order_code", 'like', '%' . $search . '%');
                });
                unset($filter["search"]);
            }
            if (isset($filter['process_status']) && $filter['process_status'] != "") {
                $oSelect->where("{$this->table}.process_status",'=', $filter['process_status']);
                unset($filter["process_status"]);
            }
            if(isset($filter["created_at"]) && $filter["created_at"] != ""){
                $arr_filter = explode(" - ",$filter["created_at"]);
                $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
                $oSelect->whereBetween("{$this->table}.created_at", [$from , $to]);
            }
        }
        else{
            $oSelect = $this
                ->leftJoin("payment_method","payment_method.payment_method_id","=","{$this->table}.payment_method_id")
                ->select(
                    "{$this->table}.stock_order_id",
                    "{$this->table}.stock_order_code",
                    "{$this->table}.customer_name",
                    "{$this->table}.customer_phone",
                    "{$this->table}.quantity",
                    "{$this->table}.money_standard",
                    "{$this->table}.total",
                    "{$this->table}.created_by",
                    "{$this->table}.created_at",
                    "{$this->table}.process_status",
                    "{$this->table}.source",
                    "payment_method_name_$lang as payment_method_name"
                )->where("{$this->table}.source","!=","transfer");
            if (isset($filter['search']) != "") {
                $search = $filter['search'];
                $oSelect->where(function ($query) use ($search) {
                    $query->where("{$this->table}.customer_name", 'like', '%' . $search . '%')
                        ->orWhere("{$this->table}.customer_phone", 'like', '%' . $search . '%')
                        ->orWhere("{$this->table}.stock_order_code", 'like', '%' . $search . '%');
                });
                unset($filter["search"]);
            }
            if (isset($filter['process_status']) && $filter['process_status'] != "") {
                $oSelect->where("{$this->table}.process_status",'=', $filter['process_status']);
                unset($filter["process_status"]);
            }
            if(isset($filter["created_at"]) && $filter["created_at"] != ""){
                $arr_filter = explode(" - ",$filter["created_at"]);
                $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
                $oSelect->whereBetween("{$this->table}.created_at", [$from , $to]);
            }
        }
        unset($filter['type_list']);
        return $oSelect->orderBy("{$this->table}.stock_order_id",'DESC');
    }

    /**
     * Chi tiết 1 yêu cầu mua cổ phiếu
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $month = (int)Carbon::now()->format('m');
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        $oSelect = $this
            ->leftJoin("payment_method","payment_method.payment_method_id","=","{$this->table}.payment_method_id")
            ->leftJoin("customers","customers.customer_id","=","{$this->table}.customer_id")
            ->leftJoin("district","district.districtid","=","customers.residence_district_id")
            ->leftJoin("province","province.provinceid","=","customers.residence_province_id")
            ->select(
                DB::raw("(SELECT stock_config_code FROM stock_config LIMIT 1) as stock_config_code"),
                DB::raw("IFNULL((SELECT percent FROM stock_bonus WHERE month = $month and {$this->table}.source='publisher'),0) as stock_bonus_rate"),
                "{$this->table}.stock_order_id",
                "{$this->table}.stock_order_code",
                "{$this->table}.customer_id",
                "{$this->table}.obj_id",
                "{$this->table}.source",
                "{$this->table}.payment_date",
                "{$this->table}.customer_name",
                "{$this->table}.quantity",
                "{$this->table}.money_standard",
                "{$this->table}.total",
                "{$this->table}.created_at",
                "{$this->table}.created_by",
                "{$this->table}.process_status",
                "{$this->table}.payment_method_id",
                'customers.email as customer_email',
                "{$this->table}.customer_phone",
                'customers.ic_no as customer_ic_no',
                'customers.residence_address as customer_residence_address',
                'district.name as district_name',
                'district.type as district_type',
                'province.name as province_name',
                'province.type as province_type',
                "payment_method_name_$lang as payment_method_name"
            )
            ->where("{$this->table}.stock_order_id","=",$id);
        return $oSelect->first();
    }

    /**
     * Chi tiết chuyển nhượng
     *
     * @param $stock_order_id
     * @return mixed
     */
    public function getDetailTransfer($stock_order_id){
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        $oSelect = $this
            ->leftJoin("payment_method","payment_method.payment_method_id","=","{$this->table}.payment_method_id")
            ->leftJoin("customers as cus_transfer","cus_transfer.customer_id","=","{$this->table}.obj_id")
            ->leftJoin("customers as cus_receiver","cus_receiver.customer_id","=","{$this->table}.customer_id")
            ->leftJoin("stock_customer as stock_cus_transfer","stock_cus_transfer.customer_id","=","{$this->table}.obj_id")
            ->leftJoin("stock_customer as stock_cus_receiver","stock_cus_receiver.customer_id","=","{$this->table}.customer_id")
            ->leftJoin("stock_market as sm","sm.stock_market_id","=","{$this->table}.obj_id")
            ->select(
                "{$this->table}.stock_order_id",
                "{$this->table}.stock_order_code",

                "{$this->table}.obj_id",
                "{$this->table}.source",
                "sm.customer_id as customer_sell_id",
                "cus_transfer.full_name as transfer_name",
                "cus_transfer.customer_code as transfer_code",
                "cus_transfer.phone as transfer_phone",
                "stock_cus_transfer.stock as transfer_stock",
                "stock_cus_transfer.wallet_stock_money as transfer_stock_money",
                "stock_cus_transfer.wallet_dividend_money as transfer_dividend_money",

                "{$this->table}.customer_id",
                "cus_receiver.full_name as receiver_name",
                "cus_receiver.customer_code as receiver_code",
                "cus_receiver.phone as receiver_phone",
                "stock_cus_receiver.wallet_stock_money as receiver_stock_money",
                "stock_cus_receiver.wallet_dividend_money as receiver_dividend_money",

                DB::raw("(SELECT stock_config_code FROM stock_config LIMIT 1) as stock_config_code"),
                "{$this->table}.quantity",
                "{$this->table}.money_standard",
                "{$this->table}.total",
                "{$this->table}.fee",
                "{$this->table}.total_money",
                "{$this->table}.payment_method_id",
                "{$this->table}.process_status",
                "payment_method_name_$lang as payment_method_name",
                "{$this->table}.created_at"
            )
            ->where("{$this->table}.stock_order_id","=",$stock_order_id);
        return $oSelect->first();
    }

    /**
     * Lấy số lượng phát hành còn lại (đợt phát hành)
     *
     * @return mixed
     */
    public function getStockPublishRemaining(){
        $oSelect = $this->select(
            DB::raw("IFNULL(SUM({$this->table}.quantity),0) as quantity_sold")
            )
            ->where("{$this->table}.source","=","publisher")
            ->where("{$this->table}.obj_id","=",DB::raw("(SELECT max(stock_id) FROM stock where is_active = 1)"))
            ->where("{$this->table}.process_status","=","paysuccess");
        $oSelect->groupBy("{$this->table}.obj_id");
        return $oSelect->first();
    }

    /**
     * Lấy số lượng bán còn lại ở chợ
     *
     * @param $objId
     * @return mixed
     */
    public function getStockMarketRemaining($objId){
        $oSelect = $this->select(
            DB::raw("IFNULL(SUM({$this->table}.quantity),0) as quantity_sold"),
            DB::raw("(SELECT quantity FROM stock_market WHERE stock_market_id = {$objId}) as quantity")
        )
            ->where("{$this->table}.source","=","market")
            ->where("{$this->table}.obj_id","=",$objId)
            ->where("{$this->table}.process_status","=","paysuccess");
        return $oSelect->first();
    }

    /**
     * Xác nhận đơn hàng
     *
     * @param $stockOrderId
     * @return mixed
     */
    public function confirmBuyStockRequest($stockOrderId){
        $data = [
            'process_status' => 'confirmed'
        ];
        return $this->where("{$this->table}.stock_order_id",$stockOrderId)->update($data);
    }

    /**
     * Cập nhật đơn hàng
     *
     * @param $stockOrderId
     * @param $data
     * @return mixed
     */
    public function updateStockOrder($stockOrderId, $data){
        return $this->where("{$this->table}.stock_order_id",$stockOrderId)->update($data);
    }

    /**
     * Lấy nguồn mua (mua từ chợ hoặc mua từ nhà phát hành) hoặc chuyển nhượng
     *
     * @param $stockOrderId
     * @return mixed
     */
    public function getSource($stockOrderId){
        return $this->select("{$this->table}.source")->where("{$this->table}.stock_order_id","=",$stockOrderId)->first()->toArray();
    }

    /**
     * Lấy số lượng mua
     *
     * @param $stockOrderId
     * @return mixed
     */
    public function getQuantity($stockOrderId){
        return $this->select("{$this->table}.quantity")->where("{$this->table}.stock_order_id","=",$stockOrderId)->first()->toArray();
    }
    public function getMoney($stockOrderId){
        return $this->select("{$this->table}.money_standard")->where("{$this->table}.stock_order_id","=",$stockOrderId)->first()->toArray();
    }
    public function totalQuantitySold($objId){
        $oSelect = $this->select(
            DB::raw("IFNULL(SUM({$this->table}.quantity),0) as quantity_sold")
        )
            ->where("{$this->table}.source","=","market")
            ->where("{$this->table}.obj_id","=",$objId)
            ->where("{$this->table}.process_status","=","paysuccess");
        if($oSelect->first() == null){
            return ['quantity_sold'=>0];
        }
        return $oSelect->first();
    }
    public function getListStockHistory($filters){
        // todo: Customer bán cho người khác--------

        $customerId = $filters['id'];
        $selectSell = DB::table('stock_market as SM')->select()
        ->leftJoin($this->table,'SM.customer_id','stock_orders.obj_id')
        ->where([
            ['stock_orders.source','=','market'],
            ['SM.customer_id', '=',$customerId]
        ])
        ->get();

        dd($selectSell);

    //    $selectSell = $this->select(
    //     'stock_orders.stock_order_code',
    //     'sm.quantity',

    //     DB::raw("'SELL'")
    //    )
    //    ->join('stock_market as sm','sm.customer_id',$this->table .".customer_id")
    //    ->where($this->table . ".customer_id",$id);


    //    $selectTransfer = $this->select(
    //     'stock_orders.stock_order_code',
    //     'stock_orders.quantity',

    //     DB::raw("'TRANSFER'")
    //    )
    //    ->where("source",'transfer')
    //    ->where('obj_id',$id);


    //    $selectBuy = $this->select(
    //     'stock_orders.stock_order_code',
    //     'stock_orders.quantity'

    //    )
    //    ->where($this->table . '.customer_id',$id);


    //    $ds = $selectSell->union($selectTransfer)
    //             ->union($selectBuy)->get();

        $ds = $selectSell->get();
         dd("nguoi mua la gi: ", $ds);


        return $selectSell;

    }

    /**
     * Dữ liệu mô tả biểu đồ báo cáo doanh thu thanh toán của cổ phiếu
     *
     * @param $params
     * @return mixed
     */
    public function getDataDescriptionOfStockReport($params){
        if (isset($params['filter_type']) && $params['filter_type'] == 'day') {
            switch ($params['type_chart']){
                case 'revenue':
                    $select = $this->select(
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue"),
                        DB::raw("(SELECT MAX(stock.money)
                                        FROM stock 
                                        WHERE stock.created_at BETWEEN '{$params['startTime']} 00:00:00' 
                                                        AND '{$params['endTime']} 23:59:59'
                                        ) as max_publisher"),
                        DB::raw("(SELECT MIN(stock.money)
                                        FROM stock 
                                        WHERE stock.created_at BETWEEN '{$params['startTime']} 00:00:00' 
                                                        AND '{$params['endTime']} 23:59:59'
                                        ) as min_publisher"),
                        DB::raw("(SELECT AVG(stock.money)
                                        FROM stock 
                                        WHERE stock.created_at BETWEEN '{$params['startTime']} 00:00:00' 
                                                        AND '{$params['endTime']} 23:59:59'
                                        ) as avg_publisher")
//                        DB::raw("AVG(stock_orders.money_standard) as avg_publisher"),
//                        DB::raw("MAX(stock_orders.money_standard) as max_publisher"),
//                        DB::raw("MIN(stock_orders.money_standard) as min_publisher")
                    )
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.source');
                    break;
                case 'count-transaction':
                    $select = $this->select(
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_transaction"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                    )
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.source');
                    break;
                case 'market-transaction':
                    $select = $this->select(
                        DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as count_transaction"),
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("SUM(stock_orders.total_money) as total_revenue"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_fee_revenue")
                    )
                        ->where("stock_orders.source","=","market")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.source');
                    break;
                case 'transfer-transaction':
                    $select = $this->select(
                        DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as count_transaction"),
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_fee_revenue"),
                        DB::raw("(SELECT COUNT(process_status) 
                                        FROM stock_orders 
                                        WHERE source='transfer' and process_status = 'new' and created_at BETWEEN '{$params['startTime']} 00:00:00' and '{$params['endTime']} 23:59:59') 
                                        as new_request"),
                        DB::raw("(SELECT COUNT(process_status) 
                                        FROM stock_orders 
                                        WHERE source='transfer' and process_status = 'paysuccess' and created_at BETWEEN '{$params['startTime']} 00:00:00' and '{$params['endTime']} 23:59:59') 
                                        as paysuccess_request"),
                        DB::raw("(SELECT COUNT(process_status) 
                                        FROM stock_orders 
                                        WHERE source='transfer' and process_status = 'cancel' and created_at BETWEEN '{$params['startTime']} 00:00:00' and '{$params['endTime']} 23:59:59') 
                                        as cancel_request")
                    )
                        ->where("stock_orders.source","=","transfer")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
                    $select = $select->groupBy('stock_orders.source');
                    break;
            }
        }
        if (isset($params['filter_type']) && $params['filter_type'] == 'month') {

            switch ($params['type_chart']){
                case 'revenue':
                    $select = $this->select(
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue"),
                        DB::raw("(SELECT MAX(stock.money)
                                        FROM stock 
                                        WHERE stock.created_at BETWEEN '{$params['month_start']}'
                                                        AND '{$params['month_end']}'
                                        ) as max_publisher"),
                        DB::raw("(SELECT MIN(stock.money)
                                        FROM stock 
                                        WHERE stock.created_at BETWEEN '{$params['month_start']}' 
                                                        AND '{$params['month_end']}'
                                        ) as min_publisher"),
                        DB::raw("(SELECT AVG(stock.money)
                                        FROM stock 
                                        WHERE stock.created_at BETWEEN '{$params['month_start']}' 
                                                        AND '{$params['month_end']}'
                                        ) as avg_publisher")
//                        DB::raw("AVG(stock_orders.money_standard) as avg_publisher"),
//                        DB::raw("MAX(stock_orders.money_standard) as max_publisher"),
//                        DB::raw("MIN(stock_orders.money_standard) as min_publisher")
                    )
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                    $select = $select->groupBy('stock_orders.source');
                    break;
                case 'count-transaction':
                    $select = $this->select(
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_transaction"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_revenue")
                    )
                        ->where("stock_orders.source","=","publisher")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                    $select = $select->groupBy('stock_orders.source');
                    break;
                case 'market-transaction':
                    $select = $this->select(
                        DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as count_transaction"),
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("SUM(stock_orders.total_money) as total_revenue"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_fee_revenue")
                    )
                        ->where("stock_orders.source","=","market")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                    $select = $select->groupBy('stock_orders.source');
                    break;
                case 'transfer-transaction':
                    $select = $this->select(
                        DB::raw("COUNT(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as count_transaction"),
                        DB::raw("SUM({$this->table}.quantity) as total_quantity"),
                        DB::raw("SUM(CASE
                            WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                            ELSE stock_orders.total_money
                        END) as total_fee_revenue"),
                        DB::raw("(SELECT COUNT(process_status) 
                                        FROM stock_orders 
                                        WHERE source='transfer' and process_status = 'new' and created_at BETWEEN '{$params['month_start']}' and '{$params['month_end']}' ) 
                                        as new_request"),
                        DB::raw("(SELECT COUNT(process_status) 
                                        FROM stock_orders 
                                        WHERE source='transfer' and process_status = 'paysuccess' and created_at BETWEEN '{$params['month_start']}' and '{$params['month_end']}') 
                                        as paysuccess_request"),
                        DB::raw("(SELECT COUNT(process_status) 
                                        FROM stock_orders 
                                        WHERE source='transfer' and process_status = 'cancel' and created_at BETWEEN '{$params['month_start']}' and '{$params['month_end']}') 
                                        as cancel_request")
                    )
                        ->where("stock_orders.source","=","transfer")
                        ->where("stock_orders.process_status","=","paysuccess");
                    $select = $select->whereBetween("stock_orders.created_at", [$params['month_start'], $params['month_end']]);
                    $select = $select->groupBy('stock_orders.source');
                    break;

            }
        }
//        dd($select->toSql());
        return $select->first();
    }


    /**
     * Dữ liệu thống kê số lượng giao dịch theo từng nguồn
     *
     * @param array $params
     * @return mixed
     */
    public function getDataStatisticTransactionChart($params = []){
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        if (isset($params['filter_type']) && $params['filter_type'] == 'day') {
            $select = $this->select(
                "{$this->table}.source",
                DB::raw("COUNT(CASE
                                WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                ELSE stock_orders.total_money
                            END) as amount_transaction"),
                DB::raw("DATE_FORMAT(stock_orders.created_at,'%d/%m/%Y') as date")
            )
                ->where("stock_orders.process_status","=","paysuccess");
            $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
            $select = $select->groupBy('stock_orders.source',DB::raw("DATE(stock_orders.created_at)"));
//            $select = $select->orderBy(DB::raw("DATE(stock_orders.created_at)"),'ASC');
        }
        else {
            $select = $this->select(
                "{$this->table}.source",
                DB::raw("COUNT(CASE
                                WHEN stock_orders.source != 'publisher' THEN stock_orders.fee*stock_orders.total/100
                                ELSE stock_orders.total_money
                            END) as amount_transaction"),
                DB::raw("DATE_FORMAT(stock_orders.created_at,'%c/%Y') as date")
            )
                ->where("stock_orders.process_status","=","paysuccess");
            $select = $select->whereBetween("stock_orders.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
            $select = $select->groupBy('stock_orders.source',DB::raw("DATE_FORMAT(stock_orders.created_at,'%c/%Y')"));
//            $select = $select->orderBy(DB::raw("DATE_FORMAT(stock_orders.created_at,'%c/%Y')"),'ASC');
        }
        return $select->get()->toArray();
    }

    public function getTotalMoney($customerId){
        $oSelect = $this
            ->select(
                DB::raw('SUM(total_money) as total_money_customer')
            )
            ->where('customer_id',$customerId)
            ->where('process_status','paysuccess')
            ->first();

        return ($oSelect['total_money_customer'] == null ? 0 : $oSelect['total_money_customer']);
    }


}