<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/2/2018
 * Time: 12:06 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;
//VSet
class StockTransferContractTable extends Model
{
    use ListTableTrait;
    protected $table = 'stock_tranfer_contract';
    protected $primaryKey = 'stock_tranfer_contract_id';
    protected $fillable = [
        "stock_tranfer_contract_id"
        ,"stock_tranfer_contract_code"
        ,"customer_id"
        ,"customer_contract_id"
        ,"total_amount"
        ,"quantity"
        ,"money"
        ,"money_paid"
        ,"status"
        ,"reason_vi"
        ,"reason_en"
        ,"created_at"
        ,"created_by"
        ,"updated_at"
        ,"updated_by"
    ];

    const IS_ACTIVE = 1;
    const NOT_DELETED = 0;

    public function _getList(&$filter = [])
    {
     
        $select = $this
            ->select(
                'stock_tranfer_contract.*',
                'c.full_name',
                'ct.customer_contract_code',
                'p.product_category_id'
               
            )
            ->leftJoin("customers as c",$this->table.".customer_id",'=','c.customer_id')          
            ->leftJoin("customer_contract as ct",$this->table.".customer_contract_id",'=','ct.customer_contract_id')          
            ->leftJoin("products as p","ct.product_id",'=','p.product_id')          
            ->orderBy($this->table . "." . $this->primaryKey,'DESC')
            ->where('ct.is_deleted',0);
        if (isset($filter['search_keyword']) && $filter['search_keyword'] != "") {
            
            $search = $filter['search_keyword'];
            
            $select->where(function ($query) use ($search) {
                $query->where('c.customer_code', 'like',"'".'%' . $search . '%' . "'")
                ->orWhere('c.phone', 'like', '%' . $search . '%')
                    ->orWhere('c.phone', 'like', '%' . $search . '%')
                    ->orWhere('c.full_name', 'like', '%' . $search . '%');
            });
           
        }

        if (isset($filter['startTime']) != "" && isset($filter['endTime'])) {            
            $select->whereBetween('stock_tranfer_contract.created_at', [$filter['startTime']. ' 00:00:00', $filter['endTime']. ' 23:59:59']);
            unset($filter['startTime']);
            unset($filter['endTime']);
        
        }
        if (isset($filter['search_keyword']) && $filter['search_keyword'] != "") {
//            dd('is_active');
            
            $search = $filter['search_keyword'];
            
            $select->where(function ($query) use ($search) {
                $query->where('c.customer_code', 'like',"'".'%' . $search . '%' . "'")
                    ->orWhere('c.phone', 'like', '%' . $search . '%')
                    ->orWhere('c.full_name', 'like', '%' . $search . '%');
            });
           
        }
        
        if (isset($filter['status'])) {
            $select->where("{$this->table}.status", $filter['status']);
        }
        if (isset($filter['status'])) {
            $select->where("{$this->table}.status", $filter['status']);
            unset($filter['status']);
        }
                
        if (isset($filter['product_category_id'])) {
            $select->where('p.product_category_id', $filter['product_category_id']);

        }
//        dd('select la gi: ', $select->toSql());
  
        return $select;
    }
  


    public function getDetail($id){
        $select = $this
            ->select(
                'stock_tranfer_contract.*',
                'c.full_name',
                'c.customer_code',
                'c.phone',
                'ct.customer_contract_end_date',
                'ct.customer_contract_code'

            )
            ->leftJoin("customers as c",$this->table.".customer_id",'=','c.customer_id')          
            ->leftJoin("customer_contract as ct",$this->table.".customer_contract_id",'=','ct.customer_contract_id')
            ->where($this->table.".".$this->primaryKey,$id)
            ->first();           
        return $select;
    }
    public function edit($id, $data){
        $res = $this->where($this->primaryKey,$id)->first()->update($data);
        return $res;
    }
    public function getStockContract($customer_id){
        return $this
            ->select()
            ->join('stock_contract as sc', "sc.customer_id","{$this->table}.customer_id")
            ->where("sc.customer_id",$customer_id)
            ->first();
    }
    public function statisticsTransactionConvert($params = [])
    {
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        if (isset($params['filter_type']) && $params['filter_type'] == 'day') {
            $select = $this->select(
                DB::raw("COUNT(stock_tranfer_contract_id) as amount_transaction"),
                DB::raw("DATE_FORMAT({$this->table}.created_at,'%d/%m/%Y') as date")
            )
                ->where("{$this->table}.status","=","approved");
            $select = $select->whereBetween("{$this->table}.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
            $select = $select->groupBy(DB::raw("DATE({$this->table}.created_at)"));
//            $select = $select->orderBy(DB::raw("DATE({$this->table}.created_at)"),'ASC');
        }
        else {
            $select = $this->select(
                DB::raw("COUNT(stock_tranfer_contract_id) as amount_transaction"),
                DB::raw("DATE_FORMAT({$this->table}.created_at,'%c/%Y') as date")
            )
                ->where("{$this->table}.status","=","approved");
            $select = $select->whereBetween("{$this->table}.created_at", [$params['startTime'] . ' 00:00:00', $params['endTime'] . ' 23:59:59']);
            $select = $select->groupBy(DB::raw("DATE_FORMAT({$this->table}.created_at,'%c/%Y')"));
//            $select = $select->orderBy(DB::raw("DATE_FORMAT({$this->table}.created_at,'%c/%Y')"),'ASC');
        }
        $select = $select->get()->toArray();
        if(count($select) == 0){
            return null;
        }
        for ($i = 0;$i < count($select); $i++){
            $select[$i]['source'] = 'convert';
        }
        return $select;
    }
   
}