<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/3/2018
 * Time: 2:26 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ProductImageTable extends Model
{
    use ListTableTrait;
    protected $table = 'product_images';
    protected $primaryKey = 'product_image_id';
//    public $timestamps = true;

    protected $fillable = ['product_image_id', 'product_id', 'name', 'type', 'created_at', 'created_by','updated_at'];

    protected function _getList()
    {
        $select = $this->leftJoin('products', 'products.product_id', '=', 'product_images.product_id')
            ->select(
                'product_images.product_image_id as productImageId',
                'product_images.name as productImageName',
                'product_images.type as productImageType',
                'product_images.created_at as productImageCreateAt',
                'product_images.product_id as productId',
                'products.product_name as productName'
            )->orderBy($this->primaryKey, 'desc');
        return $select;
    }
    public function add(array $data)
    {
        $oCustomerGroup = $this->create($data);
        return $oCustomerGroup->product_image_id;
    }

    public function remove($id)
    {
        return $this->where($this->primaryKey, $id)->delete();
    }

    public function edit(array $data, $id)
    {

        return $this->where($this->primaryKey, $id)->update($data);

    }

    public function getItem($id)
    {
        return $this->where($this->primaryKey, $id)->first();
    }
    /*
     * get image link by product.
     */
    public function getImageByProductId($productId){
        return $this->where('product_id',$productId)->get();
    }
    /*
     * delete all image by product
     */
    public function deleteByProductId($productId){
        return $this->where('product_id',$productId)->delete();
    }
    /*
     * delete image by product id and link image
     */
    public function deleteImageByProductIdAndLink($productId,$link){
        return $this->where('product_id',$productId)->where('name',$link)->delete();
    }

    public function createImage($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }
}