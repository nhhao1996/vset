<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 4/29/2020
 * Time: 11:29 AM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class CustomerContractInterestByMonthTable extends Model
{
    use ListTableTrait;
    protected $table = "customer_contract_interest_by_month";
    protected $primaryKey = "customer_contract_interest_by_month_id";
    
    protected $fillable = [
        'customer_contract_interest_by_month_id',
        'customer_contract_id',
        'customer_id',
        'interest_year',
        'interest_month',
        'interest_rate_by_month',
        'interest_amount',
        'interest_total_amount',
        'interest_banlance_amount',
        'withdraw_date_planning',
        'confirmed_by',
        'confirmed_at',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    const IS_ACTIVED = 1;
    const IS_NOT_DELETED = 0;

    /**
     * Thêm 1 record
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $select = $this->create($data);
        return $select->primaryKey;
    }

    /**
     * Thêm nhiều record
     * @param array $data
     */
    public function addInsert(array $data)
    {
        $select = $this->insert($data);
    }

    /**
     * Các customer contract đã tồn tại trong năm, tháng.
     * @param $arrContractId
     * @param $year
     * @param $month
     *
     * @return mixed
     */
    public function contractExist($arrContractId, $year, $month)
    {
        $select = $this->select($this->table . '.*')
            ->whereIn($this->table . '.customer_contract_id', $arrContractId)
            ->where($this->table . '.interest_year', $year)
            ->where($this->table . '.interest_month', $month);
        return $select->get();
    }

    /**
     * Record cuối cùng của customer contract
     * @param $customerContractId
     *
     * @return mixed
     */
    public function lastCustomerContractInterest($customerContractId)
    {
        $select = $this->select()
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $select;
    }

    /**
     * Danh sách lãi suất theo tháng
     *
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter = [])
    {
        if (isset($filter['action']) && $filter['action'] == 'list') {
            $ds = $this
                ->select(
                    "customer_contract.customer_contract_id",
                    "customer_contract.customer_contract_code",
                    "customer_contract.term_time_type",
                    "customers.full_name",
                    "customers.phone",
                    "customers.ic_no",
                    "{$this->table}.interest_year",
                    "{$this->table}.interest_month",
                    "{$this->table}.interest_rate_by_month",
                    "{$this->table}.interest_amount",
                    "{$this->table}.interest_total_amount",
                    "{$this->table}.interest_banlance_amount",
                    "{$this->table}.withdraw_date_planning",
                    "{$this->table}.confirmed_by",
                    "{$this->table}.customer_contract_interest_by_month_id"
                )
                ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
                ->join('customers','customers.customer_id','customer_contract_interest_by_month.customer_id')
                ->where('customer_contract.term_time_type',1)
                ->whereNull('customer_contract_interest_by_month.confirmed_by');
            if (isset($filter['search']) != "") {
                $search = $filter['search'];
                $ds->where(function ($query) use ($search) {
                    $query->where('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                        ->orWhere('customers.full_name', 'like', '%' . $search . '%')
                        ->orWhere('customers.phone', 'like', '%' . $search . '%')
                        ->orWhere('customers.ic_no', 'like', '%' . $search . '%');
                });
                unset($filter["search"]);
            }

            if(isset($filter["created_at"]) && $filter["created_at"] != ""){
                $arr_filter = explode(" - ",$filter["created_at"]);
                $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
                $ds = $ds->whereBetween('customer_contract_interest_by_month.withdraw_date_planning', [$from , $to]);
                unset($filter["created_at"]);
            }

            $ds = $ds->orderBy("{$this->table}.customer_contract_interest_by_month_id", "ASC");
        } else {
            $ds = $this
                ->select(
                    "{$this->table}.interest_year",
                    "{$this->table}.interest_month",
                    "{$this->table}.interest_rate_by_month",
                    "{$this->table}.interest_amount",
                    "{$this->table}.interest_total_amount",
                    "{$this->table}.interest_banlance_amount",
                    "{$this->table}.withdraw_date_planning",
                    "staffs.full_name as confirm_name",
                    "{$this->table}.confirmed_by",
                    "{$this->table}.customer_contract_interest_by_month_id",
                    "customer_contract.term_time_type"
                )
                ->join("customer_contract","customer_contract.customer_contract_id","{$this->table}.customer_contract_id")
                ->leftJoin("staffs", "staffs.staff_id", "=", "{$this->table}.confirmed_by")
                ->where("{$this->table}.customer_contract_id", $filter['customer_contract_id'])
                ->orderBy("{$this->table}.customer_contract_interest_by_month_id", "desc");
        }
        unset($filter['action']);
        unset($filter['customer_contract_id']);
        return $ds;
    }

    /**
     * Tổng tiền tất cả các tháng confirmed_by # null
     * @param $customerContractId
     *
     * @return mixed
     */
    public function getConfirmByNotNullOfContract($customerContractId)
    {
        $select = $this->select(DB::raw("SUM(interest_amount) as interest_amount"))
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->whereNotNull($this->table . '.confirmed_by')
            ->where($this->table . '.confirmed_by', '<>', 0);
        return $select->first();
    }

    /**
     * Tổng tiền tất cả các tháng cho tiết kiệm k kỳ hạng
     * @param $customerContractId
     *
     * @return mixed
     */
    public function getConfirmContractBySaving($customerContractId)
    {
        $select = $this->select(DB::raw("SUM(interest_amount) as interest_amount"))
            ->where($this->table . '.customer_contract_id', $customerContractId);
        return $select->first();
    }

    /**
     * Xóa hết những record trong năm, tháng.
     * @param $year
     * @param $month
     */
    public function removeByMonthYear($year, $month)
    {
        $this->where($this->table . '.interest_year', $year)
            ->where($this->table . '.interest_month', $month)
            ->delete();
    }

    public function getListAll($filter) {
        $page    = (int) ($filter['page'] ? $filter['page'] : 1);
        $display = 10;

        $oSelect = $this
            ->where('customer_contract_id',$filter['customer_contract_id'])
//            ->where('customer_id',$filter['customer_id'])
            ->orderBy('customer_contract_interest_by_month_id','DESC');

        if ($display > 10) {
            $page = intval(count($oSelect->get()) / $display);
        }
        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function updateInterestMonth($id, $data) {
        $oSelect = $this->where('customer_contract_interest_by_month_id',$id)->update($data);
        return $oSelect;
    }

    public function getDetail($id){
        $oSelect = $this
            ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->where('customer_contract_interest_by_month.customer_contract_interest_by_month_id',$id)
            ->select('customer_contract_interest_by_month.*','customer_contract.customer_contract_code')
            ->first();
        return $oSelect;
    }

    public function getInterestByMonth($customer_contract_id) {
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
            ->orderBy('customer_contract_interest_by_month_id','DESC');
        return $oSelect->first();
    }

    public function totalBonusCustomer($customerContractId)
    {
//        $select = $this->select(DB::raw("SUM(interest_banlance_amount) as interest_banlance_amount"))
        $select = $this->select(DB::raw("SUM(interest_amount) as interest_banlance_amount"))
//        $select = $this->select('interest_banlance_amount')
            ->where($this->table . '.customer_contract_id', $customerContractId)
//            ->where('withdraw_date_planning', '<=',Carbon::now())
            ->whereNotNull($this->table . '.confirmed_by')
            ->where($this->table . '.confirmed_by', '<>', 0);
        return $select->first();
    }

    public function totalBonusCustomerArr($customerContractId)
    {
//        $select = $this->select(DB::raw("SUM(interest_banlance_amount) as interest_banlance_amount"))
        $select = $this->select(DB::raw("SUM(interest_amount) as interest_banlance_amount"))
//        $select = $this->select('interest_banlance_amount')
            ->whereIn($this->table . '.customer_contract_id', $customerContractId)
//            ->where('withdraw_date_planning', '<=',Carbon::now())
            ->whereNotNull($this->table . '.confirmed_by')
            ->where($this->table . '.confirmed_by', '<>', 0);
        return $select->first();
    }

    /**
     * Tất cả record theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByCondition($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        $select->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    /**
     * Tất cả record theo điều kiện không tài khoản test
     * @param array $params
     *
     * @return mixed
     */
    public function getByConditionNotTest($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->join(
                'customers',
                'customers.customer_id',
                'orders.customer_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        $select
            ->where('customers.is_test','<>',1)
            ->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    /**
     * Số lượng hợp đồng
     * @param array $params
     *
     * @return mixed
     */
    public function getByContractQuantity($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            'products.product_category_id',
            'customer_contract.order_id'
        )
            ->join(
                'customer_contract',
                'customer_contract.customer_contract_id',
                $this->table . '.customer_contract_id'
            )
            ->join(
                'orders',
                'orders.order_id',
                'customer_contract.order_id'
            )
            ->join(
                'products',
                'products.product_id',
                'orders.product_id'
            );
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where('orders.process_status', $params['process_status']);
        }
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Trạng thái của order
        if (isset($params['product_category_id'])) {
            $select->where('products.product_category_id', $params['product_category_id']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where($this->table . '.interest_year', $params['year']);
        }
        $select->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy('orders.order_id');
        return $select->get()->count();
    }

    public function getAllByMonth($customer_id,$month){
        $oSelect = $this
            ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->join('products','products.product_id','customer_contract.product_id')
            ->where('customer_contract_interest_by_month.customer_id',$customer_id)
            ->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),$month)
            ->select('customer_contract_interest_by_month.*','customer_contract.customer_contract_code','products.product_name_vi')
            ->get();
        return $oSelect;
    }

    public function getInterestByMonthCount($customer_id,$month,$type){
        $oSelect = $this
            ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->where('customer_contract_interest_by_month.customer_id',$customer_id)
            ->where('customer_contract.term_time_type',0)
            ->orwhere(function ($oSelect){
                $oSelect
                    ->where('customer_contract.term_time_type',1)
                    ->whereNotNull('customer_contract_interest_by_month.confirmed_by');
            });
        if ($type == 'before') {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),'<',$month);
        } else {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),$month);
        }
            $oSelect = $oSelect->select(DB::raw("SUM(customer_contract_interest_by_month.interest_amount) as total"))->first();

        return $oSelect;
    }

    public function getInterestContractByMonthCount($customer_id,$customer_contract_id,$month,$type){
        $oSelect = $this
            ->leftJoin('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->leftJoin('withdraw_request','withdraw_request.customer_contract_id','customer_contract.customer_contract_id')
            ->where('customer_contract_interest_by_month.customer_id',$customer_id)
            ->where('customer_contract_interest_by_month.customer_contract_id',$customer_contract_id);
//            ->where(function ($oSelect) {
//                $oSelect
//                    ->where('customer_contract.term_time_type',0)
//                    ->orwhere(function ($oSelect){
//                        $oSelect
//                            ->where('customer_contract.term_time_type',1)
//                            ->whereNotNull('customer_contract_interest_by_month.confirmed_by');
//                    });
//            });
        if ($type == 'before') {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),'<',$month);
        } else if($type == 'accountStatement'){
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),$month);
        } else {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),'<=',$month);
        }
        $oSelect = $oSelect->select(
//            DB::raw("SUM(customer_contract_interest_by_month.interest_amount) as total"),
            "customer_contract_interest_by_month.interest_amount as total",
            'customer_contract_interest_by_month.withdraw_date_planning'
        )->first();
        return $oSelect;
    }

    public function checkContractMonth($data,$day){
        $oSelect = $this
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d') )"),$day)
            ->where('customer_contract_id',$data['customer_contract_id'])
            ->get();
        return $oSelect;
    }

    public function getTotalInterestByMonth($customer_contract_id){
        $oSelect = $this
            ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->where('customer_contract_interest_by_month.customer_contract_id',$customer_contract_id)
            ->where('customer_contract.term_time_type',0)
            ->orwhere(function ($oSelect){
                $oSelect
                    ->where('customer_contract.term_time_type',1)
                    ->whereNotNull('customer_contract_interest_by_month.confirmed_by');
            });
        $oSelect = $oSelect->select(DB::raw("SUM(customer_contract_interest_by_month.interest_amount) as total"))->first();

        return $oSelect;
    }

    public function getInterestByDayCount($customer_id,$day,$type){
        $oSelect = $this
            ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->where('customer_contract_interest_by_month.customer_id',$customer_id)
            ->where('customer_contract.term_time_type',0)
            ->orwhere(function ($oSelect){
                $oSelect
                    ->where('customer_contract.term_time_type',1)
                    ->whereNotNull('customer_contract_interest_by_month.confirmed_by');
            });
        if ($type == 'before') {
            $oSelect = $oSelect->whereDate('customer_contract_interest_by_month.created_at','<',$day);
        } else {
            $oSelect = $oSelect->whereDate('customer_contract_interest_by_month.created_at',$day);
        }
        $oSelect = $oSelect->select(DB::raw("SUM(customer_contract_interest_by_month.interest_amount) as total"))->first();

        return $oSelect;
    }

    public function getInfoInterestByMonth($customer_contract_id) {
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
//            ->where($this->table . '.interest_year',$year)
//            ->where($this->table . '.interest_month',$month)
            ->orderBy('customer_contract_interest_by_month_id','DESC');
        return $oSelect->first();
    }

    public function getLastContractConfirm($customer_contract_id) {
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
            ->whereNotNull($this->table . '.confirmed_by')
            ->where($this->table . '.confirmed_by', '<>', 0)
            ->orderBy('customer_contract_interest_by_month_id','DESC')
            ->first();
        return $oSelect;
    }

    public function confirmListInterst($listInterest,$data){
        $oSelect = $this->whereIn('customer_contract_interest_by_month_id',$listInterest)->update($data);
        return $oSelect;
    }

    public function getListContract($listInterest){
        $oSelect = $this->whereIn('customer_contract_interest_by_month_id',$listInterest)->select('customer_contract_id')->get();
        return $oSelect;
    }

    public function getListExport($filter){
        $ds = $this
            ->select(
                "customer_contract.customer_contract_code",
                "customers.full_name",
                "customers.phone",
                "customers.ic_no",
                "{$this->table}.interest_year",
                "{$this->table}.interest_month",
                "{$this->table}.interest_rate_by_month",
                "{$this->table}.interest_amount",
                "{$this->table}.interest_total_amount",
                "{$this->table}.interest_banlance_amount",
                "{$this->table}.withdraw_date_planning"
            )
            ->join('customer_contract','customer_contract.customer_contract_id','customer_contract_interest_by_month.customer_contract_id')
            ->join('customers','customers.customer_id','customer_contract_interest_by_month.customer_id')
            ->where('customer_contract.term_time_type',1)
            ->whereNull('customer_contract_interest_by_month.confirmed_by');

        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $ds->where(function ($query) use ($search) {
                $query->where('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                    ->orWhere('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.ic_no', 'like', '%' . $search . '%');
            });
            unset($filter["search"]);
        }

        if(isset($filter["created_at"]) && $filter["created_at"] != ""){
            $arr_filter = explode(" - ",$filter["created_at"]);
            $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
            $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
            $ds = $ds->whereBetween('customer_contract_interest_by_month.withdraw_date_planning', [$from , $to]);
            unset($filter["created_at"]);
        }

        $ds = $ds->orderBy("{$this->table}.customer_contract_interest_by_month_id", "ASC");
        return $ds->get();
    }

    public function getLastInterestByMonth($customer_contract_id){
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
            ->orderBy('customer_contract_interest_by_month_id','DESC')
            ->limit(1)
            ->first();
        return $oSelect;

    }
}