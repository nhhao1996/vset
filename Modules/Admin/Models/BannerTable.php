<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class BannerTable extends Model
{
    use ListTableTrait;
    protected $table = "banner";
    protected $primaryKey = "banner_id";
    protected $fillable = [
        'banner_id',
        'name',
        'img_desktop_vi',
        'img_desktop_en',
        'img_mobile_vi',
        'img_mobile_en',
        'from',
        'to',
        'is_active',
        'created_at',
        'created_by',
        'update_at',
        'update_by',
    ];

    public function _getList(&$filters = []) {
        $oSelect = $this;
        if (isset($filters['search'])) {
            $oSelect = $oSelect
                ->where('name', 'like', '%' . $filters['search'] . '%');
        }

        if (isset($filters['is_active'])) {
            $oSelect = $oSelect
                ->where('is_active', 'like', '%' . $filters['is_active'] . '%');
        }

        if (isset($filters['time'])) {
            $dateTime = explode(" - ", $filters['time']);
            $from = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $to = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');
            $oSelect = $oSelect
                ->where(function ($query) use ($from,$to) {
                    $query = $query->whereBetween('from',[$from,$to])
                        ->orWhereBetween('to',[$from,$to])
                        ->orWhere(function ($query) use ($from) {
                            $query = $query->where('from','>',$from)
                                ->where('to','<',$from);
                        })
                        ->orWhere(function ($query) use ($to) {
                            $query = $query->where('from','<',$to)
                                ->where('to','>',$to);
                        });
                });
            unset($filters['time']);
        }

        unset($filters['search']);
        unset($filters['is_active']);
        $oSelect = $oSelect->orderBy('banner_id','DESC');
        return $oSelect;
    }

    public function checkBanner($id, $from, $to, $params = []) {
        $oSelect = $this
            ->where(function ($query) use ($from,$to) {
                $query = $query->whereBetween('from',[$from,$to])
                    ->orWhereBetween('to',[$from,$to])
                    ->orWhere(function ($query) use ($from) {
                        $query = $query->where('from','>',$from)
                            ->where('to','<',$from);
                    })
                    ->orWhere(function ($query) use ($to) {
                        $query = $query->where('from','<',$to)
                            ->where('to','>',$to);
                    });
            });

        if ($id != null) {
            $oSelect->where('banner_id','<>', $id);
        }
        if (isset($params['is_actived'])) {
            $oSelect->where('is_active', $params['is_actived']);
        }
        $oSelect = $oSelect->get();

        return $oSelect;
    }

    public function createPopup($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    /**
     * Lấy chi tiết popup khuyến mãi
     * @param $id
     */
    public function getDetail($id){
        $oSelect = $this->where('banner_id',$id)->first();
        return $oSelect;
    }

    public function editPopup($data,$id){
        $oSelect = $this->where('banner_id',$id)->update($data);
        return $oSelect;
    }


}