<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class FaqTable extends Model
{
    use ListTableTrait;
    protected $table = "faq";
    protected $primaryKey = "faq_id";

    protected $fillable = [
        'faq_id',
        'faq_group',
        'faq_type',
        'faq_title',
        'faq_title_en',
        'faq_content',
        'faq_content_en',
        'faq_position',
        'is_actived',
        'is_deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    public function _getList(&$filter = [])
    {
        $select = $this
            ->select(
                'faq.*',
                'faq_group.faq_group_title',
                DB::raw("(CASE
                    WHEN  {$this->table}.faq_type = 'faq' THEN 'Hỏi đáp'
                    WHEN  {$this->table}.faq_type = 'privacy_policy' THEN 'Chính sách bảo mật'
                    WHEN  {$this->table}.faq_type = 'terms_use' THEN 'Điều khoản sử dụng'
                    ELSE  0
                    END
                ) as faq_type")
            )
            ->where('faq.is_deleted', 0)
//            ->join('faq_group', 'faq_group.faq_group_id', '=', $this->table.'.faq_group')
            ->leftJoin('faq_group', 'faq_group.faq_group_id', '=', $this->table.'.faq_group')
            ->orderBy('faq.faq_id','DESC');

        if (isset($filter['keyword_faq$faq_title']) != "") {
            $search = $filter['keyword_faq$faq_title'];
            $select->where(function ($query) use ($search) {
                $query
                    ->where('faq.faq_title', 'like', '%' . $search . '%')
                    ->orWhere('faq.faq_title_en', 'like', '%' . $search . '%');
            });
            unset($filter['keyword_faq$faq_title']);
        }
        if (isset($filter['created_at']) != "") {
            $arr_filter = explode(" - ", $filter["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select->whereBetween('faq.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
            unset($filter['created_at']);
        }
        return $select;
    }

    public function getById($id)
    {
        $select = $this
            ->select(
                'faq.*',
                'staffs_create_name.full_name as staffs_create_name',
                'staffs_update_name.full_name as staffs_update_name',
                DB::raw("(CASE
                    WHEN  {$this->table}.faq_type = 'faq' THEN 'Hỏi đáp'
                    WHEN  {$this->table}.faq_type = 'privacy_policy' THEN 'Chính sách bảo mật'
                    WHEN  {$this->table}.faq_type = 'terms_use' THEN 'Điều khoản sử dụng'
                    ELSE  0
                    END
                ) as faq_type")
            )
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","faq.created_by")
            ->leftJoin("staffs as staffs_update_name","staffs_update_name.staff_id","=","faq.updated_by")
            ->where('faq.faq_id',$id)->first();
        return $select;
    }

    /**
     * Thêm faq group
     *
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        return $this->create($data)->{$this->primaryKey};
    }

    public function detail($id)
    {
        $select = $this->select(
            $this->table.'.*',
            DB::raw("(CASE
                    WHEN  {$this->table}.faq_type = 'faq' THEN 'Hỏi đáp'
                    WHEN  {$this->table}.faq_type = 'privacy_policy' THEN 'Chính sách bảo mật'
                    WHEN  {$this->table}.faq_type = 'terms_use' THEN 'Điều khoản sử dụng'
                    ELSE  0
                    END
                ) as faq_type")
        )->where($this->table.'.faq_id', $id)
            ->first();

        return $select;
    }

    public function edit($data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);
    }

    /**
     * Đánh dấu xóa nhóm nội dung
     *
     * @param $id
     * @return mixed
     */
    public function remove($id)
    {
        return $this->where($this->primaryKey, $id)->update(["is_deleted" => 1]);
    }
}