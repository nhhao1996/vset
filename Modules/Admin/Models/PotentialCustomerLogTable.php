<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class PotentialCustomerLogTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_potential_log';
    protected $primaryKey = 'customer_potential_log_id';
    protected $fillable = [
        'customer_potential_log_id',
        'customer_id',
        'type',
        'obj_id',
        'created_at',
    ];

    public $timestamps = false;

    public function _getList(&$filters = [])
    {
        $select = $this;
        if (isset($filters['customer_id'])){
            $select = $select->where('customer_id',$filters['customer_id']);
            unset($filters['customer_id']);
        }
        $select = $select
            ->whereNotNull('obj_id')
            ->select(
            'customer_potential_log.*',
            DB::raw('count(*) as total')
        )
        ->groupBy('obj_id')
        ->groupBy('type')
        ->orderBy('customer_potential_log_id','DESC');
        return $select;
    }
}