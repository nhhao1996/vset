<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class SupportRequestType extends Model
{
    protected $table = 'support_request_type';
    protected $primaryKey = 'support_request_type_id';

    protected $fillable = ["support_request_type_id", "support_type", "support_request_type_name_vi",
        "support_request_type_name_en", "is_actived", "is_deleted",
        "created_at", "created_by", "updated_at", "updated_by"];

    public function getAllType(){
        $select = $this
            ->select(
                $this->table.'.support_request_type_id',
                $this->table.'.support_request_type_name_vi as support_request_type_name'
            )->where($this->table.'.is_deleted', 0)->where($this->table.'.is_actived', 1);
        return $select->get();
    }
}
