<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/23/2021
 * Time: 3:33 PM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StockHistoryDivideTable extends Model
{
    protected $table = "stock_history_divide";
    protected $primaryKey = "stock_history_divide_id";
    protected $fillable = [
        "stock_history_divide_id",
        "stock_id",
        "payment_method_id",
        "date_publish",
        "year",
        "stock_money_rate",
        "stock_bonus_before",
        "stock_bonus_after",
        "is_active",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
    ];
    public function getHistoryShareDividendLatest(){
        $data = $this->select(
            "{$this->table}.stock_history_divide_id",
            "{$this->table}.stock_id",
            "{$this->table}.year",
            "{$this->table}.stock_bonus_before",
            "{$this->table}.stock_bonus_after",
            "{$this->table}.stock_money_rate",
            "{$this->table}.date_publish"
        )
        ->where("is_active","=","0");
        return $data->orderBy('stock_history_divide_id', 'desc')->first();
    }
    public function getListShareDividend()
    {
        $data = $this->select(
            "{$this->table}.stock_history_divide_id",
            "{$this->table}.stock_id",
            "{$this->table}.year",
            "{$this->table}.stock_bonus_before",
            "{$this->table}.stock_bonus_after",
            "{$this->table}.stock_money_rate",
            "{$this->table}.date_publish"
        )
        ->where("is_active","=","0")
        ->orderBy('stock_history_divide_id', 'asc');
        return $data->get();
    }
    public function updateHistoryShareDividend($data, $id){
        return $this->where("stock_history_divide_id","=",$id)->update($data);
    }
}