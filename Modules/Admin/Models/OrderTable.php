<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/27/2018
 * Time: 1:21 PM
 */

namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class OrderTable extends Model
{
    use ListTableTrait;
    protected $table = "orders";
    protected $primaryKey = "order_id";
    protected $fillable = [
        'order_id','order_code','customer_id','product_id','quantity','price_standard','interest_rate_standard',
        'term_time_type','investment_time_id','withdraw_interest_time_id','interest_rate','commission_rate',
        'month_interest','total_interest','bonus_rate','total','bonus','total_amount','payment_method_id',
        'process_status','payment_date','customer_name','customer_phone','customer_email','customer_residence_address',
        'customer_ic_no','refer_id','commission','order_description','reason','is_deleted','created_by',
        'updated_by','created_at','updated_at','branch_id','tranport_charge','discount','customer_description',
        'order_source_id','transport_id','voucher_code','discount_member','is_apply','total_saving','total_tax',
        'is_extend','contract_code_extend'
    ];
    const IS_ACTIVED = 1;
    const IS_NOT_DELETED = 0;
    const NOT_DELETED = 0;
    const CANCEL = 'ordercancle';
    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->insertGetId($data);
        return $add;
    }

    public function getDetail($id){
        $oSelect = $this->where('order_id',$id)->first();
        return $oSelect;
    }

    public function getById($id)
    {
        $select = $this
            ->select(
                'orders.*',
                'refer.full_name as refer_name',
                'staffs_create_name.full_name as staffs_create_name',
                'staffs_update_name.full_name as staffs_update_name',
                'branches.branch_name',
                'order_sources.order_source_name',
                'productbonds.product_name_vi as product_name_vi',
                'productbonds.product_short_name_vi as product_short_name_vi',
                'productbonds.description_vi as description_vi',
                'productbonds.product_name_en as product_name_en',
                'productbonds.product_short_name_en as product_short_name_en',
                'productbonds.description_en as description_en',
                'productbonds.product_avatar_vi as product_avatar',
                'productbonds.product_image_detail_vi as product_image_detail',
                'productbonds.product_category_id',
                'paymenttable.payment_method_name_vi',
                'productbonds.product_code',
                'investment_time.investment_time_month',
                'withdraw_interest_time.withdraw_interest_month',
                DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác nhận'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Đơn hàng bị hủy'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status"),
                DB::raw("(CASE
                    WHEN  productbonds.product_category_id = '1' THEN 'Hợp tác đầu tư'
                    WHEN  productbonds.product_category_id = '2' THEN 'Tiết kiệm'
                    ELSE  ''
                    END
                ) as type_bonds")
            )
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","orders.created_by")
            ->leftJoin("staffs as staffs_update_name","staffs_update_name.staff_id","=","orders.updated_by")
            ->leftJoin("branches","branches.branch_id","=","orders.branch_id")
            ->leftJoin("products as productbonds","productbonds.product_id","=","orders.product_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","orders.order_source_id")
            ->leftJoin("customers as refer","refer.customer_id","=","orders.refer_id")
            ->leftJoin("investment_time","investment_time.investment_time_id","=","orders.investment_time_id")
            ->leftJoin("withdraw_interest_time","withdraw_interest_time.withdraw_interest_time_id","=","orders.withdraw_interest_time_id")
            ->where('orders.order_id',$id)
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","orders.payment_method_id")
            ->first();
        //dd($select);
        return $select;
    }

    public function _getList(&$filter = [])
    {
//    var_dump($filter); die;

        $ds = $this->select(
            $this->table.'.order_id',
            $this->table.'.order_code',
            'products.product_name_vi',
            $this->table.'.price_standard',
            $this->table.'.quantity',
            $this->table.'.total',
            $this->table.'.process_status',
            $this->table.'.created_at',
            'payment_method.payment_method_name_vi'

        )   ->leftJoin('products', 'products.product_id', '=', $this->table.'.product_id')
            ->leftJoin('payment_method', 'payment_method.payment_method_id', '=', $this->table.'.payment_method_id')
            ->orderBy('orders.created_at', 'desc')
            ->where('orders.is_deleted', 0)
            ->groupBy('orders.order_id');

        if (isset($filter['id'])) {
            $ds ->leftJoin('customers', 'customers.customer_id', '=', $this->table.'.customer_id')
                ->where('customers.customer_id','=', $filter['id']);
            unset($filter['id']);
        }

        return $ds;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->where('order_id', $id)->update($data);
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $this->where('order_id', $id)->update(['is_deleted' => 1, 'process_status' => 'payfail']);
    }

    public function detailDayCustomer($id)
    {
        $ds = $this->select('created_at', DB::raw('count(created_at) as number'))
            ->where('customer_id', $id)
            ->groupBy(DB::raw('Date(created_at)'))
            ->get();
        return $ds;
    }

    public function getIndexReportRevenue()
    {
        $select = $this->where('is_deleted', 0)->get();
        return $select;
    }

    public function getValueByYear($year, $startTime = null, $endTime = null)
    {
        $select = null;
        if ($year != null) {
            $yearTime = substr($startTime, 0, -6);
            if ($yearTime != false) {
                $select = $this->where('is_deleted', 0)->whereYear('created_at', $yearTime);
            } else {
                $select = $this->where('is_deleted', 0)->whereYear('created_at', $year);
            }
            if ($startTime != null) {
                $select->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
            }
        } else {
            if ($startTime != null) {
                $select = $this->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
            }
        }
        if (Auth::user()->is_admin != 1) {
            $select->where('branch_id', Auth::user()->branch_id);
        }
        return $select->get();
    }

    public function getValueByDate($date, $field = null, $valueField = null, $field2 = null, $valueField2 = null)
    {
        $select = null;
        if ($field == null || $valueField == null) {
            $select = $this->select(DB::raw('sum(amount) as total'))
                ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
                ->where('process_status', 'paysuccess')->where('is_deleted', 0);
        } else if ($field != null && $valueField != null && $field2 == null && $valueField2 == null) {
            $select = $this->select(DB::raw('sum(amount) as total'))
                ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
                ->where($field, $valueField)
                ->where('process_status', 'paysuccess')
                ->where('is_deleted', 0);
        } else if ($field != null && $valueField != null && $field2 != null && $valueField2 != null) {
            $select = $this->select(DB::raw('sum(amount) as total'))
                ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
                ->where([$field => $valueField], [$field2 => $valueField2])
                ->where('process_status', 'paysuccess')
                ->where('is_deleted', 0);
        }
        return $select->get();
    }

    //Lấy dữ liệu với tham số truyền vào(thời gian, cột)
    public function getValueByParameter($date, $filer, $valueFilter)
    {
        $select = $this->select(DB::raw('sum(amount) as total'))
            ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
            ->where($filer, $valueFilter)->where('process_status', 'paysuccess')->where('is_deleted', 0)
            ->get();
        return $select;
    }

    //Lấy giá trị từ ngày - đến ngày.
    public function getValueByDay($startTime, $endTime)
    {
        $select = $this->where('is_deleted', 0)->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
        return $select->get();
    }

    //Lấy dữ liệu với tham số truyền vào(thời gian, cột) 2
    public function getValueByParameter2($startTime, $endTime, $filer, $valueFilter)
    {
        $select = null;
        if ($filer == null && $valueFilter == null) {
            $select = $this->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where('is_deleted', 0);
        } else {
            $select = $this->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where($filer, $valueFilter)->where('is_deleted', 0);
        }
        return $select->get();
    }

    //Lấy giá trị theo năm, cột và giá trị cột truyền vào
    public function fetchValueByParameter($year, $startTime, $endTime, $field, $fieldValue)
    {
        $select = null;
        if ($startTime == null && $endTime == null) {
            $select = $this->whereYear('created_at', $year)
                ->where($field, $fieldValue)->where('is_deleted', 0);
        } else {
            $select = $this->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where($field, $fieldValue)->where('is_deleted', 0);
        }
        return $select->get();
    }

    //Lấy giá trị theo năm, cột và giá trị 2 cột truyền vào
    public function fetchValueByParameter2($year, $startTime, $endTime, $field, $fieldValue, $field2, $fieldValue2)
    {
        $select = null;
        if ($year == null) {
            $select = $this->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where($field, $fieldValue)
                ->where($field2, $fieldValue2)
                ->where('is_deleted', 0);
        } else {
            $select = $this->whereYear('created_at', $year)
                ->where($field, $fieldValue)
                ->where($field2, $fieldValue2)
                ->where('is_deleted', 0);
        }
        return $select->get();
    }

    //Lấy các giá trị theo created_at, branch_id và customer_id
    public function getValueByDate2($date, $branch, $customer)
    {
        $select = $this->select('amount')
            ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
            ->where('branch_id', $branch)
            ->where('customer_id', $customer)
            ->where('process_status', 'paysuccess')
            ->where('is_deleted', 0);
        return $select->get();
    }

    //Lấy các giá trị theo created_at, branch_id và created_by
    public function getValueByDate3($date, $branch, $staff)
    {
        $select = null;
        if ($branch != null) {
            $select = $this->select('amount')
                ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
                ->where('branch_id', $branch)
                ->where('created_by', $staff)
                ->where('process_status', 'paysuccess')
                ->where('is_deleted', 0);

        } else {
            $select = $this->select('amount')
                ->whereBetween('created_at', [$date . " 00:00:00", $date . " 23:59:59"])
                ->where('created_by', $staff)
                ->where('process_status', 'paysuccess')
                ->where('is_deleted', 0);
        }
        return $select->get();
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng.
    public function getDataReportGrowthByCustomer($year, $month, $operator, $customerOdd, $field, $valueField)
    {
        $select = null;
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select(
                'customers.customer_id as customer_id',
                'orders.created_at as order_created_at',
                'orders.branch_id as order_branch_id'
            );

        if ($field == null || $valueField == null) {
            if ($customerOdd == null) {
                if ($operator == null) {
                    //Khách hàng mới.
                    $select->whereYear('customers.created_at', $year)
                        ->whereMonth('customers.created_at', $month)
                        ->whereYear('orders.created_at', $year)
                        ->whereMonth('orders.created_at', $month)
                        ->where('customers.customer_id', '<>', 1);

                } else {
                    //Khách hàng cũ
                    $select->whereMonth('customers.created_at', $operator, $month)
                        ->whereYear('orders.created_at', $year)
                        ->whereMonth('orders.created_at', $month)
                        ->where('customers.customer_id', '<>', 1);
                }
                $select->groupBy('orders.customer_id');
            } else {
                //Khách vãng lai.
                $select->whereYear('orders.created_at', $year)
                    ->whereMonth('orders.created_at', $month)
                    ->where('customers.customer_id', '=', 1);
            }
        } else {
            if ($customerOdd == null) {
                if ($operator == null) {
                    //Khách hàng mới.
                    $select->whereYear('customers.created_at', $year)
                        ->whereMonth('customers.created_at', $month)
                        ->whereYear('orders.created_at', $year)
                        ->whereMonth('orders.created_at', $month)
                        ->where($field, $valueField)
                        ->where('customers.customer_id', '<>', 1);

                } else {
                    //Khách hàng cũ
                    $select->whereMonth('customers.created_at', $operator, $month)
                        ->whereYear('orders.created_at', $year)
                        ->whereMonth('orders.created_at', $month)
                        ->where($field, $valueField)
                        ->where('customers.customer_id', '<>', 1);
                }
                $select->groupBy('orders.customer_id');
            } else {
                //Khách vãng lai.
                $select->whereYear('orders.created_at', $year)
                    ->whereMonth('orders.created_at', $month)
                    ->where($field, $valueField)
                    ->where('customers.customer_id', '=', 1);
            }
        }
        $select->where('customers.is_deleted', 0)
            ->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess');

        return $select->get();

    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng theo năm cho từng chi nhánh.
    public function getDataReportGrowthCustomerByYear($year, $operator, $customerOdd, $branch)
    {
        $select = null;
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select(
                'customers.customer_id as customer_id',
                'orders.created_at as created_at');

        if ($customerOdd == null) {
            if ($operator == null) {
                //Khách hàng mới.
                $select->whereYear('customers.created_at', $year)
                    ->whereYear('orders.created_at', $year)
                    ->where('orders.branch_id', $branch)
                    ->where('customers.customer_id', '<>', 1);

            } else {
                //Khách hàng cũ
                $select->where('orders.branch_id', $branch)
                    ->whereYear('customers.created_at', '<>', $year)
                    ->whereYear('orders.created_at', $year)
                    ->where('customers.customer_id', '<>', 1);
            }
            $select->groupBy('orders.customer_id');
        } else {
            //Khách vãng lai.
            $select->whereYear('orders.created_at', $year)
                ->where('orders.branch_id', $branch)
                ->where('customers.customer_id', '=', 1);
        }
        $select->where('customers.is_deleted', 0)
            ->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess');
        return $select->get();
    }

    //Thống kê tăng trưởng khách hàng(theo nhóm khách hàng).
    public function getValueReportGrowthByCustomerCustomerGroup($year, $branch)
    {
        $select = null;
        if ($branch == null) {
            $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
                ->leftJoin('customer_groups', 'customer_groups.customer_group_id', '=', 'customers.customer_group_id')
                ->select(
                    'customer_groups.group_name as group_name',
                    DB::raw("COUNT(orders.customer_id) as totalCustomer")
                );
        } else {
            $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
                ->leftJoin('customer_groups', 'customer_groups.customer_group_id', '=', 'customers.customer_group_id')
                ->select(
                    'customer_groups.group_name as group_name',
                    DB::raw("COUNT(orders.customer_id) as totalCustomer")
                )
                ->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess')
            ->whereYear('orders.created_at', $year)
            ->where('customers.is_deleted', 0)
            ->where('orders.customer_id', '<>', 1)
            ->groupBy('customer_groups.customer_group_id');
        return $select->get();
    }

    //Thống kê tăng trưởng khách hàng(theo nguồn khách hàng).
    public function getValueReportGrowthByCustomerCustomerSource($year, $branch)
    {
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=', 'customers.customer_source_id')
            ->select(
                'customer_sources.customer_source_name as customer_source_name',
                DB::raw("COUNT(orders.customer_id) as totalCustomer")
            );
        if ($branch != null) {
            $select->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess')
            ->whereYear('orders.created_at', $year)
            ->where('customer_sources.is_deleted', 0)
            ->where('customers.is_deleted', 0)
            ->where('orders.customer_id', '<>', 1)
            ->groupBy('customer_sources.customer_source_id');
        return $select->get();
    }

    //Thống kê tăng trưởng khách hàng(theo giới tính).
    public function getValueReportGrowthByCustomerCustomerGender($year, $branch)
    {
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select(
                'customers.gender as gender',
                DB::raw("COUNT(orders.customer_id) as totalCustomer")
            );
        if ($branch != null) {
            $select->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess')
            ->whereYear('orders.created_at', $year)
            ->where('customers.is_deleted', 0)
            ->where('orders.customer_id', '<>', 1)
            ->groupBy('customers.gender');
        return $select->get();
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng(từ ngày đến ngày và/hoặc chi nhánh).
    public function getDataReportGrowthByCustomerDataBranch($startTime, $endTime, $operator, $customerOdd, $branch)
    {
        $select = null;
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select(
                'customers.customer_id as customer_id',
                'orders.created_at as order_created_at',
                'orders.branch_id as order_branch_id'
            );
        if (Auth::user()->is_admin != 1) {
            $branch = Auth::user()->branch_id;
        }
//        dd(Auth::user()->branch_id, $startTime, $endTime, $operator, $customerOdd, $branch);
        if ($branch == null) {
            if ($customerOdd == null) {
                if ($operator == null) {
                    //Khách hàng mới.
                    $select->whereBetween('customers.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->where('customers.customer_id', '<>', 1);

                } else {
                    //Khách hàng cũ
                    $select->whereNotBetween('customers.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->where('customers.customer_id', '<>', 1);
                }
                $select->groupBy('orders.customer_id');
            } else {
                //Khách vãng lai.
                $select->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                    ->where('customers.customer_id', '=', 1);
            }
        } else {
            if ($customerOdd == null) {
                if ($operator == null) {
                    //Khách hàng mới.
                    $select->whereBetween('customers.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->where('customers.customer_id', '<>', 1)
                        ->where('orders.branch_id', $branch);

                } else {
                    //Khách hàng cũ
                    $select->whereNotBetween('customers.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                        ->where('customers.customer_id', '<>', 1)
                        ->where('orders.branch_id', $branch);
                }
                $select->groupBy('orders.customer_id');
            } else {
                //Khách vãng lai.
                $select->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                    ->where('customers.customer_id', '=', 1)
                    ->where('orders.branch_id', $branch);
            }
        }
        $select->where('customers.is_deleted', 0)
            ->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess');

        return $select->get();
    }

    //Thống kê tăng trưởng khách hàng(theo nhóm khách hàng) theo từ ngày đến ngày và/hoặc chi nhánh.
    public function getValueReportGrowthByCustomerCustomerGroupTimeBranch($startTime, $endTime, $branch)
    {
        $select = null;
        if (Auth::user()->is_admin != 1) {
            $branch = Auth::user()->branch_id;
        }
        if ($branch == null) {
            $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
                ->leftJoin('customer_groups', 'customer_groups.customer_group_id', '=', 'customers.customer_group_id')
                ->select(
                    'customer_groups.group_name as group_name',
                    DB::raw("COUNT(orders.customer_id) as totalCustomer")
                );
        } else {
            $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
                ->leftJoin('customer_groups', 'customer_groups.customer_group_id', '=', 'customers.customer_group_id')
                ->select(
                    'customer_groups.group_name as group_name',
                    DB::raw("COUNT(orders.customer_id) as totalCustomer")
                )
                ->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess')
            ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
            ->where('customers.is_deleted', 0)
            ->where('orders.customer_id', '<>', 1)
            ->groupBy('customer_groups.customer_group_id');
        return $select->get();
    }

    //Thống kê tăng trưởng khách hàng(theo giới tính) theo từ ngày đến ngày và/hoặc chi nhánh.
    public function getValueReportGrowthByCustomerCustomerGenderTimeBranch($startTime, $endTime, $branch)
    {
        if (Auth::user()->is_admin != 1) {
            $branch = Auth::user()->branch_id;
        }
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select(
                'customers.gender as gender',
                DB::raw("COUNT(orders.customer_id) as totalCustomer")
            );
        if ($branch != null) {
            $select->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess')
            ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
            ->where('customers.is_deleted', 0)
            ->where('orders.customer_id', '<>', 1)
            ->groupBy('customers.gender');
        return $select->get();
    }

    //Thống kê tăng trưởng khách hàng(theo nguồn khách hàng) theo từ ngày tới ngày và/hoặc chi nhánh.
    public function getValueReportGrowthByCustomerCustomerSourceTimeBranch($startTime, $endTime, $branch)
    {
        if (Auth::user()->is_admin != 1) {
            $branch = Auth::user()->branch_id;
        }
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=', 'customers.customer_source_id')
            ->select(
                'customer_sources.customer_source_name as customer_source_name',
                DB::raw("COUNT(orders.customer_id) as totalCustomer")
            );
        if ($branch != null) {
            $select->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess')
            ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
            ->where('customer_sources.is_deleted', 0)
            ->where('customers.is_deleted', 0)
            ->where('orders.customer_id', '<>', 1)
            ->groupBy('customer_sources.customer_source_id');
        return $select->get();
    }

    //Lấy dữ liệu theo năm/từ ngày đến ngày và chi nhánh
    public function getValueByYearAndBranch($year, $branch, $startTime, $endTime)
    {
        $select = null;
        if ($year != null) {
            $select = $this->whereYear('created_at', $year);
        } else {
            if ($startTime != null) {
                $select = $this->whereBetween('created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
            }
        }
        if ($branch != null) {
            $select->where('branch_id', $branch);
        }
        return $select->get(['customer_id', 'branch_id', 'process_status', 'order_source_id', 'created_at']);
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng theo năm cho từng chi nhánh.
    public function getDataReportGrowthCustomerByTime($startTime, $endTime, $operator, $customerOdd, $branch)
    {
        $select = null;
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select(
                'customers.customer_id as customer_id',
                'orders.created_at as created_at');

        if ($customerOdd == null) {
            if ($operator == null) {
                //Khách hàng mới.
                $select->whereBetween('customers.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                    ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                    ->where('orders.branch_id', $branch)
                    ->where('customers.customer_id', '<>', 1);

            } else {
                //Khách hàng cũ
                $select->where('orders.branch_id', $branch)
                    ->whereNotBetween('customers.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                    ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                    ->where('customers.customer_id', '<>', 1);
            }
            $select->groupBy('orders.customer_id');
        } else {
            //Khách vãng lai.
            $select->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where('orders.branch_id', $branch)
                ->where('customers.customer_id', '=', 1);
        }
        $select->where('customers.is_deleted', 0)
            ->where('orders.is_deleted', 0)
            ->where('orders.process_status', 'paysuccess');
        return $select->get();
    }

    //search dashboard
    public function searchDashboard($keyword)
    {
        $time = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->subDays(30))->format('Y-m-d');
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'orders.created_by')
            ->select(
                'orders.order_id as order_id',
                'orders.order_code as order_code',
                'customers.full_name as full_name',
                'customers.phone as phone1',
                'staffs.full_name as staff_name',
                'orders.total as total',
                'orders.process_status as process_status',
                'orders.created_at as created_at',
                'orders.order_id as order_id',
                'customers.customer_avatar as customer_avatar'
            )
            ->where(function ($query) use ($keyword) {
                $query->where('customers.full_name', 'like', '%' . $keyword . '%')
                    ->orWhere('customers.phone1', 'like', '%' . $keyword . '%')
                    ->orWhere('customers.email', 'like', '%' . $keyword . '%')
                    ->orWhere('orders.order_code', 'like', '%' . $keyword . '%');
            })
            ->where('orders.created_at', '>', $time . ' 00:00:00');
        if (Auth::user()->is_admin != 1) {
            $select->where('orders.branch_id', Auth::user()->branch_id);
        };
        return $select->get();
    }

    public function getAllByCondition($startTime, $endTime, $branch)
    {
        if (Auth::user()->is_admin != 1) {
            $branch = Auth::user()->branch_id;
        }
        $select = $this->where('orders.process_status', 'paysuccess');
        if ($branch != null) {
            $select->where('orders.branch_id', $branch);
        }
        $select->where('orders.is_deleted', 0)
            ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
        return $select->get();
    }

    public function getCustomerDetail($id)
    {
        $ds = $this->leftJoin('customers', 'customers.customer_id', '=', 'orders.customer_id')
            ->select('customers.full_name as full_name',
                'customers.phone1 as phone',
                'orders.process_status as process_status',
                'orders.order_id as order_id',
                'customers.gender as gender',
                'customers.customer_id as customer_id',
                'orders.order_code as order_code'
            )
            ->where('orders.order_id', $id);
        return $ds->first();
    }

    //Lấy dữ liệu với tham số truyền vào(thời gian, cột) 2. Lấy tiền đã thanh toán
    public function getValueByParameter3($startTime, $endTime, $filer, $valueFilter)
    {

        $select = $this->leftJoin('receipts', 'receipts.order_id', '=', 'orders.order_id')
            ->select(
                'orders.order_id',
                'orders.order_code',
                'orders.customer_id',
                'orders.branch_id',
                'orders.total',
                'orders.discount',
                'receipts.amount_paid as amount',
                'orders.tranport_charge',
                'orders.created_by',
                'orders.updated_by',
                'orders.created_at',
                'orders.updated_at',
                'orders.process_status',
                'orders.order_description',
                'orders.payment_method_id',
                'orders.order_source_id',
                'orders.transport_id',
                'orders.voucher_code',
                'orders.is_deleted',
                'receipts.amount as total_amount',
                'receipts.status as  receipts_status'
            )
            ->where('orders.is_deleted', 0);
        if ($filer == null && $valueFilter == null) {
            $select->whereBetween('orders.created_at',
                [$startTime . " 00:00:00", $endTime . " 23:59:59"]
            );
        } else {
            $select->whereBetween('orders.created_at',
                [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where($filer, $valueFilter)->where('orders.is_deleted', 0);
        }

        return $select->get();
    }

    public function getValueByYear2($year, $startTime, $endTime)
    {
        $select = $this->leftJoin('receipts', 'receipts.order_id', '=',
            'orders.order_id')
            ->select(
                'orders.order_id',
                'orders.order_code',
                'orders.customer_id',
                'orders.branch_id',
                'orders.total',
                'orders.discount',
                'receipts.amount_paid as amount',
                'orders.tranport_charge',
                'orders.created_by',
                'orders.updated_by',
                'orders.created_at',
                'orders.updated_at',
                'orders.process_status',
                'orders.order_description',
                'orders.payment_method_id',
                'orders.order_source_id',
                'orders.transport_id',
                'orders.voucher_code',
                'orders.is_deleted',
                'receipts.amount as total_amount',
                'receipts.status as  receipts_status'
            )
            ->where('orders.is_deleted', 0);
        if ($year != null) {
            $yearTime = substr($startTime, 0, -6);
            if ($yearTime != false) {
                $select->where('orders.is_deleted', 0)
                    ->whereYear('orders.created_at', $yearTime);
            } else {
                $select->where('orders.is_deleted', 0)
                    ->whereYear('orders.created_at', $year);
            }
            if ($startTime != null) {
                $select->whereBetween('orders.created_at',
                    [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
            }
        } else {
            if ($startTime != null) {
                $select->whereBetween('orders.created_at',
                    [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
            }
        }
        if (Auth::user()->is_admin != 1) {
            $select->where('orders.branch_id', Auth::user()->branch_id);
        }
        return $select->get();
    }

    //Lấy giá trị theo năm, cột và giá trị cột truyền vào. Lấy tiền thanh toán trong receipt.
    public function fetchValueByParameter3($year, $startTime, $endTime, $field, $fieldValue)
    {
        $select = $this->leftJoin('receipts', 'receipts.order_id', '=',
            'orders.order_id')
            ->select(
                'orders.order_id',
                'orders.order_code',
                'orders.customer_id',
                'orders.branch_id',
                'orders.total',
                'orders.discount',
                'receipts.amount_paid as amount',
                'orders.tranport_charge',
                'orders.created_by',
                'orders.updated_by',
                'orders.created_at',
                'orders.updated_at',
                'orders.process_status',
                'orders.order_description',
                'orders.payment_method_id',
                'orders.order_source_id',
                'orders.transport_id',
                'orders.voucher_code',
                'orders.is_deleted',
                'receipts.amount as total_amount',
                'receipts.status as  receipts_status'
            )
            ->where('orders.is_deleted', 0);
        if ($startTime == null && $endTime == null) {
            $select ->whereYear('orders.created_at', $year)
                ->where($field, $fieldValue)->where('orders.is_deleted', 0);
        } else {
            $select ->whereBetween('orders.created_at', [$startTime . " 00:00:00", $endTime . " 23:59:59"])
                ->where($field, $fieldValue)->where('orders.is_deleted', 0);
        }
        return $select->get();
    }

    /**
     * Danh sách KH từng sử dụng/ chưa sử dụng dịch vụ.
     * @param $arrService
     * @param $where
     * @return mixed
     */
    public function getCustomerUseService($arrService, $where, $type)
    {
        $select = $this->select('orders.customer_id', 'orders.order_id')
            ->leftJoin('customers','customers.customer_id', '=', 'orders.customer_id')
            ->join('order_details','order_details.order_id', '=', 'orders.order_id')
            ->where('customers.is_deleted', 0)
            ->where('customers.is_actived', 1)
            ->where('orders.process_status', 'paysuccess')
            ->where('order_details.object_type', $type);
        if ($where == 'whereIn') {
            $select->whereIn('order_details.object_id', $arrService);
        } elseif ($where == 'whereNotIn') {
            $select->whereNotIn('order_details.object_id', $arrService);
        }
        return $select->distinct('orders.customer_id')->get();
    }

    /**
     * chi tiết nhà đầu tư
     * Lịch sử hoạt động
     * @param array $filter
     * @return
     */
    public function getListCore(&$filter)
    {
        $select = $this->select(
            $this->table.'.order_code',
            'products.product_name_vi',
            $this->table.'.price_standard',
            $this->table.'.quantity',
            $this->table.'.total',
            $this->table.'.process_status'

        )   ->leftJoin('products', 'products.product_id', '=', $this->table.'.product_id')
            ->orderBy('orders.created_at', 'desc')
            ->where('orders.is_deleted', 0)
            ->groupBy('orders.order_id');

        return $select;
    }

    /**
     * Tất cả order theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByCondition($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            DB::raw('YEAR(orders.created_at) as year'),
            DB::raw('MONTH(orders.created_at) as month')
        )
        ->join(
            'products',
            'products.product_id',
            $this->table . '.product_id'
        )
            ->join(
            'product_categories',
            'product_categories.product_category_id',
            'products.product_category_id'
        );
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        if (isset($params['process_status'])) {
            $select->where($this->table . '.process_status', $params['process_status']);
        }
        if (isset($params['type'])) {
            $select->where('products.product_category_id', $params['type']);
        }
        if (isset($params['year'])) {
            $select->where(DB::raw('YEAR(orders.created_at)'), '=', $params['year']);
        }
        $select->where('orders.is_deleted', self::IS_NOT_DELETED);
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    public function getAllByMonth($customer_id,$month){
        $oSelect = $this
            ->join('products','products.product_id','orders.product_id')
            ->where('orders.customer_id',$customer_id)
            ->where(DB::raw("(DATE_FORMAT(orders.payment_date,'%Y-%m') )"),$month)
            ->where('orders.process_status','paysuccess')
            ->select('orders.*','products.product_category_id')
            ->get();
        return $oSelect;
    }

    public function getListContractExtend() {
        $oSelect = $this
            ->where('process_status','<>','ordercancle')
            ->whereNotNull('contract_code_extend')
            ->where('is_extend',1)
            ->get();
        return $oSelect;
    }

    /**
     * Kiểm tra hợp đồng đã gia hạn chưa
     *
     * @param $contractCode
     * @param $customerId
     * @return mixed
     */
    public function checkExtendContract($contractCode)
    {
        return $this
            ->select(
                "order_id",
                "order_code",
                "customer_id",
                "is_extend",
                "contract_code_extend"
            )
            ->where("contract_code_extend", $contractCode)
            ->where("is_deleted", self::NOT_DELETED)
            ->where("process_status", "!=", self::CANCEL)
            ->first();
    }

    public function getLastOrder(){
        $oSelect = $this->select("{$this->table}.order_code")->latest("created_at")->first();

        return $oSelect;
    }

    public function getOrderByFilter($arrProcessStatus){
        $oSelect = $this->whereIn('process_status',$arrProcessStatus)->get();
        return $oSelect;
    }
}