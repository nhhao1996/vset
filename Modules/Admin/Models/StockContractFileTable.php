<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockContractFileTable extends Model
{
    use ListTableTrait;
    protected $table = 'stock_contract_file';
    protected $primaryKey = 'stock_contract_file_id';
    protected $fillable = [
        "stock_contract_file_id"
        ,"stock_contract_id"
        ,"image_file"
        ,"position_file"
        ,"created_by"
        ,"updated_by"
        ,"created_at"
        ,"updated_at"
    ];

    /**
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter=[]){
        $select = $this->select();
        if(isset($filter["stock_contract_id"])){
            $select->where('stock_contract_id', $filter['stock_contract_id']);
        }
        return $select;

    }
    /**
     * @param array $filter
     * @return mixed
     */
    public function add($data=[]){
        return $this->insert($data);
    }
    /**
     * @param array $filter
     * @return mixed
     */
    public function getLastPositionStockContractFile($filter){

        return $this
            ->select()
            ->where($filter)
            ->orderBy('stock_contract_file_id','DESC')
            ->first()['position_file'];

    }

}