<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class GroupStaffMapCustomerTable extends Model
{
    use ListTableTrait;
    protected $table = "group_staff_map_customer";
    protected $primaryKey = "group_staff_map_customer_id";
    public $timestamps = false;

    protected $fillable = [
        'group_staff_map_customer_id',
        'group_staff_id',
        'obj_id',
        'created_at',
        'created_by',
    ];

    /**
     * Thêm 1 lần nhiều record
     * @param $data
     * @return mixed
     */
    public function addInsert($data)
    {
        $this->insert($data);
    }

    public function remove($groupId)
    {
        $this->where('group_staff_id', $groupId)->delete();
    }

    public function getByGroup($groupId)
    {
        $select = $this->where('group_staff_id', $groupId)->get();
        return $select;
    }

    public function getListCustomer($group_staff_id){
        $oSelect = $this->where('group_staff_id',$group_staff_id)->get();
        return $oSelect;
    }

    public function checkDelete($id){
        return $oSelect = $this
            ->join('group_staff','group_staff.group_staff_id',$this->table.'.group_staff_id')
            ->where($this->table.'.obj_id',$id)
            ->where('group_staff.is_active',1)
            ->where('group_staff.type','group')
            ->get();

    }

    public function getListCustomerByRequest($id,$type) {
        $oSelect = $this
            ->join('group_staff','group_staff.group_staff_id',$this->table.'.group_staff_id')
            ->where($this->table.'.group_staff_id','<>',$id)
            ->where('group_staff.type',$type)
            ->get()->toArray();
        return $oSelect;

    }

    public function deleteStaff($groupStaffId){
        $oSelect = $this->where('group_staff_id',$groupStaffId)->delete();
        return $oSelect;
    }
}