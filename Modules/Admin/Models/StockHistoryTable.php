<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
use DB;

class StockHistoryTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_history_divide";
    protected $primaryKey = "stock_history_divide_id";

    protected $fillable = [
        "stock_history_divide_id",
        "stock_id",
        "payment_method_id",
        "date_publish",
        "year",
        "stock_money_rate",
        "stock_bonus_before",
        "stock_bonus_after",
        "is_active",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by",
        
    ];
      /**
     * @param array $filter
     * @return mixed
     */
    public function getList(&$filter = [])
    {
       
        $select = $this->select(
            "{$this->table}.stock_history_divide_id"
            ,"{$this->table}.stock_id"
            ,"{$this->table}.payment_method_id"
            ,"{$this->table}.date_publish"
//            ,DB::raw("DATE_FORMAT({$this->table}.date_publish, '%d-%m-%Y') as date_publish")
            ,"{$this->table}.year"
            ,"{$this->table}.stock_money_rate"
            ,"{$this->table}.stock_bonus_before"
            ,"{$this->table}.stock_bonus_after"
            ,"{$this->table}.is_active"
            ,"{$this->table}.created_at"
            ,"{$this->table}.created_by"
            ,"{$this->table}.updated_at"
            ,"{$this->table}.updated_by"
           
            )
                    ->orderBy("date_publish",'DESC');
        if(isset($filter['year'])){
            $select->where("{$this->table}.year",$filter['year']);
        }
        return $select->get();

    }
    public function add($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }
    public function edit($id, $data) {
        return $this->where($this->primaryKey, $id)->first()->update($data);
    }

}
