<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/2/2018
 * Time: 12:06 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
//VSet
class ProductTable extends Model
{
    use ListTableTrait;
    protected $table = 'products';
    protected $primaryKey = 'product_id';
    protected $fillable = [
        'product_id',
        'product_code',
        'product_name_vi',
        'product_short_name_vi',
        'description_vi',
        'product_name_en',
        'product_short_name_en',
        'description_en',
        'product_avatar_vi',
        'product_avatar_mobile_vi',
        'product_image_detail_vi',
        'product_image_detail_mobile_vi',
        'product_avatar_en',
        'product_avatar_mobile_en',
        'product_image_detail_en',
        'product_image_detail_mobile_en',
        'unit_id',
        'price_standard',
        'interest_rate_standard',
        'withdraw_fee_rate_before',
        'withdraw_fee_rate_ok',
        'withdraw_fee_interest_rate',
        'is_sales',
        'type',
        'published_at',
        'published_by',
        'product_category_id',
        'product_model_id',
        'cost',
        'is_promo',
        'type_manager',
        'count_version',
        'is_inventory_warning',
        'inventory_warning',
        'description',
        'staff_commission_value',
        'type_staff_commission',
        'refer_commission_value',
        'type_refer_commission',
        'withdraw_min_amount',
        'withdraw_min_time',
        'withdraw_min_bonus',
        'min_allow_sale',
        'month_extend',
        'supplier_id',
        'is_deleted',
        'is_actived',
        'is_all_branch',
        'slug',
        'type_app',
        'percent_sale',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
        'bonus_rate'
    ];

    const IS_ACTIVE = 1;
    const NOT_DELETED = 0;

    public function _getList(&$filter = [])
    {
       // dd($filter);
        $select = $this
            ->select(
                'products.*',
                'product_categories.category_name_vi'
            )
            ->leftJoin('product_categories','product_categories.product_category_id','products.product_category_id')
            ->orderBy('products.product_id','DESC')
            ->where('products.is_deleted',self::NOT_DELETED);
        if (isset($filter['search_keyword']) && $filter['search_keyword'] != "") {
            $search = $filter['search_keyword'];
            $select->where(function ($query) use ($search) {
                $query->where('products.product_name_en', 'like', '%' . $search . '%')
                    ->orWhere('products.product_name_vi', 'like', '%' . $search . '%')
                    ->orWhere('products.product_code', 'like', '%' . $search . '%');
            });
        }

        if (isset($filter['product_category_id'])) {
            $select = $select->where('product_categories.product_category_id',$filter['product_category_id']);
            unset($filter['product_category_id']);
        }

        if (isset($filter['startTime']) != "" && isset($filter['endTime'])) {
            $select->whereBetween('products.created_at', [$filter['startTime']. ' 00:00:00', $filter['endTime']. ' 23:59:59']);
            unset($filter['startTime']);
            unset($filter['endTime']);
        }
        if (isset($filter['type_bonds'])) {
            $select = $select->where('product_categories.product_category_id',$filter['type_bonds']);
            unset($filter['type_bonds']);
        }
        return $select;
    }

    public function deleteProduct($id) {
//        $oSelect = $this->where('product_id',$id)->delete();
        $oSelect = $this->where('product_id',$id)->update(['is_actived' => 0,'is_deleted' => 1]);
    }

    public function createProduct($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function getDetail($id){
        $oSelect = $this
            ->leftJoin('product_categories','product_categories.product_category_id','products.product_category_id')
            ->where('products.product_id',$id)
            ->select('products.*','product_categories.category_name_vi')
            ->first();
        return $oSelect;
    }

    public function editProduct($id,$data) {
        $oSelect = $this->where('product_id',$id)->update($data);
        //dd($oSelect);
        return $oSelect;
    }

    public function getListAllByCategory($category)
    {
        $oSelect = $this->where('product_category_id',$category)->get();
        return $oSelect;
    }

    public function getCheckProductById($productId)
    {
        return $this
            ->select(
                "product_id",
                "product_name_vi",
                "product_category_id",
                "extended_commission_value",
                "price_standard",
                "interest_rate_standard",
                "bonus_extend"
            )
            ->where("product_id", $productId)
            ->where("is_actived", self::IS_ACTIVE)
            ->where("is_deleted", self::NOT_DELETED)
            ->first();
    }
}