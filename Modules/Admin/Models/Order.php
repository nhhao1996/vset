<?php

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class Order extends Model
{
    use ListTableTrait;

    protected $table = "orders";
    protected $primaryKey = "order_id";
    protected $fillable = ['order_id', 'order_code', 'customer_id','product_id', 'quantity', 'price_standard',
        'interest_rate_standard', 'term_time_type', 'investment_time_id', 'withdraw_interest_time_id', 'interest_rate',
        'commission_rate', 'month_interest', 'total_interest', 'bonus_rate', 'total', 'bonus', 'total_amount', 'payment_method_id',
        'process_status', 'payment_date', 'customer_name', 'customer_phone', 'customer_email', 'customer_residence_address',
        'customer_ic_no', 'refer_id', 'commission', 'order_description', 'is_deleted', 'created_by' ,'updated_by' ,'created_at' ,'updated_at',
        'branch_id', 'tranport_charge', 'discount', 'customer_description', 'order_source_id', 'transport_id', 'voucher_code' ,'discount_member',
        'is_apply', 'total_saving' , 'total_tax','is_extend','contract_code_extend','reason_vi','reason_en'
    ];

    protected  function _getList(&$filter=[])
    {
        $oSelect = $this
            ->leftJoin("customers as customers","customers.customer_id","=","orders.customer_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","orders.order_source_id")
            ->leftJoin("products as productbonds","productbonds.product_id","=","orders.product_id")
            ->leftJoin("staffs","staffs.staff_id","=","orders.created_by")
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","orders.payment_method_id")
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->leftJoin("district","district.districtid","=","customers.residence_district_id")
            ->leftJoin("province","province.provinceid","=","customers.residence_province_id")
            ->select(
                'order_code',
                "{$this->table}.order_id",
                "{$this->table}.contract_code_extend",
                'customers.full_name as customer_name',
                'total',
                'discount',
                'tranport_charge',
                'productbonds.product_name_vi as product_name_vi',
                'productbonds.product_name_en as product_name_en',
                'productbonds.product_code',
                'productbonds.product_category_id',
                'process_status',
                'quantity',
                'customer_phone',
                'orders.created_at as created_at',
                'order_source_name',
                'staffs.full_name',
                'district.name as district_name',
                'district.type as district_type',
                'province.name as province_name',
                'province.type as province_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'paymenttable.payment_method_name_vi',
            DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không Kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác minh'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Đơn hàng bị hủy'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status"),
            DB::raw("(CASE
                    WHEN  productbonds.product_category_id = '1' THEN 'Hợp tác đầu tư'
                    WHEN  productbonds.product_category_id = '2' THEN 'Tiết kiệm'
                    ELSE  ''
                    END
                ) as type_bonds")
            );
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $oSelect->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('orders.customer_phone', 'like', '%' . $search . '%')
                    ->orWhere('orders.order_code', 'like', '%' . $search . '%');
            });
            unset($filter["search"]);
        }
        if (isset($filter['process_status']) && $filter['process_status'] != "") {
            $oSelect->where('orders.process_status','=', $filter['process_status']);
            unset($filter["process_status"]);
        }
        if (isset($filter['type_bonds']) && $filter['type_bonds'] != "") {
            $oSelect->where('productbonds.product_category_id','=', $filter['type_bonds']);
            unset($filter["type_bonds"]);
        }
        if (isset($filter['contract_code_extend'])) {
            $oSelect->where('orders.contract_code_extend', 'like', '%' . $filter['contract_code_extend'] . '%');
            unset($filter["contract_code_extend"]);
        }
        if(isset($filter["created_at"]) && $filter["created_at"] != ""){
            $arr_filter = explode(" - ",$filter["created_at"]);
            $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
            $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
            $oSelect->whereBetween('orders.created_at', [$from , $to]);
        }

//        if (isset($filter['type_extend'])) {
//            $oSelect->where('orders.is_extend',$filter['type_extend']);
//            unset($filter["type_extend"]);
//        }

        if (isset($filter['is_extend'])) {
            $oSelect->where('orders.is_extend',$filter['is_extend']);
            unset($filter["type_extend"]);
        }


        unset($filter["created_at"]);
        unset($filter["is_extend"]);
        //dd($oSelect->get());
        return $oSelect->orderBy('orders.order_id','DESC');
    }

    public function add(array $data){
        return self::create($data);
    }

    public function getById($id)
    {
        $select = $this
            ->select(
                'orders.*',
                'refer.full_name as refer_name',
                'refer.customer_code as refer_customer_code',
                'staffs_create_name.full_name as staffs_create_name',
                'staffs_update_name.full_name as staffs_update_name',
                'branches.branch_name',
                'order_sources.order_source_name',
                'productbonds.product_name_vi as product_name_vi',
                'productbonds.product_short_name_vi as product_short_name_vi',
                'productbonds.description_vi as description_vi',
                'productbonds.product_name_en as product_name_en',
                'productbonds.product_short_name_en as product_short_name_en',
                'productbonds.description_en as description_en',
                'productbonds.product_avatar_vi as product_avatar',
                'productbonds.product_image_detail_vi as product_image_detail',
                'productbonds.product_category_id',
                'paymenttable.payment_method_name_vi',
                'productbonds.product_code',
                'investment_time.investment_time_month',
                'withdraw_interest_time.withdraw_interest_month',
                'customers.full_name as full_name_customer',
                'customers.ic_no as ic_no_customer',
                'customers.email as email_customer',
                'customers.phone2 as phone2_customer',
                'customers.phone as phone_customer',
                'customers.ic_no as ic_no_customer',
                'customers.residence_address as residence_address_customer',
                'district.name as district_name',
                'district.type as district_type',
                'province.name as province_name',
                'province.type as province_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'customers.residence_address',
                $this->table.'.process_status',
                'extend_contract.customer_contract_id as extend_customer_contract_id',
                'extend_contract.staff_id as extend_staff_id',
                DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác nhận'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Không xác nhận'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status"),
                DB::raw("(CASE
                    WHEN  productbonds.product_category_id = '1' THEN 'Hợp tác đầu tư'
                    WHEN  productbonds.product_category_id = '2' THEN 'Tiết kiệm'
                    ELSE  ''
                    END
                ) as type_bonds")
            )
            ->leftJoin("customer_contract as extend_contract","extend_contract.customer_contract_code","=","orders.contract_code_extend")
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","orders.created_by")
            ->leftJoin("staffs as staffs_update_name","staffs_update_name.staff_id","=","orders.updated_by")
            ->leftJoin("branches","branches.branch_id","=","orders.branch_id")
            ->leftJoin("products as productbonds","productbonds.product_id","=","orders.product_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","orders.order_source_id")
            ->leftJoin("customers as refer","refer.customer_id","=","orders.refer_id")
            ->leftJoin("customers","customers.customer_id","=","orders.customer_id")
            ->leftJoin("district","district.districtid","=","customers.residence_district_id")
            ->leftJoin("province","province.provinceid","=","customers.residence_province_id")
            ->leftJoin("investment_time","investment_time.investment_time_id","=","orders.investment_time_id")
            ->leftJoin("withdraw_interest_time","withdraw_interest_time.withdraw_interest_time_id","=","orders.withdraw_interest_time_id")
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","orders.payment_method_id")
            ->where('orders.order_id',$id)
            ->first();
            return $select;
    }

    // change status oder

    public function edit(array $data)
    {
        return $this->where("{$this->table}.order_id", $data['order_id'])
            ->update(
                [
                    'process_status' => $data['process_status'],
                    'staff_id' => isset($data['staff_id']) && $data['staff_id'] != null ? $data['staff_id'] : null
                ]
            );
    }

    public function getDetail($id) {
        $oSelect = $this
            ->leftJoin('customers','customers.customer_id','orders.customer_id')
            ->leftJoin('products','products.product_id','orders.product_id')
            ->where('order_id',$id)
            ->select('orders.*','customers.full_name','products.product_category_id')
            ->first();
        return $oSelect;

    }

    public function updateOrder($id,$data) {
        $oSelect = $this->where('order_id',$id)->update($data);
        return $oSelect;
    }

    public function checkContractExtend($customer_contract_code){
        $oSelect = $this
            ->where('contract_code_extend',$customer_contract_code)
            ->where('process_status','paysuccess')
            ->first();
        return $oSelect;
    }

}
