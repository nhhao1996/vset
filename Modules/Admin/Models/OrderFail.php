<?php

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class OrderFail extends Model
{
    use ListTableTrait;

    protected $table = "orders_fail";
    protected $primaryKey = "order_id";
    protected $fillable = ['order_id', 'order_code', 'customer_id','product_id', 'quantity', 'price_standard',
        'interest_rate_standard', 'term_time_type', 'investment_time_id', 'withdraw_interest_time_id', 'interest_rate',
        'commission_rate', 'month_interest', 'total_interest', 'bonus_rate', 'total', 'bonus', 'total_amount', 'payment_method_id',
        'process_status', 'payment_date', 'customer_name', 'customer_phone', 'customer_email', 'customer_residence_address',
        'customer_ic_no', 'refer_id', 'commission', 'order_description', 'is_deleted', 'created_by' ,'updated_by' ,'created_at' ,'updated_at',
        'branch_id', 'tranport_charge', 'discount', 'customer_description', 'order_source_id', 'transport_id', 'voucher_code' ,'discount_member',
        'is_apply', 'total_saving' , 'total_tax','is_extend','contract_code_extend'
    ];

    protected  function _getList(&$filter=[])
    {
        $oSelect = $this
            ->join("customers as customers","customers.customer_id","=","orders_fail.customer_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","orders_fail.order_source_id")
            ->leftJoin("products as productbonds","productbonds.product_id","=","orders_fail.product_id")
            ->leftJoin("staffs","staffs.staff_id","=","orders_fail.created_by")
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","orders_fail.payment_method_id")
            ->select('order_code',
                "{$this->table}.order_id",
                'customers.full_name as customer_name',
                'total',
                'discount',
                'tranport_charge',
                'productbonds.product_name_vi as product_name_vi',
                'productbonds.product_name_en as product_name_en',
                'productbonds.product_code',
                'productbonds.product_category_id',
                'process_status',
                'quantity',
                'customer_phone',
                'orders_fail.created_at as created_at',
                'order_source_name',
                'staffs.full_name',
                'paymenttable.payment_method_name_vi',
            DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không Kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác minh'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Đơn hàng bị hủy'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status"),
            DB::raw("(CASE
                    WHEN  productbonds.product_category_id = '1' THEN 'Hợp tác đầu tư'
                    WHEN  productbonds.product_category_id = '2' THEN 'Tiết kiệm'
                    ELSE  ''
                    END
                ) as type_bonds")
            );
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $oSelect->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('orders_fail.customer_phone', 'like', '%' . $search . '%')
                    ->orWhere('orders_fail.order_code', 'like', '%' . $search . '%');
            });
            unset($filter["search"]);
        }
        if (isset($filter['process_status']) && $filter['process_status'] != "") {
            $oSelect->where('orders_fail.process_status','=', $filter['process_status']);
            unset($filter["process_status"]);
        }
        if (isset($filter['type_bonds']) && $filter['type_bonds'] != "") {
            $oSelect->where('productbonds.product_category_id','=', $filter['type_bonds']);
            unset($filter["type_bonds"]);
        }
        if(isset($filter["created_at"]) && $filter["created_at"] != ""){
            $arr_filter = explode(" - ",$filter["created_at"]);
            $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
            $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
            $oSelect->whereBetween('orders_fail.created_at', [$from , $to]);
        }

        unset($filter["created_at"]);
        //dd($oSelect->get());
        return $oSelect->orderBy('orders_fail.order_id','DESC');
    }

    public function getListExport($filter=[])
    {
        $oSelect = $this
            ->leftJoin("customers as customers","customers.customer_id","=","orders_fail.customer_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","orders_fail.order_source_id")
            ->leftJoin("products as productbonds","productbonds.product_id","=","orders_fail.product_id")
            ->leftJoin("staffs","staffs.staff_id","=","orders_fail.created_by")
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","orders_fail.payment_method_id")
            ->select('order_code',
                'customers.full_name as customer_name',
                'customer_phone',
                'quantity',
                'total',
                DB::raw("(CASE
                    WHEN  productbonds.product_category_id = '1' THEN 'Hợp tác đầu tư'
                    WHEN  productbonds.product_category_id = '2' THEN 'Đầu tư Tiết kiệm'
                    ELSE  ''
                    END
                ) as type_bonds"),
                'productbonds.product_name_vi as product_name_vi',
                'productbonds.product_code',
                'paymenttable.payment_method_name_vi',
                'orders_fail.created_at as created_at'
            );
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $oSelect->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('orders_fail.customer_phone', 'like', '%' . $search . '%')
                    ->orWhere('orders_fail.order_code', 'like', '%' . $search . '%');
            });
            unset($filter["search"]);
        }
        if (isset($filter['process_status']) && $filter['process_status'] != "") {
            $oSelect->where('orders_fail.process_status','=', $filter['process_status']);
            unset($filter["process_status"]);
        }
        if (isset($filter['type_bonds']) && $filter['type_bonds'] != "") {
            $oSelect->where('productbonds.product_category_id','=', $filter['type_bonds']);
            unset($filter["type_bonds"]);
        }
        if(isset($filter["created_at"]) && $filter["created_at"] != ""){
            $arr_filter = explode(" - ",$filter["created_at"]);
            $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
            $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
            $oSelect->whereBetween('orders_fail.created_at', [$from , $to]);
        }

        unset($filter["created_at"]);
        //dd($oSelect->get());
        return $oSelect->orderBy('orders_fail.order_id','DESC')->get();
    }


    public function getById($id)
    {
        $select = $this
            ->select(
                'orders_fail.*',
                'refer.full_name as refer_name',
                'refer.customer_code as refer_customer_code',
                'staffs_create_name.full_name as staffs_create_name',
                'staffs_update_name.full_name as staffs_update_name',
                'branches.branch_name',
                'order_sources.order_source_name',
                'productbonds.product_name_vi as product_name_vi',
                'productbonds.product_short_name_vi as product_short_name_vi',
                'productbonds.description_vi as description_vi',
                'productbonds.product_name_en as product_name_en',
                'productbonds.product_short_name_en as product_short_name_en',
                'productbonds.description_en as description_en',
                'productbonds.product_avatar_vi as product_avatar',
                'productbonds.product_image_detail_vi as product_image_detail',
                'productbonds.product_category_id',
                'paymenttable.payment_method_name_vi',
                'productbonds.product_code',
                'investment_time.investment_time_month',
                'withdraw_interest_time.withdraw_interest_month',
                'customers.full_name as full_name_customer',
                'customers.ic_no as ic_no_customer',
                'customers.email as email_customer',
                'customers.phone2 as phone2_customer',
                'customers.phone as phone_customer',
                'customers.ic_no as ic_no_customer',
                'customers.residence_address as residence_address_customer',
                'district.name as district_name',
                'district.type as district_type',
                'province.name as province_name',
                'province.type as province_type',
                DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác nhận'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Đơn hàng bị hủy'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status"),
                DB::raw("(CASE
                    WHEN  productbonds.product_category_id = '1' THEN 'Hợp tác đầu tư'
                    WHEN  productbonds.product_category_id = '2' THEN 'Tiết kiệm'
                    ELSE  ''
                    END
                ) as type_bonds")
            )
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","orders_fail.created_by")
            ->leftJoin("staffs as staffs_update_name","staffs_update_name.staff_id","=","orders_fail.updated_by")
            ->leftJoin("branches","branches.branch_id","=","orders_fail.branch_id")
            ->leftJoin("products as productbonds","productbonds.product_id","=","orders_fail.product_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","orders_fail.order_source_id")
            ->leftJoin("customers as refer","refer.customer_id","=","orders_fail.refer_id")
            ->leftJoin("customers","customers.customer_id","=","orders_fail.customer_id")
            ->leftJoin("district","district.districtid","=","customers.residence_district_id")
            ->leftJoin("province","province.provinceid","=","customers.residence_province_id")
            ->leftJoin("investment_time","investment_time.investment_time_id","=","orders_fail.investment_time_id")
            ->leftJoin("withdraw_interest_time","withdraw_interest_time.withdraw_interest_time_id","=","orders_fail.withdraw_interest_time_id")
            ->where('orders_fail.order_id',$id)
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","orders_fail.payment_method_id")
            ->first();
            //dd($select);
            return $select;
    }

}
