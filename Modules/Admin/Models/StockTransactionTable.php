<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 4:44 PM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockTransactionTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_transaction";
    protected $primaryKey = "stock_transaction_id";

    protected $fillable = [
        'stock_transaction_id','stock_type','source', 'type', 'customer_id', 'obj_id',
        'quantity_before','quantity','quantity_after','stock_order_id',
        'money_before','money_standard','payment_method_id',
        'date', 'month','year','is_active', 'created_by', 'created_at','updated_at','updated_by'
    ];

    /**
     * Thêm 1 log giao dịch cổ phiếu
     *
     * @param $data
     * @return mixed
     */
    public function createTransaction($data){
        $add = $this->create($data);
        return $add->stock_transaction_id;
    }
    public function getListStockBonus(){
        $yearAgo = Carbon::now()->subYears(1)->format('Y-m-d');
        $data = $this->select(
            "stock_transaction_id",
            "customer_id",
            "quantity",
            "date",
            "month",
            "year",
            "created_at"
        )
            ->where("stock_type","=","bonus")
            ->where("is_active","=","0")
            ->whereBetween("created_at", [$yearAgo. ' 00:00:00', $yearAgo. ' 23:59:59']);
        return $data->get()->toArray();
    }
    public function updateStatusStockBonus($data,$id){
        return $this->where("stock_transaction_id","=",$id)->update($data);
    }
}