<?php
/**
 * Created by PhpStorm.
 * User: SonVeratti
 * Date: 3/17/2018
 * Time: 1:26 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class CheckVersionTable extends Model
{
    protected $table = 'check_version';
    protected $primaryKey = 'id';

    public function getListVersion(){
        $oSelect = $this->get();
        return $oSelect;
    }
}
//