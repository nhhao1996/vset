<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 12/5/2018
 * Time: 9:19 AM
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class CustomerServiceCardTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_service_cards';
    protected $primaryKey = "customer_service_card_id";
    protected $fillable = [
        'customer_service_card_id', 'customer_id', 'card_code', 'service_card_id', 'actived_date', 'expired_date',
        'number_using', 'count_using', 'money', 'created_by', 'updated_by', 'created_at', 'updated_at', 'is_actived',
        'branch_id', 'is_deleted', 'note'
    ];

    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->customer_service_card_id;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItem($id)
    {
        $ds = $this->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('services', 'services.service_id', '=', 'service_cards.service_id')
            ->leftJoin('order_details','order_details.object_code','=','customer_service_cards.card_code')
            ->select('customer_service_cards.customer_service_card_id as customer_service_card_id',
                'customer_service_cards.customer_id as customer_id',
                'customer_service_cards.card_code as card_code',
                'customer_service_cards.is_actived as is_actived',
                'customer_service_cards.service_card_id as service_card_id',
                'customer_service_cards.actived_date as actived_date',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.count_using as count_using',
                'customer_service_cards.money as money',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note',
                'service_cards.name as card_name',
                'service_cards.service_card_type as service_card_type',
                'service_cards.date_using as date_using',
                'services.service_id as service_id',
                'order_details.price')
            ->where('customer_service_cards.customer_id', $id)
            ->get();
        return $ds;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function searchCard($code)
    {
        $ds = $this->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->select('customer_service_cards.customer_service_card_id as customer_service_card_id',
                'customer_service_cards.customer_id as customer_id',
                'customer_service_cards.card_code as card_code',
                'customer_service_cards.service_card_id as service_card_id',
                'customer_service_cards.actived_date as actived_date',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.count_using as count_using',
                'customer_service_cards.money as money',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note',
                'service_cards.name as card_name',
                'service_cards.service_card_type as service_card_type',
                'service_cards.service_is_all as service_is_all',
                'service_cards.service_id as service_id')
            ->where('customer_service_cards.card_code', $code)->first();
        return $ds;
    }

    /**
     * @param $code
     * @param $id
     * @return mixed
     */
    public function searchCardReceipt($code, $id)
    {
        $ds = $this->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('services', 'services.service_id', '=', 'service_cards.service_id')
            ->select('customer_service_cards.customer_service_card_id as customer_service_card_id',
                'customer_service_cards.customer_id as customer_id',
                'customer_service_cards.card_code as card_code',
                'customer_service_cards.service_card_id as service_card_id',
                'customer_service_cards.actived_date as actived_date',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.count_using as count_using',
                'customer_service_cards.money as money',
                'customer_service_cards.is_actived as is_actived',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note',
                'service_cards.name as card_name',
                'service_cards.service_card_type as service_card_type',
                'service_cards.service_is_all as service_is_all',
                'service_cards.service_id as service_id',
                'services.price_standard as price_standard')
            ->where('customer_service_cards.card_code', $code)
            ->where('customer_service_cards.customer_id', $id)->first();
        return $ds;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCodeOrder($id)
    {
        $ds = $this->select('customer_id', 'card_code', 'service_card_id')
            ->where('service_card_id', $id)->get();
        return $ds;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItemCard($id)
    {
        $ds = $this->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('services', 'services.service_id', '=', 'service_cards.service_id')
            ->select('customer_service_cards.card_code as card_code',
                'customer_service_cards.customer_id as customer_id',
                'customer_service_cards.money as money',
                'customer_service_cards.is_actived as is_actived',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.count_using as count_using',
                'customer_service_cards.actived_date as actived_date',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note',
                'service_cards.name as name',
                'service_cards.service_card_type as service_card_type',
                'service_cards.service_is_all as service_is_all',
                'service_cards.service_id as service_id',
                'service_cards.date_using as date_using',
                'service_cards.number_using as number_using_sv',
                'services.price_standard as price_standard')
            ->where('customer_service_cards.customer_service_card_id', $id)->first();
        return $ds;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->where('customer_service_card_id', $id)->update($data);
    }

    /**
     * @param $code
     * @param $branch
     * @return mixed
     * Tìm kiếm thẻ dịch vụ active
     */
    public function searchActiveCard($code, $branch)
    {
        $ds = $this
            ->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('services', 'services.service_id', '=', 'service_cards.service_id')
            ->leftJoin('service_card_list', 'service_card_list.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('branches', 'branches.branch_id', '=', 'service_card_list.branch_id')
            ->select('customer_service_cards.card_code as card_code',
                'customer_service_cards.customer_service_card_id as customer_service_card_id',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note',
                'service_cards.name as name_code',
                'service_cards.service_card_type as card_type',
                'service_cards.money as money',
                'services.service_name as name_sv',
                'branches.branch_name as branch_name',
                'service_cards.date_using as date_using')
//            ->where('customer_service_cards.customer_id','=',1)
            ->where('customer_service_cards.is_actived', 0)
            ->where('customer_service_cards.card_code', $code)
            ->where('branches.branch_id', $branch)->first();
        return $ds;
    }

    /**
     * @param array $data
     * @param $code
     * Cập nhật bằng code
     */
    public function editByCode(array $data, $code)
    {
        return $this->where('card_code', $code)->update($data);
    }


    /**
     * @param $id
     * @param $branch
     * @return mixed
     * Load danh sách thẻ dịch vụ theo customer_id
     */
    public function loadCardMember($id, $branch)
    {
        $ds = $this
            ->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('services', 'services.service_id', '=', 'service_cards.service_id')
            ->leftJoin('branches', 'branches.branch_id', '=', 'customer_service_cards.branch_id')
            ->select('customer_service_cards.card_code as card_code',
                'customer_service_cards.customer_service_card_id as customer_service_card_id',
                'service_cards.name as name_code',
                'service_cards.service_card_type as card_type',
                'services.service_name as name_sv',
                'branches.branch_name as branch_name',
                'service_cards.image as image',
                'services.service_id as service_id',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.count_using as count_using')
            ->where('customer_service_cards.is_actived', 1)
            ->where('service_card_type', 'service')
            ->where('customer_service_cards.customer_id', $id)
            ->where('branches.branch_id', $branch)->get();
        return $ds;
    }

    public function searchCardMember($search, $id, $branch)
    {
        $ds = $this
            ->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('services', 'services.service_id', '=', 'service_cards.service_id')
            ->leftJoin('branches', 'branches.branch_id', '=', 'customer_service_cards.branch_id')
            ->select('customer_service_cards.card_code as card_code',
                'customer_service_cards.customer_service_card_id as customer_service_card_id',
                'service_cards.name as name_code',
                'service_cards.service_card_type as card_type',
                'services.service_name as name_sv',
                'branches.branch_name as branch_name',
                'service_cards.image as image',
                'services.service_id as service_id',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.count_using as count_using')
//            ->where('customer_service_cards.customer_id','=',1)
            ->where('customer_service_cards.is_actived', 1)
            ->where('service_card_type', 'service')
            ->where(function ($query) use ($search) {
                $query->where('customer_service_cards.card_code', $search)
                    ->orWhere('service_cards.name', 'like', '%' . $search . '%');
            })
            ->where('customer_service_cards.customer_id', $id)
            ->where('branches.branch_id', $branch)
            ->get();
        return $ds;
    }

    //Lấy các thẻ dịch vụ đã sử dụng.
    public function getServiceCardUsed($objectId, array $filter = [])
    {
        $select = $this->leftJoin('order_details', 'order_details.object_code', '=', 'customer_service_cards.card_code')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_service_cards.customer_id')
            ->select(
                'customer_service_cards.card_code as card_code',
                'customers.full_name as full_name',
                'order_details.created_at as day_use',
                'customer_service_cards.expired_date as expired_date'
            )
            ->where('order_details.object_type', 'member_card')
            ->where('customer_service_cards.service_card_id', $objectId)
            ->groupBy('order_details.object_code');
//        $page = (int)($filter['page'] ?? 1);
//        $display = (int)($filter['display'] ?? PAGING_ITEM_PER_PAGE);
//        // search term
//        if (!empty($filter['search_type']) && !empty($filter['search_keyword'])) {
//            $select->where($filter['search_type'], 'like', '%' . $filter['search_keyword'] . '%');
//        }
//        unset($filter['search_type'], $filter['search_keyword'], $filter['page'], $filter['display']);
//
//        // filter list
//        foreach ($filter as $key => $val) {
//            if (trim($val) == '') {
//                continue;
//            }
//
//            $select->where(str_replace('$', '.', $key), $val);
//        }

        return $select->get();
    }

    //Lấy thẻ đã kích hoạt theo mã thẻ
    public function getCardActiveByCode($code, $keyWord, $branch, $staffActived, $startTime, $endTime)
    {
        if ($code != '') {
            $select = $this->where('card_code', $code)->first();
            return $select;
        } else {
            $select = $this->select('customer_service_card_id', 'customer_id', 'card_code', 'service_card_id', 'actived_date', 'expired_date',
                'number_using', 'count_using', 'money', 'created_by', 'updated_by', 'created_at', 'updated_at', 'is_actived',
                'branch_id', 'is_deleted', 'note');
            if ($keyWord != null) {
                $select->where('card_code', 'LIKE', '%' . $keyWord . '%');
            }
            if ($branch != null) {
                $select->where('branch_id', $branch);
            }
            if ($staffActived != null) {
                $select->where('updated_at', $staffActived);
            }
            if ($startTime != null && $endTime != null) {
                $select->whereBetween('actived_date', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
            }
            return $select->get()->toArray();
        }
    }

    public function filterCardSold($cardType, $keyWord, $branch, $staffActived, $startTime, $endTime)
    {
        $select = $this->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->select(
                'customer_service_cards.customer_service_card_id',
                'customer_service_cards.customer_id',
                'customer_service_cards.card_code', 'customer_service_cards.service_card_id',
                'customer_service_cards.actived_date',
                'customer_service_cards.expired_date',
                'customer_service_cards.number_using',
                'customer_service_cards.count_using',
                'customer_service_cards.money',
                'customer_service_cards.created_by',
                'customer_service_cards.updated_by',
                'customer_service_cards.created_at',
                'customer_service_cards.updated_at',
                'customer_service_cards.is_actived',
                'customer_service_cards.branch_id',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note'
            );
        if ($keyWord != null) {
            $select->where('customer_service_cards.card_code', 'LIKE', '%' . $keyWord . '%');
        }
        if ($branch != null) {
            $select->where('customer_service_cards.branch_id', $branch);
        }
        if ($staffActived != null) {
            $select->where('customer_service_cards.updated_by', $staffActived);
        }
        if ($startTime != null && $endTime != null) {
            $select->whereBetween('customer_service_cards.actived_date', [$startTime . " 00:00:00", $endTime . " 23:59:59"]);
        }
        $select->where('service_cards.service_card_type', $cardType);
        return $select->get();

    }

    //Lấy chi tiết của thẻ đã bán
    public function getDetailCardSold($code, array $filter = [])
    {
        $select = $this->leftJoin('customers', 'customers.customer_id', '=', 'customer_service_cards.customer_id')
            ->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('order_details', 'order_details.object_code', '=', 'customer_service_cards.card_code')
            ->leftJoin('orders', 'orders.order_id', '=', 'order_details.order_id')
            ->select(
                'customer_service_cards.card_code as card_code',
                'service_cards.service_card_type as service_card_type',
                'service_cards.number_using as number_using',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.count_using as count_using',
                'customer_service_cards.is_deleted',
                'customer_service_cards.note',
                'customer_service_cards.card_code as card_code',
                'order_details.created_at as day_using',
                'customers.full_name as customer',
                'orders.order_code as order_code',
                'customer_service_cards.is_actived as is_actived',
                DB::raw("SUM(order_details.quantity) as quantity")
//                DB::raw('YEAR(order_details.created_at) year, MONTH(order_details.created_at) month, DAY(order_details.created_at) day')
            )
            ->where('customer_service_cards.card_code', $code)
            ->where('orders.process_status', 'paysuccess')
            ->where('order_details.object_type', 'member_card')
            ->where('service_cards.is_deleted', 0)
            ->where('customers.is_deleted', 0)
            ->groupBy('order_details.created_at');
        $page = (int)($filter['page'] ?? 1);
        $display = (int)($filter['display'] ?? PAGING_ITEM_PER_PAGE);
        return $select->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    //Lấy chi tiết thẻ theo mã.
    public function getCardByCode($code)
    {
        $select = $this->leftJoin('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_service_cards.customer_id')
            ->select(
                'service_cards.service_card_type as service_card_type',
                'customer_service_cards.expired_date as expired_date',
                'customer_service_cards.count_using as count_using',
                'customer_service_cards.number_using as number_using',
                'customer_service_cards.actived_date as actived_date',
                'customers.full_name as customer_name',
                'service_cards.money as money',
                'customer_service_cards.is_actived as is_actived',
                'customer_service_cards.is_deleted as is_deleted',
                'customer_service_cards.note as note'
            )
            ->where('service_cards.is_deleted', 0)
            ->where('customer_service_cards.card_code', $code);
        return $select->first();
    }

    public function memberCardDetail($id,$branch)
    {
        $ds=$this
            ->leftJoin('service_card_list','service_card_list.code','=','customer_service_cards.card_code')
            ->leftJoin('service_cards','service_cards.service_card_id','=','customer_service_cards.service_card_id')
            ->select('service_cards.name',
                'service_card_list.code',
                'service_card_list.price',
                'service_card_list.is_actived')
                ->where('customer_service_cards.customer_id',$id)
                ->where('customer_service_cards.branch_id',$branch)->get();
        return $ds;
    }

    public function getCustomerCardAll($customer_id)
    {
        $ds = $this->join('service_cards', 'service_cards.service_card_id', '=', 'customer_service_cards.service_card_id')
            ->select(
                'service_cards.name as card_name'
            )->where('customer_id', $customer_id);
        return $ds->get()->toArray();
    }
}
