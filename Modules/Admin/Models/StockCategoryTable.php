<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockCategoryTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_category";
    protected $primaryKey = "stock_category_id";
    protected $fillable = [
        'stock_category_id',
        'stock_category_title_vi',
        'stock_category_title_en',
        'stock_category_content_vi',
        'stock_category_content_en',
        'is_active',
        'is_deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    public function _getList(&$filters = []) {
        $oSelect = $this;
        if (isset($filters['stock_category$stock_category_title_vi'])) {
            $oSelect = $oSelect->where('stock_category_title_vi', 'like', '%' . $filters['stock_category$stock_category_title_vi'] . '%');
        }

        if (isset($filters['stock_category$stock_category_title_en'])) {
            $oSelect = $oSelect->where('stock_category_title_en', 'like', '%' . $filters['stock_category$stock_category_title_en'] . '%');
        }

        if (isset($filters['stock_category$is_active'])) {
            $oSelect = $oSelect->where('is_active', $filters['stock_category$is_active']);
        }

        unset($filters['stock_category$stock_category_title_vi']);
        unset($filters['stock_category$stock_category_title_en']);
        unset($filters['stock_category$is_active']);
        $oSelect = $oSelect->orderBy('stock_category_id','DESC');
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelect = $this->where('stock_category_id',$id)->first();
        return $oSelect;
    }

    public function updateCategory($id,$data) {
        $oSelect = $this->where('stock_category_id',$id)->update($data);
        return $oSelect;
    }

    public function addCategory($data) {
        $oSelelct = $this->insertGetId($data);
        return $oSelelct;
    }

    public function deleteCategory($id) {
        $oSelect = $this->where('stock_category_id',$id)->delete();
        return $oSelect;
    }

    public function checkName($name,$id = null){
        $oSelect = $this
            ->where($name);

        if ($id != null) {
            $oSelect = $oSelect->where('stock_category_id','<>',$id);
        }

        $oSelect = $oSelect->get();
        return $oSelect;
    }

    public function getAll(){
        $oSelect = $this
            ->where('is_active',1)
            ->where('is_deleted',0)
            ->get();
        return $oSelect;
    }
}