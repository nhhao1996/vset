<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/12/2018
 * Time: 3:24 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class MapProductAttributeTable extends Model
{
    use ListTableTrait;
    protected $table = 'map_product_attributes';
    protected $primaryKey = 'map_product_attribute_id';

    protected $fillable = ['map_product_attribute_id', 'product_attribute_groupd_id', 'product_attribute_id', 'product_id', 'created_at', 'updated_at'];

    protected function _getList()
    {
        return $this->select('map_product_attribute_id', 'product_attribute_groupd_id', 'product_attribute_id', 'product_id', 'created_at', 'updated_at');
    }

    /**
     * Insert map product attribute to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $oStaffDepartment = $this->create($data);
        return $oStaffDepartment->id;
    }

    /**
     * Edit map product attribute to database
     *
     * @param array $data , $id
     * @return number
     */
    public function edit(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);
    }

    /**
     * Remove map product attribute to database
     *
     * @param number $id
     */
    public function remove($id)
    {
        $this->where($this->primaryKey, $id)->delete();
    }

    public function getItem($id)
    {
        return $this->where($this->primaryKey, $id)->first();
    }

    /*
    * Get map product attribute group by product id
    */
    public function getMapProductAttributeGroupByProductId($idProduct)
    {
        $select = $this->leftJoin('product_attribute_groups', 'product_attribute_groups.product_attribute_group_id',
            '=', 'map_product_attributes.product_attribute_groupd_id')
            ->select(
                'map_product_attributes.product_attribute_groupd_id as productAttrGroupId',
                'product_attribute_groups.product_attribute_group_name as productAttrGroupName'
            )->distinct('map_product_attributes.product_attribute_groupd_id')->where('map_product_attributes.product_id', $idProduct)->get();
        return $select;
    }

    /*
     * Get product attribute by product id
     */
    public function getProductAttributeByProductId($idProduct)
    {
        $select = $this->leftJoin('product_attributes', 'product_attributes.product_attribute_id', 'map_product_attributes.product_attribute_id')
            ->select(
                'map_product_attributes.product_attribute_id as productAttributeId',
                'map_product_attributes.product_attribute_groupd_id as productAttributeGroupId',
                'product_attributes.product_attribute_label as productAttributeLabel'
            )
            ->where('map_product_attributes.product_id', $idProduct)->get();
        return $select;
    }

    /*
     * test map product attribute
     */
    public function testMapProductAttributeIsset($idProduct, $attribute)
    {
        return $this->where('product_attribute_id', $attribute)->where('product_id', $idProduct)->first();
    }

    /*
        * get all product attribute by product id
        */
    public function getAllAttrByProductId($product)
    {
        return $this->where('product_id', $product)->get();
    }

    public function deleteMapProductAttrByAttrId($idProduct, $attribute)
    {
        return $this->where('product_attribute_id', $attribute)->where('product_id', $idProduct)->delete();
    }

    public function deleleAllByProductId($idProduct)
    {
        return $this->where('product_id', $idProduct)->delete();
    }

}