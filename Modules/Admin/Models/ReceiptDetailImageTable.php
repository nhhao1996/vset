<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ReceiptDetailImageTable extends Model
{
    use ListTableTrait;
    protected $table = "receipt_detail_image";
    protected $primaryKey = "receipt_detail_image_id";
    protected $fillable = [
        "receipt_detail_image_id",
        "receipt_id",
        "receipt_detail_id",
        "staff_id",
        "image_file",
        "created_by",
        "updated_by",
        "created_at",
        "updated_at",
    ];

    public function createImage($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function getListImage($arr) {
        $oSelect = $this
            ->whereIn('receipt_detail_id',$arr)
            ->get();
        return $oSelect;
    }
}