<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/29/2020
 * Time: 2:19 PM
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class CustomerContractFileTable extends Model
{
    use ListTableTrait;
    protected $table = "customer_contract_file";
    protected $primaryKey = "customer_contract_file_id";
    protected $fillable = ["customer_contract_file_id", "customer_contract_id", "image_file", "position_file", "created_by", "updated_by", "created_at", "updated_at"];

    /**
     * Danh sách hình ảnh hợp đồng
     *
     * @param array $filter
     * @return mixed
     */
    public function _getList($filter = [])
    {
        $ds = $this
            ->select(
                "customer_contract_file_id",
                "image_file",
                "position_file"
            )
            ->where("customer_contract_id", $filter['customer_contract_id'])
            ->orderBy("position_file", "asc");

        return $ds;
    }

    public function getImage($contractId){
        $result = $this->select("customer_contract_file_id","image_file","position_file")
                        ->where("customer_contract_id",$contractId);

        return $result->get();
    }

    public function deleteCustomerFile($customer_contract_id) {
        $result = $this
            ->where("customer_contract_id",$customer_contract_id)
            ->delete();

        return $result;
    }

    public function createContractFile($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }
}