<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class JobsEmailLogTable extends Model
{
    use ListTableTrait;
    protected $table = 'jobs_email_log';
    protected $primaryKey = 'jobs_email_id';

    protected $fillable = [
        'jobs_email_id',
        'obj_id',
        'email_type',
        'email_subject',
        'email_from',
        'email_to',
        'email_cc',
        'email_body',
        'email_params',
        'email_attach',
        'created_at',
        'body_html',
        'is_run',
        'run_at',
    ];

    public $timestamps = false;


    public function _getList(&$filter = [])
    {
        $select = $this
            ->whereNotNull('email_to')
            ->where('email_to','<>','');
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $select = $select->where(function ($query) use ($search) {
                $query->where('email_subject', 'like', '%' . $search . '%')
                    ->orWhere('email_from', 'like', '%' . $search . '%')
                    ->orWhere('email_to', 'like', '%' . $search . '%');
            });
            unset($filter['search']);
        }

        if (isset($filter['is_run']) != "") {
            $select = $select->where('is_run',$filter['is_run']);
            unset($filter['is_run']);
        }
        if (isset($filter['run_at']) != "") {
            $arr_filter = explode(" - ", $filter["run_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select = $select->whereBetween('run_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
            unset($filter['run_at']);
        }

        return $select->orderBy('jobs_email_id','DESC');
    }

    public function addJob($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }
}