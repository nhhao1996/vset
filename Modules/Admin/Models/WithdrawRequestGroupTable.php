<?php

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WithdrawRequestGroupTable extends Model
{
    use ListTableTrait;
    protected $table = "withdraw_request_group";
    protected $primaryKey = "withdraw_request_group_id";

    protected $fillable = [
        'withdraw_request_group_id',
        'withdraw_request_group_type',
        'withdraw_request_group_code',
        'customer_id',
        'withdraw_free_amount',
        'available_balance',
        'available_balance_after',
        'withdraw_request_amount',
        'withdraw_request_year',
        'withdraw_request_month',
        'withdraw_request_day',
        'withdraw_request_time',
        'withdraw_request_status',
        'payment_date_planing',
        'customer_name',
        'customer_phone',
        'customer_email',
        'customer_residence_address',
        'customer_ic_no',
        'bank_id',
        'bank_name',
        'bank_account_no',
        'bank_account_name',
        'bank_account_branch',
        'withdraw_request_goal',
        'object_id',
        'amount_refund',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function _getList(&$filter = [])
    {
        $select = $this
            ->select(
                'withdraw_request_group.*' ,
                'staffs_create_name.full_name as staffs_create_name',
                'customer_contract.customer_contract_code'
            )
            ->leftJoin('withdraw_request','withdraw_request.withdraw_request_group_id','withdraw_request_group.withdraw_request_group_id')
            ->leftJoin('customer_contract','customer_contract.customer_contract_id','withdraw_request.customer_contract_id')
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","withdraw_request_group.created_by");
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $select->where(function ($query) use ($search) {
                $query->where('withdraw_request_group.withdraw_request_group_code', 'like', '%' . $search . '%')
                    ->orWhere('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                    ->orWhere('withdraw_request_group.customer_name', 'like', '%' . $search . '%');
            });
            unset($filter['search']);
        }

        if (isset($filter['withdraw_request_group_type'])) {
            $select = $select
                ->where('withdraw_request_group.object_id',0)
                ->where('withdraw_request_group.withdraw_request_goal','<>','voucher')
                ->whereIn('withdraw_request_group.withdraw_request_group_type', $filter['withdraw_request_group_type']);
            unset($filter['withdraw_request_group_type']);
        } else {
            $select = $select
                ->where('withdraw_request_group.object_id',0)
                ->where('withdraw_request_group.withdraw_request_goal','<>','voucher')
                ->whereIn('withdraw_request_group.withdraw_request_group_type', ['interest','bonus']);
        }

        if (isset($filter['created_at']) != "") {
            $arr_filter = explode(" - ", $filter["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select->whereBetween('withdraw_request_group.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
            unset($filter['created_at']);
        }

        if (isset($filter['withdraw_request_status'])) {
            $select = $select->where('withdraw_request_group.withdraw_request_status',$filter['withdraw_request_status']);
            unset($filter['withdraw_request_status']);
        }

        return $select->orderBy('withdraw_request_group.withdraw_request_group_id','DESC');
    }

    public function getById($id)
    {
        $select = $this
            ->leftJoin('withdraw_request','withdraw_request.withdraw_request_group_id','withdraw_request_group.withdraw_request_group_id')
            ->leftJoin('customer_contract','customer_contract.customer_contract_id','withdraw_request.customer_contract_id')
            ->join('customers', 'customers.customer_id', $this->table.'.customer_id')
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->where('withdraw_request_group.withdraw_request_group_id',$id)
            ->select(
                'withdraw_request_group.*','withdraw_request.customer_contract_id',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'customers.residence_address',
                'customer_contract.customer_contract_code'
            )
            ->first();
        return $select;
    }

    public function sumContractStatus($params = [])
    {
        $select = $this
            ->leftJoin('withdraw_request','withdraw_request.withdraw_request_group_id','withdraw_request_group.withdraw_request_group_id')
            ->select(DB::raw("SUM(withdraw_request_group.withdraw_request_amount) as withdraw_request_amount"))
            ->where('withdraw_request.customer_contract_id', $params['customer_contract_id'])
            ->where($this->table . '.withdraw_request_status', $params['withdraw_request_status']);
        return $select->first();
    }

    public function changeStatus($id,$status) {
        $oSelect = $this->where('withdraw_request_group_id',$id)->update($status);
        return $oSelect;
    }

    public function getListByCustomerId($customer_id) {
        $oSelect = $this->where('customer_id',$customer_id)->get();
        return $oSelect;
    }

    public function getDetailById($id)
    {
        $select = $this
            ->where('withdraw_request_group.withdraw_request_group_id',$id)
            ->select('withdraw_request_group.*')
            ->first();
        return $select;
    }

    public function getListByOrderId($orderId,$type,$status) {
        $oSelect = $this->where('object_id',$orderId)
            ->where('withdraw_request_status',$status)
            ->where('withdraw_request_goal',$type)
            ->get();
        return $oSelect;
    }

    public function getSumWithraw($customer_id,$type,$status) {
        $oSelect = $this
            ->where('customer_id',$customer_id)
            ->where('withdraw_request_group_type',$type)
            ->where('withdraw_request_status',$status)
            ->select(
                DB::raw('SUM(available_balance) as sum_withdraw_request_group')
            )
            ->first();
        return $oSelect;
    }

    public function getWithdrawByType($customerId,$type){
            $oResult = $this->select(
                \DB::raw("SUM({$this->table}.withdraw_request_amount) as withdraw_request_total")
            )
                ->where($this->table.".withdraw_request_group_type",$type)
                ->where($this->table.".customer_id",$customerId)
                ->where($this->table.".withdraw_request_status","done")
                ->get();
            return ($oResult!=null) ? intval($oResult[0]->withdraw_request_total) :0;
    }

    public function getWithdrawByTypeByCustomerId($customer_contract_id,$type){
        $oResult = $this->select(
            \DB::raw("SUM({$this->table}.withdraw_request_amount) as withdraw_request_total")
        )
            ->where($this->table.".withdraw_request_group_type",$type)
            ->where($this->table.".customer_contract_id",$customer_contract_id)
            ->where($this->table.".withdraw_request_status","done")
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->withdraw_request_total) :0;
    }

    public function getWithdrawByGroupCustomerIdObj($customer_contract_id,$type){
        $oResult = $this
            ->join('withdraw_request','withdraw_request.withdraw_request_group_id',$this->table.'.withdraw_request_group_id')
            ->select(
                \DB::raw("SUM({$this->table}.withdraw_request_amount) as withdraw_request_total")
            )
            ->where($this->table.".withdraw_request_group_type",$type)
            ->where("withdraw_request.customer_contract_id",$customer_contract_id)
            ->where($this->table.".withdraw_request_status","done")
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->withdraw_request_total) :0;
    }

    public function getWithdrawByGroupCustomerId($customer_contract_id,$type){
        $oResult = $this
            ->join('withdraw_request','withdraw_request.withdraw_request_group_id',$this->table.'.withdraw_request_group_id')
            ->select(
            \DB::raw("SUM({$this->table}.withdraw_request_amount) as withdraw_request_total")
        )
            ->where($this->table.".withdraw_request_group_type",$type)
            ->whereIn("withdraw_request.customer_contract_id",$customer_contract_id)
            ->where($this->table.".withdraw_request_status","done")
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->withdraw_request_total) :0;
    }

    public function getAllByMonth($customer_id,$month,$type = null) {
        $oSelect = $this
            ->where('customer_id',$customer_id)
            ->where(DB::raw("(DATE_FORMAT(payment_date_planing,'%Y-%m') )"),$month)
            ->where('object_id',0)
            ->where('withdraw_request_status','done');
        if ($type == null) {
            $oSelect = $oSelect->where('withdraw_request_goal','<>','voucher');
        } else {
            $oSelect = $oSelect->where('withdraw_request_goal',$type);
        }
        return $oSelect->get();
    }

    public function getAllByMonthSum($customer_id,$month,$group_type,$type) {
        $oSelect = $this
            ->where('customer_id',$customer_id)
            ->where('withdraw_request_group_type',$group_type)
            ->where('withdraw_request_status','done');
        if ($type == 'before') {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(payment_date_planing,'%Y-%m') )"),'<',$month);
        } else {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(payment_date_planing,'%Y-%m') )"),$month);
        }
        $oSelect = $oSelect->select(DB::raw("SUM(withdraw_request_amount) as total"))->first();
        return $oSelect;
    }

    public function getAllContractByMonthSum($customer_id,$customer_contract_id,$month,$group_type,$type) {
        $oSelect = $this
            ->join('withdraw_request','withdraw_request.withdraw_request_group_id','withdraw_request_group.withdraw_request_group_id')
            ->where('withdraw_request_group.customer_id',$customer_id)
            ->where('withdraw_request_group.withdraw_request_group_type',$group_type)
            ->where('withdraw_request_group.withdraw_request_status','done')
            ->where('withdraw_request.customer_contract_id',$customer_contract_id);
        if ($type == 'before') {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(withdraw_request_group.payment_date_planing,'%Y-%m') )"),'<',$month);
        } else {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(withdraw_request_group.payment_date_planing,'%Y-%m') )"),'<=',$month);
        }
        $oSelect = $oSelect->select(DB::raw("SUM(withdraw_request_group.withdraw_request_amount) as total"))->first();
        return $oSelect;
    }

    public function getAllByDaySum($customer_id,$day,$group_type,$type) {
        $oSelect = $this
            ->where('customer_id',$customer_id)
            ->where('withdraw_request_group_type',$group_type)
            ->where('withdraw_request_status','done');
        if ($type == 'before') {
            $oSelect = $oSelect->whereDate('payment_date_planing','<',$day);
        } else {
            $oSelect = $oSelect->whereDate('payment_date_planing',$day);
        }
        $oSelect = $oSelect->select(DB::raw("SUM(withdraw_request_amount) as total"))->first();
        return $oSelect;
    }
}