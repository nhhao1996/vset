<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class OrderServicesTable extends Model
{
    use ListTableTrait;

    protected $table = 'order_services';
    protected $primaryKey = 'order_service_id';

    protected $fillable = [
        'order_service_id',
        'order_service_code',
        'customer_id',
        'service_voucher_template_id',
        'service_id',
        'quantity',
        'price_standard',
        'total',
        'payment_method_id',
        'process_status',
        'payment_date',
        'customer_name',
        'customer_phone',
        'customer_email',
        'customer_residence_address',
        'customer_ic_no',
        'refer_id',
        'is_deleted',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'tranport_charge',
        'discount',
        'customer_description',
        'order_source_id',
        'transport_id',
        'voucher_code',
        'discount_member',
        'is_apply',
        'total_saving',
        'total_tax',
    ];
    const IS_NOT_DELETED = 0;

    public function _getList(&$filter=[])
    {
        $oSelect = $this
            ->leftJoin("customers as customers","customers.customer_id","=","order_services.customer_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","order_services.order_source_id")
            ->leftJoin("services as servicesbonds","servicesbonds.service_id","=","order_services.service_id")
            ->leftJoin("staffs","staffs.staff_id","=","order_services.created_by")
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","order_services.payment_method_id")
            ->select('order_service_code',
                "{$this->table}.order_service_id",
                'customers.full_name as customer_name',
                'total',
                'discount',
                'tranport_charge',
                'servicesbonds.service_name_vi as service_name',
                'servicesbonds.service_code',
                'servicesbonds.service_category_id',
                'process_status',
                'quantity',
                'customer_phone',
                'order_services.created_at as created_at',
                'order_source_name',
                'staffs.full_name',
                'paymenttable.payment_method_name_vi',
                DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không Kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác minh'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Đơn hàng bị hủy'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status")
            );
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $oSelect->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('order_services.customer_phone', 'like', '%' . $search . '%')
                    ->orWhere('order_services.order_service_code', 'like', '%' . $search . '%');
            });
            unset($filter["search"]);
        }
        if (isset($filter['process_status']) && $filter['process_status'] != "") {
            $oSelect->where('order_services.process_status','=', $filter['process_status']);
            unset($filter["process_status"]);
        }
        if(isset($filter["created_at"]) && $filter["created_at"] != ""){
            $arr_filter = explode(" - ",$filter["created_at"]);
            $from  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 00:00:00');
            $to  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
//            $oSelect->whereBetween('order_services.created_at', [$from , $to]);
            $oSelect
                ->where('order_services.created_at','>=',$from)
                ->where('order_services.created_at','<=',$to);
        }
        unset($filter["created_at"]);
        //dd($oSelect->get());
        return $oSelect->orderBy('order_services.order_service_id','DESC');
    }

    public function getById($id)
    {
        $select = $this
            ->select(
                'order_services.*',
                'refer.full_name as refer_name',
                'staffs_create_name.full_name as staffs_create_name',
                'staffs_update_name.full_name as staffs_update_name',
                'order_sources.order_source_name',
                'services.service_name_vi as service_name',
                'services.description as service_description',
                'services.detail_description as service_detail_description',
                'services.service_avatar',
                'paymenttable.payment_method_name_vi',
                'service_voucher_template.valid_from_date',
                'service_voucher_template.valid_to_date',
                'customers.residence_address as residence_address_customer',
                'district.name as district_name',
                'district.type as district_type',
                'province.name as province_name',
                'province.type as province_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'customers.residence_address',
                DB::raw("(CASE
                    WHEN  {$this->table}.process_status = 'not_call' THEN 'Không kết nối'
                    WHEN  {$this->table}.process_status = 'confirmed' THEN 'Đã xác nhận'
                    WHEN  {$this->table}.process_status = 'ordercomplete' THEN 'Đơn hàng hoàn thành'
                    WHEN  {$this->table}.process_status = 'ordercancle' THEN 'Đơn hàng bị hủy'
                    WHEN  {$this->table}.process_status = 'paysuccess' THEN 'Thanh toán thành công'
                    WHEN  {$this->table}.process_status = 'payfail' THEN 'Thanh toán thất bại'
                    WHEN  {$this->table}.process_status = 'new' THEN 'Mới'
                    WHEN  {$this->table}.process_status = 'pay-half' THEN 'Thanh toán 1 phần'
                    ELSE  0
                    END
                ) as status")
            )
            ->leftJoin("service_voucher_template","service_voucher_template.service_voucher_template_id","=","order_services.service_voucher_template_id")
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","order_services.created_by")
            ->leftJoin("staffs as staffs_update_name","staffs_update_name.staff_id","=","order_services.updated_by")
            ->leftJoin("services","services.service_id","=","order_services.service_id")
            ->leftJoin("order_sources","order_sources.order_source_id","=","order_services.order_source_id")
            ->leftJoin("customers as refer","refer.customer_id","=","order_services.refer_id")
            ->leftJoin("customers","customers.customer_id","=","order_services.customer_id")
            ->leftJoin("district","district.districtid","=","customers.residence_district_id")
            ->leftJoin("province","province.provinceid","=","customers.residence_province_id")
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->where('order_services.order_service_id',$id)
            ->leftJoin("payment_method as paymenttable","paymenttable.payment_method_id","=","order_services.payment_method_id")
            ->first();
        //dd($select);
        return $select;
    }

    public function edit($id,$data) {
        $oSelect = $this->where('order_service_id',$id)->update($data);
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelect = $this
            ->join('services','services.service_id',$this->table.'.service_id')
            ->where('order_service_id',$id)
            ->select(
                $this->table.'.service_id',
                $this->table.'.service_voucher_template_id',
                $this->table.'.order_service_id',
                $this->table.'.quantity',
                $this->table.'.customer_id',
                'services.price_standard'
            )
            ->first();
        return $oSelect;
    }

    /**
     * Tất cả order theo điều kiện
     * @param array $params
     *
     * @return mixed
     */
    public function getByCondition($params = [])
    {
        $select = $this->select(
            $this->table . '.*',
            DB::raw('YEAR(order_services.created_at) as year'),
            DB::raw('MONTH(order_services.created_at) as month')
        );
        //Ngày tạo trong khoảng datetime
        if (isset($params['created_at'])) {
            $select->whereBetween($this->table . '.created_at', $params['created_at']);
        }
        //Trạng thái của order
        if (isset($params['process_status'])) {
            $select->where($this->table . '.process_status', $params['process_status']);
        }
        //Ngày tạo trong năm
        if (isset($params['year'])) {
            $select->where(DB::raw('YEAR(order_services.created_at)'), '=', $params['year']);
        }
        $select->where('is_deleted', self::IS_NOT_DELETED);
        $select->groupBy($this->table . '.' . $this->primaryKey);
        return $select->get();
    }

    public function getAllByMonth($customer_id,$month){
        $oSelect = $this
            ->join('services','services.service_id','order_services.service_id')
            ->where('order_services.customer_id',$customer_id)
            ->where(DB::raw("(DATE_FORMAT(order_services.payment_date,'%Y-%m') )"),$month)
            ->where('order_services.process_status','paysuccess')
            ->select('order_services.*')
            ->get();
        return $oSelect;
    }
}