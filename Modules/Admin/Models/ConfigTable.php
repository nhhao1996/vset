<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/21/2019
 * Time: 11:43 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigTable extends Model
{
    protected $table = "config";
    protected $primaryKey = "config_id";
//    public $timestamps = false;
    protected $fillable
        = [
            'config_id', 'key', 'value_vi','value_en', 'name'
        ];

    public function getAll()
    {
        return $this->select('config_id', 'key', 'value_vi as value', 'name')->get();
    }
    /**
     * Edit
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->where('config_id', $id)->update($data);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getInfoByKey($key)
    {
        return $this->where('key', $key)->select('config_id', 'key', 'value_vi as value', 'name')->first();
    }

    public function getInfoById($id)
    {
        return $this->where('config_id', $id)->select('config_id', 'key', 'value_vi as value', 'name')->first();
    }

    public function updateByKey($key,$data){
        return $this->where('key', $key)->update($data);
    }
}