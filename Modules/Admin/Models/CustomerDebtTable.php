<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use MyCore\Models\Traits\ListTableTrait;

class CustomerDebtTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_debt';
    protected $primaryKey = 'customer_debt_id';
    protected $fillable = [
        'customer_debt_id',
        'debt_code',
        'customer_id',
        'staff_id',
        'debt_type',
        'order_id',
        'status',
        'amount',
        'amount_paid',
        'note',
        'updated_by',
        'created_by',
        'updated_at',
        'created_at',
    ];

    /**
     * @param array $filters
     * @return mixed
     */
    public function _getList(&$filters = [])
    {
        $select = $this->select(
            'orders.order_code as order_code',
            'customers.full_name as customer_name',
            'staffs.full_name as staff_name',
            'customer_debt.created_at as created_at',
            'customer_debt.customer_debt_id',
            'customer_debt.amount',
            'customer_debt.amount_paid',
            'customer_debt.note',
            'customer_debt.status',
            'branches.branch_name'
        )
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'customer_debt.staff_id')
            ->leftJoin('orders', 'orders.order_id', '=', 'customer_debt.order_id')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_debt.customer_id')
            ->leftJoin('branches', 'branches.branch_id', '=', 'staffs.branch_id')
            ->orderBy('customer_debt.created_at', 'desc');
        if (
            isset($filters['search_keyword'])
            && $filters['search_keyword'] != ''
        ) {
            $keyword = $filters['search_keyword'];
            $select->where(function ($query) use ($keyword) {
                $query->where('customers.full_name', 'like',
                    '%' . $keyword . '%')
                    ->orWhere('orders.order_code', 'like',
                        '%' . $keyword . '%');
            });
            unset($filters['search_keyword']);
        }
        if (isset($filters["created_at"]) && $filters["created_at"] != "") {
            $arr_filter = explode(" - ", $filters["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select->whereBetween('customer_debt.created_at', [$startTime . ' 00:00:00', $endTime . ' 23:59:59']);
        }

        if (Auth::user()->is_admin != 1) {
            $select->where('branches.branch_id', Auth::user()->branch_id);
        }
        return $select;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->customer_debt_id;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->where('customer_debt_id', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCustomerDebt($id)
    {
        $ds = $this->leftJoin('staffs', 'staffs.staff_id', '=', 'customer_debt.staff_id')
            ->leftJoin('orders', 'orders.order_id', '=', 'customer_debt.order_id')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_debt.customer_id')
            ->select(
                'customer_debt.customer_debt_id',
                'customer_debt.debt_code',
                'customer_debt.customer_id',
                'customer_debt.order_id',
                'customer_debt.amount',
                'customer_debt.amount_paid',
                'customer_debt.created_at',
                'customer_debt.created_by',
                'staffs.full_name',
                'customer_debt.debt_type',
                'orders.order_code',
                'customers.full_name as customer_name',
                'customer_debt.note'
            )->where('customer_debt.customer_debt_id', $id)->first();
        return $ds;
    }

    /**
     * @param $id_customer
     * @return mixed
     */
    public function getItemDebt($id_customer)
    {
        return $this
            ->select(
                'orders.order_code as order_code',
                'customers.full_name as customer_name',
                'staffs.full_name as staff_name',
                'customer_debt.created_at as created_at',
                'customer_debt.customer_debt_id',
                'customer_debt.amount',
                'customer_debt.amount_paid',
                'customer_debt.note',
                'customer_debt.status',
                'staffs.full_name'
            )
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'customer_debt.staff_id')
            ->leftJoin('orders', 'orders.order_id', '=', 'customer_debt.order_id')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_debt.customer_id')
            ->where('customer_debt.customer_id', $id_customer)
            ->where('customer_debt.status', '!=', 'cancel')
            ->orderBy('customer_debt.created_at', 'desc')->get();
    }

    /**
     * @param $id_branch
     * @param $time
     * @return mixed
     */
    public function reportDebtAll($id_branch, $time)
    {
        $ds = $this
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'customer_debt.created_by')
            ->leftJoin('branches', 'branches.branch_id', '=', 'staffs.branch_id')
            ->select(
                'branches.branch_name',
                'branches.branch_id',
                'customer_debt.amount',
                'customer_debt.status',
                'customer_debt.amount_paid'
            );
        if (isset($id_branch)) {
            $ds->where('branches.branch_id', $id_branch);
        }
        if (isset($time) && $time != "") {
            $arr_filter = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $ds->whereBetween('customer_debt.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
        }
        return $ds->get();
    }


    /**
     * @param $order_id
     * @return mixed
     */
    public function getCustomerDebtByOrder($order_id)
    {
        $ds = $this->leftJoin('staffs', 'staffs.staff_id', '=', 'customer_debt.staff_id')
            ->leftJoin('orders', 'orders.order_id', '=', 'customer_debt.order_id')
            ->leftJoin('customers', 'customers.customer_id', '=', 'customer_debt.customer_id')
            ->select(
                'customer_debt.customer_debt_id',
                'customer_debt.debt_code',
                'customer_debt.customer_id',
                'customer_debt.order_id',
                'customer_debt.amount',
                'customer_debt.amount_paid',
                'customer_debt.created_at',
                'customer_debt.created_by',
                'staffs.full_name',
                'customer_debt.debt_type',
                'orders.order_code',
                'customers.full_name as customer_name',
                'customer_debt.note'
            )
            ->where('customer_debt.order_id', $order_id)
            ->where('debt_type', 'order')
            ->first();
        return $ds;
    }
}