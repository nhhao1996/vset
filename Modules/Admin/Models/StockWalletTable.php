<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/12/2021
 * Time: 5:23 PM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockWalletTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_wallet";
    protected $primaryKey = "stock_wallet_id";

    protected $fillable = [
        'stock_wallet_id', 'customer_id', 'action','type', 'source', 'total_before', 'total_money','total_after',
        'day', 'month','year', 'created_by', 'updated_by', 'created_at','updated_at'
    ];
    public $timestamps = false;
    /**
     * Lấy số dư cuối cùng của customer
     *
     * @param $customerId
     * @return array
     */
    public function getTotalAfterOfCustomer($customerId){
        $oSelect = $this->select(
            "total_after"
        )->where("{$this->table}.source","=","stock")
            ->where("{$this->table}.customer_id",$customerId)->orderBy("stock_wallet_id","DESC");
        if($oSelect->first() == null){
            return ['total_after'=>'0'];
        }
        else{
            return $oSelect->first()->toArray();
        }
    }
    public function getTotalAfterOfCustomerDividend($customerId){
        $oSelect = $this->select(
            "total_after"
        )->where("{$this->table}.source","=","dividend")
            ->where("{$this->table}.customer_id",$customerId)->orderBy("stock_wallet_id","DESC");
        if($oSelect->first() == null){
            return ['total_after'=>'0'];
        }
        else{
            return $oSelect->first()->toArray();
        }
    }
    /**
     * Tạo 1 log (+,-) cho 1 customer
     *
     * @param $data
     * @return mixed
     */
    public function createStockOrDividendWallet($data){
        $add = $this->create($data);
        return $add->stock_wallet_id;
    }
    /**
     * Tạo 1 log (+,-) cho 1 customer
     *
     * @param $data
     * @return mixed
     */
    public function createStockWallet($data){
        $add = $this->create($data);
        return $add->stock_wallet_id;
    }
       /**
     * Lấy stock wallet theo customer_id
     *
     * @param $data
     * @return mixed
     */
    public function getLastStockByCustomer($customerId){
        $select =  $this->where("customer_id",$customerId)
//                        ->where("type","add")
                        ->where("source","stock")
                        ->orderBy($this->primaryKey,'DESC')
                        ->first();
        return $select;

    }

    public function getWalletByCustomerId($customerId,$source){
        $oSelect = $this
            ->where('customer_id',$customerId)
            ->where('source',$source)
            ->orderBy('stock_wallet_id','DESC')
            ->first();
        return $oSelect;
    }

    public function updateCustomer($customerId,$data){
        $oSelect = $this->where('customer_id',$customerId)->update($data);
        return$oSelect;
    }

    public function updateStockWallet($id,$data){
        $oSelect = $this->where($this->primaryKey,$id)->update($data);
        return$oSelect;
    }

    public function createWallet($data){
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

}