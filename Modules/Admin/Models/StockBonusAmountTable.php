<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockBonusAmountTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_bonus_amount";
    protected $primaryKey = "stock_bonus_amount_id";
    public $timestamps = false;

    protected $fillable = [
        "stock_bonus_amount_id"
        ,"stock_to"
        ,"stock_from"
        ,"rate"
        ,"created_at"
        ,"created_by"
        ,"updated_at"
        ,"updated_by"
    ];


    /**
     * Lấy danh sách stock_bonus
     *
     * @param $stockId
     * @return mixed
     */
    public function _getList(array &$filters=[]){
        return $this->select("{$this->table}.*");
    }
    public function add($data){
        return $this->insert($data);
    }
    
    /**
     * Updatestock_bonus
     *
     * @param $stock_bonus_id
     * @return mixed
     */
    public function edit(array $data=[],$id){
        return $this->where($this->primaryKey, $id)->first()->update($data);        
    }
    public function remove($id){
        return $this->where($this->primaryKey,$id)->first()->delete();
    }


    public function getRateWithQuantity($quantity)
    {
        $data = $this->select("rate")
            ->where("stock_from", "<=", $quantity)
            ->where("stock_to", ">=", $quantity)->first();
        if($data == null){
            $data['rate'] = 0;
        }
        return $data;
    }
    public function getLast(){
        return $this->select()
            ->orderBy('stock_to','DESC')
            ->orderBy($this->primaryKey,'DESC')
            ->first();
    }

}