<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 11/16/2018
 * Time: 4:40 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryCheckingDetailTable extends Model
{
    protected $table = 'inventory_checking_details';
    protected $primaryKey = 'inventory_checking_detail_id';

    protected $fillable = ['inventory_checking_detail_id', 'inventory_checking_id', 'product_code', 'quantity_old', 'quantity_new', 'quantity_difference', 'current_price', 'total', 'type_resolve', 'updated_by', 'created_by', 'updated_at', 'created_at'];

    /**
     * Insert inventory input to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $inventoryInput = $this->create($data);
        return $inventoryInput->inventory_checking_detail_id;
    }

    /*
     * get detail inventory checking detail
     */
    public function getDetailInventoryCheckingDetail($parentId)
    {
        $select = $this->leftJoin('product_childs', 'product_childs.product_code', '=', 'inventory_checking_details.product_code')
            ->leftJoin('units', 'units.unit_id', '=', 'product_childs.unit_id')
            ->leftJoin('inventory_checkings', 'inventory_checkings.inventory_checking_id', '=', 'inventory_checking_details.inventory_checking_id')
//            ->leftJoin('product_inventorys', 'product_inventorys.product_code', '=', 'inventory_checking_details.product_code')
//            ->leftJoin('product_inventorys', 'product_inventorys.warehouse_id', '=', 'inventory_checkings.warehouse_id')
//            ->leftJoin('product_inventorys', function ($join) {
//                $join->on('inventory_checking_details.product_code', '=', 'product_inventorys.product_code');
//                $join->on('inventory_checkings.warehouse_id', '=', 'product_inventorys.warehouse_id');
//            })
            ->select(
                'product_childs.product_child_name as productName',
                'inventory_checking_details.quantity_old as quantityOld',
                'inventory_checking_details.quantity_new as quantityNew',
                'inventory_checking_details.quantity_difference as quantityDifference',
                'inventory_checking_details.current_price as currentPrice',
                'inventory_checking_details.total as total',
                'inventory_checking_details.type_resolve as typeResolve',
                'units.name as unitName',
                'units.unit_id as unitId',
                'inventory_checking_details.product_code as productCode'
            )
            ->where('inventory_checking_details.inventory_checking_id', $parentId)->get();
        return $select;
    }

    /*
     * edit by inventory checking id and product code
     */
    public function editByParentIdAndProductCode($parentId, $productCode, array $data)
    {
        return $this->where('inventory_checking_id', $parentId)
            ->where('product_code', $productCode)->update($data);
    }

    /*
     * Xóa khỏi db với điều kiện inventory_checking_id và product_code.
     */
    public function removeByParentIdAndProductCode($parentId, $productCode)
    {
        return $this->where('inventory_checking_id', $parentId)
            ->where('product_code', $productCode)->delete();
    }
}
//