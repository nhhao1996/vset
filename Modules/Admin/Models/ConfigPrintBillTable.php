<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 4/1/2019
 * Time: 11:57 AM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigPrintBillTable extends Model
{
    protected $table = "config_print_bill";
    protected $primaryKey = "id";

    protected $fillable = [
        'id', 'printed_sheet', 'is_print_reply', 'print_time', 'is_show_logo', 'is_show_unit', 'is_show_address', 'is_show_phone', 'is_show_order_code', 'is_show_cashier', 'is_show_customer', 'is_show_datetime', 'is_show_footer', 'updated_by', 'created_at', 'updated_at','template','symbol'
    ];

    public function getItem($id){
        $select=$this->select(
            'printed_sheet', 'is_print_reply',
            'print_time', 'is_show_logo',
            'is_show_unit', 'is_show_address',
            'is_show_phone', 'is_show_order_code',
            'is_show_cashier', 'is_show_customer',
            'is_show_datetime', 'is_show_footer','template','symbol')
            ->where($this->primaryKey, $id)->first();
        return $select;
    }

    public function edit(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);
    }
}