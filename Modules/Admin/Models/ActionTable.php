<?php
/**
 * Created by PhpStorm.
 * User: SonVeratti
 * Date: 3/17/2018
 * Time: 1:26 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class ActionTable extends Model
{
    protected $table = 'actions';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'title', 'is_actived', 'created_at', 'updated_at'
    ];


    public function add(array $data)
    {
        $oTitle = $this->create($data);
        return $oTitle->id;
    }

    public function getList()
    {
        $select = $this->select('id', 'name', 'title')
            ->where('is_actived', 1)->get()->toArray();
        return $select;
    }

    public function getAllRoute()
    {
        $select=$this->select('name')->where('is_actived',1)->get();
        $data=[];
        if ($select!=null)
        {
            foreach ($select->toArray() as $item){
                $data[]= $item['name'];
            }
        }
        return $data;
    }
}
//