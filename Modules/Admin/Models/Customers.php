<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
//Vset
class Customers extends Model
{
    protected $table = "customers";
    protected $primaryKey = "customer_id";
    protected $fillable = [
        'customer_id',
        'full_name',
        'birthday',
        'gender',
        'phone',
        'phone2',
        'phone_verified',
        'email',
        'email_verified',
        'password',
        'ic_no',
        'ic_date',
        'ic_place',
        'ic_front_image',
        'ic_back_image',
        'contact_province_id',
        'contact_district_id',
        'contact_address',
        'residence_province_id',
        'residence_district_id',
        'residence_address',
        'bank_id',
        'bank_account_no',
        'bank_account_name',
        'bank_account_branch',
        'customer_source_id',
        'customer_refer_id',
        'customer_refer_code',
        'customer_avatar',
        'note',
        'zalo',
        'facebook',
        'customer_code',
        'customer_group_id',
        'point',
        'member_level_id',
        'FbId',
        'ZaloId',
        'is_updated',
        'site_id',
        'branch_id',
        'postcode',
        'date_last_visit',
        'is_actived',
        'is_deleted',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

}
