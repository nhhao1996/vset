<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockBonusTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_bonus";
    protected $primaryKey = "stock_bonus_id";
    public $timestamps = false;

    protected $fillable = [
        "stock_bonus_id",
        "month",
        "percent",
        "created_at",
    ];


    /**
     * Lấy danh sách stock_bonus
     *
     * @param $stockId
     * @return mixed
     */
    public function _getList(array $filters=[]){
        return $this->select("{$this->table}.*")->get();
    }
    
    /**
     * Updatestock_bonus
     *
     * @param $stock_bonus_id
     * @return mixed
     */
    public function edit(array $data=[],$id){
        return $this->where($this->primaryKey, $id)->first()->update($data);        
    }

    public function getRate($month)
    {
        return $this->select("percent")->where("month", $month)->first();
    }

}