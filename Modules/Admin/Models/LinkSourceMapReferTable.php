<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class LinkSourceMapReferTable extends Model
{
    use ListTableTrait;
    protected $table = "link_source_map_refer";
    protected $primaryKey = "map_refer_id";
    protected $fillable = [
        'map_refer_id',
        'link_source_id',
        'service_id'
    ];

    public function getAll($id) {
        $oSelect = $this
            ->join('services','services.service_id','link_source_map_refer.service_id')
            ->where('link_source_id',$id)
            ->select(
                'link_source_map_refer.map_refer_id',
                'services.service_id',
                'services.service_name_vi',
                'services.price_standard'
            )
            ->get();
        return $oSelect;
    }

    public function removeRefer($id) {
        $oSelect = $this->where('map_refer_id',$id)->delete();
        return $oSelect;
    }

    public function getService($link_source_id){
        $oSelect = $this->where('link_source_id',$link_source_id)->get();
        return $oSelect;
    }

    public function addService($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }
    
}