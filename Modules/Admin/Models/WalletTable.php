<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class WalletTable extends Model
{
    use ListTableTrait;
    protected $table = "wallet";
    protected $primaryKey = "wallet_id";
    protected $fillable = [
        'wallet_id',
        'customer_contract_id',
        'customer_id',
        'type',
        'total_money',
        'created_at',
        'created_by',
    ];

    public function _getList(&$filters = []) {
        $select = $this
            ->leftJoin('customer_contract','customer_contract.customer_contract_id',$this->table.'.customer_contract_id')
            ->where($this->table.'.total_money','<>',0);
        if (isset($filters['refer_id'])){
            $select = $select->where($this->table.'.customer_id',$filters['refer_id']);
            unset($filters['refer_id']);
        }
        $select = $select
            ->select(
                $this->table.'.*',
                'customer_contract.commission_rate'
            );
        return $select->orderBy($this->table.'.created_at','DESC');
    }

    public function createWallet($data){
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function createMultipleWallet($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function totalBonusCustomer($customer_id,$type = []){
        $oSelect = $this->where('customer_id',$customer_id);
        if (count($type) != 0) {
            $oSelect = $oSelect->whereIn('type',$type);
        }
        $oSelect = $oSelect->select(DB::raw('SUM(total_money) as total_commission'))->first();
        return $oSelect;
    }


}