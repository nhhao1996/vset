<?php

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class SupportRequest extends Model
{
    use ListTableTrait;
    protected $table = 'support_request';
    protected $primaryKey = 'support_request_id';

    protected $fillable = ["support_request_id", "support_request_type_id","customer_id",
        "support_request_description", "object_type", "object_id",
        "support_request_time", "support_request_status", "is_actived",
        "is_deleted", "created_at", "created_by", "updated_at", "updated_by"];

    public function _getList(&$filter = [])
    {
        $select = $this
            ->select(
                $this->table.'.support_request_id',
                'srt.support_request_type_name_vi as support_request_type_name',
                $this->table.'.support_request_description',
                $this->table.'.object_type',
                $this->table.'.support_request_status',
                $this->table.'.support_request_time',
                $this->table.'.created_at',
                $this->table.'.updated_at',
                'customers.full_name',
                'customers.phone2',
                'customers.email',
                'customers.customer_id'

            )
            ->join("support_request_type as srt","srt.support_request_type_id","=",$this->table.".support_request_type_id")
            ->join("customers","customers.customer_id","=",$this->table.".customer_id")
            ->where($this->table.'.is_deleted', 0)
            ->where('customers.is_deleted', 0)
            ->where('customers.is_actived', 1)
            ->orderBy($this->table.'.support_request_id','DESC');

        if (isset($filter['customer']) != "") {
            $search = $filter['customer'];
            $select->where(function ($query) use ($search) {
                $query
                    ->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone2', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%');
            });
            unset($filter['customer']);
        }
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $select->where(function ($query) use ($search) {
                $query->where($this->table.'.support_request_description', 'like', '%' . $search . '%');
            });
        }
        if (isset($filter['created_at']) != "") {
            $arr_filter = explode(" - ", $filter["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select->whereBetween($this->table.'.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
            unset($filter['created_at']);
        }

//        unset($filter);
        return $select;
    }

    /**
     * @param $id
     * @return
     */
    public function getItem($id)
    {
        $oSelect = $this
            ->select(
                $this->table.'.support_request_id',
                $this->table.'.support_request_type_id',
                'srt.support_request_type_name_vi as support_request_type_name',
                $this->table.'.support_request_description',
                $this->table.'.object_id',
                $this->table.'.object_type',
                'p.product_name_vi as product_name',
                $this->table.'.support_request_status',
                $this->table.'.support_request_time',
                $this->table.'.created_at',
                $this->table.'.updated_at',
                'customers.full_name',
                'customers.phone2',
                'customers.email'
            )->join("support_request_type as srt","srt.support_request_type_id","=",$this->table.".support_request_type_id")
            ->join("customers","customers.customer_id","=",$this->table.".customer_id")
            ->leftJoin("products as p","p.product_id","=",$this->table.'.object_id')
            ->where($this->table.'.is_deleted', 0)
            ->where('customers.is_deleted', 0)
            ->where('customers.is_actived', 1)
            ->where($this->table.'.support_request_id', $id);
        return $oSelect->first();
    }

    public function updateStatus($id,$status){
        $this->where($this->table.'.support_request_id',$id)->update([
            "support_request_status"=>$status
        ]);
    }

    public function remove($id){
        $this->where($this->table.'.support_request_id',$id)->update([
            "is_deleted"=>1
        ]);
    }
}
