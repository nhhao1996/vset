<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class LinkSourceTable extends Model
{
    use ListTableTrait;
    protected $table = "link_source";
    protected $primaryKey = "link_source_id";
    protected $fillable = [
        'link_source_id',
        'source',
        'source_name',
        'updated_at',
        'updated_by'
    ];

    public function getListAll(){
        $oSelect = $this->get();
        return $oSelect;
    }

}