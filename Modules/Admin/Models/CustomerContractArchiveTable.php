<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 20/3/2019
 * Time: 15:25
 */

namespace Modules\Admin\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;
//Vset
class CustomerContractArchiveTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_contract_archive';
    protected $primaryKey = 'customer_contract_archive_id';
    protected $fillable = [
        'customer_contract_archive_id',
        'customer_contract_origin',
        'customer_contract_from',
        'customer_contract_extended',
        'created_at',
        'updated_at'
    ];

//    Kiểm tra hợp đồng gia hạn trước đó hay k
    public function checkContractExtend($customer_contract_extended)
    {
        $oSelect = $this->where('customer_contract_extended',$customer_contract_extended)->first();
        return $oSelect;
    }

//    Lưu log hợp đồng gia hạn
    public function createContractExtend($data){

    }

}