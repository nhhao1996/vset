<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class AccountStatementTable extends Model
{
    use ListTableTrait;
    protected $table = "account_statement";
    protected $primaryKey = "account_statement_id";
    protected $fillable = [
        'account_statement_id',
        'customer_id',
        'month',
        'user_name',
        'phone',
        'date_satement'
    ];

    public function _getList(&$filters = [])
    {
        $oSelect = $this
            ->join('customers','customers.customer_id','account_statement.customer_id')
            ->orderBy('account_statement_id','DESC');

        if (isset($filters['search']) != "") {
            $search = $filters['search'];
            $oSelect->where(function ($query) use ($search) {
                $query->where('customers.customer_code', 'like', '%' . $search . '%')
                    ->orWhere('account_statement.user_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%');
            });
            unset($filters['search']);
        }

        return $oSelect;
    }


    public function createAccountStatement($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

}