<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockContractSubFileTable extends Model
{
    use ListTableTrait;
    protected $table = 'stock_contract_sub';
    protected $primaryKey = 'stock_contract_sub_id';
    protected $fillable = [
        "stock_contract_sub_id",
        'stock_contract_id',
        'file',
        'created_at',
        'created_by'
    ];
    public $timestamps=false;

    /**
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter=[]){
        $select  = $this->select();
        if(isset($filter['stock_contract_id'])){
            $select->where("{$this->table}.stock_contract_id",$filter['stock_contract_id']);

        }
        return $select;

    }
    public function add($data){
        return $this->create($data);
    }
    public function getQuanConOfCustomer($stock_contract_id){
        return $this
            ->select()
            ->where('stock_contract_id',$stock_contract_id)
            ->count();
    }



}