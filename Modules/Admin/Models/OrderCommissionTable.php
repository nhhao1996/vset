<?php


namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderCommissionTable extends Model
{
    protected $table = 'order_commission';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'order_detail_id',
        'refer_id',
        'staff_id',
        'refer_money',
        'staff_money',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];


    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->id;
    }

    /**
     * @param $order_detail_id
     * @return mixed
     */
    public function getItemByOrderDetail($order_detail_id)
    {
        return $this->where('order_detail_id', $order_detail_id)->first();
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->where('id', $id)->update($data);
    }

    /**
     * @param $customer_id
     * @return mixed
     */
    public function getCommissionByCustomer($customer_id)
    {
        $ds = $this
            ->leftJoin('order_details', 'order_details.order_detail_id', '=', 'order_commission.order_detail_id')
            ->leftJoin('orders', 'orders.order_id', '=', 'order_details.order_id')
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'order_commission.created_by')
            ->select(
                'orders.order_code',
                'staffs.full_name',
                'order_commission.refer_money',
                'order_commission.status',
                'order_commission.created_at'
            )
            ->where('order_commission.refer_id', $customer_id)->get();
        return $ds;
    }

    /**
     * @param $time
     * @return mixed
     */
    public function reportStaffCommission($time)
    {
        $ds = $this->select(
            'staff_id',
            'staff_money'
        )
            ->whereNotNull('staff_id')
            ->where('status', 'approve');
        if (isset($time)) {
            $arr_filter = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $ds->whereBetween('created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
        }
        return $ds->get();
    }
}