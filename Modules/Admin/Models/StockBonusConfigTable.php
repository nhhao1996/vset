<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockBonusConfigTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_bonus_config";
    protected $primaryKey = "stock_bonus_config_id";
    public $timestamps = false;

    protected $fillable = [
        "stock_bonus_config_id"
        ,"stock_amount_bonus"
        ,"stock_payment_method_bonus"
        ,"stock_refer_bonus"
        ,"created_at"
        ,"created_by"
        ,"updated_at"
        ,"updated_by"
    ];


    /**
     * Lấy danh sách stock_bonus
     *
     * @param $stockId
     * @return mixed
     */
    public function _getList(array &$filters=[]){
        return $this->select("{$this->table}.*");
    }
    /**
     * Lấy config Bonus
     *
     * @param $stockId
     * @return mixed
     */
    public function getFirst(){
        return $this->first();
    }
    public function add($data){
        return $this->insert($data);
    }
    
    /**
     * Updatestock_bonus
     *
     * @param $stock_bonus_id
     * @return mixed
     */
    public function edit(array $data=[]){
        return $this->first()->update($data);
    }
    /**
     * Ds các stock bonus config với trạng thái của từng loại
     *
     * @return mixed
     */
    public function getStockBonusConfig()
    {
        $data = $this->first();
        return $data;
    }

}