<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 20/3/2019
 * Time: 15:25
 */

namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use MyCore\Models\Traits\ListTableTrait;

//Vset
class CustomerContractInterestByTimeLogTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_contract_interest_by_time_log';
    protected $primaryKey = 'customer_contract_interest_by_time_log_id';
    protected $fillable = [
        'customer_contract_interest_by_time_log_id',
        'customer_contract_id',
        'customer_id',
        'interest_year',
        'interest_month',
        'interest_day',
        'interest_rate_by_day',
        'interest_time',
        'interest_rate_by_time',
        'interest_amount',
        'interest_total_amount',
        'interest_banlance_amount',
        'withdraw_date_planning',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];

    /**
     * Record cuối cùng của customer contract
     * @param $customerContractId
     *
     * @return mixed
     */
    public function lastCustomerContractInterest($customerContractId)
    {
        $select = $this->select($this->table . '.*')
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $select;
    }

    /**
     * Xoá lãi theo id hợp đồng
     * @param $customerContractId
     */
    public function removeByContract($customerContractId){
        return $this->where($this->table . '.customer_contract_id', $customerContractId)->delete();
    }

    public function addLog($data){
        return $this->insertGetId($data);
    }
}