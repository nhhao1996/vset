<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class CustomerSearchLogTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_search_log';
    protected $primaryKey = 'customer_search_log_id';
    protected $fillable = [
        'customer_search_log_id',
        'content',
        'type',
        'obj_id',
        'customer_id',
        'created_at',
    ];

    public $timestamps = false;

    public function _getList(&$filters = [])
    {
        $select = $this;
        if (isset($filters['customer_id'])){
            $select = $select->where('customer_id',$filters['customer_id']);
            unset($filters['customer_id']);
        }
        $select = $select
            ->join('products', function ($join){
                $join->on('customer_search_log.obj_id','products.product_id')
                    ->where('customer_search_log.type','product');
            })
            ->whereNotNull('obj_id')
            ->select(
            'customer_search_log.*',
            'products.product_category_id',
            DB::raw('count(*) as total')
        )
        ->groupBy('customer_search_log.obj_id')
        ->groupBy('customer_search_log.type')
        ->groupBy('products.product_category_id')
        ->orderBy('customer_search_log.customer_search_log_id','DESC');
        return $select;
    }
}