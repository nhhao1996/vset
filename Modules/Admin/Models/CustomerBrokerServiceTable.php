<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class CustomerBrokerServiceTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_broker_service';
    protected $primaryKey = 'customer_broker_service_id';
    protected $fillable = [
        'customer_broker_service_id',
        'service_id'
    ];

    public function getListAll()
    {
        $oSelect = $this->get();
        return $oSelect;
    }

    public function createListService($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function removeService()
    {
        $oSelect = $this->whereNotNull('customer_broker_service_id')->delete();
        return $oSelect;
    }
}