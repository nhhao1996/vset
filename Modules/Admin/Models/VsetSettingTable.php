<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class VsetSettingTable extends Model
{
    use ListTableTrait;
    protected $table = "vset_setting";
    protected $primaryKey = "vset_setting_id";

    protected $fillable = [
        'vset_setting_id',
        'vset_setting_key',
        'vset_setting_value',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public function getSettingByKey($key) {
        $oSelect = $this->where('vset_setting_key',$key)->first();
        return $oSelect;
    }
}