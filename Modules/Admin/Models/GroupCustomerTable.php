<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;
use function Aws\filter;

class GroupCustomerTable extends Model
{
    use ListTableTrait;
    protected $table = "group_customer";
    protected $primaryKey = "group_customer_id";
    public $timestamps = false;

    const NOT_DELETED = 0;

    protected $fillable = [
        'group_customer_id',
        'group_name',
        'is_active',
        'is_deleted',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    public function add($data)
    {
        $select = $this->create($data);
        return $select->{$this->primaryKey};
    }

    public function getItem($id)
    {
        $select = $this->select($this->fillable)
            ->where('group_customer_id', $id)
            ->first();
        return $select;
    }

    public function edit($id, $data)
    {
        $this->where('group_customer_id', $id)->update($data);
    }

    protected function _getList(&$filter = [])
    {
        $select = $this->select(
            $this->table . '.group_customer_id',
            $this->table . '.group_name',
            $this->table . '.is_active',
//            DB::raw('count(group_customer_map_customer.group_customer_id) as total')
            DB::raw('SUM(IF(customers.is_actived=1,1,0)) as total')
        )
            ->leftJoin(
                'group_customer_map_customer',
                'group_customer_map_customer.group_customer_id',
                $this->table . '.group_customer_id'
            )
            ->leftJoin('customers','customers.customer_id','group_customer_map_customer.customer_id')
            ->where($this->table . '.is_deleted', self::NOT_DELETED)
//            ->where('customers.is_actived', 1)
            ->groupBy($this->table . '.group_customer_id')
            ->orderBy($this->table . '.group_customer_id', 'DESC');
        if (isset($filter['search']) && $filter['search'] != null && $filter['search'] != '') {
            $select->where($this->table . '.group_name', 'like', '%' . $filter['search'] . '%');
        }
        unset($filter['search']);
        return $select;
    }

    public function getCondition($condition = [],$listGroup = [])
    {
        $select = $this
            ->where($condition)
            ->where($this->table . '.is_deleted', self::NOT_DELETED);
        if (count($listGroup) != 0) {
            $select = $select->whereNotIn('group_customer_id',$listGroup);
        }
        $select = $select->get();
        return $select;
    }

    public function deleteCustomer($id){
        $oSelect = $this->where('group_customer_id',$id)->delete();
        return $oSelect;
    }

    public function getAll(){
        $oSelect = $this->get();
        return $oSelect;
    }
}