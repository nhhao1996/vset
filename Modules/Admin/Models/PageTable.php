<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 4/8/2019
 * Time: 1:58 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class PageTable extends Model
{
    protected $table = 'pages';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'name', 'route', 'is_actived', 'created_at', 'updated_at'];

    public function add(array $data)
    {
        $oCustomerGroup = $this->create($data);
        return $oCustomerGroup->id;
    }

    public function getList()
    {
        $select = $this->select('id', 'name', 'route')->where('is_actived', 1)->get()->toArray();
        return $select;
    }

    public function getAllRoute()
    {
        $select = $this->select('route')->where('is_actived', 1)->get();
        $data = [];
        if ($select != null) {
            foreach ($select->toArray() as $item) {
                $data[] = $item['route'];
            }
        }
        return $data;
    }
}
////