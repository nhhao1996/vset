<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockTable extends Model
{
    use ListTableTrait;
    protected $table = "stock";
    protected $primaryKey = "stock_id";

    protected $fillable = [
        'stock_id', 'stock_config_id', 'quantity', 'quantity_min', 'quantity_sale', 'money', 'publish_at','money_publish',
        'is_active', 'created_at','created_by', 'updated_at', 'updated_by','quantity_publish'
    ];

    /**
     * Lấy số lượng phát hành
     *
     * @return mixed
     */
    public function getQuantity(){
        $data = $this->select("quantity")
            ->where("is_active","=","1")
            ->orderBy("stock_id","DESC")->first();
        return $data;
    }

    /**
     * Lấy số lượng phát hành đã bán
     *
     * @param $stockId
     * @return mixed
     */
    public function getQuantitySale(){
        $data = $this->select("quantity_sale")
            ->where("is_active","=","1")
            ->orderBy("stock_id","DESC")->first();
        return $data;
    }

    public function getMoney(){
        $data = $this->select("money","money_publish")
            ->orderBy("stock_id","DESC")->first();
        return $data;
    }
    /**
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter = [])
    {
        $select = $this->select($this->table . ".*")
                    ->orderBy("publish_at",'DESC');
         if (isset($filter['startTime']) != "" && isset($filter['endTime'])) {            
             $select->whereBetween($this->table . '.publish_at', [$filter['startTime']. ' 00:00:00', $filter['endTime']. ' 23:59:59']);
            unset($filter['startTime']);
             unset($filter['endTime']);         
         }        
        return $select;
        

    }
    public function getStockPublish(){
        $ds = $this->select($this->table .".*")
            ->orderBy("stock_id",'DESC')
            ->paginate(2);
        // dd("get Stock Publish o Model: ", $ds);
        return $ds;

    }
    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->stock_id;
    }
    public function getLastStock(){
        $select = $this
            ->orderBy("stock_id","DESC")->first();
        return $select;
    }
    public function getStockPublishTransfer($startTime){
       $select =  $this
           ->select()
           ->whereDate("{$this->table}.created_at","<=",$startTime)
           ->orderBy("created_at",'DESC');
       return $select->first();
//           dd('$select: ', $select->toSql(0));

    }
    public function getListStockFile(){
        $select = $this
        ->leftJoin('stock_config_file as sf',$this->table .'.stock_id','sf.stock_id')
        ->orderBy("stock_id","DESC")->get();
        return $select;

    }
    public function edit($data,$id){
        $this->where($this->primaryKey,$id)->update($data);

    }
    public function deActiveAllStock($data){
        return  $this->where("is_active",1)->update($data);

    }


}