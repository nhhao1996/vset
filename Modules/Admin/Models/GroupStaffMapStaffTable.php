<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class GroupStaffMapStaffTable extends Model
{
    use ListTableTrait;
    protected $table = "group_staff_map_staff";
    protected $primaryKey = "group_staff_map_staff_id";
    public $timestamps = false;

    protected $fillable = [
        'group_staff_map_staff_id',
        'group_staff_id',
        'staff_id',
    ];

    /**
     * Thêm 1 lần nhiều record
     * @param $data
     * @return mixed
     */
    public function addInsert($data)
    {
        $this->insert($data);
    }

    public function remove($groupCustomerId)
    {
        $this->where('group_staff_id', $groupCustomerId)->delete();
    }

    public function getByGroup($groupId)
    {
        $select = $this->where('group_staff_id', $groupId)->get();
        return $select;
    }

    public function getListStaffId($group_staff_id = null){
        $oSelect = $this;
        if ($group_staff_id != null){
            $oSelect = $oSelect->where('group_staff_id','<>',$group_staff_id);
        }
        $oSelect = $oSelect->get();
        return $oSelect;
    }

    public function deleteStaff($groupStaffId){
        $oSelect = $this->where('group_staff_id',$groupStaffId)->delete();
        return $oSelect;
    }

}