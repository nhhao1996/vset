<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockConfigFileTable extends Model{
    use ListTableTrait;
    protected $table = 'stock_config_file';
    protected $primaryKey = 'stock_config_file_id';
    protected $fillable = [
        "stock_config_file_id",
        "stock_id",
        "file",
        "created_at",
        "created_by",
        "updated_at",
        "updated_by"
    ];
        /**
     * @param array $data
     * @return mixed
     */
    public function _getList()
    {
      
        $select = $this->select()
        ->leftJoin("stock_config as sc",$this->table . ".stock_config_id",'sc.stock_config_id')
        ->orderBy($this->primaryKey,'DESC')
        ->get();    
        
        return $select;
    }
      /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
      
        $add = $this->insert($data);
        // return $add->stock_config_file_id;
        
        return $add;
    }
    public function remove($arrFileIds){
        return $this->whereNotIn($this->primaryKey,$arrFileIds)->delete();
    }


}