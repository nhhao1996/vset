<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 20/3/2019
 * Time: 15:25
 */

namespace Modules\Admin\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use MyCore\Models\Traits\ListTableTrait;

//Vset
class CustomerContractInterestByTimetable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_contract_interest_by_time';
    protected $primaryKey = 'customer_contract_interest_by_time_id';
    protected $fillable = [
        'customer_contract_interest_by_time_id',
        'customer_contract_id',
        'customer_id',
        'interest_year',
        'interest_month',
        'interest_day',
        'interest_rate_by_day',
        'interest_time',
        'interest_rate_by_time',
        'interest_amount',
        'interest_total_amount',
        'interest_banlance_amount',
        'withdraw_date_planning',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];

    //// ví lãi customer
    public function interestWalletCustomer($id)
    {
        $select = $this->select(
            $this->table.'.interest_total_amount'
        )
            ->where($this->table.'.customer_id', $id)
            ->groupby($this->table.'.customer_contract_interest_by_time_id')
            ->orderBy($this->table.'.customer_contract_interest_by_time_id', 'desc')
            ->first();
        return $select;
    }

    public function getInterestTotalInterest($contract_id){
        $oResult = $this->select(
            $this->table.".interest_total_amount",$this->table.".interest_banlance_amount", $this->table.'.withdraw_date_planning'
        )
            ->where($this->table.".customer_contract_id",$contract_id)
            ->orderBy($this->table.".customer_contract_interest_by_time_id","DESC")
            ->first();
//        dd($oResult);
        return $oResult;
    }

    /**
     * Danh sách lãi suất theo thời điểm
     *
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter = [])
    {
        $ds = $this
            ->select(
                "{$this->table}.interest_year",
                "{$this->table}.interest_month",
                "{$this->table}.interest_day",
                "{$this->table}.interest_time",
                "{$this->table}.interest_rate_by_day",
                "{$this->table}.interest_rate_by_time",
                "{$this->table}.interest_amount",
                "{$this->table}.interest_total_amount",
                "{$this->table}.interest_banlance_amount",
                "{$this->table}.withdraw_date_planning",
                "customer_contract.term_time_type"
            )
            ->join("customer_contract","customer_contract.customer_contract_id","{$this->table}.customer_contract_id")
            ->where("{$this->table}.customer_contract_id", $filter['customer_contract_id'])
            ->orderBy("{$this->table}.customer_contract_interest_by_time_id", "desc");
        unset($filter['customer_contract_id']);
        return $ds;
    }

    /**
     * Xóa hết record trong ngày
     * @param $year
     * @param $month
     * @param $day
     */
    public function removeByMonthYearDay($year, $month, $day)
    {
        $this->where($this->table . '.interest_year', $year)
            ->where($this->table . '.interest_month', $month)
            ->where($this->table . '.interest_day', $day)
            ->delete();
    }

    /**
     * Thêm 1 record
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $select = $this->create($data);
        return $select->primaryKey;
    }

    /**
     * Thêm nhiều record
     * @param array $data
     */
    public function addInsert(array $data)
    {
        $select = $this->insert($data);
    }

    /**
     * Record cuối cùng của customer contract
     * @param $customerContractId
     *
     * @return mixed
     */
    public function lastCustomerContractInterest($customerContractId)
    {
        $select = $this->select($this->table . '.*')
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $select;
    }

//    Lấy record cuối cùng để cập nhật tiền lãi mỗi khi tạo lãi tháng
    public function getLasted($customerContractId){
        $select = $this
            ->select(
                'customer_contract_interest_by_time_id',
                'customer_contract_id',
                'customer_id',
                'interest_year',
                'interest_month',
                'interest_day',
                'interest_rate_by_day',
                'interest_time',
                'interest_rate_by_time',
                'interest_amount',
                'interest_total_amount',
                'interest_banlance_amount',
                'withdraw_date_planning'
            )
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy('customer_contract_interest_by_time_id', 'DESC')
            ->first();
        return $select;
    }

    public function updateInterestTime($id,$data){
        $oSelect = $this->where('customer_contract_interest_by_time_id',$id)->update($data);
        return $oSelect;
    }
}