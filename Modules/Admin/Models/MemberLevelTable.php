<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17/03/2018
 * Time: 2:45 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

/**
 * User Model
 *
 * @author isc-daidp
 * @since Feb 23, 2018
 */
class MemberLevelTable extends Model
{

    use ListTableTrait;
    protected $table = 'member_levels';
    protected $primaryKey = "member_level_id";


    protected $fillable = [
        'member_level_id', 'name', 'name_en', 'slug', 'code', 'point', 'discount','member_image', 'range_content','range_content_en',
        'description','description_en', 'is_actived', 'created_by', 'updated_by', 'created_at',
        'updated_at', 'is_deleted'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected function _getList(&$filters = [])
    {
        $oSelect = $this->select(
            'member_level_id',
            'name',
            'name_en',
            'range_content',
            'range_content_en',
            'description',
            'description_en',
            'point',
            'is_actived',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
            'discount'
        )
            ->where('is_deleted', '=', 0);

        if (isset($filters['search_keyword']) != "") {
            $search = $filters['search_keyword'];
            $oSelect = $oSelect->where(function ($query) use ($search) {
                $query
                    ->where('name', 'like', '%' . $search . '%')
                    ->orWhere('name_en', 'like', '%' . $search . '%');
            });
            unset($filters['search_keyword']);
        }

        if (isset($filters['is_actived'])) {
            $oSelect = $oSelect->where('is_actived',$filters['is_actived']);
            unset($filters['is_actived']);
        }

        $oSelect = $oSelect->orderBy('point','ASC');
        return $oSelect;
    }


    /**
     * Remove user
     *
     * @param number $id
     */
    public function remove($id)
    {
        return $this->where($this->primaryKey, $id)->update(['is_deleted' => 1]);

    }

    /**
     * Insert user to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $oUser = $this->create($data);

        return $oUser->id;
    }

    public function edit(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)->update($data);
    }

    /**
     * Function get item
     */
    public function getItem($id)
    {
        return $this->where($this->primaryKey, $id)->first();
    }

    /**
     * Function get option member level
     */
    public function getOptionMemberLevel()
    {
        return $this->select('member_level_id', 'name', 'point')->get();
    }

    //function bat trung ten
    public function testName($name, $id)
    {
        return $this->where('slug', $name)->where('member_level_id', '<>', $id)->where('is_deleted', 0)->first();
    }

    /**
     * @param $point
     * @return mixed
     */
    public function rankByPoint($point)
    {
        $ds = $this
            ->select(
                'member_level_id',
                'name',
                'slug',
                'code',
                'point',
                'discount'
            )
            ->where('point', '<=', $point)
            ->where('is_deleted', 0)
            ->orderBy('point', 'desc')
            ->get();
        return $ds;
    }

    public function checkByPoint($point) {
        $oSelect = $this
            ->where('point', '<=', $point)
            ->where('is_actived', 1)
            ->where('is_deleted', 0)
            ->orderBy('point','DESC')
            ->first();
        return $oSelect;
    }
}