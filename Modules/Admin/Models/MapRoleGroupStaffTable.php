<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 4/22/2019
 * Time: 4:02 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class MapRoleGroupStaffTable extends Model
{
    protected $table = 'map_role_group_staff';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'role_group_id', 'staff_id', 'is_actived', 'created_at', 'updated_at'
    ];

    public function add(array $data)
    {
        $oAdd = $this->create($data);
        return $oAdd->id;
    }

    public function edit(array $data, $roleGroupId, $staffId)
    {
        return $this->where('role_group_id', $roleGroupId)
            ->where('staff_id', $staffId)
            ->update($data);
    }

    public function checkIssetMap($roleGroupId, $staffId)
    {
        return $this->select('id', 'role_group_id', 'staff_id', 'is_actived')
            ->where('role_group_id', $roleGroupId)
            ->where('staff_id', $staffId)->first();
    }

    public function getRoleGroupByStaffId($staffId)
    {
        return $this->select('id', 'role_group_id', 'staff_id', 'is_actived')
            ->where('staff_id', $staffId)
            ->where('is_actived', 1)
            ->get();
    }

    public function editById(array $data, $id)
    {
        return $this->where($this->primaryKey, $id)
            ->update($data);
    }

    public function removeByUser($id)
    {
        return $this->where('staff_id', $id)
            ->delete();
    }

    public function getRolePageByStaff($staff)
    {
        $select = $this->leftJoin('staffs', 'staffs.staff_id', '=', 'map_role_group_staff.staff_id')
            ->leftJoin('role_group', 'role_group.id', '=', 'map_role_group_staff.role_group_id')
            ->leftJoin('role_pages', 'role_pages.group_id', '=', 'role_group.id')
            ->leftJoin('pages', 'pages.id', '=', 'role_pages.page_id')
            ->where('map_role_group_staff.is_actived', 1)
            ->where('role_group.is_actived', 1)
            ->where('role_pages.is_actived', 1)
            ->where('pages.is_actived', 1)
            ->where('map_role_group_staff.staff_id', $staff)
            ->select('pages.route as route')->get();
        $data = [];
        if ($select != null) {
            foreach ($select as $item) {
                $data[] = $item['route'];
            }
        }

        return $data;
    }

    public function getRoleActionByStaff($staff)
    {
        $select = $this->leftJoin('staffs', 'staffs.staff_id', '=', 'map_role_group_staff.staff_id')
            ->leftJoin('role_group', 'role_group.id', '=', 'map_role_group_staff.role_group_id')
            ->leftJoin('role_actions', 'role_actions.group_id', '=', 'role_group.id')
            ->leftJoin('actions', 'actions.id', '=', 'role_actions.action_id')
            ->where('map_role_group_staff.is_actived', 1)
            ->where('role_group.is_actived', 1)
            ->where('role_actions.is_actived', 1)
            ->where('actions.is_actived', 1)
            ->where('map_role_group_staff.staff_id', $staff)
            ->select('actions.name as route')->get();
        $data = [];
        if ($select != null) {
            foreach ($select as $item) {
                $data[] = $item['route'];
            }
        }
        return $data;
    }
}
////