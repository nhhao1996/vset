<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 20/3/2019
 * Time: 15:25
 */

namespace Modules\Admin\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;
//Vset
class CustomerContactTable extends Model
{
    use ListTableTrait;
    protected $table = 'customer_contract';
    protected $primaryKey = 'customer_contract_id';
    protected $fillable = [
        'customer_contract_id',
        'customer_contract_code',
        'order_id',
        'customer_id',
        'product_id',
        'product_code',
        'product_name_vi',
        'product_short_name_vi',
        'description_vi',
        'product_name_en',
        'product_short_name_en',
        'description_en',
        'product_avatar',
        'product_image_detail',
        'quantity',
        'price_standard',
        'interest_rate_standard',
        'term_time_type',
        'investment_time',
        'withdraw_interest_time',
        'interest_rate',
        'commission_rate',
        'month_interest',
        'total_interest',
        'bonus_rate',
        'total',
        'bonus',
        'total_amount',
        'payment_method',
        'customer_contract_start_date',
        'customer_contract_end_date',
        'customer_name',
        'customer_phone',
        'customer_email',
        'customer_residence_address',
        'is_fully_withdraw',
        'customer_ic_no',
        'refer_id',
        'commission',
        'staff_id',
        'is_active',
        'is_deleted',
        'stock_convert_rate',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    const IS_DELETE = 0;

    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;
    const BOND = 1;
    const CONFIRM = '';

//    public $timestamps = false;

    public function _getList(&$filters = [])
    {
        $select = $this
            ->select(
                $this->table.'.*',
                'customers.full_name',
                'customers.birthday',
                'customers.birthday',
                'customers.ic_date',
                'customers.ic_place',
                'customers.residence_province_id',
                'province.name as province_name_contact',
                'district.name as district_name_contact',
                'customers.contact_address as contact_address_contact',
                'customers.ic_front_image',
                'customers.ic_back_image',
                "cct.withdraw_date_planning",
                'product_categories.category_name_vi'
            )
            ->join('customers','customers.customer_id','customer_contract.customer_id')
            ->join('products','products.product_id','customer_contract.product_id')
            ->join('product_categories','product_categories.product_category_id','products.product_category_id')
//            ->leftJoin("customer_contract_interest_by_time as cct", function ($join){
//                $join->on('cct.customer_contract_id', $this->table.'.customer_contract_id')
//                    ->where('cct.customer_contract_interest_by_time_id', '=', DB::raw("(select max(`customer_contract_interest_by_time_id`) from customer_contract_interest_by_time where customer_contract_id = {$this->table}.customer_contract_id)"));
//            })
            ->leftJoin("customer_contract_interest_by_month as cct", function ($join){
                $join->on('cct.customer_contract_id', $this->table.'.customer_contract_id')
//                    ->orderBy('cct.customer_contract_interest_by_month_id','DESC')
//                    ->limit(1);
                    ->where('cct.customer_contract_interest_by_month_id', '=', DB::raw("(select max(`customer_contract_interest_by_month_id`) from customer_contract_interest_by_month where customer_contract_id = {$this->table}.customer_contract_id)"));
            })
            ->leftjoin('province','province.provinceid','customers.contact_province_id')
            ->leftjoin('district','district.districtid','customers.contact_district_id');
//            ->where('customer_contract.is_deleted', self::IS_DELETE);
        if (isset($filters['time_expired'])) {
            $currentDateTime = Carbon::now();
            $month = (int)$filters["time_expired"];
            $newDateTime = Carbon::now()->addMonths($month)->format("Y-m-d");
            $select = $select->whereDate('customer_contract_end_date','<=', $newDateTime);
            unset($filters["time_expired"]);
            $select = $select->orderBy('customer_contract.customer_contract_end_date', 'asc');
        } else {
            $select = $select->orderBy('customer_contract.customer_contract_id', 'desc');
        }
        if (isset($filters['search'])) {
            $search = $filters['search'];
            $select = $select->where(function ($query) use ($search) {
                $query->where('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                      ->orWhere('customer_contract.customer_phone', 'like', '%' . $search . '%')
                      ->orWhere('customers.full_name', 'like', '%' . $search . '%');
            });
            unset($filters['search']);
        }
//        if (isset($filters['is_active']) && $filters['is_active'] != "") {
//            $select->where($this->table.'.is_active','=', $filters['is_active']);
//        }
//        if (isset($filters['is_deleted']) && $filters['is_deleted'] != "") {
//            $select->where($this->table.'.is_deleted','=', $filters['is_deleted']);
//            unset($filters['is_deleted']);
//        }
        if (isset($filters['startTime']) != "" && isset($filters['endTime'])) {
            $select = $select->whereBetween('customer_contract.created_at', [$filters['startTime']. ' 00:00:00', $filters['endTime']. ' 23:59:59']);
            unset($filters['startTime']);
            unset($filters['endTime']);
        }

        if (isset($filters['startTimeContract']) != "" && isset($filters['endTimeContract'])) {
            $select = $select->whereBetween('customer_contract.customer_contract_start_date', [$filters['startTimeContract']. ' 00:00:00', $filters['endTimeContract']. ' 23:59:59']);
            unset($filters['startTimeContract']);
            unset($filters['endTimeContract']);
        }

        if (isset($filters['product_category_id'])){
            $select = $select->where('product_categories.product_category_id',$filters['product_category_id']);
            unset($filters['product_category_id']);
        }

        if (isset($filters['id'])) {
            $select = $select->where('customers.customer_id','=', $filters['id']);
            unset($filters['id']);
        }

        if (isset($filters['refer_id'])) {
            $select = $select->where('customer_contract.refer_id','=', $filters['refer_id']);
            unset($filters['refer_id']);
        }
        if (isset($filters['active'])) {
            if ($filters['active'] == 1){
                $select = $select
                    ->where(function ($select){
                        $select = $select->where('customer_contract.customer_contract_end_date','<', Carbon::now())
//                    ->whereNotNull('customer_contract.customer_contract_end_date')
//                    ->whereDate('customer_contract.customer_contract_end_date','<=', Carbon::now());
                            ->orWhere(function ($select){
                                $select
                                    ->where('customer_contract.is_active',0)
                                    ->whereNull('customer_contract.customer_contract_end_date');

                            });
                    });
            } else {
                $select = $select
                    ->where('customer_contract.is_active',1)
                    ->where(function ($select){
                        $select
                            ->where('customer_contract.customer_contract_end_date','>', Carbon::now())
                            ->orWhereNull('customer_contract.customer_contract_end_date');

                    });
            }
            unset($filters['active']);
        }
        unset($filters['active']);

        if (isset($filters['nearly_expired'])){
            $select = $select
                ->where('customer_contract.customer_contract_end_date','>', Carbon::now())
                ->whereNotNull('customer_contract.customer_contract_end_date');
            unset($filters["nearly_expired"]);
        }
        return $select;
    }

    public function getListAllPagination($filters = []){
        $page    = (int) (isset($filters['page']) ? $filters['page'] : 1);
        $display = (int) (isset($filters['perpage']) ? $filters['perpage'] : PAGING_ITEM_PER_PAGE);

        $select = $this
            ->select(
                $this->table.'.*',
                'customers.full_name',
                'product_categories.category_name_vi',
                'products.month_extend'
            )
            ->join('customers','customers.customer_id','customer_contract.customer_id')
            ->join('products','products.product_id','customer_contract.product_id')
            ->join('product_categories','product_categories.product_category_id','products.product_category_id')
            ->where('customer_contract.is_active', self::IS_ACTIVE)
            ->where('customer_contract.is_deleted', self::IS_DELETE);

        if (isset($filters['search']) != "") {
            $search = $filters['search'];
            $select->where(function ($query) use ($search) {
                $query->where('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                    ->orWhere('customers.full_name', 'like', '%' . $search . '%');
            });
        }
        if (isset($filters['manager_extend_contract'])) {
            $select = $select->where(DB::raw("(DATE_FORMAT(customer_contract_end_date,'%Y-%m-%d') )"),'<=',$filters['manager_extend_contract']);
        }

        if (isset($filters['product_category_id'])){
            $select->where('product_categories.product_category_id',$filters['product_category_id']);
            unset($filters['product_category_id']);
        }

        if (isset($filters['list_extend_contract'])) {
            $select = $select->whereNotIn('customer_contract_code',$filters['list_extend_contract']);
        }
        return $select
            ->orderBy('customer_contract_end_date','ASC')
            ->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function getListExport($filters = []){
        $select = $this
            ->select(
                $this->table.'.*',
                'customers.full_name',
                'customers.birthday',
                'customers.birthday',
                'customers.ic_date',
                'customers.ic_place',
                'customers.residence_province_id',
                'province.name as province_name_contact',
                'district.name as district_name_contact',
                'customers.contact_address as contact_address_contact',
                'customers.ic_front_image',
                'customers.ic_back_image',
                "cct.withdraw_date_planning",
                'product_categories.category_name_vi'
            )
            ->join('customers','customers.customer_id','customer_contract.customer_id')
            ->join('products','products.product_id','customer_contract.product_id')
            ->join('product_categories','product_categories.product_category_id','products.product_category_id')
//            ->leftJoin("customer_contract_interest_by_time as cct", function ($join){
//                $join->on('cct.customer_contract_id', $this->table.'.customer_contract_id')
//                    ->where('cct.customer_contract_interest_by_time_id', '=', DB::raw("(select max(`customer_contract_interest_by_time_id`) from customer_contract_interest_by_time where customer_contract_id = {$this->table}.customer_contract_id)"));
//            })
            ->leftJoin("customer_contract_interest_by_month as cct", function ($join){
                $join->on('cct.customer_contract_id', $this->table.'.customer_contract_id')
                    ->where('cct.customer_contract_interest_by_month_id', '=', DB::raw("(select max(`customer_contract_interest_by_month_id`) from customer_contract_interest_by_month where customer_contract_id = {$this->table}.customer_contract_id)"));
            })
            ->leftjoin('province','province.provinceid','customers.contact_province_id')
            ->leftjoin('district','district.districtid','customers.contact_district_id');
//            ->where('customer_contract.is_deleted', self::IS_DELETE);

        if (isset($filters['time_expired'])) {
            $currentDateTime = Carbon::now();
            $month = (int)$filters["time_expired"];
            $newDateTime = Carbon::now()->addMonths($month)->format("Y-m-d");
            $select->whereDate('customer_contract_end_date','<=', $newDateTime);
            unset($filters["time_expired"]);
            $select->orderBy('customer_contract.customer_contract_end_date', 'asc');
        } else {
            $select->orderBy('customer_contract.customer_contract_id', 'desc');
        }
        if (isset($filters['search']) != "") {
            $search = $filters['search'];
            $select->where(function ($query) use ($search) {
                $query->where('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                    ->orWhere('customer_contract.customer_phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.full_name', 'like', '%' . $search . '%');
            });
            unset($filters['search']);
        }

        if (isset($filters['is_active']) && $filters['is_active'] != "") {
            $select->where($this->table.'.is_active','=', $filters['is_active']);
        }
        if (isset($filters['startTime']) != "" && isset($filters['endTime'])) {
            $select->whereBetween('customer_contract.created_at', [$filters['startTime']. ' 00:00:00', $filters['endTime']. ' 23:59:59']);
            unset($filters['startTime']);
            unset($filters['endTime']);
        }

        if (isset($filters['startTimeContract']) != "" && isset($filters['endTimeContract'])) {
            $select->whereBetween('customer_contract.customer_contract_start_date', [$filters['startTimeContract']. ' 00:00:00', $filters['endTimeContract']. ' 23:59:59']);
            unset($filters['startTimeContract']);
            unset($filters['endTimeContract']);
        }

        if (isset($filters['product_category_id'])){
            $select->where('product_categories.product_category_id',$filters['product_category_id']);
            unset($filters['product_category_id']);
        }

        if (isset($filters['id'])) {
            $select->where('customers.customer_id','=', $filters['id']);
            unset($filters['id']);
        }

        if (isset($filters['refer_id'])) {
            $select->where('customer_contract.refer_id','=', $filters['refer_id']);
            unset($filters['refer_id']);
        }
        if (isset($filters['active'])) {
            if ($filters['active'] == 1){
                $select = $select
                    ->where('customer_contract.customer_contract_end_date','<', Carbon::now())
//                    ->whereNotNull('customer_contract.customer_contract_end_date')
//                    ->whereDate('customer_contract.customer_contract_end_date','<=', Carbon::now());
                    ->orwhere(function ($select){
                        $select
                            ->where('customer_contract.is_active',0)
                            ->whereNull('customer_contract.customer_contract_end_date');

                    });
            } else {
                $select = $select
                    ->where('customer_contract.is_active',1)
                    ->where(function ($select){
                        $select
                            ->where('customer_contract.customer_contract_end_date','>', Carbon::now())
                            ->orWhereNull('customer_contract.customer_contract_end_date');

                    });
            }

            unset($filters["active"]);
        }

        if (isset($filters['nearly_expired'])){
            $select = $select
                ->where('customer_contract.customer_contract_end_date','>', Carbon::now())
                ->whereNotNull('customer_contract.customer_contract_end_date');
            unset($filters["nearly_expired"]);
        }

        return $select->get();
    }

    public function deleteCustomerContract($customer_contract_id) {
        $oSelect = $this->where('customer_contract_id',$customer_contract_id)->delete();
        return $oSelect;
    }

    public function getDetail($customer_contract_id) {
        $oSelect = $this->select(
            $this->table.".*",
//            "wit.withdraw_interest_month",
//            "it.investment_time_month",
            "cct.withdraw_date_planning",
            'staffs.full_name as staff_full_name',
            'thuongtru_pro.name as thuongtru_pro_name',
            'thuongtru_pro.type as thuongtru_pro_type',
            'thuongtru_dis.name as thuongtru_dis_name',
            'thuongtru_dis.type as thuongtru_dis_type',
            'lienhe_pro.name as lienhe_pro_name',
            'lienhe_pro.type as lienhe_pro_type',
            'lienhe_dis.name as lienhe_dis_name',
            'lienhe_dis.type as lienhe_dis_type',
            'customers.residence_address'
        )
//            ->leftJoin("withdraw_interest_time as wit",function ($join){
//                $join->on("wit.withdraw_interest_time_id","=",$this->table.".withdraw_interest_time")
//                    ->where("wit.is_actived",1)
//                    ->where("wit.is_deleted",0);
//            })
//            ->leftJoin("investment_time as it",function ($join){
//                $join->on("it.investment_time_id","=",$this->table.".investment_time")
//                    ->where("wit.is_actived",1)
//                    ->where("wit.is_deleted",0);
//            })
            ->leftJoin("customer_contract_interest_by_time as cct", function ($join){
                $join->on('cct.customer_contract_id', $this->table.'.customer_contract_id')
                    ->where('cct.customer_contract_interest_by_time_id', '=', DB::raw("(select max(`customer_contract_interest_by_time_id`) from customer_contract_interest_by_time where customer_contract_id = {$this->table}.customer_contract_id)"));
            })
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->join("customers", "customers.customer_id", "=", "{$this->table}.customer_id")
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->where($this->table.'.customer_contract_id',$customer_contract_id)->first();
        return $oSelect;
    }

    // đã đầu tư
    public function investedCustomer($id)
    {
        $select = $this
            ->select(
                $this->table.'.total_amount'
            )
            ->where('customer_contract.is_deleted', self::IS_DELETE)
            ->join('customers','customers.customer_id','customer_contract.customer_id')
            ->where('customers.customer_id', $id)
            ->get();

        return $select;
    }

    // ví tiết kiệm
    public function savingsWallet($id)
    {
        $select = $this->select(
            'ccibt.interest_total_amount'
        )
            ->where($this->table.'.customer_id', $id)
            ->join('customers', 'customers.customer_id', '=', $this->table.'.customer_id')
            ->join('customer_contract_interest_by_time as ccibt', 'ccibt.customer_contract_id', '=', $this->table.'.customer_contract_id' )
            ->join('products', 'products.product_id', '=', $this->table.'.product_id')
            ->where('products.product_category_id', 2)
            ->groupBy('ccibt.customer_contract_interest_by_time_id')
            ->orderBy('ccibt.customer_contract_interest_by_time_id', 'desc')
            ->first();

        return $select;
    }

    // Ví thưởng
    public function bonusWalletCustomer($id)
    {
        $select = $this->select(
            $this->table.'.commission'
        )
            ->where($this->table.'.refer_id', $id)
            ->get();

        return $select;
    }

// ví trái phiếu
    public function getBondTotalPrice($customer_id){
        $oResult = $this->select(
            \DB::raw("SUM(total) as sum_amount")
        )
            ->join("products as p","p.product_id","=",$this->table.".product_id")
            ->where("p.product_category_id",1)
            ->where($this->table.".customer_id",$customer_id)
            ->where($this->table.".is_active",1)
            ->get();
        return ($oResult!=null) ? $oResult[0]->sum_amount :0;
    }

    public function bondWalletCustomer($id)
    {
//        $select = $this->select(
//            'ccibt.interest_total_amount'
//        )
//            ->where($this->table.'.customer_id', $id)
//            ->join('customers', 'customers.customer_id', '=', $this->table.'.customer_id')
//            ->join('customer_contract_interest_by_time as ccibt', 'ccibt.customer_contract_id', '=', $this->table.'.customer_contract_id' )
//            ->join('products', 'products.product_id', '=', $this->table.'.product_id')
//            ->where('products.product_category_id', 1)
//            ->where('customer_contract.is_active', 1)
//            ->where('customer_contract.is_deleted', 0)
//            ->groupBy('ccibt.customer_contract_interest_by_time_id')
//            ->orderBy('ccibt.customer_contract_interest_by_time_id', 'desc')
//            ->first();
//        return $select;

        $oResult = $this->select(
            \DB::raw("SUM(total_amount) as sum_amount")
        )
            ->join("products as p","p.product_id","=",$this->table.".product_id")
            ->where("p.product_category_id",1)
            ->where($this->table.".customer_id",$id)
            ->where($this->table.".is_active",1)
            ->where($this->table.".is_deleted",0)
//            ->toSql();
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->sum_amount) :0;
    }

//    Ví tiết kiệm
    public function getSavingTotalPrice($customer_id){
        $oResult = $this->select(
            \DB::raw("SUM(total_amount) as sum_amount")
        )
            ->join("products as p","p.product_id","=",$this->table.".product_id")
            ->where("p.product_category_id",2)
            ->where($this->table.".customer_id",$customer_id)
//            ->where($this->table.".is_active",1)
//            ->where($this->table.".is_deleted",0)
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->sum_amount) :0;
    }

    public function getBondCount($customer_id){
        $oResult = $this->select(
            \DB::raw("SUM({$this->table}.quantity) as quantity")
        )
            ->join("products as p","p.product_id","=",$this->table.".product_id")
            ->where("p.product_category_id",1)
            ->where($this->table.".customer_id",$customer_id)
            ->where($this->table.".is_active",1)
            ->count();
        return ($oResult!=null) ? $oResult :0;
    }

    public function getAllContract($customer_id){
        $oResult = $this->select(
            $this->table.".customer_contract_id",$this->table.".customer_contract_code","{$this->table}.quantity"
        )
            ->where($this->table.".is_active",1)
            ->where($this->table.".is_deleted",0)
            ->where($this->table.".customer_id",$customer_id)
            ->get();
        return $oResult;
    }

    /**
     * Lấy thông tin hợp đồng
     *
     * @param $contractId
     * @return mixed
     */
    public function getItem($contractId)
    {
        return $this
            ->select(
                "{$this->table}.customer_contract_id",
                "{$this->table}.customer_id",
                "{$this->table}.customer_contract_code",
                "{$this->table}.customer_name",
                "{$this->table}.customer_phone",
                "{$this->table}.customer_email",
                "customers.birthday",
                "{$this->table}.customer_ic_no",
                "customers.ic_date",
                "customers.ic_place",
                "{$this->table}.customer_residence_address",
                "customers.contact_address",
                "{$this->table}.quantity",
                "{$this->table}.price_standard",
                "{$this->table}.total_amount",
                "{$this->table}.customer_contract_start_date",
                "{$this->table}.customer_contract_end_date",
                "{$this->table}.payment_method",
                "{$this->table}.is_active",
                "{$this->table}.by_month_total_interest",
                "{$this->table}.by_day_total_interest",
                "{$this->table}.by_time_total_interest",
                "customers.bank_account_no",
                "products.product_name_vi as product_name",
                "products.bonus_extend",
                "products.product_id",
                "products.product_category_id",
                "product_categories.category_name_vi",
                "{$this->table}.interest_rate",
                "{$this->table}.investment_time",
                "{$this->table}.withdraw_interest_time",
                "{$this->table}.month_interest",
                "{$this->table}.is_fully_withdraw",
                'staffs.full_name as staff_full_name',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'customers.residence_address',
                'orders.contract_code_extend'
            )
            ->join("customers", "customers.customer_id", "{$this->table}.customer_id")
            ->leftJoin("staffs", "staffs.staff_id", "=", "{$this->table}.staff_id")
            ->leftJoin("orders", "orders.order_id", "=", "{$this->table}.order_id")
            ->join("products", "products.product_id", "=", "{$this->table}.product_id")
            ->join("product_categories", "product_categories.product_category_id", "=", "products.product_category_id")
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->where("{$this->table}.customer_contract_id", $contractId)
            ->first();
    }

    /**
     * Lấy thông tin hợp đồng
     *
     * @param $contractId
     * @return mixed
     */
    public function getItemInterest($contractId)
    {
        return $this
            ->select(
                "{$this->table}.customer_contract_id",
                "{$this->table}.customer_id",
                "{$this->table}.customer_contract_code",
                "{$this->table}.customer_name",
                "{$this->table}.customer_phone",
                "{$this->table}.customer_email",
                "{$this->table}.customer_ic_no",
                "{$this->table}.customer_residence_address",
                "{$this->table}.quantity",
                "{$this->table}.price_standard",
                "{$this->table}.total_amount",
                "{$this->table}.customer_contract_start_date",
                "{$this->table}.customer_contract_end_date",
                "{$this->table}.payment_method",
                "{$this->table}.is_active",
                "{$this->table}.by_month_total_interest",
                "{$this->table}.by_day_total_interest",
                "{$this->table}.by_time_total_interest",
                "{$this->table}.interest_rate",
                "{$this->table}.investment_time",
                "{$this->table}.withdraw_interest_time",
                "{$this->table}.month_interest",
                "{$this->table}.is_fully_withdraw"
            )
            ->where("{$this->table}.customer_contract_id", $contractId)
            ->first();
    }

    public function getListContractChange($customer_id){
        $oSelect = $this
            ->join('customers','customers.customer_id','customer_contract.customer_id')
            ->join('products','products.product_id','customer_contract.product_id')
            ->join('product_categories','product_categories.product_category_id','products.product_category_id')
            ->where('customer_contract.customer_id',$customer_id)
//            ->where('customer_contract.is_active',1)
//            ->where('customer_contract.is_deleted',0)
            ->where('customer_contract.is_active',1)
            ->where(function ($select){
                $select
                    ->where('customer_contract.customer_contract_end_date','>', Carbon::now())
                    ->orWhereNull('customer_contract.customer_contract_end_date');

            })
            ->select($this->table.'.*')
            ->get();
        return $oSelect;
    }

    /**
     * Lấy thông tin hợp đồng
     *
     * @param $contractId
     * @return mixed
     */
    public function getItemCode($contractCode)
    {
        return $this
            ->select(
                "{$this->table}.customer_contract_id",
                "{$this->table}.customer_id",
                "{$this->table}.customer_contract_code",
                "{$this->table}.customer_name",
                "{$this->table}.customer_phone",
                "{$this->table}.customer_email",
                "customers.birthday",
                "{$this->table}.customer_ic_no",
                "customers.ic_date",
                "customers.ic_place",
                "{$this->table}.customer_residence_address",
                "customers.contact_address",
                "{$this->table}.quantity",
                "{$this->table}.price_standard",
                "{$this->table}.total_amount",
                "{$this->table}.customer_contract_start_date",
                "{$this->table}.customer_contract_end_date",
                "{$this->table}.payment_method",
                "customers.bank_account_no",
                "products.product_name_vi as product_name",
                "products.bonus_extend",
                "products.product_id",
                "products.product_category_id",
                "product_categories.category_name_vi",
                "{$this->table}.interest_rate",
                "{$this->table}.investment_time",
                "{$this->table}.withdraw_interest_time",
                "{$this->table}.month_interest"
            )
            ->join("customers", "customers.customer_id", "=", "{$this->table}.customer_id")
            ->join("products", "products.product_id", "=", "{$this->table}.product_id")
            ->join("product_categories", "product_categories.product_category_id", "=", "products.product_category_id")
            ->where("{$this->table}.customer_contract_code", $contractCode)
            ->first();
    }

    /**
     * Lấy danh sách hợp đồng
     *
     * @param $contractId
     * @return mixed
     */
    public function getAllContractUpdateLog()
    {
        return $this
            ->select(
                "{$this->table}.customer_contract_id",
                "{$this->table}.customer_id",
                "{$this->table}.customer_contract_code",
                "{$this->table}.customer_name",
                "{$this->table}.customer_phone",
                "{$this->table}.customer_email",
                "customers.birthday",
                "{$this->table}.customer_ic_no",
                "customers.ic_date",
                "customers.ic_place",
                "{$this->table}.customer_residence_address",
                "customers.contact_address",
                "{$this->table}.quantity",
                "{$this->table}.price_standard",
                "{$this->table}.total_amount",
                "{$this->table}.customer_contract_start_date",
                "{$this->table}.customer_contract_end_date",
                "{$this->table}.payment_method",
                "customers.bank_account_no",
                "products.product_name_vi as product_name",
                "{$this->table}.interest_rate",
                "{$this->table}.investment_time",
                "{$this->table}.withdraw_interest_time",
                "{$this->table}.month_interest"
            )
            ->join("customers", "customers.customer_id", "=", "{$this->table}.customer_id")
            ->join("products", "products.product_id", "=", "{$this->table}.product_id")
            ->get();
    }

    public function createCustomerContract($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function countMoneyContract($customer_id) {
        $oSelect = $this->where('customer_id',$customer_id)
            ->select(DB::raw("SUM(total_amount) as total_amount"))->first();
        return $oSelect;
    }

    /**
     * Danh sách customer contract theo customer_contract_start_date và customer_contract_end_date
     * @param array $filters
     *
     * @return mixed
     */
    public function getAll($filters = [])
    {
        $select = $this->select($this->table . '.*');
        if (isset($filters['now'])) {
            /**
             * 1. Ngày bắt đầu <= now
             * 2. kết thúc hợp đồng >= now
             */
            $select->where($this->table . '.customer_contract_start_date',
                '<=', $filters['now']);
            $select->where(function ($query) use ($filters) {
                $query->where($this->table . '.customer_contract_end_date',
                    '>=', $filters['now'])
//                    ->orWhere($this->table . '.customer_contract_end_date', '0000-00-00 00:00:00')
                    ->orWhere($this->table . '.customer_contract_end_date', null);
            });
        }
        if (isset($filters['day'])) {
            $select->whereIn(DB::raw("(DATE_FORMAT(customer_contract.customer_contract_start_date,'%d') )"),$filters['day']);
        }
        if (isset($filters['customer_contract_id'])) {
            $select->where('customer_contract_id',$filters['customer_contract_id']);
        } else {
            $select
                ->where($this->table . '.is_active', self::IS_ACTIVE)
                ->where($this->table . '.is_deleted', self::NOT_DELETE);
        }

        return $select->get();
    }

    public function getAllById($filters = [])
    {
        $select = $this->select($this->table . '.*')
            ->whereIn('customer_contract_id',$filters['customer_contract_id']);
        return $select->get();
    }

    public function countMoney($arrCustomer) {
        $oSelect = $this->whereIn('customer_id',$arrCustomer)->sum('total_amount');
        return $oSelect;
    }

    public function updateContract($id,$data){
        $oSelect = $this->where('customer_contract_id',$id)->update($data);
        return $oSelect;
    }

    public function updateContractByCode($code,$data){
        $oSelect = $this->where('customer_contract_code',$code)->update($data);
        return $oSelect;
    }

    public function updateByEndDate($time){
        $oSelect = $this->where('customer_contract_end_date','<',$time)->update(['is_deleted' => 1]);
        return $oSelect;
    }

//    Ví thưởng
    public function getTotalCommission($customer_id){
        $oResult = $this->select(
            \DB::raw("SUM({$this->table}.commission) as commission")
        )
            ->join("products as p","p.product_id","=",$this->table.".product_id")
            ->join("orders as o","o.order_id","=",$this->table.".order_id")

            ->where("o.process_status","paysuccess")
            ->where($this->table.".is_active",1)
            ->where($this->table.".is_deleted",0)
            ->where($this->table.".refer_id",$customer_id)
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->commission) :0;
    }

    public function getContractByCustomer($customerId)
    {
        return $this
            ->select(
                "{$this->table}.customer_contract_id",
                "{$this->table}.customer_contract_code",
                "{$this->table}.total",
                "{$this->table}.total_amount",
                "{$this->table}.term_time_type",
                "{$this->table}.payment_method",
                "{$this->table}.customer_contract_start_date",
                "{$this->table}.customer_contract_end_date",
                "withdraw_request.withdraw_request_amount",
                "withdraw_request.withdraw_request_status"
            )
            ->join("products", "products.product_id", "=", "{$this->table}.product_id")
            ->join("product_categories", "product_categories.product_category_id", "=", "products.product_category_id")
            ->leftJoin("withdraw_request", "withdraw_request.customer_contract_id", "=", "{$this->table}.customer_contract_id")
            ->where("product_categories.product_category_id", self::BOND)
            ->where("{$this->table}.customer_id", $customerId)
            ->where($this->table.".is_active",1)
            ->where($this->table.".is_deleted",0)
//            ->whereDate("{$this->table}.customer_contract_end_date", ">=", Carbon::now()->format('Y-m-d'))
            ->groupBy($this->table.'.customer_contract_id')
            ->get();
    }

    public function getDetailContract($customer_contract_id){
        $oSelect = $this
            ->select(
                "{$this->table}.*",
                "p.bonus_rate",
                "p.product_category_id",
                "p.withdraw_min_time"
            )
            -> leftJoin('products as p', "p.product_id", "{$this->table}.product_id")
            ->where('customer_contract_id',$customer_contract_id)->first();
        return $oSelect;
    }

    public function totalBonusCustomer($customer_id){
        $oSelect = $this->where('refer_id',$customer_id)
            ->select(DB::raw('SUM(commission) as total_commission'))->first();
        return $oSelect;
    }

    public function getContractWithCustomerId($customer_id){
        $oSelect = $this->where('customer_id',$customer_id)->get();
        return $oSelect;
    }

    public function getDetailByCode($code){
        $oSelect = $this->where('customer_contract_code',$code)->first();
        return $oSelect;
    }

    public function listContractByRefer($refer_id,$month){
        $oSelect = $this
            ->where('refer_id',$refer_id)
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m') )"),$month)->get();
        return $oSelect;
    }

    public function getBonusByMonthCount($refer_id,$month,$type){
        $oSelect = $this
            ->where('refer_id',$refer_id);
        if ($type == 'before'){
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m') )"),'<',$month);
        } else {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m') )"),$month);
        }
        $oSelect = $oSelect->select(DB::raw("SUM(commission) as total"))->first();
        return $oSelect;
    }

    public function getListContractByMonth($customer_id,$month,$type){
        $oSelect = $this
            ->join('products','products.product_id','customer_contract.product_id')
            ->where('customer_id',$customer_id);
        if ($type == 'before'){
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract.created_at,'%Y-%m') )"),'<',$month);
        } else {
            $oSelect = $oSelect->where(DB::raw("(DATE_FORMAT(customer_contract.created_at,'%Y-%m') )"),$month);
        }
        $oSelect = $oSelect->groupBy('products.product_category_id')->select(DB::raw("SUM(customer_contract.total_amount) as total"),'products.product_category_id')->get();
        return $oSelect;
    }

    public function getContractByCode($customerContractCode)
    {
        return $this->where('customer_contract_code',$customerContractCode)->first();

    }

//    Danh sách hợp đồng sắp hết hạn
    public function listContractWaringExtend($day){
        $oSelect = $this
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->whereNotNull('customer_contract.customer_contract_end_date')
            ->where(DB::raw("(DATE_FORMAT(customer_contract.customer_contract_end_date,'%Y-%m-%d') )"),'>=',Carbon::now()->format('Y-m-d'))
            ->where(DB::raw("(DATE_FORMAT(customer_contract.customer_contract_end_date,'%Y-%m-%d') )"),'<=',$day)
            ->select($this->table.'.*','staffs.full_name as staff_full_name')
            ->get();
        return $oSelect;
    }

    public function listContractWaringExtendSMS($day,$customerId){
        $oSelect = $this
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->whereNotNull('customer_contract.customer_contract_end_date')
            ->where(DB::raw("(DATE_FORMAT(customer_contract.customer_contract_end_date,'%Y-%m-%d') )"),'>=',Carbon::now()->format('Y-m-d'))
            ->where(DB::raw("(DATE_FORMAT(customer_contract.customer_contract_end_date,'%Y-%m-%d') )"),$day)
            ->whereNotNull('customer_contract.customer_contract_end_date')
            ->where('customer_contract.customer_id',$customerId)
            ->where('customer_contract.is_deleted',0)
            ->where('customer_contract.is_active',1)
            ->select($this->table.'.*','staffs.full_name as staff_full_name')
            ->get();
        return $oSelect;
    }

    public function getListContractByCustomer($customer_id){
        $oSelect = $this
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->join('products','products.product_id',$this->table.'.product_id')
            ->leftJoin('customer_contract_interest_by_month',function ($join){
                $join->on('customer_contract_interest_by_month.customer_contract_id',$this->table.'.customer_contract_id')
//                    ->where('customer_contract_interest_by_month.customer_contract_interest_by_month_id', '=', DB::raw("(select max(`customer_contract_interest_by_month_id`) from customer_contract_interest_by_month where customer_contract_id = customer_contract.customer_contract_id and DATE_FORMAT(customer_contract_interest_by_month.withdraw_date_planning,'%Y-%m') = ".Carbon::now()->format('Y-m')." ORDER BY customer_contract_interest_by_month.customer_contract_interest_by_month_id DESC )"));
                    ->where('customer_contract_interest_by_month.customer_contract_interest_by_month_id', '=', DB::raw("(select max(`customer_contract_interest_by_month_id`) from customer_contract_interest_by_month where customer_contract_id = customer_contract.customer_contract_id and customer_contract_interest_by_month.withdraw_date_planning <= NOW() order by customer_contract_interest_by_month_id desc)"));
//                    ->whereDate('customer_contract_interest_by_month.withdraw_date_planning', "<=", Carbon::now()->format('Y-m-d'));
            })
            ->leftJoin('customer_contract_interest_by_time',function ($join){
                $join->on('customer_contract_interest_by_time.customer_contract_id',$this->table.'.customer_contract_id')
//                    ->where('customer_contract_interest_by_time.customer_contract_interest_by_time_id', '=', DB::raw("(select max(`customer_contract_interest_by_time_id`) from customer_contract_interest_by_time where customer_contract_id = customer_contract.customer_contract_id and DATE_FORMAT(customer_contract_interest_by_time.withdraw_date_planning,'%Y-%m') = ".Carbon::now()->format('Y-m')." ORDER BY customer_contract_interest_by_time.customer_contract_interest_by_time_id DESC)"));
                    ->where('customer_contract_interest_by_time.customer_contract_interest_by_time_id', '=', DB::raw("(select max(`customer_contract_interest_by_time_id`) from customer_contract_interest_by_time where customer_contract_id = customer_contract.customer_contract_id AND customer_contract_interest_by_time.withdraw_date_planning <= NOW() order by customer_contract_interest_by_time_id desc)"));
//                    ->whereDate('customer_contract_interest_by_time.withdraw_date_planning', "<=", Carbon::now()->format('Y-m-d'));
            })
            ->where($this->table.".customer_id",$customer_id)
//            ->where($this->table.".is_deleted",0)
//            ->where($this->table.".is_active",1)
//            ->where(function ($oSelect) {
//                $oSelect->where("{$this->table}.customer_contract_end_date", ">", Carbon::now())
//                    ->orWhereNull("{$this->table}.customer_contract_end_date");
//            })
//            ->where('customer_contract.is_active',1)
            ->where("{$this->table}.is_deleted", 0)

            ->where(function ($select){
                $select
                    ->where('customer_contract.customer_contract_end_date','>', Carbon::now())
                    ->orWhereNull('customer_contract.customer_contract_end_date');

            })
//            ->where('customer_contract_interest_by_time.interest_banlance_amount','<>',0)
//            ->whereNotNull('customer_contract_interest_by_time.interest_banlance_amount')
            ->select(
                $this->table.'.*',
                'staffs.full_name as staff_full_name',
                'products.product_category_id',
                'customer_contract_interest_by_month.customer_contract_interest_by_month_id',
                'customer_contract_interest_by_month.interest_amount as month_interest_amount',
                'customer_contract_interest_by_time.interest_banlance_amount',
                'customer_contract_interest_by_time.withdraw_date_planning',
                'customer_contract_interest_by_month.interest_total_amount as interest_total_amount_month',
                'customer_contract_interest_by_month.withdraw_date_planning as withdraw_date_planning_month'
            )
            ->get();
        return $oSelect;

    }

    public function getListContractByCustomerStatement($customer_id) {
        $oSelect = $this
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->leftJoin('products','products.product_id',$this->table.'.product_id')
            ->where($this->table.".customer_id",$customer_id)
            ->where($this->table.'.is_deleted',0)
            ->select($this->table.'.*','staffs.full_name as staff_full_name')
            ->orderBy($this->table.'.customer_contract_id','DESC')
            ->get();
        return $oSelect;
    }

    public function getListContractByCustomerStatementByMonth($customer_id,$month) {
        $oSelect = $this
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->leftJoin('products','products.product_id',$this->table.'.product_id')
            ->leftJoin('customer_contract_interest_by_month','customer_contract_interest_by_month.customer_contract_id',$this->table.'.customer_contract_id')
            ->where($this->table.".customer_id",$customer_id)
            ->where(DB::raw("(DATE_FORMAT(customer_contract_interest_by_month.created_at,'%Y-%m') )"),$month)
            ->select(
                $this->table.'.*',
                'staffs.full_name as staff_full_name',
                'customer_contract_interest_by_month.interest_amount'
            )
            ->orderBy($this->table.'.customer_contract_id','DESC')
            ->get();
        return $oSelect;
    }

    public function endContract($filter = []){
        $oSelect = $this->where($filter)->update(["is_deleted" => 1]);
        return $oSelect;

    }

    public function getAllContractBonus() {
        $oSelect = $this->get();
        return $oSelect;
    }

    public function getAllChart($input){

        $oSelect = $this->select(
            DB::raw('DATE_FORMAT(receipt_details.created_at, "%m-%Y") as month'),
            DB::raw("SUM(receipt_details.amount) as money")
        )
            ->join('receipts', function($join){
                $join->on('receipts.object_id', '=', $this->table.'.order_id')
                    ->on('receipts.object_type', '=', DB::raw('"order"'));
            })
            ->join('receipt_details', function($join){
                $join->on('receipt_details.receipt_id', '=', 'receipts.receipt_id');
            })
            ->join('customers', function ($join){
                $join->on('customers.customer_id', '=', 'customer_contract.customer_id')
                ->on('customers.is_test', '<>', DB::raw('"1"'));
            })
            ->orderBy($this->table.'.created_at','ASC')
            ->groupBy('month');

        $start = Carbon::createFromFormat('m/Y',$input['month_start'])
            ->format('Y-m-01 00:00:00');

        $end = Carbon::createFromFormat('m/Y',$input['month_end'])
            -> endOfMonth()->format('Y-m-d 23:59:59');

        $oSelect->where('receipt_details.created_at','>=',$start)
            ->where('receipt_details.created_at','<=',$end);

        if ($input['staff_id'] == 0 || $input['staff_id'] == null) {
            $oSelect = $oSelect->whereNull('customer_contract.staff_id');
        } else {
            $oSelect = $oSelect->where('customer_contract.staff_id',$input['staff_id']);
        }

        return $oSelect->get()->toArray();
    }

    public function getListContractChangeInfo($customer_id){
        return $this
            ->select(
                "{$this->table}.customer_contract_id",
                "{$this->table}.customer_contract_code",
                "{$this->table}.total",
                "{$this->table}.total_amount",
                "{$this->table}.term_time_type",
                "{$this->table}.payment_method",
                "{$this->table}.customer_contract_start_date",
                "{$this->table}.customer_contract_end_date",
                "withdraw_request.withdraw_request_amount",
                "withdraw_request.withdraw_request_status"
            )
            ->where("{$this->table}.customer_id", $customer_id)
            ->where($this->table.".is_active",1)
            ->where($this->table.".is_deleted",0)
            ->get();
    }

    public function checkContract($customer_contract_id){
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
            ->where('is_active',1)
            ->where('is_deleted',0)
            ->first();
        return $oSelect;
    }

    public function checkContractByCustomerId($customerId){
        $oSelect = $this->where('customer_id',$customerId)->get();
        return $oSelect;
    }

    public function getListSumSale($start,$end,$page = null,$display = null){
        $oSelect = $this
            ->select(
                'staffs.full_name as name',
                DB::raw("SUM(receipt_details.amount) as money")
            )
            ->join('receipts', function($join){
                $join->on('receipts.object_id', '=', $this->table.'.order_id')
                    ->on('receipts.object_type', '=', DB::raw('"order"'));
            })
            ->join('receipt_details', function($join){
                $join->on('receipt_details.receipt_id', '=', 'receipts.receipt_id');
            })
            ->join('customers', function ($join){
                $join->on('customers.customer_id', '=', 'customer_contract.customer_id')
                    ->on('customers.is_test', '<>', DB::raw('"1"'));
            })
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->whereDate($this->table.'.created_at','>=',$start)
            ->whereDate($this->table.'.created_at','<=',$end)
            ->orderby('money', 'desc')
            ->groupBy($this->table.'.staff_id');
        if ($page == null) {
            return $oSelect->get();
        } else {
            return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
        }



    }
    public function getSumSale($start,$end,$page = null,$display = null){
        $oSelect = $this
            ->select(
                DB::raw("SUM(receipt_details.amount) as money")
            )
            ->join('receipts', function($join){
                $join->on('receipts.object_id', '=', $this->table.'.order_id')
                ->on('receipts.object_type', '=', DB::raw('"order"'));
            })
            ->join('receipt_details', function($join){
                $join->on('receipt_details.receipt_id', '=', 'receipts.receipt_id');
            })
            ->join('customers', function ($join){
                $join->on('customers.customer_id', '=', 'customer_contract.customer_id')
                    ->on('customers.is_test', '<>', DB::raw('"1"'));
            })
            ->leftJoin('staffs','staffs.staff_id',$this->table.'.staff_id')
            ->whereDate($this->table.'.created_at','>=',$start)
            ->whereDate($this->table.'.created_at','<=',$end)
//            ->groupBy($this->table.'.staff_id')
            ->first();
        return $oSelect;

    }

    public function getListSumSaleGroup($start,$end,$page = null,$display = null){
        $oSelect = $this
            ->select(
                'group_staff.group_name as name',
                DB::raw("SUM(receipt_details.amount) as money")
            )
            ->join('receipts', function($join){
                $join->on('receipts.object_id', '=', $this->table.'.order_id')
                    ->on('receipts.object_type', '=', DB::raw('"order"'));
            })
            ->join('receipt_details', function($join){
                $join->on('receipt_details.receipt_id', '=', 'receipts.receipt_id');
            })
            ->join('customers', function ($join){
                $join->on('customers.customer_id', '=', 'customer_contract.customer_id')
                    ->on('customers.is_test', '<>', DB::raw('"1"'));
            })
            ->leftJoin('group_staff_map_staff','group_staff_map_staff.staff_id',$this->table.'.staff_id')
            ->leftJoin('group_staff','group_staff.group_staff_id','group_staff_map_staff.group_staff_id')
            ->whereDate($this->table.'.created_at','>=',$start)
            ->whereDate($this->table.'.created_at','<=',$end)
            ->orderby('money', 'desc')
            ->groupBy('group_staff.group_staff_id');
        if ($page == null) {
            return $oSelect->get();
        } else {
            return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
        }
    }

    public function getSumSaleGroup($start,$end,$page = null,$display = null){
        $oSelect = $this
            ->select(
                DB::raw("SUM(receipt_details.amount) as money")
            )
            ->join('receipts', function($join){
                $join->on('receipts.object_id', '=', $this->table.'.order_id')
                    ->on('receipts.object_type', '=', DB::raw('"order"'));
            })
            ->join('receipt_details', function($join){
                $join->on('receipt_details.receipt_id', '=', 'receipts.receipt_id');
            })
            ->join('customers', function ($join){
                $join->on('customers.customer_id', '=', 'customer_contract.customer_id')
                    ->on('customers.is_test', '<>', DB::raw('"1"'));
            })
            ->leftJoin('group_staff_map_staff','group_staff_map_staff.staff_id',$this->table.'.staff_id')
            ->leftJoin('group_staff','group_staff.group_staff_id','group_staff_map_staff.group_staff_id')
            ->whereDate($this->table.'.created_at','>=',$start)
            ->whereDate($this->table.'.created_at','<=',$end)
            ->first();
        return $oSelect;
    }


}
//