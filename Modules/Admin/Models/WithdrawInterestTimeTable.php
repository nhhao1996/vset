<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class WithdrawInterestTimeTable extends Model
{
    use ListTableTrait;
    protected $table="withdraw_interest_time";
    protected $primaryKey="withdraw_interest_time_id";

    protected $fillable=[
        'withdraw_interest_time_id',
        'withdraw_interest_month',
        'is_deleted',
        'is_actived',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    const IS_DELETED = 0;
    const IS_ACTIVED = 1;

    public function getAll() {
        $oSelect = $this
            ->where('is_deleted',self::IS_DELETED)
            ->where('is_actived',self::IS_ACTIVED)
            ->get();
        return $oSelect;
    }
}