<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class GroupCustomerMapCustomerTable extends Model
{
    use ListTableTrait;
    protected $table = "group_customer_map_customer";
    protected $primaryKey = "group_customer_map_customer_id";
    public $timestamps = false;
    
    protected $fillable = [
        'group_customer_map_customer_id',
        'group_customer_id',
        'customer_id',
    ];

    /**
     * Thêm 1 lần nhiều record
     * @param $data
     * @return mixed
     */
    public function addInsert($data)
    {
        $this->insert($data);
    }

    public function remove($groupCustomerId)
    {
        $this->where('group_customer_id', $groupCustomerId)->delete();
    }

    public function getByGroup($groupId)
    {
        $select = $this->where('group_customer_id', $groupId)->get();
        return $select;
    }

    public function getListCustomer($staff_id){
        $oSelect = $this
            ->join('group_customer','group_customer.group_customer_id',$this->table.'.group_customer_id')
            ->join('group_staff_map_customer','group_staff_map_customer.obj_id','group_customer.group_customer_id')
            ->join('group_staff','group_staff.group_staff_id','group_staff_map_customer.group_staff_id')
//            ->join('group_staff_map_staff','group_staff_map_staff.group_staff_id','group_staff_map_customer.group_staff_id')
            ->where('group_customer.is_active',1)
            ->where('group_customer.is_deleted',0)
            ->where('group_staff.is_active',1)
            ->where('group_staff.is_deleted',0)
//            ->where('group_staff_map_staff.staff_id',$staff_id)
            ->select('group_customer_map_customer.*')
            ->get();
        return $oSelect;
    }

    public function getListCustomerId($group_customer_id = null){
        $oSelect = $this;
        if ($group_customer_id != null){
            $oSelect = $oSelect->where('group_customer_id','<>',$group_customer_id);
        }
        $oSelect = $oSelect->get();
        return $oSelect;
    }

    public function getCustomerByGroupArr(){
        $oSelect = $this
            ->join('group_customer','group_customer.group_customer_id',$this->table.'.group_customer_id')
            ->where('group_customer.is_deleted',0)
//            ->whereIn($this->table.'.group_customer_id',$arrGroup)
            ->get()->toArray();
        return $oSelect;
    }

    public function deleteCustomer($id){
        $oSelect = $this->where('group_customer_id',$id)->delete();
        return $oSelect;
    }

    public function deleteCustomerById($customerId) {
        $oSelect = $this->where('customer_id',$customerId)->delete();
        return $oSelect;
    }
}