<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/21/2019
 * Time: 11:43 PM
 */

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class SmsMessageTable extends Model
{
    protected $table = "sms_message";
    protected $primaryKey = "sms_message_id";
//    public $timestamps = false;
    protected $fillable
        = [

            "sms_message_id"
            ,"title"
            ,"key"
            ,"message"
            ,"created_at"
            ,"created_by"
        ];
    public function getMessageByKey($key){
        return $this->select()->where("key", $key)->first()->message;
    }


}