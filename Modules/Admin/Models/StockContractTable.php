<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockContractTable extends Model
{
    use ListTableTrait;
    protected $table = 'stock_contract';
    protected $primaryKey = 'stock_contract_id';
    protected $fillable = [
        "stock_contract_id"
        ,"stock_contract_code"
        ,"customer_id"
        ,"quantity"
        ,"customer_name"
        ,"customer_phone"
        ,"customer_email"
        ,"customer_residence_address"
        ,"customer_ic_no"
        ,"is_active"
        ,"is_deleted"
        ,"created_by"
        ,"updated_by"
        ,"created_at"
        ,"updated_at"
    ];

    /**
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter=[]){

        $select = $this
            ->select(
                "{$this->table}.stock_contract_id"
                ,"{$this->table}.stock_contract_code"
                ,"{$this->table}.customer_id"
                ,DB::raw("IFNULL(SC.stock,0) as quantity")
                ,"{$this->table}.customer_name"
                ,"{$this->table}.customer_phone"
                ,"{$this->table}.customer_email"
                ,"C.residence_address"
                ,"{$this->table}.customer_ic_no"
                ,"{$this->table}.is_active"
                ,"{$this->table}.is_deleted"
                ,"{$this->table}.created_by"
                ,"{$this->table}.updated_by"
                ,"{$this->table}.created_at"
                ,"{$this->table}.updated_at"
                ,"SC.wallet_stock_money"
                ,"SC.wallet_dividend_money"
                ,DB::raw("CONCAT(C.contact_address,', ',D.type,' ',D.name,', ',P.type,' ',P.name) as contact_address")
            )
            ->leftJoin("stock_customer as SC", "SC.customer_id", "{$this->table}.customer_id")
            ->leftJoin("customers as C", "C.customer_id", "{$this->table}.customer_id")
            ->leftJoin("district as D", "D.districtid","C.contact_district_id")
            ->leftJoin("province as P","P.provinceid","C.contact_province_id")
            ->orderBy("{$this->table}.{$this->primaryKey}",'DESC');
        if(isset($filter['search'])){
            $search = $filter['search'];
            $select = $select
                ->where("{$this->table}.stock_contract_code",'like',"%{$search}%")
                ->orWhere("{$this->table}.customer_name",'like',"%{$search}%");
        }
        if(isset($filter['is_active'])){
            $select = $select->where("{$this->table}.is_active",$filter["is_active"]);
            unset($filter['is_active']);
        }
        if (isset($filter['startTime'])) {
            $select->whereBetween($this->table . '.created_at', [$filter['startTime']. ' 00:00:00', $filter['endTime']. ' 23:59:59']);
            unset($filter['startTime']);
            unset($filter['endTime']);
        }
        if(isset($filter['stock_contract_id'])){
            $select->where("{$this->table}.stock_contract_id",$filter['stock_contract_id']);
            unset($filter['stock_contract_id']);
        }
        return $select;
    }
    /**
     * @param array $filter
     * @return mixed
     */
    public function _getListExport(&$filter=[]){

        $select = $this
            ->select(
                "{$this->table}.stock_contract_id"
                ,"{$this->table}.stock_contract_code"
                ,"{$this->table}.customer_id"
                ,"{$this->table}.quantity"
                ,"{$this->table}.customer_name"
                ,"{$this->table}.customer_phone"
                ,"{$this->table}.customer_email"
                ,DB::raw("CONCAT(C.contact_address,', ',D.type,' ',D.name,', ',P.type,' ',P.name) as customer_contact_address")
                ,"{$this->table}.customer_ic_no"
                ,"{$this->table}.is_active"
                ,"{$this->table}.is_deleted"
                ,"{$this->table}.created_by"
                ,"{$this->table}.updated_by"
                ,"{$this->table}.created_at"
                ,"{$this->table}.updated_at"
                ,"SC.wallet_stock_money"
                ,"SC.wallet_dividend_money"
            )
            ->leftJoin("stock_customer as SC", "SC.customer_id", "{$this->table}.customer_id")
            ->leftJoin("customers as C", "C.customer_id", "{$this->table}.customer_id")
            ->leftJoin("district as D", "D.districtid","C.contact_district_id")
            ->leftJoin("province as P","P.provinceid","C.contact_province_id")
            ->orderBy("{$this->table}.{$this->primaryKey}",'DESC');
        if(isset($filter['search'])){
            $search = $filter['search'];
            $select = $select
                ->where("{$this->table}.stock_contract_code",'like',"%{$search}%")
                ->orWhere("{$this->table}.customer_name",'like',"%{$search}%");
        }
        if(isset($filter['is_active'])){
            $select = $select->where("{$this->table}.is_active",$filter["is_active"]);
            unset($filter['is_active']);
        }
        if (isset($filter['startTime'])) {
            $select->whereBetween($this->table . '.created_at', [$filter['startTime']. ' 00:00:00', $filter['endTime']. ' 23:59:59']);
            unset($filter['startTime']);
            unset($filter['endTime']);
        }
        return $select->get();
    }
    public function detail($id){
        return $this->select(
            "{$this->table}.stock_contract_id"
            ,"{$this->table}.stock_contract_code"
            ,"{$this->table}.customer_id"
            ,"{$this->table}.quantity"
            ,"{$this->table}.customer_name"
            ,"c.phone"
            ,"c.email"
            ,"{$this->table}.customer_residence_address"
            ,"{$this->table}.customer_ic_no"
            ,"{$this->table}.is_active"
            ,"{$this->table}.is_deleted"
            ,"{$this->table}.created_by"
            ,"{$this->table}.updated_by"
            ,"{$this->table}.created_at"
            ,"{$this->table}.updated_at"
            ,"c.contact_address"
        )
            ->leftJoin("customers as c","c.customer_id", "{$this->table}.customer_id")
            ->where($this->primaryKey,$id)->first();
    }
    public function getLastStockContract(){
        return $this
            ->select()
            ->orderBy($this->primaryKey,"DESC")
            ->count();
    }
    public function add($data){
        return $this->create($data);
    }
    public function edit($id, $data){
        return $this->select()->where($this->primaryKey,$id)->first()->update($data);
    }
    public function getStockContractByCustomer($customerId){
        return $this->select()->where("customer_id", $customerId)->first();
    }
}