<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class StockBonusPaymentMethodTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_bonus_payment_method";
    protected $primaryKey = "stock_bonus_payment_method_id";
    public $timestamps = false;

    protected $fillable = [
        "stock_bonus_payment_method_id"
        ,"cash_rate"
        ,"transfer_rate"
        ,"stock_rate"
        ,"dividend_rate"
        ,"created_at"
        ,"created_by"
        ,"updated_at"
        ,"updated_by"
    ];


    /**
     * Lấy chi tiết stock_bonus_payment
     *
     * @param $stockId
     * @return mixed
     */
    public function detail(){
        $select = $this->first();
        if($select == null) $select = new StockBonusPaymentMethodTable();
        return $select;
    }
    public function add($data){
        return $this->insert($data);
    }

    /**
     * Updatestock_bonus
     *
     * @param $stock_bonus_id
     * @return mixed
     */
    public function edit(array $data=[]){
        return $this->first()->update($data);
    }
    public function remove($id){
        return $this->where($this->primaryKey,$id)->first()->delete();
    }
    public function getRateWithIdMethod($id)
    {
        $data = $this->select(
            DB::raw("(CASE 
                WHEN {$id} = 4 THEN cash_rate
                WHEN {$id} = 1 THEN transfer_rate
                WHEN {$id} = 5 THEN stock_rate
                WHEN {$id} = 6 THEN dividend_rate
                END) as rate")
        )->first();
        return $data;
    }


}