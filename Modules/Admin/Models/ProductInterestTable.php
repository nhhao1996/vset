<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ProductInterestTable extends Model
{
    use ListTableTrait;
    protected $table = 'product_interest';
    protected $primaryKey = 'product_interest_id';

    protected $fillable = [
        'product_interest_id',
        'product_id',
        'term_time_type',
        'investment_time_id',
        'withdraw_interest_time_id',
        'interest_rate',
        'commission_rate',
        'month_interest',
        'total_interest',
        'is_default_display',
        'is_deleted',
        'is_actived',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];

    const NOT_TERM = 0;
    const IS_TERM = 1;
    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;

    public function getListInterestRate($filter) {

        $page    = (int) ($filter['page'] ? $filter['page'] : 1);
        $display = 10;

        $oSelect = $this
            ->leftJoin('investment_time','investment_time.investment_time_id','product_interest.investment_time_id')
            ->leftJoin('withdraw_interest_time','withdraw_interest_time.withdraw_interest_time_id','product_interest.withdraw_interest_time_id')
            ->where('product_id',$filter['id'])
            ->orderBy('product_interest.product_interest_id','DESC')
            ->select(
                'product_interest.*',
                'investment_time.investment_time_month',
                'withdraw_interest_time.withdraw_interest_month'
            );

        if ($display > 10) {
            $page = intval(count($oSelect->get()) / $display);
        }
        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function removeProductInterest($id) {
        $oSelect = $this->where('product_interest_id',$id)->delete();
        return $oSelect;
    }

    public function checkProductInterest($data) {
        $oSelect = $this
            ->where('product_id',$data['product_id'])
            ->where('investment_time_id',$data['investment_time_id']);
        if (isset($data['product_interest_id'])) {
            $oSelect = $oSelect->where('product_interest_id','<>',$data['product_interest_id']);
        }
        $oSelect = $oSelect->get();
        return $oSelect;
    }

    public function createWithdrawInterest($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function updateWithdrawInterestDisplay($product_id) {
        $oSelect = $this->where('product_id',$product_id)->update(['is_default_display' => 0]);
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelect = $this->where('product_interest_id',$id)->first();
        return $oSelect;
    }

    public function updateWithdrawInterest($id,$data) {
        $oSelect = $this->where('product_interest_id',$id)->update($data);
        return $oSelect;
    }

    public function getListByProductId($product_id) {
        $oSelect = $this->where('product_id',$product_id)->get();
        return $oSelect;
    }

    public function getInterestRate($product_id,$interest_id,$withdraw_id)
    {
        $oSelect = $this
            ->where('product_id',$product_id)
            ->where('investment_time_id',$interest_id)
            ->where('withdraw_interest_time_id',$withdraw_id)
            ->first();
        return $oSelect;
    }

    public function getInterest($categoryId, $productId, $interestTime, $withdrawTime)
    {
        $ds = $this
            ->select(
                "{$this->table}.product_id",
                "{$this->table}.term_time_type",
                "investment_time.product_category_id",
                "investment_time.investment_time_month",
                "withdraw_interest_time.withdraw_interest_month",
                "{$this->table}.interest_rate",
                "{$this->table}.commission_rate",
                "{$this->table}.month_interest",
                "{$this->table}.total_interest"
            )
            ->leftJoin("investment_time", "investment_time.investment_time_id", "=", "{$this->table}.investment_time_id")
            ->leftJoin("withdraw_interest_time", "withdraw_interest_time.withdraw_interest_time_id", "=", "{$this->table}.withdraw_interest_time_id")
            ->where("{$this->table}.product_id", $productId)
            ->where("withdraw_interest_time.withdraw_interest_time_id", $withdrawTime)
            ->where("{$this->table}.is_actived", self::IS_ACTIVE)
            ->where("withdraw_interest_time.is_actived", self::IS_ACTIVE)
            ->where("withdraw_interest_time.is_deleted", self::NOT_DELETE);

        if ($interestTime != 0) {
            $ds->where("{$this->table}.term_time_type", self::IS_TERM)
                ->where("investment_time.product_category_id", $categoryId)
                ->where("investment_time.investment_time_id", $interestTime)
                ->where("investment_time.is_actived", self::IS_ACTIVE)
                ->where("investment_time.is_deleted", self::NOT_DELETE);
        } else {
            $ds->where("{$this->table}.term_time_type", self::NOT_TERM);
        }

        return $ds->first();
    }

    public function createProductInterest($data){
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function deleteByInvestmentTime($investmentTime){
        $oSelect = $this->where('investment_time_id',$investmentTime)->delete();
        return $oSelect;
    }

    public function getIsDisplay($investment_time_id){
        $oSelect = $this->where('investment_time_id',$investment_time_id)->where('is_default_display',1)->get();
        return $oSelect;
    }

    public function updateByProduct($product_id,$data){
        $oSelect = $this
            ->where('product_id',$product_id)
            ->first()
            ->update($data);
        return $oSelect;
    }


}