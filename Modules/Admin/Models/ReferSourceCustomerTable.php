<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ReferSourceCustomerTable extends Model
{
    use ListTableTrait;
    protected $table = "refer_source_customer";
    protected $primaryKey = "refer_source_customer_id";
    protected $fillable = [
        'refer_source_customer_id',
        'refer_source_id',
        'type',
        'type_bonus',
        'money',
        'is_active',
        'start',
        'end',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    public function getListAll(){
        $oSelect = $this->get();
        return $oSelect;
    }

    public function getConfig($id){
        $oSelect = $this
            ->where('refer_source_id',$id)
            ->get();
        return $oSelect;
    }

    public function checkReferCustomer($refer_source_id,$type){
        $oSelect = $this
            ->where('refer_source_id',$refer_source_id)
            ->where('type',$type)
            ->first();
        return $oSelect;
    }

    public function updateConfig($id,$data) {
        $oSelect = $this->where('refer_source_customer_id',$id)->update(collect($data)->toArray());
        return $oSelect;
    }

    public function createConfig($data){
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

}