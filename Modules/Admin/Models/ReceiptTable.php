<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 12/5/2018
 * Time: 2:37 PM
 */

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class ReceiptTable extends Model
{
    use ListTableTrait;
    protected $table = "receipts";
    protected $primaryKey = "receipt_id";
    protected $fillable = [
        "receipt_id",
        "receipt_code",
        "customer_id",
        "staff_id",
        "order_id",
        "total_money",
        "voucher_code",
        "status",
        "discount",
        "custom_discount",
        "is_discount",
        "amount",
        "amount_paid",
        "amount_return",
        "note",
        "created_by",
        "updated_by",
        "created_at",
        "updated_at",
        "object_id",
        "object_type"
    ];

    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->receipt_id;
    }

    public function createReceipt(array $data)
    {
        $add = $this->insertGetId($data);
        return $add;
    }

    public function getItem($id)
    {
        $ds = $this->leftJoin("staffs", "staffs.staff_id", "=", "receipts.created_by")
            ->select(
                "receipts.receipt_id",
                "receipts.receipt_code",
                "receipts.customer_id",
                "receipts.order_id",
                "receipts.amount",
                "receipts.amount_paid",
                "receipts.created_at",
                "receipts.created_by",
                "receipts.amount_return",
                "staffs.full_name"
            )->where("receipts.order_id", $id)->first();
        return $ds;
    }

    public function edit(array $data, $id)
    {
        return $this->where("receipt_id", $id)->update($data);
    }

    public function _getList(&$filters = [])
    {
        $select = $this->select(
            "orders.order_code as order_code",
            "customers.full_name as customer_name",
            "staffs.full_name as staff_name",
            "receipts.created_at as created_at",
            "receipts.receipt_id",
            "receipts.amount",
            "receipts.amount_paid",
            "receipts.note"
        )
            ->leftJoin("staffs", "staffs.staff_id", "=", "receipts.staff_id")
            ->leftJoin("orders", "orders.order_id", "=", "receipts.order_id")
            ->leftJoin("customers", "customers.customer_id", "=", "receipts.customer_id")
            ->whereIn("receipts.status", ["part-paid", "unpaid"]);
        if (
            isset($filters["search_keyword"])
            && $filters["search_keyword"] != ""
        ) {
            $keyword = $filters["search_keyword"];
            $select->where(function ($query) use ($keyword) {
                $query->where("customers.full_name", "like",
                    "%" . $keyword . "%")
                    ->orWhere("orders.order_code", "like",
                        "%" . $keyword . "%");
            });
            unset($filters["search_keyword"]);
        }
        return $select;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReceipt($id)
    {
        $ds = $this
            ->leftJoin("staffs", "staffs.staff_id", "=", "receipts.staff_id")
            ->select(
                "receipts.receipt_id",
                "receipts.receipt_code",
                "receipts.customer_id",
                "receipts.object_id",
                "receipts.amount",
                "receipts.amount_paid",
                "receipts.amount_return",
                "staffs.full_name",
                "receipts.total_money",
                "receipts.custom_discount as discount",
                "receipts.created_at",
                "staffs.full_name"
            )
            ->where("receipts.object_id", $id)
            ->where("receipts.object_type", "debt")
            ->get();
        return $ds;
    }

    public function getReceiptOrder($order_id){
        return $this->where('object_id',$order_id)
            ->where("receipts.object_type", "order")->first();

    }
    public function getReceiptStockOrder($stock_order_id){
        return $this->where('object_id',$stock_order_id)
            ->where("receipts.object_type", "stock")->first();

    }

    public function getReceiptOrderService($order_id){
        return $this->where('object_id',$order_id)
            ->where("receipts.object_type", "order-service")->first();

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAmountDebt($id)
    {
        $select = $this
            ->leftJoin("orders", "orders.order_id", "=", "receipts.order_id")
            ->leftJoin("staffs", "staffs.staff_id", "=", "receipts.created_by")
            ->select(
                "receipts.total_money",
                "receipts.custom_discount as discount",
                "receipts.amount",
                "receipts.amount_paid",
                "receipts.amount_return",
                "orders.order_code",
                "receipts.receipt_id",
                "staffs.full_name",
                "receipts.created_at"
            )
            ->where("receipts.customer_id", $id)
            ->whereIn("receipts.status", ["unpaid", "part-paid"])
            ->get();
        return $select;
    }

    public function getReceiptById($id)
    {
        $ds = $this->leftJoin("staffs", "staffs.staff_id", "=", "receipts.staff_id")
            ->select(
                "receipts.receipt_id",
                "receipts.receipt_code",
                "receipts.customer_id",
                "receipts.object_id",
                "receipts.amount",
                "receipts.amount_paid",
                "receipts.amount_return",
                "staffs.full_name",
                "receipts.total_money",
                "receipts.custom_discount as discount",
                "receipts.created_at",
                "staffs.full_name",
                "receipts.note"
            )
            ->where("receipts.receipt_id", $id)
            ->where("receipts.object_type", "debt")
            ->first();
        return $ds;
    }

    /**
     * Lấy tất cả tiền đã thanh toán của đơn hàng
     *
     * @return mixed
     */
    public function getAllReceipt()
    {
        return $this
            ->select(
                "order_id",
                DB::raw("SUM(amount_paid) as amount_paid"),
                "note"
            )
            ->whereNotIn("status", ["cancel", "fail"])
            ->groupBy("order_id")
            ->get();
    }

    /**
     * Lấy tất cả thanh toán của đơn hàng
     *
     * @param $orderId
     * @return mixed
     */
    public function getReceiptByOrder($orderId)
    {
        return $this
            ->select(
                "receipts.receipt_id",
                "receipts.receipt_code",
                "receipts.customer_id",
                "receipts.order_id",
                "receipts.amount",
                "receipts.amount_paid",
                "receipts.created_at",
                "receipts.created_by",
                "receipts.amount_return",
                "staffs.full_name"
            )
            ->leftJoin("staffs", "staffs.staff_id", "=", "receipts.created_by")
            ->where("receipts.order_id", $orderId)
            ->get();
    }

    public function getDetail($id) {
        $oSelect = $this
            ->leftJoin('customers','customers.customer_id','receipts.customer_id')
            ->leftJoin('staffs','staffs.staff_id','receipts.staff_id')
            ->where('receipts.receipt_id',$id)
            ->select('receipts.*','staffs.user_name as staff_name','customers.email as customer_email')
            ->first();
        return $oSelect;
    }

    public function updateReceipt($id,$data) {
        $oSelect = $this->where('receipt_id',$id)->update($data);
    }
}