<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MyCore\Models\Traits\ListTableTrait;

class CustomerTable extends Model
{
    use ListTableTrait;
    protected $table = 'customers';
    protected $primaryKey = 'customer_id';
    protected $fillable = [
        'customer_id', 'full_name', 'birthday', 'gender', 'phone', 'phone2',
        'phone_verified', 'email', 'email_verified', 'password', 'ic_no', 'ic_date', 'ic_place', 'ic_front_image', 'contact_province_id',
        'contact_province_id', 'contact_district_id', 'contact_address', 'residence_province_id', 'residence_district_id', 'residence_address',
        'bank_id', 'bank_account_no', 'bank_account_name', 'bank_account_branch', 'customer_source_id', 'customer_refer_id', 'customer_refer_code',
        'customer_avatar', 'note', 'zalo', 'facebook', 'customer_code', 'customer_group_id', 'point', 'member_level_id', 'FbId', 'ZaloId', 'is_updated',
        'site_id', 'branch_id', 'postcode', 'date_last_visit', 'is_actived', 'is_deleted', 'created_by', 'updated_by', 'created_at', 'updated_at',
        'is_logout','is_test','customer_journey_id','is_referal'
    ];

    /**
     * @param array $filter
     * @return mixed
     */
    protected function _getList(&$filter = [])
    {
        $ds = $this
            ->leftJoin('customer_contract', 'customer_contract.customer_id', '=', 'customers.customer_id')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=', 'customers.customer_source_id')
            ->leftJoin('refer_source', 'refer_source.source','customers.utm_source')
            ->leftJoin('customers as broker', 'broker.customer_refer_id', '=', 'customers.customer_id')
//            ->leftJoin('group_customer', 'group_customer.group_customer_id', '=', 'customers.customer_journey_id')
            ->leftJoin('group_customer', 'group_customer.group_customer_id', '=', 'customers.customer_group_id')
            ->leftJoin('stock_customer as sc', 'sc.customer_id', '=', $this->table . '.customer_id')
	    ->select(
                'customers.customer_id as customer_id',
                'broker.customer_id as broker_customer_id',
                'customers.branch_id as branch_id',
                'customers.full_name as full_name',
                'customers.birthday as birthday',
                'customers.gender as gender',
                'customers.email as email',
                'customers.is_referal',
                'customers.phone2 as phone2',
                'customers.phone as phone_login',
                'customers.customer_code as customer_code',
                'customers.customer_avatar as customer_avatar',
                'customers.created_at as created_at',
                'customers.customer_refer_id',
                'customers.is_test',
                'customers.utm_source',
                'customers.customer_refer_code',
                'customer_sources.customer_source_name',
                'refer_source.source_name as refer_source_name',
                'group_customer.group_name as customer_journey_name',
                DB::raw("SUM(customer_contract.total_amount) as sum_money_contract"),
                'customers.is_actived',
                'sc.stock'
            );
        if(isset($filter['type_customer']) && $filter['type_customer'] == 'potential'){
            $ds->whereNotExists(function ($ds){
                $ds->select(DB::raw(1))
                    ->from('customer_contract')
                    ->whereRaw('customer_contract.customer_id = customers.customer_id');
            });
            unset($filter['type_customer']);
        }

        if(isset($filter['type_customer']) && $filter['type_customer'] == 'broker'){
            $ds = $ds->whereNotNull('customers.customer_refer_id')->groupBy('customers.customer_refer_id');
            unset($filter['type_customer']);
        } else {
            $ds = $ds->groupBy('customers.customer_id');
        }

        if (isset($filter['arr_refer_id'])){
            $ds = $ds->whereIn('customers.customer_id',$filter['arr_refer_id']);
            unset($filter['arr_refer_id']);
        }

        if (isset($filter['introduct_refer_id'])){
            $ds = $ds->where('customers.customer_refer_id',$filter['introduct_refer_id']);
            unset($filter['introduct_refer_id']);
        }

        if (isset($filter['listCustomer'])){
            $ds = $ds->whereIn('customers.customer_id',$filter['listCustomer']);
            unset($filter['listCustomer']);
        }

        $ds = $ds->where('customers.is_deleted', 0);
//            ->where('customers.customer_id', '<>', 1)
//            ->where('customers.customer_id', 1)

        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $ds->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone2', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%')
                    ->orWhere('group_customer.group_name', 'like', '%' . $search . '%')
                    ->where('customers.is_deleted', 0);
            });
        }

        unset($filter['search']);
        if (isset($filter['type_customer']) && $filter['type_customer'] == 1){
            $ds = $ds->whereNotNull('customer_contract.customer_contract_id');
        } else if (isset($filter['type_customer']) && $filter['type_customer'] == 0) {
            $ds = $ds->whereNull('customer_contract.customer_contract_id');
        }

        if(isset($filter['customer_source_id'])) {
            $ds = $ds->where('customer_sources.customer_source_id',$filter['customer_source_id']);
            unset($filter['customer_source_id']);
        }

        if(isset($filter['utm_source'])) {
            if($filter['utm_source'] == 'na') {
                $ds = $ds
                    ->whereNull('customers.customer_refer_code');
            } else if($filter['utm_source'] == 'other') {
                $ds = $ds
                    ->whereNull('customers.utm_source')
                    ->where(function ($ds){
                        $ds
                            ->whereNull('customers.is_referal')
                            ->orWhere('customers.is_referal',0);
                    })
                    ->whereNotNull('customers.customer_refer_code');
            }
            else if ($filter['utm_source'] == 'default') {
                $ds = $ds->whereNull('customers.utm_source')
                    ->where('customers.is_referal',1)
                    ->whereNotNull('customers.customer_refer_code');
            } else {
                $ds = $ds->where('customers.utm_source',$filter['utm_source']);
            }
            unset($filter['utm_source']);
        }

        if (isset($filter['created'])){
            $day = $filter['created'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');

            $ds = $ds
                ->where('customers.created_at','>=', $startTime)
                ->where('customers.created_at','<=', $endTime);
            unset($filter['created']);
        }

        if (isset($filter['is_broker'])){
            $ds = $ds->whereNotNull('broker.customer_id');
            unset($filter['is_broker']);
        }

        if (isset($filter['is_test'])){
            $ds = $ds->where('customers.is_test',1);
            unset($filter['is_test']);
        }
        if (isset($filter['is_active'])){
            $ds = $ds->where('customers.is_actived',$filter['is_active']);
            unset($filter['is_active']);
        }
        if (isset($filter['not_in'])){
            $ds = $ds->whereNotIn('customers.customer_id',$filter['not_in']);
            unset($filter['not_in']);
        }
        //Keyword tên, sđt, email
        if (isset($filter['keyword_FN_P_E'])) {
            $search = $filter['keyword_FN_P_E'];
            $ds->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%');
            });
            unset($filter['keyword_FN_P_E']);
        }

        if (isset($filter['getListCustomer'])){
            $ds = $ds->whereNotIn('customers.customer_id',$filter['getListCustomer']);
            unset($filter['getListCustomer']);
        }
        if (isset($filter['listCustomerNotIn'])){
            $ds = $ds->whereNotIn('customers.customer_id',$filter['listCustomerNotIn']);
            unset($filter['listCustomerNotIn']);
        }
         // todo: Top 100
        if (isset($filter['is_top'])){
            $ds = $ds->whereNotNull("sc.stock");
            $ds = $ds->limit(100);
            unset($filter['is_top']);
            if(isset($filter['stock_order_by']) != ''){
                if($filter['stock_order_by'] == 1){
                    $ds = $ds->orderBy("sc.stock","ASC");
                }
                else{
                    $ds = $ds->orderBy("sc.stock","DESC");
                }
                unset($filter['stock_order_by']);
            }
        }       
        $ds = $ds
            ->orderBy('customers.customer_id', 'desc')->groupBy('customers.customer_id');
        unset($filter['type_customer']);
//        unset($filter['perpage']);
        return $ds;
    }

    /**
     * export dữ liệu
     * @param array $filter
     * @return mixed
     */
    public function getListExport($filter)
    {

        $ds = $this
            ->leftJoin('customer_contract', 'customer_contract.customer_id', '=', 'customers.customer_id')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=', 'customers.customer_source_id')
            ->leftJoin('refer_source', 'refer_source.source','customers.utm_source')
            ->leftJoin('customers as broker', 'broker.customer_refer_id', '=', 'customers.customer_id')
            ->leftJoin('group_customer', 'group_customer.group_customer_id', '=', 'customers.customer_group_id')
            ->select(
                'customers.customer_id as customer_id',
                'broker.customer_id as broker_customer_id',
                'customers.branch_id as branch_id',
                'customers.full_name as full_name',
                'customers.birthday as birthday',
                'customers.is_referal',
                'customers.utm_source',
                'customers.customer_refer_code',
                'customers.gender as gender',
                'customers.email as email',
                'customers.phone2 as phone2',
                'customers.phone as phone_login',
                'customers.customer_code as customer_code',
                'customers.customer_avatar as customer_avatar',
                'customers.created_at as created_at',
                'customers.customer_refer_id',
                'customers.is_test',
                'customer_sources.customer_source_name',
                'refer_source.source_name as refer_source_name',
                'group_customer.group_name as customer_journey_name',
                DB::raw("SUM(customer_contract.total_amount) as sum_money_contract"),
                'customers.is_actived'
            );
        if(isset($filter['type_customer']) && $filter['type_customer'] == 'potential'){
            $ds->whereNotExists(function ($ds){
                $ds->select(DB::raw(1))
                    ->from('customer_contract')
                    ->whereRaw('customer_contract.customer_id = customers.customer_id');
            });
            unset($filter['type_customer']);
        }

        if(isset($filter['utm_source'])) {
            if($filter['utm_source'] == 'na') {
                $ds = $ds
                    ->whereNull('customers.customer_refer_code');
            } else if($filter['utm_source'] == 'other') {
                $ds = $ds
                    ->whereNull('customers.utm_source')
                    ->where(function ($ds){
                        $ds
                            ->whereNull('customers.is_referal')
                            ->orWhere('customers.is_referal',0);
                    })
                    ->whereNotNull('customers.customer_refer_code');
            }
            else if ($filter['utm_source'] == 'default') {
                $ds = $ds->whereNull('customers.utm_source')
                    ->where('customers.is_referal',1)
                    ->whereNotNull('customers.customer_refer_code');
            } else {
                $ds = $ds->where('customers.utm_source',$filter['utm_source']);
            }
            unset($filter['utm_source']);
        }

        if(isset($filter['type_customer']) && $filter['type_customer'] == 'broker'){
            $ds = $ds->whereNotNull('customers.customer_refer_id')->groupBy('customers.customer_refer_id');
            unset($filter['type_customer']);
        } else {
            $ds = $ds->groupBy('customers.customer_id');
        }

        if (isset($filter['arr_refer_id'])){
            $ds = $ds->whereIn('customers.customer_id',$filter['arr_refer_id']);
            unset($filter['arr_refer_id']);
        }

        if (isset($filter['introduct_refer_id'])){
            $ds = $ds->where('customers.customer_refer_id',$filter['introduct_refer_id']);
            unset($filter['introduct_refer_id']);
        }

        $ds = $ds->where('customers.is_deleted', 0)
            ->where('customers.customer_id', '<>', 1)
//            ->where('customers.customer_id', 1)
            ->orderBy('customers.customer_id', 'desc');

        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $ds->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone2', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%')
                    ->where('customers.is_deleted', 0);
            });
        }

        unset($filter['search']);
        if (isset($filter['type_customer']) && $filter['type_customer'] == 1){
            $ds = $ds->whereNotNull('customer_contract.customer_contract_id');
        } else if (isset($filter['type_customer']) && $filter['type_customer'] == 0) {
            $ds = $ds->whereNull('customer_contract.customer_contract_id');
        }

        if(isset($filter['customer_source_id'])) {
            $ds = $ds->where('customer_sources.customer_source_id',$filter['customer_source_id']);
            unset($filter['customer_source_id']);
        }

        if (isset($filter['created'])){
            $day = $filter['created'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');

            $ds = $ds
                ->where('customers.created_at','>=', $startTime)
                ->where('customers.created_at','<=', $endTime);
            unset($filter['created']);
        }

        if (isset($filter['is_broker'])){
            $ds = $ds->whereNotNull('broker.customer_id');
            unset($filter['is_broker']);
        }

        if (isset($filter['is_test'])){
            $ds = $ds->where('customers.is_test',1);
            unset($filter['is_test']);
        }

        $ds = $ds->groupBy('customers.customer_id');
        unset($filter['type_customer']);
        return $ds->get();
    }

    public function exportList100($filter = []){
        $ds = $this
            ->leftJoin('customer_contract', 'customer_contract.customer_id', '=', 'customers.customer_id')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=', 'customers.customer_source_id')
            ->leftJoin('refer_source', 'refer_source.source','customers.utm_source')
            ->leftJoin('customers as broker', 'broker.customer_refer_id', '=', 'customers.customer_id')
//            ->leftJoin('group_customer', 'group_customer.group_customer_id', '=', 'customers.customer_journey_id')
            ->leftJoin('group_customer', 'group_customer.group_customer_id', '=', 'customers.customer_group_id')
            ->leftJoin('stock_customer as sc', 'sc.customer_id', '=', $this->table . '.customer_id')
            ->select(
                'customers.customer_id as customer_id',
                'broker.customer_id as broker_customer_id',
                'customers.branch_id as branch_id',
                'customers.full_name as full_name',
                'customers.birthday as birthday',
                'customers.gender as gender',
                'customers.email as email',
                'customers.is_referal',
                'customers.phone2 as phone2',
                'customers.phone as phone_login',
                'customers.customer_code as customer_code',
                'customers.customer_avatar as customer_avatar',
                'customers.created_at as created_at',
                'customers.customer_refer_id',
                'customers.is_test',
                'customers.utm_source',
                'customers.customer_refer_code',
                'customer_sources.customer_source_name',
                'refer_source.source_name as refer_source_name',
                'group_customer.group_name as customer_journey_name',
                DB::raw("SUM(customer_contract.total_amount) as sum_money_contract"),
                'customers.is_actived',
                'sc.stock'
            );
        if(isset($filter['type_customer']) && $filter['type_customer'] == 'potential'){
            $ds->whereNotExists(function ($ds){
                $ds->select(DB::raw(1))
                    ->from('customer_contract')
                    ->whereRaw('customer_contract.customer_id = customers.customer_id');
            });
            unset($filter['type_customer']);
        }

        if(isset($filter['type_customer']) && $filter['type_customer'] == 'broker'){
            $ds = $ds->whereNotNull('customers.customer_refer_id')->groupBy('customers.customer_refer_id');
            unset($filter['type_customer']);
        } else {
            $ds = $ds->groupBy('customers.customer_id');
        }

        if (isset($filter['arr_refer_id'])){
            $ds = $ds->whereIn('customers.customer_id',$filter['arr_refer_id']);
            unset($filter['arr_refer_id']);
        }

        if (isset($filter['introduct_refer_id'])){
            $ds = $ds->where('customers.customer_refer_id',$filter['introduct_refer_id']);
            unset($filter['introduct_refer_id']);
        }

        if (isset($filter['listCustomer'])){
            $ds = $ds->whereIn('customers.customer_id',$filter['listCustomer']);
            unset($filter['listCustomer']);
        }

        $ds = $ds->where('customers.is_deleted', 0);
//            ->where('customers.customer_id', '<>', 1)
//            ->where('customers.customer_id', 1)

        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $ds->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone2', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%')
                    ->orWhere('group_customer.group_name', 'like', '%' . $search . '%')
                    ->where('customers.is_deleted', 0);
            });
        }

        unset($filter['search']);
        if (isset($filter['type_customer']) && $filter['type_customer'] == 1){
            $ds = $ds->whereNotNull('customer_contract.customer_contract_id');
        } else if (isset($filter['type_customer']) && $filter['type_customer'] == 0) {
            $ds = $ds->whereNull('customer_contract.customer_contract_id');
        }

        if(isset($filter['customer_source_id'])) {
            $ds = $ds->where('customer_sources.customer_source_id',$filter['customer_source_id']);
            unset($filter['customer_source_id']);
        }

        if(isset($filter['utm_source'])) {
            if($filter['utm_source'] == 'na') {
                $ds = $ds
                    ->whereNull('customers.customer_refer_code');
            } else if($filter['utm_source'] == 'other') {
                $ds = $ds
                    ->whereNull('customers.utm_source')
                    ->where(function ($ds){
                        $ds
                            ->whereNull('customers.is_referal')
                            ->orWhere('customers.is_referal',0);
                    })
                    ->whereNotNull('customers.customer_refer_code');
            }
            else if ($filter['utm_source'] == 'default') {
                $ds = $ds->whereNull('customers.utm_source')
                    ->where('customers.is_referal',1)
                    ->whereNotNull('customers.customer_refer_code');
            } else {
                $ds = $ds->where('customers.utm_source',$filter['utm_source']);
            }
            unset($filter['utm_source']);
        }

        if (isset($filter['created'])){
            $day = $filter['created'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');

            $ds = $ds
                ->where('customers.created_at','>=', $startTime)
                ->where('customers.created_at','<=', $endTime);
            unset($filter['created']);
        }

        if (isset($filter['is_broker'])){
            $ds = $ds->whereNotNull('broker.customer_id');
            unset($filter['is_broker']);
        }

        if (isset($filter['is_test'])){
            $ds = $ds->where('customers.is_test',1);
            unset($filter['is_test']);
        }
        if (isset($filter['is_active'])){
            $ds = $ds->where('customers.is_actived',$filter['is_active']);
            unset($filter['is_active']);
        }
        if (isset($filter['not_in'])){
            $ds = $ds->whereNotIn('customers.customer_id',$filter['not_in']);
            unset($filter['not_in']);
        }
        //Keyword tên, sđt, email
        if (isset($filter['keyword_FN_P_E'])) {
            $search = $filter['keyword_FN_P_E'];
            $ds->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('customers.phone', 'like', '%' . $search . '%')
                    ->orWhere('customers.email', 'like', '%' . $search . '%');
            });
            unset($filter['keyword_FN_P_E']);
        }

        if (isset($filter['getListCustomer'])){
            $ds = $ds->whereNotIn('customers.customer_id',$filter['getListCustomer']);
            unset($filter['getListCustomer']);
        }
        if (isset($filter['listCustomerNotIn'])){
            $ds = $ds->whereNotIn('customers.customer_id',$filter['listCustomerNotIn']);
            unset($filter['listCustomerNotIn']);
        }
        // todo: Top 100
        if (isset($filter['is_top'])){
            $ds = $ds->whereNotNull("sc.stock");
            $ds = $ds->limit(100);
            unset($filter['is_top']);
            if(isset($filter['stock_order_by']) != ''){
                if($filter['stock_order_by'] == 1){
                    $ds = $ds->orderBy("sc.stock","ASC");
                }
                else{
                    $ds = $ds->orderBy("sc.stock","DESC");
                }
                unset($filter['stock_order_by']);
            }
        }
        $ds = $ds
            ->orderBy('customers.customer_id', 'desc')->groupBy('customers.customer_id');
        unset($filter['type_customer']);
//        unset($filter['perpage']);
        return $ds->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        $add = $this->create($data);
        return $add->customer_id;
    }

    public function getCustomerSearch($data)
    {
        $select = $this->select('customer_id', 'full_name', 'phone1', 'customer_avatar', 'account_money', 'address')
//            ->where('full_name', 'like', '%' . $data . '%')
//            ->orWhere('phone1', 'like', '%' . $data . '%')
            ->where(function ($query) use ($data) {
                $query->where('full_name', 'like', '%' . $data . '%')
                    ->orWhere('phone1', 'like', '%' . $data . '%')
                    ->orWhere('phone', 'like', '%' . $data . '%');
            })
            ->where('is_deleted', 0)
            ->where('is_actived', 1);
//        if (Auth::user()->is_admin != 1) {
//            $select->where('branch_id', Auth::user()->branch_id);
//        }
        return $select->paginate(6);
    }

    /**
     * @param $id
     * @return
     */
    public function getItem($id)
    {
        $get = $this
//            ->leftJoin('customer_groups as group', 'group.customer_group_id', '=', 'customers.customer_group_id')
            ->leftJoin('customer_sources as source', 'source.customer_source_id', '=', 'customers.customer_source_id')
//            ->leftJoin('province', 'province.provinceid', '=', 'customers.province_id')
//            ->leftJoin('district', 'district.districtid', '=', 'customers.district_id')
//            ->leftJoin('member_levels', 'member_levels.member_level_id', '=', 'customers.member_level_id')
            ->select(
//                'customers.customer_group_id as customer_group_id',
//                'group.group_name as group_name',
//                'customers.full_name as full_name',
//                'customers.customer_code as customer_code',
//                'customers.gender as gender',
//                'customers.phone as phone1',
//                'province.name as province_name',
//                'province.type as province_type',
//                'district.name as district_name',
//                'district.type as district_type',
//                'customers.contact_address as address',
//                'customers.email as email',
//                'customers.customer_source_id as customer_source_id',
//                'customers.birthday as birthday',
//                'source.customer_source_name',
//                'customers.customer_refer_id',
//                'customers.facebook as facebook',
//                'customers.zalo as zalo',
//                'customers.note as note',
//                'customers.customer_id as customer_id',
//                'customers.is_actived as is_actived',
//                'customers.phone2 as phone2',
//                'customers.customer_avatar as customer_avatar',
//                'customers.created_at as created_at',
//                'customers.account_money as account_money',
//                'customers.province_id as province_id',
//                'customers.district_id as district_id',
//                'customers.point as point',
//                'customers.member_level_id as member_level_id',
//                'member_levels.name as member_level_name',
//                'customers.point as point',
//                'member_levels.discount as member_level_discount',
//                "{$this->table}.postcode"
            )
            ->where('customers.customer_id', $id);
//        if (Auth::user()->is_admin != 1) {
//            $get->where('customers.branch_id', Auth::user()->branch_id);
//        }
        return $get->first();
    }


    /**
     * @param $id
     */
    public function getItemRefer($id)
    {
        $get = $this->Join('customers as cs', 'cs.customer_refer_id', '=', 'customers.customer_id')
            ->select('customers.full_name as full_name_refer', 'customers.customer_id as customer_id')
            ->where('cs.customer_id', $id)->first();
        return $get;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->where('customer_id', $id)->update($data);
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $this->where('customer_id', $id)->update(['is_deleted' => 1]);
    }

    /**
     * @return mixed
     */
    public function getCustomerOption()
    {
        return $this->select('full_name', 'customer_code', 'customer_id', 'phone')
            ->where('is_actived', 1)
            ->where('is_deleted', 0)
            ->where('customer_id', '!=', 1)->get()->toArray();
    }

    /**
     * @param $phone
     * @param $id
     * @return mixed
     * Kiểm tra số điện thoại đã tồn tại chưa
     */
    public function testPhone($phone, $id)
    {
        return $this->where(function ($query) use ($phone) {
            $query->where('phone', '=', $phone)
                ->orWhere('phone2', '=', $phone);
        })->where('customer_id', '<>', $id)
            ->where('is_deleted', 0)->first();
    }

    public function searchPhone($phone)
    {
        $select = $this->select('customer_id', 'full_name', 'phone')
            ->where('phone', 'like', '%' . $phone . '%')
            ->where('is_actived', 1)
            ->where('is_deleted', 0);
        return $select->get();
    }

    /**
     * @param $phone
     * @return mixed
     * Lấy danh sách sđt khách hàng
     */
    public function getCusPhone($phone)
    {
        $select = $this->select('customer_id', 'full_name', 'phone')
            ->where('phone', $phone)
            ->where('is_actived', 1)
            ->where('is_deleted', 0);
        return $select->first();
    }

    public function getCusPhone2($phone)
    {
        $select = $this->select('customer_id', 'full_name', 'phone')
            ->where('phone', $phone)
            ->where('is_deleted', 0);
        return $select->first();
    }

    /**
     * @return mixed
     * Tổng số khách hàng từ năm hiện tại trở về trước
     */
    public function totalCustomer($yearNow)
    {

        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('YEAR(created_at)'), '<=', $yearNow)
            ->where('is_deleted', 0)->get();
        return $ds;
    }

    /**
     * @param $dayNow
     * @return mixed
     * Tổng số khách hàng đã tạo trong năm hiện tại
     */
    public function totalCustomerNow($yearNow)
    {

        $ds = $this->select(DB::raw('count(customer_id) as number'))
//            ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') = DATE_FORMAT('$yearNow','%Y-%m-%d')")
            ->whereRaw("YEAR(created_at)=$yearNow")
            ->where('is_deleted', 0)
            ->get();
        return $ds;
    }

    /**
     * @param $year
     * @param $branch
     * @return mixed
     * Tổng số khách hàng trong năm hiện tại trở về trước và chi nhánh
     */
    public function filterCustomerYearBranch($year, $branch)
    {

        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('YEAR(created_at)'), '<=', $year)->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    /**
     * @param $year
     * @param $branch
     * @return mixed
     * Tổng số khách hàng trong năm hiện tại và chi nhánh
     */
    public function filterNowCustomerBranch($year, $branch)
    {
        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('YEAR(created_at)'), '=', $year)->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    /**
     * @param $time
     * @param $branch
     * @return mixed
     * Tổng số KH từ thời gian endTime trở về trước và chi nhánh
     */
    public function filterTimeToTime($time, $branch)
    {

        $arr_filter = explode(" - ", $time);
        $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
        $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->where(DB::raw('DATE(created_at)'), '<=', $endTime)->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    /**
     * @param $time
     * @param $branch
     * @return mixed
     * Tổng số KH từ khoản thời gian start time và end time
     */
    public function filterTimeNow($time, $branch)
    {
        $arr_filter = explode(" - ", $time);
        $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
        $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
        $ds = $this->select(DB::raw('count(customer_id) as number'))
            ->whereBetween(DB::raw('DATE(created_at)'), [$startTime, $endTime])
            ->where('is_deleted', 0);
        if (!is_null($branch)) {
            $ds->where('branch_id', $branch);
        }
        return $ds->get();
    }

    public function searchCustomerEmail($data, $birthday, $gender)
    {
        $select = $this->leftJoin('branches', 'branches.branch_id', '=', 'customers.branch_id')
            ->select('customers.customer_id',
                'customers.full_name',
                'customers.phone', 'customers.birthday',
                'customers.gender', 'branches.branch_name', 'customers.email')
            ->where('customers.is_actived', 1)
            ->where('customers.is_deleted', 0)
            ->where('customers.customer_id', '<>', 1);
        if ($data != null) {
            $select->where(function ($query) use ($data, $birthday, $gender) {
                $query->where('customers.full_name', 'like', '%' . $data . '%')
                    ->orWhere('customers.phone', 'like', '%' . $data . '%')
                    ->orWhere('customers.email', 'like', '%' . $data . '%');
            });
        }
        if ($birthday != null) {
            $select->where('customers.birthday', $birthday);
        }
        if ($gender != null) {
            $select->where('customers.gender', $gender);
        }

        return $select->get();
    }

    public function getBirthdays()
    {
        $select = $this->whereMonth('birthday', '=', date('m'))
            ->whereDay('birthday', '=', date('d'))
            ->where('is_deleted', 0)
            ->where('is_actived', 1)
            ->where('phone1', '<>', null)->get();
        return $select;
    }

    //search dashboard
    public function searchDashboard($keyword)
    {
        $select = $this->select(
            'customer_id',
            'full_name',
            'phone',
            'customers.email as email',
            'branches.branch_name as branch_name',
            'customers.updated_at as updated_at',
            'customer_avatar',
            'customer_id',
            'group_name',
            'customer_avatar'
        )
            ->leftJoin('branches', 'branches.branch_id', '=', 'customers.branch_id')
            ->leftJoin('customer_groups', 'customer_groups.customer_group_id', '=', 'customers.customer_group_id')
            ->where(function ($query) use ($keyword) {
                $query->where('customers.full_name', 'like', '%' . $keyword . '%')
                    ->orWhere('customers.phone', 'like', '%' . $keyword . '%')
                    ->orWhere('customers.email', 'like', '%' . $keyword . '%');
            })
            ->where('customers.is_deleted', 0)
            ->where('customers.is_actived', 1)
            ->where('customers.customer_id', '<>', 1)
            ->get();
        return $select;
    }

    /**
     * Báo cáo công nợ theo khách hàng
     *
     * @param $id_branch
     * @param $time
     * @param $top
     * @return mixed
     */
    public function reportCustomerDebt($id_branch, $time, $top)
    {
        $ds = $this
            ->leftJoin('customer_debt', 'customer_debt.customer_id', '=', 'customers.customer_id')
            ->leftJoin('branches', 'branches.branch_id', '=', 'customers.branch_id')
            ->select(
                'customers.full_name',
                'customer_debt.debt_type',
                'customer_debt.status',
                'customer_debt.amount',
                'customer_debt.amount_paid'
            );
        if (isset($id_branch)) {
            $ds->where('branches.branch_id', $id_branch);
        }
        if (isset($time) && $time != "") {
            $arr_filter = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $ds->whereBetween('customer_debt.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
        }
        return $ds->get();
    }

    protected function getListCore(&$filters = [])
    {
        $oSelect = $this
            ->leftJoin(
                'branches', 'branches.branch_id', '=', 'customers.branch_id'
            )
            ->leftJoin(
                'customer_groups', 'customer_groups.customer_group_id', '=',
                'customers.customer_group_id'
            )
            ->select(
                'customers.customer_id as customer_id',
                'customers.branch_id as branch_id',
                'customers.full_name as full_name',
                'customers.birthday as birthday',
                'customers.gender as gender',
                'customers.email as email',
                'customers.phone as phone1',
                'customers.customer_code as customer_code',
                'customer_groups.group_name as group_name',
                'customers.customer_avatar as customer_avatar',
                'customers.created_at as created_at',
                'customers.updated_at as updated_at',
                'customers.customer_group_id as customer_group_id',
                'branches.branch_name as branch_name',
                'customers.is_actived'
            )
            ->where('customers.is_deleted', 0)
            ->where('customers.customer_id', '<>', 1)
            ->orderBy('customers.customer_id', 'desc');
        if (isset($filters['arrayUser'])) {
            $oSelect->whereIn($this->table . '.phone', $filters['arrayUser']);
            unset($filters['arrayUser']);
        }
        return $oSelect;
    }

    public function getCustomerInGroupAuto($arrayCondition)
    {
        $select = $this->select('customers.customer_id')->where(
            'customers.is_deleted', 0
        )->leftJoin(
            'customer_appointments',
            'customer_appointments.customer_id', '=',
            'customers.customer_id'
        );
        foreach ($arrayCondition as $key => $value) {
            if ($key == 1) {
                $select->leftJoin(
                    'customer_group_define_detail',
                    'customer_group_define_detail.phone', '=',
                    'customers.phone'
                )->orWhere('customer_group_define_detail.id', $value);
            } elseif ($key == 2) {
                $select->orWhere('customer_appointments.date', '>=' , $value);
            } elseif ($key == 3) {
                $select->orWhere('customer_appointments.status', '=' , $value);
            } elseif ($key == 4) {
                $select->orWhereBetween('customer_appointments.time', [$value['hour_from'], $value['hour_to']]);
            }
        }
        return $select->get();
    }

    public function getCustomerNotAppointment()
    {
        $select = $this->select('customers.customer_id', 'customer_appointments.customer_appointment_id')
            ->leftJoin(
                'customer_appointments',
                'customer_appointments.customer_id',
                'customers.customer_id'
            )
            ->where('customers.is_deleted', 0)
            ->where('customers.is_actived', 1)
            ->get();
        return $select;
    }

    public function getCustomerUseService($arrService, $where)
    {
        $select = $this->select(
            'customers.customer_id',
            'orders.order_id',
            'order_details.object_type',
            'order_details.object_id',
            'orders.process_status'
        )
            ->leftJoin('orders','orders.customer_id', '=', 'customers.customer_id')
            ->leftJoin('order_details','order_details.order_id', '=', 'orders.order_id')
            ->where('customers.is_deleted', 0)
            ->where('customers.is_actived', 1);
        return $select->get();
    }

    /**
     * Lấy List full khách hàng
     *
     * @return mixed
     */
    public function getAllCustomer()
    {
        $ds = $this->select(
            'customer_id',
            'full_name',
            'phone',
            'gender',
            'member_level_id'
        )
            ->where('customer_id', '!=',1)
            ->where('is_deleted', 0)
            ->get();
        return $ds;
    }


    /**
     * Lấy List full khách hàng  for withdraw request
     *
     * @return mixed
     */
    public function getAllCustomerWithdraw()
    {
        $ds = $this->select(
            'customer_id',
            'full_name'
        )
            ->where('customer_id', '!=',1)
            ->where('is_deleted', 0)
            ->get();
        return $ds;
    }

    /** V_Set get detail customer
     * @param int $id
     * @return mixed
     */
    public function vSetGetDetailCustomer($id)
    {
        $oSelect = $this
            ->leftJoin('customers as customer_refer', 'customer_refer.customer_refer_code', '=', $this->table.'.customer_code')
            ->leftJoin('bank', 'bank.bank_id', '=', $this->table.'.bank_id')
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', '=', $this->table.'.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', '=', $this->table.'.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', '=', $this->table.'.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', '=', $this->table.'.residence_district_id')
            ->leftJoin('member_levels', 'member_levels.member_level_id', '=', $this->table.'.member_level_id')
            ->leftJoin('refer_source', 'refer_source.source', '=', $this->table.'.utm_source')
            ->leftJoin('customer_sources', 'customer_sources.customer_source_id', '=', 'customers.customer_source_id')
            ->select(
                $this->table.'.*',
//                'customer_code.code',
                'bank.bank_name',
                'customer_refer.full_name as customer_refer_name',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'member_levels.name as member_levels_name',
                $this->table.'.contact_province_id',
                $this->table.'.contact_district_id',
                $this->table.'.contact_address',
                $this->table.'.residence_province_id',
                $this->table.'.residence_district_id',
                $this->table.'.residence_address',
                $this->table.'.is_test',
                'refer_source.source_name',
//                'customers.utm_source',
//                'customers.customer_refer_code',
                'customer_sources.customer_source_name',
                'refer_source.source_name as refer_source_name',
                $this->table.'.customer_journey_id'
            )
            ->where('customers.customer_id', $id);

        return $oSelect->first();
    }

    public function updateCustomer($id,$data) {
        $oSelect = $this->where('customer_id',$id)->update($data);
        return $oSelect;
    }

    public function getFullNameCustomerRefer($customer_refer_code) {
        $oSelect = $this->where('customer_code',$customer_refer_code)->select('full_name')->first();
        return $oSelect;
    }

    public function getCustomer($customer_id){
        $oSelect = $this
            ->leftJoin('member_levels','member_levels.member_level_id','customers.member_level_id')
            ->where('customers.customer_id',$customer_id)
            ->select('customers.*','member_levels.name as member_levels_name')->get();
        return $oSelect;
    }

    public function getDetail($customer_id){
        $oSelect = $this
            ->leftJoin('member_levels','member_levels.member_level_id','customers.member_level_id')
            ->where('customers.customer_id',$customer_id)
            ->select('customers.*','member_levels.name as member_levels_name')->first();
        return $oSelect;
    }

    public function getAll(){
        $oSelect = $this
            ->where('is_actived',1)->where('is_deleted',0)->get();
        return $oSelect;
    }

    public function getAllByDay($day){
        $oSelect = $this
            ->where('is_actived',1)
            ->where('is_deleted',0)
            ->where(DB::raw("(DATE_FORMAT(birthday,'%Y-%m-%d') )"),$day)->get();
        return $oSelect;
    }

    public function getAllByBirthday($day){
        $oSelect = $this
            ->where('is_actived',1)
            ->where('is_deleted',0)
            ->where(DB::raw("(DATE_FORMAT(birthday,'%m-%d') )"),$day)->get();
        return $oSelect;
    }

    public function getListReferCustomer($customer_id) {
        $oSelect = $this->where('customer_refer_id',$customer_id)->select('is_referal')->get();
        return $oSelect;
    }

    /**
     * VuND
     * @param $filter
     */
    public function getListCustomerChartNew($filter){
        /**
        select DATE_FORMAT(created_at, '%Y-%m-%d') as date, count(*) as 'total',
        SUM(IF(is_referal is null OR is_referal = 0, 1, 0)) as 'other',
        SUM(if(utm_source is null AND is_referal = 1 AND customer_refer_code is not null, 1, 0)) as 'copy',
        SUM(if(utm_source = 'facebook', 1, 0)) as 'facebook',
        SUM(if(utm_source = 'zalo', 1, 0)) as 'zalo',
        SUM(if(utm_source = 'twitter', 1, 0)) as 'twitter',
        SUM(if(utm_source = 'wechat', 1, 0)) as 'wechat'
        from customers
        GROUP BY date
         */

        $oSelect = $this->select(
            DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as date, count(*) as 'all'"),
            DB::raw("CAST(SUM(IF(is_referal is null OR is_referal = 0, 1, 0)) AS UNSIGNED) as 'na'"),
            DB::raw("CAST(SUM(if(utm_source is null AND is_referal = 1 AND customer_refer_code is not null, 1, 0)) AS UNSIGNED) as 'default'"),
            DB::raw("CAST(SUM(if(utm_source = 'facebook', 1, 0)) AS UNSIGNED) as 'facebook'"),
            DB::raw("CAST(SUM(if(utm_source = 'zalo', 1, 0)) AS UNSIGNED) as 'zalo'"),
            DB::raw("CAST(SUM(if(utm_source = 'twitter', 1, 0)) AS UNSIGNED) as 'twitter'"),
            DB::raw("CAST(SUM(if(utm_source = 'wechat', 1, 0)) AS UNSIGNED) as 'wechat'"),
        )->where('created_at','>=',$filter['start'])
            ->where('created_at','<=',$filter['end'])
            ->where('is_deleted', 0)
            ->groupBy('date');

        if($oSelect->get()){
            return $oSelect->get()->toArray();
        }
        return [];
    }

//    Lấy danh sách các nhà đầu tư đăng ký qua giới thiệu
    public function getListCustomerChart($filter){
        $oSelect = $this;
//            ->where(function ($oSelect){
//                $oSelect
//                    ->whereNotNull('customer_refer_id')
//                    ->orWhereNotNull('customer_refer_code');
//            });
//            ->whereNotNull('customer_refer_code');
        if ($filter['source'] == 'default') {
            $oSelect = $oSelect
                ->where('is_referal',1)
                ->whereNotNull('customer_refer_code')
                ->whereNUll('utm_source');
        } else if($filter['source'] == 'na') {
            $oSelect = $oSelect
                ->whereNUll('is_referal')
                ->whereNUll('utm_source');
        } else if($filter['source'] != 'all') {
            $oSelect = $oSelect->where('utm_source',$filter['source']);
        }

        $oSelect = $oSelect
            ->where('created_at','>=',$filter['start'])
            ->where('created_at','<=',$filter['end'])
            ->where('is_deleted', 0)
//            ->where('is_actived', 1)
//            ->where('phone_verified',1)
            ->orderBy('created_at','ASC')
            ->select(DB::raw('DATE_FORMAT(created_at, "%d-%m-%Y") as date'),'utm_source','is_referal')
            ->get();
        return $oSelect;

    }

//    Lấy danh sách các nhà đầu tư tự đăng kí không qua giới thiệu
    public function getListCustomerChartNull($filter){
        $oSelect = $this
            ->whereNull('customer_refer_code')
            ->whereNull('customer_refer_id');
        $oSelect = $oSelect
            ->where('created_at','>=',$filter['start'])
            ->where('created_at','<=',$filter['end'])
            ->where('is_deleted', 0)
//            ->where('is_actived', 1)
//            ->where('phone_verified',1)
            ->orderBy('created_at','ASC')
            ->select(DB::raw('DATE_FORMAT(created_at, "%d-%m-%Y") as date'))
            ->get();
        return $oSelect;
    }

    /**
     * Lấy thông tin khách hàng
     *
     * @param $customerId
     * @return mixed
     */
    public function getInfo($customerId)
    {
        return $this
            ->select(
                "customer_id",
                "customer_code",
                "full_name",
                "birthday",
                "gender",
                "phone"
            )
            ->where("$this->primaryKey", $customerId)
            ->first();
    }

    public function deleteJourney($customer_journey_id){
        $oSelect = $this->where('customer_group_id',$customer_journey_id)->update(['customer_group_id' => null]);
    }

    public function updateByArrCustomerId($arrId,$data){
        $oSelect = $this->whereIn('customer_id',$arrId)->update($data);
        return $oSelect;
    }

    /**
     * get Id refer by customer id
     *
     * @param $customerId
     * @return mixed
     */
    public function getReferIdOfCustomer($customerId)
    {
        return $this->select("customer_refer_id")->where("customer_id", $customerId)->first();
    }
}