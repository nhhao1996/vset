<?php

namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WithdrawRequestTable extends Model
{
    use ListTableTrait;
    protected $table = "withdraw_request";
    protected $primaryKey = "withdraw_request_id";

    protected $fillable = [
        'withdraw_request_id',
        'withdraw_request_code',
        'withdraw_request_type',
        'customer_contract_id',
        'customer_id',
        'withdraw_free_rate',
        'withdraw_free_amount',
        'available_balance',
        'withdraw_request_amount',
        'withdraw_request_year',
        'withdraw_request_month',
        'withdraw_request_day',
        'withdraw_request_time',
        'withdraw_request_status',
        'payment_date_planing',
        'customer_name',
        'customer_phone',
        'customer_email',
        'customer_residence_address',
        'customer_ic_no',
        'bank_id',
        'bank_name',
        'bank_account_no',
        'bank_account_name',
        'bank_account_branch',
        'withdraw_request_group_id',
        'withdraw_request_goal',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public function _getList(&$filter = [])
    {
        $select = $this
            ->select(
                'withdraw_request.*' ,
                'withdraw_request_group.withdraw_request_group_code' ,
                'staffs_create_name.full_name as staffs_create_name',
                'customer_contract.customer_contract_code',
                'customer_contract.term_time_type',
                'customers.residence_address',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type'
            )
            ->join('customers','customers.customer_id','withdraw_request.customer_id')
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->join("withdraw_request_group","withdraw_request_group.withdraw_request_group_id","=","withdraw_request.withdraw_request_group_id")
            ->leftJoin("staffs as staffs_create_name","staffs_create_name.staff_id","=","withdraw_request.created_by")
            ->leftJoin("customer_contract","customer_contract.customer_contract_id","=","withdraw_request.customer_contract_id")
            ->orderBy('withdraw_request.withdraw_request_id','DESC');
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $select->where(function ($query) use ($search) {
                $query->where('withdraw_request_group.withdraw_request_group_code', 'like', '%' . $search . '%')
                    ->orWhere('customer_contract.customer_contract_code', 'like', '%' . $search . '%')
                    ->orWhere('withdraw_request.customer_name', 'like', '%' . $search . '%');
            });
            unset($filter['search']);
        }
//        if (isset($filter['withdraw_request_type']) != "") {
//              if ($filter['withdraw_request_type'] == "bond_saving"){
//                  $select = $select->whereIn('withdraw_request.withdraw_request_type', ['bond','saving']);
//              }
//              if($filter['withdraw_request_type'] == "interest_commission"){
//                  $select = $select->whereIn('withdraw_request.withdraw_request_type', ['interest','commission']);
//            }
//            unset($filter['withdraw_request_type']);
//        }

        if (!isset($filter['all'])){
            $select = $select->whereIn('withdraw_request.withdraw_request_type', ['bond','saving']);
        }

        if (isset($filter['created_at']) != "") {
            $arr_filter = explode(" - ", $filter["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select->whereBetween('withdraw_request.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
            unset($filter['created_at']);
        }

        if (isset($filter['withdraw_request_status'])) {
            $select = $select->where('withdraw_request.withdraw_request_status',$filter['withdraw_request_status']);
            unset($filter['withdraw_request_status']);
        }

        unset($filter['all']);

        return $select->where('withdraw_request_group.object_id',0)->orderBy('withdraw_request.withdraw_request_id','DESC');
    }

    public function getById($id)
    {
        $select = $this
            ->select(
                'withdraw_request.*',
                'withdraw_request_group.withdraw_request_group_code',
                'customer_contract.customer_contract_code',
                'customer_contract.total_amount as contract_total_amount',
                'customer_contract.term_time_type',
                'customer_contract.investment_time',
                'customer_contract.withdraw_interest_time',
                'customer_contract.customer_contract_start_date',
                'customer_contract.total_amount as customer_contract_total_amount',
                'products.withdraw_min_time',
                'products.withdraw_fee_rate_before',
                'products.withdraw_fee_rate_ok',
                'withdraw_request_group.withdraw_request_group_code',
                'customer_contract.is_active as contract_is_active',
                'customer_contract.is_deleted as contract_is_deleted',
                'thuongtru_pro.name as thuongtru_pro_name',
                'thuongtru_pro.type as thuongtru_pro_type',
                'thuongtru_dis.name as thuongtru_dis_name',
                'thuongtru_dis.type as thuongtru_dis_type',
                'lienhe_pro.name as lienhe_pro_name',
                'lienhe_pro.type as lienhe_pro_type',
                'lienhe_dis.name as lienhe_dis_name',
                'lienhe_dis.type as lienhe_dis_type',
                'customers.residence_address'
            )
            ->join("withdraw_request_group","withdraw_request_group.withdraw_request_group_id","=","withdraw_request.withdraw_request_group_id")
            ->leftJoin("customer_contract","customer_contract.customer_contract_id","=","withdraw_request.customer_contract_id")
            ->leftJoin("products","products.product_id","=","customer_contract.product_id")
            ->join('customers', 'customers.customer_id', 'customer_contract.customer_id')
            ->leftJoin('province as thuongtru_pro', 'thuongtru_pro.provinceid', 'customers.contact_province_id')
            ->leftJoin('district as thuongtru_dis', 'thuongtru_dis.districtid', 'customers.contact_district_id')
            ->leftJoin('province as lienhe_pro', 'lienhe_pro.provinceid', 'customers.residence_province_id')
            ->leftJoin('district as lienhe_dis', 'lienhe_dis.districtid', 'customers.residence_district_id')
            ->where('withdraw_request.withdraw_request_id',$id)->first();
        return $select;
    }

    /**
     * Tổng tiền yêu cầu rút của contract theo status.
     * @param array $params
     *
     * @return mixed
     */
    public function sumContractStatus($params = [])
    {
        $select = $this->select(DB::raw("SUM(withdraw_request_amount) as withdraw_request_amount"))
            ->where($this->table . '.customer_contract_id', $params['customer_contract_id'])
            ->where($this->table . '.withdraw_request_status', $params['withdraw_request_status']);
        if (isset($params['withdraw_request_type'])) {
            $select = $select->where($this->table . '.withdraw_request_type', $params['withdraw_request_type']);
        }
        return $select->first();
    }


    /**
     * Record cuối cùng của customer contract
     * @param $customerContractId
     *
     * @return mixed
     */
    public function lastCustomerContract($customerContractId)
    {
        $select = $this->select()
            ->where($this->table . '.customer_contract_id', $customerContractId)
            ->orderBy($this->table . '.withdraw_request_id', 'DESC')
            ->first();
        return $select;
    }

    public function getListAll($param) {
        $page    = (int) ($param['page'] ? $param['page'] : 1);
        $display = 10;
        $oSelect = $this
//            ->where('withdraw_request_group_id', $param['withdraw_request_group_id'])
//            ->where('withdraw_request_type', $param['withdraw_request_group_type'])
//            ->where('withdraw_request_status',$param['withdraw_request_status'])
//            thêm qua request
            ->where('customer_contract_id',$param['customer_contract_id'])
            ->orderBy('withdraw_request_id','DESC');

        if ($display > 10) {
            $page = intval(count($oSelect->get()) / $display);
        }
        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    public function changeStatus($id,$status) {
        $oSelect = $this->where('withdraw_request_id',$id)->update($status);
        return $oSelect;
    }

    public function getListIdContract($order_id,$withdraw_request_goal){
        $oSelect = $this
            ->join('withdraw_request_group','withdraw_request_group.withdraw_request_group_id','withdraw_request.withdraw_request_group_id')
            ->where('withdraw_request_group.object_id',$order_id)
            ->whereNotIn('withdraw_request_group.withdraw_request_goal',$withdraw_request_goal)
//            ->where('withdraw_request_group.withdraw_request_status','done')
//            ->where('withdraw_request.withdraw_request_status','done')
            ->get();
        return $oSelect;
    }

    public function changeStatusByWithdrawGroup($group_id,$status) {
        $oSelect = $this->where('withdraw_request_group_id',$group_id)->update($status);
        return $oSelect;
    }

    public function getListByGroupId($arrGroupId) {
        $oSelect = $this->whereIn('withdraw_request_group_id',$arrGroupId)->get();
        return $oSelect;
    }

    public function getWithdrawByType($customerId,$type=null){
        $oResult = $this->select(
            \DB::raw("SUM({$this->table}.withdraw_request_amount) as withdraw_request_total")
        )
            ->where($this->table.".customer_id",$customerId)
            ->where($this->table.".withdraw_request_status","done");

        if($type != null){
            $oResult->where($this->table.".withdraw_request_type",$type);
        }


        $result = $oResult->get();
        return ($result!=null) ? intval($result[0]->withdraw_request_total) :0;
    }

    public function totalRequestDone($customer_contract_id,$arrType = null){
        $oSelect = $this
            ->where('customer_contract_id',$customer_contract_id)
            ->where('withdraw_request_status','done');

        if ($arrType != null) {
            $oSelect = $oSelect->whereIn('withdraw_request_type',$arrType);
        }
        $oSelect = $oSelect->select(DB::raw('SUM(withdraw_request_amount) as total_withdraw_request_amount'))->first();
        return $oSelect;
    }

    public function getListByContract($arr){
        $oSelect = $this->whereIn('customer_contract_id',$arr)->get();
        return $oSelect;
    }

    public function getListIdRequestByGroup($idGroup){
        $oSelect = $this->where('withdraw_request_group_id',$idGroup)->get();
        return $oSelect;
    }

    public function getWithdrawByContract($id,$customer_contract_id,$withdraw_request_type){
        $oSelect = $this
            ->where('withdraw_request_id','<>',$id)
            ->where('customer_contract_id',$customer_contract_id)
            ->where('withdraw_request_type',$withdraw_request_type)
            ->whereIn('withdraw_request_status',['inprocess','done'])
            ->get();
        return $oSelect;
    }

    public function getWithdrawByGroupCustomerIdObjFix($customer_contract_id,$type){
        $oResult = $this
            ->select(
                \DB::raw("SUM({$this->table}.withdraw_request_amount) as withdraw_request_total")
            )
            ->where($this->table.".withdraw_request_type",$type)
            ->where($this->table.".customer_contract_id",$customer_contract_id)
            ->where($this->table.".withdraw_request_status","done")
            ->get();
        return ($oResult!=null) ? intval($oResult[0]->withdraw_request_total) :0;
    }
}