<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class AccountStatementMapContractTable extends Model
{
    use ListTableTrait;
    protected $table = "account_statement_map_contract";
    protected $primaryKey = "account_statement_map_id";
    protected $fillable = [
        'account_statement_map_id',
        'account_statement_id',
        'customer_contract_id',
        'customer_contract_code',
        'total_amount',
        'amount_in',
        'amount_out'
    ];

    public function createAccountStatement($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    /**
     * Lấy chi tiết thông tin sao kê
     * @param $id
     */
    public function getDetail($id){
        $oSelect = $this
            ->join('account_statement','account_statement.account_statement_id','account_statement_map_contract.account_statement_id')
            ->where('account_statement.account_statement_id',$id)
            ->get();
        return $oSelect;
    }

}