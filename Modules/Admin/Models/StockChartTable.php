<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 10:45 AM
 * @author nhandt
 */


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StockChartTable extends Model
{
    use ListTableTrait;
    protected $table = "stock_chart";
    protected $primaryKey = "stock_chart_id";
    public $timestamps = false;

    protected $fillable = [
        "stock_chart_id",
        "value",
        "time",
        "timestamp",
        "created_at"
    ];




    /**
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filters = [])
    {
        $select = $this->select($this->table . ".*")
                    ->orderBy("timestamp",'ASC');
         if (isset($filters['startTime']) != "" && isset($filters['endTime'])) {
             $select->whereBetween($this->table . '.time', [$filters['startTime']. ' 00:00:00', $filters['endTime']. ' 23:59:59']);
            unset($filters['startTime']);
             unset($filters['endTime']);
         }
        return $select;

    }
    /**
     * @param array $data
     * @return mixed
     */
    public function findByTime($filter){
        $select = $this->select();
        if(isset($filter['stock_chart_id'])){
            $select = $select->where($this->primaryKey, "!=", $filter['stock_chart_id']);
        }
        $select = $select->where('time','like',"%{$filter['time']}%");
        return $select->first();

    }

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
//        dd($data);
        $add = $this->create($data);
        return $add->stock_chart_id;
    }
    public function edit($data,$id){
        $this->where($this->primaryKey,$id)->update($data);

    }
    public function remove($id){
        $this->where($this->primaryKey,$id)->first()->delete();

    }

}