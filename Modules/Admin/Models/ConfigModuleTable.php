<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ConfigModuleTable extends Model
{
    use ListTableTrait;
    protected $table = "config_module";
    protected $primaryKey = "config_module_id";
    protected $fillable = [
        'config_module_id',
        'key',
        'route_site',
        'name',
        'is_active',
        'updated_at',
        'update_by',
    ];

    public function getList(){
        $oSelect = $this->get();
        return $oSelect;
    }

    public function updateActive($id,$active) {
        $oSelect = $this->where('config_module_id',$id)->update(['is_active' => $active]);
        return $oSelect;
    }

}