<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class SendEmailTable extends Model
{
    use ListTableTrait;
    protected $table = "send_email";
    protected $primaryKey = "email_id";
    protected $fillable = [
        "id",
        "email",
        "email_name",
        "type",
        "is_actived",
        "created_at",
        "created_by",
    ];

    public function getEmailMain()
    {
        $oSelect = $this
            ->where('type','main')
            ->first();
        return $oSelect;
    }

    public function getEmailCC()
    {
        $oSelect = $this
            ->where('type','cc')
            ->get();
        return $oSelect;
    }

    public function deleteEmails()
    {
        $oSelect = $this->truncate();
        return $oSelect;
    }

    public function createEmail($data)
    {
        $oSelect = $this->insert($data);
        return $oSelect;
    }
}