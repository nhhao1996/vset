<?php


namespace Modules\Admin\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ReferSourceTable extends Model
{
    use ListTableTrait;
    protected $table = "refer_source";
    protected $primaryKey = "refer_source_id";
    protected $fillable = [
        'refer_source_id',
        'source',
        'source_name',
        'updated_at',
        'updated_by'
    ];

    public function getListAll(){
        $oSelect = $this->get();
        return $oSelect;
    }

    public function getListAllByKey($source){
        $oSelect = $this;
        if ($source != 'all') {
            $oSelect = $oSelect->where('source',$source);
        }
        return $oSelect->get();
    }

    public function getSource($id){
        $oSelect = $this->where('refer_source_id',$id)->first();
        return $oSelect;
    }

}