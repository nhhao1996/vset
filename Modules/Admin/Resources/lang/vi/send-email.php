<?php
return [
    'index' => [
        'title' => 'Cấu hình email nhận thông báo',
        'save' => 'Lưu',
        'cancel' => 'Hủy',
        'email_name' => 'Tên tài khoản',
        'email_main' => 'Email chính',
        'email_cc' => 'Email được cc',
        'active' => 'Trạng thái',
        'add_email' => 'Thêm email cc',
    ],
    'validate' => [
        'name_required' => 'Vui lòng nhập tên tài khoản',
        'name_max' => 'Tên tài khoản vượt quá 250 ký tự',
        'email_required' => 'Vui lòng nhập email chính',
        'email_type' => 'Vui lòng nhập lại email. Email chính sai định dạng',
        'email_max' => 'Email chính vượt quá 250 ký tự',
        'email_cc_required' => 'Vui lòng nhập email',
        'email_cc_max' => 'Email vượt quá 250 ký tự',
        'email_cc_type' => 'Vui lòng nhập lại email. Email sai định dạng',
        'email_success' => 'Cập nhật email thành công',
        'email_fail' => 'Cập nhật email thất bại',
        'cancel' => 'Hủy cập nhật',
        'yes_text' => 'Bạn có chắc muốn hủy hết mọi thay đổi của lần này',
        'yes' => 'Đồng ý',
        'no' => 'Không',
        'email_using' => 'Email cc trùng với email chính',
        'email_cc_using' => 'Email cc bị trùng'
    ]
];