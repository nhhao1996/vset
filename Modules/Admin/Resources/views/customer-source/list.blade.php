<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table ss--nowrap">
        <thead>
        <tr>
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Tên nguồn khách hàng')}}</th>
            <th class="ss--font-size-th">{{__('Mô tả nguồn khách hàng')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày tạo')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr>
                    @if(isset($page))
                        <td class="ss--font-size-13">{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td class="ss--font-size-13">{{ ($key + 1) }}</td>
                    @endif
                    <td class="ss--font-size-13" title="{{ $item['customer_source_name'] }}">{{ subString($item['customer_source_name'],50) }}</td>
                    <td class="ss--font-size-13" title="{{ $item['customer_source_description'] }}">{{ subString($item['customer_source_description'],50) }}</td>
                    <td class="ss--font-size-13">
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox" {{$item['is_actived'] == 1 ? 'checked' : ''}} disabled>
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td class="ss--font-size-13">{{ $item['created_at'] != null ? \Carbon\Carbon::parse($item['created_at'])->format('d/m/Y') : null }}</td>
                    <td class="ss--font-size-13">
                        @if(in_array('admin.customer-source.edit-submit',session('routeList')))
                            <button onclick="customerSource.edit({{$item['customer_source_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="Cập nhật"><i class="la la-edit"></i>
                            </button>
                        @endif
                        @if(in_array('admin.customer-source.remove',session('routeList')))
                            <button onclick="customerSource.remove(this, '{{ $item['customer_source_id'] }}')"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="Xóa"><i class="la la-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}