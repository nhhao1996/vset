<div class="modal-content">
    <div class="modal-header">
        <span class="m-portlet__head-icon">
            <i class="fa fa-plus-circle ss--icon-title-modal"></i>
        </span>
        <h4 class="modal-title ss--title m--font-bold">
            {{__('THÊM NGUỒN KHÁCH HÀNG')}}
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>
                {{__('Tên nguồn khách hàng')}}:<b class="text-danger">*</b>
            </label>
            <input type="text" id="customer_source_name" name="customer_source_name" class="form-control m-input" maxlength="100"
                   placeholder="{{__('Nhập tên nguồn khách hàng')}}">
            <span class="error-customer-source-name"></span>
        </div>
        <div class="form-group">
            <label>
                {{__('Mô tả nguồn khách hàng')}}:
            </label>
            <textarea rows="10" id="customer_source_description" name="customer_source_description" class="form-control m-input"
                   placeholder="{{__('Nhập mô tả nguồn khách hàng')}}"></textarea>
            <span class="error-customer-source-description"></span>
        </div>
        <div class="form-group">
            <label>
                {{__('Trạng thái')}} :
            </label>
            <div class="input-group">
                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                    <label>
                        <input id="is_actived_add" type="checkbox" checked class="manager-btn" name="">
                        <span></span>
                    </label>
                </span>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m--align-right">
                <button data-dismiss="modal"
                        class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                <span class="ss--text-btn-mobi">
                <i class="la la-arrow-left"></i>
                <span>{{__('HỦY')}}</span>
                </span>
                </button>

                <button type="button" onclick="customerSource.addClose()"
                        class="ss--btn-mobiles btn ss--button-cms-piospa m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-left-10 m--margin-bottom-5">
                <span class="ss--text-btn-mobi">
                    <i class="la la-check"></i>
                    <span>{{__('LƯU THÔNG TIN')}}</span>
                    </span>
                </button>

{{--                <button type="button" onclick="customerSource.add()"--}}
{{--                        class="ss--btn-mobiles btn ss--button-cms-piospa m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-left-10 m--margin-bottom-5">--}}
{{--                <span class="ss--text-btn-mobi">--}}
{{--                    <i class="fa fa-plus-circle"></i>--}}
{{--                    <span>{{__('LƯU & TẠO MỚI')}}</span>--}}
{{--                    </span>--}}
{{--                </button>--}}
            </div>
        </div>
    </div>
</div>

