@extends('layout')
@section('title_header')
{{--    <span class="title_header"><img--}}
{{--                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""--}}
{{--                style="height: 20px;"></span>--}}
@endsection
@section("after_css")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('content')
    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/

        .form-control-feedback {
            color: red;
        }

        .title_header {
            color: #008990;
            font-weight: 400;
        }
    </style>
    <!--begin::Portlet-->
    <div class="m-portlet" id="autotable">
{{--        <div class="m-portlet__head">--}}
{{--            <div class="m-portlet__head-caption">--}}
{{--                <div class="m-portlet__head-title">--}}
{{--                   <span class="m-portlet__head-icon">--}}
{{--                                <i class="la la-th-list"></i>--}}
{{--                             </span>--}}
{{--                    <h3 class="m-portlet__head-text">--}}
{{--                    </h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="m-portlet__head-tools">--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="m-portlet__body">
            <h3 class="text-center">{{__('')}}</h3>
        </div>
    </div>
@stop
