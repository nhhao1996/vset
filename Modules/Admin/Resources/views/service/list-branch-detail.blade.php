<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default" id="table_branch">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Chi nhánh')}}</th>
            <th class="tr_thead_list">{{__('Giá dịch vụ')}}</th>
            <th class="tr_thead_list">{{__('Giá chi nhánh')}}</th>

        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @foreach($itemBranch as $key=>$value)
            <tr>
                <td>
                @if(isset($page))
                    {{ ($page-1)*10 + $key+1}}
                @else
                    {{$key+1}}
                @endif
                <input type="hidden" id="service_branch_price_id"
                       name="service_branch_price_id"
                       value="{{$value['service_branch_price_id']}}">
                </td>
                <td>{{$value['branch_name']}}<input type="hidden"
                                                    value="{{$value['branch_id']}}"></td>
                <td>
                    {{number_format($value['old_price'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}
                    <input type="hidden" value="{{$value['old_price']}}">
                </td>
                <td>{{number_format($value['new_price'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}</td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$itemBranch->links('helpers.paging') }}
