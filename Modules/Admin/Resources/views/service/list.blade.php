<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            {{--<th>Mã</th>--}}
            {{--<th class="tr_thead_list"></th>--}}
            <th class="tr_thead_list">{{__('Dịch vụ')}}</th>
            <th class="tr_thead_list">{{__('Nhóm')}}</th>
            <th class="tr_thead_list">{{__('Thời gian')}}</th>
{{--            <th class="tr_thead_list">{{__('Tình trạng')}}</th>--}}
            <th class="tr_thead_list"></th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    @if(isset($page))
                        <td>{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td>{{$key+1}}</td>
                    @endif
                    {{--<td>{{$item['service_code']}}</td>--}}
                    {{--<td>--}}

                    {{--@if($item['service_avatar']!=null)--}}
                    {{--<img src="/{{$item['service_avatar']}}" title="" width="50px"--}}
                    {{--class="img-cl">--}}
                    {{--@else--}}
                    {{--<img src="https://thumbs.dreamstime.com/z/beautiful-woman-spa-healthcare-salon-therapy-icons-face-vector-illustration-44039775.jpg"--}}
                    {{--title="" width="50px"--}}
                    {{--class="img-cl">--}}
                    {{--@endif--}}

                    {{--</td>--}}
                    <td>
                        @if($item['number']>0)
                            <i class="la la-check"></i>
                        @endif
                        <a class="m-link" style="color:#464646" title="{{__('Xem chi tiết')}}"
                           href='{{route("admin.service.detail",$item['service_id'])}}'>
                            {{$item['service_name']}}
                        </a>

                    </td>
                    <td>{{$item['name']}}</td>
                    <td>{{$item['time']}} {{__('phút')}}</td>
{{--                    <td>--}}
{{--                        @if(in_array('admin.service.change-status',session('routeList')))--}}
{{--                            @if ($item['is_actived'])--}}
{{--                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label style="margin: 0 0 0 10px; padding-top: 4px">--}}
{{--                                    <input type="checkbox"--}}
{{--                                           onclick="service.changeStatus(this, '{!! $item['service_id'] !!}', 'publish')"--}}
{{--                                           checked class="manager-btn" name="">--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </span>--}}
{{--                            @else--}}
{{--                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label style="margin: 0 0 0 10px; padding-top: 4px">--}}
{{--                                    <input type="checkbox"--}}
{{--                                           onclick="service.changeStatus(this, '{!! $item['service_id'] !!}', 'unPublish')"--}}
{{--                                           class="manager-btn" name="">--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </span>--}}
{{--                            @endif--}}
{{--                        @else--}}
{{--                            @if ($item['is_actived'])--}}
{{--                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label style="margin: 0 0 0 10px; padding-top: 4px">--}}
{{--                                    <input type="checkbox"--}}
{{--                                           checked class="manager-btn" name="">--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </span>--}}
{{--                            @else--}}
{{--                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label style="margin: 0 0 0 10px; padding-top: 4px">--}}
{{--                                    <input type="checkbox"--}}
{{--                                           class="manager-btn" name="">--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </span>--}}
{{--                            @endif--}}
{{--                        @endif--}}
{{--                    </td>--}}
                    <td>
                        @if(in_array('admin.service.edit',session('routeList')))
                            <a href="{{route('admin.service.edit',array ('id'=>$item['service_id']))}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Cập nhật')}}">
                                <i class="la la-edit"></i>
                            </a>
                        @endif
                        @if(in_array('admin.service.remove',session('routeList')))
                            <button onclick="service.remove(this, {{$item['service_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="{{__('Xóa')}}">
                                <i class="la la-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}
