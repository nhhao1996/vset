<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('STT')}}</th>
            <th class="ss--font-size-th">{{__('Mã yêu cầu mua')}}</th>
            <th class="ss--font-size-th">{{__('Nhà đầu tư')}}</th>
            <th class="ss--font-size-th">{{__('SĐT ')}}</th>
            <th class="ss--font-size-th">{{__('Tên gói')}}</th>
            <th class="ss--font-size-th">{{__('Loại')}}</th>
            <th class="ss--font-size-th">{{__('Số lượng')}}</th>
            <th class="ss--font-size-th">{{__('Thành tiền (vnđ)')}}</th>
            <th class="ss--font-size-th">{{__('Hình thức thanh toán ')}}</th>
            <th class="ss--font-size-th">{{__('Ngày khởi tạo ')}}</th>
            <th class="ss--font-size-th">{{__('Hành Động ')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                        <td>{{$item['order_code']}}</td>
                        <td>{{$item['customer_name']}}</td>
                        <td>{{$item['customer_phone']}}</td>
                        <td>{{$item['product_name_vi']}}</td>
                        <td>{{$item['type_bonds']}}</td>
                        <td>{{$item['quantity']}}</td>
                        <td>{{number_format($item['total'])}}</td>
                        <td>{{$item['payment_method_name_vi']}}</td>
                        <td>{{ Carbon\Carbon::parse($item['created_at'])->format('H:i:s d-m-Y') }}</td>
                        <td>
{{--                            @if(in_array('admin.buy-bonds-request-fail.view', session('routeList')))--}}
                                <a href="{{route('admin.buy-bonds-request-fail.view',['id'=>$item['order_id']])}}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="{{__('Chi tiết')}}">
                                    <i class="flaticon-eye"></i>
                                </a>
{{--                            @endif--}}
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}