@extends('layout') @section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection @section('title_header')
    <span class="title_header">
    <img src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;" />
    {{__('QUẢN LÝ YÊU CẦU MUA GÓI ĐẦU TƯ - TIẾT KIỆM THẤT BẠI')}}
</span>
@endsection @section('content')
    <meta http-equiv="refresh" content="number" />
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <form class="frmFilter">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            {{__('DANH SÁCH YÊU CẦU MUA GÓI ĐẦU TƯ - TIẾT KIỆM THẤT BẠI')}}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools nt-class">
                    <a href="{{route('admin.buy-bonds-request-fail.export')}}" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                        <span>
                            <i class="fa fa-file-export m--margin-right-5"></i>
                            <span> Export</span>
                        </span>
                    </a>
                </div>
            </div>
            <div class="card-header tab-card-header ">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                    {{--                <li class="nav-item">--}}
                    {{--                    <a class="nav-link active show" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="All" aria-selected="true">Tất cả</a>--}}
                    {{--                </li>--}}
                    <li class="nav-item">
                        <a class="nav-link " id="one-tab" href="{{route('admin.buy-bonds-request')}}" >Yêu cầu thành công</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active show" id="two-tab" href="{{route('admin.buy-bonds-request-fail')}}" >Yêu cầu thất bại</a>
                    </li>
                </ul>
            </div>
            <div class="m-portlet__body">
                <div class="bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="{{__('Nhập tên, mã, sdt khách hàng')}}" value="{{isset($FILTER['search']) ? $FILTER['search'] : null}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 form-group">
                            <select style="width: 100%;" name="type_bonds" class="form-control m-input ss--select-2">
                                <option value="">Loại yêu cầu</option>
                                <option value="2" {{isset($FILTER['type_bonds']) && $FILTER['type_bonds'] == 2 ? 'checked' : ''}}>Tiết Kiệm</option>
                                <option value="1"  {{isset($FILTER['type_bonds']) && $FILTER['type_bonds'] == 1 ? 'checked' : ''}}>Hợp tác đầu tư</option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group" style="background-color: white;">
                                <div class="m-input-icon m-input-icon--right">
                                    <input readonly="" class="form-control m-input daterange-picker" id="created_at" name="created_at" autocomplete="off" placeholder="{{__('Ngày khởi tạo')}}" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button class="btn btn-primary color_button btn-search">{{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i></button>
                                <a href="{{route('admin.buy-bonds-request-fail')}}" class="btn btn-metal btn-search padding9x">
                                <span>
                                    <i class="flaticon-refresh"></i>
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>

                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible"><strong>{{__('Success')}} : </strong> {!! session('status') !!}.</div>
                    @endif
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::buy-bonds-request-fail.list')
                </div>
                <!-- end table-content -->
            </div>
        </form>
    </div>
@endsection @section('after_script')
    <script src="{{asset('static/backend/js/admin/buy-bonds-request-fail/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
@stop
