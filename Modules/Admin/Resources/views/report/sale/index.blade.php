@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-thong-ke.png')}}" alt=""
                style="height: 20px;"> {{__('BÁO CÁO')}}</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .total-money {
            background-image: url({{asset("uploads/admin/report/hinh3.jpg")}});
            background-size: cover;
        }

        .align-conter1 {
            text-align: center;
        }

        .ss--text-white {
            color: white !important;
        }
    </style>

    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('BÁO CÁO DOANH THU NHÂN VIÊN')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body" id="autotable">
            <form id="sale-form">
                <div class="row form-group">
                    <span class="mb-0 pt-2">@lang('Từ'):</span>
                    <div class="col-lg-2">
                        <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                            <input readonly=""
                                   class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                   id="month_start"
                                   name="month_start"
                                   autocomplete="off"
                                   placeholder="Tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                   onchange="sales.filter()"
                            >
                        </div>
                    </div>
                    <span class="mb-0 pt-2">@lang('Đến'):</span>
                    <div class="col-lg-2">
                        <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                            <input readonly=""
                                   class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                   id="month_end"
                                   name="month_end"
                                   autocomplete="off"
                                   placeholder="Tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                   onchange="sales.filter()"
                            >
                        </div>
                    </div>
                    <span class="mb-0 pt-2">@lang('Nhân viên'):</span>
                    <div class="col-lg-2 form-group">
                        <select name="staff_id" onchange="sales.filter()"
                                style="width: 100%"
                                id="source"
                                class="form-control"
                                onchange="sales.filter()">
                            <option value="0">N/A</option>
                            @foreach($listStaff as $item)
                                <option value="{{$item['staff_id']}}">{{$item['full_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-lg-12 form-group">
                    <div id="sales" style="height: 600px;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h5>Doanh thu theo từng nhân viên</h5>
                </div>
                <div class="col-12 mt-3">
                    <div class="row">
                        <div class="col-6">
                            <p>Tổng doanh thu : <span class="money-sale"></span></p>
                            <div id="chartSale" style="height: 600px;"></div>
                        </div>
                        <div class="col-6">
                            <form id="filter-sale">
                                <div class="row form-group">
                                    <span class="mb-0 pt-2">@lang('Từ'):</span>
                                    <div class="col-lg-4">
                                        <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                                            <input readonly=""
                                                   class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                                   id="month_start_sale"
                                                   name="month_start_sale"
                                                   autocomplete="off"
                                                   placeholder="Tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                                   onchange="sales.filterSale()"
                                            >
                                        </div>
                                    </div>
                                    <span class="mb-0 pt-2">@lang('Đến'):</span>
                                    <div class="col-lg-4">
                                        <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                                            <input readonly=""
                                                   class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                                   id="month_end_sale"
                                                   name="month_end_sale"
                                                   autocomplete="off"
                                                   placeholder="Tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                                   onchange="sales.filterSale()"
                                            >
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-12 table-sale">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h5>Doanh thu theo từng nhóm nhân viên</h5>
                </div>
                <div class="col-12 mt-3">
                    <div class="row">
                        <div class="col-6">
                            <p>Tổng doanh thu : <span class="money-sale-group"></span></p>
                            <div id="chartGroup" style="height: 600px;"></div>
                        </div>
                        <div class="col-6">
                            <form id="filter-group">
                                <div class="row form-group">
                                    <span class="mb-0 pt-2">@lang('Từ'):</span>
                                    <div class="col-lg-4">
                                        <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                                            <input readonly=""
                                                   class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                                   id="month_start_group"
                                                   name="month_start_group"
                                                   autocomplete="off"
                                                   placeholder="Tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                                   onchange="sales.filterGroup()"
                                            >
                                        </div>
                                    </div>
                                    <span class="mb-0 pt-2">@lang('Đến'):</span>
                                    <div class="col-lg-4">
                                        <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                                            <input readonly=""
                                                   class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                                   id="month_end_group"
                                                   name="month_end_group"
                                                   autocomplete="off"
                                                   placeholder="Tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                                   onchange="sales.filterGroup()"
                                            >
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-12 table-group">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
{{--    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>--}}
{{--    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>--}}


    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"
            type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/loader.js?v='.time())}}"
            type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('static/backend/js/admin/report/sales/script.js?v='.time())}}"
            type="text/javascript"></script>




@stop
