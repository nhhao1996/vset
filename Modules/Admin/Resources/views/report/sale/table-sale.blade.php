<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list text-center">#</th>
            <th class="tr_thead_list ">Họ và tên</th>
            <th class="tr_thead_list ">Doanh thu</th>
        </tr>
        </thead>
        <tbody>
            @foreach($listSale as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle text-center">{{ ($page-1)*$display + $key+1}}</td>
                    @else
                        <td class="text_middle text-center">{{$key+1}}</td>
                    @endif
                    <td>{{$item['name'] == '' ? 'Khác' : $item['name']}}</td>
                    <td>{{number_format($item['money'],3)}} VND</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $listSale->links('admin::report.sale.helpers.paging-sale') }}