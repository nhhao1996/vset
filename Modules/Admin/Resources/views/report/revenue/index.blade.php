@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-thong-ke.png')}}" alt=""
                style="height: 20px;"> {{__('BÁO CÁO')}}</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .total-money {
            background-image: url({{asset("uploads/admin/report/hinh3.jpg")}});
            background-size: cover;
        }

        .align-conter1 {
            text-align: center;
        }

        .ss--text-white {
            color: white !important;
        }
    </style>

    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('BÁO CÁO DOANH THU - THANH TOÁN')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body" id="autotable">
            <div class="row form-group">
                <div class="col-lg-4 align-conter1 ss--text-white form-group">
                    <div class="total-money">
                        <div class="m--padding-30">
                            <h6 class="ss--font-size-12">
                                {{__('TỔNG DOANH THU')}}
                            </h6>
                            <h3 class="ss--font-size-18" id="total_money">
                                0 VNĐ
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 form-group"></div>
                <div class="col-lg-3 form-group">
                    <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                        <input readonly=""
                               class="form-control m-input daterange-picker ss--search-datetime-hd"
                               id="time"
                               name="time"
                               autocomplete="off"
                               placeholder="Từ ngày - đến ngày">
                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                <span><i class="la la-calendar"></i></span></span>
                    </div>
                </div>
                <div class="col-lg-2 form-group">
                    <select name="year"
                            style="width: 100%"
                            id="year"
                            class="form-control"
                            onchange="revenue.onchangeYear(this)">
                        <option value="">
                            {{__('Năm')}}
                        </option>
                        @for($i = 0; $i <= 4; $i++)
                            <option value="{{\Carbon\Carbon::now()->subYear($i)->year}}">
                                {{ \Carbon\Carbon::now()->subYear($i)->year}}
                            </option>
                        @endfor
                    </select>
                </div>
                <div class="col-lg-2 form-group">
                    <select name="type"
                            style="width: 100%"
                            id="type"
                            class="form-control"
                            onchange="revenue.filter()">
                        <option value="">
                            {{__('Tất cả')}}
                        </option>
                        <option value="1">
                            {{__('Hợp tác đầu tư')}}
                        </option>
                        <option value="2">
                            {{__('Tiết kiệm')}}
                        </option>
                        <option value="3">
                            {{__('Dịch vụ')}}
                        </option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-group">
                    <div id="revenue" style="height: 600px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row m--font-bold align-conter1">
        <div class="col-lg-4">
            <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                <div class="form-group m-form__group ss--margin-bottom-0">
                    <label class="m--margin-top-20 ss--text-center ss--font-weight-400">
                        {{__('PHƯƠNG THỨC THANH TOÁN')}}
                    </label>
                </div>
                <div id="pie_chart_payment_method"
{{--                     style="min-width: 290px; height: 290px; max-width: 290px; margin: 0 auto">--}}
                     style="height: 300px">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" readonly="" class="form-control m-input daterange-picker"
           id="time-hidden" name="time-hidden" autocomplete="off">
@stop
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/loader.js?v='.time())}}"
            type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/report/revenue/script.js?v='.time())}}"
            type="text/javascript"></script>
@stop
