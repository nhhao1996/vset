@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-thong-ke.png')}}" alt=""
                style="height: 20px;"> {{__('BÁO CÁO')}}</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .total-money {
            background-image: url({{asset("uploads/admin/report/hinh3.jpg")}});
            background-size: cover;
        }

        .align-conter1 {
            text-align: center;
        }

        .ss--text-white {
            color: white !important;
        }
    </style>

    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('BÁO CÁO NHÀ ĐẦU TƯ ĐĂNG KÝ TỪ REFERRAL LINK')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body" id="autotable">
            <div class="row form-group">
                <div class="col-lg-3 form-group">
                    <div class="m-input-icon m-input-icon--right" id="m_daterangepicker_6">
                        <input readonly=""
                               class="form-control m-input daterange-picker ss--search-datetime-hd"
                               id="time"
                               name="time"
                               autocomplete="off"
                               placeholder="Từ ngày - đến ngày">
                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                <span><i class="la la-calendar"></i></span></span>
                    </div>
                </div>
                <span class="mb-0 pt-2">Chọn nguồn khách hàng</span>
                <div class="col-lg-2 form-group">
                    <select name="type"
                            style="width: 100%"
                            id="source"
                            class="form-control"
                            onchange="shareLink.filter()">
                        <option value="all">Tất cả</option>
                        <option value="na">Khác</option>
                        @foreach($listSource as $item)
                            <option value="{{$item['source']}}">{{$item['source_name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-group" style="overflow-x:auto">
                    <div id="share-link" style="height: 601px;"></div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" readonly="" class="form-control m-input daterange-picker"
           id="time-hidden" name="time-hidden" autocomplete="off">
@stop
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/loader.js?v='.time())}}"
            type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('static/backend/js/admin/report/share-link/script.js?v='.time())}}"
            type="text/javascript"></script>
@stop
