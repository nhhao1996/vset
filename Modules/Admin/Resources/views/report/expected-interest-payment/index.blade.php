@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-thong-ke.png')}}" alt=""
                style="height: 20px;"> {{__('BÁO CÁO')}}</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .total-money {
            background-image: url({{asset("uploads/admin/report/hinh3.jpg")}});
            background-size: cover;
        }

        .align-conter1 {
            text-align: center;
        }

        .ss--text-white {
            color: white !important;
        }
        /*.amcharts-chart-div svg a {*/
        /*    display: none;*/
        /*}*/
        .select_week .select2 {
            width:20% !important;
        }
        #chart ,#chart .amcharts-chart-div {
            overflow: unset !important;
        }
        /*#chart svg {*/
        /*    height: 400px !important;*/
        /*}*/
    </style>

    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('BÁO CÁO TRẢ LÃI DỰ KIẾN')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                {{--                @if(in_array('admin.customer.export',session('routeList')))--}}
                <a href="{{route('admin.report.expectedInterestPayment.export')}}"
                   class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                                <span>
                                    <i class="fa fa-file-export m--margin-right-5"></i>
                                    <span> {{__('Export')}}</span>
                                </span>
                </a>
                {{--                @endif--}}
            </div>
        </div>
        <div class="m-portlet__body pb-5" id="autotable">
            <form id="form-chart">
                <div class="row form-group">
                    {{--                    <div class="col-lg-2 col-md-4 form-group"></div>--}}
                    <div class="col-lg-2 col-md-3 form-group">
                        <select name="select_type" class="form-control" id="select_type" onchange="expectedInterestPayment.filter()">
                            <option value="day">Theo ngày</option>
                            <option value="week">Theo tuần</option>
                            <option value="month">Theo tháng</option>
                        </select>
                    </div>
                    {{--                Ngày --}}
                    <div class="col-lg-3 col-md-4 form-group ">
                        <div class="m-input-icon m-input-icon--right day_block block_hide" id="m_daterangepicker_6">
                            <input readonly=""
                                   class="form-control m-input daterange-picker ss--search-datetime-hd pr-3"
                                   id="time-expected"
                                   name="time-expected"
                                   autocomplete="off"
                                   placeholder="Từ ngày - đến ngày" data-bind="daterangepicker: dateRange"
                                   onchange="expectedInterestPayment.filter()">
                        </div>
                        <div class="week_block block_hide" style="display: none">
                            <select name="week" class="form-control" id="week" onchange="expectedInterestPayment.filter()">
                                <option value="week_now">{{__('Tuần hiện tại')}}</option>
                                <option value="week_after">{{__('Tuần sau')}}</option>
                                <option value="month_after">{{__('Tháng sau')}}</option>
                                <option value="select_week">{{__('Chọn tuần')}}</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="m-input-icon month_block block_hide col-5" style="display: none">
                                <input readonly=""
                                       class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                       id="month_start"
                                       name="month_start"
                                       autocomplete="off"
                                       placeholder="Từ tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                       onchange="expectedInterestPayment.filter()">
                            </div>
                            <div class="col-2 month_block block_hide text-center" style="display: none"><p class="mb-0 pt-2"> _ </p></div>
                            <div class="m-input-icon month_block block_hide col-5" style="display: none">
                                <input readonly=""
                                       class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                       id="month_end"
                                       name="month_end"
                                       autocomplete="off"
                                       placeholder="Đến tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                       onchange="expectedInterestPayment.filter()">
                            </div>
                        </div>
                        {{--                    <div class="month_block block_hide" style="display: none">--}}
                        {{--                        <select name="year"--}}
                        {{--                                style="width: 100%"--}}
                        {{--                                id="year"--}}
                        {{--                                class="form-control"--}}
                        {{--                                onchange="expectedInterestPayment.filter()">--}}
                        {{--                            @for($i = 0; $i < 5; $i++)--}}
                        {{--                                <option value="{{\Carbon\Carbon::now()->addMonth($i)->month}}">--}}
                        {{--                                    {{ \Carbon\Carbon::now()->addMonth($i)->month}}--}}
                        {{--                                </option>--}}
                        {{--                            @endfor--}}
                        {{--                        </select>--}}
                        {{--                    </div>--}}
                    </div>

                    <div class="col-lg-7 col-md-4 form-group select_week pr-0" style="display: none">
                        {{--                        <input name="week_before" onfocusout="expectedInterestPayment.filter()" type="text" class="form-control m-0 d-inline" style="width:20%" placeholder="Tuần số">--}}
                        {{--                        <input name="year_before" onfocusout="expectedInterestPayment.filter()" type="text" class="form-control m-0 d-inline"style="width:20%" placeholder="Năm"> ---}}
                        {{--                        <input name="week_after" onfocusout="expectedInterestPayment.filter()" type="text" class="form-control m-0 d-inline" style="width:20%" placeholder="Tuần số">--}}
                        {{--                        <input name="year_after" onfocusout="expectedInterestPayment.filter()" type="text" class="form-control m-0 d-inline" style="width:20%" placeholder="Năm">--}}
                        <select name="week_before" class="form-control" onchange="expectedInterestPayment.filter()">
                            @for($i = 1 ; $i <= 53 ; $i++)
                                <option value="{{$i}}" {{\Carbon\Carbon::now()->weekOfYear == $i ? 'selected' : ''}}>Tuần {{$i}}</option>
                            @endfor
                        </select>
                        <select name="year_before" class="form-control" onchange="expectedInterestPayment.filter()">
                            @for($i = \Carbon\Carbon::now()->year ; $i <= \Carbon\Carbon::now()->addYears(2)->year ; $i++)
                                <option value="{{$i}}">Năm {{$i}}</option>
                            @endfor
                        </select>
                        <p class="pl-3 pr-3 d-inline-block">_</p>
                        <select name="week_after" class="form-control" onchange="expectedInterestPayment.filter()">
                            @for($i = 1 ; $i <= 53 ; $i++)
                                <option value="{{$i}}" {{\Carbon\Carbon::now()->weekOfYear == $i ? 'selected' : ''}}>Tuần {{$i}}</option>
                            @endfor
                        </select>
                        <select name="year_after" class="form-control" onchange="expectedInterestPayment.filter()">
                            @for($i = \Carbon\Carbon::now()->year ; $i <= \Carbon\Carbon::now()->addYears(2)->year ; $i++)
                                <option value="{{$i}}">Năm {{$i}}</option>
                            @endfor
                        </select>
                    </div>

                </div>
            </form>
            <div class="row">
                <div class="col-lg-12 form-group">
                    <div id="chart" style="height: 600px;"></div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" readonly="" class="form-control m-input daterange-picker"
           id="time-hidden" name="time-hidden" autocomplete="off">
    <input type="hidden" readonly="" class="form-control m-input datemonthrange-picker"
           id="month-hidden" name="month-hidden" autocomplete="off">
@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('after_script')
    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"
            type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/loader.js?v='.time())}}"
            type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/report/expected-interest-payment/script.js?v='.time())}}"
            type="text/javascript"></script>
    <script>
        $('select').select2();
    </script>
@stop