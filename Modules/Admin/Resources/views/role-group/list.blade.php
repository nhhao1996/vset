<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table ss--nowrap">
        <thead>
        <tr>
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('TÊN NHÓM QUYỀN')}}</th>
            <th class="ss--text-center ss--font-size-th">{{__('TRẠNG THÁI')}}</th>
            <th class="ss--text-center ss--font-size-th">{{__('NGÀY TẠO')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr>
                    <td class="ss--font-size-13">{{ ($key+1) }}</td>
                    <td class="ss--font-size-13">{{ $item['name'] }}</td>
                    <td class="ss--text-center ss--font-size-13">
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label class="ss--switch">
                                <input  type="checkbox" {{$item['is_actived'] == 1 ? 'checked' :''}} disabled
                                        class="manager-btn" name="">
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td class="ss--text-center ss--font-size-13">
                        {{(new DateTime($item['created_at']))->format('d/m/Y')}}
                    </td>
                    <td class="">
                        @if(in_array('admin.role-group.edit', session()->get('routeList')))
                            <a href="{{route('admin.authorization.edit', array('id'=>$item['id']))}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="{{__('Chỉnh quyền')}}">
                                <i class="la la-user-secret"></i></a>
                            <button onclick="roleGroup.edit({{$item['id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="{{__('Cập nhật')}}"><i class="la la-edit"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{--{{ $LIST->links('helpers.paging') }}--}}