@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('Quản lý thông tin hướng dẫn')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('Chi tiết  thông tin hướng dẫn')}}
                    </h2>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-12">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Id Câu hỏi:
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['faq_id']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Faq group:
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['faq_group']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Faq Type:
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['faq_type']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Faq tiêu đề:
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['faq_title']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Faq Content:
                        </label>
                        <div class="col-lg-10">
                            <textarea class="form-control" rows="6" disabled> {{$detail['faq_content']}}</textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Faq position:
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['faq_position']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Faq actived:
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['is_actived'] =='1' ? 'Active' : 'DeActive'}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Người  tạo :
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['staffs_create_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Người cập nhật :
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['updated_by']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Ngày tạo :
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['created_at']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-2 black-title">
                            Ngày cập nhật :
                        </label>
                        <div class="col-lg-10">
                            <input class="form-control" value="{{$detail['staffs_update_name']}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
            <div class="m-portlet__foot">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.support-management')}}"
                           class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('QUAY LẠI ')}}</span>
						</span>
                        </a>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
@stop