<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Nhóm Faq')}}</th>
            <th class="ss--font-size-th">{{__('Loại Faq')}}</th>
            <th class="ss--font-size-th">{{__('Tiêu đề')}}</th>
            <th class="ss--font-size-th">{{__('Thứ tự hiển thị')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày tạo ')}}</th>
            <th class="ss--font-size-th">{{__('Ngày cập nhật ')}}</th>

        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                        <td>{{$item['parent_id']}}</td>
                        <td>{{$item['faq_group_type']}}</td>
                        <td>{{$item['faq_group_title']}}</td>
                        <td>{{$item['faq_group_position']}}</td>
                        <td>{{$item['is_actived']}}</td>
                        <td>{{$item['created_at']}}</td>
                        <td>{{$item['updated_at']}}</td>

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}