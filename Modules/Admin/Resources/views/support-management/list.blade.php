<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Nhóm Faq')}}</th>
            <th class="ss--font-size-th">{{__('Loại Faq')}}</th>
            <th class="ss--font-size-th">{{__('Tiêu đề')}}</th>
            <th class="ss--font-size-th">{{__('Thứ tự hiển thị')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày tạo ')}}</th>
            <th class="ss--font-size-th">{{__('Ngày cập nhật ')}}</th>
            <th class="ss--font-size-th">{{__('Hành động ')}}</th>

        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
{{--                {{dd($LIST)}}--}}
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                        <td>{{$item['faq_group']}}</td>
                        <td>{{$item['faq_type']}}</td>
                        <td>{{$item['faq_title']}}</td>
                        <td>{{$item['faq_position']}}</td>
                        <td>{{$item['is_actived'] =='1' ? 'Active' : 'DeActive'}}</td>
                        <td>{{$item['created_at']}}</td>
                        <td>{{$item['updated_at']}}</td>
                        <td>
                            @if(in_array('admin.support-management.view', session('routeList')))
                                <a href="{{route('admin.support-management.view',['id'=>$item['faq_id']])}}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="{{__('Cập nhật')}}">
                                    <i class="flaticon-eye"></i>
                                </a>
                            @endif
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}