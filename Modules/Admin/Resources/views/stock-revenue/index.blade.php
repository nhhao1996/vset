@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-thong-ke.png')}}" alt=""
                style="height: 20px;"> {{__('BÁO CÁO')}}</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .total-money {
            background-image: url({{asset("uploads/admin/report/hinh3.jpg")}});
            background-size: cover;
        }

        .align-conter1 {
            text-align: center;
        }

        .ss--text-white {
            color: white !important;
        }
        /*.amcharts-chart-div svg a {*/
        /*    display: none;*/
        /*}*/
        .select_week .select2 {
            width:20% !important;
        }
        #chart ,#chart .amcharts-chart-div {
            overflow: unset !important;
        }
        /*#chart svg {*/
        /*    height: 400px !important;*/
        /*}*/
    </style>

    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('BÁO CÁO DOANH THU THANH TOÁN CỔ PHIẾU')}}
                    </h2>
                </div>
            </div>
        </div>
        <div class="m-portlet__body pb-5" id="autotable">
            <form id="form-chart">
                <div class="row form-group">
                    <div class="col-lg-2 col-md-3 form-group">
                        <select name="filter_type" class="form-control" id="filter_type" onchange="stockRevenue.filter()">
                            <option value="day">Báo cáo theo ngày</option>
                            <option value="month">Báo cáo theo tháng</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-4 form-group ">
                        <div class="m-input-icon m-input-icon--right day_block block_hide" id="m_daterangepicker_6">
                            <input readonly=""
                                   class="form-control m-input daterange-picker ss--search-datetime-hd pr-3"
                                   id="time"
                                   name="time"
                                   autocomplete="off"
                                   placeholder="Từ ngày - đến ngày" data-bind="daterangepicker: dateRange"
                                   onchange="stockRevenue.filter()">
                        </div>
                        <div class="row">
                            <div class="m-input-icon month_block block_hide col-5" style="display: none">
                                <input readonly=""
                                       class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                       id="month_start"
                                       name="month_start"
                                       autocomplete="off"
                                       placeholder="Từ tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                       onchange="stockRevenue.filter()">
                            </div>
                            <div class="col-2 month_block block_hide text-center" style="display: none"><p class="mb-0 pt-2"> _ </p></div>
                            <div class="m-input-icon month_block block_hide col-5" style="display: none">
                                <input readonly=""
                                       class="form-control m-input datemonthrange-picker ss--search-datetime-hd"
                                       id="month_end"
                                       name="month_end"
                                       autocomplete="off"
                                       placeholder="Đến tháng" value="{{\Carbon\Carbon::now()->format('m/Y')}}"
                                       onchange="stockRevenue.filter()">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group row">
                <div class="col-lg-12 form-group">
                    <div id="chart" style="height: 600px;"></div>
                </div>
            </div>
            <div class="form-group float-right" id="radio">
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" checked name="optradio" value="revenue">Doanh thu
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optradio" value="count-transaction">Số lượng giao dịch
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optradio" value="market-transaction">Giao dịch trên chợ
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="optradio" value="transfer-transaction">Giao dịch chuyển nhượng
                    </label>
                </div>
            </div>

        </div>
        <div class="m-portlet__body pb-5 row form-group">
            <div class="col-lg-6 form-group bg" id="text_description">

            </div>
            <div class="col-lg-6 form-group">
                <div id="payment_method_chart">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" readonly="" class="form-control m-input daterange-picker"
           id="time-hidden" name="time-hidden" autocomplete="off">
    <input type="hidden" readonly="" class="form-control m-input datemonthrange-picker"
           id="month-hidden" name="month-hidden" autocomplete="off">
@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/report/highcharts.js')}}"></script>
    <script src="{{asset('static/backend/js/admin/stock-revenue/script.js?v='.time())}}"
            type="text/javascript"></script>
    <script>
        $('select').select2();
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
@stop