{{--<div class="tab-content">--}}
{{--<div class="tab-pane active" id="m_widget4_tab1_content">--}}
{{--<!--begin::Widget 14-->--}}
{{--<div class="m-widget4 append">--}}
{{--<!--begin::Widget 14 Item-->--}}
{{--@foreach($list as $item)--}}

{{--<div class="m-widget4__item">--}}
{{--<div class="m-widget4__img m-widget4__img--pic">--}}
{{--@if($item['avatar']!=null)--}}
{{--<img src="{{asset($item['avatar'])}}" height="52px" width="52px">--}}
{{--@else--}}
{{--<img src="{{asset('uploads/admin/icon/default-placeholder.png')}}">--}}
{{--@endif--}}
{{--</div>--}}
{{--<div class="m-widget4__info">--}}
{{--<span class="m-widget4__title">--}}
{{--{{$item['name']}}--}}
{{--</span><br>--}}
{{--<span class="m-widget4__sub m--font-bolder m--font-success">--}}
{{--{{($item['price'])}}đ--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="m-widget4__ext">--}}
{{--<a href="javascript:void(0)"--}}
{{--onclick="order.append_table('{{$item['id']}}','{{$item['price_hidden']}}','{{$item['type']}}','{{$item['name']}}','{{$item['code']}}')"--}}
{{--class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary">--}}
{{--Chọn--}}
{{--</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--@endforeach--}}
{{--<!--end::Widget 14 Item-->--}}
{{--</div>--}}
{{--<!--end::Widget 14-->--}}
{{--</div>--}}

{{--</div>--}}

<div class="tab-content">
    <div class="tab-pane active" id="m_widget4_tab1_content">
        <!--begin::Widget 14-->
        <div class="row append m--margin-left-10">
            @foreach($list as $item)
                @if(isset($item['is_sale']) && $item['is_sale'] == 1)
                    <div class="info-box col-lg-3 m--margin-bottom-10"
                         onclick="order.append_table('{{$item['id']}}','{{($item['promotion_price'])}}','{{$item['type']}}','{{$item['name']}}','{{$item['code']}}')">
                        <div class="info-box-content ss--text-center">
                            <span class="info-box-number ss--text-center" style="text-decoration: line-through;">{{($item['price'])}}@lang('đ')</span>
                            <span class="info-box-number ss--text-center">{{($item['promotion_price'])}}@lang('đ')</span>
                            <div class="info-box-number ss--text-center">
                                @if($item['avatar']!=null)
                                    <img src="{{asset($item['avatar'])}}" class="ss--image-pos">
                                @else
                                    <img src="{{asset('uploads/admin/icon/default-placeholder.png')}}"
                                         class="ss--image-pos">
                                @endif
                            </div>
                            <span class="info-box-text ss--text-center">{{$item['name']}}</span>
                        </div>
                    </div>
                @else
                    <div class="info-box col-lg-3 m--margin-bottom-10"
                         onclick="order.append_table('{{$item['id']}}','{{($item['price_hidden'])}}','{{$item['type']}}','{{$item['name']}}','{{$item['code']}}')">
                        <div class="info-box-content ss--text-center">
                            <span class="info-box-number ss--text-center">{{($item['price'])}}@lang('đ')</span>
                            <div class="info-box-number ss--text-center">
                                @if($item['avatar']!=null)
                                    <img src="{{asset($item['avatar'])}}" class="ss--image-pos">
                                @else
                                    <img src="{{asset('uploads/admin/icon/default-placeholder.png')}}"
                                         class="ss--image-pos">
                                @endif
                            </div>
                            <span class="info-box-text ss--text-center">{{$item['name']}}</span>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <!--end::Widget 14-->
    </div>

</div>