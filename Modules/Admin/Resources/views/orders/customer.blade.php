<div class="modal fade" role="dialog" id="modal-customer">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_index">
                    <i class="la la-user-plus"></i> {{__('CHỌN KHÁCH HÀNG')}}
                </h4>
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--<span aria-hidden="true">×</span>--}}
                {{--</button>--}}
            </div>
            <form id="form-customer">
                <div class="modal-body">
                    <div class="form-group m-form__group">
                        <div>
                            <label style="font-weight: bold;font-size:13px ">@lang('Tìm kiếm khách hàng'):</label>
                        </div>
                        <div>
                            <select class="form-control" id="customer-search" style="width: 100%">

                            </select>
                        </div>

                        <input type="hidden" name="customer_id_modal" id="customer_id_modal">
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="form-group m-form__group col-lg-6 name">
                            <label style="font-size: 11px">{{__('Tên khách hàng')}}:</label>
                            <input type="text" class="form-control btn-sm" name="full_name" id="full_name"
                                   placeholder="{{__('Nhập tên khách hàng')}}">
                            <span class="error_name" style="color: #ff0000"></span>
                        </div>
                        <div class="form-group m-form__group col-lg-6 phone">
                            <label style="font-size: 11px">{{__('Số điện thoại')}}:</label>
                            <input type="number" class="form-control btn-sm" name="phone" id="phone"
                                   placeholder="{{__('Nhập số điện thoại')}}" onkeydown="javascript: return event.keyCode == 69 ? false : true">
                            <input type="hidden" name="customer_avatar" id="customer_avatar">
                            <input type="hidden" name="member_money" id="member_money">
                            <span class="error_phone" style="color: #ff0000"></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label style="font-size: 11px">{{__('Địa chỉ')}}:</label>
                        <input type="text" class="form-control btn-sm" name="address" id="address"
                               placeholder="{{__('Nhập địa chỉ khách hàng')}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="m-form__actions m--align-right w-100">
                        <button data-dismiss="modal"
                                class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn ">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
                        </button>
                        <button type="button" onclick="order.modal_customer_click()"
                                class="btn btn-primary color_button  m-btn m-btn--icon m-btn--wide m-btn--md btn-print m--margin-left-10 son-mb">
							<span>
							<span>{{__('ĐỒNG Ý')}}</span>
							</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
