@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-order.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ ĐƠN HÀNG ")</span>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/pos-order.css')}}">
@endsection
@section('content')
    @include('admin::orders.modal-discount')
    @include('admin::orders.modal-discount-bill')
    @include('admin::orders.receipt')
    @include('admin::orders.modal-enter-phone-number')
    @include('admin::orders.modal-enter-email')
    <div class="m-portlet m-portlet--head-sm" id="m-order-add">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        {{__('THANH TOÁN ĐƠN HÀNG')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-lg-8 bdr">
                    <div class="row bdb" style="padding: 0px !important;">
                        <div class="m-section__content col-lg-12">
                            <div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true"
                                 style="height: 200px; overflow: hidden; width:100%;padding: 0px !important;">
                                <div class="table-responsive">
                                    <table class="table table-striped m-table m-table--head-bg-default" id="table_add">
                                        <thead class="bg">
                                        <tr>
                                            <td class="tr_thead_od">#</td>
                                            <td class="tr_thead_od">{{__('Tên')}}</td>
                                            <td class="tr_thead_od">{{__('Giá')}}</td>
                                            <td class="tr_thead_quan width-110-od">{{__('Số lượng')}}</td>
                                            <td class="tr_thead_od text-center">{{__('Giảm')}}</td>
                                            <td class="tr_thead_od text-center">{{__('Thành tiền')}}</td>
                                            <td class="tr_thead_od text-center">{{__('Nhân viên')}}</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody class="tr_thead_od">
                                        @foreach($order_detail as $key=>$item1)
                                            <tr class="tr_table {{in_array($item1['object_type'], ['product_gift', 'service_gift', 'service_card_gift']) ? 'promotion_gift' : ''}}">
                                                <td>

                                                </td>
                                                <td class="td_vtc">
                                                    {{$item1['object_name']}}
                                                    <input type="hidden" name="id_detail" id="id_detail"
                                                           value="{{$item1['order_detail_id']}}">
                                                    <input type="hidden" name="id" value="{{$item1['object_id']}}">
                                                    <input type="hidden" name="name" value="{{$item1['object_name']}}">
                                                    <input type="hidden" name="object_type"
                                                           value="{{$item1['object_type']}}">
                                                    <input type="hidden" name="object_code"
                                                           value="{{$item1['object_code']}}">
                                                </td>
                                                <td class="td_vtc">
                                                    {{number_format($item1['price'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}@lang('đ')
                                                    <input type="hidden" name="price" value="{{$item1['price']}}">
                                                </td>
                                                <td class="td_vtc">
                                                    @if(in_array($item1['object_type'], ['product', 'service', 'service_card']))
                                                        <input style="text-align: center;" type="text" name="quantity"
                                                               class="quantity form-control btn-ct"
                                                               value="{{$item1['quantity']}}" data-id="{{$key+1}}">
                                                        <input type="hidden" name="quantity_hidden" value="{{$item1['quantity']}}">
                                                    @elseif($item1['object_type'] == 'member_card')
                                                        <input style="text-align: center;" type="text" name="quantity"
                                                               class="quantity_card form-control btn-ct-input"
                                                               value="{{$item1['quantity']}}" disabled>
                                                        <input type="hidden" name="quantity_hidden" value="{{$item1['max_quantity_card']['number_using']-$item1['max_quantity_card']['count_using']}}">
                                                    @elseif(in_array($item1['object_type'], ['product_gift', 'service_gift', 'service_card_gift']))
                                                        <input style="text-align: center;" type="text" name="quantity"
                                                               class="form-control btn-ct"
                                                               value="{{$item1['quantity']}}" data-id="{{$key+1}}" disabled>
                                                        <input type="hidden" name="quantity_hidden" value="{{$item1['quantity']}}">
                                                    @endif
                                                </td>
                                                <td class="discount-tr-{{$item1['object_type']}}-{{$key+1}} td_vtc"
                                                    style="text-align: center">
                                                    @if($item1['object_type']!='member_card')
                                                        @if($item1['discount']>0)
                                                            @if($item1['object_type']=='service')
                                                                <a class="abc" href="javascript:void(0)"
                                                                   onclick="list.close_amount('{{$item1['object_id']}}','1','{{$key+1}}')">
                                                                    <i class="la la-close cl_amount m--margin-right-5"></i>
                                                                </a>
                                                            @elseif($item1['object_type']=='service_card')
                                                                <a class="abc" href="javascript:void(0)"
                                                                   onclick="list.close_amount('{{$item1['object_id']}}','2','{{$key+1}}')">
                                                                    <i class="la la-close cl_amount m--margin-right-5"></i>
                                                                </a>
                                                            @elseif($item1['object_type'] =='product')
                                                                <a class="abc" href="javascript:void(0)"
                                                                   onclick="list.close_amount('{{$item1['object_id']}}','3','{{$key+1}}')">
                                                                    <i class="la la-close cl_amount m--margin-right-5"></i>
                                                                </a>
                                                            @endif
                                                        @else
                                                            @if($item1['object_type']=='service')
                                                                <a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus"
                                                                   href="javascript:void(0)"
                                                                   onclick="list.modal_discount('{{$item1['amount']}}','{{$item1['object_id']}}','1','{{$key+1}}')">
                                                                    <i class="la la-plus icon-sz"></i>
                                                                </a>
                                                            @elseif($item1['object_type']=='service_card')
                                                                <a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus"
                                                                   href="javascript:void(0)"
                                                                   onclick="list.modal_discount('{{$item1['amount']}}','{{$item1['object_id']}}','2','{{$key+1}}')">
                                                                    <i class="la la-plus icon-sz"></i>
                                                                </a>
                                                            @elseif($item1['object_type'] =='product')
                                                                <a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus"
                                                                   href="javascript:void(0)"
                                                                   onclick="list.modal_discount({{$item1['amount']}},{{$item1['object_id']}},'3','{{$key+1}}')">
                                                                    <i class="la la-plus icon-sz"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    @else
                                                        0đ
                                                    @endif
                                                    @if($item1['discount']>0)
                                                        {{$item1['discount']}}@lang('đ')
                                                    @endif
                                                    <input type="hidden" name="discount" value="{{$item1['discount']}}">
                                                    <input type="hidden" name="voucher_code"
                                                           value="{{$item1['voucher_code']}}">

                                                </td>
                                                <td class="amount-tr td_vtc" style="text-align: center">
                                                    {{number_format($item1['amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}@lang('đ')
                                                    <input type="hidden" name="amount" value="{{$item1['amount']}}">
                                                </td>
                                                <td>
                                                    <select class="form-control staff" name="staff_id"
                                                            style="width:100%;"
                                                            {{in_array($item1['object_type'], ['member_card', 'product_gift', 'service_gift', 'service_card_gift']) ? 'disabled':''}}>
                                                        <option></option>
                                                        @foreach($staff_technician as $key => $value)
                                                            <option value="{{$key}}" {{$item1['staff_id'] == $key ? 'selected':''}}>{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td class="td_vtc">
                                                    @if(in_array($item1['object_type'], ['product', 'service', 'service_card']))
                                                        <a class="remove" href="javascript:void(0)"
                                                           style="color: #a1a1a1"><i class="la la-trash"></i></a>
                                                    @elseif($item1['object_type'] == 'member_card')
                                                        <a class="remove_card" href="javascript:void(0)"
                                                           style="color: #a1a1a1"><i class="la la-trash"></i></a>
                                                    @elseif(in_array($item1['object_type'], ['product_gift', 'service_gift', 'service_card_gift']))
                                                        <a href="javascript:void(0)" onclick="order.removeGift(this)"
                                                           style="color: #a1a1a1"><i class="la la-trash"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <span class="error-table" style="color: #ff0000"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group m--margin-top-10 row">
                        <div class="col-lg-12 bdr">
                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm"
                                role="tablist">
                                <li class="nav-item m-tabs__item type">
                                    <a id="load" href="javascript:void(0)" onclick="order.click('service')"
                                       class="nav-link m-tabs__link type active show " data-toggle="tab" role="tab"
                                       aria-selected="true" data-name="service">
                                       @lang("Dịch vụ")
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item type">
                                    <a href="javascript:void(0)" onclick="order.click('service_card')"
                                       class="nav-link m-tabs__link type " data-toggle="tab" role="tab"
                                       aria-selected="true" data-name="service_card">
                                       @lang("Thẻ dịch vụ")
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item type">
                                    <a href="javascript:void(0)" onclick="order.click('product')"
                                       class="nav-link m-tabs__link " data-toggle="tab" role="tab"
                                       aria-selected="false" data-name="product">
                                       @lang("Sản phẩm")
                                    </a>
                                </li>
                                @if(count($data)>0)
                                    <li class="nav-item m-tabs__item type">
                                        <a href="javascript:void(0)" onclick="order.click('member_card')"
                                           class="nav-link m-tabs__link tab_member_card" data-toggle="tab"
                                           role="tab"
                                           aria-selected="false" data-name="member_card">
                                            <i class="la la-shopping-cart"></i>@lang("Thẻ dịch vụ đã mua")
                                        </a>
                                    </li>
                                @endif
                            </ul>
                            <div class="form-group m-form__group ">
                                <div class="m-input-icon m-input-icon--left">
                                    <span class="">
                                            <input id="search" name="search" autocomplete="off" type="text"
                                                   class="form-control m-input--pill m-input" value=""
                                                   placeholder="@lang("Nhập thông tin tìm kiếm")">
                                    </span>
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i
                                                    class="la la-search"></i></span></span>
                                </div>
                            </div>
                            <div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true"
                                 style="height: 250px; overflow: hidden;" id="list-product">

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row m--margin-bottom-25">
                        <input type="hidden" name="order_code" id="order_code" value="{{$item['order_code']}}">
                        <input type="hidden" name="order_id" id="order_id" value="{{$item['order_id']}}">
                        <input type="hidden" name="customer_id" id="customer_id" value="{{$item['customer_id']}}">
                        <input type="hidden" name="money" id="money" value="{{$money['balance']}}">
                        <div class="col-lg-8">
                            <div class="form-group row customer">
                                <div class="col-lg-3">
                                    @if($item['customer_avatar']!="")
                                        <img src="{{asset($item['customer_avatar'])}}" height="52px" width="52px">
                                    @else
                                        <img src="{{asset('uploads/admin/icon/person.png')}}"
                                             style="width: 52px;height: 52px">
                                    @endif                                </div>
                                <div class="col-lg-9">
                                     <span class="m-widget4__title m-font-uppercase">
                                         {{$item['full_name']}}
                                        <span class="m-badge m-badge--success vanglai" data-toggle="m-tooltip"
                                              data-placement="top" title="" data-original-title="{{__('Thành viên')}}"></span>
                                     </span>
                                    <br>
                                    <span class="m-widget4__title m-font-uppercase">
                                        <i class="flaticon-support m--margin-right-5"></i>
                                        {{$item['phone']}}
                                    </span>
                                    <br>
                                    <span class="m-widget4__title">
                                        @lang("Hạng"): {{$item['member_level_name'] != null ? $item['member_level_name'] : __('Thành Viên')}}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">

                        </div>
                    </div>
                    <div class="row m--margin-bottom-25">
                        <div class="col-lg-12">
                            <select class="form-control" id="refer_id" name="refer_id" style="width:100%;">
                                <option></option>
                                @foreach($customer_refer as $key => $value)
                                    @if($key != 1)
                                        <option value="{{$key}}" {{$item['refer_id'] == $key ? 'selected':''}}>{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if($item['order_source_id'] == 2 && $item['delivery_active'] == 0)
                        <div class="row m--margin-bottom-25">
                            <div class="col-lg-2">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input id="delivery_active" name="delivery_active" type="checkbox"
                                                {{$item['delivery_active'] == 1 ? 'checked' : ''}}>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-10 m--margin-top-5">
                                <span>@lang("Xác nhận đơn hàng")</span>
                            </div>
                        </div>
                    @endif
                    <div class="row m--margin-left-5 m--margin-right-5"
                         style="padding-top: 0px !important;padding-bottom: 0px !important;">
                        <div class="order_tt">
                            <div class="m-list-timeline__items">
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text sz_word m--font-boldest">{{__('Tổng tiền')}}:</span>
                                    <span class="m-list-timeline__time m--font-boldest append_bill">
                                    {{number_format($item['total'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}@lang('đ')
                                    <input type="hidden" name="total_bill" id="total_bill"
                                           class="form-control total_bill"
                                           value="{{$item['total']}}">
                                </span>
                                </div>
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text sz_word m--font-boldest">@lang("Chiết khấu thành viên"):</span>
                                    <span class="m-list-timeline__time m--font-boldest">
                                        <span class="span_member_level_discount">0</span>@lang('đ')
                                    </span>
                                    <input type="hidden" name="member_level_discount" id="member_level_discount"
                                           class="form-control" value="0">

                                </div>
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text m--font-boldest sz_word">@lang("Giảm giá"):</span>
                                    <span class="m-list-timeline__time m--font-boldest discount_bill">
                                       @if($item['discount']>0)
                                            <a class="tag_a" href="javascript:void(0)"
                                               onclick="list.close_discount_bill({{$item['total']}})">
                                                <i class="la la-close cl_amount_bill"></i>
                                            </a>
                                        @else
                                            <a href="javascript:void(0)"
                                               onclick="list.modal_discount_bill({{$item['total']}})"
                                               class="tag_a">
                                            <i class="fa fa-plus-circle icon-sz m--margin-right-5"
                                               style="color: #4fc4cb;"></i>
                                            </a>
                                        @endif
                                        @if($item['discount']>0)
                                            {{number_format($item['discount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}@lang('đ')
                                        @else
                                            0 @lang('đ')
                                        @endif

                                        <input type="hidden" id="discount_bill" name="discount_bill"
                                               value="{{$item['discount']}}">
                                    <input type="hidden" id="voucher_code_bill" name="voucher_code_bill"
                                           value="{{$item['voucher_code']}}">
                                </span>
                                </div>
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text m--font-boldest sz_word">@lang("Thành tiền"):</span>
                                    <span class="m-list-timeline__time m--font-boldest amount_bill" style="color: red">
                                         {{number_format($item['amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}@lang('đ')
                                        <input type="hidden" name="amount_bill_input"
                                               class="form-control amount_bill_input"
                                               value="{{$item['amount']}}">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__group m--margin-top-10">
                        <button type="submit" id="btn_order"
                                class="btn btn-success color_button son-mb wd_type
                                m-btn m-btn--icon m-btn--wide m-btn--md">
							<span>
							<i class="la la-reorder"></i>
							<span>@lang("THANH TOÁN")</span>
							</span>
                        </button>
                    </div>
                    <div class="m-form__group m--margin-top-10">
                        <button type="button" id="btn_add" onclick="save.submit_edit('{{$item['order_id']}}')"
                                class="btn btn-success color_button son-mb m-btn m-btn--icon wd_type
                                m-btn--wide m-btn--md btn-add">
							<span>
							<i class="la la-check"></i>
							<span>{{__('LƯU THÔNG TIN')}}</span>
							</span>
                        </button>
                    </div>
                    <div class="m-form__group m--margin-top-10">
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <a href="{{route('admin.order')}}"
                                   class="btn btn-metal bold-huy  m-btn m-btn--icon m-btn--wide m-btn--md wd_type">
                                    <span>
                                    <i class="la la-arrow-left"></i>
                                    <span>{{__('HỦY')}}</span>
                                    </span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="content-print-card" style="display: none;"></div>

    <div class="modal fade" role="dialog" id="modal-print" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title title_index">
                        @lang("DANH SÁCH THẺ IN")
                    </h4>

                </div>
                <div class="modal-body body-card">
                    <div class="m-scrollable m-scroller ps ps--active-y"
                         data-scrollable="true"
                         data-height="380" data-mobile-height="300"
                         style="height: 500px; overflow: hidden;">
                        <div class="list-card load_ajax">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.order')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10 m--margin-bottom-10">
						<span>
						<i class="la la-arrow-left"></i>
						<span>@lang("THOÁT")</span>
						</span>
                        </a>

                        <button type="button" onclick="order.print_all()"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md  m--margin-left-10 m--margin-bottom-10">
							<span>
							<i class="la la-print"></i>
							<span>@lang("IN TẤT CẢ")</span>
							</span>
                        </button>
                        <button type="button" onclick="ORDERGENERAL.sendAllCodeCard()"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn_add m--margin-left-10 m--margin-bottom-10 btn-send-sms">
							<span>
							<i class="la la-mobile-phone"></i>
							<span>@lang("SMS TẤT CẢ")</span>
							</span>
                        </button>
                        {{--<button type="button"--}}
                        {{--class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn_add m--margin-left-10 btn-send-sms m--margin-bottom-10">--}}
                        {{--<span>--}}
                        {{--<i class="la la-compress"></i>--}}
                        {{--<span>CẢ HAI</span>--}}
                        {{--</span>--}}
                        {{--</button>--}}
                        <button type="button" onclick="order.send_mail()"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn_add m--margin-left-10 m--margin-bottom-10">
							<span>
                                <i class="la la-envelope-o"></i>
							<span>@lang("GỬI EMAIL")</span>
							</span>
                        </button>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" name="pt-discount" class="form-control pt-discount"
           value="{{$item['member_level_discount'] != null ? $item['member_level_discount'] : 0}}">
    <form id="form-order-ss" target="_blank" action="{{route('admin.order.print-bill2')}}" method="GET">
        <input type="hidden" name="ptintorderid" id="orderiddd" value="">
    </form>
    <input type="hidden" value="{{$orderIdsss}}" class="hiddenOrderIdss">
    <input type="hidden" id="order_source_id" value="{{$item['order_source_id']}}">
@endsection
@section('after_script')
    <script>
        $(document).ready(function () {
            $('body').addClass('m-brand--minimize m-aside-left--minimize');
        });
    </script>
    <script src="{{asset('static/backend/js/admin/order/html2canvas.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/jquery.printPage.js?v='.time())}}"
            type="text/javascript"></script>
    <script type="text/template" id="list-tpl">
        <div class="info-box col-lg-3 m--margin-bottom-10"
             onclick="order.append_table({id},'{price_hidden}','{type}','{name}','{code}')">
            <div class="info-box-content ss--text-center">
                <span class="info-box-number ss--text-center">{price}@lang('đ')</span>
                <div class="info-box-number ss--text-center">
                    <img src="{img}" class="ss--image-pos">
                    <input type="hidden" class="type_hidden" name="type_hidden" value="{type}">
                </div>
                <span class="info-box-text ss--text-center">{name}</span>
            </div>
        </div>
    </script>
    <script type="text/template" id="table-tpl">
        <tr class="table_add">
            <td></td>
            <td class="td_vtc">
                {name}
                <input type="hidden" name="id" value="{id}">
                <input type="hidden" name="name" value="{name}">
                <input type="hidden" name="object_type" value="{type_hidden}">
                <input type="hidden" name="object_code" value="{code}">
            </td>
            <td class="td_vtc">
                {price}đ
                <input type="hidden" name="price" value="{price_hidden}">
            </td>
            <td class="td_vtc">
                <input style="text-align: center;" type="text" name="quantity"
                       class="quantity_add form-control btn-ct-input" data-id="{stt}" value="1">
                <input type="hidden" name="quantity_hid" value="{quantity_hid}">
            </td>
            <td class="discount-tr-{type_hidden}-{stt} td_vtc" style="text-align: center">
                <input type="hidden" name="discount" class="form-control discount" value="0">
                <input type="hidden" name="voucher_code" value="">
                <a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus"
                   href="javascript:void(0)"
                   onclick="list.modal_discount_add('{amount_hidden}','{id}','{id_type}','{stt}')">
                    <i class="la la-plus icon-sz"></i>
                </a>
            </td>
            <td class="amount-tr td_vtc" style="text-align: center">
                {amount}đ
                <input type="hidden" style="text-align: center;" name="amount" class="form-control amount"
                       value="{amount_hidden}">
            </td>
            <td>
                <select class="form-control staff" name="staff_id" style="width:100%;">
                    <option></option>
                    @foreach($staff_technician as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </td>
            <td class="td_vtc">
                <a class='remove' href="javascript:void(0)" style="color: #a1a1a1"><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
    <script type="text/template" id="bill-tpl">
        <span class="total_bill">
                                   {total_bill_label}đ
                 <input type="hidden" id="total_bill" name="total_bill" class="form-control total_bill"
                        value="{total_bill}">

                    </span>

    </script>
    <script type="text/template" id="type-receipt-tpl">
        <div class="row">
            <label class="col-lg-6 font-13">{label}:<span
                        style="color:red;font-weight:400">{money}</span></label>
            <div class="input-group input-group-sm col-lg-6">
                <input onkeyup="order.changeAmountReceipt(this)" style="color: #008000" class="form-control m-input" placeholder="{{__('Nhập giá tiền')}}"
                       aria-describedby="basic-addon1"
                       name="{name_cash}" id="{id_cash}" value="0">
                <div class="input-group-append"><span class="input-group-text" id="basic-addon1"> @lang("VNĐ")</span>
                </div>
            </div>
        </div>
    </script>
    <script type="text/template" id="active-tpl">
        <div class="m-checkbox-list">
            <label class="m-checkbox m-checkbox--air m-checkbox--solid m-checkbox--state-success sz_dt">
                <input type="checkbox" name="check_active" id="check_active" value="0"> @lang("Kích hoạt thẻ dịch vụ")
                <span></span>
            </label>
        </div>
    </script>
    <script type="text/template" id="price-card-cus-tpl">
        <label>@lang("Tiền giảm từ thẻ dịch vụ"):</label>
        <div class="input-group m-input-group">
            <input class="form-control m-input" placeholder="{{__('Nhập giá tiền')}}" aria-describedby="basic-addon1"
                   name="service_cash" id="service_cash" disabled="disabled" value="{price_card}">
        </div>

    </script>
    <script type="text/template" id="table-card-tpl">
        <tr class="table_add">
            <td></td>
            <td class="td_vtc">{name}
                <input type="hidden" name="id" value="{id}">
                <input type="hidden" name="name" value="{name}">
                <input type="hidden" name="object_type" value="{type_hidden}">
                <input type="hidden" name="object_code" value="{code}">
            </td>
            <td class="td_vtc">
                {price}đ
                <input type="hidden" name="price" value="{price_hidden}">
            </td>
            <td class="td_vtc">
                <input style="text-align: center;" type="text" name="quantity"
                       class="quantity_c form-control btn-ct-input">
                <input type="hidden" name="quantity_hid" value="{quantity_hid}">
            </td>
            <td class="discount-tr-{type_hidden}-{id} td_vtc text-center">
                0đ
                <input type="hidden" name="discount" class="form-control discount" value="0">
                <input type="hidden" name="voucher_code" value="">
                <a class="{class} m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary"
                   href="javascript:void(0)" onclick="order.modal_discount('{amount_hidden}','{id}','{id_type}')">
                    <i class="la la-plus"></i>
                </a>
            </td>
            <td class="amount-tr td_vtc text-center">
                {amount}đ
                <input type="hidden" style="text-align: center;" name="amount" class="form-control amount"
                       value="{amount_hidden}">
            </td>
            <td>
                <select class="form-control staff" name="staff_id" style="width:100%;" disabled>
                    <option></option>
                    @foreach($staff_technician as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </td>
            <td class="td_vtc">
                <a class='remove_card_new' href="javascript:void(0)" style="color: #a1a1a1">
                    <i class='la la-trash'></i>
                </a>
            </td>
        </tr>
    </script>
    <script type="text/template" id="list-card-tpl">
        <div class="info-box col-lg-3 m--margin-bottom-10"
             onclick="order.append_table_card({id_card},'0','member_card','{card_name}','{quantity_app}','{card_code}',this)">
            <div class="info-box-content ss--text-center">
                <div class="m-widget4__item card_check_{id_card}">
                    <span class="m-widget4__sub m--font-bolder m--font-success quantity">
                        {quantity}(@lang("lần"))
                    </span>
                    <div class="m-widget4__img m-widget4__img--pic">
                        <img src="{{asset('{img}')}}" class="ss--image-pos">
                    </div>
                    <div class="m-widget4__info">
                        <span class="m-widget4__title"> {card_name} </span>
                    </div>
                    <input type="hidden" class="card_hide" value="{card_code}">
                    <input type="hidden" class="quantity_hide" name="quantity_hide" value="{quantity_app}">
                    <input type="hidden" class="quantity_card" value="{quantity_app}">
                </div>
            </div>
        </div>
    </script>
    <script type="text/template" id="button-discount-tpl">
        <div class="m-form__actions m--align-right w-100">
            <button data-dismiss="modal"
                    class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
            </button>
            <button type="button" onclick="list.discount('{id}','{id_type}','{stt}')"
                    class="btn btn-primary color_button son-mb m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<span>{{__('ĐỒNG Ý')}}</span>
							</span>
            </button>
        </div>
    </script>
    <script type="text/template" id="button-discount-add-tpl">
        <div class="m-form__actions m--align-right w=100">
            <button data-dismiss="modal"
                    class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
            </button>
            <button type="button" onclick="list.discount_add('{id}','{id_type}','{stt}')"
                    class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<span>{{__('ĐỒNG Ý')}}</span>
							</span>
            </button>
        </div>
    </script>
    <script type="text/template" id="button-discount-bill-tpl">
        <div class="m-form__actions m--align-right w-100">
            <button data-dismiss="modal"
                    class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
            </button>
            <button type="button" onclick="list.modal_discount_bill_click()"
                    class="btn btn-primary color_button son-mb m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<span>{{__('ĐỒNG Ý')}}</span>
							</span>
            </button>
        </div>
    </script>

    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/order/list.js?v='.time())}}" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/send-sms-code-service-card.js?v='.time())}}"
            type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/order/set-interval.js?v='.time())}}" type="text/javascript"></script>
    <script type="text/template" id="list-promotion-tpl">
        <div class="info-box col-lg-3 m--margin-bottom-10"
             onclick="order.append_table({id},'{price_hidden}','{type}','{name}','{code}')">
            <div class="info-box-content ss--text-center">
                <span class="info-box-number ss--text-center"
                      style="text-decoration: line-through;">{price}{{__('đ')}}</span>
                <span class="info-box-number ss--text-center">{price_hidden}{{__('đ')}}</span>
                <div class="info-box-number ss--text-center">
                    <img src="{img}" class="ss--image-pos">
                    <input type="hidden" class="type_hidden" name="type_hidden" value="{type}">
                </div>
                <span class="info-box-text ss--text-center">{name}</span>
            </div>
        </div>
    </script>
    <script type="text/template" id="table-gift-tpl">
        <tr class="table_add promotion_gift">
            <td></td>
            <td class="td_vtc">
                {name}
                <input type="hidden" name="id" value="{id}">
                <input type="hidden" name="name" value="{name}">
                <input type="hidden" name="object_type" value="{type_hidden}">
                <input type="hidden" name="object_code" value="{code}">
            </td>
            <td class="td_vtc">
                {price} {{__('đ')}}
                <input type="hidden" name="price" value="{price_hidden}">
            </td>
            <td class="td_vtc">
                <input style="text-align: center;" type="text" name="quantity" class="form-control btn-ct-input"
                       data-id="{stt}" value="{quantity}" disabled>
                <input type="hidden" name="quantity_hid" value="{quantity_hid}">
            </td>
            <td class="discount-tr-{type_hidden}-{stt} td_vtc" style="text-align: center">
                <input type="hidden" name="discount" class="form-control discount" value="0">
                <input type="hidden" name="voucher_code" value="">
            </td>
            <td class="amount-tr td_vtc" style="text-align: center">
                {amount} @lang('đ')
                <input type="hidden" style="text-align: center;" name="amount" class="form-control amount"
                       value="{amount_hidden}">
            </td>
            <td>
                <select class="form-control staff" name="staff_id" style="width:100%;" disabled>
                    <option></option>
                    @foreach($staff_technician as $value)
                        <option value="{{$value['staff_id']}}">{{$value['full_name']}}</option>
                    @endforeach
                </select>
            </td>
            <td class="td_vtc">
                <a class='remove' href="javascript:void(0)" onclick="order.removeGift(this)" style="color: #a1a1a1"><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
@stop
