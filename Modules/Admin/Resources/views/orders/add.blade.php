@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-order.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ ĐƠN HÀNG')}}</span>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/pos-order.css')}}">
@endsection
@section('content')
    @include('admin::orders.modal-discount')
    @include('admin::orders.customer')
    @include('admin::orders.modal-discount-bill')
    @include('admin::orders.receipt')
    @include('admin::orders.modal-enter-phone-number')
    @include('admin::orders.modal-enter-email')
    <!--begin::Portlet-->

    <input type="hidden" id="img_hidden" value="{{asset('uploads/admin/icon/person.png')}}">
    <div class="m-portlet m-portlet--head-sm" id="m-order-add">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        {{__('THÊM ĐƠN HÀNG')}}

                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
            <input type="hidden" name="customer_id" id="customer_id" value="1">
            <input type="hidden" name="money_customer" id="money_customer">
        </div>
        <div class="m-portlet__body">
            {{--<div class="row">--}}
            {{--<div class="info-box col-lg-3">--}}
            {{--<div class="info-box-content">--}}
            {{--<span class="info-box-number">41,410đ </span>--}}
            {{--<div class="info-box-number">--}}
            {{--<img src="http://spa-piotech.local:8080/uploads/admin/service/20190815/2156586358215082019_service.png" height="52px" width="52px">--}}
            {{--</div>--}}
            {{--<span class="info-box-text">Dưỡng trắng da ngừa thâm Hahaha ngừa thâm Hahaha</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="row">
                <div class="col-lg-8 bdr">
                    <div class="row bdb" style="padding: 0px !important;">
                        <div class="m-section__content col-lg-12">
                            <div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true"
                                 style="height: 200px; overflow: hidden; width:100%;padding: 0px !important;">
                                <div class="table-responsive">
                                    <table class="table table-striped m-table m-table--head-bg-default" id="table_add">
                                        <thead class="bg">
                                        <tr>
                                            <td class="tr_thead_od m--font-bolder m--font-transform-u">#</td>
                                            <td class="tr_thead_od m--font-bolder m--font-transform-u">{{__('Tên')}}</td>
                                            <td class="tr_thead_od m--font-bolder m--font-transform-u">{{__('Giá')}}</td>
                                            <td class="tr_thead_quan width-110-od m--font-bolder m--font-transform-u">{{__('Số lượng')}}
                                            </td>
                                            <td class="tr_thead_od text-center m--font-bolder m--font-transform-u">
                                                {{__('Giảm')}}
                                            </td>
                                            <td class="tr_thead_od text-center m--font-bolder m--font-transform-u">{{__('Thành tiền')}}
                                            </td>
                                            <td class="tr_thead_od text-center m--font-bolder m--font-transform-u">
                                                {{__('Nhân viên')}}
                                            </td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody class="tr_thead_od">

                                        </tbody>
                                    </table>
                                </div>
                                <span class="error-table" style="color: #ff0000"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group m--margin-top-10 row">
                        <div class="col-lg-12 bdr">
                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm tab-list m--margin-bottom-10"
                                role="tablist">
                                <li class="nav-item m-tabs__item type">
                                    <a class="nav-link m-tabs__link active show" data-toggle="tab"
                                       href="javascript:void(0)" onclick="order.click('service')" role="tab"
                                       data-name="service">
                                        {{__('Dịch vụ')}}
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item type">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="javascript:void(0)"
                                       onclick="order.click('service_card')" role="tab" data-name="service_card">{{__('Thẻ dịch vụ')}}
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item type">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="javascript:void(0)"
                                       onclick="order.click('product')" role="tab" data-name="product">
                                        {{__('Sản phẩm')}}
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item type">

                                </li>
                            </ul>
                            <div class="form-group m-form__group ">
                                <div class="m-input-icon m-input-icon--left">
                                    <span class="">
                                            <input id="search" name="search" autocomplete="off" type="text"
                                                   class="form-control m-input--pill m-input" value=""
                                                   placeholder="{{__('Nhập thông tin tìm kiếm')}}">
                                    </span>
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i
                                                    class="la la-search"></i></span></span>
                                </div>
                            </div>
                            <div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true"
                                 style="height: 250px; overflow: hidden;" id="list-product">

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row customer m--margin-bottom-25">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <img src="{{asset('uploads/admin/icon/person.png')}}" style="width: 52px;height: 52px">
                                <span class="m-widget4__title m-font-uppercase">{{__('Khách vãng lai')}}
                                <span class="m-badge m-badge--success vanglai"
                                      data-toggle="m-tooltip" data-placement="top" title=""
                                      data-original-title="{{__('Khách mới')}}"></span>
                                </span>

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <a href="javascript:void(0)" onclick="order.modal_customer()"
                               class="m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary choose_cus son-mb">
                                <i class="la la-user-plus icon-sz"></i> {{__('Chọn khách hàng')}}</a>
                        </div>
                    </div>
                    <div class="row m--margin-bottom-25">
                        <div class="col-lg-12">
                            <select class="form-control" id="refer_id" name="refer_id" style="width:100%;">
                                <option></option>
                                @foreach($customer_refer as $key => $value)
                                    @if($key != 1)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row m--margin-left-5 m--margin-right-5"
                         style="padding-top: 0px !important;padding-bottom: 0px !important;">
                        <div class="order_tt">
                            <div class="m-list-timeline__items">
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text sz_word m--font-boldest">{{__('Tổng tiền')}}:</span>
                                    <span class="m-list-timeline__time m--font-boldest append_bill">
                                    {{__('0đ')}}
                                    <input type="hidden" id="total_bill" name="total_bill" class="form-control total_bill" value="0">
                                </span>
                                </div>
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text sz_word m--font-boldest">{{__('Chiết khấu thành viên')}}:</span>
                                    <span class="m-list-timeline__time m--font-boldest">
                                        <span class="span_member_level_discount">0</span> {{__('đ')}}
                                    </span>
                                    <input type="hidden" name="member_level_discount" id="member_level_discount" class="form-control" value="0">

                                </div>
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text m--font-boldest sz_word">{{__('Giảm giá')}}:</span>
                                    <span class="m-list-timeline__time m--font-boldest discount_bill">
                                        <a href="javascript:void(0)" onclick="order.modal_discount_bill(0)"
                                           class="tag_a">
                                        <i class="fa fa-plus-circle icon-sz" style="color: #4fc4cb "></i>
                                        </a>
                                        0 @lang('đ')
                                        <input type="hidden" id="discount_bill" name="discount_bill" value="0">
                                        <input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="">
                                </span>
                                </div>
                                <div class="m-list-timeline__item sz_bill">
                                    <span class="m-list-timeline__text m--font-boldest sz_word">{{__('Thành tiền')}}:</span>
                                    <span class="m-list-timeline__time m--font-boldest amount_bill" style="color: red">
                                        0 {{__('đ')}}
                                        <input type="hidden" name="amount_bill_input"
                                               class="form-control amount_bill_input"
                                               value="0">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__group m--margin-top-10">
                        <button type="submit$" onclick="order.receipt()" id="btn_order"
                                class="btn btn-success color_button son-mb wd_type
                                m-btn m-btn--icon m-btn--wide m-btn--md">
							<span>
							<i class="la la-reorder"></i>
							<span>{{__('THANH TOÁN')}}</span>
							</span>
                        </button>
                    </div>
                    <div class="m-form__group m--margin-top-10">
                        <button type="submit" id="btn_add"
                                class="btn btn-success color_button son-mb m-btn m-btn--icon wd_type
                                m-btn--wide m-btn--md btn-add">
							<span>
							<i class="la la-check"></i>
							<span>{{__('LƯU THÔNG TIN')}}</span>
							</span>
                        </button>
                    </div>
                    <div class="m-form__group m--margin-top-10">
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <a href="{{route('admin.order')}}"
                                   class="btn btn-metal bold-huy  m-btn m-btn--icon m-btn--wide m-btn--md wd_type">
                                    <span>
                                    <i class="la la-arrow-left"></i>
                                    <span>{{__('HỦY')}}</span>
                                    </span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="content-print-bill" style="display: none"></div>
    <div class="content-print-card" style="display: none;"></div>

    <div class="modal fade" role="dialog" id="modal-print" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title title_index">
                        {{__('DANH SÁCH THẺ IN')}}
                    </h4>

                </div>
                <div class="modal-body body-card">
                    <div class="m-scrollable m-scroller ps ps--active-y"
                         data-scrollable="true"
                         data-height="380" data-mobile-height="300"
                         style="height: 500px; overflow: hidden;">
                        <div class="list-card load_ajax">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.order')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10 m--margin-bottom-10">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('THOÁT')}}</span>
						</span>
                        </a>

                        <button type="button" onclick="order.print_all()"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md  m--margin-left-10 m--margin-bottom-10">
							<span>
							<i class="la la-print"></i>
							<span>{{__('IN TẤT CẢ')}}</span>
							</span>
                        </button>
                        <button type="button" onclick="ORDERGENERAL.sendAllCodeCard()"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn_add m--margin-left-10 m--margin-bottom-10 btn-send-sms">
							<span>
							<i class="la la-mobile-phone"></i>
							<span>{{__('SMS TẤT CẢ')}}</span>
							</span>
                        </button>
                        {{--<button type="button"--}}
                        {{--class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn_add m--margin-left-10 btn-send-sms m--margin-bottom-10">--}}
                        {{--<span>--}}
                        {{--<i class="la la-compress"></i>--}}
                        {{--<span>CẢ HAI</span>--}}
                        {{--</span>--}}
                        {{--</button>--}}
                        <button type="button" onclick="order.send_mail()"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn_add m--margin-left-10 m--margin-bottom-10">
							<span>
                                <i class="la la-envelope-o"></i>
							<span>{{__('GỬI EMAIL')}}</span>
							</span>
                        </button>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <input type="hidden" name="pt-discount" class="form-control pt-discount" value="0">
    <form id="form-order-ss" target="_blank" action="{{route('admin.order.print-bill2')}}" method="GET">
        <input type="hidden" name="ptintorderid" id="orderiddd" value="">
    </form>
    <input type="hidden" value="" class="hiddenOrderIdss">
@stop

@section('after_script')
    <script type="text/template" id="tab-card-tpl">
        <li class="nav-item m-tabs__item type">
            <a href="javascript:void(0)" onclick="order.click('member_card')"
               class="nav-link m-tabs__link tab_member_card" data-toggle="tab" role="tab"
               aria-selected="false" data-name="member_card">
                <i class="la la-shopping-cart"></i>{{__('Thẻ dịch vụ đã mua')}}
            </a>
        </li>
    </script>
    <script src="{{asset('static/backend/js/admin/order/html2canvas.min.js')}}" type="text/javascript"></script>
    <script type="text/template" id="button-tpl">
        <a href="javascript:void(0)" onclick="order.customer_haunt('1',this)"
           class="m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary  cus_haunt  choose_1 son-mb"><i
                    class="la la-user icon-sz"></i>{{__('Khách hàng vãng lai')}}</a>
    </script>
    <script type="text/template" id="customer-haunt-tpl">
        <div class="col-lg-8">
            <div class="form-group">
                <img src="{{asset('uploads/admin/icon/person.png')}}" style="width: 52px;height: 52px">
                <span class="m-widget4__title m-font-uppercase">{{__('Khách vãng lai')}}
                                <span class="m-badge m-badge--success vanglai"
                                      data-toggle="m-tooltip" data-placement="top" title=""
                                      data-original-title="{{__('Khách mới')}}"></span>
                                </span>

            </div>
        </div>
        <div class="col-lg-4">
            <a href="javascript:void(0)" onclick="order.modal_customer()"
               class="m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary choose_cus son-mb">
                <i class="la la-user-plus icon-sz"></i> {{__('Chọn khách hàng')}}</a>
        </div>
    </script>
    <script type="text/template" id="list-tpl">
        <div class="info-box col-lg-3 m--margin-bottom-10"
             onclick="order.append_table({id},'{price_hidden}','{type}','{name}','{code}')">
            <div class="info-box-content ss--text-center">
                <span class="info-box-number ss--text-center">{price}@lang('đ')</span>
                <div class="info-box-number ss--text-center">
                    <img src="{img}" class="ss--image-pos">
                    <input type="hidden" class="type_hidden" name="type_hidden" value="{type}">
                </div>
                <span class="info-box-text ss--text-center">{name}</span>
            </div>
        </div>
    </script>
    <script type="text/template" id="table-tpl">
        <tr class="tr_table">
            <td></td>
            <td class="td_vtc">
                {name}
                <input type="hidden" name="id" value="{id}">
                <input type="hidden" name="name" value="{name}">
                <input type="hidden" name="object_type" value="{type_hidden}">
                <input type="hidden" name="object_code" value="{code}">
            </td>
            <td class="td_vtc">
                {price}{{__('đ')}}
                <input type="hidden" name="price" value="{price_hidden}">
            </td>
            <td class="td_vtc">
                <input style="text-align: center;" type="text" name="quantity"
                       class="quantity form-control btn-ct-input" data-id="{stt}" value="1">
                <input type="hidden" name="quantity_hid" value="{quantity_hid}">
            </td>
            <td class="discount-tr-{type_hidden}-{stt} td_vtc" style="text-align: center">
                <input type="hidden" name="discount" class="form-control discount" value="0">
                <input type="hidden" name="voucher_code" value="">
                <a class="abc m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary btn-sm-cus"
                   href="javascript:void(0)"
                   onclick="order.modal_discount('{amount_hidden}','{id}','{id_type}','{stt}')">
                    <i class="la la-plus icon-sz"></i>
                </a>
            </td>
            <td class="amount-tr td_vtc" style="text-align: center">
                {amount}{{__('đ')}}
                <input type="hidden" style="text-align: center;" name="amount" class="form-control amount"
                       value="{amount_hidden}">
            </td>
            <td>
                <select class="form-control staff" name="staff_id" style="width:100%;">
                    <option></option>
                    @foreach($staff_technician as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </td>
            <td class="td_vtc">
                <a class='remove' href="javascript:void(0)" style="color: #a1a1a1"><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
    <script type="text/template" id="bill-tpl">
        <span class="total_bill">{total_bill_label}
            <input type="hidden" name="total_bill" id="total_bill"
                   class="form-control total_bill" value="{total_bill}">
        </span>
    </script>
    <script type="text/template" id="customer-tpl">
        <div class="col-lg-8">
            <div class="form-group row">
                <div class="col-lg-3">
                    <img src="{{'{img}'}}" style="width: 52px;height: 52px">
                </div>
                <div class="col-lg-9">
                     <span class="m-widget4__title m-font-uppercase">{full_name}
                    <span class="m-badge m-badge--success vanglai"
                          data-toggle="m-tooltip" data-placement="top" title=""
                          data-original-title="{{__('Thành viên')}}"></span>
                     </span>
                    <br>
                    <span class="m-widget4__title m-font-uppercase">
                    <i class="flaticon-support m--margin-right-5"></i> {phone}</span>
                    <br>
                    <span class="m-widget4__title">
                    {{__('Hạng')}}: {member_level_name} {icon}
                    </span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <a href="javascript:void(0)" onclick="order.modal_customer()"
               class="m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary choose_cus son-mb">
                <i class="la la-user-plus icon-sz"></i> {{__('Chọn khách hàng')}}</a>
            <a href="javascript:void(0)" onclick="order.customer_haunt('1',this)"
               class="m-btn m-btn--pill m-btn--hover-brand-od btn btn-sm btn-secondary choose_1 son-mb m--margin-top-5"><i
                        class="la la-user icon-sz"></i>{{__('Khách hàng vãng lai')}}</a>
        </div>
    </script>
    <script type="text/template" id="type-receipt-tpl">
        <div class="row">
            <label class="col-lg-6 font-13">{label}:<span
                        style="color:red;font-weight:400">{money}</span></label>
            <div class="input-group input-group-sm col-lg-6" style="height: 30px;">
                <input onkeyup="order.changeAmountReceipt(this)" style="color: #008000" class="form-control m-input" placeholder="{{__('Nhập giá tiền')}}"
                       aria-describedby="basic-addon1"
                       name="{name_cash}" id="{id_cash}" value="0">
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon1">{{__('VNĐ')}}
                    </span>
                </div>
            </div>
        </div>
    </script>
    <script type="text/template" id="active-card-tpl">
        <a href="javascript:void(0)" onclick="order.modal_card({id})"
           class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary active-a"><i class="la la-plus"></i>{{__('Thẻ dịch vụ đã mua')}}</a>
    </script>
    <script type="text/template" id="list-card-tpl">
        <div class="info-box col-lg-3 m--margin-bottom-10"
             onclick="order.append_table_card({id_card},'0','member_card','{card_name}','{quantity_app}','{card_code}',this)">
            <div class="info-box-content ss--text-center">
                <div class="m-widget4__item card_check_{id_card}">
                    <span class="m-widget4__sub m--font-bolder m--font-success quantity">
                        {quantity}({{__('lần')}})
                    </span>
                    <div class="m-widget4__img m-widget4__img--pic">
                        <img src="{{asset('{img}')}}" class="ss--image-pos">
                    </div>
                    <div class="m-widget4__info">
                        <span class="m-widget4__title"> {card_name} </span>
                    </div>
                    <input type="hidden" class="card_hide" value="{card_code}">
                    <input type="hidden" class="quantity_card" value="{quantity_app}">
                </div>
            </div>
        </div>
    </script>
    <script type="text/template" id="table-card-tpl">
        <tr class="tr_table">
            <td></td>
            <td class="td_vtc">
                {name}
                <input type="hidden" name="id" value="{id}">
                <input type="hidden" name="name" value="{name}">
                <input type="hidden" name="object_type" value="{type_hidden}">
                <input type="hidden" name="object_code" value="{code}">
            </td>
            <td class="td_vtc">{price}{{__('đ')}} <input type="hidden" name="price" value="{price_hidden}"></td>
            <td class="td_vtc">
                <input style="text-align: center;" type="text" name="quantity"
                       class="quantity_c form-control btn-ct-input">
                <input type="hidden" name="quantity_hid" value="{quantity_hid}">
            </td>
            <td class="discount-tr-{type_hidden}-{stt} td_vtc" style="text-align: center">
                0{{__('đ')}}
                <input type="hidden" name="discount" class="form-control discount" value="0">
                <input type="hidden" name="voucher_code" value="">
                <a class="{class} m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary"
                   href="javascript:void(0)" onclick="order.modal_discount('{amount_hidden}','{id}','{id_type}')">
                    <i class="la la-plus"></i>
                </a>
            </td>
            <td class="amount-tr td_vtc" style="text-align: center">
                {amount}{{__('đ')}}
                <input type="hidden" style="text-align: center;" name="amount" class="form-control amount"
                       value="{amount_hidden}">
            </td>
            <td>
                <select class="form-control staff" name="staff_id" style="width:100%;" disabled>
                    <option></option>
                    @foreach($staff_technician as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </td>
            <td class="td_vtc">
                <a class='remove_card' href="javascript:void(0)" style="color: #a1a1a1"><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
    <script type="text/template" id="active-tpl">
        <div class="m-checkbox-list">
            <label class="m-checkbox m-checkbox--air m-checkbox--solid m-checkbox--state-success sz_dt">
                <input type="checkbox" name="check_active" id="check_active" value="0"> {{__('Kích hoạt thẻ dịch vụ')}}
                <span></span>
            </label>
        </div>
    </script>
    <script type="text/template" id="close-discount-bill">
        <a href="javascript:void(0)" onclick="order.close_discount_bill('{close_discount_hidden}')"
           class="tag_a">
            <i class="la la-close cl_amount_bill"></i>
        </a>
        {discount}{{__('đ')}}
        <input type="hidden" id="discount_bill" name="discount_bill" value="{discount_hidden}">
        <input type="hidden" id="voucher_code_bill" name="voucher_code_bill" value="{code_bill}">
    </script>
    <script type="text/template" id="button-discount-tpl">
        <div class="m-form__actions m--align-right w-100">
            <button data-dismiss="modal"
                    class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
            </button>
            <button type="button" onclick="order.discount('{id}','{id_type}','{stt}')"
                    class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<span>{{__('ĐỒNG Ý')}}</span>
							</span>
            </button>
        </div>
    </script>
    <script type="text/template" id="button-discount-bill-tpl">
        <div class="m-form__actions m--align-right w-100">
            <button data-dismiss="modal"
                    class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
            </button>
            <button type="button" onclick="order.modal_discount_bill_click()"
                    class="btn btn-primary  color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<span>{{__('ĐỒNG Ý')}}</span>
							</span>
            </button>
        </div>
    </script>
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/general/jquery.printPage.js?v='.time())}}"
            type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/order/script.js?v='.time())}}" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/general/send-sms-code-service-card.js?v='.time())}}"
            type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('body').addClass('m-brand--minimize m-aside-left--minimize');
        });
    </script>
    <script type="text/template" id="table-gift-tpl">
        <tr class="tr_table promotion_gift">
            <td></td>
            <td class="td_vtc">
                {name}
                <input type="hidden" name="id" value="{id}">
                <input type="hidden" name="name" value="{name}">
                <input type="hidden" name="object_type" value="{type_hidden}">
                <input type="hidden" name="object_code" value="{code}">
            </td>
            <td class="td_vtc">
                {price} {{__('đ')}}
                <input type="hidden" name="price" value="{price_hidden}">
            </td>
            <td class="td_vtc">
                <input style="text-align: center;" type="text" name="quantity" class="form-control btn-ct-input" data-id="{stt}" value="{quantity}" disabled>
                <input type="hidden" name="quantity_hid" value="{quantity_hid}">
            </td>
            <td class="discount-tr-{type_hidden}-{stt} td_vtc" style="text-align: center">
                <input type="hidden" name="discount" class="form-control discount" value="0">
                <input type="hidden" name="voucher_code" value="">
            </td>
            <td class="amount-tr td_vtc" style="text-align: center">
                {amount} @lang('đ')
                <input type="hidden" style="text-align: center;" name="amount" class="form-control amount"
                       value="{amount_hidden}">
            </td>
            <td>
                <select class="form-control staff" name="staff_id" style="width:100%;" disabled>
                    <option></option>
                    @foreach($staff_technician as $value)
                        <option value="{{$value['staff_id']}}">{{$value['full_name']}}</option>
                    @endforeach
                </select>
            </td>
            <td class="td_vtc">
                <a class='remove' href="javascript:void(0)" onclick="order.removeGift(this)" style="color: #a1a1a1"><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
    <script type="text/template" id="list-promotion-tpl">
        <div class="info-box col-lg-3 m--margin-bottom-10"
             onclick="order.append_table({id},'{price_hidden}','{type}','{name}','{code}')">
            <div class="info-box-content ss--text-center">
                <span class="info-box-number ss--text-center" style="text-decoration: line-through;">{price}{{__('đ')}}</span>
                <span class="info-box-number ss--text-center">{price_hidden}{{__('đ')}}</span>
                <div class="info-box-number ss--text-center">
                    <img src="{img}" class="ss--image-pos">
                    <input type="hidden" class="type_hidden" name="type_hidden" value="{type}">
                </div>
                <span class="info-box-text ss--text-center">{name}</span>
            </div>
        </div>
    </script>
@stop
