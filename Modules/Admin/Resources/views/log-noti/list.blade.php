<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Nhà đầu tư')}}</th>
            <th class="tr_thead_list">{{__('Avatar')}}</th>
            <th class="tr_thead_list">{{__('Tiêu đề')}}</th>
            <th class="tr_thead_list">{{__('Nội dung')}}</th>
            <th class="tr_thead_list">{{__('Trạng thái')}}</th>
            <th class="tr_thead_list">{{__('Ngày tạo')}}</th>
            <th></th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td>{{$item['full_name']}}</td>
                    <td>
                        @if($item['notification_avatar'] != null)
                            <img width="50px" src="{{$item['notification_avatar']}}">
                        @else
                            <img width="50px" src="{{asset('/static/backend/images/no-image-product.png')}}">
                        @endif
                    </td>
                    <td>{{$item['notification_title_vi']}}</td>
                    <td>{{subString($item['notification_message_vi'])}}</td>
                    <td>{{$item['is_read'] == 1 ? 'Đã đọc' : 'Chưa đọc'}}</td>
                    <td>{{$item['created_at'] == null ? 'N/A' : \Carbon\Carbon::parse($item['created_at'])->format('d-m-Y')}}</td>
                    <th>
                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" href="{{route("admin.log-noti.show",$item['notification_id'])}}" title="Chi tiết">
                            <i class="flaticon-eye"></i>
                        </a>
                    </th>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }
    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }
    .a-custom .flaticon-eye{
        top: 1px;
        left: 2px;
    }
</style>
