@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ LOG THÔNG BÁO TỰ ĐỘNG')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/

        .form-control-feedback {
            color: red;
        }

    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH LOG THÔNG BÁO TỰ ĐỘNG')}}
                    </h2>

                </div>
            </div>

            <div class="m-portlet__head-tools">
            </div>

        </div>
        <div class="m-portlet__body">
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       placeholder="{{__('Nhập tên nhà đầu tư, tiêu đề hoặc nội dung')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <select class="form-control m_selectpicker" name="is_read">
                                    <option value="">Chọn trạng thái</option>
                                    <option value="1">Đã đọc</option>
                                    <option value="0">Chưa đọc</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text"
                                   class="form-control m-input daterange-picker" id="created_at"
                                   name="created_at"
                                   autocomplete="off" placeholder="{{__('Chọn ngày gửi')}}">
                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span></span>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m-form m-form--label-align-right">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group m-form__group">
                                        <button class="btn btn-primary color_button btn-search">
                                            {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                        </button>
                                        <a href="{{route('admin.log-noti')}}"
                                           class="btn btn-metal  btn-search padding9x padding9px">
                                            <span><i class="flaticon-refresh"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                    </div>
                @endif
            </form>
            <div class="table-content m--padding-top-30">
                @include('admin::log-noti.list')

            </div><!-- end table-content -->

        </div>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')


    <script src="{{asset('static/backend/js/admin/log-noti/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
@stop