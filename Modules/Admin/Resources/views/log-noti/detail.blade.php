@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ LOG THÔNG BÁO TỰ ĐỘNG')}}
    </span>
@endsection
@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-eye"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT LOG THÔNG BÁO TỰ ĐỘNG')}}
                </div>
                    </h3>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Nhà đầu tư')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                                   value="{{$detail['full_name']}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Trạng thái')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                                   value="{{$detail['is_read'] == 1? 'Đã đọc' : 'Chưa đọc'}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Tiêu đề')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                                   value="{{$detail['notification_title_vi']}}">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Nội dung')}}:
                        </label>
                        <div class="input-group">
                            <textarea type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                            >{{$detail['notification_message_vi']}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Avatar')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-widget19__pic">
                                @if($detail['notification_avatar']!=null)
                                    <img
                                         src="{{$detail['notification_avatar']}}" width="300px"
                                         alt="{{__('Hình ảnh')}}"/>
                                @else
                                    <img
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="300px"/>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Ngày tạo')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                                   value="{{\Carbon\Carbon::parse($detail['created_at'])}}">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer save-attribute m--margin-right-20">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.log-noti')}}"
                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/customer-contract/detail.js?v='.time())}}"></script>
@stop
