@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ HÀNH TRÌNH KHÁCH HÀNG')}}</span>
@stop
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('content')
    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/

        .form-control-feedback {
            color: red;
        }

    </style>
    <!--begin::Portlet-->
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                                <i class="la la-th-list"></i>
                             </span>
                    <h3 class="m-portlet__head-text">
                        {{__('DANH SÁCH HÀNH TRÌNH KHÁCH HÀNG')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
{{--                @if(in_array('admin.customer-journey.add',session('routeList')))--}}
                    <button href="javascript:void(0)"
                            data-toggle="modal"
                            data-target="#modalAdd"
                            onclick="customerJourney.clearAdd()"
                            class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                                <span>
                                    <i class="fa fa-plus-circle"></i>
                                    <span> {{__('THÊM HÀNH TRÌNH KHÁCH HÀNG')}}</span>
                                </span>
                    </button>
                    <a href="javascript:void(0)"
                       data-toggle="modal"
                       data-target="#modalAdd"
                       onclick="customerJourney.clearAdd()"
                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill
                                 color_button btn_add_mobile"
                       style="display: none">
                        <i class="fa fa-plus-circle" style="color: #fff"></i>
                    </a>
{{--                @endif--}}
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <form class="frmFilter ss--background">
                <div class="row ss--bao-filter">
                    <div class="col-lg-3 ">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="hidden" name="search_type" value="name">
                                <button class="btn btn-primary btn-search" style="display: none">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input type="text" class="form-control" name="search_keyword"
                                       placeholder="{{__('Nhập tên hành trình khách hàng')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <select name="is_active" class="form-control" id="is_active">
                            <option value="">{{__('Chọn trạng thái')}}</option>
                            <option value="1">{{__('Hoạt động')}}</option>
                            <option value="0">{{__('Tạm ngưng')}}</option>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group m-form__group">
                            <button onclick="customerJourney.search()"
                                    class="btn ss--btn-search">
                                {{__('TÌM KIẾM')}}
                                <i class="fa fa-search ss--icon-search"></i>
                            </button>
                            <a href="{{route('admin.customer-journey')}}"
                               class="btn btn-metal  btn-search padding9x padding9px">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-content m--margin-top-30">
                @include('admin::customer-journey.list')
            </div><!-- end table-content -->
        </div>

    </div>
    <!--end::Portlet-->
    <div class="modal fade show" id="modalAdd" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <!-- Modal content-->
            @include('admin::customer-journey.add')
        </div>
    </div>
    <div class="modal fade show" id="modalEdit" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <!-- Modal content-->
            @include('admin::customer-journey.edit')
        </div>
    </div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/customer-journey/list.js?v='.time())}}"
            type="text/javascript"></script>
@stop

