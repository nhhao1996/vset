<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Mã đơn hàng')}}</th>
            <th class="ss--font-size-th">{{__('Nhà đầu tư')}}</th>
            <th class="ss--font-size-th">{{__('Tên dịch vụ')}}</th>
            <th class="ss--font-size-th">{{__('Số Serial')}}</th>
            <th class="ss--font-size-th">{{__('Loại')}}</th>
            <th class="ss--font-size-th">{{__('Thời gian bắt đầu')}}</th>
            <th class="ss--font-size-th">{{__('Thời gian kết thúc')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Kích hoạt')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
            <tr class="ss--font-size-13 ss--nowrap">
                @if(isset($page))
                    <td>{{ (($page-1)*10 + $key + 1) }}</td>
                @else
                    <td>{{ ($key + 1) }}</td>
                @endif
                    <td>{{$item['order_service_code']}}</td>
                    <td>{{$item['full_name']}}</td>
                    <td>{{$item['service_name']}}</td>
                    <td>{{$item['serial']}}</td>
                    <td>{{$item['type'] == 'buy' ? 'Mua' :'Thưởng'}}</td>
                    <td>{{\Carbon\Carbon::parse($item['valid_from_date'])->format('H:i:s d/m/Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($item['valid_to_date'])->format('H:i:s d/m/Y')}}</td>
                    <td>
                        @if($item['service_serial_status'] == 'new')
                            {{__('Mới')}}
                        @elseif($item['service_serial_status'] == 'used')
                            {{__('Được sử dụng')}}
                        @elseif($item['service_serial_status'] == 'cancel')
                            {{__('Hủy')}}
                        @endif
                    </td>

                    <td>
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label class="ss--switch">
                                <input type="checkbox" disabled class="manager-btn" {{$item['is_actived'] == 1? 'checked' : ''}}>
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td>
                        @if(in_array('admin.service-serial.show', session('routeList')))
                            <a href="{{route('admin.service-serial.show',['id'=>$item['service_serial_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="flaticon-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.service-serial.edit', session('routeList')))
                            <a href="{{route('admin.service-serial.edit',['id'=>$item['service_serial_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chỉnh sửa')}}">
                                <i class="flaticon-edit"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}