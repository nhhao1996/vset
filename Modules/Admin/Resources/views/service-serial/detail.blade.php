@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ SERIAL DỊCH VỤ    ')}}
    </span>
@endsection
@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-eye"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT SERIAL DỊCH VỤ')}}
                </div>
                    </h3>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Mã đơn hàng')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                                   value="{{$detail['order_service_code']}}">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Loại')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   disabled
                                   value="{{$detail['type'] == 'buy' ? 'Mua' : 'Thưởng'}}">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Tên dịch vụ')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                    disabled
                                   value="{{$detail['service_name'] != null ? $detail['service_name'] : 'N/A'}}">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Số serial')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Nhập số serial')}}" disabled
                                   value="{{$detail['serial']}}">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Thời gian bắt đầu')}}:
                        </label>
                        <div class="input-group">
                            <input type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Nhập số serial')}}" disabled
                                   value="{{\Carbon\Carbon::parse($detail['valid_from_date'])->format('H:i:s d/m/Y')}}">
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Thời gian kết thúc')}}:
                        </label>
                        <div class="input-group">
                            <input  type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Nhập số serial')}}" disabled
                                   value="{{\Carbon\Carbon::parse($detail['valid_to_date'])->format('H:i:s d/m/Y')}}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Trạng thái')}}:
                        </label>
                        <div class="input-group">
                            {{--                            <input type="text"--}}
                            {{--                                   class="form-control m-input class font-weight-bold" disabled--}}
                            {{--                            value={{$detail['service_serial_status'] == 'new' ? __('Mới') : ($detail['service_serial_status'] == 'used' ? __('Được sử dụng') : ($detail['service_serial_status'] == 'cancel' ? __('Hủy') : ''))}}>--}}
                            <select class="form-control select2" name="service_serial_status" disabled>
                                <option value="new" {{$detail['service_serial_status'] == 'new' ? 'selected' :''}}>{{__('Mới')}}</option>
                                <option value="used" {{$detail['service_serial_status'] == 'used' ? 'selected' :''}}>{{__('Được sử dụng')}}</option>
                                <option value="cancel" {{$detail['service_serial_status'] == 'cancel' ? 'selected' :''}}>{{__('Hủy')}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Kích hoạt')}}:
                        </label>
                        <div class="input-group">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label class="ss--switch">
                                    <input type="checkbox" disabled class="manager-btn" {{$detail['is_actived'] == 1? 'checked' : ''}}>
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer save-attribute m--margin-right-20">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.service-serial')}}"
                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/customer-contract/detail.js?v='.time())}}"></script>
@stop
