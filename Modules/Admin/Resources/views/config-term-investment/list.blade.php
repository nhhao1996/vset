<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list text-center">#</th>
            <th class="tr_thead_list text-center">{{__('Loại gói')}}</th>
            <th class="tr_thead_list text-center">{{__('Kì hạn')}}</th>
            <th class="tr_thead_list text-center">{{__('Trạng thái')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                @php($num = rand(0,7))

                <tr>
                    @if(isset($page))
                        <td class="text_middle text-center">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle text-center">{{$key+1}}</td>
                    @endif
                    <td class="text_middle text-center">{{$item['category_name_vi']}}</td>
{{--                    <td class="text_middle text-center">{{$item['investment_time_month'] >= 12 ? $item['investment_time_month']/12 .' năm' : $item['investment_time_month'].' tháng' }}</td>--}}
                    <td class="text_middle text-center">{{$item['investment_time_month'].' tháng' }}</td>
                    <td class="text-center">
{{--                        @if(in_array('admin.customer.edit', session('routeList')))--}}
{{--                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"--}}
{{--                           onclick="configTermInvestment.showPopup({{$item['investment_time_id']}})"--}}
{{--                           href="javascript:void(0)" title="Chỉnh sửa">--}}
{{--                            <i class="la la-edit"></i>--}}
{{--                        </a>--}}
{{--                        @endif--}}
{{--                        @if(in_array('admin.customer.detail', session('routeList')))--}}
{{--                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"--}}
{{--                           href="javascript:void(0)" onclick="configTermInvestment.deleteTermInvestment(this,`{{$item['investment_time_id']}}`)" title="Xóa">--}}
{{--                            <i class="la la-trash"></i>--}}
{{--                        </a>--}}
{{--                        @endif--}}
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox" {{$item['is_deleted'] == 0 ? 'checked' : ''}} id="{{$item['investment_time_id']}}" class="manager-btn" onclick="configTermInvestment.changeStatus({{$item['product_category_id']}},{{$item['investment_time_id']}},{{$item['is_deleted'] == 1 ? 0 : 1}},{{$item['investment_time_month']}})">
                                <span></span>
                            </label>
                        </span>
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }

    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }

    .a-custom .flaticon-eye {
        top: 1px;
        left: 2px;
    }
</style>
