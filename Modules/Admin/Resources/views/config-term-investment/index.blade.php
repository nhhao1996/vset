@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ KÌ HẠN ĐẦU TƯ')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/
        .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > span , .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > input:checked ~ span {
            background: #4fc4ca;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <form class="frmFilter">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                             <i class="la la-th-list"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{__('Quản lý kì hạn đầu tư')}}
                        </h2>

                    </div>
                </div>

                <div class="m-portlet__head-tools nt-class">
{{--                    @if(in_array('admin.config-term-investment.create',session('routeList')))--}}
                        <a href="javascript:void(0)" onclick="configTermInvestment.showPopup()"
                           class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                        <span>
						    <i class="fa fa-plus-circle m--margin-right-5"></i>
							<span> {{__('THÊM KÌ HẠN')}}</span>
                        </span>
                        </a>
{{--                    @endif--}}
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="frmFilter bg">
                    <div class="row padding_row">
                        <div class="col-lg-3 form-group">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <select class="form-control select-fix" name="product_category_id">
                                        <option value="">{{__('Chọn loại gói')}}</option>
                                        @foreach($productCategories as $item)
                                            <option value="{{$item['product_category_id']}}" {{isset($FILTER['product_category_id']) && $FILTER['product_category_id'] == $item['product_category_id'] ? 'selected' :''}}>{{$item['category_name_vi']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <select class="form-control select-fix" name="is_deleted">
                                        <option value="">{{__('Chọn trạng thái')}}</option>
                                        <option value="0">Đang hoạt động</option>
                                        <option value="1">Ngưng hoạt động</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button class="btn btn-primary color_button btn-search " style="width: 150px">
                                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                </button>
                                <a href="{{route('admin.config-term-investment')}}"
                                   class="btn btn-metal  btn-search padding9x padding9px">
                                    <span><i class="flaticon-refresh"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                            <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                        </div>
                    @endif
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::config-term-investment.list')

                </div><!-- end table-content -->

            </div>
            <input type="hidden" id="page" name="page" value="1" >
        </form>

        <div class="showPopup">

        </div>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@stop
@section('after_script')

    <script>
        $('.select-fix').select2();
    </script>
    <script src="{{asset('static/backend/js/admin/config-term-investment/script.js?v='.time())}}" type="text/javascript"></script>
@stop