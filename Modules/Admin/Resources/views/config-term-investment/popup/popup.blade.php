
<div class="modal fade" id="show_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>{{_('Thêm kì hạn đầu tư')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-investment" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>
                                    {{__('Loại gói')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <select name="product_category_id" class="form-control select-fix">
                                        <option value="">Chọn loại gói</option>
                                        @foreach($category as $item)
                                            <option value="{{$item['product_category_id']}}" {{$getDetail != null && $getDetail['product_category_id'] == $item['product_category_id'] ? 'selected' : '' }}>{{$item['category_name_vi']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>
{{--                        <div class="col-lg-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>--}}
{{--                                    {{__('Loại kì hạn')}}: <b class="text-danger">*</b>--}}
{{--                                </label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <select id="type_investment" name="type_investment" class="form-control select-fix">--}}
{{--                                        <option value="month">Theo tháng</option>--}}
{{--                                        <option value="year">Theo năm</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        Theo tháng--}}
                        <div class="col-lg-12 month">
                            <div class="form-group">
                                <label>
                                    {{__('Số tháng')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
{{--                                    <input type="number" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="investment_time_month" class="form-control" placeholder="Số tháng">--}}
                                    <select class="form-control select-fix" name="investment_time_month">
                                        <option value="">Chọn số tháng</option>
                                        @for($i = 1 ; $i <= 11 ; $i++)
                                            <option value="{{$i}}" {{$getDetail != null && $getDetail['investment_time_month'] == $i ? 'selected' : '' }}>{{$i}} tháng</option>
                                        @endfor
                                        @for($i = 1 ; $i <= 10 ; $i++)
                                            <option value="{{$i*12}}" {{$getDetail != null && $getDetail['investment_time_month'] == $i*12 ? 'selected' : '' }}>{{$i*12}} tháng</option>
                                        @endfor
                                    </select>
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>
{{--                        Theo năm--}}
{{--                        <div class="col-lg-12 year">--}}
{{--                            <div class="form-group">--}}
{{--                                <label>--}}
{{--                                    {{__('Số năm')}}: <b class="text-danger">*</b>--}}
{{--                                </label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <select class="form-control select-fix" name="investment_time_year">--}}
{{--                                        <option value="">Chọn số năm</option>--}}
{{--                                        @for($i = 1 ; $i <= 10 ; $i++)--}}
{{--                                            <option value="{{$i*12}}" {{$getDetail != null && $getDetail['investment_time_month'] == $i*12 ? 'selected' : '' }}>{{$i}} năm</option>--}}
{{--                                        @endfor--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <span class="errs error-product-code"></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                    <input type="hidden" name="investment_time_id" value="{{$getDetail != null ? $getDetail['investment_time_id'] : ''}}">
                </form>
                <div class="save-attribute">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                        <div class="m-form__actions m--align-right">
                            <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close"
                               class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
                            </a>
                            @if($getDetail != null)
                                <a href="javascript:void(0)" onclick="configTermInvestment.editInvestment()"
                                   class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>{{__('LƯU THÔNG TIN')}}</span>
                                </span>
                                </a>
                            @else
                                <a href="javascript:void(0)" onclick="configTermInvestment.addInvestment()"
                                   class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>{{__('LƯU THÔNG TIN')}}</span>
                                </span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>