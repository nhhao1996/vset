@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-thong-ke.png')}}" alt=""
                style="height: 20px;"> {{__('THỐNG KÊ')}}</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .total-money {
            background-image: url({{asset("uploads/admin/report/hinh3.jpg")}});
            background-size: cover;
        }

        .align-conter1 {
            text-align: center;
        }

        .ss--text-white {
            color: white !important;
        }
        /*.amcharts-chart-div svg a {*/
        /*    display: none;*/
        /*}*/
        .select_week .select2 {
            width:20% !important;
        }
        #chart ,#chart .amcharts-chart-div {
            overflow: unset !important;
        }
        /*#chart svg {*/
        /*    height: 400px !important;*/
        /*}*/
    </style>

    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('THỐNG KÊ SỐ LƯỢNG GIAO DỊCH')}}
                    </h2>
                </div>
            </div>
        </div>
        <div class="m-portlet__body pb-5" id="autotable">
            <form id="form-chart">
                <div class="row form-group">
                    <div class="col-lg-2 col-md-3 form-group">
                        <select name="filter_type" class="form-control" id="filter_type" onchange="statisticsTransaction.filter()">
                            <option value="day">Báo cáo theo ngày</option>
                            <option value="year">Báo cáo theo năm</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-4 form-group ">
                        <div class="m-input-icon m-input-icon--right day_block block_hide" id="m_daterangepicker_6">
                            <input readonly=""
                                   class="form-control m-input daterange-picker ss--search-datetime-hd pr-3"
                                   id="time"
                                   name="time"
                                   autocomplete="off"
                                   placeholder="Từ ngày - đến ngày" data-bind="daterangepicker: dateRange"
                                   onchange="statisticsTransaction.filter()">
                        </div>
                        <div class="row">
                            <div class="m-input-icon year_block block_hide col-5" style="display: none">
                                <select class="form-control m-input" name="year" id="year" onchange="statisticsTransaction.filter()">
                                    @for($i = 0; $i <= 4; $i++)
                                        <option value="{{\Carbon\Carbon::now()->subYear($i)->year}}">
                                            {{ \Carbon\Carbon::now()->subYear($i)->year}}
                                        </option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group row">
                <div class="col-lg-12 form-group">
                    <div id="chart" style="height: 600px;"></div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" readonly="" class="form-control m-input daterange-picker"
           id="time-hidden" name="time-hidden" autocomplete="off">
@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/report/highcharts.js')}}"></script>
    <script src="{{asset('static/backend/js/admin/statistics-transaction/script.js?v='.time())}}"
            type="text/javascript"></script>
    <script>
        $('select').select2();
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
@stop