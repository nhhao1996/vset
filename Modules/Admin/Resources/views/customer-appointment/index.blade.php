@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ HỔ TRỢ KHÁCH HÀNG')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/

        .form-control-feedback {
            color: red;
        }

        .icon-padding {
            padding: 10px;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH HỔ TRỢ KHÁCH HÀNG')}}
                    </h2>

                </div>
            </div>

            <div class="m-portlet__head-tools">
{{--                @if(in_array('admin.customer.add', session()->get('routeList')))--}}
                    <a href="{{route('admin.customer-appointment.add')}}"
                       class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span>
                                <i class="fa fa-plus-circle"></i>
                                <span> {{__('THÊM LỊCH HẸN')}}</span>
                            </span>
                    </a>
{{--                @endif--}}
            </div>

        </div>
        <div class="m-portlet__body">
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-4">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       placeholder="{{__('Nhập tên hoặc số điện thoại')}}">
                                <div class="input-group-append">
{{--                                    <a href="javascript:void(0)" id="refresh"--}}
{{--                                       class="btn btn-info btn-sm m-btn--icon">--}}
{{--                                    <i class="la la-refresh"></i>--}}
{{--                                    </a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="m-form m-form--label-align-right">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group m-form__group">
                                        <button class="btn btn-primary color_button btn-search">
                                            {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-content m--padding-top-30">
                @include('admin::customer-appointment.list')

            </div><!-- end table-content -->

        </div>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')

    <script src="{{asset('static/backend/js/admin/customer-appointment/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
@stop