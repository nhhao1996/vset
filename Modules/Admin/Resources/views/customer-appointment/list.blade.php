<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Họ và tên')}}</th>
            <th class="tr_thead_list">{{__('Số điện thoại')}}</th>
            <th class="tr_thead_list">{{__('Mô tả')}}</th>
            <th class="tr_thead_list">{{__('Thơi gian hẹn')}}</th>
            <th class="tr_thead_list">{{__('Thơi gian đặt')}}</th>
            <th class="tr_thead_list">{{__('Trạng thái')}}</th>
            <th class="tr_thead_list">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td class="text_middle">{{$item['full_name_cus']}}</td>
                    <td class="text_middle">{{$item['phone2']}}</td>
                    <td class="text_middle">{{subString($item['description'])}}</td>
                    <td class="text_middle">{{date("d/m/Y H:i:s",strtotime($item['date_appointment']))}}</td>
                    <td class="text_middle">{{date("d/m/Y H:i:s",strtotime($item['created_at']))}}</td>
                    <td class="text_middle">
                        @if($item['status'] == 'new')
                            {{__('Mới')}}
                        @elseif($item['status'] == 'confirm')
                            {{__('Đã Xác nhận')}}
                        @elseif($item['status'] == 'cancel')
                            {{__('Đã hủy')}}
                        @elseif($item['status'] == 'finish')
                            {{__('Đã hoàn hành')}}
                        @else
                            {{__('Đang chờ')}}
                        @endif
                    </td>
                        <td class="text-center">
                            <a class ="a-custom" href="{{route("admin.customer.detail",$item['customer_appointment_id'])}}">
                                <i class="flaticon-eye"></i>
                            </a>
                            <a class="a-custom" href="{{route("admin.customer.edit",$item['customer_appointment_id'])}}">
                                <i class="flaticon-edit"></i>
                            </a>
                            <a class="a-custom" href="javascript:void(0)" onclick="customer.resetPass('{!! $item['customer_appointment_id'] !!}')">
                                <i class="la la-refresh"></i>
                            </a>
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }
    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }
    .a-custom .flaticon-eye{
        top: 1px;
        left: 2px;
    }
</style>