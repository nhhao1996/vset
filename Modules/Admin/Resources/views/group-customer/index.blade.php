@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ NHÓM KHÁCH HÀNG')}}</span>
@stop
@section('content')

    <style>
        .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > span, .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > input:checked ~ span {
            background: #4fc4ca;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <form class="frmFilter">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                             <i class="la la-th-list"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{__('DANH SÁCH NHÓM KHÁCH HÀNG')}}
                        </h2>

                    </div>
                </div>
                <div class="m-portlet__head-tools nt-class">
                    @if(in_array('admin.group-customer.create', session('routeList')))
                        <a href="{{route('admin.group-customer.create')}}"
                           class="btn btn-primary btn-default-piospa m-btn m-btn--icon m-btn--pill btn-sm">
                                <span>
                                    <i class="flaticon-add-circular-button"></i>
                                    <span>@lang('Thêm')</span>
                                </span>
                        </a>
                    @endif
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="frmFilter bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search"
                                           placeholder="@lang('Nhập tên nhóm khách hàng')">
                                </div>
                            </div>
                        </div>
{{--                        <div class="col-lg-3">--}}
{{--                            <div class="form-group m-form__group">--}}
{{--                                <div class="input-group">--}}
{{--                                    <select class="form-control" name="group_customer$is_active">--}}
{{--                                        <option value="">{{__('Chọn trạng thái')}}</option>--}}
{{--                                        <option value="1">{{__('Hoạt động')}}</option>--}}
{{--                                        <option value="0">{{__('Tạm ngưng')}}</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button type="submit"
                                        class="btn btn-primary color_button btn-search"
                                        style="width: 150px">
                                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                </button>
                                <a href="{{route('admin.group-customer')}}"
                                   class="btn btn-metal  btn-search padding9x padding9px">
                                    <span><i class="flaticon-refresh"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::group-customer.list')
                </div><!-- end table-content -->

            </div>
        </form>
    </div>
@endsection
@section("after_style")
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/group-customer/index.js?v='.time())}}"
            type="text/javascript"></script>
@stop