<div class="modal fade" id="modal_customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-width-80" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    @lang('Thêm khách hàng')
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-lg-6">
                        <input placeholder="Nhập tên, sđt hoặc email"
                               type="text"
                               name="name"
                               class="form-control keyword">
                    </div>
                    <div class="col-lg-3">
                        <select
                                class="form-control is_actived">
                            <option value="">
                                @lang('Trạng thái')
                            </option>
                            <option value="1">
                                @lang('Hoạt động')
                            </option>
                            <option value="0">
                                @lang('Tạm ngưng')
                            </option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <button onclick="groupCustomer.loadCustomer()"
                                type="button"
                                class="btn btn-primary btn-default-piospa">
                            @lang('Tìm kiếm')
                        </button>
                    </div>
                </div>
                <div class="form-group" id="div_list_customer">
                    <table class="table table-striped m-table ss--header-table">
                        <thead>
                        <tr class="ss--nowrap">
                            <th class="m--width-10">
                                <label class="m-checkbox m-checkbox--solid m-checkbox--state-success m--margin-bottom-15">
                                    <input type="checkbox">
                                    <span></span>
                                </label>
                            </th>
                            <th>#</th>
                            <th>
                                @lang('Mã nhà đầu tư')
                            </th>
                            <th>
                                @lang('Họ và tên')
                            </th>
                            <th>
                                @lang('Email')
                            </th>
                            <th>
                                @lang('Số điện thoại')
                            </th>
                            <th>
                                @lang('Trạng thái')
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-metal" data-dismiss="modal">
                    <i class="la la-arrow-left"></i>
                    @lang('Hủy')
                </button>
                <button type="button"
                        class="btn btn-primary btn-default-piospa"
                        onclick="groupCustomer.submitAddItem()">
                    @lang('Thêm khách hàng')
                </button>
            </div>
        </div>
    </div>
</div>