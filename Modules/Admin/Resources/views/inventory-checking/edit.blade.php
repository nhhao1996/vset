@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-kho.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ KHO')}}
    </span>
@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
@stop
@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CẬP NHẬT PHIẾU KIỂM KHO')}}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group" style="display: none">
                        <label>
                            {{__('Mã phiếu')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="input-group">
                            <div class="input-group m-input-group m-input-group--solid">
                                <input readonly class="form-control" value="{{$inventoryChecking->code}}"
                                       id="checking-code" type="text">
                            </div>
                        </div>
                        <span class="errs error-supplier"></span>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Kho')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="input-group">
                            <div class="input-group m-input-group">
                                <select id="warehouse" class="form-control m_selectpicker">
                                    @foreach($warehouse as $key=>$value)
                                        @if($inventoryChecking->warehouseId==$key)
                                            <option selected value="{{$key}}">{{$value}}</option>
                                        @else
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <span class="errs error-warehouse"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group" style="display: none">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>
                                    {{__('Người tạo')}}:
                                </label>
                                <div class="input-group">
                                    <div class="input-group m-input-group m-input-group--solid">
                                        <input id="created-by" type="text" value="{{$inventoryChecking->user}}" readonly
                                               class="form-control m-input class">
                                    </div>
                                    <span class="errs error-product-name"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label>
                                    {{__('Ngày kiểm tra')}}:
                                </label>
                                <div class="input-group">
                                    <div class="input-group m-input-group m-input-group--solid">
                                        <div class="input-group-append">
                                            <input disabled id="created-at" type="text"
                                                   class="form-control m-input class"
                                                   value="{{(new DateTime($inventoryChecking->createdAt))->format('d/m/Y')}}"
                                                   aria-describedby="basic-addon1">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <span class="errs error-day-checking"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Lý do')}}:<b class="text-danger">*</b>
                        </label>
                        <div class="input-group">
                                <textarea placeholder="{{__('Nhập lý do kiểm kho')}}" rows="5" cols="50" name="description"
                                          id="note" class="form-control">{{$inventoryChecking->reason}}</textarea>
                        </div>
                    </div>
                    <span class="errs error-note"></span>
                </div>
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" style="margin-bottom: 0;" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show son" data-toggle="tab" href="#" data-target="#inventory">
                                <h7>{{__('THỦ CÔNG')}}</h7>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link son" data-toggle="tab" href="#inventory-input">
                                <h7>{{__('QUÉT MÃ QR CODE')}}</h7>
                            </a>
                        </li>
                    </ul>
                    <div class="bd-ct">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="inventory" role="tabpanel">
                                <div class="form-group m-form__group">
                                    <label class="label-son">
                                        {{__('Danh sách sản phẩm')}}:
                                    </label>
                                    <div class="col-xl-6 order-2 order-xl-1">
                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="input-group">
                                                <select style="width: 100%" class="form-control ss--width-100-"
                                                        name="list-product"
                                                        id="list-product">
                                                    <option value="">{{__('Chọn sản phẩm')}}</option>
                                                    @foreach($productList as $key=>$value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="inventory-input" role="tabpanel">
                                <div class="form-group m-form__group">
                                    <label class="label-son">
                                        {{__('Mã sản phẩm')}}:
                                    </label>
                                    <div class="col-xl-6 order-2 order-xl-1">
                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="input-group col-xs-10">
                                                <div class="input-group m-input-group">
                                                    <input placeholder="{{__('Nhập mã sản phẩm')}}" autofocus id="product-code"
                                                           type="text" value=""
                                                           class="form-control m-input class">
                                                </div>
                                                <span class="errs error-code-product"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Table version--}}
                        <div class="table-responsive">
                            <table id="table-product"
                                   class="table table-striped m-table ss--header-table">
                                <thead>
                                <tr class="ss--nowrap">
                                    <th class="ss--font-size-th ss--text-center">#</th>
                                    <th class="ss--font-size-th">{{__('SẢN PHẨM')}}</th>
                                    <th class="ss--font-size-th ss--text-center">{{__('ĐƠN VỊ TÍNH')}}</th>
                                    <th class="ss--font-size-th ss--text-center">{{__('HỆ THỐNG')}}</th>
                                    <th class="ss--font-size-th ss--text-center">{{__('THỰC TẾ')}}</th>
                                    <th class="ss--font-size-th ss--text-center">{{__('CHÊNH LỆCH')}}</th>
                                    <th class="ss--font-size-th ss--text-center">{{__('XỬ LÝ')}}</th>
                                    <th class="ss--font-size-th"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($inventoryCheckingDetail as $key=>$value)
                                    <tr class="ss--select2-mini">
                                        <td class="stt ss--font-size-13 ss--text-center">{{($key+1)}}</td>
                                        <td class="ss--font-size-13">{{ $value['productName'] }}
                                            <input type="hidden" class="productCode"
                                                   value="{{ $value['productCode'] }}">
                                        </td>
                                        <td style="width: 150px" class="ss--font-size-13 ss--text-center">
                                            <select class="form-control unit ss--width-150">
                                                @foreach($unit as $k=>$v)
                                                    @if($k==$value['unitId'])
                                                        <option selected value="{{$k}}">{{$v}}</option>
                                                    @else
                                                        <option value="{{$k}}">{{$v}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td valign="top" style="width: 140px" class="ss--text-center ss--font-size-13">
                                            {{$value['quantityOld']!=null?$value['quantityOld']:0}}
                                            @if($value['quantityOld']!=null)
                                                <input readonly style="text-align: center" type="hidden"
                                                       class="form-control quantityOld"
                                                       value="{{ $value['quantityOld'] }}">
                                            @else
                                                <input readonly style="text-align: center" type="hidden"
                                                       class="form-control quantityOld"
                                                       value="0">
                                            @endif
                                        </td>
                                        <td style="width: 150px" class="ss--font-size-13 ss--text-center">
                                            <input onchange="changeQuantityNew(this)"
                                                   onkeyup="changeQuantityNew(this)" min="0"
                                                   style="text-align: center" type="text"
                                                   class="form-control ss--btn-ct quantityNew ss--width-150"
                                                   value="{{ $value['quantityNew'] }}">
                                        </td>
                                        <td valign="top" style="width: 140px" class="ss--font-size-13 ss--text-center">
                                            <span class="quantityDifference">{{$value['quantityOld']-$value['quantityNew']}}</span>
                                            <input style="text-align: center" readonly type="hidden"
                                                   class="form-control quantityDifference"
                                                   value="{{ $value['quantityOld']-$value['quantityNew']}}">
                                        </td>
                                        <td style="width: 100px" class="ss--font-size-13 ss--text-center">
                                            @if($value['quantityOld']-$value['quantityNew']>0)
                                                <b class="m--font-danger resolve">
                                                    {{__('Xuất kho')}}
                                                </b>
                                            @elseif($value['typeResolve']=="input")
                                                <b class="m--font-success resolve">
                                                    {{__('Nhập kho')}}
                                                </b>
                                            @else
                                                <b class="m--font-success resolve"></b>
                                            @endif
                                        </td>
                                        <td style="width: 100px" class="ss--font-size-13 ss--text-center">
                                            <button onclick="deleteProductInList(this)"
                                                    class="change-class m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                                    title="Xóa">
                                                <i class="la la-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group row pull-right">
                                    <div class="col-12">
                                        <span class="errs error-product"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                            <div class="m-form__actions m--align-right">
                                <button onclick="location.href='{{route('admin.product-inventory')}}'"
                                        class="ss--btn-mobiles btn btn-metal ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-bottom-5">
                                                    <span class="ss--text-btn-mobi">
                                                    <i class="la la-arrow-left"></i>
                                                    <span>{{__('HỦY')}}</span>
                                                    </span>
                                </button>
                                <button id="btn-save-draft" type="button"
                                        class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5">
                                    <span class="ss--text-btn-mobi">
                                    <i class="la la-file-o"></i>
                                    <span>{{__('LƯU NHÁP')}}</span>
                                    </span>
                                </button>
                                <button type="button"
                                        class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md btn-save m--margin-left-10 m--margin-bottom-5">
                                    <span class="ss--text-btn-mobi">
                                        <i class="la la-check"></i>
                                        <span>{{__('LƯU THÔNG TIN')}}</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="idHidden" value="{{$inventoryChecking->id}}">
@endsection
@section('after_script')
    <script type="text/template" id="product-childs">
        <tr class="ss--select2-mini">
            <td class="stt ss--font-size-13 ss--text-center">{stt}</td>
            <td class="name-version ss--font-size-13">{name}
                <input type="hidden" class="productCode" value="{code}">
                <input class="cost" type="hidden"
                       value="{cost}">
            </td>
            <td style="width: 150px" class="ss--font-size-13 ss--text-center">
                <select class="form-control unit ss--width-150">
                    {option}
                </select>
            </td>
            <td valign="top" style="width: 140px" class="ss--text-center ss--font-size-13">
                {quantityOld}
                <input readonly style="text-align: center" type="hidden" class="form-control quantityOld"
                       value="{quantityOld}">
            </td>
            <td style="width: 150px" class="ss--font-size-13 ss--text-center">
                <input min="0" onchange="changeQuantityNew(this)"
                       style="text-align: center"
                       type="text" class="form-control quantityNew ss--btn-ct ss--width-150"
                       value="{quantityNew}">
            </td>
            <td valign="top" style="width: 140px" class="ss--font-size-13 ss--text-center">
                <span class="quantityDifference2">{quantityDifference}</span>
                <input style="text-align: center" readonly type="hidden"
                       class="form-control quantityDifference"
                       value="{quantityDifference}">
            </td>
            <td class="typeResolve ss--font-size-13 ss--text-center" style="width: 100px">
                <b class="resolve">
                </b>
            </td>
            <td style="width: 100px" class="ss--font-size-13 ss--text-center">
                <button onclick="deleteProductInList(this)"
                        class="change-class m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                        title="Xóa">
                    <i class="la la-trash"></i>
                </button>
            </td>
        </tr>
    </script>
    <script src="{{asset('static/backend/js/admin/inventory-checking/edit-script.js?v='.time())}}"
            type="text/javascript"></script>
@endsection