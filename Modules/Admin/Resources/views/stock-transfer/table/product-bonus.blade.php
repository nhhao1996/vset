<div class="table-responsive pb-5">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <td>{{__('Hình thức thanh toán')}}</td>
            <td>{{__('Tỉ lệ tiền thưởng')}} (%)</td>
            <td>{{__('Kỳ hạn đầu tư')}} ({{__('tháng')}})</td>
            <td>{{__('Trạng thái')}}</td>
            <td class="action-product">{{__('Hành động')}}</td>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $key => $item)
            <tr>
                <td>{{$item['payment_method_name_vi']}}</td>
                <td>{{number_format($item['bonus_rate'])}}</td>
                <td>{{$item['investment_time_month']}} </td>
                <td>
                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                            <input type="checkbox" disabled class="manager-btn" {{$item['is_actived'] == 1 ? 'checked' : ''}}>
                            <span></span>
                        </label>
                    </span>
                </td>
                <td class="action-product">
                    <a href="javascript:void(0)" onclick="product.detailProductBonus({{$item['product_bonus_id']}})"
                       class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                       title="{{__('Chi tiết')}}">
                        <i class="la la-eye"></i>
                    </a>
                    <a href="javascript:void(0)" onclick="product.editProductBonus({{$item['product_bonus_id']}})"
                       class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                       title="{{__('Chỉnh sửa')}}">
                        <i class="la la-edit"></i>
                    </a>
                    <button type="button" onclick="product.removeProductBonus(this, {{$item['product_bonus_id']}})"
                            class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                            title="{{__('Xoá')}}">
                        <i class="la la-trash"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $list->links('admin::product.helpers.paging-product-bonus') }}
</div>

