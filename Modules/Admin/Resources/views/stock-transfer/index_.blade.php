@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ BIỂU ĐỒ HỢP TÁC ĐẦU TƯ')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        DANH SÁCH YÊU CẦU CHUYỂN ĐỔI
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                <a onclick="StockChartHandler.showPopupImport()" href="javascript:void(0)" class="mr-4 btn btn-primary btn-sm color_button m-btn m-btn--icon m-btn--pill btn_add_pc">
                        <span>
						    <i class="fa fa-plus-circle"></i>
							<span> Import</span>
                        </span>
                </a>
                <a onclick="StockChartHandler.showPopupAdd()" href="javascript:void(0)" class="btn btn-primary btn-sm color_button m-btn m-btn--icon m-btn--pill btn_add_pc">
                        <span>
						    <i class="fa fa-plus-circle"></i>
							<span> THÊM GIÁ TRỊ</span>
                        </span>
                </a>

            </div>
        </div>
        {{--        <div class="m-portlet__head">--}}
        {{--            <div class="m-portlet__head-caption">--}}
        {{--                <div class="m-portlet__head-title">--}}
        {{--                     <span class="m-portlet__head-icon">--}}
        {{--                        <i class="la la-th-list"></i>--}}
        {{--                     </span>--}}
        {{--                    <h3 class="m-portlet__head-text">--}}
        {{--                        {{__('DANH SÁCH GÓI TRÁI PHIẾU - TIẾT KIỆM')}}--}}
        {{--                    </h3>--}}
        {{--                </div>--}}
        {{--            </div>--}}

        {{--                --}}{{-- Begin: Tab--------------- --}}
        {{--                <div class="d-flex ml-auto">--}}
        {{--                    <div  class="m-portlet__head-caption p-3 @if($type=='bond') color_button @endif">--}}
        {{--                        <div class="m-portlet__head-title">--}}

        {{--                            <a id="traiphieu"  href="{{route('admin.stock-transfer.getListBond')}}" class="m-portlet__head-text ">--}}
        {{--                                TRÁI PHIẾU--}}
        {{--                            </a>--}}

        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                    <div class="m-portlet__head-caption p-3 @if($type=='saving') color_button @endif">--}}
        {{--                        <div class="m-portlet__head-title">--}}
        {{--                            <a  href="{{route('admin.stock-transfer.getListSaving')}}" class="m-portlet__head-text ">--}}
        {{--                                TIẾT KIỆM--}}
        {{--                            </a>--}}

        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--                --}}{{-- end: Tab --}}
        {{--        </div>--}}
        <div class="m-portlet__body">
            <form class="frmFilter ">
                <div class="row ss--bao-filter ss--background">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                {{--                                <input type="hidden" name="search_type" value="product_name_vi">--}}
                                <button class="btn btn-primary btn-search" style="display: none">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input type="text"  class="form-control " name="search_keyword"
                                       placeholder="{{__('Nhập tên, mã, sđt khách hàng')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%;" name="status" class="form-control m-input ss--select-2">
                            <option value="">Chọn trạng thái</option>
                            <option value="new">Mới</option>
                            <option value="approved">Đã duyệt</option>
                            <option value="cancel">Hủy</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                {{--                                <input type="hidden" name="search_type" value="product_name_vi">--}}
                                <button class="btn btn-primary btn-search" style="display: none">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input type="text"  class="form-control time daterange" name="created_at"
                                       placeholder="{{__('Nhập thời gian')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 form-group">
                        <div class="form-group m-form__group">
                            <button href="javascript:void(0)"
                                    class="btn ss--btn-search ">
                                {{__('TÌM KIẾM')}}
                                <i class="fa fa-search ss--icon-search"></i>
                            </button>
                            <a href="{{route('admin.product')}}"
                               class="btn btn-metal  btn-search padding9x">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="table-content m--padding-top-30">
                    @include('admin::stock-chart.list')

                </div><!-- end table-content -->
            </form>
            @include('admin::stock-chart.popup.add')
            @include('admin::stock-chart.popup.edit')
            @include('admin::stock-chart.popup.import-excel')
        </div>
    </div>
@endsection
@section('after_script')
    <script>
        $('#autotable').PioTable({
            baseUrl: laroute.route('admin.stock-chart.list')
        });

        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>

    <script src="{{asset('static/backend/js/admin/stock-chart/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        var LIST = @json($LIST);
        $("[name='time']").datetimepicker({
            format: 'dd/mm/yyyy hh:ii:00'
        });
        $('.time').daterangepicker({
            // changeMonth: true,
            // changeYear: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });


    </script>
@stop
