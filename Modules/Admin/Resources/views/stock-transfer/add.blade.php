@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <style>
        .note-editor {
            width: 100%;
        }
    </style>
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ GÓI')}}
    </span>
@endsection
@section('content')
    <form id="form-product" autocomplete="off">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                        <h3 class="m-portlet__head-text">
                            {{__('THÊM GÓI')}}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <div
                            class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--open btn-hover-add-new"
                            m-dropdown-toggle="hover" aria-expanded="true">
                        <div class="m-dropdown__wrapper dropdow-add-new" style="z-index: 101;display: none">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                  style="left: auto; right: 21.5px;"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__item">
                                                <a data-toggle="modal"
                                                   data-target="#modal-add-product-category" href="" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-users"></i>
                                                    <span class="m-nav__link-text">{{__('Thêm danh mục sản phẩm')}} </span>
                                                </a>
                                                <a data-toggle="modal"
                                                   data-target="#modal-add-unit" href="" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-users"></i>
                                                    <span class="m-nav__link-text">{{__('Thêm đơn vị tính')}} </span>
                                                </a>
                                                <a data-toggle="modal"
                                                   data-target="#modal-add-product-model" href="" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-users"></i>
                                                    <span class="m-nav__link-text">{{__('Thêm nhãn hiệu')}} </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>{{__('Danh mục')}}:<b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <select style="width: 100%" id="product_category_id" name="product_category_id" class="form-control product_category_id m_selectpicker"
                                        title="{{__('Chọn danh mục')}}">
                                    <option value="">{{__('Chọn danh mục')}}</option>
                                    @foreach($category as $key=>$value)
                                        <option value="{{$value['product_category_id']}}">{{$value['category_name_vi']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <span class="errs error-category"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Mã gói')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="product-code" name="product_code" type="text" class="form-control m-input class"
                                       placeholder="{{__('Mã gói')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-product-code"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tên gói VI')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="product-name-vi" name="product_name_vi" type="text" class="form-control m-input class"
                                       placeholder="{{__('Tên gói VI')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-product-name-vi"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tên gói EN')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="product-name-en" name="product_name_en" type="text" class="form-control m-input class"
                                       placeholder="{{__('Tên gói EN')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-product-name-en"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Mô tả ngắn VI')}}:
                            </label>
                            <div class="input-group">
                                <textarea id="description-vi" name="product_short_name_vi" type="text" class="form-control m-input class summernote-fix"
                                          placeholder="{{__('Mô tả ngắn VI')}}"
                                          aria-describedby="basic-addon1"></textarea>
                            </div>
                            <span class="errs error-product-short-name-vi"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Mô tả ngắn EN')}}:
                            </label>
                            <div class="input-group">
                                <textarea id="description-vi" name="product_short_name_en" type="text" class="form-control m-input class summernote-fix"
                                          placeholder="{{__('Mô tả ngắn EN')}}"
                                          aria-describedby="basic-addon1"></textarea>
                            </div>
                            <span class="errs error-product-short-name-en"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Mô tả VI')}}:
                            </label>
                            <div class="input-group">
                                <textarea id="description-vi" name="description_vi" type="text" class="form-control m-input class summernote-fix"
                                       placeholder="{{__('Mô tả Vi')}}"
                                          aria-describedby="basic-addon1"></textarea>
                            </div>
                            <span class="errs error-description-vi"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Mô tả EN')}}:
                            </label>
                            <div class="input-group">
                                <textarea id="description-en" name="description_en" type="text" class="form-control m-input class summernote-fix"
                                       placeholder="{{__('Mô tả EN')}}"
                                          aria-describedby="basic-addon1"></textarea>
                            </div>
                            <span class="errs error-description-en"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Giá trị gói')}} (Vnđ) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="price-standard" name="price_standard" type="text" class="form-control m-input class number-money-fix"
                                       placeholder="{{__('Giá trị gói')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-price-standard"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tỉ lệ lãi suất chuẩn')}} (%/năm) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="interest-rate-standard" name="interest_rate_standard" type="text" class="number-money form-control m-input class"
                                       placeholder="{{__('Tỉ lệ lãi suất chuẩn')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-interest-rate-standard"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tỉ lệ phí rút tiền trước kỳ hạn')}} (%) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="withdraw-fee-rate-before" name="withdraw_fee_rate_before" type="text" class="number-money form-control m-input class"
                                       placeholder="{{__('Tỉ lệ phí rút tiền trước kỳ hạn')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-withdraw-fee-rate-before"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tỉ lệ phí rút tiền đúng kỳ hạn')}} (%) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="withdraw-fee-rate-ok" name="withdraw_fee_rate_ok" type="text" class="number-money form-control m-input class"
                                       placeholder="{{__('Tỉ lệ phí rút tiền trước đúng kỳ hạn')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-withdraw-fee-rate-ok"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tỉ lệ phí rút tiền lãi ')}} (%) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="withdraw-fee-interest-rate" name="withdraw_fee_interest_rate" type="text" class="number-money form-control m-input class"
                                       placeholder="{{__('Tỉ lệ phí rút tiền lãi')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-withdraw-fee-interest-rate"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Số tiền tối thiểu có thể rút lãi')}} (Vnđ) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="withdraw-min-amount" name="withdraw_min_amount" type="text" class="form-control m-input class number-money-fix"
                                       placeholder="{{__('Số tiền tối thiểu có thể rút lãi')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-withdraw-min-amount"></span>
                        </div>
                    </div>
{{--                    <div class="col-lg-6">--}}
{{--                        <div class="form-group m-form__group">--}}
{{--                            <label>--}}
{{--                                {{__('Giá trị thấp nhất có thể bán')}} (Vnđ) : <b class="text-danger">*</b>--}}
{{--                            </label>--}}
{{--                            <div class="input-group">--}}
{{--                                <input id="min-allow-sale" name="min_allow_sale" type="text" class="form-control m-input class number-money-fix"--}}
{{--                                       placeholder="{{__('Giá trị thấp nhất có thể bán')}}"--}}
{{--                                       aria-describedby="basic-addon1">--}}
{{--                            </div>--}}
{{--                            <span class="errs error-min-allow-sale"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Ngày phát hành')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="min-allow-sale" name="published_at" type="text" class="published_at form-control m-input class"
                                       placeholder="{{__('Ngày phát hành')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-min-allow-sale"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Thưởng khi gia hạn hợp đồng')}} (Vnđ) : <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="withdraw-min-amount" name="bonus_extend" type="text" class="form-control m-input class number-money-fix"
                                       placeholder="{{__('Thưởng khi gia hạn hợp đồng')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-bonus_extend"></span>
                        </div>
                    </div>
{{--                    <div class="col-lg-6">--}}
{{--                        <div class="form-group m-form__group" id="month_extend">--}}
{{--                            <label>--}}
{{--                                {{__('Thời gian gia hạn hợp đồng trước ngày hết hạn hợp đồng')}}:--}}
{{--                            </label>--}}
{{--                            <div class="input-group">--}}
{{--                                <select class="form-control"  name="month_extend">--}}
{{--                                    <option value="0">{{__('Chọn tháng')}}</option>--}}
{{--                                    @for($i = 1 ; $i <= 12 ; $i ++)--}}
{{--                                        <option value="{{$i}}">{{$i}} {{__('tháng')}}</option>--}}
{{--                                    @endfor--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <span class="errs error-month_extend"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-6">--}}
{{--                        <div class="form-group m-form__group">--}}
{{--                            <label>--}}
{{--                                {{__('Số tiền thưởng tối thiểu có thể rút')}} (Vnđ) : <b class="text-danger">*</b>--}}
{{--                            </label>--}}
{{--                            <div class="input-group">--}}
{{--                                <input id="withraw-min-bonus" name="withraw_min_bonus" type="text" class="form-control m-input class number-money-fix"--}}
{{--                                       placeholder="{{__('Số tiền thưởng tối thiểu có thể rút')}}"--}}
{{--                                       aria-describedby="basic-addon1">--}}
{{--                            </div>--}}
{{--                            <span class="errs error-withraw_min_bonus"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Trạng thái')}}:
                            </label>
                            <div class="input-group">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label style="margin: 0 0 0 10px; padding-top: 4px">
                                        <input type="checkbox" id="is_actived" class="manager-btn" disabled>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group" id="withdraw_min_time">
                            <label>
                                {{__('Thời gian tối thiểu có thể rút gốc')}}:
                            </label>
                            <div class="input-group">
                                <select class="form-control"  name="withdraw_min_time">
                                    <option value="">{{__('Chọn thời gian tối thiểu có thể rút gốc')}}</option>
                                    @for($i = 1 ; $i <= 12 ; $i ++)
                                        <option value="{{$i}}">{{$i}} {{__('tháng')}}</option>
                                    @endfor
                                </select>
                            </div>
                            <span class="errs error-min-allow-sale"></span>
                        </div>
                    </div>
{{--                    <div class="col-6"></div>--}}
{{--                    ------------------------------------------------------------------------------------------------------------------------------------------------------------}}
                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình đại diện gói (VI)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_main_hidden_vi" >
                                <input type="hidden" id="image_main_upload_vi" name="product_avatar_vi"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_main_vi"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_main_vi" type='file'
                                       onchange="uploadImageMain(this,'vi');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_main_vi').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình chi tiết gói (VI)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_detail_hidden_vi">
                                <input type="hidden" id="image_detail_upload_vi" name="product_image_detail_vi"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_detail_vi"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_detail_vi" type='file'
                                       onchange="uploadImageDetail(this,'vi');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_detail_vi').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                        <span class="m--margin-left-20">
                                            <i class="fa fa-camera"></i>
                                            <span>
                                                {{__('Tải ảnh lên')}}
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình đại diện gói (mobile) (VI)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_main_hidden_mobile_vi" >
                                <input type="hidden" id="image_main_upload_mobile_vi" name="product_avatar_mobile_vi"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_main_mobile_vi"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_main_mobile_vi" type='file'
                                       onchange="uploadImageMainMobile(this,'vi');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_main_mobile_vi').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình chi tiết gói (moible) (VI)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_detail_hidden_mobile_vi">
                                <input type="hidden" id="image_detail_upload_mobile_vi" name="product_image_detail_mobile_vi"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_detail_mobile_vi"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_detail_mobile_vi" type='file'
                                       onchange="uploadImageDetailMobile(this,'vi');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_detail_mobile_vi').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

{{--                    ------------------------------------------------------------------------------------------------------------------------------------------------}}
                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình đại diện gói (EN)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_main_hidden_en" >
                                <input type="hidden" id="image_main_upload_en" name="product_avatar_en"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_main_en"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_main_en" type='file'
                                       onchange="uploadImageMain(this,'en');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_main_en').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình chi tiết gói (EN)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_detail_hidden_en">
                                <input type="hidden" id="image_detail_upload_en" name="product_image_detail_en"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_detail_en"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_detail_en" type='file'
                                       onchange="uploadImageDetail(this,'en');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_detail_en').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình đại diện gói (mobile) (EN)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_main_hidden_mobile_en" >
                                <input type="hidden" id="image_main_upload_mobile_en" name="product_avatar_mobile_en"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_main_mobile_en"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_main_mobile_en" type='file'
                                       onchange="uploadImageMainMobile(this,'en');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_main_mobile_en').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình chi tiết gói (moible) (EN)')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="image_detail_hidden_mobile_en">
                                <input type="hidden" id="image_detail_upload_mobile_en" name="product_image_detail_mobile_en"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_detail_mobile_en"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_image_detail_mobile_en" type='file'
                                       onchange="uploadImageDetailMobile(this,'en');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_image_detail_mobile_en').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    ------------------------------------------------------------------------------------------------------------------------------------------------}}

                    <div class="col-6">
                        <div class="form-group m-form__group ">
                            <label>{{__('Ảnh gói hợp đồng')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('customer-contract.upload-dropzone')}}" id="dropzoneone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('Ảnh gói hợp đồng')}}
                                    </h3>
                                    <span>{{__('Chọn ảnh gói hợp đồng')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image">
                            @if( isset($imageContract) && count($imageContract) > 0)
                                @foreach($imageContract as $key => $v)
                                    <div class="image-show-child col-12">
                                        <input type="hidden" name="arrImageContract[desktop][{{$key}}][image]" value="{{$v['name']}}">
                                        <a href="{{asset($v['image_file'])}}" target="_blank">
                                            <p>{{asset($v['image_file'])}}</p>
                                        </a>
                                        <span class="delete-img-sv" style="display: block;">
                                            <a href="javascript:void(0)" onclick="removeImage(this)">
                                                <i class="la la-close"></i>
                                            </a>
                                        </span>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group m-form__group ">
                            <label>{{__('Ảnh gói hợp đồng (mobile)')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('customer-contract.upload-dropzone')}}" id="dropzoneoneimage">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('Ảnh gói hợp đồng (mobile)')}}
                                    </h3>
                                    <span>{{__('Chọn ảnh gói hợp đồng (mobile)')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image-mobile">
                            @if( isset($imageContractMobile) && count($imageContractMobile) > 0)
                                @foreach($imageContractMobile as $key => $v)
                                    <div class="image-show-child-mobile col-12">
                                        <input type="hidden" name="arrImageContract[mobile][{{$key}}][image]" value="{{$v['name']}}">
                                        <input type="hidden" name="type" value="1">
                                        <a href="{{asset($v['image_file'])}}" target="_blank">
                                            <p>{{asset($v['image_file'])}}</p>
                                        </a>
                                        <span class="delete-img-sv" style="display: block;">
                                                <a href="javascript:void(0)" onclick="removeImageMobile(this)">
                                                    <i class="la la-close"></i>
                                                </a>
                                            </span>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer save-attribute m--margin-right-20">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.product')}}"
                           class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                        </a>
                        <button onclick="product.addProduct(1)" type="button"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                            <i class="la la-check"></i>
                            <span>{{__('LƯU THÔNG TIN')}}</span>
                            </span>
                        </button>
                        <button type="button" onclick="product.addProduct(0)"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                             <i class="fa fa-plus-circle"></i>
                            <span> {{__('LƯU & TẠO MỚI')}}</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('after_script')
{{--    <script src="{{asset('static/backend/js/admin/product/jquery.masknumber.js')}}"--}}
            {{--type="text/javascript"></script>--}}
    <script>
        var decimal_number = 0;
        var numberImage = 0;
        var numberImageMobile = 0;
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/product/script.js?v='.time())}}"></script>

    <script>
        var Summernote = {
            init: function () {
                // $(".summernote").summernote({
                //     height: 208,
                //     placeholder: 'Nhập nội dung',
                //     toolbar: [
                //         ['style', ['bold', 'italic', 'underline']],
                //         ['fontsize', ['fontsize']],
                //         ['color', ['color']],
                //         ['para', ['ul', 'ol', 'paragraph']],
                //         // ['insert', ['link', 'picture']]
                //     ]
                //     })
            }
        };
        jQuery(document).ready(function () {
            // $('.summernote-fix').summernote({
            //     height: 100,
            //     placeholder: 'Nhập nội dung',
            //     toolbar: [
            //         ['style', ['bold', 'italic', 'underline']],
            //         ['fontsize', ['fontsize']],
            //         ['color', ['color']],
            //         ['para', ['ul', 'ol', 'paragraph']],
            //         // ['insert', ['link', 'picture']]
            //     ]
            // });
            // Summernote.init();
            $('.note-btn').attr('title', '');
            $('.published_at').datepicker({
                format: 'dd/mm/yyyy',
                // rtl: KTUtil.isRTL(),
                // todayBtn: "linked",
                // clearBtn: true,
                todayHighlight: true,
                // templates: arrows
            });
        });
        new AutoNumeric.multiple('.number-money', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });

        new AutoNumeric.multiple('.number-money-fix', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 0
        });

    </script>

    <script>
        dropzone();
    </script>
    <script type="text/template" id="imageShow">
        <div class="image-show-child mb-0 col-12">
            <input type="hidden" name="arrImageContract[desktop][{numberImage}][image]" value="{link_hidden}">
            <p>{{asset('{link}')}}</p>
            <span class="delete-img-sv" style="display: block;">
                    <a href="javascript:void(0)" onclick="removeImage(this)">
                        <i class="la la-close"></i>
                    </a>
                </span>
        </div>
    </script>

    <script type="text/template" id="imageShowMobile">
        <div class="image-show-child-mobile col-12">
            <input type="hidden" name="arrImageContract[mobile][{numberImageMobile}][image]" value="{link_hidden}">
            <p>{{asset('{link}')}}</p>

            <span class="delete-img-sv" style="display: block;">
                        <a href="javascript:void(0)" onclick="removeImageMobile(this)">
                            <i class="la la-close"></i>
                        </a>
                    </span>
        </div>
    </script>
@stop
