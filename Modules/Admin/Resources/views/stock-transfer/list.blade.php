<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Mã yêu cầu')}}</th>
            <th class="ss--font-size-th">{{__('Nhà đầu tư')}}</th>
            <th class="ss--font-size-th">{{__('Mã hợp đồng')}}</th>
            <th class="ss--font-size-th">{{__('Giá trị hợp đồng')}}</th>
            <th class="ss--font-size-th">{{__('Số lượng chuyển đổi')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>            
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key => $item)               
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                    <td>
{{--                        {{dd($item['stock_tranfer_contract_code'])}}--}}
{{--                        {{$item['stock_tranfer_contract_code']}}--}}
                       <a href="{{route('admin.stock-transfer.detail',['id'=>$item['stock_tranfer_contract_id']])}}">
                           {{$item['stock_tranfer_contract_code']}}
                       </a>
                    </td>
                    <td>{{$item->full_name}}</td>
                    <td>{{$item->customer_contract_code}}</td>
                    <td>{{$item['total_amount']}}</td>
                    <td>
                        {{$item['quantity']}}
                    </td>
                    <td>
                        @if($item['status'] == 'new')
                            Mới
                        @elseif ($item['status'] == 'cancel')
                            Hủy
                        @else
                            Đã duyệt
                        @endif


                    </td>
                    <td>
                        @if(in_array('admin.product.detail', session('routeList')))
                            <a href="{{route('admin.stock-transfer.detail',['id'=>$item['stock_tranfer_contract_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="la la-eye"></i>
                            </a>
                        @endif                                              
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
 {{ $LIST->links('helpers.paging') }}
{{--.--}}