@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ CỔ PHIẾU')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH CỔ PHIẾU')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                {{-- @if(in_array('admin.customer-contract.export', session('routeList')))
                    <a href="{{route('admin.customer-contract.export')}}"
                       class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-2">
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span> {{__('Export HỢP ĐỒNG')}}</span>
                        </span>
                    </a>
                @endif --}}

{{--                <a href="{{route('admin.customer-contract.add')}}"--}}
{{--                   class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">--}}
{{--                    <span>--}}
{{--                        <i class="fa fa-plus-circle"></i>--}}
{{--                        <span> {{__('THÊM HỢP ĐỒNG')}}</span>--}}
{{--                    </span>--}}
{{--                </a>--}}

            </div>
            {{-- Begin: Tab--------------- --}}
            <div class="d-flex ml-auto">
            <div class="m-portlet__head-caption p-3">
                <div class="m-portlet__head-title">
                   
                    <a href="javascript::void(0)" class="m-portlet__head-text">
                        THÔNG TIN
                    </a>

                </div>
            </div>
            <div class="m-portlet__head-caption p-3 color_button">
                <div class="m-portlet__head-title ">
                 
                    <a  href="javascript::void(0)" class="m-portlet__head-text  ">
                        PHÁT HÀNH
                    </a>

                </div>
            </div>
            <div class="m-portlet__head-caption p-3">
                <div class="m-portlet__head-title">
                    <a href="javascript::void(0)" class="m-portlet__head-text">
                      CHI PHÍ
                    </a>

                </div>
            </div>
        </div>
           
           



            {{-- <div class="card-header tab-card-header ">
                {{-- <div class="row">
                    <div class="d-inline p-2 bg-primary text-white">THÔNG TIN</div>
                    <div class="d-inline p-2 bg-dark text-white">PHÁT HÀNH</div>
                    <div class="d-inline p-2 bg-dark text-white">CHI PHÍ</div>
                </div> --}}
              
            {{-- </div> --}} 
            {{-- End: Tab------------------ --}}

        </div>
        <div class="m-portlet__head-tools nt-class">
            <a id="btnPublish" href="javascript:void(0)" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm float-right">
<span>    
    <span>PHÁT HÀNH</span>
</span>
</a>
    </div>
       
        <div class="m-portlet__body">
            {{-- begin:search--------------- --}}
              <!-- seach -->
            {{-- <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%" name="active"
                                class="form-control m-input ss--select-2">
                            <option value="">Chọn trạng thái</option>
                            <option value="0">Đang hoạt động</option>
                            <option value="1">Đã hết hạn</option>
                        </select>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%" name="product_category_id"
                                class="form-control m-input ss--select-2">
                            <option value="">Chọn loại gói</option>
                            <option value="2">Tiết kiệm</option>
                            <option value="1">Trái phiếu</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group" style="background-color: white">
                            <div class="m-input-icon m-input-icon--right">
                                <input ="" class="form-control m-input daterange-picker"
                                       id="created_at" name="created_at" autocomplete="off"
                                       value="">
                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                        <span><i class="la la-calendar"></i></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <button class="btn btn-primary color_button btn-search">
                                {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                            </button>
                            <a href="{{route('admin.customer-contract')}}"
                               class="btn btn-metal  btn-search padding9x padding9px">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>


                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                    </div>
                @endif
            </form> --}}
            {{-- end: search------------------------- --}}
         
            <div class="table-content m--padding-top-30 ">
                <form id = "frm">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Số lượng phát hành:</label>
                                    {{-- {{dd($stock->toArray())}} --}}
                                    <input name="stock_id" type="hidden" value="">
                                    <input disabled name="quantity" class="form-control bg-secondary " value=""  />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold ">Thành tiền:</label>
                                    {{-- {{dd($stock->toArray())}} --}}
                                    <input disabled readonly  id="total_amount"  class="form-control bg-secondary" value=""  />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold ">Ngày phát hành:</label>
                                    {{-- {{dd($stock->toArray())}} --}}
                                    <input disabled name="publish_at" value="" readonly  id="day_publish"  class="form-control bg-secondary"   />
                                    
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Đã bán:</label>                            
                                    <input disabled  name="quantity_sale"  class="form-control bg-secondary " value=""   />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold w-100">Trạng thái:</label>
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input disabled name="is_active" type="checkbox"  checked="" class="manager-btn">
                                            <span></span>
                                        </label>
                                    </span>                         
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Giá phát hành:</label>                          
                                    <input name="money"  class="form-control " value=""  />
                                </div>

                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <span id="btnSave"   class="btn ss--btn-search float-right">
                                Lưu
                                
                            </span>                            
                              
                        </div>
                 </form>
               
                   
                </div>
             
                {{-- <div class="form-group m-form__group row">
                    <label class="black-title">Tài liệu liên quan:</label>
                    
               

                {{-- @include('admin::stock.list') --}}

            {{-- </div><!-- end table-content --> --}} 
            {{-- <div class="form-group m-form__group row d-flex flex-row-reverse">
                
                <a href="{{route('admin.stock.edit',1)}}" class="btn btn-primary color_button btn-search">Chỉnh sửa thông tin</a>
                <button onclick="window.location.href='/'" class="btn  btn-metal mr-4">Hủy <i class="fa fa-arrow-left ic-search m--margin-left-5"></i></button>
               
            </div> --}}

        </div>
    </div>
    @include('admin::stock.popup.popup_publish')
    +
@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
    <script>        
      
    </script>
    {{-- <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script> --}}
    <script src="{{asset('static/backend/js/admin/stock/stock-publish.edit.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
{{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop