<div class="modal fade" id="product_interest_popup_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>{{_('Chi tiết lãi suất')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="product_interest_add_form" autocomplete="off">
                    <div class="row">
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Loại lãi suất')}}</label>
                                </div>
                                <div class="col-9">
                                    <select class="form-control select2 w-100 disabled-false" disabled name="term_time_type">
                                        <option value="">{{_('Chọn loại lãi suất')}}</option>
                                        <option value="1" {{$detailProductInterest['term_time_type'] == 1 ? 'selected' : ''}}>{{_('Có kỳ hạn')}}</option>
                                        <option value="0" {{$detailProductInterest['term_time_type'] == 0 ? 'selected' : ''}}>{{_('Không có kỳ hạn')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Kỳ hạn đầu tư')}} ({{_('tháng')}})</label>
                                </div>
                                <div class="col-9">
                                    <select class="form-control select2 w-100 disabled-false" disabled id="investment_time_id" name="investment_time_id">
                                        <option value="">{{_('Chọn kỳ hạn đầu tư')}}</option>
                                        @foreach($listInvestmentTime as $item)
                                            <option value="{{$item['investment_time_id']}}" {{$detailProductInterest['investment_time_id'] == $item['investment_time_id'] ? 'selected' : ''}}>{{$item['investment_time_month']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Kỳ hạn rút lãi')}} ({{_('tháng')}})</label>
                                </div>
                                <div class="col-9">
                                    <select class="form-control select2 w-100 disabled-false" disabled id="withdraw_interest_time_id" name="withdraw_interest_time_id">
                                        <option value="">{{_('Chọn kỳ hạn rút lãi')}}</option>
                                        @foreach($withdrawInterestTime as $item)
                                            <option value="{{$item['withdraw_interest_time_id']}}" {{$detailProductInterest['withdraw_interest_time_id'] == $item['withdraw_interest_time_id'] ? 'selected' : ''}}>{{$item['withdraw_interest_month']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Lãi suất')}} (%)</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" name="interest_rate" disabled class="form-control number-money disabled-false" placeholder="{{_('Nhập lãi suất')}}" value="{{number_format($detailProductInterest['interest_rate'])}} ">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Tỉ lệ hoa hồng')}} (%)</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" name="commission_rate" disabled class="form-control number-money disabled-false" placeholder="{{_('Nhập tỉ lệ hoa hồng')}}" value="{{number_format($detailProductInterest['commission_rate'])}} ">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Tổng lãi suất hàng tháng')}} (vnđ)</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" name="month_interest" disabled class="form-control number-money disabled-false" placeholder="{{_('Nhập tổng lãi suất hàng tháng')}}" value="{{number_format($detailProductInterest['month_interest'])}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Tổng lãi suất')}} (vnđ)</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" name="total_interest" disabled class="form-control number-money disabled-false" placeholder="{{_('Nhập tổng lãi suất')}}" value="{{number_format($detailProductInterest['total_interest'])}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Hiển thị chính')}}</label>
                                </div>
                                <div class="col-9">
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input type="checkbox" disabled {{$detailProductInterest['is_default_display'] == 1 ? 'checked' : ''}} class="manager-btn is_default_display_add disabled-false">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Trạng thái')}}</label>
                                </div>
                                <div class="col-9">
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input type="checkbox" disabled {{$detailProductInterest['is_actived'] == 1 ? 'checked' : ''}} class="manager-btn is_actived_add disabled-false">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-arrow-left"></i>
                                    <span>{{_('HỦY')}}</span>
                                </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>