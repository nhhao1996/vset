@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ CỔ PHIẾU')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-content">
    <div class="m-portlet" id="autotable-stock-transfer">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('DANH SÁCH YÊU CẦU CHUYỂN ĐỔI HỢP ĐỒNG TIẾT KIỆM - HỢP TÁC ĐẦU TƯ SANG CỔ PHIẾU')}}
                    </h3>
                </div>
            </div>
          
                {{-- Begin: Tab--------------- --}}
                <div class="d-flex ml-auto">              
                    <div  class="m-portlet__head-caption p-3 @if($type=='bond') color_button @endif">
                        <div class="m-portlet__head-title">
    
                            <a id="traiphieu"  href="{{route('admin.stock-transfer.getListBond')}}" class="m-portlet__head-text ">
                                HỢP TÁC ĐẦU TƯ
                            </a>
    
                        </div>
                    </div>
                    <div class="m-portlet__head-caption p-3 @if($type=='saving') color_button @endif">
                        <div class="m-portlet__head-title">
                            <a  href="{{route('admin.stock-transfer.getListSaving')}}" class="m-portlet__head-text ">
                                TIẾT KIỆM
                            </a>
    
                        </div>
                    </div>
                </div>
                {{-- end: Tab --}}
        </div>
        <div class="m-portlet__body">
            <form class="frmFilter ss--background">
                <div class="row ss--bao-filter">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="hidden" name="product_category_id" value="{{$type=='bond'?1:2}}">
                                <input type="text" class="form-control" name="search_keyword"
                                       placeholder="{{__('Nhập tên mã,sđt khách hàng')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="row">
                            @php $i = 0; @endphp
                            @foreach ($FILTER as $name => $item)
                                @if ($i > 0 && ($i % 4 == 0))
                        </div>
                        <div class="form-group m-form__group row align-items-center">
                            @endif
                            @php $i++; @endphp
                            <div class="col-lg-12 form-group input-group">
                                <select name="status" style="width: 100%;" name="process_status" class="form-control m-input ss--select-2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Chọn trạng thái</option>
                                    <option value="new">Mới</option>
                                    <option value="approved">Đã duyệt</option>
                                    <option value="cancel">Hủy</option>
                                </select>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text"
                                   class="form-control m-input daterange-picker" id="created_at"
                                   name="created_at"
                                   autocomplete="off" placeholder="{{__('Chọn ngày phát hành')}}">
                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span></span>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-group m-form__group">
                            <button class="btn btn-primary color_button btn-search">TÌM KIẾM <i class="fa fa-search ic-search m--margin-left-5"></i></button>
                            <a href="{{route('admin.stock-transfer.index')}}"
                               class="btn btn-metal  btn-search padding9x">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-content m--padding-top-30">
                @include('admin::stock-transfer.list')
            </div><!-- end table-content -->
        </div>
    </div>
    </div>
@endsection
@section('after_script')
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>

<script src="{{asset('static/backend/js/admin/stock-transfer/stock_transfer_script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $('#autotable').PioTable({
                baseUrl: laroute.route('admin.stock-transfer.list')
        });
        $('#autotable-stock-transfer').PioTable({
            baseUrl: laroute.route('admin.stock-transfer.listSaving')
        });
        $('#autotable-stock-transfer').PioTable('search');

        $(".m_selectpicker").selectpicker();
    </script>
@stop
