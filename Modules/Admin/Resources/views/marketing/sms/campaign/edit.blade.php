@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-sms.png')}}" alt="" style="height: 20px;">
        {{__('SMS')}}
    </span>
@endsection
@section('content')
    <style>
        .modal-lg {
            max-width: 85%;
        }

        input[type="file"] {
            display: none;
        }
    </style>
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHỈNH SỬA CHIẾN DỊCH')}}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Tên chiến dịch')}}: <b class="text-danger">*</b>
                        </label>

                        <input id="name" type="text" value="{{$campaign->name}}" class="form-control"
                               placeholder="{{__('Nhập tên chiến dịch')}}">
                        <span class="text-danger error-name"></span>

                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Tham số')}}:
                        </label>
                        <div>
                            <button href="javascript:void(0)"
                                    class="btn ss--btn-parameter m--margin-right-10 m--margin-bottom-5 ss--btn-mobiles ss--font-weight-200"
                                    onclick="EditCampaign.valueParameter('customer-name')">
                                    {{__('Tên khách hàng')}}
                            </button>
                            <button href="javascript:void(0)"
                                    class="btn ss--btn-parameter m--margin-right-10 m--margin-bottom-5 ss--btn-mobiles ss--font-weight-200"
                                    onclick="EditCampaign.valueParameter('full-name')">
                                    {{__('Họ & Tên')}}
                            </button>
                            <button href="javascript:void(0)"
                                    class="btn ss--btn-parameter m--margin-right-10 m--margin-bottom-5 ss--btn-mobiles ss--font-weight-200"
                                    onclick="EditCampaign.valueParameter('customer-birthday')">
                                    {{__('Ngày sinh')}}
                            </button>
                            <button href="javascript:void(0)"
                                    class="btn ss--btn-parameter m--margin-bottom-5 ss--btn-mobiles ss--font-weight-200"
                                    onclick="EditCampaign.valueParameter('customer-gender')">
                                    {{__('Giới tính')}}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Nội dung tin nhắn mẫu')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="form-group m-form__group">
                    <textarea onkeyup="EditCampaign.countCharacter(this)" rows="5" cols="40" id="message-content"
                              class="form-control m-input"
                              placeholder="{{__('Nhập tin nhắn mẫu')}}">{{$campaign->content}}</textarea>
                            <i class="pull-right">{{__('Số ký tự')}}: <i class="count-character">0</i>/480</i>
                            <span class="text-danger error-count-character">
                        </span>
                        </div>

                    </div>
                </div>
            </div>
            {{--<div class="form-group m-form__group row">--}}
            {{--<label for="" class="col-lg-2">--}}
            {{--Trạng thái: <b class="text-danger">*</b>--}}
            {{--</label>--}}
            {{--<div class="col-lg-3">--}}
            {{--<input type="text" class="form-control" disabled value="Mới">--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="form-group m-form__group">
                <label>
                    {{__('Thời gian gửi')}}: <b class="text-danger">*</b>
                </label>
                <div class="row">
                    <div class="col-lg-6 form-group">
                        <div class="m-input-icon m-input-icon--right">
                            <input {{$campaign->is_now==1?'disabled':''}}  readonly=""
                                   class="form-control m-input daterange-picker"
                                   id="day-send"
                                   name="day-send" autocomplete="off" placeholder="{{__('Chọn ngày gửi')}}">
                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                     <span><i class="la la-calendar"></i></span></span>
                        </div>
                    </div>
                    <div class="col-lg-6 form-group">
                        <div class="m-input-icon m-input-icon--right">
                            <input {{$campaign->is_now==1?'disabled':''}} readonly=""
                                   class="form-control m-input daterange-picker"
                                   id="time-send"
                                   name="created_at" autocomplete="off" placeholder="{{__('Chọn giờ')}}">
                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                     <span><i class="la la-clock-o"></i></span></span>
                        </div>
                    </div>
                </div>
                <span class="text-danger error-datetime"></span>
            </div>
            {{--<div class="form-group m-form__group row">--}}
                {{--<div class="col-lg-6">--}}
                    {{--<select name="branchOption" class="form-control" id="branchOption">--}}
                        {{--<option value="">{{__('Chọn chi nhánh')}}</option>--}}
                        {{--@foreach($branch as $key=>$value)--}}
                            {{--<option {{$campaign->branch_id==$key?'selected':''}} value="{{$key}}">{{$value}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                    {{--<span class="text-danger error-branch"></span>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group m-form__group">
                <div class="input-group">
                    <label class="m-checkbox m-checkbox--air m-checkbox--solid ss--m-checkbox--state-success">
                        <input {{$campaign->is_now==1?'checked':''}} id="is_now" type="checkbox">
                        {{__('Gửi ngay')}}
                        <span></span>
                    </label>
                </div>

            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-lg-6">
                        {{--@if(in_array('admin.campaign.submit-edit',session('routeList')))--}}
                            <button onclick="EditCampaign.saveChange()" type="button"
                                    class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn color_button m-btn--wide m--margin-right-10 m--margin-bottom-5">
                                <i class="la la-check"></i>
                                {{__('LƯU THÔNG TIN')}}
                            </button>
                            <button type="button"
                                    class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn--wide m--margin-bottom-5">
                                <i class="fa fa-plus-circle"></i>
                                <span class="m--margin-left-5">{{__('GỬI TIN NHẮN')}}</span>
                            </button>
                        {{--@endif--}}
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <div class="pull-left m--margin-right-5  ss--width--100 ss--text-btn-mobi">
                        <br>
                        <span class="m--margin-top-5 ss--font-weight-500 ss--font-size-13">{{__('DANH SÁCH KHÁCH HÀNG NHẬN TIN NHẮN')}}</span>
                    </div>
                    <div class="pull-right m--margin-right-5  ss--width--100 ss--text-btn-mobi">
{{--                        @if(in_array('admin.campaign.submit-edit',session('routeList')))--}}
                            <a data-toggle="modal" href="javascript:void(0)"
                               data-target="#add-customer" onclick="EditCampaign.emptyListCustomer()"
                               class="ss--padding-left-padding-right-1rem m-btn--wide ss--btn-mobiles btn btn-outline-successsss m-btn m-btn--icon m--margin-bottom-5">
                                <i class="fa fa-plus-circle m--margin-right-5"></i>
                                {{__('Thêm khách hàng')}}
                            </a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalChooseFile"
                               class="ss--padding-left-padding-right-1rem m-btn--wide ss--btn-mobiles btn btn-outline-successsss m-btn m-btn--icon m--margin-left-10 m--margin-bottom-5">
                                <i class="la la-files-o m--margin-right-5"></i>
                                {{__('Chọn file')}}
                            </a>
                        {{--@endif--}}
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-8"></div>
                <div class="col-lg-4">
                    <span class="text-danger pull-right error-customer-"></span>
                </div>

            </div>
            <div class="form-group m-form__group m--margin-bottom-10">
                <div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true"
                     style="height: 300px; overflow: hidden;">
                    <table class="table table-striped m-table ss--header-table">
                        <thead class="bg">
                        <tr class="ss--font-size-th ss--nowrap">
                            <th>#</th>
                            <th class="ss--max-width-200">{{__('TÊN KHÁCH HÀNG')}}</th>
                            <th class="ss--text-center">{{__('SỐ ĐIỆN THOẠI')}}</th>
                            <th>{{__('NỘI DUNG TIN NHẮN')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody class="table-list-customer table_list_body" style="font-size: 12px">
                        @foreach($listLog as $key=>$value)
                            <tr>
                                <td class="stt">{{$key+1}}</td>
                                <td>{{$value['customer_name']}}</td>
                                <td class="ss--text-center">{{$value['phone']}}</td>
                                <td>{{$value['message']}}</td>
                                <td>
                                    <button onclick="EditCampaign.removeCustomerLog(this,'{{ $value['id'] }}')"
                                            class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                            title="Delete">
                                        <i class="la la-trash"></i>
                                    </button>
                                </td>
                                {{--<td class="ss--display-none">--}}
                                {{--<input type="hidden" value="{{ $value['id'] }}" class="id-log">--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m-form__actions--solid m--align-right">
                    <a href="{{route('admin.sms.sms-campaign')}}"
                       class="ss--btn-mobiles m--margin-bottom-5 btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10 ss--btn">
						<span class="ss--text-btn-mobi">
						<i class="la la-arrow-left"></i>
						<span>{{__('HỦY')}}</span>
						</span>
                    </a>
                    <button onclick="EditCampaign.saveLog()"
                            class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md">
                         	<span class="ss--text-btn-mobi">
							<i class="la la-check"></i>
                                <span>{{__('LƯU LẠI')}}</span>
							</span>
                    </button>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="add-customer" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <!-- Modal content-->
            @include('admin::marketing.sms.campaign.add-customer')
        </div>
    </div>
    <!--end::Portlet-->
    <input type="hidden" id="hidden-daySent" value="{{$daySent}}">
    <input type="hidden" id="hidden-timeSent" value="{{$timeSent}}">
    <input type="hidden" id="id" value="{{$id}}">


    <div class="modal fade" id="modalChooseFile" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title ss--title m--font-bold">
                        <i class="la la-files-o ss--icon-title m--margin-right-5"></i>
                        {{__('THÊM FILE')}}
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group row">
                            {{--<label for="file-upload" class="ss--custom-file-upload">--}}
                            {{--<i class="fa fa-cloud-upload"></i> Custom Upload--}}
                            {{--</label>--}}
                            {{--<input onchange="EditCampaign.chooseFile()" type="file" id="file_excel"--}}
                            {{--value="CHỌN TỆP">--}}
                            {{--<span class="department-name"></span>--}}
                            <div class="col-lg-8">
                                <input style="font-size: 0.8rem !important; margin-top: 0px !important;"
                                       class="file-name-excels ss--text-black m--margin-top-10 form-control" value=""
                                       readonly>
                            </div>
                            <div class="col-lg-4">
                                <label for="file_excel" class="ss--btn ss--custom-file-upload ss--font-size-13">
                                    <i class="fa fa-cloud-upload"></i> {{__('CHỌN TỆP')}}
                                </label>
                                <label class="error-file-name-excels text-danger"></label>
                                <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                       id="file_excel" onchange="EditCampaign.showNameFile()" type="file">
                            </div>
                        </div>
                        <div class="form-group">
                            <a class="m--font-boldest ss--color-default"
                               href="{{ route('admin.campaign.export.file',['type'=>'xls']) }}">
                                {{__('Tải file mẫu')}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                        <div class="m-form__actions m--align-right">
                            <button data-dismiss="modal"
                                    class="ss--btn-mobiles m--margin-bottom-5 btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
                            </button>
                            <button onclick="EditCampaign.chooseFile()"
                                    class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m--margin-bottom-5 m--margin-left-10 m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
						<span class="ss--text-btn-mobi">
						<i class="la la-check"></i>
						<span>{{__('THÊM FILE')}}</span>
						</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/marketing/sms/campaign/edit.js?v='.time())}}"
            type="text/javascript"></script>
    <script type="text/template" id="customer-list-tpl">
        <tr>
            <td class="stt2">{stt}</td>
            <td>
                {name}
                <input type="hidden" name="customer_id" value="{customer_id}">
                <input type="hidden" name="customer_id" value="{name}">
            </td>
            <td class="ss--text-center">
                {phone}
                <input type="hidden" name="customer_id" value="{phone}">
            </td>
            <td class="ss--text-center">
                {birthday}
                <input type="hidden" name="hiddenBirthday" value="{birthday}">
            </td>
            <td class="ss--text-center">{gender}
                <input type="hidden" name="hiddenGender" value="{gender}">
            </td>
            {{--<td>{branchName}</td>--}}
            <td>
                <label class="m-checkbox m-checkbox--air">
                    <input class="check" checked name="check" type="checkbox">
                    <span></span>
                </label>
            </td>
        </tr>
    </script>
    <script type="text/template" id="customer-list-append">
        <tr>
            <td class="stt">{stt}</td>
            <td>
                {name}
                <input type="hidden" name="customer_id" value="{customer_id}">
                <input type="hidden" name="customer_id" value="{name}">
            </td>
            <td class="ss--text-center">
                {phone}
                <input type="hidden" name="customer_id" value="{phone}">
            </td>
            <td class="contentmessage">
                {content}
                <input type="hidden" name="hiddenBirthday" value="{content}">
            </td>
            <td>
                <button onclick="EditCampaign.removeCustomer(this)"
                        class="aaaa m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                        title="Delete">
                    <i class="la la-trash"></i>
                </button>
            </td>
        </tr>
    </script>
@endsection