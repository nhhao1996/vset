<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th>#</th>
            <th>{{__('Mã hợp đồng ')}}</th>
            <th>{{__('Họ và tên nhà đầu tư')}}</th>
            <th>{{__('Số điện thoại nhà đầu tư')}}</th>
            <th>{{__('Địa chỉ liên hệ')}}</th>
            <th data-toggle="tooltip" title="Số lượng hợp đồng">{{__('Số lượng cổ phiếu hiện tại')}}</th>
            <th>{{__('Ngày tạo hợp đồng')}}</th>

            <th>{{__('Hành động ')}}</th>
{{--            <th class="hidden-th"></th>--}}
        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                @php($num = rand(0,7))
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td>
{{--                        @if(in_array('admin.stock-contract.detail', session('routeList')))--}}
                            <a href="{{route('admin.stock-contract.detail',$item->stock_contract_id)}}">
                                {{$item['stock_contract_code']}}
                            </a>
{{--                        @else--}}
{{--                            {{$item['stock_contract_code']}}--}}
{{--                        @endif--}}
                    </td>
                    <td>
                        {{$item['customer_name']}}

                    </td>
                    <td>{{$item['customer_phone']}}</td>
                    <td>{{$item['contact_address']}}</td>
                    <td class="text-center" data-toggle="tooltip"
                        title="Số lượng hợp đồng">{{ $item['quantity'] == null ? 'N/A' : number_format($item['quantity'],0,'.',',') }}</td>
                    <td>{{\Carbon\Carbon::parse($item->created_at)->format("d/m/Y")}}</td>

{{--                    <td class="text-center">--}}
{{--                        @if(in_array('admin.stock--contract.detail', session('routeList')))--}}
{{--                            <a href="{{route('admin.stock-contract.detail',$item['stock_contract_id'])}}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chi tiết">--}}
{{--                                <i class="flaticon-eye"></i>--}}
{{--                            </a>--}}
{{--                        @endif--}}
{{--                    </td>--}}
                    <td class="text-center tdEndContract">
{{--                        @if(in_array('admin.stock-contract.detail', session('routeList')))--}}
                        <a href="{{route('admin.stock-contract.detail',$item->stock_contract_id)}}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chi tiết">
                            <i class="flaticon-eye"></i>
                        </a>
{{--                        @endif--}}
{{--                        @if(in_array('admin.stock-contract.edit', session('routeList')))--}}
                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               href="{{route("admin.stock-contract.edit",$item['stock_contract_id'])}}" title="Chỉnh sửa">
                                <i class="la la-edit"></i>
                            </a>
{{--                        @endif--}}
{{--                        @if(\Carbon\Carbon::now() > \Carbon\Carbon::parse($item['customer_contract_end_date']) && $item['is_active'] == 0--}}
{{--                             && $item['is_deleted'] =='0')--}}
{{--                            <button data-id="{{$item['customer_contract_id']}}" onclick="CustomerContractHandler.onBtnEndContract({{$item['customer_contract_id']}})"--}}
{{--                                    class="btnEndContract btn btn-primary color_button ">--}}
{{--                                @lang('Kết thúc hợp đồng')--}}
{{--                            </button>--}}
{{--                        @endif--}}
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}


