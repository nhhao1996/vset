<form id="list-file-appendix">
    {{ csrf_field() }}
    <div class="form-group noselector cash">
        <div class="form-group m-form__group pt-3">
            <label>{{__('')}}:</label>
            <div class="m-dropzone dropzone dz-clickable"
                 action="{{route('admin.stock-contract.upload')}}" id="dropzoneonecash">
                <div class="m-dropzone__msg dz-message needsclick">
                    <h3 href="" class="m-dropzone__msg-title">
                        {{__('File')}}
                    </h3>
                    <span>{{__('Chọn file hợp đồng')}}</span>
                </div>
            </div>
        </div>
        <div class="form-group m-form__group row" id="upload-image-cash"></div>
    </div>
    <input type="hidden" id="appendix_contract_id" name="appendix_contract_id" value="">
</form>