@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ HỢP ĐỒNG')}}
    </span>
@endsection
@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-eye"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT HỢP ĐỒNG CỔ PHIẾU')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group">
                <h4 class="m--font-info">Thông tin cá nhân:</h4>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            Họ và tên:
                        </label>
                        <div class="input-group">
                            <input id="customer_name" name="customer_name" type="text" class="form-control m-input class font-weight-bold" placeholder=" Nhập họ và tên " disabled="" value="{{$item->customer_name}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Số điện thoại :
                        </label>
                        <div class="input-group">
                            <input id="phone" name="phone" type="text" class="form-control m-input class font-weight-bold" placeholder="Số điện thoại " disabled="" value="{{$item->phone}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Email:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text" class="form-control m-input class font-weight-bold"  disabled="" value="{{$item->email}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>Ngày sinh:
                        </label>
                        <div class="input-group">
                                                            <input id="birthday" name="birthday" type="text" class="form-control m-input class" placeholder=" Ngày sinh " disabled="" value="{{\Carbon\Carbon::parse($customer->birthday)->format("d/m/Y")}}">
                                                    </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Địa chỉ liên hệ: <b class="text-danger"></b>
                        </label>
                        <div class="input-group">

                            <textarea id="contact_address" name="contact_address" type="text" class="form-control m-input class" placeholder="Địa chỉ liên hệ" disabled="">{{$customer->contact_address}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            CMND:
                        </label>
                        <div class="input-group">
                            <input id="customer_ic_no" name="customer_ic_no" type="text" class="form-control m-input class font-weight-bold" placeholder="CMND" disabled="" value="{{$customer->ic_no}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                             Ngày cấp CMND:
                        </label>
                        <div class="input-group">
                            <input id="birthday" name="birthday" type="text" class="form-control m-input class" placeholder="Ngày cấp CMND" disabled="" value="{{\Carbon\Carbon::parse($customer->ic_date)->format("d/m/Y")}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Nơi cấp CMND:
                        </label>
                        <div class="input-group">
                            <input id="description-en" name="description_en" type="text" class="form-control m-input class" disabled="" placeholder="Nơi cấp CMND" value="{{$customer->ic_place}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Địa chỉ thường trú: <b class="text-danger"></b>
                        </label>
                        <div class="input-group">
                                                            <input id="unit-id" name="unit_id" type="text" class="form-control m-input class" placeholder="Địa chỉ thường trú" disabled="" value="{{$customer->residence_address}}">
                                                    </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Số tài khoản:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text" class="form-control m-input class" placeholder="Số tài khoản" disabled="" value="{{$customer->bank_account_no}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <h4 class="m--font-info">Thông tin hợp đồng:</h4>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                                        <div class="form-group m-form__group">
                        <label>
                            Mã hợp đồng:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text" class="form-control m-input class font-weight-bold" placeholder="Mã hợp đồng" disabled="" value="{{$item->stock_contract_code}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Tên cổ phiếu:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text" class="form-control m-input class font-weight-bold" placeholder="Tên gói" disabled="" value="{{$item['stock_config_code']}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Số lượng:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text" class="form-control m-input class" placeholder="Số lượng" disabled="" value="{{number_format($item->quantity,0,'.',',')}}">
                        </div>
                    </div>



                </div>
                <div class="col-lg-6">

                    <div class="form-group m-form__group">
                        <label>
                            Ngày tạo hợp đồng:
                        </label>
                        <div class="input-group">
                            <input id="created_at" name="created_at" type="text" class="form-control m-input class" placeholder="Ngày phát hành" disabled="" value="{{\Carbon\Carbon::parse($item->created_at)->format('d/m/Y')}}">
                        </div>
                    </div>

{{--                    <div class="form-group m-form__group">--}}
{{--                        <label>--}}
{{--                            Trạng thái:--}}
{{--                        </label>--}}
{{--                        <div class="input-group">--}}
{{--                            <span class="text-success">--}}
{{--                                {{$item->is_active==1?'Đang hoạt động':'Hết hạn'}}--}}

{{--                            </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="tab-content">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item border-right-0">
                            <a class="nav-link active show" data-toggle="tab" show="" id="file_contract">FILE HỢP ĐỒNG GỐC</a>
                        </li>
                        <li class="nav-item border-right-0 li_interest_month">
                            <a class="nav-link" data-toggle="tab" id="interest_month">FILE HỢP ĐỒNG PHỤ</a>
                        </li>
{{--                        <li class="nav-item border-right-0">--}}
{{--                            <a class="nav-link" data-toggle="tab" id="interest_date">LÃI SUẤT NGÀY</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item border-right-0">--}}
{{--                            <a class="nav-link" data-toggle="tab" id="interest_time">LÃI SUẤT THỜI ĐIỂM</a>--}}
{{--                        </li>--}}
                        
                        
                        
                        
{{--                        <li class="nav-item border-right-0">--}}
{{--                            <a class="nav-link" data-toggle="tab" id="withdraw">YÊU CẦU RÚT GỐC</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item border-right-0">--}}
{{--                            <a class="nav-link" data-toggle="tab" id="serial_contract">SỐ SERIAL</a>--}}
{{--                        </li>--}}
                    </ul>
                    <div>
                        <div id="div_file_contract" style="display: block">
                            <div id="autotable-file">
                                <form class="frmFilter">
                                    <input type="hidden" id="stock_contract_id" name="stock_contract_id" value="{{$item->stock_contract_id}}">
                                </form>
                                <div class="table-content">
<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">File hợp đồng</th>
        <th class="tr_thead_list_dt_cus">Thứ tự</th>
        <th class="tr_thead_list_dt_cus">Tải xuống</th>
    </tr>
    </thead>
    <tbody style="font-size: 13px">
                    </tbody>
</table>
<div class="m-datatable m-datatable--default">
    <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
    	        <div class="m-datatable__pager-info" style="float: left">
            
                    
                    

            
            <span class="m-datatable__pager-detail">Hiển thị  -  của 0</span>
        </div>
    </div>
</div>


</div>
                            </div>
                        </div>
                        <div id="div_interest_month" style="display: none">
                            <div id="autotable-sub-file">
                                <form class="frmFilter">
                                    <input type="hidden" id="stock_contract_id" name="stock_contract_id" value="{{$item->stock_contract_id}}">
                                </form>
                                <div class="table-content"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer save-attribute m--margin-right-20">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.stock-contract.index')}}"
                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/stock-contract/detail.js?v='.time())}}"></script>
    @if(isset($tab) && $tab == 'month')
        <script>
            $(document).ready(function () {
                $("#interest_month").trigger("click");
            });
        </script>
    @endif
@stop
