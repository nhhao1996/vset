<div class="table-responsive">
<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">@lang('File hợp đồng')</th>

        <th class="tr_thead_list_dt_cus">@lang('Tải xuống')</th>
    </tr>
    </thead>
    <tbody style="font-size: 13px">
    @if(isset($listSubFile))
        @foreach ($listSubFile as $k => $v)
            <tr>
                <td>
                    <a href="{{asset($v['file'])}}" target="_blank">



                        <p>{{asset($v['file'])}}</p>
                    </a>
                </td>

                <td class="nt-active-btn-download">
                  <p>  <a download href="{{asset($v['file'])}}" target="_blank" class="btn btn-light-success btn-sm mr-3">
                          <i class="fa fa-download"></i> Tải xuống</a></p>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
</div>
{{ $listSubFile->links('helpers.paging') }}

