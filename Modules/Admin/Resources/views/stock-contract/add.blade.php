@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ HỢP ĐỒNG')}}
    </span>
@endsection
@section('content')
    <form id="form-product" autocomplete="off">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                        <h3 class="m-portlet__head-text">
                            {{__('THÊM HỢP ĐỒNG')}}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <div
                            class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--open btn-hover-add-new"
                            m-dropdown-toggle="hover" aria-expanded="true">
                        <div class="m-dropdown__wrapper dropdow-add-new" style="z-index: 101;display: none">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                  style="left: auto; right: 21.5px;"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__item">
                                                <a data-toggle="modal"
                                                   data-target="#modal-add-product-category" href="" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-users"></i>
                                                    <span class="m-nav__link-text">{{__('Thêm danh mục sản phẩm')}} </span>
                                                </a>
                                                <a data-toggle="modal"
                                                   data-target="#modal-add-unit" href="" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-users"></i>
                                                    <span class="m-nav__link-text">{{__('Thêm đơn vị tính')}} </span>
                                                </a>
                                                <a data-toggle="modal"
                                                   data-target="#modal-add-product-model" href="" class="m-nav__link">
                                                    <i class="m-nav__link-icon la la-users"></i>
                                                    <span class="m-nav__link-text">{{__('Thêm nhãn hiệu')}} </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">

                    @include("admin::customer-contract.order-list")

                </div>
            </div>
            <div class="modal-footer save-attribute m--margin-right-20">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.product')}}"
                           class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                        </a>
                        <button onclick="product.addProduct(1)" type="button"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                            <i class="la la-check"></i>
                            <span>{{__('LƯU THÔNG TIN')}}</span>
                            </span>
                        </button>
                        <button type="button" onclick="product.addProduct(0)"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                             <i class="fa fa-plus-circle"></i>
                            <span> {{__('LƯU & TẠO MỚI')}}</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('after_script')
    {{--    <script src="{{asset('static/backend/js/admin/product/jquery.masknumber.js')}}"--}}
    {{--type="text/javascript"></script>--}}
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/product/script.js?v='.time())}}"></script>

    <script>
        var Summernote = {
            init: function () {
                $.getJSON(laroute.route('translate'), function (json) {
                    $(".summernote").summernote({
                        height: 208,
                        placeholder: json['Nhập nội dung'],
                        toolbar: [
                            ['style', ['bold', 'italic', 'underline']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            // ['insert', ['link', 'picture']]
                        ]
                    })
                });
            }
        };
        jQuery(document).ready(function () {
            Summernote.init()
            $('.note-btn').attr('title', '');
        });
    </script>
@stop
