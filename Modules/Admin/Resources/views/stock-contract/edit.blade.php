@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ HỢP ĐỒNG')}}
    </span>
@endsection
@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHỈNH SỬA HỢP ĐỒNG CỔ PHIẾU')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body">
            <form id="frmStockContract">
                <input type="hidden" name="stock_contract_id" value="{{$item->stock_contract_id}}">
            <div class="form-group m-form__group">
                <h4 class="m--font-info">Thông tin cá nhân:</h4>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            Họ và tên:
                        </label>
                        <div class="input-group">
                            <input id="customer_name" name="customer_name" type="text" disabled class="form-control m-input class font-weight-bold" placeholder=" Nhập họ và tên "  value="{{$item->customer_name}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Số điện thoại :
                        </label>
                        <div class="input-group">
                            <input id="customer_phone" name="customer_phone" type="text" disabled class="form-control m-input class font-weight-bold" placeholder="Số điện thoại "  value="{{$item->phone}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Email:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text" disabled class="form-control m-input class font-weight-bold"   value="{{$item->customer_email}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>Ngày sinh:
                        </label>
                        <div class="input-group">
                                                            <input id="birthday" name="birthday" type="text" disabled class="form-control m-input class" placeholder=" Ngày sinh "  value="{{\Carbon\Carbon::parse($customer->birthday)->format("d/m/Y")}}">
                                                    </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Địa chỉ liên hệ: <b class="text-danger"></b>
                        </label>
                        <div class="input-group">

                            <textarea id="price-standard" name="price_standard" type="text" disabled class="form-control m-input class" placeholder="Địa chỉ liên hệ" >{{$customer->contact_address}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            CMND:
                        </label>
                        <div class="input-group">
                            <input id="customer_ic_no" name="customer_ic_no" type="text" disabled class="form-control m-input class font-weight-bold" placeholder="CMND"  value="{{$customer->ic_no}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                             Ngày cấp CMND:
                        </label>
                        <div class="input-group">
                            <input id="birthday" name="birthday" type="text" disabled class="form-control m-input class" placeholder="Ngày cấp CMND"  value="{{\Carbon\Carbon::parse($customer->ic_date)->format("d/m/Y")}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Nơi cấp CMND:
                        </label>
                        <div class="input-group">
                            <input id="description-en" name="description_en" type="text" disabled class="form-control m-input class"  placeholder="Nơi cấp CMND" value="{{$customer->ic_place}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Địa chỉ thường trú: <b class="text-danger"></b>
                        </label>
                        <div class="input-group">
                                                            <input id="unit-id" name="unit_id" type="text" disabled class="form-control m-input class" placeholder="Địa chỉ thường trú"  value="{{$customer->residence_address}}">
                                                    </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Số tài khoản:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text" disabled class="form-control m-input class" placeholder="Số tài khoản"  value="{{$customer->bank_account_no}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <h4 class="m--font-info">Thông tin hợp đồng:</h4>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                                        <div class="form-group m-form__group">
                        <label>
                            Mã hợp đồng:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text" disabled class="form-control m-input class font-weight-bold" placeholder="Mã hợp đồng"  value="{{$item->stock_contract_code}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Tên cổ phiếu:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text" disabled class="form-control m-input class font-weight-bold"   value="{{$item['stock_config_code']}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            Số lượng:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text" disabled class="form-control m-input class" placeholder="Số lượng"  value="{{number_format($item->quantity,0,'.',',')}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            File hợp đồng:
                        </label>
                        @include('admin::stock-contract.include.upload')

                    </div>




                </div>
                <div class="col-lg-6">

                    <div class="form-group m-form__group">
                        <label>
                            Ngày phát hành:
                        </label>
                        <div class="input-group">
                                                            <input id="birthday" name="birthday" type="text" disabled class="form-control m-input class" placeholder="Ngày phát hành"  value="17:12 15/04/2021">
                                                    </div>
                    </div>

                    <div class="form-group m-form__group">
                        <label>
                            Trạng thái:
                        </label>
                        <div class="input-group">
                            <span class="text-success">
                                {{$item->is_active==1?'Đang hoạt động':'Hết hạn'}}

                            </span>
                        </div>
                    </div>

                </div>
            </div>

        </form>
            <div class="form-group m-form__group row">
                <div class="col-12" id="upload-image-cash"></div>
            </div>
            <div class="m-portlet__foot">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.customer-contract')}}"
                           class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('HUỶ')}}</span>
						</span>
                        </a>
                        <button type="button" onclick="StockContractHandler.editStockContract({{$item['stock_contract_id']}})"
                                class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit m--margin-left-10">
							<span>
							<i class="la la-edit"></i>
							<span>{{__('CẬP NHẬT')}}</span>
							</span>
                        </button>
                    </div>
                </div>
            </div>


            {{--   
        </div>
{{--        <div class="modal-footer save-attribute m--margin-right-20">--}}
{{--            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">--}}
{{--                <div class="m-form__actions m--align-right">--}}
{{--                    <a href="{{route('admin.stock-contract.index')}}"--}}
{{--                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">--}}
{{--                        <span class="ss--text-btn-mobi">--}}
{{--                        <i class="la la-arrow-left"></i>--}}
{{--                        <span>{{__('HỦY')}}</span>--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}


{{--        </div>--}}
    </div>

@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/stock-contract/edit.js?v='.time())}}"></script>
    <script type="text/template" id="imageFile">
        <div class="list-file mb-0 col-12 position-relative">
            <input type="hidden" name="arrFile[]" value="{link_hidden}">
            <p>{link}</p>
            <span class="delete-img-sv mt-3 position-absolute" style="display: block;left: 109%;top:-36%;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
        </div>
    </script>
    <script>
        function removeImage(e){
            $(e).closest('.list-file').remove();
        }

        function dropzone() {
            Dropzone.options.dropzoneonecash = {
                paramName: 'file',
                maxFilesize: 10, // MB
                maxFiles: 20,
                // acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dictRemoveFile: 'Xóa',
                dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
                dictInvalidFileType: 'Tệp không hợp lệ',
                dictCancelUpload: 'Hủy',
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                    var random = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    for (let z = 0; z < 10; z++) {
                        random += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
                },
                init: function () {
                    this.on("success", function (file, response) {
                        var a = document.createElement('span');
                        a.className = "thumb-url btn btn-primary";
                        a.setAttribute('data-clipboard-text', laroute.route('admin.customer-change-request.uploadAppendixContract'));

                        if (file.status === "success") {
                            //Xóa image trong dropzone
                            $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                                file.previewElement.remove();
                            });
                            $('#dropzoneonecash').removeClass('dz-started');
                            //Append vào div image
                            let tpl = $('#imageFile').html();
                            tpl = tpl.replace(/{link}/g, response.file);
                            tpl = tpl.replace(/{link_hidden}/g, response.file);
                            $('#upload-image-cash').append(tpl);
                        }
                    });

                }
            }
        }
        dropzone();
    </script>
    @if(isset($tab) && $tab == 'month')
        <script>
            $(document).ready(function () {
                $("#interest_month").trigger("click");
            });
        </script>
    @endif
@stop
