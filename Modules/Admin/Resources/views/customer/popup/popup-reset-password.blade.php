<div class="form-group row">
{{--    <label class="col-xl-4 col-lg-4 font-weight-bold col-form-label">{{__('Tài khoản')}}</label>--}}
{{--    <div class="col-lg-8 col-xl-8">--}}
{{--        <input type="text" class="form-control" id="user_account" name="user_account"--}}
{{--               hidden value="{{$item['email']}}">--}}
{{--        <label class="col-form-label">{{$item['email']}}</label>--}}
{{--    </div>--}}
</div>
<div class="form-group row">
    <label class="col-xl-4 col-lg-4 font-weight-bold col-form-label">{{__('Mật khẩu tự sinh')}}</label>
    <div class="col-lg-8 col-xl-8">
        <div class="col-lg-12 col-xl-12 col-form-label row">
                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                   <label class="ss--switch">
                        <input type="checkbox"  class="manager-btn"
                               onchange="customer.autoGeneratePassword(this)">
                        <span></span>
                    </label>
                </span>
        </div>

        <div class="append-password">
            <div class="m-input-icon m-input-icon--right">
                <input type="password" class="form-control" id="password2" name="password2"
                       placeholder="{{__('Nhập mật khẩu')}}">
                <span></span>
                <a href="javascript:void(0)" onclick="customer.show_password('#password2')"
                   class="m-input-icon__icon m-input-icon__icon--right">
                <span class="m-input-icon__icon m-input-icon__icon--right">
                    <span><i class="la la-eye icon-padding"></i></span>
                 </span>
                </a>
            </div>
        </div>

    </div>
</div>
