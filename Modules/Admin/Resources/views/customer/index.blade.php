@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ NHÀ ĐẦU TƯ')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/
        .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > span , .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > input:checked ~ span {
            background: #4fc4ca;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <form id="frmFilter">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                             <i class="la la-th-list"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{__('DANH SÁCH NHÀ ĐẦU TƯ')}}
                        </h2>

                    </div>
                </div>

                <div class="m-portlet__head-tools nt-class">

                </div>
                  {{-- Begin: Tab--------------- --}}
            <div class="d-flex ml-auto">
                <div class="m-portlet__head-caption p-3 @if(!$isTop100) color_button @endif">
                    <div class="m-portlet__head-title">

                        <a @if(!$isTop100)style="color:#fff !important"@endif href="{{route('admin.customer')}}" class="m-portlet__head-text @if(!$isTop100) color_button @endif">
                            TẤT CẢ
                        </a>

                    </div>
                </div>
                <div class="m-portlet__head-caption p-3 @if($isTop100) color_button @endif">
                    <div class="m-portlet__head-title">

                        <a @if($isTop100)style="color:#fff !important"@endif href="{{route('admin.customer-top100')}}" class="m-portlet__head-text ">
                            TOP 100
                        </a>

                    </div>
                </div>

            </div>
            {{-- end: Tab --}}

            </div>
            <div class="m-portlet__body">
                <div class="frmFilter bg">
                    <div class="row padding_row">
                        <div class="col-lg-4">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    @if($isTop100)
                                    <input type="text" class="form-control" name="search"
                                        value="{{ isset($FILTER['search']) ? $FILTER['search'] : '' }}"
                                        placeholder="{{ __('Nhập tên, số điện thoại hoặc email') }}">
                                        <input type="hidden" class="form-control" name="is_top"
                                        value="is_top"
                                    >
                                    @else
                                        <input type="text" class="form-control" name="search" value="{{isset($FILTER['search']) ? $FILTER['search'] : ''}}"
                                               placeholder="{{__('Nhập tên, số điện thoại, email, hoặc hành trình khách hàng')}}">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <select class="form-control" name="type_customer">
                                        <option value="">{{ __('Chọn loại khách hàng') }}</option>
                                        <option value="1"
                                            {{ isset($FILTER['type_customer']) && $FILTER['type_customer'] == 1 ? 'selected' : '' }}>
                                            {{ __('Nhà đầu tư') }}</option>
                                        <option value="0"
                                            {{ isset($FILTER['type_customer']) && $FILTER['type_customer'] == 0 ? 'selected' : '' }}>
                                            {{ __('Tiềm năng') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="pt-2">
                                <div class="kt-checkbox-list">
                                    <label
                                        class="m-checkbox m-checkbox--air m-checkbox--solid ss--m-checkbox--state-success mr-3">
                                        <input type="checkbox" name="is_broker"
                                            {{ isset($FILTER['is_broker']) ? 'checked' : '' }}> Môi giới
                                        <span></span>
                                    </label>
                                    <label
                                        class="m-checkbox m-checkbox--air m-checkbox--solid ss--m-checkbox--state-success ">
                                        <input type="checkbox" name="is_test"
                                            {{ isset($FILTER['is_test']) ? 'checked' : '' }}> Tài khoản test
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
{{--                        <div class="col-lg-4"></div>--}}
                        <div class="col-lg-4">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control created"
                                        value="{{ isset($FILTER['created']) ? $FILTER['created'] : '' }}" name="created"
                                        placeholder="Chọn ngày tạo">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group m-form__group">
                                <div class="input-group">
{{--                                    <select class="form-control" name="customer_source_id">--}}
                                    <select class="form-control" name="utm_source">
                                        <option value="">{{__('Chọn nguồn khách hàng')}}</option>
{{--                                        @foreach($listCustomerSource as $item)--}}
{{--                                            <option value="{{$item['customer_source_id']}}" {{isset($FILTER['customer_source_id']) && $FILTER['customer_source_id'] == $item['customer_source_id'] ? 'selected' :''}}>{{$item['customer_source_name']}}</option>--}}
{{--                                        @endforeach--}}
                                        <option value="na" {{isset($FILTER['utm_source']) && $FILTER['utm_source'] == 'na' ? 'selected' :''}}>{{__('N/A')}}</option>
                                        <option value="other" {{isset($FILTER['utm_source']) && $FILTER['utm_source'] == 'other' ? 'selected' :''}}>{{__('Khác')}}</option>
                                        @foreach($listCustomerSource as $item)
                                            <option value="{{$item['source']}}" {{isset($FILTER['utm_source']) && $FILTER['utm_source'] == $item['source'] ? 'selected' :''}}>{{$item['source_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <select class="form-control" name="is_active">
                                        <option value="">{{__('Chọn trạng thái')}}</option>
                                        <option value="1" {{isset($FILTER['is_active']) && $FILTER['is_active'] == 1 ? 'selected' : ''}}>{{__('Hoạt động')}}</option>
                                        <option value="0" {{isset($FILTER['is_active']) && $FILTER['is_active'] == 0 ? 'selected' : ''}}>{{__('Ngưng hoạt động')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group m-form__group">
                                <button type="submit" class="btn btn-primary color_button btn-search " style="width: 150px">
                                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                </button>
                                <a href="{{isset($FILTER['is_top'] )? route('admin.customer-top100') : route('admin.customer')}}"
                                   class="btn btn-metal  btn-search padding9x padding9px">
                                    <span><i class="flaticon-refresh"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                            <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                        </div>
                    @endif
                </div>

                <div class="table-content m--padding-top-30">
                    @if(in_array('admin.customer.export',session('routeList')))
                        <a style="margin-bottom:30px"  href="{{route('admin.customer.export',$FILTER)}}"
                           class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm float-right">
                                <span>
                                    <i class="fa fa-file-export m--margin-right-5"></i>
                                    <span> {{__('Export')}}</span>
                                </span>
                        </a>
                    @endif

                    @include('admin::customer.list')

                </div><!-- end table-content -->

            </div>
            <input type="hidden" id="page" name="page" value="1" >
            <input type="hidden" id="stock_order_by" name="stock_order_by" value="{{isset($FILTER['stock_order_by']) ? $FILTER['stock_order_by'] : ''}}">
{{--            @dd($FILTER);--}}
        </form>
    </div>

    @include('admin::customer.modal.modal-reset-password')
    @include('admin::customer.modal.modal-reset-password-success')
@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@stop
@section('after_script')


    <script src="{{asset('static/backend/js/admin/customer/script.js?v='.time())}}" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/customer/import-excel.js?v='.time())}}"
            type="text/javascript"></script>
    <script>

        function changeValue(){
            $('[name="stock_order_by"]').val() == "1" ? $('[name="stock_order_by"]').val('0') : $('[name="stock_order_by"]').val('1');
            $('.btn-search').trigger('click');
        }
        $(".m_selectpicker").selectpicker();
        $('select').select2();
        $('.created').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        @if(isset($FILTER['created']))
        $('.created').val('{{$FILTER['created']}}');
        @else
        $('.created').val('');
        @endif
    </script>
    <script type="text/template" id="tb-card-tpl">
        <tr class="tr-card">
            <td>
                {code}
                <input type="hidden" name="code" value="{code}">
            </td>
            <td>
                {name_code}
            </td>
            <td>
                {day_active}
                <input type="hidden" name="day_active" value="{day_active}">
            </td>
            <td>
                {day_expiration}
                <input type="hidden" name="day_active" value="{day_expiration}">
            </td>
            <td>
                {name_type}
                <input type="hidden" name="type" value="{type}">
            </td>
            <td>
                {price_td}
                <input type="hidden" name="price" value="{price}">
            </td>
            <td>
                <input type="hidden" name="number_using" value="{number_using}">
                <input type="hidden" name="service_card_id" value="{service_card_id}">
                <input type="hidden" name="service_card_list_id" value="{service_card_list_id}">
                <a style="margin-top: -5px;"
                   class='remove m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill'><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
@stop