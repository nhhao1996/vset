<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list text-center">#</th>
            <th class="tr_thead_list text-center">{{__('Mã nhà đầu tư')}}</th>
            <th class="tr_thead_list text-center">{{__('Họ và tên')}}</th>
            {{--            <th class="tr_thead_list">{{__('Tài khoản')}}</th>--}}
            <th class="tr_thead_list text-center">{{__('Email')}}</th>
            <th class="tr_thead_list text-center">{{__('Số điện thoại')}}</th>

            @if(isset($FILTER['is_top']) != '')
                <th class="tr_thead_list text-center">
                    <a class="" name="stock_order_by_icon" onclick="changeValue();">
                        {{__('Số cổ phiếu')}}
                    <i class="fa {{isset($FILTER['stock_order_by']) ? ($FILTER['stock_order_by'] == '1' ? 'fa-arrow-up' : 'fa-arrow-down') : ''}}" id="123"></i>
                    </a>
                </th>
            @else
                <th class="tr_thead_list text-center">{{__('Số cổ phiếu')}}</th>
            @endif
            <th class="tr_thead_list text-center">{{__('Loại khách hàng')}}</th>
            <th class="tr_thead_list text-center">{{__('Đang đầu tư')}} (vnđ)</th>
            <th class="tr_thead_list text-center">{{__('Nhà môi giới')}}</th>
            <th class="tr_thead_list text-center">{{__('Tài khoản test')}}</th>
            <th class="tr_thead_list text-center">{{__('Nguồn khách hàng')}}</th>
            <th class="tr_thead_list text-center">{{__('Hành trình khách hàng')}}</th>
            <th class="tr_thead_list text-center">{{__('Ngày tạo')}}</th>
            <th class="tr_thead_list text-center">{{__('Trạng thái')}}</th>
            <th class="tr_thead_list text-center">{{__('Hành Động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        <?php
        $color = ["success", "brand", "danger", "accent", "warning", "metal", "primary", "info"];
        ?>
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                @php($num = rand(0,7))
                <tr>
                    @if(isset($page))
                        <td class="text_middle text-center">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle text-center">{{$key+1}}</td>
                    @endif
                    <td class="text_middle text-center">{{$item['customer_code']}}</td>
                    <td class="text_middle text-center">
                        <div class="m-list-pics m-list-pics--sm">
                            @if($item['customer_avatar']!=null)
                                <div class="m-card-user m-card-user--sm">
                                    <div class="m-card-user__pic">
                                        <img src="{{$item['customer_avatar']}}"
                                             onerror="this.onerror=null;this.src='https://placehold.it/40x40/00a65a/ffffff/&text=' + '{{substr(str_slug($item['full_name']),0,1)}}';"
                                             class="m--img-rounded m--marginless" alt="photo" width="40px"
                                             height="40px">
                                    </div>
                                    <div class="m-card-user__details">
                                        <a href="{{route("admin.customer.detail",$item['customer_id'])}}"
                                           class="m-card-user__name line-name font-name">{{$item['full_name']}}</a>
                                        {{--                                        <span class="m-card-user__email font-sub">--}}
                                        {{--                                                {{$item['group_name']}}--}}
                                        {{--                                            </span>--}}
                                    </div>
                                </div>
                            @else
                                <span style="width: 150px;">
                                        <div class="m-card-user m-card-user--sm">
                                            <div class="m-card-user__pic">
                                                <div class="m-card-user__no-photo m--bg-fill-{{$color[$num]}}">
                                                    <span>
                                                        {{substr(str_slug($item['full_name']),0,1)}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="m-card-user__details">
                                                <a href="{{route("admin.customer.detail",$item['customer_id'])}}"
                                                   class="m-card-user__name line-name font-name">{{$item['full_name']}}</a>
{{--                                                <span class="m-card-user__email font-sub">{{$item['group_name']}}</span>--}}
                                            </div>
                                        </div>
                                    </span>
                            @endif
                        </div>
                    </td>
                    <td class="text_middle text-center">{{$item['email']}}</td>
                    <td class="text_middle text-center">{{$item['phone2']}}</td>
                    <td class="text_middle text-center">
                        {{$item['stock']}}
                    </td>
                    <td class="text_middle text-center">
                        {{--                        @if($item['type_customer'] == 1)--}}
                        @if($item['sum_money_contract'] != 0)
                            {{__('Nhà đầu tư')}}
                        @else
                            {{__('Tiềm năng')}}
                        @endif
                    </td>
                    <td class="text_middle text-center">{{isset($item['sum_money_contract']) ? number_format($item['sum_money_contract']) : 0 }}</td>
                    <td class="text_middle text-center">
                        @if($item['broker_customer_id'] != null)
                            <i class="fa fa-check"></i>
                        @endif
                    </td>
                    <td class="text_middle text-center">
                        @if($item['is_test'] == 1)
                            <i class="fa fa-check"></i>
                        @endif
                    </td>
                    <td class="text_middle text-center">
                        @if($item['utm_source'] == null)
                            @if($item['customer_refer_code'] == null)
                                N/A
                            @else
                                @if($item['is_referal'] == 1)
                                    @if($item['customer_source_name'] == null)
                                        @lang('Link copy')
                                    @else
                                        {{$item['customer_source_name']}}
                                    @endif
                                @else
                                    Khác
                                @endif
                            @endif
                        @else
                            @if($item['is_referal'] == 1)
                                {{$item['refer_source_name']}}
                            @else
                                Khác
                            @endif
                        @endif
                    </td>
                    <td class="text_middle text-center">
                        {{$item['customer_journey_name'] == null ? 'N/A' : $item['customer_journey_name']}}
                    </td>
                    <td class="text_middle text-center">{{date("d/m/Y",strtotime($item['created_at']))}}</td>
                    <td class="text_middle text-center">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label class="ss--switch">
                                    <input type="checkbox"
                                           {{$item['is_actived'] == 1 ? 'checked' : ''}} disabled
                                           class="manager-btn" name="">
                                    <span></span>
                                </label>
                            </span>
                    </td>
                    <td class="text-center">
                        @if(in_array('admin.customer.detail', session('routeList')))
                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               href="{{route("admin.customer.detail",$item['customer_id'])}}" title="Chi tiết">
                                <i class="la la-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.customer.edit', session('routeList')))
                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               href="{{route("admin.customer.edit",$item['customer_id'])}}" title="Chỉnh sửa">
                                <i class="la la-edit"></i>
                            </a>
                        @endif
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('admin::customer.helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }

    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }

    .a-custom .flaticon-eye {
        top: 1px;
        left: 2px;
    }
</style>
