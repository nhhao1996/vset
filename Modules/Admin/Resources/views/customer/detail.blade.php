@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ NHÀ ĐẦU TƯ")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }

        .m-datatable__head th {
            background-color: #dff7f8 !important;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"
                                 style="font-size: 13px"></i> @lang("CHI TIẾT NHÀ ĐẦU TƯ")</span>
                    </h2>
                </div>
            </div>
        </div>
        <style>
            .nt-custome-row .form-group, .row {
                margin-right: 0;
                margin-left: 0;
            }

        </style>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
                    {{--                    mã khách hàng--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Mã nhà đầu tư')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{$item['customer_code']}}">
                                {{--                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i--}}
                                {{--                                                class="flaticon-menu-1"></i></span></span>--}}
                            </div>
                        </div>
                    </div>
                    {{--                    tài khoản--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Tài khoản')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="full_name" name="full_name" disabled
                                       class="form-control m-input"
                                       value="{{$item['phone']}}">
                            </div>
                        </div>
                    </div>
                    {{--                    email--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Email')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="email" name="email" class="form-control m-input" disabled
                                   placeholder="Vd: piospa@gmail.com" value="{{$item['email']}}">
                        </div>
                    </div>
                    {{--                    // tên khách hàng--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Tên khách hàng')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="full_name" name="full_name" disabled
                                       class="form-control m-input"
                                       placeholder="@lang('Nhập tên khách hàng')"
                                       value="{{$item['full_name']}}">
                            </div>
                        </div>
                    </div>
                    {{--                    // người giới thiệu--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Mã giới thiệu')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="customer_refer_code" name="customer_refer_code"
                                   class="form-control m-input" disabled
                                   value="{{$item['customer_refer_code']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-menu-1"></i></span></span>--}}
                        </div>
                    </div>
                    {{--                    // đã giới thiệu--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Người giới thiệu')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="customer_refer" name="customer_refer" class="form-control m-input"
                                   disabled
                                   value="{{$item['customer_refer_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-signs"></i></span></span>--}}
                        </div>
                    </div>
                    {{--                    // ngày sinh--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Ngày sinh')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="birthday" name="birthday" class="form-control m-input" disabled
                                   value="{{$item['birthday'] != null ? date("d/m/Y",strtotime($item['birthday'])) : null}}">
                        </div>
                    </div>
                    {{--                    // giới tính--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Giới tính')}}:
                        </label>
                        <div class="input-group">
                            <select id="gender" name="gender" class="form-control" disabled
                                    style="width: 100%">
                                <option value="">
                                    @lang('Chọn giới tính')
                                </option>
                                <option value="male"
                                        @if($item['gender'] == 'male') selected @endif>{{__('Nam')}} </option>
                                <option value="female"
                                        @if($item['gender'] == 'female') selected @endif>{{__('Nữ')}} </option>
                                <option value="other"
                                        @if($item['gender'] == 'other') selected @endif>{{__('Khác')}} </option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Ngân hàng')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" class="form-control m-input" disabled
                                   value="{{$item['bank_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Chi nhánh')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" class="form-control m-input" disabled
                                   value="{{$item['bank_account_branch']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
                    {{--                    //Tên tài khoản--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Tên tài khoản')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="bank_name" name="bank_name" class="form-control m-input" disabled
                                   value="{{$item['bank_account_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
                    {{--                    // số tài khoản--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số tài khoản')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="bank_account_no" name="bank_account_no" class="form-control m-input"
                                   disabled
                                   value="{{$item['bank_account_no']}}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label class="col-form-label label black-title">
                                {{__('Trạng thái')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label class="ss--switch">
                                    <input type="checkbox" {{$item['is_actived'] == 1 ? 'checked' :''}} disabled>
                                    <span></span>
                                </label>
                            </span>
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Tài khoản test')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label class="ss--switch">
                                        <input type="checkbox" {{$item['is_test'] == 1 ? 'checked' :''}} disabled>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">

{{--                    Hành trình khách hàng--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Hành trình khách hàng')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <select id="gender" name="gender" class="form-control" disabled
                                    style="width: 100%">
                                <option value="">
                                    @lang('Chọn hành trình khách hàng')
                                </option>
                                @foreach($customerJourney as $itemValue)
                                    <option value="{{$itemValue['group_customer_id']}}" {{$item['customer_group_id'] == $itemValue['group_customer_id'] ? 'selected' : ''}}> {{$itemValue['group_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    {{--                        // Hạng thành viên--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Hạng thành viên')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="rank_member" name="rank_member" class="form-control m-input"
                                   value="{{$item['member_levels_name']}}" disabled>
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-customer"></i></span></span>--}}
                        </div>
                    </div>
                    {{--                    // ĐIểm hạng--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Điểm hạng')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="point_rank" name="point_rank" class="form-control m-input"
                                   value="{{$item['point']}}" disabled>
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-diagram"></i></span></span>--}}
                        </div>
                    </div>
                    {{--                    // Ngày tạo--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Ngày tạo')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="created_at" name="created_at" class="form-control m-input"
                                   value="{{date("d/m/Y",strtotime($item['created_at']))}}" disabled>
                        </div>
                    </div>
                    {{--                    // số điện thoại liên hệ--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số điện thoại liên hệ')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="phone2" name="phone2" class="form-control m-input" disabled
                                   value="{{$item['phone2']}}">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Nguồn khách hàng')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <?php $text = 'N/A'; ?>

                            @if($item['utm_source'] == null)
                                @if($item['customer_refer_code'] == null)
                                    <?php $text = 'N/A'; ?>
                                @else
                                    @if($item['is_referal'] == 1)
                                        @if($item['customer_source_name'] == null)
                                            <?php $text = 'Link copy'; ?>
                                        @else
                                            <?php $text = $item['customer_source_name']; ?>
                                        @endif
                                    @else
                                        <?php $text = 'Khác'; ?>
                                    @endif
                                @endif
                            @else
                                @if($item['is_referal'] == 1)
                                    <?php $text = $item['refer_source_name']; ?>
                                @else
                                    <?php $text = 'Khác'; ?>
                                @endif
                            @endif

                            <input type="text" disabled class="form-control"
                                   value="{{$text}}">
                            {{--                            <input type="text" disabled class="form-control" value="{{$item['source_name'] == null ? 'Mặc định' : $item['source_name']}}">--}}
                        </div>
                    </div>
                    {{--                        // địa chỉ liên hệ--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Địa chỉ liên hệ')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            @if($item['contact_address'] == null && $item['thuongtru_dis_name'] == null && $item['thuongtru_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control"
                                       value="{{$item['contact_address']}}, {{$item['thuongtru_dis_type']}} {{$item['thuongtru_dis_name']}},{{$item['thuongtru_pro_type']}} {{$item['thuongtru_pro_name']}}"
                                       disabled>
                            @endif
                        </div>
                    </div>
                    {{--                    // địa chỉ thường trú--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Địa chỉ thường trú')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            @if($item['residence_address'] == null && $item['lienhe_dis_name'] == null && $item['lienhe_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control"
                                       value="{{$item['residence_address']}}, {{$item['lienhe_dis_type']}}  {{$item['lienhe_dis_name']}},{{$item['lienhe_pro_type']}} {{$item['lienhe_pro_name']}}"
                                       disabled>
                            @endif
                        </div>
                    </div>
                    {{--                    // số cmnd--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số CMND')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_no" name="ic_no" class="form-control m-input" disabled
                                   value="{{$item['ic_no']}}">
                        </div>
                    </div>
                    {{--                    // ngày cấp cmnd--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Ngày cấp')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_date" name="ic_date" class="form-control m-input" readonly disabled
                                   value="{{$item['ic_date'] != null ? \Carbon\Carbon::parse($item['ic_date'])->format('d/m/Y') : null}}" >
                        </div>
                    </div>
                    {{--                        // nơi cấp--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Nơi cấp')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_place" name="ic_place" class="form-control m-input" disabled
                                   value="{{$item['ic_place']}}" >

{{--                            <select name="ic_place" id="ic_place" class="form-control">--}}
{{--                                <option value="">Chọn Tỉnh/Thành phố</option>--}}
{{--                                @foreach($optionProvince as $key => $value)--}}
{{--                                    @if ($item['ic_place'] == $value)--}}
{{--                                        <option value="{{$value}}" selected>{{$value}}</option>--}}
{{--                                    @else--}}
{{--                                        <option value="{{$value}}">{{$value}}</option>--}}
{{--                                    @endif--}}
{{--                                @endforeach--}}
{{--                            </select>--}}

                            {{--                                <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-map-location"></i></span></span>--}}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-6">
                            <div class="row">
                                <label class="col-form-label label col-lg-12 black-title">
                                    {{__('Mặt trước')}}:
                                </label>
                                <div class="col-lg-12">
                                    <input type="hidden" id="ic_front_image" name="ic_front_image"
                                           value="{{$item['ic_front_image']}}">
                                    <input type="hidden" id="ic_front_image_upload" name="ic_front_image_upload"
                                           value="">
                                    <div class="m-widget19__pic">
                                        @if($item['ic_front_image']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah_ic_front_image"
                                                 src="{{$item['ic_front_image']}}" width="220px" height="220px"
                                                 alt="{{__('Hình ảnh')}}"/>
                                        @else
                                            <img class="m--bg-metal  m-image img-sd" id="blah_ic_front_image"
                                                 src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                                 alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                        @endif

                                    </div>
                                    {{--                            <img src="{{$item['ic_front_image']}}" height="auto" width="100%">--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <label class="col-form-label label col-lg-12 black-title">
                                    {{__('Mặt sau')}}:
                                </label>
                                <div class="col-lg-12">
                                    {{--                            <img src="{{$item['ic_back_image']}}" height="auto" width="100%">--}}
                                    <input type="hidden" id="ic_back_image" name="ic_back_image"
                                           value="{{$item['ic_back_image']}}">
                                    <input type="hidden" id="ic_back_image_upload" name="ic_back_image_upload"
                                           value="">
                                    <div class="m-widget19__pic">
                                        @if($item['ic_back_image']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah_ic_back_image"
                                                 src="{{$item['ic_back_image']}}" width="220px" height="220px"
                                                 alt="{{__('Hình ảnh')}}"/>
                                        @else
                                            <img class="m--bg-metal  m-image img-sd" id="blah_ic_back_image"
                                                 src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                                 alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--begin::Portlet-->
        <div class="m-portlet__body">
            <div class=" m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.customer')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('HUỶ')}}</span>
                    </span>
                    </a>

                    @if(in_array('admin.customer.edit', session('routeList')))
                        <a href="{{route('admin.customer.edit',  $item['customer_id'])}}"
                           class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span>
                            <i class="la la-edit"></i>
                            <span>{{__('CHỈNH SỬA')}}</span>
                            </span>
                        </a>
                    @endif

                </div>
            </div>
            <div class="kt-portlet__body">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#"
                           data-target="#info_customer">{{_('Danh sách ví')}}</a>
                    </li>
                    <li class="nav-item" onclick="vsetListContract.tab_list_contract()">
                        <a class="nav-link" data-toggle="tab" href="#"
                           data-target="#list_contract">{{_('Danh sách hợp đồng')}}</a>
                    </li>
                    <li class="nav-item" onclick="vsetListStockContract.tab_list_stock_contract()">
                        <a class="nav-link" data-toggle="tab" href="#"
                           data-target="#list_stock_contract">{{_('Danh sách hợp đồng cổ phiếu')}}</a>
                    </li>
                    <li class="nav-item" onclick="vsetScript.tab_list_history()">
                        <a class="nav-link " data-toggle="tab" href="#"
                           data-target="#history_actived">{{_('Lịch sử giao dịch')}}</a>
                    </li>
                    <li class="nav-item" onclick="vsetStockHistoryScript.tab_list_stock_history()">
                        <a class="nav-link " data-toggle="tab" href="#"
                           data-target="#history_stock">{{_('Lịch sử giao dịch cổ phiếu')}}</a>
                    </li>

                    <li class="nav-item" onclick="vsetListIntroduct.tab_list_introduct()">
                        <a class="nav-link" data-toggle="tab" href="#"
                           data-target="#list_introduct">{{_('Danh sách đã giới thiệu')}}</a>
                    </li>
                    <li class="nav-item" onclick="vsetContractCommission.tab_list_contract_commission()">
                        <a class="nav-link " data-toggle="tab" href="#"
                           data-target="#list_contract_commission">{{_('Lịch sử nhận thưởng')}}</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="info_customer" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            <div class="table-responsive">
                                <table class="table table-striped m-table m-table--head-bg-default">
                                    <thead class="bg">
                                    <tr>
                                        <td class="font-weight-bold">{{__('Ví hợp tác đầu tư')}} (vnđ)</td>
                                        <td class="font-weight-bold">{{__('Ví cổ phiếu')}} (vnđ)</td>
                                        <td class="font-weight-bold">{{__('Ví cổ tức')}} (vnđ)</td>
                                        <td class="font-weight-bold">{{__('Ví tiết kiệm')}} (vnđ)</td>
                                        <td class="font-weight-bold">{{__('Ví lãi')}} (vnđ)</td>
                                        <td class="font-weight-bold">{{__('Ví thưởng')}} (vnđ)</td>
{{--                                        <td class="font-weight-bold">{{__('Ví đã đầu tư')}} (vnđ)</td>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{number_format($bondWallet == null ? 0 : $bondWallet, 2, '.', ',')}}</td>
                                        <td>{{number_format($stockCustomer == null ? 0 : $stockCustomer['wallet_stock_money'], 2, '.', ',')}}</td>
                                        <td>{{number_format($stockCustomer == null ? 0 : $stockCustomer['wallet_dividend_money'], 2, '.', ',')}}</td>
                                        <td>{{number_format($savingsWallet == null ? 0 : $savingsWallet, 2, '.', ',')}}</td>
                                        <td>{{number_format($interestWalletCustomer == null ? 0 : $interestWalletCustomer, 2, '.', ',')}}</td>
                                        <td>{{number_format($bonusWallet == null ? 0 : $bonusWallet, 2, '.', ',')}}</td>
{{--                                        <td>{{number_format($totalInvested, 2, '.', ',')}}</td>--}}
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="list_contract" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            <div class="table-responsive">
                                @include('admin::customer.include.list-contract')
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="list_stock_contract" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            <div class="table-responsive">
                                @include('admin::customer.include.list-stock-contract')
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="history_stock" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            @include('admin::customer.include.list-stock-history')
                        </div>
                    </div>
                    <div class="tab-pane" id="history_actived" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            @include('admin::customer.include.list-history')
                        </div>
                    </div>
                    <div class="tab-pane" id="list_introduct" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            <div class="table-responsive">
                                @include('admin::customer-broker.include.list-introduct')
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="list_contract_commission" role="tabpanel">
                        <div class="form-group col-lg-12 p-0">
                            @include('admin::customer-broker.include.list-contract-commission')
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--end::Portlet-->


        <!-- HTML5 -->
        {{--<p style="width:80%" data-value="80"></p>--}}
        {{--<progress max="100" value="80" class="html5">--}}
        {{--<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->--}}
        {{--<div class="progress-bar">--}}
        {{--<span style="width: 80%">80%</span>--}}
        {{--</div>--}}
        {{--</progress>--}}
    </div>

    <input type="hidden" id="customer_id_hidden" value="{{$item['customer_id']}}">
    {{--    <form id="bill-receipt" target="_blank" action="{{route('admin.receipt.print-bill')}}" method="GET">--}}
    <input type="hidden" id="amount_bill" name="amount_bill">
    <input type="hidden" id="customer_debt_id" name="customer_debt_id">
    <input type="hidden" id="receipt_id" name="receipt_id">
    <input type="hidden" id="amount_return_bill" name="amount_return_bill">
    {{--    </form>--}}
@stop
@section("after_style")
    {{--    <link rel="stylesheet" href="{{asset('static/backend/css/process.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">

@stop
@section('after_script')
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>--}}
    {{--    <script>--}}
    {{--        var id_customer = {{$item['customer_id']}}--}}
    {{--    </script>--}}
    {{--    <script src="{{asset('static/backend/js/admin/customer/script.js')}}" type="text/javascript"></script>--}}
    <script src="{{asset('static/backend/js/admin/customer/vset-script.js?v='.time())}}"
            type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/customer/vset-list-contract-script.js?v='.time())}}"
            type="text/javascript"></script>
@stop