@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ HỢP ĐỒNG')}}
    </span>
@endsection
@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-eye"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT HỢP ĐỒNG')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group">
                <h4 class="m--font-info">@lang('Thông tin cá nhân'):</h4>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Họ và tên')}}:
                        </label>
                        <div class="input-group">
                            <input id="customer_name" name="customer_name" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__(' Nhập họ và tên ')}}" disabled
                                   value="{{$item['customer_name'] != null ? $item['customer_name'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Số điện thoại ')}}:
                        </label>
                        <div class="input-group">
                            <input id="customer_phone" name="customer_phone" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Số điện thoại ')}}" disabled
                                   value="{{$item['customer_phone'] != null ? $item['customer_phone'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Email')}}:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Email')}}" disabled
                                   value="{{$item['customer_email'] != null ? $item['customer_email'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{__('Ngày sinh')}}:
                        </label>
                        <div class="input-group">
                            @if($item['birthday'] != null)
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__(' Ngày sinh ')}}" disabled
                                       value="{{\Carbon\Carbon::parse($item['birthday'])->format('d/m/Y')}}">
                            @else
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__(' Ngày sinh ')}}" disabled value="N/A">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Địa chỉ liên hệ')}}: <b class="text-danger"></b>
                        </label>
                        <div class="input-group">

                            @if($item['residence_address'] != null && $item['lienhe_dis_name'] != null && $item['lienhe_pro_name'] != null)
                                <textarea id="price-standard" name="price_standard" type="text"
                                          class="form-control m-input class"
                                          placeholder="{{__('Địa chỉ liên hệ')}}"
                                          disabled>{{$item['contact_address']}}, {{$item['thuongtru_dis_type']}} {{$item['thuongtru_dis_name']}},{{$item['thuongtru_pro_type']}} {{$item['thuongtru_pro_name']}}</textarea>
                            @else
                                <input class="form-control" value="" disabled>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('CMND')}}:
                        </label>
                        <div class="input-group">
                            <input id="customer_ic_no" name="customer_ic_no" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('CMND')}}" disabled
                                   value="{{$item['customer_ic_no'] != null ? $item['customer_ic_no'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__(' Ngày cấp CMND')}}:
                        </label>
                        <div class="input-group">
                            @if($item['ic_date'] != null)
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày cấp CMND')}}" disabled
                                       value="{{\Carbon\Carbon::parse($item['ic_date'])->format('d/m/Y')}}">
                            @else
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày cấp CMND')}}" disabled value="N/A">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Nơi cấp CMND')}}:
                        </label>
                        <div class="input-group">
                            <input id="description-en" name="description_en" type="text"
                                   class="form-control m-input class" disabled
                                   placeholder="{{__('Nơi cấp CMND')}}"
                                   value="{{$item['ic_place'] != null ? $item['ic_place'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Địa chỉ thường trú')}}: <b class="text-danger"></b>
                        </label>
                        <div class="input-group">
                            @if($item['residence_address'] == null && $item['lienhe_dis_name'] == null && $item['lienhe_pro_name'] == null)
                                <input class="form-control" value="N/A" disabled>
                            @else
                                <input id="unit-id" name="unit_id" type="text" class="form-control m-input class"
                                       placeholder="{{__('Địa chỉ thường trú')}}" disabled
                                       value="{{$item['residence_address']}}, {{$item['lienhe_dis_type']}} {{$item['lienhe_dis_name']}},{{$item['lienhe_pro_type']}} {{$item['lienhe_pro_name']}}">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Số tài khoản')}}:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text"
                                   class="form-control m-input class"
                                   placeholder="{{__('Số tài khoản')}}" disabled
                                   value="{{$item['bank_account_no'] != null ? $item['bank_account_no'] : 'N/A'}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <h4 class="m--font-info">@lang('Thông tin hợp đồng'):</h4>
            </div>
            <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    @if($item['contract_code_extend'] != null)
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Mã hợp đồng được gia hạn')}}:
                            </label>
                            <div class="input-group">
                                <input id="interest-rate-standard" name="interest_rate_standard" type="text"
                                       class="form-control m-input class font-weight-bold"
                                       placeholder="" disabled
                                       value="{{$item['contract_code_extend']}}">
                            </div>
                        </div>
                    @endif
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Mã hợp đồng')}}:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Mã hợp đồng')}}" disabled
                                   value="{{$item['customer_contract_code'] != null ? $item['customer_contract_code'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Tên gói')}}:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Tên gói')}}" disabled
                                   value="{{$item['product_name'] != null ? $item['product_name'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Số lượng')}}:
                        </label>
                        <div class="input-group">
                            <input id="interest-rate-standard" name="interest_rate_standard" type="text"
                                   class="form-control m-input class"
                                   placeholder="{{__('Số lượng')}}" disabled
                                   value="{{$item['quantity'] != null ? $item['quantity'] : 'N/A'}}">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Mệnh giá')}} (vnđ):
                        </label>
                        <div class="input-group">
                            <input id="withdraw-fee-rate-before" name="withdraw_fee_rate_before" type="text"
                                   class="form-control m-input class font-weight-bold"
                                   placeholder="{{__('Mệnh giá')}}" disabled
                                   value="{{number_format($item['price_standard'] != null ? $item['price_standard'] : 0)}}">
                        </div>
                    </div>
                    @if($oldContract != null)
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Thưởng thêm khi gia hạn hợp đồng')}} (vnđ):
                            </label>
                            <div class="input-group">
                                <input id="withdraw-fee-rate-before" name="" type="text"
                                       class="form-control m-input class font-weight-bold"
                                       placeholder="{{__('Thưởng thêm khi gia hạn hợp đồng')}}" disabled
                                       value="{{number_format($item['bonus_extend'] != null ? $item['bonus_extend'] : 0)}}">
                            </div>
                        </div>
                    @endif
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Tổng giá trị HĐ')}} (vnđ):
                        </label>
                        <div class="input-group">
                            @if($oldContract != null)
                                <input id="withdraw-fee-interest-rate" name="withdraw_fee_interest_rate" type="text"
                                       class="form-control m-input class font-weight-bold"
                                       placeholder="{{__('Tổng giá trị HĐ')}}" disabled
                                       value="{{number_format(($item['price_standard']*$item['quantity'])+ $item['bonus_extend'])}}">
                            @else
                                <input id="withdraw-fee-interest-rate" name="withdraw_fee_interest_rate" type="text"
                                       class="form-control m-input class font-weight-bold"
                                       placeholder="{{__('Tổng giá trị HĐ')}}" disabled
                                       value="{{number_format($item['total_amount'] != null ? $item['total_amount'] : 0)}}">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Ngày kí hợp đồng')}}:
                        </label>
                        <div class="input-group">
                            @if($item['customer_contract_start_date'] != null)
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày kí hợp đồng')}}" disabled
                                       value="{{\Carbon\Carbon::parse($item['customer_contract_start_date'])->format('H:i d/m/Y')}}">
                            @else
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày kí hợp đồng')}}" disabled value="N/A">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Nhân viên hỗ trợ')}}:
                        </label>
                        <div class="input-group">
                            <input id="withdraw-min-amount" name="withdraw_min_amount" type="text"
                                   class="form-control m-input class"
                                   disabled
                                   value="{{$item['staff_full_name'] != null ? $item['staff_full_name'] : 'N/A'}}">
                        </div>
                        <span class="errs error-withdraw-min-amount"></span>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Lãi suất')}} (%):
                        </label>
                        <div class="input-group">
                            <input id="withdraw-min-amount" name="withdraw_min_amount" type="text"
                                   class="form-control m-input class"
                                   placeholder="{{__('Lãi suất')}}" disabled
                                   value="{{$item['interest_rate'] != null ? number_format($item['interest_rate'],2) : 'N/A'}}">
                        </div>
                        <span class="errs error-withdraw-min-amount"></span>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Ngày phát hành')}}:
                        </label>
                        <div class="input-group">
                            @if($item['customer_contract_start_date'] != null)
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày phát hành')}}" disabled
                                       value="{{\Carbon\Carbon::parse($item['customer_contract_start_date'])->format('H:i d/m/Y')}}">
                            @else
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày phát hành')}}" disabled value="N/A">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Ngày đến hạn')}}:
                        </label>
                        <div class="input-group">
                            @if($item['customer_contract_end_date'] != null && $item['customer_contract_end_date'] != 0)
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày đến hạn')}}" disabled
                                       value="{{\Carbon\Carbon::parse($item['customer_contract_end_date'])->format('H:i d/m/Y')}}">
                            @else
                                <input id="birthday" name="birthday" type="text" class="form-control m-input class"
                                       placeholder="{{__('Ngày đến hạn')}}" disabled value="N/A">
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Trạng thái')}}:
                        </label>
                        <div class="input-group">
                            @if($item['customer_contract_end_date'] != null && $item['customer_contract_end_date'] != '')
                                @if(\Carbon\Carbon::now() < \Carbon\Carbon::parse($item['customer_contract_end_date']) && $item['is_active'] == 1)
                                    <span class="text-success">
                                        @lang('Đang hoạt động')
                                    </span>
                                @else
                                    <span class="text-danger">
                                        @lang('Đã hết hạn')
                                    </span>
                                @endif
                            @else
                                @if($item['is_active'] == 1)
                                    <span class="text-success">
                                        @lang('Đang hoạt động')
                                    </span>
                                @else
                                    <span class="text-danger">
                                        @lang('Đã hết hạn')
                                    </span>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>
                            {{__('Phương thức thanh toán')}}:
                        </label>
                        <div class="input-group">
                            <input id="refer-commission-value" name="refer_commission_value" type="text"
                                   class="form-control m-input class"
                                   placeholder="{{__('Phương thức thanh toán')}}" disabled
                                   value="{{$item['payment_method'] != null ? $item['payment_method'] : 'N/A'}}">
                        </div>
                    </div>
{{--                    <div class="form-group m-form__group">--}}
{{--                        <label>--}}
{{--                            {{__('Hợp đồng được gia hạn')}}:--}}
{{--                        </label>--}}
{{--                        <div class="input-group">--}}
{{--                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label style="margin: 0 0 0 10px; padding-top: 4px">--}}
{{--                                    <input type="checkbox"--}}
{{--                                           {{$oldContract != null ? 'checked' : ''}} class="manager-btn"--}}
{{--                                           name="" disabled="">--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="tab-content">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item border-right-0">
                            <a class="nav-link active show" data-toggle="tab" show
                               id="file_contract">@lang("FILE HỢP ĐỒNG")</a>
                        </li>
                        <li class="nav-item border-right-0 li_interest_month">
                            <a class="nav-link" data-toggle="tab"
                               id="interest_month">@lang("LÃI SUẤT THÁNG")</a>
                        </li>
{{--                        <li class="nav-item border-right-0">--}}
{{--                            <a class="nav-link" data-toggle="tab" id="interest_date">@lang("LÃI SUẤT NGÀY")</a>--}}
{{--                        </li>--}}
                        <li class="nav-item border-right-0">
                            <a class="nav-link" data-toggle="tab"
                               id="interest_time">@lang("LÃI SUẤT THỜI ĐIỂM")</a>
                        </li>
                        {{--                        <li class="nav-item">--}}
                        {{--                            <a class="nav-link son" data-toggle="tab"--}}
                        {{--                               id="withdraw_group">@lang("YÊU CẦU RÚT LÃI")</a>--}}
                        {{--                        </li>--}}
                        <li class="nav-item border-right-0">
                            <a class="nav-link" data-toggle="tab" id="withdraw">@lang("YÊU CẦU RÚT GỐC")</a>
                        </li>
                        <li class="nav-item border-right-0">
                            <a class="nav-link" data-toggle="tab" id="serial_contract">@lang("SỐ SERIAL")</a>
                        </li>
                    </ul>
                    <div>
                        <div id="div_file_contract" style="display: block">
                            <div id="autotable-file">
                                <form class="frmFilter">
                                    <input type="hidden" id="customer_contract_id" name="customer_contract_id"
                                           value="{{$item['customer_contract_id']}}">
                                </form>
                                <div class="table-content">

                                </div>
                            </div>
                        </div>
                        <div id="div_interest_month" style="display: none">
                            <div id="autotable-interest-month">
                                <form class="frmFilter">
                                    <input type="hidden" id="customer_contract_id" name="customer_contract_id"
                                           value="{{$item['customer_contract_id']}}">
                                </form>
                                <div class="table-content">

                                </div>
                            </div>
                        </div>
{{--                        <div id="div_interest_date" style="display: none">--}}
{{--                            <div id="autotable-interest-date">--}}
{{--                                <form class="frmFilter">--}}
{{--                                    <input type="hidden" id="customer_contract_id" name="customer_contract_id"--}}
{{--                                           value="{{$item['customer_contract_id']}}">--}}
{{--                                </form>--}}
{{--                                <div class="table-content">--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div id="div_interest_time" style="display: none">
                            <div id="autotable-interest-time">
                                <form class="frmFilter">
                                    <input type="hidden" id="customer_contract_id" name="customer_contract_id"
                                           value="{{$item['customer_contract_id']}}">
                                </form>
                                <div class="table-content">

                                </div>
                            </div>
                        </div>
                        <div id="div_withdraw_group" style="display: none">
                            Không có dữ liệu
                        </div>
                        <div id="div_withdraw" style="display: none">
                            <div id="autotable-withdraw">
                                <form class="frmFilter">
                                    <input type="hidden" id="customer_contract_id" name="customer_contract_id"
                                           value="{{$item['customer_contract_id']}}">
                                </form>
                                <div class="table-content">

                                </div>
                            </div>
                        </div>
                        <div id="div_serial_contract" style="display: none">
                            <div id="autotable-serial-contract">
                                <form class="frmFilter">
                                    <input type="hidden" id="customer_contract_id" name="customer_contract_id"
                                           value="{{$item['customer_contract_id']}}">
                                </form>
                                <div class="table-content">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer save-attribute m--margin-right-20">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.customer-contract')}}"
                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/customer-contract/detail.js?v='.time())}}"></script>
    @if(isset($tab) && $tab == 'month')
        <script>
            $(document).ready(function () {
                $("#interest_month").trigger("click");
            });
        </script>
    @endif
@stop
