<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th>
                <label class="m-checkbox m-checkbox--air m-checkbox--solid ss--m-checkbox--state-success mr-3 checkAll" onclick="check()">
                    <input type="checkbox" id="module_1" name="is_active">
                    <span class="color_button"></span>
                </label>
            </th>
            <th>{{__('Số hợp đồng ')}}</th>
            <th>{{__('Giá Trị Lãi Suất Có Thể Rút (Vnđ)')}}</th>
            <th>{{__('Họ và tên nhà đầu tư')}}</th>
            <th>{{__('Số điện thoại')}}</th>
{{--            <th>{{__('CMND')}}</th>--}}
            <th>{{__('Ngày Rút Theo Kế Hoạch')}}</th>
            <th>{{__('Lãi suất tháng')}}</th>
            <th>{{__('Tỉ lệ lãi suất tháng (%)')}}</th>
            <th>{{__('Giá trị lãi suất (Vnđ)')}}</th>
            <th>{{__('Giá Trị Lãi Suất Tích Lũy (Vnđ)')}}</th>

        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($list))
            @foreach ($list as $key => $item)
                @php($num = rand(0,7))
                <tr>
                    <td>
                        <label class="m-checkbox m-checkbox--air m-checkbox--solid ss--m-checkbox--state-success mr-3 checkItem" >
                            <input type="checkbox" data-id="{{$item['customer_contract_interest_by_month_id']}}" name="is_active">
                            <span class="color_button"></span>
                        </label>
                    </td>
                    <td>
                        @if(in_array('admin.customer-contract.detail', session('routeList')))
                            <a href="{{route('admin.customer-contract.detail', [$item['customer_contract_id'], 'month'])}}">
                                {{$item['customer_contract_code']}}
                            </a>
                        @else
                            {{$item['customer_contract_code']}}
                        @endif
                    </td>
                    <td>{{ number_format($item['interest_banlance_amount'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}</td>
                    <td>{{$item['full_name']}}</td>
                    <td>{{$item['phone']}}</td>
{{--                    <td>{{$item['ic_no']}}</td>--}}
                    <td>{{\Carbon\Carbon::parse($item['withdraw_date_planning'])->format('H:i d/m/Y')}}</td>
                    <td>{{$item['interest_month']}}/{{$item['interest_year']}}</td>
                    <td>{{ number_format($item['interest_rate_by_month'], 3)}}</td>
                    <td>{{ number_format($item['interest_amount'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}</td>
                    <td>{{ number_format($item['interest_total_amount'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}</td>



                </tr>

            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $list->links('admin::customer-contract.helpers.paging') }}


