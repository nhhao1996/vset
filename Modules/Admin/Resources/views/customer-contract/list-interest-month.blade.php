<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">@lang('Tháng')</th>
        <th class="tr_thead_list_dt_cus">@lang('Tỉ lệ lãi suất tháng') (%)</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị lãi suất') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị lãi suất tích lũy') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị lãi suất có thể rút') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Ngày rút theo kế hoạch') (vnđ)</th>
        <th></th>
    </tr>
    </thead>
    <tbody style="font-size: 13px">
    @if(isset($listMonth))
        @foreach ($listMonth as $k => $v)
            <tr>
                <td>
                    {{$v['interest_month'] .'/'. $v['interest_year']}}
                </td>
                <td>{{$v['interest_rate_by_month']}}</td>
                <td>
                    {{$v['interest_amount'] != null ? number_format($v['interest_amount'],3) : 0}}
                </td>
                <td>
                    {{$v['interest_total_amount'] != null ? number_format($v['interest_total_amount'],3) : 0}}

                </td>
                <td>
                    {{$v['interest_banlance_amount'] != null ? number_format($v['interest_banlance_amount'],3) : 0}}

                </td>
                <td>
                    {{$v['term_time_type'] == 0 ? 'N/A' : \Carbon\Carbon::parse($v['withdraw_date_planning'])->format('d/m/Y')}}
                </td>
                <td>
                    @if($v['confirmed_by'] == null)
                        <button type="button" onclick="contract.confirm({{$v['customer_contract_interest_by_month_id']}})" class="btn-confirm-{{$v['customer_contract_interest_by_month_id']}} ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md ">Xác nhận</button>
                    @endif
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ $listMonth->links('helpers.paging') }}

