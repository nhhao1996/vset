@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ HỢP ĐỒNG')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
    {{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH LÃI HỢP ĐỒNG')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
{{--                @if(in_array('admin.customer-contract.confirm-contract-interest', session('routeList')))--}}
                    <a href="javascript:void(0)" onclick="customerContract.approved()"
                       class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-2">
                        <span>
                            <span> {{__('XÁC NHẬN LÃI')}}</span>
                        </span>
                    </a>
{{--                @endif--}}
{{--                @if(in_array('admin.customer-contract.export-list-contract-interest', session('routeList')))--}}
                    <a href="{{route('admin.customer-contract.export-list-contract-interest')}}"
                       class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-2">
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span> {{__('Export DANH SÁCH LÃI')}}</span>
                        </span>
                    </a>
{{--                @endif--}}

                {{--                <a href="{{route('admin.customer-contract.add')}}"--}}
                {{--                   class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">--}}
                {{--                    <span>--}}
                {{--                        <i class="fa fa-plus-circle"></i>--}}
                {{--                        <span> {{__('THÊM HỢP ĐỒNG')}}</span>--}}
                {{--                    </span>--}}
                {{--                </a>--}}

            </div>

        </div>
        <div class="card-header tab-card-header ">
            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                {{--                <li class="nav-item">--}}
                {{--                    <a class="nav-link active show" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="All" aria-selected="true">Tất cả</a>--}}
                {{--                </li>--}}
                <li class="nav-item">
                    <a class="nav-link"  href="{{route('admin.customer-contract')}}" >Danh sách hợp đồng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active show" href="{{route('admin.customer-contract.list-interest-approved')}}" >Danh sách lãi cần duyệt</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{route('admin.customer-contract.list-nearly-expired')}}" >Danh sách hợp đồng sắp hết hạn</a>
                </li>
            </ul>
        </div>
        <div class="m-portlet__body">
            <!-- seach -->
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       placeholder="{{__('Nhập mã hợp đồng , tên nhà đầu tư , số điện thoại hoặc CMND')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group" style="background-color: white">
                            <div class="m-input-icon m-input-icon--right">
                                <input readonly="" class="form-control m-input daterange-picker"
                                       id="created_at" name="created_at" autocomplete="off"
                                       placeholder="{{__('Ngày rút theo kế hoạch')}}">
                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                        <span><i class="la la-calendar"></i></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <button class="btn btn-primary color_button btn-search">
                                {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                            </button>
                            <a href="{{route('admin.customer-contract.list-interest-approved')}}"
                               class="btn btn-metal  btn-search padding9x padding9px">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>

            <!--seach -->
            <div class="table-content m--padding-top-30">
                @include('admin::customer-contract.list-contract-approved')

            </div><!-- end table-content -->

        </div>
    </div>
@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
<link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
    {{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/customer-contract-interest/script.js?v='.time())}}" type="text/javascript"></script>
<script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        new AutoNumeric.multiple('.number-money', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });
    </script>
@stop