<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th>#</th>
            <th>{{__('Số hợp đồng ')}}</th>
            <th>{{__('Họ và tên nhà đầu tư')}}</th>
            <th>{{__('Số điện thoại nhà đầu tư')}}</th>
            <th>{{__('Địa chỉ liên hệ')}}</th>
            <th data-toggle="tooltip" title="Số lượng hợp đồng">{{__('Số lượng gói')}}</th>
            <th>{{__('Loại gói')}}</th>
            <th>{{__('Tổng giá trị HĐ (vnđ)')}}</th>
            <th>{{__('Lãi suất (%) ')}}</th>
            <th>{{__('Ngày nhận lãi ')}}</th>
            <th>{{__('Ngày bắt đầu hợp đồng')}}</th>
            <th>{{__('Ngày hết hạn hợp đồng')}}</th>
            <th>{{__('Trạng thái ')}}</th>
            <th>{{__('Hành động ')}}</th>
{{--            <th class="hidden-th"></th>--}}
        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                @php($num = rand(0,7))
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td>
                        @if(in_array('admin.customer-contract.detail', session('routeList')))
                            <a href="{{route('admin.customer-contract.detail', $item['customer_contract_id'])}}">
                                {{$item['customer_contract_code']}}
                            </a>
                        @else
                            {{$item['customer_contract_code']}}
                        @endif
                    </td>
                    <td>{{$item['full_name']}}</td>
                    <td>{{$item['customer_phone']}}</td>
                    <td>{{$item['contact_address_contact']}} - {{$item['district_name_contact']}}
                        - {{$item['province_name_contact']}}</td>
                    <td class="text-center" data-toggle="tooltip"
                        title="Số lượng hợp đồng">{{ $item['quantity'] == null ? 'N/A' : $item['quantity'] }}</td>
                    <td>{{ $item['category_name_vi']}}</td>
                    <td>{{ number_format($item['total_amount'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}</td>
                    <td>{{$item['interest_rate'] == null ? 'N/A' : $item['interest_rate']}}</td>
                    <td>{{ $item['withdraw_date_planning'] == null ? \Carbon\Carbon::parse($item['customer_contract_start_date'])->addMonth($item['withdraw_interest_time'])->format('d-m-Y')
                            : date("d-m-Y",strtotime($item['withdraw_date_planning']))  }}</td>
                    <td>{{ $item['customer_contract_start_date'] == null ? 'N/A' : date("d-m-Y",strtotime($item['customer_contract_start_date']))  }}</td>
                    <td>
                        {{ $item['customer_contract_end_date'] == 0 ? 'N/A'
                        : date("d-m-Y",strtotime($item['customer_contract_end_date']))  }}
                    </td>
                    <td class="text-center">
                        @if($item['customer_contract_end_date'] != null && $item['customer_contract_end_date'] != '')
                            @if(\Carbon\Carbon::now() < \Carbon\Carbon::parse($item['customer_contract_end_date']) && $item['is_active'] == 1)
                                <span class="text-success">
                                    @lang('Đang hoạt động')
                                </span>
                            @else
                                <span class="text-danger">
                                    @lang('Đã hết hạn')
                                </span>
                            @endif
                        @else
                            @if($item['is_active'] == 1)
                                <span class="text-success">
                                    @lang('Đang hoạt động')
                                </span>
                            @else
                                <span class="text-danger">
                                    @lang('Đã hết hạn')
                                </span>
                            @endif

                        @endif
                    </td>
                    <td class="text-center">
                        @if(in_array('admin.customer-contract.edit', session('routeList')))
                            <a href="{{route('admin.customer-contract.edit',['id'=>$item['customer_contract_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chỉnh sửa')}}">
                                <i class="la la-edit"></i>
                            </a>
                        @endif
                    </td>
{{--                    <td class="text-center tdEndContract">--}}
{{--                        @if(\Carbon\Carbon::now() > \Carbon\Carbon::parse($item['customer_contract_end_date']) && $item['is_active'] == 0--}}
{{--                             && $item['is_deleted'] =='0')--}}
{{--                            <button data-id="{{$item['customer_contract_id']}}" onclick="CustomerContractHandler.onBtnEndContract({{$item['customer_contract_id']}})"--}}
{{--                                    class="btnEndContract btn btn-primary color_button ">--}}
{{--                                @lang('Kết thúc hợp đồng')--}}
{{--                            </button>--}}
{{--                        @endif--}}
{{--                    </td>--}}
                </tr>

            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}


