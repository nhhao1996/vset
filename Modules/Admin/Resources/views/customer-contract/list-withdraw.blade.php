<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">@lang('Mã giao dịch')</th>
        <th class="tr_thead_list_dt_cus">@lang('Loại giao dịch')</th>
        <th class="tr_thead_list_dt_cus">@lang('Số tiền yêu cầu rút') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Ngày rút')</th>
        <th class="tr_thead_list_dt_cus">@lang('Trạng thái')</th>
{{--        <th class="tr_thead_list_dt_cus">@lang('Ngày dự chi theo kế hoạch')</th>--}}
    </tr>
    </thead>
    <tbody style="font-size: 13px">
    @if(isset($listWithdraw))
        @foreach ($listWithdraw as $k => $v)
            <tr>
                <td>{{$v['withdraw_request_code']}}</td>
                <td>
                    @if($v['withdraw_request_type'] == 'bond')
                        @lang('Hợp tác đầu tư')
                    @elseif($v['withdraw_request_type'] == 'interest')
                        @lang('Lãi suất')
                    @elseif($v['withdraw_request_type'] == 'saving')
                        @lang('Tiết kiệm')
                    @endif
                </td>
                <td>
                    {{number_format($v['withdraw_request_amount'] != null ? $v['withdraw_request_amount'] : 0)}}
                </td>
                <td>
                    {{$v['withdraw_request_day'] .'/'. $v['withdraw_request_month'] .'/'. $v['withdraw_request_year'] .' '. $v['withdraw_request_time']}}
                </td>
                <td>
                    @if($v['withdraw_request_status'] == 'new')
                        <span class="m-badge m-badge--success m-badge--wide">@lang('Mới')</span>
                    @elseif($v['withdraw_request_status'] == 'inprocess')
                        <span class="m-badge m-badge--primary m-badge--wide">@lang('Đang tiến hàng')</span>
                    @elseif($v['withdraw_request_status'] == 'confirm')
                        <span class="m-badge m-badge--warning m-badge--wide">@lang('Đã xác nhận')</span>
                    @elseif($v['withdraw_request_status'] == 'done')
                        <span class="m-badge m-badge--info m-badge--wide">@lang('Hoàn thành')</span>
                    @elseif($v['withdraw_request_status'] == 'cancel')
                        <span class="m-badge m-badge--danger m-badge--wide">@lang('Hủy')</span>
                    @endif
                </td>
{{--                <td>--}}
{{--                    {{\Carbon\Carbon::parse($v['withdraw_date_planning'])->format('H:i d/m/Y')}}--}}
{{--                </td>--}}
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ $listWithdraw->links('helpers.paging') }}

