@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ HỢP ĐỒNG')}}</span>
@stop
@section('content')
    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/

        input[type=file] {
            padding: 10px;
            background: #fff;
        }

        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }

        .m-image {
            /*padding: 5px;*/
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }

        .form-control-feedback {
            color: red;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT HỢP ĐỒNG')}}
                    </h2>

                </div>
            </div>
        </div>
        <form id="form-edit">
            <div class="m-portlet__body">
                <input type="hidden" id="customer_id" name="customer_id" value="{{$item['customer_id']}}">

                <div class="row">
                    <div class="col-lg-10">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-4">
                                @if(isset($oldContract['customer_contract_origin']))
                                    <div class="form-group m-form__group">
                                        <label class="black-title">
                                            {{__('Mã hợp đồng được gia hạn')}}:
                                        </label>
                                        <div class="input-group">
                                            <div class="m-input-icon m-input-icon--right">
                                                <input type="text" id="full_name" name="full_name"
                                                       class="form-control m-input"
                                                       disabled
                                                       value="{{$oldContract['customer_contract_origin']}}">
                                            </div>

                                        </div>
                                    </div>
                                @endif
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Mã hợp đồng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['customer_contract_code']}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Mã gói')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['product_code']}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Tên gói')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['product_name_vi']}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Số lượng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['quantity']}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Giá trị gói')}} (vnđ):
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{ number_format($item['price_standard'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Lãi suất')}} (%):
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{ $item['interest_rate'] }}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Kì hạn đầu tư')}} ({{__("tháng")}}):
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['investment_time'] != null ? $item['investment_time'] : 'N/A'}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Kì hạn rút lãi')}} ({{__("tháng")}}):
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['withdraw_interest_time'] != null ? $item['withdraw_interest_time'] : 'N/A'}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Nhân viên hỗ trợ')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['staff_full_name']}}">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-4">
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Tên khách hàng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['customer_name']}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('SĐT khách hàng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['customer_phone']}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Địa chỉ khách hàng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            @if($item['residence_address'] == null && $item['lienhe_dis_name'] == null && $item['lienhe_pro_name'] == null)
                                                <input type="text" id="full_name" name="full_name"
                                                       class="form-control m-input"
                                                       disabled
                                                       value="{{$item['customer_residence_address']}}">
                                            @else
                                                <input class="form-control"
                                                       id="full_name"
                                                       name="full_name"
                                                       value="{{$item['residence_address']}}, {{$item['lienhe_dis_type']}}  {{$item['lienhe_dis_name']}},{{$item['lienhe_pro_type']}} {{$item['lienhe_pro_name']}}" disabled>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Ngày bắt đầu hợp đồng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['customer_contract_start_date'] != null ? date("d-m-Y",strtotime($item['customer_contract_start_date'])) : 'N/A'}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Ngày kết thúc hợp đồng')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['customer_contract_end_date'] != null && $item['customer_contract_end_date'] != 0  ? date("d-m-Y",strtotime($item['customer_contract_end_date'])) : 'N/A'}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>
                                        {{__('Trạng thái')}}:
                                    </label>
                                    <div class="input-group">
                                        @if($item['customer_contract_end_date'] != null && $item['customer_contract_end_date'] != '')
                                            @if(\Carbon\Carbon::now() < \Carbon\Carbon::parse($item['customer_contract_end_date']) && $item['is_active'] == 1)
                                                <span class="text-success">
                                                    @lang('Đang hoạt động')
                                                </span>
                                            @else
                                                <span class="text-danger">
                                                    @lang('Đã hết hạn')
                                                </span>
                                            @endif
                                        @else
                                            @if($item['is_active'] == 1)
                                                <span class="text-success">
                                                    @lang('Đang hoạt động')
                                                </span>
                                            @else
                                                <span class="text-danger">
                                                    @lang('Đã hết hạn')
                                                </span>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Ngày nhận lãi')}}:
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{$item['withdraw_date_planning'] != null ? date("d-m-Y",strtotime($item['withdraw_date_planning'])) : \Carbon\Carbon::parse($item['customer_contract_start_date'])->addMonth($item['withdraw_interest_time'])->format('d-m-Y')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="black-title">
                                        {{__('Tổng tiền thanh toán')}} (vnđ):
                                    </label>
                                    <div class="input-group">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" id="full_name" name="full_name"
                                                   class="form-control m-input"
                                                   disabled
                                                   value="{{ number_format($item['total'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>
                                        {{__('Hợp đồng được gia hạn')}}:
                                    </label>
                                    <div class="input-group">
                                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                                <input type="checkbox" {{$item['is_fully_withdraw'] == 1 ? 'checked' : ''}} class="manager-btn" name="" disabled="">
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row clearfix">
                            <div class="table-responsive">
                                <table class="table table-striped m-table m-table--head-bg-default">
                                    <thead class="bg">
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('Số Serial')}}</th>
                                        <th>{{__('Hình mặt trước của gói')}}</th>
                                        <th>{{__('Hình mặt sau của gói')}}</th>
                                        <th>{{__('Giá trị gói (vnđ)')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody style="font-size: 13px">

                                        {{--            {{dd($LIST)}}--}}
                                        @for($i =0 ;$i<intval($item['quantity']);$i++)
                                            <tr>
                                                <td class="text_middle">{{ $i+1}}</td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="m-input-icon m-input-icon--right">
                                                            <input type="hidden" name="customer_contract_serial_id_{{$i}}"
                                                                   class="form-control m-input product_serial_no"
                                                                   value="{{(isset($serial[$i]) && $serial[$i] !=null) ? $serial[$i]["customer_contract_serial_id"] :""}}">
                                                            <input type="text" name="product_serial_no_{{$i}}"
                                                                   class="form-control m-input product_serial_no"
                                                                   value="{{(isset($serial[$i]) && $serial[$i] !=null) ? $serial[$i]["product_serial_no"] :""}}">
                                                        </div>

                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group m-widget19">
                                                        <input type="hidden" class="img_front_hidden_{{$i}}" name="img_front_hidden_{{$i}}" value="{{isset($serial[$i]) ? asset($serial[$i]["product_serial_front"]) : url("/")."/assets/images/ic_front.png"}}">
                                                        <input type="hidden" class="img_front_{{$i}}" name="img_front_{{$i}}" value="">
                                                        <div class="m-widget19__pic">
                                                            <img class="m--bg-metal m-image  img-sd blah_front_{{$i}}"
                                                                 src="{{(isset($serial[$i]) && $serial[$i] !=null) ? asset($serial[$i]["product_serial_front"]) :url("/")."/assets/images/ic_front.png"}}"
                                                                 alt="Hình ảnh" width="220px" height="220px">
                                                        </div>
                                                        <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                               data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                                               id="getFile_front_{{$i}}" type="file" onchange="uploadFrontImage(this,{{$i}});" class="form-control"
                                                               style="display:none">
                                                        <div class="m-widget19__action" style="max-width: 155px">
                                                            <a href="javascript:void(0)" onclick="document.getElementById('getFile_front_'+{{$i}}).click()"
                                                               class="btn btn-sm m-btn--icon color w-100">
                                                                <span class="m--margin-left-20">
                                                                <i class="fa fa-camera"></i>
                                                                <span>
                                                                {{__('Tải ảnh lên')}}
                                                                </span>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group m-widget19">
                                                        <input type="hidden" class="img_back_hidden_{{$i}}" name="img_back_hidden_{{$i}}" value="{{isset($serial[$i]) ? asset($serial[$i]["product_serial_back"]) : url("/")."/assets/images/ic_front.png"}}">
                                                        <input type="hidden" class="img_back_{{$i}}" name="img_back_{{$i}}" value="">
                                                        <div class="m-widget19__pic">
                                                            <img class="m--bg-metal m-image  img-sd blah_back_{{$i}}"
                                                                 src="{{ (isset($serial[$i]) && $serial[$i] !=null) ? asset($serial[$i]["product_serial_back"]) :url("/") . "/assets/images/ic_back.png"}}"
                                                                 alt="Hình ảnh" width="220px" height="220px">
                                                        </div>
                                                        <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                               data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                                               id="getFile_back_{{$i}}" type="file" onchange="uploadBackImage(this,{{$i}});" class="form-control"
                                                               style="display:none">
                                                        <div class="m-widget19__action" style="max-width: 155px">
                                                            <a href="javascript:void(0)" onclick="document.getElementById('getFile_back_'+{{$i}}).click()"
                                                               class="btn btn-sm m-btn--icon color w-100">
                                                                <span class="m--margin-left-20">
                                                                <i class="fa fa-camera"></i>
                                                                <span>
                                                                {{__('Tải ảnh lên')}}
                                                                </span>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="m-input-icon m-input-icon--right">
                                                            <input type="text" name="product_serial_amount_{{$i}}"
                                                                   class="form-control m-input product_serial_amount"
                                                                   value="{{(isset($serial[$i]) && $serial[$i] !=null) ? number_format($serial[$i]["product_serial_amount"]) :""}}">
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label>{{__('File hợp đồng')}}:</label>
                                    <div class="m-dropzone dropzone dz-clickable"
                                         action="{{route('customer-contract.upload-dropzone')}}" id="dropzoneone">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 href="" class="m-dropzone__msg-title">
                                                {{__('File hợp đồng')}}
                                            </h3>
                                            <span>{{__('Chọn file hợp đồng')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" id="upload-image">
                                    @if(count($file) > 0)
                                        @foreach($file as $v)
                                            <div class="image-show-child col-12">
                                                <input type="hidden" name="img-sv" value="{{$v['image_file']}}">
                                                <input type="hidden" name="type" value="1">
                                                <a href="{{asset($v['image_file'])}}" target="_blank">
{{--                                                    <img class='m--bg-metal m-image img-sd' src='{{$v['image_file']}}'--}}
{{--                                                         alt='{{__('File hợp đồng')}}' width="100px"--}}
{{--                                                         height="100px">--}}
                                                    <p>{{asset($v['image_file'])}}</p>
                                                </a>
                                                <span class="delete-img-sv" style="display: block;">
                                                <a href="javascript:void(0)" onclick="removeImage(this)">
                                                    <i class="la la-close"></i>
                                                </a>
                                            </span>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.customer-contract')}}"
                           class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('HUỶ')}}</span>
						</span>
                        </a>
                        <button type="button" onclick="submitContract({{$item['quantity']}},{{$item['customer_contract_id']}},{{$item['product_id']}})"
                                class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit m--margin-left-10">
							<span>
							<i class="la la-edit"></i>
							<span>{{__('CẬP NHẬT')}}</span>
							</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script>

        // Shorthand for $( document ).ready()
        $(function () {
            $('.issued_date').datepicker({
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

        });

        new AutoNumeric.multiple('.product_serial_amount', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });

    </script>
    <script src="{{asset('static/backend/js/admin/customer/edit.js?v='.time())}}" type="text/javascript"></script>
    <script>
        dropzone();
    </script>
    <script type="text/template" id="imageShow">
        <div class="image-show-child col-12">
            <input type="hidden" name="img-sv" value="{link_hidden}">
            <input type="hidden" name="type" value="0">
{{--            <a href="{{asset('{link}')}}" target="_blank">--}}
{{--                <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('File hợp đồng')}}' width="100px"--}}
{{--                     height="100px">--}}
                <p>{{asset('{link}')}}</p>
{{--            </a>--}}

            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
        </div>
    </script>
@stop