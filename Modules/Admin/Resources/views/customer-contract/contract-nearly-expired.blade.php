@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ HỢP ĐỒNG')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
        .hidden-button .tdEndContract , .hidden-th {
            display: none;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH HỢP ĐỒNG')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

{{--                <a href="{{route('admin.customer-contract.add')}}"--}}
{{--                   class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">--}}
{{--                    <span>--}}
{{--                        <i class="fa fa-plus-circle"></i>--}}
{{--                        <span> {{__('THÊM HỢP ĐỒNG')}}</span>--}}
{{--                    </span>--}}
{{--                </a>--}}

            </div>

        </div>
        <div class="card-header tab-card-header ">
            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                {{--                <li class="nav-item">--}}
                {{--                    <a class="nav-link active show" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="All" aria-selected="true">Tất cả</a>--}}
                {{--                </li>--}}
                <li class="nav-item">
                    <a class="nav-link "  href="{{route('admin.customer-contract')}}" >Danh sách hợp đồng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.customer-contract.list-interest-approved')}}" >Danh sách lãi cần duyệt</a>
                </li>
                <li class="nav-item active show">
                    <a class="nav-link" href="{{route('admin.customer-contract.list-nearly-expired')}}" >Danh sách hợp đồng sắp hết hạn</a>
                </li>
            </ul>
        </div>
        <div class="m-portlet__body">
            <!-- seach -->
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       placeholder="{{__('Nhập mã hợp đồng hoặc tên nhà đầu tư')}}">
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-lg-3 form-group">--}}
{{--                        <select style="width: 100%" name="is_deleted"--}}
{{--                                class="form-control m-input ss--select-2">--}}
{{--                            <option value="">Chọn trạng thái</option>--}}
{{--                            <option value="0">Hoạt động</option>--}}
{{--                            <option value="1">Tạm ngưng</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%" name="product_category_id"
                                class="form-control m-input ss--select-2">
                            <option value="">Chọn loại gói</option>
                            <option value="2">Tiết kiệm</option>
                            <option value="1">Hợp tác đầu tư</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group" style="background-color: white">
                            <div class="m-input-icon m-input-icon--right">
                                <input readonly="" class="form-control m-input daterange-picker"
                                       id="created_at" name="created_at" autocomplete="off"
                                       placeholder="{{__('Ngày hợp đồng')}}">
                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                        <span><i class="la la-calendar"></i></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%" name="time_expired"
                                class="form-control m-input ss--select-2">
                            <option value="">Chọn thời gian</option>
                            <option selected value="3">3 tháng</option>
                            <option value="2">2 tháng</option>
                            <option value="1">1 tháng</option>
                        </select>
                    </div>
                    
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <button class="btn btn-primary color_button btn-search">
                                {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                            </button>
                            <a href="{{route('admin.customer-contract.list-nearly-expired')}}"
                               class="btn btn-metal  btn-search padding9x padding9px">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>


                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                    </div>
                @endif
            </form>

            <!--seach -->
            <div class="table-content m--padding-top-30 hidden-button">
                @include('admin::customer-contract.list-contract-nearly-expired')

            </div><!-- end table-content -->

        </div>
    </div>
@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/customer-contract/nearly-expired.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
{{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop