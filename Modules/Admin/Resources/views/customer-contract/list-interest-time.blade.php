<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">@lang('Thời điểm')</th>
        <th class="tr_thead_list_dt_cus">@lang('Tỉ lệ lãi suất thời điểm') (%)</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị lãi suất') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị lãi suất tích lũy') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị lãi suất có thể rút') (vnđ)</th>
        <th class="tr_thead_list_dt_cus">@lang('Ngày rút theo kế hoạch') (vnđ)</th>
    </tr>
    </thead>
    <tbody style="font-size: 13px">
    @if(isset($listTime))
        @foreach ($listTime as $k => $v)
            <tr>
                <td>
                    {{$v['interest_day'] .'/'. $v['interest_month'] .'/'. $v['interest_year'] .' '. $v['interest_time']}}
                </td>
{{--                <td>{{number_format($v['interest_rate_by_time'],7)}}</td>--}}
                <td>{{$v['interest_rate_by_time']}}</td>
                <td>
                    {{$v['interest_amount'] != null ? number_format($v['interest_amount'],3) : 0}}
                </td>
                <td>
                    {{$v['interest_total_amount'] != null ? number_format($v['interest_total_amount'],3) : 0}}
                </td>
                <td>
                    {{$v['interest_banlance_amount'] != null ? number_format($v['interest_banlance_amount'],3) : 0}}
                </td>
                <td>
                    {{$v['term_time_type'] == 0 ? 'N/A' : \Carbon\Carbon::parse($v['withdraw_date_planning'])->format('d/m/Y')}}
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ $listTime->links('helpers.paging') }}

