<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">@lang('Tên gói')</th>
        <th class="tr_thead_list_dt_cus">@lang('Mã serial')</th>
        <th class="tr_thead_list_dt_cus">@lang('Ngày phát hành')</th>
        <th class="tr_thead_list_dt_cus">@lang('Giá trị') (vnđ)</th>
    </tr>
    </thead>
    <tbody style="font-size: 13px">
    @if(isset($listSerial))
        @foreach ($listSerial as $k => $v)
            <tr>

                <td>{{$v['product_name']}}</td>
                <td>{{$v['product_serial_no']}}</td>
                <td>
                    {{\Carbon\Carbon::parse($v['issued_date'])->format('d/m/Y H:i')}}
                </td>
                <td>
                    {{number_format($v['product_serial_amount'] != null ? $v['product_serial_amount'] : 0)}}
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ $listSerial->links('helpers.paging') }}

