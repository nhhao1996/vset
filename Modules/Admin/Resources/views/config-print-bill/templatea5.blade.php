<style>
    .font-size-12 {
        font-size: 12px !important;
    }

    .font-size-13 {
        font-size: 13px !important;
    }

    .font-size-14 {
        font-size: 14px !important;
    }

    #divToPrint {
        width: 80% !important;
        margin: 0 auto;
    }

    .border-bottom {
        border-bottom: 1px dashed;
    }

    .hr2 {
        border-bottom: 1px solid;
        margin-bottom: 0.5rem;
    }

    .imgss {
        width: 70%;
        height: 70%;
    }
</style>

<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title ss--title m--font-bold">
            <i class="la la-print ss--icon-title m--margin-right-5"></i>
            {{__('MẪU IN HÓA ĐƠN')}}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <div id="divToPrint">
            <div class="receipt">
                <section class="sheet">
                    <div id="PrintArea">
                        <div class="widhtss">
                            <div class="row">
                                @if($configPrintBill['is_show_logo']==1)
                                    <div class="form-group m-form__group text-center col-lg-3">
                                        <img class="imgss" src="{{asset($spaInfo['logo'])}}">
                                    </div>
                                @endif
                                <div class="font-size-13 text-right col-lg-9" style="margin: 0 auto">
                                    @if($configPrintBill['is_show_unit']==1)
                                        <h5 class="text-center font-size-13">
                                            {{$spaInfo['name']}}
                                        </h5>
                                    @endif
                                    @if($configPrintBill['is_show_address']==1)
                                        <h5 class="text-center font-size-13">
                                        <span class="font-size-13 text-center">
                                            {{$spaInfo['address'].' '.$spaInfo['district_type'].' '
                                            .$spaInfo['district_name'].' '.$spaInfo['province_name']}}
                                        </span>
                                        </h5>
                                    @endif
                                    @if($configPrintBill['is_show_phone']==1)
                                        <h4 class="text-center font-size-13">
                                        <span>
                                           {{$spaInfo['phone']}}
                                       </span>
                                        </h4>
                                    @endif
                                </div>

                            </div>
                            <div class="hr2"></div>
                            <div class="mm-mauto">
                                <h4 class="text-center font-size-14">{{__('HÓA ĐƠN BÁN HÀNG')}}</h4>
                                @if($configPrintBill['is_show_order_code']==1)
                                    <div class="roww text-left">
                                        <div>
                                            <h5 class="font-size-13">HĐ: {{date('dmyY'.'001')}}
                                            </h5>
                                        </div>
                                        {{--<div>--}}
                                        {{--<h5 class="ss-font-size-10">Ngày in: {{date("d/m/Y H:m:i")}}--}}
                                        {{--</h5>--}}
                                        {{--</div>--}}
                                    </div>
                                @endif
                            </div>
                            <div class="hr2"></div>
                            <div class="mm-mauto tientong roww">
                                @if($configPrintBill['is_show_customer']==1)
                                    <strong class="font-size-13">
                                        {{__('KHÁCH HÀNG')}}: {{__('Khách hàng vãng lai')}}
                                    </strong>
                                @endif
                                @if($configPrintBill['is_show_cashier']==1)
                                    <strong class="font-size-13" style="float:right;">{{__('Thu ngân')}}:
                                        <strong class="ss-font-size-10">Nguyễn Văn An</strong>
                                    </strong>
                                @endif
                            </div>
                            <div class="mm-mauto">
                                <strong class="font-size-13">
                                    @if($configPrintBill['is_show_datetime']==1)
                                        {{date("d/m/Y H:i:s")}}
                                    @endif
                                </strong>
                            </div>
                            @if($configPrintBill['is_show_order_code']==1||$configPrintBill['is_show_datetime']==1||$configPrintBill['is_show_customer']==1||$configPrintBill['is_show_cashier']==1)
                                <div class="hr2"></div>
                            @endif
                            <div class="tientong row font-size-13" style="font-weight: bold;">
                                <span class="col-lg-7">{{__('Tên SP/DV')}}</span>
                                <span class="col-lg-5 ss--text-right">{{__('Tổng tiền')}}</span>
                            </div>
                            <div class="tientong row font-size-12">
                                <span class="col-lg-7">{{__('Đắp mặt nạ')}}</span>
                                <span class="col-lg-5 ss--text-right"></span>
                            </div>
                            <div class="tientong row font-size-12">
                                <span class="col-lg-7">(1x200,000)</span>
                                <span class="col-lg-5 ss--text-right">200,000</span>
                            </div>
                            <div class="border-bottom"></div>
                            <div class="tientong row font-size-12">
                                <span class="col-lg-7">{{__('Massage mặt')}}</span>
                                <span class="col-lg-5 ss--text-right"></span>
                            </div>
                            <div class="tientong row font-size-12">
                                <span class="col-lg-7">(1x250,000)</span>
                                <span class="col-lg-5 ss--text-right">250,000</span>
                            </div>
                            <div class="tientong row font-size-12">
                                <span class="col-lg-7">{{__('Giảm giá')}}</span>
                                <span class="col-lg-5 ss--text-right">-50,000</span>
                            </div>
                            <div class="hr2"></div>
                            <div class="mm-mauto tientong font-size-15 row">
                                <strong class="col-lg-9 font-size-15">{{__('TỔNG TIỀN ĐÃ GIẢM')}}:</strong>
                                <strong class="col-lg-3 font-size-15 ss--text-right">-50,000</strong>
                            </div>
                            <div class="mm-mauto tientong font-size-15 row">
                                <strong class="col-lg-9 font-size-15">{{__('TỔNG TIỀN PHẢI T.TOÁN')}}:</strong>
                                <strong class="col-lg-3 font-size-15 ss--text-right">400,000</strong>
                            </div>
                            <div class="mm-mauto tientong font-size-15 row">
                                <strong class="col-lg-9 font-size-15">{{__('TIỀN MẶT')}}:</strong>
                                <strong class="col-lg-3 font-size-15 ss--text-right">400,000</strong>
                            </div>
                            <div class="hr2"></div>
                            @if($configPrintBill['is_show_footer']==1)
                                <div class="mm-mauto text-center tks">
                                    <strong class="ss-nowap">{{__('CẢM ƠN QUÝ KHÁCH VÀ HẸN GẶP LẠI')}}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
            <div class="m-form__actions m--align-right">
                <button data-dismiss="modal"
                        class="ss--btn-mobiles m--margin-bottom-5 btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn">
                    <span class="ss--text-btn-mobi">
                    <i class="la la-arrow-left"></i>
                    <span>{{__('HỦY')}}</span>
                    </span>
                </button>
                {{--<button type="button" onclick="customerGroup.addClose()"--}}
                {{--class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">--}}
                {{--<span class="ss--text-btn-mobi">--}}
                {{--<i class="la la-check"></i>--}}
                {{--<span>{{__('LƯU THÔNG TIN')}}</span>--}}
                {{--</span>--}}
                {{--</button>--}}
                {{--<button type="button" onclick="customerGroup.add()"--}}
                {{--class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">--}}
                {{--<span class="ss--text-btn-mobi">--}}
                {{--<i class="fa fa-plus-circle ss--icon-title m--margin-left-10"></i>--}}
                {{--<span>{{__('LƯU & TẠO MỚI')}}</span>--}}
                {{--</span>--}}
                {{--</button>--}}
            </div>
        </div>
    </div>
</div>
