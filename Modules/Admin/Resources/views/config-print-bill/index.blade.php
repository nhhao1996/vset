@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-services.png')}}" alt=""
                style="height: 20px;"> {{__('CẤU HÌNH')}}</span>
@endsection
@section("after_css")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                                <i class="la la-cog"></i>
                             </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CẤU HÌNH IN HÓA ĐƠN')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
        </div>
        {!! Form::open(["route"=>"admin.config-print-bill.submitEdit","method"=>"POST","id"=>"form", 'class' => 'm-form--group-seperator-dashed ']) !!}
        <div class="m-portlet__body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label for="">{{__('Kích thước')}}</label>
                        <div class="">
                            <select name="template" id="template" class="form-control class-select2" style="width: 100%">
                                <option {{$configPrintBill->template=='k80'?'selected':''}} value="k80">K80</option>
                                <option {{$configPrintBill->template=='k58'?'selected':''}} value="k58">K58</option>
                                <option {{$configPrintBill->template=='A5'?'selected':''}} value="A5">A5</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Số liên in')}}</label>
                        <div class="">
                            <select name="printed_sheet" id="printed_sheet" class="form-control class-select2" style="width: 100%">
                                <option {{$configPrintBill->printed_sheet==1?'selected':''}} value="1">1</option>
                                <option {{$configPrintBill->printed_sheet==2?'selected':''}} value="2">2</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="">{{__('In lại')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input id="is_print_reply" {{$configPrintBill->is_print_reply==1?'checked':''}} type="checkbox" class="manager-btn" name="is_print_reply">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Số lần in tối đa')}}</label>
                        <input value="{{$configPrintBill->print_time}}" name="print_time" id="print_time" type="text" class="class-number form-control">
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện logo')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_logo==1?'checked':''}} id="is_show_logo" type="checkbox" class="manager-btn" name="is_show_logo">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện tên đơn vị/ cty')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_unit==1?'checked':''}} id="is_show_unit" type="checkbox" class="manager-btn" name="is_show_unit">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện địa chỉ đơn vị/ cty')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_address==1?'checked':''}} id="is_show_address" type="checkbox" class="manager-btn" name="is_show_address">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện nhân viên thu ngân')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_cashier==1?'checked':''}} id="is_show_cashier" type="checkbox" class="manager-btn" name="is_show_cashier">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện khách hàng')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_customer==1?'checked':''}} id="is_show_customer" type="checkbox" class="manager-btn" name="is_show_customer">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện thời gian in')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_datetime==1?'checked':''}} id="is_show_datetime" type="checkbox" class="manager-btn" name="is_show_datetime">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện mã hóa đơn')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_order_code==1?'checked':''}} id="is_show_order_code" type="checkbox" class="manager-btn" name="is_show_order_code">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện SĐT')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_phone==1?'checked':''}} id="is_show_phone" type="checkbox" class="manager-btn" name="is_show_phone">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Hiện footer')}}:</label>
                        <div class="input-group row">
                            <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input {{$configPrintBill->is_show_footer==1?'checked':''}} id="is_show_footer" type="checkbox" class="manager-btn" name="is_show_footer">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-lg-4 m--margin-top-5">
                                <i>{{__('Chọn để kích hoạt')}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Ký hiệu')}}:</label>
                        <input value="{{$configPrintBill->symbol}}" name="symbol" id="symbol" type="text" class="class-number form-control">
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">{{__('Mã số thuế')}}:</label>
                        <input value="{{$spaInfo->tax_code}}" name="tax_code" id="tax_code" type="number" class="class-number form-control" onkeyup="Config.onKeyDownInputNumber(this)">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer m--margin-right-20">
            <div class="form-group m-form__group m--margin-top-10">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a data-toggle="modal"
                           data-target="#modalAdd"
                           href="javascript:void(0)"
                           class="ss--btn-mobiles btn-save btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-eye"></i>
                                <span>{{__('XEM TRƯỚC')}}</span>
                            </span>
                        </a>
                        <button type="submit"
                                class="ss--btn-mobiles save-change btn-save btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                                <span class="ss--text-btn-mobi">
                                            <i class="la la-check"></i>
                                            <span>{{__('CẬP NHẬT THÔNG TIN')}}</span>
                                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="modal fade" id="modalAdd" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <!-- Modal content-->
            @include('admin::config-print-bill.templatea5')
        </div>
    </div>
@endsection

@section('after_script')
    <script src="{{asset('static/backend/js/admin/general/jquery.printPage.js?v='.time())}}" type="text/javascript"></script>

    <script src="{{asset('static/backend/js/admin/config-print-bill/config-print-bill.js')}}"
            type="text/javascript"></script>

    @if(Session::has("statusss"))
        <script>
            $.getJSON(laroute.route('translate'), function (json) {
            swal(json["Cấu hình in hóa đơn thành công"], "", "success");
            });
        </script>
    @endif
@endsection
