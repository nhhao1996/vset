@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <style>
        .note-editor {
            width: 100%;
        }
    </style>
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ GÓI')}}
    </span>
@endsection
@section('content')

        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                        <h3 class="m-portlet__head-text">
                            {{__('CHI TIẾT GÓI')}}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
{{--                    <div onmouseover="product.onMouseOverAddNew()" onmouseout="product.onMouseOutAddNew()"--}}
{{--                                                   class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--open btn-hover-add-new"--}}
{{--                                                   m-dropdown-toggle="hover" aria-expanded="true">--}}
{{--                        <a href="#"--}}
{{--                           class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">--}}
{{--                            <i class="la la-plus m--hide"></i>--}}
{{--                            <i class="la la-ellipsis-h"></i>--}}
{{--                        </a>--}}
{{--                        <div class="m-dropdown__wrapper dropdow-add-new" style="z-index: 101;display: none">--}}
{{--                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"--}}
{{--                                  style="left: auto; right: 21.5px;"></span>--}}
{{--                            <div class="m-dropdown__inner">--}}
{{--                                <div class="m-dropdown__body">--}}
{{--                                    <div class="m-dropdown__content">--}}
{{--                                        <ul class="m-nav">--}}
{{--                                            <li class="m-nav__item">--}}
{{--                                                <a data-toggle="modal"--}}
{{--                                                   data-target="#product_bonus_popup" href="" class="m-nav__link">--}}
{{--                                                    <i class="m-nav__link-icon la la-usd"></i>--}}
{{--                                                    <span class="m-nav__link-text">{{__('Thêm tiền thưởng')}} </span>--}}
{{--                                                </a>--}}
{{--                                                <a data-toggle="modal"--}}
{{--                                                   data-target="#product_interest_popup" href="" class="m-nav__link">--}}
{{--                                                    <i class="m-nav__link-icon la la-money"></i>--}}
{{--                                                    <span class="m-nav__link-text">{{__('Thêm lãi suất')}} </span>--}}
{{--                                                </a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="m-portlet__body">
                <form id="form-product" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>{{__('Danh mục')}}:<b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <select style="width: 100%" id="product_category_id" name="product_category_id" class="form-control product_category_id m_selectpicker"
                                            title="{{__('Chọn danh mục')}}">
                                        <option value="">{{__('Chọn danh mục')}}</option>
                                        @foreach($category as $key=>$value)
                                            <option value="{{$value['product_category_id']}}" {{$detail['product_category_id'] == $value['product_category_id'] ? "selected" :""}}>{{$value['category_name_vi']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <span class="errs error-category"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Mã gói')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="product-code" disabled type="text" class="form-control m-input class"
                                           placeholder="{{__('Mã gói')}}"
                                           aria-describedby="basic-addon1" value="{{$detail['product_code']}}">
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tên gói VI')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="product-name-vi" name="product_name_vi" type="text" class="form-control m-input class"
                                           placeholder="{{__('Tên gói VI')}}"
                                           aria-describedby="basic-addon1" value="{{$detail['product_name_vi']}}">
                                </div>
                                <span class="errs error-product-name-vi"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tên gói EN')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="product-name-en" name="product_name_en" type="text" class="form-control m-input class"
                                           placeholder="{{__('Tên gói EN')}}"
                                           aria-describedby="basic-addon1" value="{{$detail['product_name_en']}}">
                                </div>
                                <span class="errs error-product-name-en"></span>
                            </div>
                        </div>
                        <div class="col-lg-6 d-none">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Mô tả ngắn VI')}}:
                                </label>
                                <div class="input-group">
                                <textarea id="description-vi" name="product_short_name_vi" type="text" class="form-control m-input class summernote-fix"
                                          placeholder="{{__('Mô tả ngắn VI')}}"
                                          aria-describedby="basic-addon1">{!! $detail['product_short_name_vi'] !!}</textarea>
                                </div>
                                <span class="errs error-product-short-name-vi"></span>
                            </div>
                        </div>
                        <div class="col-lg-6 d-none">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Mô tả ngắn EN')}}:
                                </label>
                                <div class="input-group">
                                <textarea id="description-vi" name="product_short_name_en" type="text" class="form-control m-input class summernote-fix"
                                          placeholder="{{__('Mô tả ngắn EN')}}"
                                          aria-describedby="basic-addon1">{!! $detail['product_short_name_en'] !!}</textarea>
                                </div>
                                <span class="errs error-product-short-name-en"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Mô tả VI')}}:
                                </label>
                                <div class="input-group">
                                <textarea id="description-vi" name="description_vi" type="text" class="form-control m-input class summernote-fix"
                                          placeholder="{{__('Mô tả Vi')}}"
                                          aria-describedby="basic-addon1">{!! $detail['description_vi'] !!}</textarea>
                                </div>
                                <span class="errs error-description-vi"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Mô tả EN')}}:
                                </label>
                                <div class="input-group">
                                <textarea id="description-en" name="description_en" type="text" class="form-control m-input class summernote-fix"
                                          placeholder="{{__('Mô tả EN')}}"
                                          aria-describedby="basic-addon1">{!! $detail['description_en'] !!}</textarea>
                                </div>
                                <span class="errs error-description-en"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Giá trị gói (Vnđ) ')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="price-standard" name="price_standard" type="text" class="form-control m-input class number-money-fix"
                                           placeholder="{{__('Giá trị gói')}}"
                                           aria-describedby="basic-addon1" value="{{number_format($detail['price_standard'], 0) }}">
                                    <input type="hidden" class="price_standard_hidden" value="{{$detail['price_standard']}}">
                                </div>
                                <span class="errs error-price-standard"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tỉ lệ lãi suất chuẩn (%/năm) ')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="interest-rate-standard" name="interest_rate_standard" type="text" class="form-control m-input class"
                                           placeholder="{{__('Tỉ lệ lãi suất chuẩn')}}"
                                           aria-describedby="basic-addon1" value="{{number_format($detail['interest_rate_standard'], 2) }}">
                                </div>
                                <span class="errs error-interest-rate-standard"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tỉ lệ phí rút tiền trước kỳ hạn (%) ')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="withdraw-fee-rate-before" name="withdraw_fee_rate_before" type="text" class="form-control m-input class"
                                           placeholder="{{__('Tỉ lệ phí rút tiền trước kỳ hạn')}}"
                                           aria-describedby="basic-addon1" value="{{number_format($detail['withdraw_fee_rate_before'], 2) }}">
                                </div>
                                <span class="errs error-withdraw-fee-rate-before"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tỉ lệ phí rút tiền đúng kỳ hạn (%) ')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="withdraw-fee-rate-ok" name="withdraw_fee_rate_ok" type="text" class="form-control m-input class"
                                           placeholder="{{__('Tỉ lệ phí rút tiền đúng kỳ hạn')}}"
                                           aria-describedby="basic-addon1" value="{{number_format($detail['withdraw_fee_rate_ok'], 2) }}">
                                </div>
                                <span class="errs error-withdraw-fee-rate-ok"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tỉ lệ phí rút tiền lãi (%) ')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="withdraw-fee-interest-rate" name="withdraw_fee_interest_rate" type="text" class="form-control m-input class"
                                           placeholder="{{__('Tỉ lệ phí rút tiền lãi')}}"
                                           aria-describedby="basic-addon1" value="{{number_format($detail['withdraw_fee_interest_rate'], 2) }}">
                                </div>
                                <span class="errs error-withdraw-fee-interest-rate"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Số tiền tối thiểu có thể rút lãi (Vnđ) ')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="withdraw-min-amount" name="withdraw_min_amount" type="text" class="form-control m-input class number-money-fix"
                                           placeholder="{{__('Số tiền tối thiểu có thể rút lãi')}}"
                                           aria-describedby="basic-addon1" value="{{number_format($detail['withdraw_min_amount'], 0) }}">
                                </div>
                                <span class="errs error-withdraw-min-amount"></span>
                            </div>
                        </div>
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="form-group m-form__group">--}}
{{--                                <label>--}}
{{--                                    {{__('Giá trị thấp nhất có thể bán (Vnđ) ')}}: <b class="text-danger">*</b>--}}
{{--                                </label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <input id="min-allow-sale" name="min_allow_sale" type="text" class="form-control m-input class number-money-fix"--}}
{{--                                           placeholder="{{__('Giá trị thấp nhất có thể bán')}}"--}}
{{--                                           aria-describedby="basic-addon1" value="{{number_format($detail['min_allow_sale'], 0) }}">--}}
{{--                                </div>--}}
{{--                                <span class="errs error-min-allow-sale"></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Ngày phát hành')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="min-allow-sale" name="published_at" type="text" class="published_at form-control m-input class"
                                           placeholder="{{__('Ngày phát hành')}}"
                                           aria-describedby="basic-addon1" value="{{\Carbon\Carbon::parse($detail['published_at'])->format('d/m/Y')}}">
                                </div>
                                <span class="errs error-min-allow-sale"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Thưởng khi gia hạn hợp đồng')}} (Vnđ) : <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="withdraw-min-amount" name="bonus_extend" type="text" class="form-control m-input class number-money-fix"
                                           placeholder="{{__('Thưởng khi gia hạn hợp đồng')}}"
                                           value="{{number_format($detail['bonus_extend'], 0) }}"
                                           aria-describedby="basic-addon1">
                                </div>
                                <span class="errs error-bonus_extend"></span>
                            </div>
                        </div>
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="form-group m-form__group" id="month_extend">--}}
{{--                                <label>--}}
{{--                                    {{__('Thời gian gia hạn hợp đồng trước ngày hết hạn hợp đồng')}}:--}}
{{--                                </label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <select class="form-control"  name="month_extend">--}}
{{--                                        <option value="0">{{__('Chọn tháng')}}</option>--}}
{{--                                        @for($i = 1 ; $i <= 12 ; $i ++)--}}
{{--                                            <option value="{{$i}}" {{$detail['month_extend'] == $i ? 'selected' :''}}>{{$i}} {{__('tháng')}}</option>--}}
{{--                                        @endfor--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <span class="errs error-month_extend"></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="form-group m-form__group">--}}
{{--                                <label>--}}
{{--                                    {{__('Số tiền thưởng tối thiểu có thể rút')}} (Vnđ) : <b class="text-danger">*</b>--}}
{{--                                </label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <input id="withraw-min-bonus" name="withraw_min_bonus" type="text" class="form-control m-input class number-money-fix"--}}
{{--                                           placeholder="{{__('Số tiền thưởng tối thiểu có thể rút')}}"--}}
{{--                                           value="{{number_format($detail['withraw_min_bonus'], 0) }}"--}}
{{--                                           aria-describedby="basic-addon1">--}}
{{--                                </div>--}}
{{--                                <span class="errs error-withraw_min_bonus"></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-lg-6">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Trạng thái')}}:
                                </label>
                                <div class="input-group">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label style="margin: 0 0 0 10px; padding-top: 4px">
                                    <input type="checkbox" id="is_actived" class="manager-btn" {{$detail['is_actived'] == 1 ? 'checked' : ''}}>
                                    <span></span>
                                </label>
                            </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group" id="withdraw_min_time">
                                <label>
                                    {{__('Thời gian tối thiểu có thể rút gốc')}}:
                                </label>
                                <div class="input-group">
                                    <select class="form-control" name="withdraw_min_time">
                                        <option value="">{{__('Chọn thời gian tối thiểu có thể rút gốc')}}</option>
                                        @for($i = 1 ; $i <= 12 ; $i ++)
                                            <option value="{{$i}}" {{$detail['withdraw_min_time'] == $i ? 'selected' : ''}}>{{$i}} {{__('tháng')}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <span class="errs error-min-allow-sale"></span>
                            </div>
                        </div>
{{--                        <div class="col-lg-6"></div>--}}
{{--                        ---------------------------------------------------------------------------------------------------------------------------}}
                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình đại diện gói (VI)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_main_hidden_vi" >
                                    <input type="hidden" id="image_main_upload_vi" name="product_avatar_vi"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_main_vi"
                                             src="{{$detail['product_avatar_vi'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_avatar_vi']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình chi tiết gói (VI)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_detail_hidden_vi">
                                    <input type="hidden" id="image_detail_upload_vi" name="product_image_detail_vi"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_detail_vi"
                                             src="{{$detail['product_image_detail_vi'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_image_detail_vi']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình đại diện gói (mobile) (VI)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_main_hidden_mobile_vi" >
                                    <input type="hidden" id="image_main_upload_mobile_vi" name="product_avatar_mobile_vi"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_main_mobile_vi"
                                             src="{{$detail['product_avatar_mobile_vi'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_avatar_mobile_vi']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình chi tiết gói (mobile) (VI)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_detail_hidden_mobile_vi">
                                    <input type="hidden" id="image_detail_upload_mobile_vi" name="product_image_detail_mobile_vi"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_detail_mobile_vi"
                                             src="{{$detail['product_image_detail_mobile_vi'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_image_detail_mobile_vi']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>
{{--                        ---------------------------------------------------------------------------------------------------------------------------}}
                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình đại diện gói (EN)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_main_hidden_en" >
                                    <input type="hidden" id="image_main_upload_en" name="product_avatar_en"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_main_en"
                                             src="{{$detail['product_avatar_vi'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_avatar_vi']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình chi tiết gói (EN)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_detail_hidden_en">
                                    <input type="hidden" id="image_detail_upload_en" name="product_image_detail_en"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_detail_en"
                                             src="{{$detail['product_image_detail_en'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_image_detail_en']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình đại diện gói (mobile) (EN)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_main_hidden_mobile_en" >
                                    <input type="hidden" id="image_main_upload_mobile_en" name="product_avatar_mobile_en"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_main_mobile_en"
                                             src="{{$detail['product_avatar_mobile_en'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_avatar_mobile_en']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Hình chi tiết gói (mobile) (EN)')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_detail_hidden_mobile_en">
                                    <input type="hidden" id="image_detail_upload_mobile_en" name="product_image_detail_mobile_en"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_detail_mobile_en"
                                             src="{{$detail['product_image_detail_mobile_en'] == null ? asset('/static/backend/images/no-image-product.png') : $detail['product_image_detail_mobile_en']}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>
{{--                        ---------------------------------------------------------------------------------------------------------------------------}}

{{--                        <div class="col-6">--}}
{{--                            <div class="form-group m-form__group ">--}}
{{--                                <label>{{__('Ảnh gói hợp đồng')}}:</label>--}}
{{--                                <div class="m-dropzone dropzone dz-clickable"--}}
{{--                                     action="{{route('customer-contract.upload-dropzone')}}" id="dropzoneone">--}}
{{--                                    <div class="m-dropzone__msg dz-message needsclick">--}}
{{--                                        <h3 href="" class="m-dropzone__msg-title">--}}
{{--                                            {{__('Ảnh gói hợp đồng')}}--}}
{{--                                        </h3>--}}
{{--                                        <span>{{__('Chọn ảnh gói hợp đồng')}}</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group m-form__group row" id="upload-image">--}}
{{--                                @if( isset($imageContract) && count($imageContract) > 0)--}}
{{--                                    @foreach($imageContract as $v)--}}
{{--                                        <div class="image-show-child mb-0 col-12">--}}
{{--                                            <input type="hidden" name="img-sv" value="{{$v['name']}}">--}}
{{--                                            <input type="hidden" name="type" value="1">--}}
{{--                                            <a href="{{asset($v['name'])}}" target="_blank">--}}
{{--                                                <p>{{asset($v['name'])}}</p>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="col-6">--}}
{{--                            <div class="form-group m-form__group ">--}}
{{--                                <label>{{__('Ảnh gói hợp đồng (mobile)')}}:</label>--}}
{{--                                <div class="m-dropzone dropzone dz-clickable"--}}
{{--                                     action="{{route('customer-contract.upload-dropzone')}}" id="dropzoneoneimage">--}}
{{--                                    <div class="m-dropzone__msg dz-message needsclick">--}}
{{--                                        <h3 href="" class="m-dropzone__msg-title">--}}
{{--                                            {{__('Ảnh gói hợp đồng (mobile)')}}--}}
{{--                                        </h3>--}}
{{--                                        <span>{{__('Chọn ảnh gói hợp đồng (mobile)')}}</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group m-form__group row" id="upload-image-mobile">--}}
{{--                                @if( isset($imageContractMobile) && count($imageContractMobile) > 0)--}}
{{--                                    @foreach($imageContractMobile as $v)--}}
{{--                                        <div class="image-show-child-mobile col-12">--}}
{{--                                            <input type="hidden" name="img-sv" value="{{$v['name']}}">--}}
{{--                                            <input type="hidden" name="type" value="1">--}}
{{--                                            <a href="{{asset($v['name'])}}" target="_blank">--}}
{{--                                                --}}{{--                                                    <img class='m--bg-metal m-image img-sd' src='{{$v['image_file']}}'--}}
{{--                                                --}}{{--                                                         alt='{{__('File hợp đồng')}}' width="100px"--}}
{{--                                                --}}{{--                                                         height="100px">--}}
{{--                                                <p>{{asset($v['name'])}}</p>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}


                    </div>
                    <input type="hidden" id="product_id" name="product_id" value="{{$detail['product_id']}}">
                </form>
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.product')}}"
                           class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                        </a>
                        @if(in_array('admin.product.edit', session('routeList')))
                            <a href="{{route('admin.product.edit',['id' => $detail['product_id']])}}"
                               class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>{{__('CHỈNH SỬA GÓI')}}</span>
                                </span>
                            </a>
                        @endif
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#" data-target="#product_interest_tab">{{_('Cấu hình lãi suất')}}</a>
                        </li>
                        <li class="nav-item" >
                            <a class="nav-link" data-toggle="tab" href="#" data-target="#product_bonus_tab">{{_('Cấu hình tiền thưởng')}}</a>
                        </li>
                        <li class="nav-item" >
                            <a class="nav-link" data-toggle="tab" href="#" data-target="#service_bonus_tab">{{_('Cấu hình thưởng dịch vụ')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="product_interest_tab" role="tabpanel">
                            <div class="m-portlet__body pl-0 pt-0 table-responsive">
                                <button onclick="product.submitListInterestWithraw()" type="button"
                                        class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md ">
                                        <span class="ss--text-btn-mobi">
                                            <span>{{__('LƯU')}}</span>
                                        </span>
                                </button>
                                <table class="table table-striped m-table m-table--head-bg-default mt-4 investment-withdraw">
                                    <thead class="bg">
                                    <tr>
                                        @if($detail['product_category_id'] == 2)
                                            <td class="text-center">{{__('Loại kỳ hạn')}}</td>
                                        @endif
                                        <td class="text-center">{{__('Kì hạn đầu tư')}} (tháng)</td>
                                        <td class="text-center">{{__('Kì hạn rút lãi')}} (tháng)</td>
                                        <td class="text-center">{{__('Tỉ lệ hoa hồng')}} (%)</td>
                                        <td class="text-center">{{__('Tỉ lệ lãi suất')}} (%/{{__('năm')}})</td>
                                        <td class="text-center">{{__('Lãi suất tháng')}} (Vnđ)</td>
                                        <td class="text-center">{{__('Tổng lãi suất')}} (Vnđ)</td>
                                        <td class="text-center">{{__('Hiển thị chính')}}</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form id="list-investment-withdraw">
                                        <input type="hidden" class="disabled-form" id="product_id" name="product_id" value="{{$detail['product_id']}}">
                                        @if($detail['product_category_id'] == 2)
                                            @for($i = 1 ; $i <= 1; $i++)
                                                @foreach($listInvestmentTime as $itemInvestment)
                                                    @foreach($withdrawInterestTime as $itemWithraw)
                                                        @if($itemInvestment['investment_time_month'] >= $itemWithraw['withdraw_interest_month'])
                                                            <input type="hidden" class="disabled-form" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][product_interest_id]" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i])? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['product_interest_id'] :''}}">
                                                            <tr class="investment_time_{{$itemInvestment['investment_time_id']}}_{{$i}}">

                                                            <td class="text-center">
                                                                <p>{{$i == 1 ? 'Có kì hạn' : 'Không có kì hạn'}}</p>
                                                                <input type="hidden" class="disabled-form" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][term_time_type]" value="{{$i}}">
                                                            </td>

                                                            <td class="text-center">
                                                                {{$itemInvestment['investment_time_month']}}
                                                                <input type="hidden" class="disabled-form investment_time_month_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{$itemInvestment['investment_time_month']}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][investment_time]">
                                                            </td>
                                                            <td class="text-center">
                                                                {{$itemWithraw['withdraw_interest_month']}}
                                                                <input type="hidden" class="disabled-form withdraw_interest_month_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{$itemWithraw['withdraw_interest_month']}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][withdraw_interest]">
                                                            </td>
                                                            <td><input type="text" class="disabled-form form-control number-money commission_rate_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][commission_rate]" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['commission_rate'] : 3}}"></td>
                                                            <td>
                                                                <input type="text" class="disabled-form interest_rate_outfocus form-control number-money interest_rate_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][interest_rate]" onfocusout="product.calculateInterest({{$itemInvestment['investment_time_id']}},{{$itemWithraw['withdraw_interest_time_id']}},{{$i}})" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['interest_rate'] : $detail['interest_rate_standard']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p class="mb-0 number-money text_month_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}">{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['month_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']}}</p>
                                                                <input type="hidden" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][month_interest]" class="disabled-form month_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['month_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p class="mb-0 number-money text_total_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}">{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['total_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']*$itemInvestment['investment_time_month']}}</p>
                                                                <input type="hidden" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][total_interest]" class="disabled-form total_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['total_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']*$itemInvestment['investment_time_month']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                                                    <label style="margin: 0 0 0 10px; padding-top: 4px">
                                                                        <input type="checkbox" onchange="product.changDisplay({{$itemInvestment['investment_time_id']}},{{$itemWithraw['withdraw_interest_time_id']}},{{$i}})" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][is_default_display]" class="manager-btn is_default_display_check disabled-form is_default_display_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}"
                                                                                {{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) && $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['is_default_display'] == 1 ? 'checked' :''}}>
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            @endfor
                                            @for($i = 0 ; $i <= 0; $i++)
                                                @foreach($withdrawInterestTime as $key => $itemWithraw)
                                                    @if($key == 0)
                                                        <input type="hidden" class="disabled-form" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][product_interest_id]" value="{{isset($productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i])? $productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['product_interest_id'] :''}}">
                                                        <tr class="investment_time_{{$i}}">

                                                            <td class="text-center">
                                                                <p>{{$i == 1 ? 'Có kì hạn' : 'Không có kì hạn'}}</p>
                                                                <input type="hidden" class="disabled-form" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][term_time_type]" value="{{$i}}">
                                                            </td>

                                                            <td class="text-center">
                                                                N/A
                                                                <input type="hidden" class="disabled-form investment_time_month_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][investment_time]">
                                                            </td>
                                                            <td class="text-center">
{{--                                                                {{$itemWithraw['withdraw_interest_month']}}--}}
                                                                N/A
                                                                <input type="hidden" class="disabled-form withdraw_interest_month_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{$itemWithraw['withdraw_interest_month']}}" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][withdraw_interest]">
                                                            </td>
                                                            <td><input type="text" class="disabled-form form-control number-money commission_rate_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][commission_rate]" value="{{isset($productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['commission_rate'] : 3}}"></td>
                                                            <td><input type="text" class="disabled-form interest_rate_outfocus form-control number-money interest_rate_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][interest_rate]" onfocusout="product.calculateWithdraw({{$itemWithraw['withdraw_interest_time_id']}},{{$i}})" value="{{isset($productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['interest_rate'] : $detail['interest_rate_standard']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p class="mb-0 number-money text_month_interest_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}">{{isset($productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['month_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']}}</p>
                                                                <input type="hidden" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][month_interest]" class="disabled-form month_interest_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{isset($productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['month_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                N/A
                                                            </td>
                                                            <td class="text-center">
                                                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                                                    <label style="margin: 0 0 0 10px; padding-top: 4px">
                                                                        <input type="checkbox" name="product[{{$i}}][{{$itemWithraw['withdraw_interest_time_id']}}][is_default_display]" class="manager-btn is_default_display_check is_default_display_check_term disabled-form is_default_display_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}"
                                                                                {{isset($productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) && $productInterest['-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['is_default_display'] == 1 ? 'checked' :''}}>
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endfor
                                        @else
                                            @for($i = 1 ; $i <= 1; $i++)
                                                @foreach($listInvestmentTime as $itemInvestment)
                                                    @foreach($withdrawInterestTime as $itemWithraw)
                                                        <input type="hidden" class="disabled-form" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][product_interest_id]" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i])? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['product_interest_id'] :''}}">
                                                        <tr class="investment_time_{{$itemInvestment['investment_time_id']}}_{{$i}}">
                                                            <td class="text-center">
                                                                {{$itemInvestment['investment_time_month']}}
                                                                <input type="hidden" class="disabled-form investment_time_month_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{$itemInvestment['investment_time_month']}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][investment_time]">
                                                            </td>
                                                            <td class="text-center">
                                                                {{$itemWithraw['withdraw_interest_month']}}
                                                                <input type="hidden" class="disabled-form withdraw_interest_month_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{$itemWithraw['withdraw_interest_month']}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][withdraw_interest]">
                                                            </td>
                                                            <td><input type="text" class="disabled-form form-control number-money commission_rate_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][commission_rate]" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['commission_rate'] : 3}}"></td>
                                                            <td><input type="text" class="disabled-form interest_rate_outfocus form-control number-money interest_rate_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][interest_rate]" onfocusout="product.calculateInterest({{$itemInvestment['investment_time_id']}},{{$itemWithraw['withdraw_interest_time_id']}},{{$i}})" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['interest_rate'] : $detail['interest_rate_standard']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p class="mb-0 number-money text_month_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}">{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['month_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']}}</p>
                                                                <input type="hidden" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][month_interest]" class="disabled-form month_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['month_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p class="mb-0 number-money text_total_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}">{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['total_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']*$itemInvestment['investment_time_month']}}</p>
                                                                <input type="hidden" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][total_interest]" class="disabled-form total_interest_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}" value="{{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) ? $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['total_interest'] : ($detail['interest_rate_standard']/(100*12))*$detail['price_standard']*$itemInvestment['investment_time_month']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                                                    <label style="margin: 0 0 0 10px; padding-top: 4px">
                                                                        <input type="checkbox" onchange="product.changDisplay({{$itemInvestment['investment_time_id']}},{{$itemWithraw['withdraw_interest_time_id']}},{{$i}})" name="product[{{$i}}][{{$itemInvestment['investment_time_id']}}][{{$itemWithraw['withdraw_interest_time_id']}}][is_default_display]" class="manager-btn is_default_display_check disabled-form is_default_display_{{$itemInvestment['investment_time_id']}}_{{$itemWithraw['withdraw_interest_time_id']}}_{{$i}}"
                                                                                {{isset($productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]) && $productInterest[$itemInvestment['investment_time_id'].'-'.$itemWithraw['withdraw_interest_time_id'].'-'.$i]['is_default_display'] == 1 ? 'checked' :''}}>
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endfor
                                        @endif
                                    </form>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="product_bonus_tab" role="tabpanel">
                            <div class="m-portlet__body pl-0 pt-0 table-responsive">
                                <button onclick="product.submitBonusProduct()" type="button"
                                        class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md ">
                                    <span class="ss--text-btn-mobi">
                                        <span>{{__('LƯU')}}</span>
                                    </span>
                                </button>
                                <table class="table table-striped m-table m-table--head-bg-default mt-4 investment-withdraw">
                                    <thead class="bg">
                                    <tr>
                                        <td class="text-center">{{__('Hình thức thanh toán')}}</td>
                                        <td class="text-center">{{__('Kỳ hạn đầu tư')}} ({{__('tháng')}})</td>
                                        <td class="text-center">{{__('Tỉ lệ tiền thưởng')}} (%)</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <form id="list-product-bonus">
                                            <input type="hidden" class="disabled-form" id="product_id" name="product_id" value="{{$detail['product_id']}}">
                                            @if($detail['product_category_id'] == 2)
                                                @foreach($listPaymentMethod as $item)
                                                    @foreach($listInvestmentTime as $value)
                                                        <input type="hidden" class="disabled-form" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][product_bonus]" value="{{isset($productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]) ? $productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]['product_bonus_id'] : ''}}">
                                                        <tr>
                                                            <td class="text-center">
                                                                <p>{{$item['payment_method_name_vi']}}</p>
                                                                <input type="hidden" class="payment_method_{{$item['payment_method_id']}}_{{$value['investment_time_id']}}" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][payment_method]" value="{{$item['payment_method_id']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p>{{$value['investment_time_month']}}</p>
                                                                <input type="hidden" class="payment_method_{{$item['payment_method_id']}}_{{$value['investment_time_id']}}" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][investment_time]" value="{{$value['investment_time_id']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <input type="text" class="form-control number-money disabled-form" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][bonus_rate]" value="{{isset($productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]) ? number_format($productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]['bonus_rate'], 2) : ''}}">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                                @foreach($listPaymentMethod as $item)
                                                    <input type="hidden" class="disabled-form" name="bonus_withraw[{{$item['payment_method_id']}}][product_bonus]" value="{{isset($productBonus[$item['payment_method_id'].'_']) ? $productBonus[$item['payment_method_id'].'_']['product_bonus_id'] : ''}}">
                                                    <tr>
                                                        <td class="text-center">
                                                            <p>{{$item['payment_method_name_vi']}}</p>
                                                            <input type="hidden" name="bonus_withraw[{{$item['payment_method_id']}}][payment_method]" value="{{$item['payment_method_id']}}">
                                                        </td>
                                                        <td class="text-center">
                                                            <p>N/A</p>
                                                            <input type="hidden" name="bonus_withraw[{{$item['payment_method_id']}}][investment_time]" value="">
                                                        </td>
                                                        <td class="text-center">
                                                            <input type="text" class="form-control number-money disabled-form" name="bonus_withraw[{{$item['payment_method_id']}}][bonus_rate]" value="{{isset($productBonus[$item['payment_method_id'].'_']) ? number_format($productBonus[$item['payment_method_id'].'_']['bonus_rate'], 2) : ''}}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                @foreach($listPaymentMethod as $item)
                                                    @foreach($listInvestmentTime as $value)
                                                        <input type="hidden" class="disabled-form" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][product_bonus]" value="{{isset($productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]) ? $productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]['product_bonus_id'] : ''}}">
                                                        <tr>
                                                            <td class="text-center">
                                                                <p>{{$item['payment_method_name_vi']}}</p>
                                                                <input type="hidden" class="payment_method_{{$item['payment_method_id']}}_{{$value['investment_time_id']}}" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][payment_method]" value="{{$item['payment_method_id']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <p>{{$value['investment_time_month']}}</p>
                                                                <input type="hidden" class="payment_method_{{$item['payment_method_id']}}_{{$value['investment_time_id']}}" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][investment_time]" value="{{$value['investment_time_id']}}">
                                                            </td>
                                                            <td class="text-center">
                                                                <input type="text" class="form-control number-money disabled-form" name="bonus[{{$item['payment_method_id']}}][{{$value['investment_time_id']}}][bonus_rate]" value="{{isset($productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]) ? number_format($productBonus[$item['payment_method_id'].'_'.$value['investment_time_id']]['bonus_rate'], 2) : ''}}">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        </form>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="service_bonus_tab" role="tabpanel">
                            <form id="form-product-bonus-config">
                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        <button onclick="linkSource.saveConfig()" type="button"
                                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md ">
                                        <span class="ss--text-btn-mobi">
                                            <span>{{__('LƯU')}}</span>
                                        </span>
                                        </button>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Thời gian thưởng</label>
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control daterange-picker date_action_register" name="date_action_register" placeholder="Chọn thời gian phát thưởng" value="{{isset($config) && $config['start'] != null ? \Carbon\Carbon::parse($config['start'])->format('d/m/Y').' - '.\Carbon\Carbon::parse($config['end'])->format('d/m/Y') : ''}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6">
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label>Trạng thái</label>
                                        <div class="row">
                                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                                <input type="checkbox" class="manager-btn is_active_register" {{isset($config) && $config['is_active'] == 1 ? 'checked' :''}} >
                                                <span></span>
                                            </label>
                                        </div>
                                    </span>
                                    </div>

                                    <div class="form-group col-lg-12">
                                        <button type="button" class="btn btn-primary color_button btn-search add_service_register voucher_register" disabled onclick="linkSource.showPopup('register')">
                                            Thêm dịch vụ
                                        </button>
                                        <div class="table-content m--padding-top-30">
                                            <div class="table-responsive">
                                                <table class="table table-striped m-table ss--header-table table-refer">
                                                    <thead class="bg">
                                                    <tr class="ss--nowrap">
                                                        <th class="ss--font-size-th">{{__('Tên dịch vụ')}}</th>
                                                        {{--                                        <th class="ss--font-size-th">{{__('Số tiền (Vnđ)')}}</th>--}}
                                                        <th class="ss--font-size-th">Hành động</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="body_tr_register">
                                                    @if(isset($listService))
                                                        @foreach($listService as $key => $item)
                                                            <tr id="tr_{{$item['service_id']}}">
                                                                <td>{{$item['service_name_vi']}}</td>
                                                                {{--                                        <td>{{$item['price_standard']}}</td>--}}
                                                                <td>
                                                                    <button type="button" onclick="linkSource.removeService(this,{{$item['service_id']}})" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xoá">
                                                                        <i class="la la-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {{--    popup tiền thưởng--}}
    @include('admin::product.popup.product-bonus-popup')

    {{--    popup lãi suất--}}
    @include('admin::product.popup.product-interest-popup')

    <div id="append-product-bonus"></div>
    <div id="append-product-interest"></div>

    <div class="modal fade" id="popup_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Danh sách dịch vụ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="search_service">
                        <div class="form-group">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="form-control" id="service_name" name="service_name" placeholder="Tên dịch vụ">
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-secondary" onclick="linkSource.deleteSearch()">Xóa</button>
                                        <button type="button" class="btn btn-primary color_button" onclick="linkSource.searchPopup(1)">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group table-service">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary color_button" onclick="linkSource.addListService()" data-dismiss="modal">Chọn</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_script')
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/product/script.js?v='.time())}}"></script>

    <script>
        var Summernote = {
            init: function () {
                // $.getJSON(laroute.route('translate'), function (json) {
                //     $(".summernote").summernote({
                //         height: 208,
                //         placeholder: 'Nhập nội dung',
                //         toolbar: [
                //             ['style', ['bold', 'italic', 'underline']],
                //             ['fontsize', ['fontsize']],
                //             ['color', ['color']],
                //             ['para', ['ul', 'ol', 'paragraph']],
                //             // ['insert', ['link', 'picture']]
                //         ]
                //     })
                // });
            }
        };
        jQuery(document).ready(function () {
            $('html, body').animate({
                scrollTop: $('a[data-target="#{{$tab}}"]').offset().top
            }, 500);

            // $('.summernote-fix').summernote({
            //     height: 100,
            //     placeholder: 'Nhập nội dung',
            //     toolbar: [
            //         ['style', ['bold', 'italic', 'underline']],
            //         ['fontsize', ['fontsize']],
            //         ['color', ['color']],
            //         ['para', ['ul', 'ol', 'paragraph']],
            //         // ['insert', ['link', 'picture']]
            //     ]
            // });
            $('textarea[name=description_vi]').summernote('disable');
            $('textarea[name=description_en]').summernote('disable');
            $('textarea[name=product_short_name_vi]').summernote('disable');
            $('textarea[name=product_short_name_en]').summernote('disable');
            product.getListProductBonus(1);
            product.getListInterestRate(1);
            $('.select2').select2();
            $('input').prop('disabled',true);
            $('#product_bonus_popup input').prop('disabled',false);
            $('input[name="interest_rate"]').prop('disabled',false);
            $('.is_actived_check').prop('disabled',false);
            $('select').prop('disabled',true);
            $('#product_bonus_popup select').prop('disabled',false);
            $('.disabled-false').prop('disabled',false);
            $('textarea').prop('disabled',true);
            Summernote.init()
            $('.note-btn').attr('title', '');
            $('.term_time_type_select').prop('disabled',false);

            $('.interest_rate_outfocus').trigger('click');
            $('.disabled-form').prop('disabled',false);

            $('#service_bonus_tab input').prop('disabled',false);
            $('#service_bonus_tab button').prop('disabled',false);
            $('#service_name').prop('disabled',false);
        });
        new AutoNumeric.multiple('.number-money', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });

    </script>
@stop
