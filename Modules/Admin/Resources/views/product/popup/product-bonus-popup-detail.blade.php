<div class="modal fade" id="product_bonus_popup_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>{{_('Chi tiết tiền thưởng')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="product_bonus_add" autocomplete="off">
                    <div class="row">
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Hình thức thanh toán')}}</label>
                                </div>
                                <div class="col-9">
                                    <select class="form-control select2 w-100" disabled name="payment_method_id">
                                        <option value="">{{_('Chọn hình thức thanh toán')}}</option>
                                        @foreach($listPaymentMethod as $item)
                                            <option value="{{$item['payment_method_id']}}" {{$detailProductBonus['payment_method_id'] == $item['payment_method_id'] ? 'selected' : ''}}>{{$item['payment_method_name_vi']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Kỳ hạn đầu tư')}} ({{_('tháng')}})</label>
                                </div>
                                <div class="col-9">
                                    <select class="form-control select2 w-100" disabled id="investment_time_id" name="investment_time_id">
                                        <option value="">{{_('Chọn kỳ hạn đầu tư')}}</option>
                                        @foreach($listInvestmentTime as $item)
                                            <option value="{{$item['investment_time_id']}}" {{$detailProductBonus['investment_time_id'] == $item['investment_time_id'] ? 'selected' : ''}}>{{$item['investment_time_month']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Tỉ lệ tiền thưởng')}} (%)</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" name="bonus_rate" disabled value="{{number_format($detailProductBonus['bonus_rate'], 0) }}" class="form-control number-money" placeholder="{{_('Nhập tỉ lệ quà tặng thêm')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-3">
                                    <label>{{_('Trạng thái')}}</label>
                                </div>
                                <div class="col-9">
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input type="checkbox" disabled class="manager-btn is_actived_product_bonus" {{$detailProductBonus['is_actived'] == 1 ? 'checked' :''}}>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="product_id" value="{{$detailProductBonus['product_id']}}">
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-arrow-left"></i>
                                    <span>{{_('HỦY')}}</span>
                                </span>
                        </button>
{{--                        <button type="button" onclick="product.addProductBonus()" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5">--}}
{{--                                <span class="ss--text-btn-mobi">--}}
{{--                                    <i class="la la-check"></i>--}}
{{--                                    <span>{{_('LƯU')}}</span>--}}
{{--                                </span>--}}
{{--                        </button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>