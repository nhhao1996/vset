<div class="table-responsive pb-5">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <td>{{__('Loại lãi suất')}}</td>
            <td>{{__('Kỳ hạn đầu tư')}} ({{__('tháng')}})</td>
            <td>{{__('Kỳ hạn rút lãi')}} ({{__('tháng')}})</td>
            <td>{{__('Lãi suất')}} (%)</td>
            <td>{{__('Tỉ lệ hoa hồng')}} (%)</td>
            <td>{{__('Tổng lãi suất hàng tháng')}} (vnđ)</td>
            <td>{{__('Tổng lãi suất')}} (vnđ)</td>
            <td>{{__('Hiển thị chính')}}</td>
            <td>{{__('Trạng thái')}}</td>
            <td class="action-product">{{__('Hành động')}}</td>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $key => $item)
            <tr>
                <td>{{$item['term_time_type'] == 1 ? __('Có kỳ hạn') : __('Không có kỳ hạn')}}</td>
                <td>{{$item['investment_time_month']}}</td>
                <td>{{$item['withdraw_interest_month']}}</td>
                <td>{{number_format($item['interest_rate'])}} </td>
                <td>{{number_format($item['commission_rate'])}} </td>
                <td>{{number_format($item['month_interest'])}}</td>
                <td>{{number_format($item['total_interest'])}}</td>
                <td>
                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                            <input type="checkbox" disabled class="manager-btn" {{$item['is_default_display'] == 1 ? 'checked' : ''}}>
                            <span></span>
                        </label>
                    </span>
                </td>
                <td>
                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                            <input type="checkbox" disabled class="manager-btn" {{$item['is_actived'] == 1 ? 'checked' : ''}}>
                            <span></span>
                        </label>
                    </span>
                </td>
                <td class="action-product">
                    <a href="javascript:void(0)" onclick="product.detailProductInterest({{$item['product_interest_id']}})"
                       class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                       title="{{__('Chi tiết')}}">
                        <i class="la la-eye"></i>
                    </a>
                    <a href="javascript:void(0)" onclick="product.editProductInterest({{$item['product_interest_id']}})"
                       class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                       title="{{__('Chỉnh sửa')}}">
                        <i class="la la-edit"></i>
                    </a>
                    <button type="button" onclick="product.removeProductInterest(this, {{$item['product_interest_id']}})"
                            class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                            title="{{__('Xoá')}}">
                        <i class="la la-trash"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $list->links('admin::product.helpers.paging-product-interest') }}
</div>