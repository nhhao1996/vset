<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Mã gói')}}</th>
            <th class="ss--font-size-th">{{__('Tên gói tiếng Việt')}}</th>
            <th class="ss--font-size-th">{{__('Loại gói')}}</th>
            <th class="ss--font-size-th">{{__('Giá trị gói (vnđ)')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày phát hành')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                    <td>{{$item['product_code']}}</td>
                    <td>{{$item['product_name_vi']}}</td>
                    <td>{{$item['category_name_vi']}}</td>
                    <td>{{number_format($item['price_standard'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0) }}</td>
                    <td>
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox"
                                       disabled
                                       {{ ($item['is_actived'] == 1) ? 'checked' : '' }} class="manager-btn">
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td>{{\Carbon\Carbon::parse($item['published_at'])->format('d/m/Y')}}</td>
                    <td>
                        @if(in_array('admin.product.detail', session('routeList')))
                            <a href="{{route('admin.product.detail',['id'=>$item['product_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="la la-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.product.edit', session('routeList')))
                            <a href="{{route('admin.product.edit',['id'=>$item['product_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Cập nhật')}}">
                                <i class="la la-edit"></i>
                            </a>
                        @endif
                        @if(in_array('admin.product.remove', session('routeList')))
                            <button onclick="product.remove(this, {{$item['product_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="{{__('Xoá')}}">
                                <i class="la la-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}