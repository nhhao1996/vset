@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ GÓI ĐẦU TƯ - TIẾT KIỆM')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('DANH SÁCH GÓI ĐẦU TƯ - TIẾT KIỆM')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools nt-class">
                @if(in_array('admin.product.add',session('routeList')))
                    <a href="{{route('admin.product.add')}}"
                       class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                        <span>
						    <i class="fa fa-plus-circle m--margin-right-5"></i>
							<span> {{__('THÊM GÓI')}}</span>
                        </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="m-portlet__body">
            <form class="frmFilter ss--background">
                <div class="row ss--bao-filter">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
{{--                                <input type="hidden" name="search_type" value="product_name_vi">--}}
                                <button class="btn btn-primary btn-search" style="display: none">
                                    <i class="fa fa-search"></i>
                                </button>
                                <input type="text" class="form-control" name="search_keyword"
                                       placeholder="{{__('Nhập mã hoặc tên gói')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <select class="form-control select-fix" name="product_category_id">
                                    <option value="">{{__('Chọn loại gói')}}</option>
                                    @foreach($listCategory as $item)
                                        <option value="{{$item['product_category_id']}}" {{isset($FILTER['product_category_id']) && $FILTER['product_category_id'] == $item['product_category_id'] ? 'selected' :''}}>{{$item['category_name_vi']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="row">
                            @php $i = 0; @endphp
                            @foreach ($FILTER as $name => $item)
                                @if ($i > 0 && ($i % 4 == 0))
                        </div>
                        <div class="form-group m-form__group row align-items-center">
                            @endif
                            @php $i++; @endphp
                            <div class="col-lg-12 form-group input-group">
                                @if(isset($item['text']))
                                    <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        {{ $item['text'] }}
                                                    </span>
                                    </div>
                                @endif
                                @if($name=='products$is_actived')
                                    {!! Form::select($name, $item['data'], $item['default'] ?? null, ['class' => 'form-control m-input m_selectpicker ss--width-100-','title'=>__('Chọn trạng thái')]) !!}
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text"
                                   class="form-control m-input daterange-picker" id="created_at"
                                   name="created_at"
                                   autocomplete="off" placeholder="{{__('Chọn ngày phát hành')}}">
                            <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span></span>
                        </div>
                    </div>
                    <div class="col-lg-12 form-group text-right">
                        <div class="form-group m-form__group">
                            <button href="javascript:void(0)" onclick="product.search()"
                                    class="btn ss--btn-search ">
                                {{__('TÌM KIẾM')}}
                                <i class="fa fa-search ss--icon-search"></i>
                            </button>
                            <a href="{{route('admin.product')}}"
                               class="btn btn-metal  btn-search padding9x">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-content m--padding-top-30">
                @include('admin::product.list')
            </div><!-- end table-content -->
        </div>
    </div>
@endsection
@section('after_script')
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>

    <script src="{{asset('static/backend/js/admin/product/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $('.select-fix').select2();
        $(".m_selectpicker").selectpicker();
    </script>
@stop
