<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Loại hỗ trợ')}}</th>
            <th class="ss--font-size-th">{{__('Nhà đầu tư')}}</th>
            <th class="ss--font-size-th">{{__('Số điện thoại')}}</th>
            <th class="ss--font-size-th">{{__('Email')}}</th>
            <th class="ss--font-size-th">{{__('Nội dung')}}</th>

            {{--            <th class="ss--font-size-th">{{__('Đối tượng')}}</th>--}}
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày tạo ')}}</th>
            <th class="ss--font-size-th">{{__('Ngày cập nhật ')}}</th>
            <th class="ss--font-size-th">{{__('Hành động ')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                {{--                {{dd($LIST)}}--}}
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                    <td>
                        {{$item['support_request_type_name']}}
                    </td>
                        <td>
                            <a href="{{route('admin.customer.detail',['id' => $item['customer_id']])}}">
                                {{$item['full_name']}}
                            </a>
                        </td>
                        <td>
                            {{$item['phone2']}}
                        </td>
                        <td>
                            {{$item['email']}}
                        </td>

                        <td>{{str_limit(strip_tags($item['support_request_description']), 60, "...")}}</td>
{{--                    <td>--}}
{{--                        @switch($item['object_type'])--}}
{{--                            @case("bond")--}}
{{--                            {{"Ví trái phiếu"}}--}}
{{--                            @break--}}
{{--                            @case("saving")--}}
{{--                            {{"Ví tiết kiệm"}}--}}
{{--                            @break--}}
{{--                            @default--}}
{{--                            {{"Khác"}}--}}
{{--                            @break--}}
{{--                        @endswitch--}}
{{--                    </td>--}}
                    <td>
                        @switch($item['support_request_status'])
                            @case("new")
                            {{"Mới"}}
                            @break
                            @case("in_process")
                            {{"Đang xử lý"}}
                            @break
                            @case("done")
                            {{"Hoàn thành"}}
                            @break
                            @default
                            {{"Khác"}}
                            @break
                        @endswitch
                    </td>
                    <td>{{ date("d/m/Y H:i:s",strtotime($item['created_at'])) }}</td>
                    <td>{{date("d/m/Y H:i:s",strtotime($item['updated_at']))}}</td>
                    <td>
                        @if(in_array('admin.support.show', session('routeList')))
                        <a href="{{route('admin.support.show',['id'=>$item['support_request_id']])}}"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           title="{{__('Chi tiết')}}">
                            <i class="la la-eye"></i>
                        </a>
                        @endif
                        @if(in_array('admin.support.edit', session('routeList')))
                        <a href="{{route('admin.support.edit',['id'=>$item['support_request_id']])}}"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           title="{{__('Cập nhật')}}">
                            <i class="la la-edit"></i>
                        </a>
                        @endif
                        @if(in_array('admin.support.remove', session('routeList')))
                        <button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           title="{{__('Xóa')}}" onclick="support.remove(this, {{$item['support_request_id']}})">
                            <i class="la la-trash"></i>
                        </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}