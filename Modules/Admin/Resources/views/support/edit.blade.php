@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ HỖ TRỢ")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"
                                 style="font-size: 13px"></i> @lang("CẬP NHẬT HỖ TRỢ")</span>
                    </h2>
                </div>
            </div>
        </div>
        <style>
            .nt-custome-row  .form-group{
                margin-right: 0;
                margin-left: 0;
            }
        </style>
        <form id="form-edit">
            <div class="m-portlet__body">
                <div class="row nt-custome-row">
                    <div class="form-group col-lg-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Tên loại hỗ trợ')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="support_request_type_name" disabled
                                           class="form-control m-input"
                                           placeholder="@lang('Tên loại hỗ trợ')"
                                           value="{{$item['support_request_type_name']}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Trạng thái')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <select name="support_request_status"  class="form-control">
                                    <option value="new" {{($item["support_request_status"] == "new") ? "selected" :null}}>Mới</option>
                                    <option value="in_process" {{($item["support_request_status"] == "in_process") ? "selected" :null}}>Đang xử lý</option>
                                    <option value="done" {{($item["support_request_status"] == "done") ? "selected" :null}}>Xong</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Vấn đề cần hỗ trợ')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <textarea type="text" name="support_request_description"
                                           class="form-control m-input" disabled rows="5"
                                              value="">{{$item['support_request_description']}}</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-group col-lg-6">
                        @if($item["product_name"] != null)
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Sản phầm cần hỗ trợ')}}:
                                </label>
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="product_name" class="form-control m-input" disabled
                                           placeholder="{{__('Sản phầm cần hỗ trợ')}}" value="{{$item['product_name']}}">
                                </div>
                            </div>
                        @endif
                        @if($item["support_request_time"] != null)
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Thời gian yêu cầu liên hệ lại	:')}}
                                </label>
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" id="customer_refer_code" name="customer_refer_code" class="form-control m-input" disabled
                                           value="{{ date("d/m/Y H:i",strtotime($item['support_request_time'])) }}">
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Nhà đầu tư')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input" disabled
                                       placeholder="{{__('Nhà đầu tư')}}" value="{{$item['full_name']}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Số điện thoại')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input" disabled
                                       placeholder="{{__('Số điện thoại')}}" value="{{$item['phone2']}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Email')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input" disabled
                                       placeholder="{{__('Email')}}" value="{{$item['email']}}">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <input type="hidden" id="support_request_id_hidden" value="{{$item['support_request_id']}}">
        </form>

        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.support')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('HUỶ')}}</span>
						</span>
                    </a>

                    <button type="submit" onclick="support.editStatus()"
                            class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
							<span>
							<i class="la la-edit"></i>
							<span>{{__('CẬP NHẬT')}}</span>
							</span>
                    </button>
                </div>
            </div>
        </div>
    </div>


@stop
@section("after_style")
    {{--    <link rel="stylesheet" href="{{asset('static/backend/css/process.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">

@stop
@section('after_script')

    <script>

        // Shorthand for $( document ).ready()
        $(function () {
            $('#birthday').datepicker({
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

        });

    </script>

    <script src="{{asset('static/backend/js/admin/support/edit.js?v='.time())}}" type="text/javascript"></script>
@stop