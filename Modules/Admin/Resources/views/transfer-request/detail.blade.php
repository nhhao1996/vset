@extends('layout')@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("YÊU CẦU CHUYỂN NHƯỢNG CỔ PHIẾU")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }

        .m-datatable__head th {
            background-color: #dff7f8 !important;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT YÊU CẦU CHUYỂN NHƯỢNG')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <!-- Thông tin của nhà đầu tư chưa đủ thì không cho xác nhận -->
                @if($item['process_status'] == 'new')
                    @if(
                            $item['transfer_name'] == null ||
                            $item['transfer_code'] == null ||
                            $item['transfer_phone'] == null ||
                            strlen(str_replace(' ','',$item['transfer_name'] )) == 0 ||
                            strlen(str_replace(' ','',$item['transfer_code'] )) == 0 ||
                            strlen(str_replace(' ','',$item['transfer_phone'] )) == 0

                    )
                        <button type="button" onclick="TransferRequest.confirmFail(1)"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
                                <i class="fas fa-check-square"></i>
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                        </button>

                    @elseif(
                            $item['receiver_name'] == null ||
                            $item['receiver_code'] == null ||
                            $item['receiver_phone'] == null ||
                            strlen(str_replace(' ','',$item['receiver_name'] )) == 0 ||
                            strlen(str_replace(' ','',$item['receiver_code'] )) == 0 ||
                            strlen(str_replace(' ','',$item['receiver_phone'] )) == 0

                    )
                        <button type="button" onclick="TransferRequest.confirmFail(2)"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
                                <i class="fas fa-check-square"></i>
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                        </button>
                    @elseif($item['transfer_stock'] < $item['quantity'])
                        <button type="button" onclick="TransferRequest.confirmFail(3)"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
                                <i class="fas fa-check-square"></i>
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                        </button>
                    @elseif($item['payment_method_id'] != 1 && $item['payment_method_id'] != 4)
                        @if(($item['payment_method_id'] == 5 && $item['transfer_stock_money'] < $item['fee']*$item['total']/100)
                         || ($item['payment_method_id'] == 6 && $item['transfer_dividend_money'] < $item['fee']*$item['total']/100))
                            <button type="button" onclick="TransferRequest.confirmFail(4)"
                                    class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                <span class="text-white">
                                    <i class="fas fa-check-square"></i>
                                    <span class="zxrem">  {{__('Xác nhận')}}</span>
                                </span>
                            </button>
                        @else
                            <button type="button" onclick="TransferRequest.changeStatus('new')"
                                    class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
                                <i class="fas fa-check-square"></i>
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                            </button>
                        @endif
                    @else
                        <button type="button" onclick="TransferRequest.changeStatus('new')"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
                                <i class="fas fa-check-square"></i>
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                        </button>
                    @endif
                    <a href="javascript:void(0)" onclick="TransferRequest.changeStatus('cancel')"
                       class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                                <span class="text-white">
                                    <span class="zxrem">Huỷ xác nhận</span>
                                </span>
                    </a>
                @endif
        </div>
        </div>
        <div class="m-portlet__body">
            <form id="transfer-request-detail">
                <div class="row">
                    <div class="form-group col-lg-6 row">
                        <div class="form-group col-lg-12 row">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Mã yêu cầu')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="stock_order_code"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['stock_order_code']) ? $item['stock_order_code'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6 row">
                        <div class="form-group col-lg-12" hidden>
                            <input type="text" name="stock_order_id"
                                   value="{{$item['stock_order_id']}}">
                            <input type="text" name="customer_id"
                                   value="{{$item['customer_id']}}">
                            <input type="text" name="obj_id"
                                   value="{{$item['obj_id']}}">
                            <input type="text" name="payment_method_id"
                                   value="{{$item['payment_method_id']}}">
                            <input type="text" name="quantity"
                                   value="{{$item['quantity']}}">
                            <input type="text" name="money_standard"
                                   value="{{$item['money_standard']}}">
                            <input type="text" name="total"
                                   value="{{$item['total']}}">
                            <input type="text" name="fee"
                                   value="{{$item['fee']}}">
                            <input type="text" name="total_money"
                                   value="{{$item['total_money']}}">
                        </div>
                    </div>


                    <div class="form-group col-lg-6 row">
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Nhà đầu tư chuyển')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['transfer_name']) ? $item['transfer_name'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Mã NĐT')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['transfer_code']) ? $item['transfer_code'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Số điện thoại')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['transfer_phone']) ? $item['transfer_phone'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-lg-6 row">
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Nhà đầu tư nhận')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['receiver_name']) ? $item['receiver_name'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Mã NĐT')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['receiver_code']) ? $item['receiver_code'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Số điện thoại')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['receiver_phone']) ? $item['receiver_phone'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__head-caption" style="color: #027177 !important;">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{__('THÔNG TIN CHUYỂN NHƯỢNG')}}
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="row form-group col-lg-6">
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Cổ phiếu')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['stock_config_code']) ? $item['stock_config_code'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>

                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Số lượng')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="quantity"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['quantity']) ? number_format($item['quantity']) : 'N/A'}}">
                                </div>
                            </div>
                        </div>

                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Đơn giá')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="money_standard"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['money_standard']) ? number_format($item['money_standard']) : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Thành tiền')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="total"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['total']) ? number_format($item['total']) : 'N/A'}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group col-lg-6">
                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Phí chuyển nhượng')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="fee"
                                           class="form-control m-input" disabled
                                           value="{{(isset($item['fee']) && isset($item['total'])) ? number_format($item['fee']*$item['total']/100) : 'N/A'}}">
                                </div>
                            </div>
                        </div>


                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Nguồn tiền')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['payment_method_name']) ? $item['payment_method_name'] : 'N/A'}}">
                                </div>
                            </div>
                        </div>


                        <div class="row form-group col-lg-12">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Thời gian')}}:
                            </label>
                            <div class="input-group col-lg-8">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"
                                           class="form-control m-input" disabled
                                           value="{{isset($item['created_at']) ? Carbon\Carbon::parse($item['created_at'])->format('d/m/Y H:i') : 'N/A'}}">
                                </div>
                            </div>
                        </div>

                        <div class="row form-group col-lg-12">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!--begin::Portlet-->
        <div class="m-portlet__body">
            <div class=" m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.transfer-request')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                        <span>
                            <i class="la la-arrow-left"></i>
                            <span>{{__('QUAY LẠI')}}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
    {{--    </form>--}}
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/transfer-request/script.js?v='.time())}}" type="text/javascript"></script>
@stop
