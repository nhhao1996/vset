@extends('layout') @section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection @section('title_header')
    <span class="title_header">
    <img src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;" />
    {{__('DANH SÁCH YÊU CẦU CHUYỂN NHƯỢNG')}}
</span>
@endsection @section('content')
    <meta http-equiv="refresh" content="number" />
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <form class="frmFilter">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            {{__('DANH SÁCH YÊU CẦU CHUYỂN NHƯỢNG')}}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="{{__('Nhập mã yêu cầu, tên nhà đầu tư')}}" value="{{isset($FILTER['search']) ? $FILTER['search'] : null}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <select name="process_status" class="form-control select m-input">
                                        <option value="">{{__('Chọn trạng thái')}}</option>
                                        <option value="new">{{__('Mới')}}</option>
                                        <option value="paysuccess">{{__('Đã duyệt')}}</option>
                                        <option value="cancel">{{__('Đã huỷ')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group" style="background-color: white;">
                                <div class="m-input-icon m-input-icon--right">
                                    <input readonly="" class="form-control m-input daterange-picker" id="created_at" name="created_at" autocomplete="off" placeholder="{{__('Ngày khởi tạo')}}" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button class="btn btn-primary color_button btn-search">{{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i></button>
                                <a href="{{route('admin.transfer-request')}}" class="btn btn-metal padding9x">
                                <span>
                                    <i class="flaticon-refresh"></i>
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::transfer-request.list')
                </div>
                <!-- end table-content -->
            </div>
        </form>
    </div>
@endsection @section('after_script')
    <script src="{{asset('static/backend/js/admin/transfer-request/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
@stop
