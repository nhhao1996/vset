@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <style>
        .note-editor {
            width: 100%;
        }
    </style>
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ TIN TỨC')}}
    </span>
@endsection
@section('content')
    <form id="form" autocomplete="off">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                        <h3 class="m-portlet__head-text">
                            {{__('CHI TIẾT TIN TỨC')}}
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group m-form__group">
                            <label>{{__('Danh mục')}}:<b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <select style="width: 100%" id="stock_category_id" disabled name="stock_category_id" class="form-control stock_category_id m_selectpicker"
                                        title="{{__('Chọn danh mục')}}">
                                    <option value="">{{__('Chọn danh mục')}}</option>
                                    @foreach($listCategory as $key=>$value)
                                        <option value="{{$value['stock_category_id']}}" {{$detail['stock_category_id'] == $value['stock_category_id'] ? 'selected' :''}}>{{$value['stock_category_title_vi']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <span class="errs error-category"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tên tin tức(VI)')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input name="stock_news_title_vi" type="text" class="form-control m-input class"
                                       placeholder="{{__('Tên tin tức(VI)')}}" value="{{$detail['stock_news_title_vi']}}" disabled
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-product-code"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tên tin tức(EN)')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input name="stock_news_title_en" type="text" class="form-control m-input class"
                                       placeholder="{{__('Tên tin tức(EN)')}}" value="{{$detail['stock_news_title_en']}}" disabled
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-product-code"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Nội dung tin tức(VI)')}}:
                            </label>
                            <div class="input-group">
                                <textarea id="stock_news_content_vi" name="stock_news_content_vi" disabled type="text" class="form-control m-input class"
                                       placeholder="{{__('Nội dung tin tức(VI)')}}"
                                          aria-describedby="basic-addon1">{!! $detail['stock_news_content_vi'] !!}</textarea>
                            </div>
                            <span class="errs error-product-code"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Nội dung tin tức(EN)')}}:
                            </label>
                            <div class="input-group">
                                <textarea id="stock_news_content_en" name="stock_news_content_en" disabled type="text" class="form-control m-input class"
                                       placeholder="{{__('Nội dung tin tức(EN)')}}"
                                          aria-describedby="basic-addon1">{!! $detail['stock_news_content_vi'] !!}</textarea>
                            </div>
                            <span class="errs error-product-code"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình tin tức')}}:
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="img_stock_news_img_hidden" >
                                <input type="hidden" id="img_stock_news_img" name="stock_news_img"
                                       value="{{$detail['stock_news_img']}}">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="image_main"
                                         src="{{$detail['stock_news_img'] != null ? $detail['stock_news_img'] : asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group m-form__group">
                            <div class="row">
                                <label class="col-2">
                                    {{__('Trạng thái')}}:
                                </label>
                                <div class="input-group col-8">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label style="margin: 0 0 0 10px; padding-top: 4px">
                                        <input type="checkbox" id="is_active" class="manager-btn" disabled  {{$detail['is_active'] == 1 ? 'checked' : ''}}>
                                        <span></span>
                                    </label>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer save-attribute m--margin-right-20">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.stock-news')}}"
                           class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('after_script')
    <script>
    </script>

    <script src="{{asset('static/backend/js/admin/stock-news/script.js?v='.time())}}"></script>

    <script>
        $(document).ready(function () {
            $('#stock_category_id').select2();

            $('#stock_news_content_vi').summernote({
                height: 400,
                lang: 'vi-VN',
                placeholder: 'Nhập nội dung...',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'picture','file']]
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        for(let i=0; i < files.length; i++) {
                            uploadImgCk(files[i],'vi');
                        }
                    }
                },

            });

            $('#stock_news_content_en').summernote({
                height: 400,
                lang: 'vi-VN',
                placeholder: 'Nhập nội dung...',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'picture','file']]
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        for(let i=0; i < files.length; i++) {
                            uploadImgCk(files[i],'en');
                        }
                    }
                },

            });

            $('#stock_news_content_vi').summernote('disable');
            $('#stock_news_content_en').summernote('disable');

        });

    </script>
@stop
