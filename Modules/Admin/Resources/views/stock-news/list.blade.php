<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Tên danh mục(VI)')}}</th>
            <th class="ss--font-size-th">{{__('Tên danh mục(EN)')}}</th>
            <th class="ss--font-size-th">{{__('Tên tin tức(VI)')}}</th>
            <th class="ss--font-size-th">{{__('Tên tin tức(EN)')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr class="ss--font-size-13 ss--nowrap">
                    <td>{{ (($LIST->currentPage()-1)*10 + $key + 1) }}</td>
                    <td title="{{$item['stock_category_title_vi']}}">{{subString($item['stock_category_title_vi'])}}</td>
                    <td title="{{$item['stock_category_title_en']}}">{{subString($item['stock_category_title_en'])}}</td>
                    <td title="{{$item['stock_news_title_vi']}}">{{subString($item['stock_news_title_vi'])}}</td>
                    <td title="{{$item['stock_news_title_en']}}">{{subString($item['stock_news_title_en'])}}</td>
                    <td>
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox" id="is_actived" class="manager-btn" {{$item['is_active'] == 1 ? 'checked' : ''}} disabled>
                                <span></span>
                            </label>
                        </span>
                    </td>

                    <td>
                        @if(in_array('admin.stock-news.detail', session('routeList')))
                            <a href="{{route('admin.stock-news.detail',['id' => $item['stock_news_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="la la-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.stock-news.edit', session('routeList')))
                            <a href="{{route('admin.stock-news.edit',['id' => $item['stock_news_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Cập nhật')}}">
                                <i class="la la-edit"></i>
                            </a>
                        @endif
                        @if(in_array('admin.stock-news.remove', session('routeList')))
                            <button type="button" onclick="stockNews.remove(this, {{$item['stock_news_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="{{__('Xoá')}}">
                                <i class="la la-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}