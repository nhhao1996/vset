<div id="editForm" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_index">
                    <i class="la la-edit"></i> {{__('CHỈNH SỬA NHÓM DỊCH VỤ')}}
                </h4>

            </div>
            <form id="formEdit">
                <div class="modal-body">
                    <input type="hidden" id="hhidden" value="{{$detail['service_category_id']}}">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>
                                    {{__('Nhóm dịch vụ (VI)')}}:<b class="text-danger">*</b>
                                </label>
                                <div>
                                    <input placeholder="{{__('Nhập tên nhóm dịch vụ (VI)')}}..." type="text" name="name_vi"
                                           class="form-control m-input" id="h_name_vi" value="{{$detail['name_vi']}}">
                                    <span class="error-name_vi"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    {{__('Nhóm dịch vụ (EN)')}}:<b class="text-danger">*</b>
                                </label>
                                <div>
                                    <input placeholder="{{__('Nhập tên nhóm dịch vụ')}}..." type="text" name="name_en"
                                           class="form-control m-input" id="h_name_en" value="{{$detail['name_en']}}">
                                    <span class="error-name_en"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    {{__('Mô tả (VI)')}}:
                                </label>
                                <div class="input-group">
                                <textarea placeholder="{{__('Nhập thông tin mô tả (VI)')}}" rows="3" cols="50" name="description_vi"
                                          id="h_description_vi" class="form-control">{{$detail['description_vi']}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    {{__('Mô tả (EN)')}}:
                                </label>
                                <div class="input-group">
                                <textarea placeholder="{{__('Nhập thông tin mô tả (EN)')}}" rows="3" cols="50" name="description_en"
                                          id="h_description_en" class="form-control">{{$detail['description_en']}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    {{__('Trạng thái')}}:
                                </label>
                                <div class="input-group">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input id="h_is_actived" name="is_actived" type="checkbox" class="is_actived" {{$detail['is_actived'] == 1 ? 'checked' : ''}}>
                                        <span></span>
                                    </label>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit w-100">
                        <div class="m-form__actions m--align-right">
                            <button data-dismiss="modal"
                                    class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                                <span>
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
                            </button>

                            <button type="button" id="btnLuu" onclick="service_category.editPost()"
                                    class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                    <span>
                                    <i class="la la-edit"></i>
                                    <span>{{__('CẬP NHẬT')}}</span>
                                    </span>
                            </button>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
