@extends('layout')
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/service-card.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ SAO KÊ')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
        .form-control-feedback {
            color : red;
        }
        #create-bill {
            overflow: auto !important;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT SAO KÊ')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>

        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-12">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group m-form__group">
                                <label>
                                    Họ và tên:
                                </label>
                                <div class="input-group">
                                    <input id="customer_name" name="customer_name" type="text" class="form-control m-input class font-weight-bold" placeholder=" Nhập họ và tên " disabled="" value="{{$detail[0]['user_name']}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group m-form__group">
                                <label>
                                    Số điện thoại
                                </label>
                                <div class="input-group">
                                    <input id="customer_name" name="customer_name" type="text" class="form-control m-input class font-weight-bold" disabled="" value="{{$detail[0]['phone']}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group m-form__group">
                                <label>
                                    Tháng sao kê
                                </label>
                                <div class="input-group">
                                    <input id="customer_name" name="customer_name" type="text" class="form-control m-input class font-weight-bold" placeholder=" Nhập họ và tên " disabled="" value="{{\Carbon\Carbon::parse($detail[0]['date_satement'])->format('m-Y')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped m-table m-table--head-bg-default">
                        <thead class="bg">
                            <tr>
                                <th>Hợp đồng số</th>
                                <th>Hạn mức đầu tư (Vnđ)</th>
                                <th>Lãi suất hợp tác đầu tư (Vnđ)</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($detail as $item)
                            <tr>
                                <th><a href="{{route('admin.customer-contract.detail',['id' => $item['customer_contract_id']])}}" >{{$item['customer_contract_code']}}</a></th>
                                <th>{{number_format($item['total_amount'],2)}} </th>
                                <th>{{number_format($item['money_in']-$item['money_out'],2)}} </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- hóa đơn -->
        </div>
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.account-statement')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('QUAY LẠI ')}}</span>
                    </span>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/extend-contract/script.js?v='.time())}}"></script>

@stop