<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list text-center">#</th>
            <th class="tr_thead_list text-center">{{__('Mã nhà đầu tư')}}</th>
            <th class="tr_thead_list text-center">{{__('Họ và tên')}}</th>
            <th class="tr_thead_list text-center">{{__('Email')}}</th>
            <th class="tr_thead_list text-center">{{__('Thời gian sao kê')}}</th>
            <th class="tr_thead_list text-center">{{__('Hành Động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle text-center">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle text-center">{{$key+1}}</td>
                    @endif
                    <td class="text_middle text-center">{{$item['customer_code']}}</td>
                    <td class="text_middle text-center">{{$item['user_name']}}</td>
                    <td class="text_middle text-center">{{$item['email']}}</td>
                    <td class="text_middle text-center">{{\Carbon\Carbon::parse($item['month'])->format('m-Y')}}</td>
                        <td class="text-center">
                            @if(in_array('admin.account-statement.detail', session('routeList')))
                                <a class ="a-custom" href="{{route("admin.account-statement.detail",['id' => $item['account_statement_id'] ])}}" title="Chi tiết">
                                    <i class="flaticon-eye"></i>
                                </a>
                            @endif
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }
    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }
    .a-custom .flaticon-eye{
        top: 1px;
        left: 2px;
    }
</style>
