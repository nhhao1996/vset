@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ SAO KÊ')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/
        .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > span , .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > input:checked ~ span {
            background: #4fc4ca;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <form class="frmFilter" autocomplete="off">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                             <i class="la la-th-list"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{__('DANH SÁCH SAO KÊ HÀNG THÁNG')}}
                        </h2>

                    </div>
                </div>

                <div class="m-portlet__head-tools nt-class">
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="frmFilter bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" value="{{isset($FILTER['search']) ? $FILTER['search'] : ''}}"
                                           placeholder="{{__('Nhập mã nhà đầu tư, họ tên hoặc email')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button type="submit" class="btn btn-primary color_button btn-search " style="width: 150px">
                                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                </button>
                                <a href="{{route('admin.account-statement')}}"
                                   class="btn btn-metal  btn-search padding9x padding9px">
                                    <span><i class="flaticon-refresh"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                            <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                        </div>
                    @endif
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::account-statement.list')

                </div><!-- end table-content -->

            </div>
        </form>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@stop
@section('after_script')


    <script src="{{asset('static/backend/js/admin/account-statement/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
        $('select').select2();
        $('.created').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        @if(isset($FILTER['created']))
        $('.created').val('{{$FILTER['created']}}');
        @else
        $('.created').val('');
        @endif
    </script>
    <script type="text/template" id="tb-card-tpl">
        <tr class="tr-card">
            <td>
                {code}
                <input type="hidden" name="code" value="{code}">
            </td>
            <td>
                {name_code}
            </td>
            <td>
                {day_active}
                <input type="hidden" name="day_active" value="{day_active}">
            </td>
            <td>
                {day_expiration}
                <input type="hidden" name="day_active" value="{day_expiration}">
            </td>
            <td>
                {name_type}
                <input type="hidden" name="type" value="{type}">
            </td>
            <td>
                {price_td}
                <input type="hidden" name="price" value="{price}">
            </td>
            <td>
                <input type="hidden" name="number_using" value="{number_using}">
                <input type="hidden" name="service_card_id" value="{service_card_id}">
                <input type="hidden" name="service_card_list_id" value="{service_card_list_id}">
                <a style="margin-top: -5px;"
                   class='remove m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill'><i
                            class='la la-trash'></i></a>
            </td>
        </tr>
    </script>
@stop