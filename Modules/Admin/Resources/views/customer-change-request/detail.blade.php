@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ YÊU CẦU THAY ĐỔI THÔNG TIN CÁ NHÂN")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }
        .m-datatable__head th {
            background-color: #dff7f8 !important;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"
                                 style="font-size: 13px"></i> @lang("CẬP NHẬT YÊU CẦU THAY ĐỔI THÔNG TIN")</span>
                    </h2>
                </div>
            </div>
        </div>
        <style>
            .nt-custome-row  .form-group, .row{
                margin-right: 0;
                margin-left: 0;
            }

        </style>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
{{--                    mã khách hàng--}}
{{--                    <div class="row form-group">--}}
{{--                        <label  class="col-form-label label col-lg-4 black-title">--}}
{{--                            {{__('Mã nhà đầu tư')}}:--}}
{{--                        </label>--}}
{{--                        <div class="input-group">--}}
{{--                            <div class="m-input-icon m-input-icon--right">--}}
{{--                                <input type="text" id="customer_code" name="customer_code"--}}
{{--                                       class="form-control m-input" disabled--}}
{{--                                       value="{{$item['customer_code']}}">--}}
{{--                                --}}{{--                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i--}}
{{--                                --}}{{--                                                class="flaticon-menu-1"></i></span></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row form-group">--}}

{{--                        <div class="input-group">--}}
{{--                            <div class="m-input-icon m-input-icon--right">--}}
{{--                                <input type="text" id="customer_code" name="customer_code"--}}
{{--                                       class="form-control m-input" disabled--}}
{{--                                       value="{{$itemAfter['customer_code']}}">--}}
{{--                                --}}{{--                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i--}}
{{--                                --}}{{--                                                class="flaticon-menu-1"></i></span></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    tài khoản--}}
                    @if($itemAfter['status'] == 'cancel')
                        <div class="row form-group"   >
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Lý do từ chối (VI)')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <textarea type="text" disabled
                                              class="form-control m-input text-danger">{{$itemAfter['reason_vi']}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group"   >
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Lý do từ chối (EN)')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <textarea type="text" disabled
                                              class="form-control m-input text-danger">{{$itemAfter['reason_en']}}</textarea>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Tài khoản')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="full_name" name="account" disabled
                                       class="form-control m-input"
                                       value="{{$item['phone']}}">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group"   >
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="account" name="account" disabled
                                       class="form-control m-input {{$item['phone'] != $itemAfter['phone'] ? 'text-danger' : ''}} "
                                       value="{{$itemAfter['phone']}}">
                            </div>
                        </div>
                    </div>
{{--                    email 2--}}
{{--                    <div class="row form-group">--}}
{{--                        <label  class="col-form-label label col-lg-4 black-title">--}}
{{--                            {{__('Email')}}: <b class="text-danger">*</b>--}}
{{--                        </label>--}}
{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="email" name="email" class="form-control m-input" disabled--}}
{{--                                    value="{{$item['email']}}">--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="row form-group">--}}

{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="email" name="email" class="form-control m-input text-danger" disabled--}}
{{--                                    value="{{$itemAfter['email']}}">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    // tên khách hàng--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Tên khách hàng')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="full_name" name="full_name" disabled
                                       class="form-control m-input"
{{--                                       placeholder="@lang('Nhập tên khách hàng')"--}}
                                       value="{{$item['full_name']}}">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="full_name" name="full_name" disabled
                                       class="form-control m-input {{$item['full_name'] != $itemAfter['full_name'] ? 'text-danger' : ''}} "
{{--                                       placeholder="@lang('Nhập tên khách hàng')"--}}
                                       value="{{$itemAfter['full_name']}}">
                            </div>
                        </div>
                    </div>
{{--                    // ngày sinh--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Ngày sinh')}}:
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="birthday" name="birthday" class="form-control m-input" disabled
                                   value="{{$item['birthday'] != null ? date("d/m/Y",strtotime($item['birthday'])) : null}}" >
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="birthday" name="birthday" class="form-control m-input {{$item['birthday'] != $itemAfter['birthday'] ? 'text-danger' : '' }} " disabled
                                   value="{{$itemAfter['birthday'] != null ? date("d/m/Y",strtotime($itemAfter['birthday'])) : null}}" >
                        </div>
                    </div>
{{--                    // giới tính--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Giới tính')}}:
                        </label>
                        <div class="input-group">
                            <select id="gender" name="gender" class="form-control" disabled
                                    style="width: 100%">
                                <option value=""></option>
                                <option value="male" @if($item['gender'] == 'male') selected @endif>{{__('Nam')}} </option>
                                <option value="female" @if($item['gender'] == 'female') selected @endif>{{__('Nữ')}} </option>
                                <option value="other" @if($item['gender'] == 'other') selected @endif>{{__('Khác')}} </option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="input-group">
                            <select id="gender" name="gender" class="form-control {{$item['gender'] != $itemAfter['gender'] ? 'text-danger' : ''}} " disabled
                                    style="width: 100%">
                                <option value=""></option>
                                <option value="male" @if($itemAfter['gender'] == 'male') selected @endif>{{__('Nam')}} </option>
                                <option value="female" @if($itemAfter['gender'] == 'female') selected @endif>{{__('Nữ')}} </option>
                                <option value="other" @if($itemAfter['gender'] == 'other') selected @endif>{{__('Khác')}} </option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Ngân hàng')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input name="bank_name" type="text"  class="form-control m-input" disabled
                                   value="{{$item['bank_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input name="bank_name" type="text"  class="form-control m-input {{$item['bank_name'] != $itemAfter['bank_name'] ? 'text-danger' : ''}} " disabled
                                   value="{{$itemAfter['bank_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Chi nhánh')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text"  class="form-control m-input" disabled
                                   value="{{$item['bank_account_branch']}}">
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text"  class="form-control m-input {{$item['bank_account_branch'] != $itemAfter['bank_account_branch'] ? 'text-danger' : ''}} " disabled
                                   value="{{$itemAfter['bank_account_branch']}}">
                        </div>
                    </div>
{{--                    //Tên tài khoản--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Tên tài khoản')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="bank_name" name="bank_name" class="form-control m-input" disabled
                                   value="{{$item['bank_account_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="bank_name" name="bank_name" class="form-control m-input {{$item['bank_account_name'] != $itemAfter['bank_account_name'] ? 'text-danger' : ''}} " disabled
                                   value="{{$itemAfter['bank_account_name']}}">
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-presentation"></i></span></span>--}}
                        </div>
                    </div>
{{--                    // số tài khoản--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số tài khoản')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="bank_account_no" name="bank_account_no" class="form-control m-input" disabled
                                   value="{{$item['bank_account_no']}}">
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="bank_account_no" name="bank_account_no" class="form-control m-input {{$item['bank_account_no'] != $itemAfter['bank_account_no'] ? 'text-danger' : ''}} " disabled
                                   value="{{$itemAfter['bank_account_no']}}">
                        </div>
                    </div>
                    {{-- <div class="row form-group">
                        <div class="col-6">
                            <label  class="col-form-label label black-title">
                                {{__('Trạng thái')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label class="ss--switch">
                                    <input type="checkbox" {{$item['is_actived'] == 1 ? 'checked' :''}} disabled>
                                    <span></span>
                                </label>
                            </span>
                            </div>
                        </div>
                        <div class="col-6">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Tài khoản test')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label class="ss--switch">
                                        <input type="checkbox" {{$item['is_test'] == 1 ? 'checked' :''}} disabled>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="form-group col-lg-6">

                    {{--                        // Hạng thành viên--}}
{{--                    <div class="row form-group">--}}
{{--                        <label  class="col-form-label label col-lg-4 black-title">--}}
{{--                            {{__('Hạng thành viên')}}:--}}
{{--                        </label>--}}
{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="rank_member" name="rank_member" class="form-control m-input"--}}
{{--                                   value="{{$item['member_levels_name']}}" disabled>--}}
{{--                            --}}{{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                            --}}{{--                                        <span><i class="flaticon-customer"></i></span></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                      --}}{{--                        // Hạng thành viên 2--}}
{{--                      <div class="row form-group">--}}

{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="rank_member" name="rank_member" class="form-control m-input"--}}
{{--                                   value="{{$itemAfter['member_levels_name']}}" disabled>--}}
{{--                            --}}{{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                            --}}{{--                                        <span><i class="flaticon-customer"></i></span></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}


                    {{--                    // Ngày tạo--}}
{{--                    <div class="row form-group">--}}
{{--                        <label  class="col-form-label label col-lg-4 black-title">--}}
{{--                            {{__('Ngày tạo')}}:--}}
{{--                        </label>--}}
{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="created_at" name="created_at" class="form-control m-input"--}}
{{--                                   value="{{date("d/m/Y",strtotime($item['created_at']))}}" disabled>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                       --}}{{--                    // Ngày tạo 2--}}
{{--                       <div class="row form-group">--}}

{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="created_at" name="created_at" class="form-control m-input"--}}
{{--                                   value="{{date("d/m/Y",strtotime($itemAfter['created_at']))}}" disabled>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    // số điện thoại liên hệ--}}
{{--                    <div class="row form-group">--}}
{{--                        <label  class="col-form-label label col-lg-4 black-title">--}}
{{--                            {{__('Số điện thoại liên hệ')}}: <b class="text-danger">*</b>--}}
{{--                        </label>--}}
{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="phone2" name="phone2" class="form-control m-input" disabled--}}
{{--                                   value="{{$item['phone2']}}" >--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row form-group">--}}

{{--                        <div class="m-input-icon m-input-icon--right">--}}
{{--                            <input type="text" id="phone2" name="phone2" class="form-control m-input text-danger" disabled--}}
{{--                                   value="{{$itemAfter['phone2']}}" >--}}
{{--                        </div>--}}
{{--                    </div>--}}


{{--                        // địa chỉ liên hệ--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Địa chỉ liên hệ')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            @if($item['contact_address'] == null && $item['thuongtru_dis_name'] == null && $item['thuongtru_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control " value="{{$item['contact_address']}}, {{$item['thuongtru_dis_type']}} {{$item['thuongtru_dis_name']}},{{$item['thuongtru_pro_type']}} {{$item['thuongtru_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>

                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            @if($itemAfter['contact_address'] == null && $itemAfter['thuongtru_dis_name'] == null && $itemAfter['thuongtru_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control {{$item['contact_address'] != $itemAfter['contact_address'] || $item['thuongtru_dis_name'] != $itemAfter['thuongtru_dis_name'] || $item['thuongtru_pro_name'] != $itemAfter['thuongtru_pro_name'] ? 'text-danger' : ''}} " value="{{$itemAfter['contact_address']}}, {{$itemAfter['thuongtru_dis_type']}} {{$itemAfter['thuongtru_dis_name']}},{{$itemAfter['thuongtru_pro_type']}} {{$itemAfter['thuongtru_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>
{{--                    // địa chỉ thường trú--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Địa chỉ thường trú')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            @if($item['residence_address'] == null && $item['lienhe_dis_name'] == null && $item['lienhe_pro_name'] == null)
                                <input name="address" class="form-control" value="" disabled>
                            @else
                                <input name="address" class="form-control" value="{{$item['residence_address']}}, {{$item['lienhe_dis_type']}}  {{$item['lienhe_dis_name']}},{{$item['lienhe_pro_type']}} {{$item['lienhe_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>

                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            @if($itemAfter['residence_address'] == null && $itemAfter['lienhe_dis_name'] == null && $itemAfter['lienhe_pro_name'] == null)
                                <input name="address" class="form-control" value="" disabled>
                            @else
                                <input name="address" class="form-control {{$item['residence_address'] != $itemAfter['residence_address'] || $item['lienhe_dis_name'] != $itemAfter['lienhe_dis_name'] || $item['lienhe_pro_name'] != $itemAfter['lienhe_pro_name'] ? 'text-danger' : ''}} " value="{{$itemAfter['residence_address']}}, {{$itemAfter['lienhe_dis_type']}}  {{$itemAfter['lienhe_dis_name']}},{{$itemAfter['lienhe_pro_type']}} {{$itemAfter['lienhe_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>
{{--                    // số cmnd--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Số CMND')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_no" name="ic_no" class="form-control m-input" disabled
                                   value="{{$item['ic_no']}}" >
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_no" name="ic_no" class="form-control m-input {{ $item['ic_no'] != $itemAfter['ic_no'] ? 'text-danger' : ''}} " disabled
                                   value="{{$itemAfter['ic_no']}}" >
                        </div>
                    </div>
{{--                    // nơi cấp cmnd--}}
                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Nơi cấp')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_place" name="ic_place" class="form-control m-input" disabled
                                   value="{{$item['ic_place']}}" >
{{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                                        <span><i class="flaticon-map-location"></i></span></span>--}}
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" id="ic_place" name="ic_place" class="form-control m-input {{ $item['ic_place'] != $itemAfter['ic_place'] ? 'text-danger' : '' }} " disabled
                                   value="{{$itemAfter['ic_place']}}" >
{{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                                        <span><i class="flaticon-map-location"></i></span></span>--}}
                        </div>
                    </div>

                    <div class="row form-group">
                        <label  class="col-form-label label col-lg-4 black-title">
                            {{__('Ngày cấp')}}: <b class="text-danger">*</b>
                        </label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" class="form-control m-input" disabled
                                   value="{{\Carbon\Carbon::parse($item['ic_date'])->format('d-m-Y')}}" >
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-map-location"></i></span></span>--}}
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="ic_date" class="form-control m-input {{ $item['ic_date'] != $itemAfter['ic_date'] ? 'text-danger' : '' }} " disabled
                                   value="{{$itemAfter['ic_date'] != null ? \Carbon\Carbon::parse($itemAfter['ic_date'])->format('d-m-Y') : ''}}" >
                            {{--                            <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                            {{--                                        <span><i class="flaticon-map-location"></i></span></span>--}}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-6">
                            <div class="row">
                                <label  class="col-form-label label col-lg-12 black-title">
                                    {{__('Mặt trước (Ảnh hiện tại)')}}:
                                </label>
                                <div class="col-lg-12">
                                    <input type="hidden" id="ic_front_image" name="ic_front_image"
                                           value="{{$item['ic_front_image']}}">
                                    <input type="hidden" id="ic_front_image_upload" name="ic_front_image_upload"
                                           value="">
                                    <div class="m-widget19__pic">
                                        @if($item['ic_front_image']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah_ic_front_image"
                                                 src="{{$item['ic_front_image']}}" width="220px" height="220px"
                                                 alt="{{__('Hình ảnh')}}"/>
                                        @else
                                            <img class="m--bg-metal  m-image img-sd" id="blah_ic_front_image"
                                                 src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                                 alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                        @endif

                                    </div>
                                    {{--                            <img src="{{$item['ic_front_image']}}" height="auto" width="100%">--}}
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="row">
                                <label  class="col-form-label label col-lg-12 black-title">
                                    {{__('Mặt sau (Ảnh hiện tại)')}}:
                                </label>
                                <div class="col-lg-12">
                                    {{--                            <img src="{{$item['ic_back_image']}}" height="auto" width="100%">--}}
                                    <input type="hidden" id="ic_back_image" name="ic_back_image"
                                           value="{{$item['ic_back_image']}}">
                                    <input type="hidden" id="ic_back_image_upload" name="ic_back_image_upload"
                                           value="">
                                    <div class="m-widget19__pic">
                                        @if($item['ic_back_image']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah_ic_back_image"
                                                 src="{{$item['ic_back_image']}}" width="220px" height="220px"
                                                 alt="{{__('Hình ảnh')}}"/>
                                        @else
                                            <img class="m--bg-metal  m-image img-sd" id="blah_ic_back_image"
                                                 src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                                 alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                        @endif

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <div class="row">
                                <label  class="col-form-label label col-lg-12 black-title {{$item['ic_front_image'] != $itemAfter['ic_front_image'] ? 'text-danger' : ''}} ">
                                    {{__('Mặt trước (Ảnh thay đổi)')}}:
                                </label>
                                <div class="col-lg-12">
                                    <input type="hidden" id="ic_front_image" name="ic_front_image"
                                           value="{{$itemAfter['ic_front_image']}}">
                                    <input type="hidden" id="ic_front_image_upload" name="ic_front_image_upload"
                                           value="">
                                    <div class="m-widget19__pic">
                                        @if($itemAfter['ic_front_image']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah_ic_front_image"
                                                 src="{{$itemAfter['ic_front_image']}}" width="220px" height="220px"
                                                 alt="{{__('Hình ảnh')}}"/>
                                        @else
                                            <img class="m--bg-metal  m-image img-sd" id="blah_ic_front_image"
                                                 src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                                 alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                        @endif

                                    </div>
                                    {{--                            <img src="{{$item['ic_front_image']}}" height="auto" width="100%">--}}
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="row">
                                <label  class="col-form-label label col-lg-12 black-title {{$item['ic_back_image'] != $itemAfter['ic_back_image'] ? 'text-danger' : ''}} ">
                                    {{__('Mặt sau (Ảnh thay đổi)')}}:
                                </label>
                                <div class="col-lg-12">
                                    {{--                            <img src="{{$item['ic_back_image']}}" height="auto" width="100%">--}}
                                    <input type="hidden" id="ic_back_image" name="ic_back_image"
                                           value="{{$itemAfter['ic_back_image']}}">
                                    <input type="hidden" id="ic_back_image_upload" name="ic_back_image_upload"
                                           value="">
                                    <div class="m-widget19__pic">
                                        @if($itemAfter['ic_back_image']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah_ic_back_image"
                                                 src="{{$itemAfter['ic_back_image']}}" width="220px" height="220px"
                                                 alt="{{__('Hình ảnh')}}"/>
                                        @else
                                            <img class="m--bg-metal  m-image img-sd" id="blah_ic_back_image"
                                                 src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                                 alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                        @endif

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--begin::Portlet-->
        <div class="m-portlet__body">
            <div class=" m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.customer-change-request')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <span>{{__('HUỶ')}}</span>
                    </span>
                    </a>

                    @if($itemAfter['status'] == 'new')
                        <a id="btnCancel" href="javascript:void(0)" onclick="cancel()"
                           class="btn btn-danger color_button  son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10" style="background-color: #F64E60 !important;border-color: #F64E60 !important;">
                            <span>
                            <span>{{__('TỪ CHỐI')}}</span>
                            </span>
                        </a>
                        <a id="btnApprove" href="javascript:void(0)"
                           class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span>
                            <span>{{__('DUYỆT')}}</span>
                            </span>
                        </a>
                    @endif

                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="row">
                <div class="col-12">
                    <table class="table ">
                        <thead class="bg">
                            <tr>
                                <th>STT</th>
                                <th>Mã hợp đồng</th>
                                <th>Phụ lục</th>
                                <th>File cập nhật</th>
                                <th class="text-center">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listContract as $key => $item)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$item['customer_contract_code']}}</td>
                                <td>
                                    @if($item['file'] != null)
                                        <a href="{{$item['file']}}" target="_blank">{{$item['file']}}</a>
                                    @endif
                                </td>
                                <td>
                                    @if(isset($listAppendix[$item['appendix_contract_id']]))
                                        @foreach($listAppendix[$item['appendix_contract_id']] as $itemAppendix)
                                            <p class="mb-0"><a href="{{$itemAppendix['file']}}" target="_blank">{{$itemAppendix['file']}}</a></p>
                                        @endforeach
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a id="updateFile" onclick="updateFile('{{$item["appendix_contract_id"]}}')" href="javascript:void(0)" class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                                        <span>
                                            <i class="la la-edit"></i>
                                            <span>Update file</span>
                                        </span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include('admin::customer-change-request.popup.upload')


@stop
@section("after_style")
    {{--    <link rel="stylesheet" href="{{asset('static/backend/css/process.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">

@stop
@section('after_script')
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>--}}
{{--    <script>--}}
{{--        var id_customer = {{$item['customer_id']}}--}}
{{--    </script>--}}
{{--    <script src="{{asset('static/backend/js/admin/customer/script.js')}}" type="text/javascript"></script>--}}

{{--    <script src="{{asset('static/backend/js/admin/customer/vset-script.js?v='.time())}}" type="text/javascript"></script>--}}
    {{-- <script src="{{asset('static/backend/js/admin/customer/vset-list-contract-script.js?v='.time())}}" type="text/javascript"></script> --}}
    <script type="text/template" id="imageFile">
        <div class="list-file mb-0 col-12">
            <input type="hidden" name="arrFile[]" value="{link_hidden}">
            <p>{link}</p>
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
        </div>
    </script>
    <script>
        var RequestChangeHandler = {
            appendixContractId:0,
            customerId:0,
            init(){
                // this.setupCsrfToken();
                // this.setupDropZone();
                this.onBtnChangeReq();


            },
            // setupDropZone(){
            //     $("#dropzoneonecash").dropzone({
            //         url: laroute.route("admin.customer-change-request.uploadAppendixContract1111"),
            //         addRemoveLinks : true,
            //         // maxFilesize: 5,
            //         dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
            //         dictResponseError: 'Error uploading file!',
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         },
            //     });
            //
            // },
            // setupCsrfToken(){
            //     $.ajaxSetup({
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     });
            //
            // },
            onBtnChangeReq(){
                $("#btnApprove").on('click', function(event){
                    event.preventDefault();
                    // alert("change request");
                    RequestChangeHandler.handleApporve();

                });
            },
            makeid(length) {
                var result           = '';
                var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;
                for ( var i = 0; i < length; i++ ) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }
                return result;
            },
            exportPdf(){

                $.ajax({
                // url: laroute.route('admin.customer-change-request.exportPdf', 2),
                url:"{{route('admin.customer-change-request.exportPdf',2)}}",
                method: "GET"
                // dataType: "JSON",

                ,
                success: function (data) {
                    if(data.error == 1){
                        swal('', data.message, "error");
                    }
                    else{
                        swal('', "Bạn đã duyệt thành công", "success");

                    }

                },
                error: function (res) {
                    console.log('error: ', res);
                    var mess_error = '';
                    jQuery.each(res.responseJSON.errors, function (key, val) {
                        mess_error = mess_error.concat(val + '<br/>');
                    });
                    swal.fire('', mess_error, "error");
                }

            });

            },

            handleApporve(){
                const data = @json($itemAfter);
                RequestChangeHandler.customerId = data['customer_id'];

                data['customer_id_hidden'] = data['customer_id'];


                $.ajax({
                    url: laroute.route('admin.customer-change-request.approve'),
                    method: "POST",
                    dataType: "JSON",
                    data:data
                    ,
                    success: function (data) {
                        // RequestChangeHandler.exportPdf();
                        if(data.error == 1){
                            swal('', data.message, "error");
                        }
                        else{
                            swal('', "Bạn đã duyệt thành công", "success").then(function () {
                                location.reload();
                            });

                        }

                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }

                });
            },
            uploadAppendixContract(){
                alert('upload appendix Contract');
                const data = {
                    test:"test"
                };
                $.ajax({
                    url: laroute.route('admin.customer-change-request.uploadAppendixContract111'),
                    method: "POST",
                    dataType: "JSON",
                    data:data
                    ,
                    success: function (data) {
                        // RequestChangeHandler.exportPdf();
                        if(data.error == 1){
                            swal('', data.message, "error");
                        }
                        else{
                            swal('', "Bạn đã duyệt thành công", "success");

                        }

                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }

                });
            },
            saveAppendixContractMap(){
                // var files = new FormData();
                // files.append('appendixContractId', RequestChangeHandler.appendixContractId);
                // let count = 0;
                // $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                //             // file.previewElement.remove();
                //             // files.push(data);
                //              files.append('file-'+i, file);
                //              i++;
                //         });

                // --------------------------------------------

                $.ajax({
                    url: laroute.route('admin.customer-change-request.saveAppendixContractMap'),
                    method: "POST",
                    dataType: "JSON",
                    data:$('#list-file-appendix').serialize(),

                    success: function (data) {
                        if(data.error == 1){
                            swal('', data.message, "error");
                        }
                        else{
                            $('#uploadFile').modal('hide');
                            swal('', data.message, "success").then(function () {
                                location.reload();
                            });

                        }

                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }

                });

            }

        };

        function updateFile(appendix_contract_id) {
            // RequestChangeHandler.appendixContractId = appendix_contract_id;
            $('#appendix_contract_id').val(appendix_contract_id);
            $('#upload-image-cash').empty();
            $('#uploadFile').modal('show');
        }

        function removeImage(e){
            $(e).closest('.list-file').remove();
        }

        $(document).ready(function(){
            RequestChangeHandler.init();
        });


        function dropzone() {
            Dropzone.options.dropzoneonecash = {
                paramName: 'file',
                maxFilesize: 10, // MB
                maxFiles: 20,
                // acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dictRemoveFile: 'Xóa',
                dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
                dictInvalidFileType: 'Tệp không hợp lệ',
                dictCancelUpload: 'Hủy',
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                    var random = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    for (let z = 0; z < 10; z++) {
                        random += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
                },
                init: function () {
                    this.on("success", function (file, response) {
                        var a = document.createElement('span');
                        a.className = "thumb-url btn btn-primary";
                        a.setAttribute('data-clipboard-text', laroute.route('admin.customer-change-request.uploadAppendixContract'));

                        if (file.status === "success") {
                            //Xóa image trong dropzone
                            $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                                file.previewElement.remove();
                            });
                            $('#dropzoneonecash').removeClass('dz-started');
                            //Append vào div image
                            let tpl = $('#imageFile').html();
                            tpl = tpl.replace(/{link}/g, response.file);
                            tpl = tpl.replace(/{link_hidden}/g, response.file);
                            $('#upload-image-cash').append(tpl);
                        }
                    });

                }
            }
        }

        function cancel() {
            swal.fire({
                title: 'Từ chối yêu cầu',
                type: 'question',
                // input: 'textarea',
                // inputPlaceholder: 'Nhập lý do',
                // confirmButtonText: "Đồng ý",
                // cancelButtonText: "Hủy",
                // showCancelButton: true,
                // inputValidator: (value) => {
                //     if (!value) {
                //         return 'Yêu cầu nhập lý do!'
                //     } else if (value.length > 255){
                //         return 'Lý do vượt quá 255 ký tự!'
                //     }
                // }

                html:
                    '<input id="reason_vi" class="swal2-input" placeholder="Nhập lý do hủy (VI)">' +
                    '<input id="reason_en" class="swal2-input" placeholder="Nhập lý do hủy (EN)">',
                // focusConfirm: false,
                // preConfirm: () => {
                //     return [
                //         document.getElementById('reason_vi').value,
                //         document.getElementById('reason_en').value
                //     ]
                // },
                preConfirm: () => {
                    if($('#reason_vi').val() == ''){
                        swal.showValidationError("Yêu cầu nhập lý do hủy (VI)");
                    } else if($('#reason_en').val() == ''){
                        swal.showValidationError("Yêu cầu nhập lý do hủy (EN)");
                    } else if($('#reason_vi').val().length > 255){
                        swal.showValidationError("Lý do hủy (VI) vượt quá 255 ký tự");
                    } else if($('#reason_en').val().length > 255){
                        swal.showValidationError("Lý do hủy (EN) vượt quá 255 ký tự");
                    }
                }
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: laroute.route('admin.customer-change-request.cancelRequest'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            customer_change_id : '{{$itemAfter['customer_change_id']}}',
                            reason_vi: $('#reason_vi').val(),
                            reason_en: $('#reason_en').val()
                        },
                        success: function (res) {
                            if (res.error == true) {
                                swal(res.message,'','error').then(function () {
                                    location.reload();
                                });
                            } else {
                                swal(res.message,'','success').then(function () {
                                    location.reload();
                                });
                            }
                        },
                    });
                }
            })
        }

        dropzone();

    </script>
@stop