@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ THAY ĐỔI THÔNG TIN NHÀ ĐẦU TƯ')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/
        .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > span , .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > input:checked ~ span {
            background: #4fc4ca;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <form autocomplete="off">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                             <i class="la la-th-list"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{__('DANH SÁCH YÊU CẦU THAY ĐỔI THÔNG TIN')}}
                        </h2>

                    </div>
                </div>

                <div class="m-portlet__head-tools nt-class">
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="frmFilter bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" value="{{isset($FILTER['search']) ? $FILTER['search'] : ''}}"
                                           placeholder="{{__('Nhập tên hoặc số điện thoại')}}">
                                </div>
                            </div>
                        </div>
{{--                        <div class="col-lg-3">--}}
{{--                            <div class="form-group m-form__group">--}}
{{--                                <div class="input-group">--}}
{{--                                    <input type="text" class="form-control created" value="{{isset($FILTER['created']) ? $FILTER['created'] : ''}}" name="created" placeholder="Chọn ngày tạo" >--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <select class="form-control" name="status">
                                        <option value="">{{__('Trạng thái')}}</option>
                                        <option value="new" {{isset($FILTER['status']) && $FILTER['status'] == 'new' ? 'selected' : ''}}>Mới</option>
                                        <option value="success" {{isset($FILTER['status']) && $FILTER['status'] == 'success' ? 'selected' : ''}}>Thành công</option>
                                        <option value="cancel" {{isset($FILTER['status']) && $FILTER['status'] == 'cancel' ? 'selected' : ''}}>Từ chối</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button type="submit" class="btn btn-primary color_button btn-search " style="width: 150px">
                                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                </button>
                                <a href="{{route('admin.customer-change-request')}}"
                                   class="btn btn-metal  btn-search padding9x padding9px">
                                    <span><i class="flaticon-refresh"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                            <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                        </div>
                    @endif
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::customer-change-request.list')

                </div><!-- end table-content -->

            </div>
        </form>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@stop
@section('after_script')


    <script src="{{asset('static/backend/js/admin/customer-change-request/script.js?v='.time())}}" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/customer/import-excel.js?v='.time())}}"
            type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
        $('select').select2();
        $('.created').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        @if(isset($FILTER['created']))
        $('.created').val('{{$FILTER['created']}}');
        @else
        $('.created').val('');
        @endif
    </script>
@stop