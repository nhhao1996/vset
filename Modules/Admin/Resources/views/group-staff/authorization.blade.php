@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang('NHÂN VIÊN')</span>
@stop
@section('content')
    <div class="m-portlet" id="form_data">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        @lang('Nhóm nhân viên')
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools nt-class">
                <a href="{{route('admin.group-staff.show', ['id' => $group['group_staff_id']])}}"
                   class="btn m-btn--square btn-secondary  btn-lg">
                                <span>
                                    <span>@lang('NHÂN VIÊN')</span>
                                </span>
                </a>
                <button type="button"
                        class="btn m-btn--square btn-primary btn-default-piospa btn-lg">
                                <span>
                                    <span>@lang('QUYỀN')</span>
                                </span>
                </button>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row form-group">
                <div class="col-lg-12">
                    <label>
                        @lang('Tên nhóm nhân viên')<b class="text-danger">*</b>
                    </label>
                    <input disabled
                           placeholder="Nhập tên nhóm nhân viên"
                           type="text"
                           name="group_name"
                           class="form-control" id="group_name"
                           value="{{$group['group_name']}}">
                    <input
                            type="hidden"
                            class="form-control" id="group_staff_id"
                            value="{{$group['group_staff_id']}}">
                </div>
            </div>
            <div class="form-group">
                <label>
                    @lang('Phân quyền đối với nhóm khách hàng')
                </label>
            </div>
{{--            <div class="row form-group m--margin-left-30">--}}
{{--                <div class="col-lg-12">--}}
{{--                    <label class="m-radio m-radio--bold m-radio--state-success">--}}
{{--                        <input onchange="groupStaff.onChangeType('all')"--}}
{{--                               type="radio"--}}
{{--                               name="type"--}}
{{--                               value="all"--}}
{{--                                {{$group['type'] == 'all' ? 'checked' : ''}}> @lang('Xem tất cả')--}}
{{--                        <span></span>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="row form-group m--margin-left-30">
                <div class="col-lg-3">
                    <label class="m-radio m-radio--bold m-radio--state-success" style="cursor: default">
                        <input onchange="groupStaff.onChangeType('group')"
                               type="radio"
                               name="type"
                               value="group"
{{--                                {{$group['type'] == 'group' ? 'checked' : ''}}--}}
                               checked
                        > @lang('Xem theo group khách hàng')
                        <span></span>
                    </label>
                </div>
                <div class="col-lg-9">
                    <select
{{--                            {{$group['type'] == 'group' ? '' : 'disabled'}}--}}
                            style="width:100%"
                            class="js-example-basic-multiple"
                            name="states2[]"
                            multiple="multiple"
                            data-placeholder="Chọn nhóm khách hàng"
                            id="group_customer">
                        @foreach($option['group'] as $item)
                            <option value="{{$item['group_customer_id']}}"
                                    {{isset($mapCustomer[$item['group_customer_id']])
                                        && $group['type'] == 'group' ? 'selected' : ''}}>
                                {{$item['group_name']}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
{{--            <div class="row form-group m--margin-left-30">--}}
{{--                <div class="col-lg-3">--}}
{{--                    <label class="m-radio m-radio--bold m-radio--state-success">--}}
{{--                        <input--}}
{{--                                onchange="groupStaff.onChangeType('detail')"--}}
{{--                                type="radio"--}}
{{--                                name="type"--}}
{{--                                value="detail"--}}
{{--                                {{$group['type'] == 'detail' ? 'checked' : ''}}> @lang('Xem chi tiết')--}}
{{--                        <span></span>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--                <div class="col-lg-9">--}}
{{--                    <select {{$group['type'] == 'detail' ? '' : 'disabled'}}--}}
{{--                            style="width:100%"--}}
{{--                            class="js-example-basic-multiple"--}}
{{--                            name="states[]"--}}
{{--                            multiple--}}
{{--                            id="customer">--}}
{{--                        @foreach($option['customer'] as $item)--}}
{{--                            <option value="{{$item['customer_id']}}"--}}
{{--                                    {{isset($mapCustomer[$item['customer_id']])--}}
{{--                                        && $group['type'] == 'detail' ? 'selected' : ''}}>--}}
{{--                                {{$item['full_name']}}--}}
{{--                            </option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="row form-group">
                <div class="col-lg-12">
                    <button onclick="groupStaff.updateAuthorization()"
                            type="button"
                            class="btn btn-primary btn-default-piospa float-right m--margin-left-10">
                        @lang('Lưu thông tin')
                    </button>
                    <a href="{{route('admin.group-staff')}}" class="btn btn-metal float-right ">
                        <i class="la la-arrow-left"></i>
                        @lang('Hủy')
                    </a>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="unique" value="{{$unique}}">
    <input type="hidden" id="show" value="1">
@endsection
@section("after_style")
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/group-staff/script.js?v='.time())}}"
            type="text/javascript"></script>
@stop