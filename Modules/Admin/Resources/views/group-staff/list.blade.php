<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Tên nhóm')}}</th>
            <th class="tr_thead_list text-center">{{__('Số thành viên')}}</th>
{{--            <th class="tr_thead_list text-center">{{__('Trạng thái')}}</th>--}}
            <th class="tr_thead_list text-center">{{__('Hoạt động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    <td>
                        {{$key + 1}}
                    </td>
                    <td>
                        {{$item['group_name']}}
                    </td>
                    <td class="text-center">
                        {{$item['total']}}
                    </td>
{{--                    <td class="text-center">--}}
{{--                       <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label class="ss--switch">--}}
{{--                                    <input type="checkbox" disabled="" class="manager-btn" name=""--}}
{{--                                    {{$item['is_active'] == 1 ? 'checked' : ''}}>--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                            </span>--}}
{{--                    </td>--}}
                    <td class="text-center m--width-150">
                        @if(in_array('admin.group-staff.show', session('routeList')))
                            <a href="{{route('admin.group-staff.show', [
                                        'id' => $item['group_staff_id']
                                    ])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="Chi tiết"><i class="flaticon-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.group-staff.edit', session('routeList')))
                            <a href="{{route('admin.group-staff.edit', [
                                        'id' => $item['group_staff_id']
                                    ])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="Cập nhật"><i class="la la-edit"></i>
                            </a>
                        @endif
                        @if(in_array('admin.group-staff.destroy', session('routeList')))
                            <button type="button"
                                    onclick="groupStaff.remove(this, '{{ $item['group_staff_id'] }}')"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="Xóa"><i class="la la-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>

    </table>
</div>
@if(isset($LIST))
    {{ $LIST->links('helpers.paging') }}
@endif
