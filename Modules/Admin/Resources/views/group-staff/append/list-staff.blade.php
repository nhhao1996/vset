<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            @if($isItemSelected == 0)
                <th class="m--width-20 text-center">
                    <label class="m-checkbox m-checkbox--solid m-checkbox--state-success m--margin-bottom-15 m--margin-left-10">
                        <input type="checkbox" onclick="groupStaff.checkedAllItem(this)">
                        <span></span>
                    </label>
                </th>
            @else
                <th class="m--width-10 text-center">
                    #
                </th>
            @endif

            <th>
                @lang('Nhân viên')
            </th>
            <th>
                @lang('Tài khoản')
            </th>
            <th>
                @lang('Email')
            </th>
            <th>
                @lang('Trạng thái')
            </th>
            @if($isItemSelected == 1 && $show == 0)
                <th class="text-center">
                    @lang('Hành động')
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @if(isset($list))
            <?php $color = ["success", "brand", "danger", "accent", "warning", "metal", "primary", "info"]; ?>
            @foreach($list as $key => $item)
                @php($num = rand(0,7))
                <tr>
                    @if($isItemSelected == 0)
                        <td>
                            <div class="m-card-user m-card-user--sm">
                                <div class="m-card-user__pic m--height-40">
                                </div>
                                <div class="m-card-user__details">
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--state-success m--margin-bottom-15">
                                        <input onclick="groupStaff.checkedOneItem(this, '{{$item['staff_id']}}')"
                                               type="checkbox" class="checkbox_item"
                                               {{in_array($item['staff_id'], $itemTemp) ? 'checked' : ''}}
                                               value="{{$item['staff_id']}}">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </td>
                    @else
                        <td>
                            <div class="m-card-user m-card-user--sm">
                                <div class="m-card-user__pic m--height-40">
                                </div>
                                <div class="m-card-user__details">
                                    {{($page - 1) * $perpage + ($key + 1)}}
                                </div>
                            </div>
                        </td>
                    @endif
                    <td>
                        @if($item['staff_avatar']!=null)
                            <div class="m-card-user m-card-user--sm">
                                <div class="m-card-user__pic">
                                    <img src="{{$item['staff_avatar']}}"
                                         onerror="this.onerror=null;this.src='https://placehold.it/40x40/00a65a/ffffff/&text=' + '{{substr(str_slug($item['name']),0,1)}}';"
                                         class="m--img-rounded m--marginless" alt="photo" width="40px"
                                         height="40px">
                                </div>
                                <div class="m-card-user__details">
                                    {{$item['name']}}
                                </div>
                            </div>
                        @else
                            <span style="width: 150px;">
                                        <div class="m-card-user m-card-user--sm">
                                            <div class="m-card-user__pic">
                                                <div class="m-card-user__no-photo m--bg-fill-{{$color[$num]}}">
                                                    <span>
                                                        {{substr(str_slug($item['name']),0,1)}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="m-card-user__details">
                                                {{$item['name']}}
                                            </div>
                                        </div>
                                    </span>
                        @endif
                    </td>
                    <td>
                        <div class="m-card-user m-card-user--sm">
                            <div class="m-card-user__pic m--height-40">
                            </div>
                            <div class="m-card-user__details">
                                {{$item['account']}}
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="m-card-user m-card-user--sm">
                            <div class="m-card-user__pic m--height-40">
                            </div>
                            <div class="m-card-user__details">
                                {{$item['email']}}
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="m-card-user m-card-user--sm">
                            <div class="m-card-user__pic m--height-40">
                            </div>
                            <div class="m-card-user__details">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label class="ss--switch">
                                        <input type="checkbox"
                                               disabled="" class="manager-btn"
                                               {{$item['is_actived'] == 1 ? 'checked' : ''}}>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </td>
                    @if($isItemSelected == 1 && $show == 0)
                        <td class="text-center">
                            <button onclick="groupStaff.removeItem('{{$item['staff_id']}}', {{$page}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="Xóa">
                                <i class="la la-trash"></i>
                            </button>
                        </td>
                    @endif
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

@if(isset($list))
    @if($isItemSelected == 0)
        {{$list->links('admin::group-staff.helpers.paging-list-item')}}
    @else
        {{$list->links('admin::group-staff.helpers.paging-list-item-selected')}}
    @endif
@endif