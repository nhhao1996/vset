@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang('QUẢN LÝ NHÓM NHÂN VIÊN')</span>
@stop
@section('content')
    <div class="m-portlet" id="form_data">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-server"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        @lang('Nhóm nhân viên')
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools nt-class">
                <button type="button"
                        class="btn m-btn--square btn-primary btn-default-piospa btn-lg">
                                <span>
                                    <span>@lang('NHÂN VIÊN')</span>
                                </span>
                </button>
                <button type="button"
                        class="btn m-btn--square btn-secondary btn-lg not-pointer">
                                <span>
                                    <span>@lang('QUYỀN')</span>
                                </span>
                </button>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row form-group">
                <div class="col-lg-12">
                    <label>
                        @lang('Tên nhóm nhân viên')<b class="text-danger">*</b>
                    </label>
                    <input placeholder="Nhập tên nhóm nhân viên"
                           type="text"
                           name="group_name"
                           class="form-control" id="group_name">
                </div>
            </div>
            <div class="form-group m--margin-bottom-0">
                <label>
                    @lang('Danh sách nhân viên')
                </label>
            </div>
            <div class="row form-group">
                <div class="col-lg-4">
                    <input placeholder="Nhập tên hoặc email"
                           type="text"
                           name=""
                           class="form-control keyword" id="">
                </div>
                <div class="col-lg-3">
                    <select name="" id=""
                            class="form-control is_actived">
                        <option value="">
                            @lang('Chọn trạng thái')
                        </option>
                        <option value="1">
                            @lang('Hoạt động')
                        </option>
                        <option value="0">
                            @lang('Tạm ngưng')
                        </option>
                    </select>
                </div>
                <div class="col-lg-3">
                    <button onclick="groupStaff.loadItemSelected()"
                            type="button"
                            class="btn btn-primary btn-default-piospa">
                        @lang('Tìm kiếm')
                    </button>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <button type="button"
                            class="btn btn-primary btn-default-piospa m--float-right btn m-btn--pill btn-primary m-btn m-btn--custom"
                            onclick="groupStaff.showModalItem()"
                            data-toggle="modal" data-target="#modal_staff">
                        <i class="flaticon-add-circular-button"></i>
                        @lang('Thêm')
                    </button>
                </div>
            </div>
            <div class="form-group" id="div_list_item_selected">
                <table class="table table-striped m-table ss--header-table">
                    <thead>
                    <tr class="ss--nowrap">
                        <th>#</th>
                        <th>
                            @lang('Nhân viên')
                        </th>
                        <th>
                            @lang('Tài khoản')
                        </th>
                        <th>
                            @lang('Email')
                        </th>
                        <th>
                            @lang('Trạng thái')
                        </th>
                        <th>
                            @lang('Hành động')
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="row form-group">
                <div class="col-lg-12">
                    <button onclick="groupStaff.store()"
                            type="button"
                            class="btn btn-primary btn-default-piospa float-right m--margin-left-10">
                        @lang('Lưu thông tin')
                    </button>
                    <a href="{{route('admin.group-staff')}}" class="btn btn-metal float-right ">
                        <i class="la la-arrow-left"></i>
                        @lang('Hủy')
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('admin::group-staff.popup.staff')
    <input type="hidden" id="unique" value="{{$unique}}">
    <div id="div_modal"></div>
@endsection
@section("after_style")
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/group-staff/script.js?v='.time())}}"
            type="text/javascript"></script>
@stop