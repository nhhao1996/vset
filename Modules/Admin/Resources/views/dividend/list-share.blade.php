<div class="table-responsive p-4" style="background-color:#a9e2e685;height:500px">
    
        <div class="row py-4 row-stock">
            <div class="col-lg-3 item-stock d-flex align-items-center">
                <div class=" m-form__group">
                    <div class="input-group bold font-weight-bold">
                        Cổ phiếu VSG <br>                        
                        Ngày phát Cổ tức: 01/02/2020
                        Thời gian nhận dự kiến: 1 tháng
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 item-stock d-flex align-items-end">
                <div class=" m-form__group">
                    <div class="input-group font-weight-bold">
                        Phương thức
                        Tiền mặt
                        Cổ phiếu                      
                    </div>
                </div>
            </div>
            <div class="col-lg-3 item-stock d-flex align-items-end">
                <div class=" m-form__group">
                    <div class="input-group font-weight-bold">
                        51% (51000VNĐ)
                        20:1                                           
                    </div>
                </div>
            </div>
            <div class="col-lg-3 item-stock d-flex align-items-start">
                <div class=" m-form__group">
                    <div class="input-group font-weight-bold row">
                        <a href="http://localhost:8000/admin/product/add" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm col-5">
                            <span>                               
                                <span>Sửa</span>
                            </span>
                        </a>
                        <a href="http://localhost:8000/admin/product/add" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm ml-4 col-5">
                            <span>                             
                                <span>Upload</span>
                            </span>
                        </a>                                         
                        <a href="http://localhost:8000/admin/product/add" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm col-12 mt-3">
                            <span>                             
                                <span>Upload</span>
                            </span>
                        </a>   
                    </div>
                </div>
            </div>
            
              
        </div>

        @if (session('status'))
            <div class="alert alert-success alert-dismissible">
                <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
            </div>
        @endif
    
   
</div>

{{--.--}}