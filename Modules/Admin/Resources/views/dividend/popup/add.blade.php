<form id="frmAddDividend">
    <input type="hidden" name="stock_id" value="{{$stockPublish->stock_id}}">
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title ss--title m--font-bold"><i
                            class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                        </i>Thêm cổ tức</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group ">
                        <label class="black-title ">Ngày phát cổ tức</label>
                        <input name="date_publish" class="form-control " placeholder="Nhập phát cổ tức" />
                    </div>
                    <div style="width:35%" class="form-group m-form__group ">
                        <label class="black-title ">Phương thức chia cổ tức</label>


                    </div>
                    <div class="form-group m-form__group row">
                        <label class="black-title col-2 d-flex align-items-center">Tiền mặt</label>

                        <input  type="text" name="stock_money_rate" class="form-control col-5 number-percent"
                            placeholder="Nhập cổ tức (%)" />
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="black-title col-2 d-flex align-items-center">Cổ phiếu</label>
                        <input  type="text" id="stock1" name="stock_bonus_before" class="form-control col-2 number-int"
                            placeholder="Tỷ lệ chia" />
                        <span class="col-1 d-flex align-items-center">:</span>
                        <input  type="text" id="stock2" name="stock_bonus_after"
                            class="form-control col-2 float-right number-int" />

                    </div>




                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                        <div class="m-form__actions m--align-center">
                            <button data-dismiss="modal"
                                class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-arrow-left"></i>
                                    <span>Hủy</span>
                                </span>
                            </button>
                            <button id="btnAdd" onclick="DividendHandler.add()" type="button"
                                class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>Thêm</span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>


</div>
<div class="modal-footer">
    {{-- <h1>Hello </h1> --}}
    {{-- <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>Hủy</span>
                            </span>
                        </button>
                        <button id="btnPublishPopup" type="button"  class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>Phát hành</span>
                            </span>
                        </button>
                    </div>
                </div> --}}
</div>
</div>
</div>
</div>
</form>
