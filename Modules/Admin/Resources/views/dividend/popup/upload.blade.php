<div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>UPLOAD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="list-file-dividend">
                    {{ csrf_field() }}
                    <input  name="stock_history_divide_id" type="hidden">
                    <div class="form-group noselector cash">
                        <div class="form-group m-form__group pt-3">
                            <label>{{__('File phụ lục')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('admin.dividend.upload')}}" id="dropzoneonecash">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('File')}}
                                    </h3>
                                    <span>{{__('Chọn file')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image-cash">
                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>
                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>
                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>
                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298498506062021_file.docx</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>

                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298499306062021_file.jpg">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/8162298499306062021_file.jpg</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>

                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/5162298499606062021_file.jpg">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/5162298499606062021_file.jpg</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>

                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/4162298499806062021_file.jpg">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/4162298499806062021_file.jpg</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>

                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/2162298500606062021_file.jpg">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/2162298500606062021_file.jpg</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>

                            <div class="list-file d-flex mb-0 col-12">
                                <input type="hidden" name="arrFile[]" value="https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/6162298501006062021_file.docx">
                                <p>https://traiphieu-cms.dev.piotech.xyz/uploads/admin/dividend/20210606/6162298501006062021_file.docx</p>
                                <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
                            </div>
                        </div>
                    </div>
{{--                    <div class="form-group m-form__group row" id="upload-image-cash"></div>--}}
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>HỦY</span>
                            </span>
                        </button>
                        <button type="button" onclick="DividendHandler.saveFile()" class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>LƯU THÔNG TIN</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>