<div class="table-responsive p-4" style="background-color:#a9e2e685;height:500px">
   
    @foreach($LIST as  $item)
    
    <div class="row py-4 row-stock">
        <div class="col-lg-3 item-stock d-flex align-items-center">
            <div class=" m-form__group mt-0">
                <div class="input-group bold font-weight-bold">
                    <p class="w-100">Cổ phiếu {{$stockConfig->stock_config_code}} </p>
                    <p class="w-100">Ngày phát Cổ tức: <span id="datepublish-{{$item->stock_history_divide_id}}">{{\Carbon\Carbon::parse($item->date_publish)->format("d-m-Y")}}</span></p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 item-stock d-flex align-items-end">
            <div class=" m-form__group">
                <div class="input-group font-weight-bold">
                    Phương thức <br>
                    Tiền mặt    <br>
                    Cổ phiếu
                </div>
            </div>
        </div>
        <div class="col-lg-3 item-stock d-flex align-items-end">
            <div class=" m-form__group">
                <div class="input-group font-weight-bold">
                    <br>
                    {{number_format($item->stock_money_rate,2)}}% <br>
                    @if ($item->stock_bonus_before !== null && $item->stock_bonus_after !== null)
                        {{$item->stock_bonus_before}}:{{$item->stock_bonus_after}}
                    @endif
                </div>
            </div>
        </div>        
        <div class="col-lg-3 item-stock d-flex align-items-start">
{{--            <div class=" m-form__group">--}}
                <div class="row " style="width:76%">
{{--                    <a--}}
{{--                        onclick="DividendHandler.showPopupEdit({{$item->stock_history_divide_id}})"                        --}}
{{--                        class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm  btnAction float-left btnEdit"--}}
{{--                        style="color:#fff !important;"--}}
{{--                    >--}}
{{--                        <span>--}}
{{--                            <span >Sửa</span>--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                    <a onclick="DividendHandler.showPopupUpload({{$item->stock_history_divide_id}})" href="javascript:void(0)"--}}
{{--                        class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm ml-2 btnAction btnUpload ml-auto">--}}
{{--                        <span>--}}
{{--                            <span>Upload</span>--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                    <a href="http://localhost:8000/admin/product/add"--}}
{{--                    class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm mt-3 w-100--}}
{{--                    ">--}}
{{--                    <span>--}}
{{--                        <span>Chia cổ tức</span>--}}
{{--                    </span>--}}
{{--                </a>--}}
{{--                    todo: Hide Chia cổ tức--}}
                    @if($item->is_active == 1)
                    <a onclick="DividendHandler.showPopupUpload({{$item->stock_history_divide_id}})" href="javascript:void(0)"
                       class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm ml-2 btnAction btnUpload ml-auto w-100">
                        <center><span>Upload</span></center>
                    </a>

                    <a  href="javascript:void(0)"
                       class="btn btn-metal m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm mt-2 btnAction btnUpload ml-auto w-100">
                        <center><span>Cổ tức đã chia</span></center>
                    </a>
                    @else
                        <a
                                onclick="DividendHandler.showPopupEdit({{$item->stock_history_divide_id}})"
                                class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm  btnAction float-left btnEdit"
                                style="color:#fff !important;width:48%"
                        >

                            <center> <span >Sửa</span></center>

                        </a>
                        <a style="width:48%" onclick="DividendHandler.showPopupUpload({{$item->stock_history_divide_id}})" href="javascript:void(0)"
                           class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm ml-2 btnAction btnUpload ml-auto">
                            <center><span>Upload</span></center>
                        </a>

                    @endif

{{--                    <a href="http://localhost:8000/admin/product/add"--}}
{{--                       class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm mt-3 w-100--}}
{{--                    ">--}}
{{--                    <span>--}}
{{--                        <span>Chia cổ tức</span>--}}
{{--                    </span>--}}
{{--                    </a>--}}
                </div>
{{--            </div>--}}
        </div>

    </div>
    @endforeach

    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
            <strong>{{ __('Success') }} : </strong> {!! session('status') !!}.
        </div>
    @endif


</div>

{{-- . --}}
