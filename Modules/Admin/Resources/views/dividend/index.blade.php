@extends('layout')
@section('after_style')
    <link rel="stylesheet" type="text/css" href="{{ asset('static/backend/css/customize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static/backend/css/sinh-custom.css') }}">
@endsection
@section('title_header')
    <span class="title_header"><img src="{{ asset('uploads/admin/icon/icon-product.png') }}" alt="" style="height: 20px;">
        {{ __('QUẢN LÝ CỔ PHIẾU') }}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .item-stock {
            line-height: 3rem;
            height: 100px;

        }
        .col-filter-time{
            height:60px;
        }

        .btnAction {
            width: 6rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
        }

        .row-stock {
            border-bottom: 4px solid #fff;
        }

    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH CỔ TỨC')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
            {{-- Begin: Tab--------------- --}}
            <div class="d-flex ml-auto">
                <div class="m-portlet__head-caption p-3 color_button">
                    <div class="m-portlet__head-title">

                        <a href="javascript::void(0)" class="m-portlet__head-text">
                            THÔNG TIN
                        </a>

                    </div>
                </div>            
               
            </div>

        </div>   
      
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade p-3" id="all" role="tabpanel" aria-labelledby="all-tab">
                <div class="m-portlet__body">

                    <div class="table-content">
                        {{-- @include('admin::withdraw-request.list-all') --}}
                    </div><!-- end table-content -->
                </div>
            </div>
            <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                <div class="m-portlet__body">
                    
                    <div class="table-content m--padding-top-10">
                        <a onclick="DividendHandler.showPopup()" href="javascript:void(0)" class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button  mb-3 float-right text-center">
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span>Thêm</span>
                        </span>
                        </a>
{{--                        <a onclick="DividendHandler.showPopup()" href="javascript:void(0)"--}}
{{--                            class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm  btnAction float-right text-center mb-4">--}}
{{--                            <span>--}}
{{--                                <span>Thêm</span>--}}
{{--                            </span>--}}
{{--                        </a>--}}
                        <div class="table-responsive p-4" style="background-color:#a9e2e685;">

                            <div class="row py-0 row-stock">
                                <div class="col-lg-12 item-stock col-filter-time  d-flex align-items-center">
                                    <div class="form-group m-form__group">
                                        <div class="input-group">
                                            <label class="font-weight-bold">Thời gian</label>
                                            <input onchange="DividendHandler.search(this)" type="text"
                                                class="form-control datepicker-year ml-4 mt-0" name="search"
                                                placeholder="Chọn thời gian">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-dividend">
                            @include('admin::dividend.list')
                        </div>
                    </div><!-- end table-content -->
                </div>
            </div>
            <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                <div class="m-portlet__body">
                    <form class="frmFilter bg">
                        <div class="row padding_row">
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search"
                                            placeholder="{{ __('Nhập mã yêu cầu, mã hợp đồng hoặc tên khách hàng') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <select class="form-control select2-fix" name="withdraw_request_status">
                                            <option value="">{{ __('Chọn trạng thái') }}</option>
                                            <option value="new">{{ __('Mới') }}</option>
                                            <option value="inprocess">{{ __('Đang xử lý') }}</option>
                                            <option value="confirm">{{ __('Xác nhận') }}</option>
                                            <option value="done">{{ __('Hoàn thành') }}</option>
                                            <option value="cancel">{{ __('Hủy') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <button class="btn btn-primary color_button btn-search">
                                        {{ __('TÌM KIẾM') }} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                    </button>
                                    <a href="{{ route('admin.withdraw-request') }}"
                                        class="btn btn-metal  btn-search padding9x">
                                        <span>
                                            <i class="flaticon-refresh"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible">
                                <strong>{{ __('Success') }} : </strong> {!! session('status') !!}.
                            </div>
                        @endif

                        @include('admin::dividend.list')
                    </form>
                    <div class="table-content m--padding-top-30">
                        {{-- @include('admin::withdraw-request.list-commission') --}}
                    </div><!-- end table-content -->
                </div>
            </div>
            <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                <div class="m-portlet__body">
                    <form class="frmFilter bg">
                        <div class="row padding_row">
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control daterange-picker" name="search"
                                            placeholder="{{ __('Nhập mã yêu cầu hoặc tên khách hàng') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <select class="form-control select2-fix" name="withdraw_request_status">
                                            <option value="">{{ __('Chọn trạng thái') }}</option>
                                            <option value="new">{{ __('Mới') }}</option>
                                            <option value="inprocess">{{ __('Đang xử lý') }}</option>
                                            <option value="confirm">{{ __('Xác nhận') }}</option>
                                            <option value="done">{{ __('Hoàn thành') }}</option>
                                            <option value="cancel">{{ __('Hủy') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <button class="btn btn-primary color_button btn-search">
                                        {{ __('TÌM KIẾM') }} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                    </button>
                                    <a href="{{ route('admin.withdraw-request') }}"
                                        class="btn btn-metal  btn-search padding9x">
                                        <span>
                                            <i class="flaticon-refresh"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible">
                                <strong>{{ __('Success') }} : </strong> {!! session('status') !!}.
                            </div>
                        @endif
                        {{-- @include('admin::withdraw-request.list-stock') --}}


                    </form>
                    <div class="table-content m--padding-top-30">
                        {{-- @include('admin::withdraw-request.list-stock') --}}
                    </div><!-- end table-content -->
                </div>
            </div>
        </div>
        <!-- !tab -->

    </div>
    {{-- todo: Table------ --}}
    @include('admin::dividend.popup.add')
    @include('admin::dividend.popup.edit')
    @include('admin::dividend.popup.upload')
    {{-- end: Table------ --}}
@endsection
@section('after_script')
<script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{ asset('static/backend/js/admin/dividend/script.js?v=' . time()) }}" type="text/javascript">
    </script>
    <script type="text/template" id="imageFile">
        <div class="list-file d-flex mb-0 col-12">
            <input type="hidden" name="arrFile[]" value="{link_hidden}">
            <p>{link}</p>
            <span class="delete-img-sv ml-auto" style="display: block;">
                <a href="javascript:void(0)" onclick="removeImage(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
        </div>
    </script>
    <script>
        // todo: Config----------
        function removeImage(e){
            $(e).closest('.list-file').remove();
        }
        function dropzone() {
    Dropzone.options.dropzoneonecash = {
        paramName: 'file',
        maxFilesize: 10, // MB
        maxFiles: 20,
        // acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dictRemoveFile: 'Xóa',
        dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
        dictInvalidFileType: 'Tệp không hợp lệ',
        dictCancelUpload: 'Hủy',
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
            var random = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (let z = 0; z < 10; z++) {
                random += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
        },
        init: function () {
            this.on("success", function (file, response) {
                var a = document.createElement('span');
                a.className = "thumb-url btn btn-primary";
                a.setAttribute('data-clipboard-text', laroute.route('admin.customer-change-request.uploadAppendixContract'));

                if (file.status === "success") {
                    //Xóa image trong dropzone
                    $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                        file.previewElement.remove();
                    });
                    $('#dropzoneonecash').removeClass('dz-started');
                    //Append vào div image
                    let tpl = $('#imageFile').html();
                    tpl = tpl.replace(/{link}/g, response.file);
                    tpl = tpl.replace(/{link_hidden}/g, response.file);
                    $('#upload-image-cash').append(tpl);
                }
            });

        }
    }
}
        dropzone();
        // !---------------------

        var LIST=@json($LIST);
        var LISTFILE = @json($LISTFILE);
        $('.select2-fix').select2();
        $(".m_selectpicker").selectpicker();
        $('.datepicker-year').each(function() {
            $(this).datepicker({
                autoclose: true,
                format: " yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $(this).datepicker('clearDates');
        });
        var datePublish = $("[name='date_publish']").datepicker({
                format:'dd-mm-yyyy'
        });



    </script>
@stop
