@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ YÊU CẦU RÚT TIỀN CỔ PHIẾU')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT YÊU CẦU RÚT TIỀN CỔ PHIẾU')}}
                    </h2>

                </div>
            </div>
            @if(in_array('admin.withdraw-request.change-status', session('routeList')))
                @if($detail['withdraw_request_status'] == 'new')
                    <div class="m-portlet__head-tools">
                        <button type="button" {{$totalBonus - $detail['withdraw_request_amount'] < 0 ? 'disabled' :''}} onclick="withrawRequest.changeStatusStock('inprocess')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                            <span class="text-white">
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                        </button>

                        <a href="javascript:void(0)" onclick="withrawRequest.changeStatusStock('cancel')" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                            <span class="text-white">
                                <span class="zxrem">  {{__('Không xác nhận')}}</span>
                            </span>
                        </a>
                    </div>
                @elseif($detail['withdraw_request_status'] == 'inprocess')
                    <div class="m-portlet__head-tools">
                        <a href="javascript:void(0)" onclick="withrawRequest.changeStatusStock('done')" {{$totalBonus - $detail['withdraw_request_amount'] < 0 ? 'disabled' :''}}  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                            <span class="text-white">
                                <span class="zxrem">  {{__('Hoàn thành')}}</span>
                            </span>
                        </a>
                        <a href="javascript:void(0)" onclick="withrawRequest.changeStatusStock('cancel')" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                            <span class="text-white">
                                <span class="zxrem">  {{__('Không xác nhận')}}</span>
                            </span>
                        </a>
                    </div>
                @endif
            @endif

        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Mã yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['withdraw_request_group_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Loại yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control"
                               @if($detail['withdraw_request_group_type'] == 'stock')
                                   value="Ví cổ phiếu"
                               @elseif($detail['withdraw_request_group_type'] == 'dividend')
                                   value="Ví cổ tức"
                               @endif

                                   disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số dư khả dụng (vnđ):
                        </label>
                        <div class="col-lg-8">
{{--                            <input class="form-control text-danger" value="{{number_format($detail['available_balance'])}}" disabled="">--}}
                            <input class="form-control {{(double)$totalBonus >= (double)$detail['withdraw_request_amount'] ? 'text-success' : 'text-danger'}}" value="{{number_format($totalBonus)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tiền yêu cầu rút (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount'])}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Phí rút tiền (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control text-danger" value="{{number_format($detail['withdraw_free_amount'])}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tiền cần chuyển khoản (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount'] - $detail['withdraw_free_amount'])}}" disabled="">
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Thời gian yêu cầu rút:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['withdraw_request_time']}} {{$detail['withdraw_request_day']}}/{{$detail['withdraw_request_month']}}/{{$detail['withdraw_request_year']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Thời gian hoàn thành yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            @if($detail['withdraw_request_status'] != 'done')
                                <input class="form-control" value="" disabled="">
                            @else
                                <input class="form-control" value="{{\Carbon\Carbon::parse($detail['updated_at'])->format('H:i:s d/m/Y')}}" disabled="">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Trạng thái yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control"
                                   @if($detail['withdraw_request_status'] == 'new')
                                   value="Mới"
                                   @elseif($detail['withdraw_request_status'] == 'inprocess')
                                   value="Đang xử lý"
                                   @elseif($detail['withdraw_request_status'] == 'confirm')
                                   value="Xác nhận"
                                   @elseif($detail['withdraw_request_status'] == 'done')
                                   value="Hoàn thành"
                                   @elseif($detail['withdraw_request_status'] == 'cancel')
                                   value="Hủy"
                                   @endif

                                   disabled="">
                        </div>
                    </div>
                    @if($detail['withdraw_request_status'] == 'cancel')
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Lý do
                            </label>
                            <div class="col-lg-8">
                                <textarea class="form-control" disabled>{{$detail['reason']}}</textarea>
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Họ và tên nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số chứng minh nhân dân:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_ic_no']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số điện thoại nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_phone']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Email nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_email']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Địa chỉ thường trú nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
{{--                            <input class="form-control" value="{{$detail['customer_residence_address']}}" disabled="">--}}
                            @if($detail['residence_address'] == null && $detail['lienhe_dis_name'] == null && $detail['lienhe_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control" value="{{$detail['residence_address']}}, {{$detail['lienhe_dis_type']}} {{$detail['lienhe_dis_name']}}, {{$detail['lienhe_pro_type']}} {{$detail['lienhe_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tên ngân hàng nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bank_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tài khoản ngân hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bank_account_no']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tên tài khoản ngân hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bank_account_name']}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="withdraw_request_group_type" value="{{$detail['withdraw_request_group_type']}}">
        <input type="hidden" id="withdraw_request_group_id" value="{{$detail['withdraw_request_group_id']}}">
        <input type="hidden" id="available_balance_after" value="{{$totalBonus}}">
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.withdraw-request.stock')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('QUAY LẠI ')}}</span>
                    </span>
                    </a>
                </div>
            </div>
        </div>

{{--            <div class="m-portlet__body">--}}
{{--                <ul class="nav nav-tabs" role="tablist">--}}
{{--                    <li class="nav-item active">--}}
{{--                        <a class="nav-link active show" data-toggle="tab" href="#" data-target="#list_withraw_request">{{_('Danh sách yêu cầu đã rút tiền')}}</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link " data-toggle="tab" href="#" data-target="#customer_contract_interest_by_month">{{_('Lãi suất hợp đồng theo tháng')}}</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link " data-toggle="tab" href="#" data-target="#customer_contract_interest_by_date">{{_('Lãi suất hợp đồng theo ngày')}}</a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--                <div class="tab-content">--}}
{{--                    <div class="tab-pane active" id="list_withraw_request" role="tabpanel">--}}
{{--                        <div class="form-group col-lg-12 list_withraw_request p-0">--}}

{{--                        </div>--}}
{{--                        <input type="hidden" id="page_withraw_request" >--}}
{{--                    </div>--}}
{{--                    <div class="tab-pane" id="customer_contract_interest_by_month" role="tabpanel">--}}
{{--                        <div class="form-group col-lg-12 customer_contract_interest_by_month">--}}

{{--                        </div>--}}
{{--                        <input type="hidden" id="page_customer_contract_interest_by_month">--}}
{{--                    </div>--}}
{{--                    <div class="tab-pane" id="customer_contract_interest_by_date" role="tabpanel">--}}
{{--                        <div class="form-group col-lg-12 customer_contract_interest_by_date">--}}

{{--                        </div>--}}
{{--                        <input type="hidden" id="page_customer_contract_interest_by_date">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/withdraw-request/script.js?v='.time())}}" type="text/javascript"></script>
{{--    <script>--}}
{{--            withrawRequest.listWithrawRequest(1);--}}
{{--            withrawRequest.listCustomerContractInterestMonth(1);--}}
{{--            withrawRequest.listCustomerContractInterestDate(1);--}}
{{--    </script>--}}
@stop