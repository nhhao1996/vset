@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ YÊU CẦU RÚT TIỀN CỔ PHIẾU')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('DANH SÁCH YÊU CẦU RÚT TIỀN CỔ PHIẾU')}}
                    </h3>
                </div>
            </div>
            <!-- btn add -->
            <div class="m-portlet__head-tools">
            </div>
            <!-- !btn add -->
        </div>
        <!-- tab -->
        <div class="m-portlet__body" id="three">
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       placeholder="{{__('Nhập mã yêu cầu hoặc tên khách hàng')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <select class="form-control select2-fix" name="withdraw_request_status">
                                    <option value="">{{__('Chọn trạng thái')}}</option>
                                    <option value="new">{{__('Mới')}}</option>
                                    <option value="inprocess">{{__('Đang xử lý')}}</option>
                                    <option value="done">{{__('Hoàn thành')}}</option>
                                    <option value="cancel">{{__('Hủy')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <button class="btn btn-primary color_button btn-search">
                                {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                            </button>
                            <button onclick="loadPageStock();"
                                    class="btn btn-metal  btn-search padding9x">
                                            <span>
                                            <i class="flaticon-refresh"></i>
                                            </span>
                            </button>
                        </div>
                    </div>
                </div>

                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                    </div>
                @endif
            </form>
            <div class="table-content m--padding-top-30" >
                @include('admin::withdraw-request.list-stock')
            </div><!-- end table-content -->
        </div>
        <!-- !tab -->

    </div>
@endsection
@section('after_script')
    <script>
        $(document).ready(function () {
            $('a.btn-search').attr('href','javascript:;');
            $('a.btn-search').click(function () {
                $('input , select').val('');
                $('select').select2('destroy');
                $('select').select2();
                $('button.btn-search').trigger('click');
            });
        })
    </script>
    <script src="{{asset('static/backend/js/admin/withdraw-request/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $('.select2-fix').select2();
        $(".m_selectpicker").selectpicker();
        function loadPageStock(){
            $('[name="search"]').val('');
            $('[name="withdraw_request_status"]').val('');
            $('[name="withdraw_request_status"]').select2();
            // $('.btn-search').trigger('click');
        }
    </script>
@stop
