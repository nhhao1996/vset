<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Mã yêu cầu')}}</th>
            <th class="ss--font-size-th">{{__('Mã hợp đồng')}}</th>
            <th class="ss--font-size-th">{{__('Số tiền (vnđ)')}}</th>
            <th class="ss--font-size-th">{{__('Loại')}}</th>
            <th class="ss--font-size-th">{{__('Loại kỳ hạn')}}</th>
            <th class="ss--font-size-th">{{__('Khách hàng')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày yêu cầu')}}</th>
            <th class="ss--font-size-th">{{__('Hành động ')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LISTALL))
            @foreach ($LISTALL as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                        <td>{{$item['withdraw_request_code']}}</td>
                        <td>{{$item['customer_contract_code']}}</td>
                        <td>{{number_format($item['withdraw_request_amount'])}}</td>
                        <td>
                            @if($item['withdraw_request_type'] == 'bond')
                                {{__('Hợp tác đầu tư')}}
                            @elseif($item['withdraw_request_type'] == 'saving')
                                {{__('Tiết kiệm')}}
                            @elseif($item['withdraw_request_type'] == 'interest')
                                {{__('Lãi suất')}}
                            @elseif($item['withdraw_request_type'] == 'commission')
                                {{__('Hoa hồng')}}
                            @endif
                        </td>
                        <td>{{$item['term_time_type'] == 1 ? 'Có kỳ hạn' : 'Không có kỳ hạn'}}</td>
                        <td>{{$item['customer_name']}}</td>
                        <td>
                            @if ($item['withdraw_request_status'] == 'new')
                                {{__('Mới')}}
                            @elseif ($item['withdraw_request_status'] == 'inprocess')
                                {{__('Đang xử lý')}}
                            @elseif ($item['withdraw_request_status'] == 'confirm')
                                {{__('Xác nhận')}}
                            @elseif ($item['withdraw_request_status'] == 'done')
                                {{__('Hoàn thành')}}
                            @elseif ($item['withdraw_request_status'] == 'cancel')
                                {{__('Hủy')}}
                            @endif
                        </td>
                        <td>{{$item['withdraw_request_time']}} {{$item['withdraw_request_day']}}/{{$item['withdraw_request_month']}}/{{$item['withdraw_request_year']}}</td>
                        <td>
                            @if(in_array('admin.withdraw-request.show', session('routeList')))
                                <a href="{{route('admin.withdraw-request.show',['id'=>$item['withdraw_request_id']])}}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="{{__('Chi tiết')}}">
                                    <i class="flaticon-eye"></i>
                                </a>
                            @endif
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LISTALL->links('helpers.paging') }}
{{--.--}}