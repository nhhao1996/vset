@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ YÊU CẦU RÚT TIỀN')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT YÊU CẦU RÚT TIỀN')}}
                    </h2>

                </div>
            </div>
            @if(in_array($detail['withdraw_request_type'],['saving','bond']))
                @if(in_array('admin.withdraw-request.change-status', session('routeList')))
                    @if($detail['withdraw_request_status'] == 'new')
                        @if($detail['withdraw_request_type'] == 'saving')
                            <div class="m-portlet__head-tools">
                                <button type="button"
                                   {{($checkRequest == 0 && $checkMintime == 0) || ($detail['term_time_type'] == 1 && $checkMintime == 0 ) || $detail['contract_total_amount'] != $detail['withdraw_request_amount'] || $detail['contract_is_active'] == 0 || $detail['contract_is_deleted'] == 1 ? 'disabled' :''}}
{{--                                    onclick="withrawRequest.changeStatus('inprocess')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">--}}
                                    onclick="withrawRequest.changeStatus('done')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                                    <span class="text-white">
{{--                                        <span class="zxrem">  {{__('Xác nhận')}}</span>--}}
                                        <span class="zxrem">  {{__('Hoàn thành')}}</span>
                                    </span>
                                </button>

                                <a href="javascript:void(0)" onclick="withrawRequest.changeStatus('cancel')" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                                <span class="text-white">
                                    <span class="zxrem">  {{__('Không xác nhận')}}</span>
                                </span>
                                </a>
                            </div>
                        @elseif($detail['withdraw_request_type'] == 'bond')
                            <div class="m-portlet__head-tools">
                                <button type="button"
{{--                                        onclick="withrawRequest.changeStatus('inprocess')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">--}}
                                        onclick="withrawRequest.changeStatus('done')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                                    <span class="text-white">
{{--                                        <span class="zxrem">  {{__('Xác nhận')}}</span>--}}
                                        <span class="zxrem">  {{__('Hoàn thành')}}</span>
                                    </span>
                                </button>

                                <a href="javascript:void(0)" onclick="withrawRequest.changeStatus('cancel')" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                                <span class="text-white">
                                    <span class="zxrem">  {{__('Không xác nhận')}}</span>
                                </span>
                                </a>
                            </div>
                        @else
                            <div class="m-portlet__head-tools">
                                <button type="button" {{($detail['term_time_type'] == 0 && $checkMintime == 0 ) || $detail['contract_total_amount'] != $detail['withdraw_request_amount'] || $detail['contract_is_active'] == 0 || $detail['contract_is_deleted'] == 1 ? 'disabled' :''}}
{{--                                onclick="withrawRequest.changeStatus('inprocess')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">--}}
                                onclick="withrawRequest.changeStatus('done')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                                    <span class="text-white">
{{--                                        <span class="zxrem">  {{__('Xác nhận')}}</span>--}}
                                        <span class="zxrem">  {{__('Hoàn thành')}}</span>
                                    </span>
                                </button>

                                <a href="javascript:void(0)" onclick="withrawRequest.changeStatus('cancel')" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                                    <span class="text-white">
                                        <span class="zxrem">  {{__('Không xác nhận')}}</span>
                                    </span>
                                </a>
                            </div>
                        @endif
                    @elseif($detail['withdraw_request_status'] == 'inprocess')
                        <div class="m-portlet__head-tools">
{{--                            <a href="javascript:void(0)"  onclick="withrawRequest.changeStatus('done')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">--}}
                            <a href="javascript:void(0)"  onclick="withrawRequest.changeStatus('done')"  class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                                <span class="text-white">
                                    <span class="zxrem">  {{__('Hoàn thành')}}</span>
                                </span>
                            </a>
                            <a href="javascript:void(0)" onclick="withrawRequest.changeStatus('cancel')" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                                <span class="text-white">
                                    <span class="zxrem">  {{__('Không xác nhận')}}</span>
                                </span>
                            </a>
                        </div>
                    @endif
                @endif
            @else
                <div class="m-portlet__head-tools">
                    <a href="{{route('admin.withdraw-request.show-group',['id' => $detail['withdraw_request_group_id'] ])}}" class="btn btn-sm m-btn m-btn--icon m-btn--pill" style="background-color: #4fc4ca !important;">
                        <span class="text-white">
                            <span class="zxrem">{{__('Yêu cầu rút tiền')}}</span>
                        </span>
                    </a>
                </div>
            @endif
        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Mã yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['withdraw_request_group_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group {{in_array($detail['withdraw_request_type'],['saving','bond']) ? 'd-none' : ''}}">
                        <label class="col-form-label label col-lg-4 black-title">
                            Mã nhóm yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['withdraw_request_group_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Loại yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control"
                               @if($detail['withdraw_request_type'] == 'bond')
                                   value="Hợp tác đầu tư"
                               @elseif($detail['withdraw_request_type'] == 'saving')
                                   value="Tiết kiệm"
                               @elseif($detail['withdraw_request_type'] == 'interest')
                                   value="Lãi suất"
                               @elseif($detail['withdraw_request_type'] == 'commission')
                                   value="Hoa hồng"
                               @endif
                                   disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Hợp đồng rút
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_contract_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Kỳ hạn đầu tư
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{\Carbon\Carbon::parse($detail['customer_contract_start_date'])->addMonth($detail['investment_time'])->format('d/m/Y')}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Loại kỳ hạn
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['term_time_type'] == 1 ? 'Có kỳ hạn' : 'Không có kỳ hạn'}}" disabled="">
                        </div>
                    </div>
                    @if($detail['withdraw_request_type'] == 'saving')
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Loại rút tiền
                            </label>
                            <div class="col-lg-8 d-flex align-items-center">
                                <div class="radio-inline">
                                    <label class="radio mr-5">
                                        <input type="radio" value="1" name="investment_time" {{$checkRequest == 0 ? 'checked' :'disabled'}}>
                                        <span></span>{{__('Trước kỳ hạn')}}</label>
                                    <label class="radio">
                                        <input type="radio" value="0" name="investment_time" {{$checkRequest == 1 ? 'checked' :'disabled'}}>
                                        <span></span>{{__('Đúng kỳ hạn')}}</label>
                                </div>
                            </div>
                        </div>
                    @endif
{{--                    <div class="row form-group">--}}
{{--                        <label class="col-form-label label col-lg-4 black-title">--}}
{{--                            Tỉ lệ phí rút tiền (%):--}}
{{--                        </label>--}}
{{--                        <div class="col-lg-8">--}}
{{--                            <input class="form-control" value="{{number_format($detail['withdraw_free_rate'])}}" disabled="">--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="row form-group {{!in_array($detail['withdraw_request_type'],['saving','bond']) ? 'd-none' : ''}}">
                        <label class="col-form-label label col-lg-4 black-title">
                            Giá trị hợp đồng (vnđ):
                        </label>
                        <div class="col-lg-8">
                            @if(in_array($detail['withdraw_request_status'],['done','cancel']))
                                <input class="form-control {{(double)$detail['contract_total_amount'] >= (double)$detail['withdraw_request_amount'] ? 'text-success' : 'text-danger'}}" value="{{ number_format($detail['contract_total_amount'])}}" disabled="">
                            @else
                                <input class="form-control {{(double)$detail['contract_total_amount'] >= (double)$detail['withdraw_request_amount'] ? 'text-success' : 'text-danger'}}" value="{{ $detail['contract_is_active'] == 0 || $detail['contract_is_deleted'] == 1 ? 0 : number_format($detail['contract_total_amount'])}}" disabled="">
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tiền yêu cầu rút (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount'])}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group {{!in_array($detail['withdraw_request_type'],['saving','bond']) ? 'd-none' : ''}}">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tiền truy thu (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control text-danger" value="{{$checkRequest == 1 ? 0 : number_format($getListInterest['total_withdraw_request_amount'])}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group {{!in_array($detail['withdraw_request_type'],['saving','bond']) ? 'd-none' : ''}}">
                        <label class="col-form-label label col-lg-4 black-title">
                            Phí rút tiền (vnđ):
                        </label>
                        <div class="col-lg-8">
{{--                            <input class="form-control text-danger" value="{{number_format($detail['withdraw_free_amount'])}}" disabled="">--}}
                            @if($detail['withdraw_request_type'] != 'bond')
                                @if($checkRequest == 1)
                                    <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount']*($detail['withdraw_fee_rate_ok']/100))}}" disabled="">
                                @else
                                    <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount']*($detail['withdraw_fee_rate_before']/100))}}" disabled="">
                                @endif
                            @else
                                <input class="form-control text-danger" value="0" disabled="">
                            @endif
                        </div>
                    </div>
                    <div class="row form-group {{!in_array($detail['withdraw_request_type'],['saving','bond']) ? 'd-none' : ''}}">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tiền cần chuyển khoản (vnđ):
                        </label>
                        <div class="col-lg-8">
                            @if($detail['withdraw_request_type'] !='bond')
                                @if($checkRequest == 1)
                                    <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount'] - ($detail['withdraw_request_amount']*($detail['withdraw_fee_rate_ok']/100)))}}" disabled="">
                                @else
                                    <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount'] - $getListInterest['total_withdraw_request_amount'] - ($detail['withdraw_request_amount']*($detail['withdraw_fee_rate_before']/100)))}}" disabled="">
                                @endif
                            @else
                                <input class="form-control text-danger" value="{{number_format($detail['withdraw_request_amount'] - $getListInterest['total_withdraw_request_amount'])}}" disabled="">
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Thời gian tối thiểu có thể rút:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{ $detail['term_time_type'] == 0 ? 'N/A' : \Carbon\Carbon::parse($detail['customer_contract_start_date'])->addMonth($detail['withdraw_min_time'] == null ? 0 : $detail['withdraw_min_time'])->format('H:i:s d/m/Y')}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Thời gian yêu cầu rút:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['withdraw_request_time']}} {{$detail['withdraw_request_day']}}/{{$detail['withdraw_request_month']}}/{{$detail['withdraw_request_year']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Thời gian hoàn thành yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            @if($detail['withdraw_request_status'] != 'done')
                                <input class="form-control" value="" disabled="">
                            @else
                                <input class="form-control" value="{{\Carbon\Carbon::parse($detail['updated_at'])->format('H:i:s d/m/Y')}}" disabled="">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Trạng thái yêu cầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control"
                                   @if($detail['withdraw_request_status'] == 'new')
                                   value="Mới"
                                   @elseif($detail['withdraw_request_status'] == 'inprocess')
                                   value="Đang xử lý"
                                   @elseif($detail['withdraw_request_status'] == 'confirm')
                                   value="Xác nhận"
                                   @elseif($detail['withdraw_request_status'] == 'done')
                                   value="Hoàn thành"
                                   @elseif($detail['withdraw_request_status'] == 'cancel')
                                   value="Hủy"
                                   @endif

                                   disabled="">
                        </div>
                    </div>
                    @if($detail['withdraw_request_status'] == 'cancel')
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Lý do
                            </label>
                            <div class="col-lg-8">
                                <textarea class="form-control" disabled>{{$detail['reason']}}</textarea>
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Họ và tên nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số chứng minh nhân dân:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_ic_no']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số điện thoại nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_phone']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Email nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_email']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Địa chỉ thường trú nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            @if($detail['residence_address'] == null && $detail['lienhe_dis_name'] == null && $detail['lienhe_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control" value="{{$detail['residence_address']}}, {{$detail['lienhe_dis_type']}} {{$detail['lienhe_dis_name']}}, {{$detail['lienhe_pro_type']}} {{$detail['lienhe_pro_name']}}" disabled>
                            @endif
{{--                            <input class="form-control" value="{{$detail['customer_residence_address']}}" disabled="">--}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tên ngân hàng nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bank_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tài khoản ngân hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bank_account_no']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tên tài khoản ngân hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bank_account_name']}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="customer_contract_id" value="{{$detail['customer_contract_id']}}">
        <input type="hidden" id="customer_id" value="{{$detail['customer_id']}}">
        <input type="hidden" id="withdraw_request_type" value="{{$detail['withdraw_request_type']}}">
        <input type="hidden" id="withdraw_request_id" value="{{$detail['withdraw_request_id']}}">
        <input type="hidden" id="available_balance_after" value="{{$detail['contract_total_amount']}}">
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.withdraw-request')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('QUAY LẠI ')}}</span>
                    </span>
                    </a>
                </div>
            </div>
        </div>

{{--        @if(in_array($detail['withdraw_request_type'],['interest','commission']))--}}
            <div class="m-portlet__body">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active show" data-toggle="tab" href="#" data-target="#list_withraw_request">{{_('Danh sách yêu cầu rút tiền')}}</a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link " data-toggle="tab" href="#" data-target="#customer_contract_interest_by_month">{{_('Lãi suất hợp đồng theo tháng')}}</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link " data-toggle="tab" href="#" data-target="#customer_contract_interest_by_date">{{_('Lãi suất hợp đồng theo ngày')}}</a>--}}
{{--                    </li>--}}

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="list_withraw_request" role="tabpanel">
                        <div class="form-group col-lg-12 list_withraw_request p-0">

                        </div>
                        <input type="hidden" id="page_withraw_request" >
                    </div>
                    <div class="tab-pane" id="customer_contract_interest_by_month" role="tabpanel">
                        <div class="form-group col-lg-12 customer_contract_interest_by_month">

                        </div>
                        <input type="hidden" id="page_customer_contract_interest_by_month">
                    </div>
                    <div class="tab-pane" id="customer_contract_interest_by_date" role="tabpanel">
                        <div class="form-group col-lg-12 customer_contract_interest_by_date">

                        </div>
                        <input type="hidden" id="page_customer_contract_interest_by_date">
                    </div>
                </div>
            </div>
{{--        @endif--}}
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/withdraw-request/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
{{--        @if(in_array($detail['withdraw_request_type'],['interest','commission']))--}}
            withrawRequest.listWithrawRequest(1);
            withrawRequest.listCustomerContractInterestMonth(1);
            withrawRequest.listCustomerContractInterestDate(1);
{{--        @endif--}}
    </script>
@stop