@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ YÊU CẦU RÚT TIỀN')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('DANH SÁCH YÊU CẦU RÚT TIỀN')}}
                    </h3>
                </div>
            </div>
            <!-- btn add -->
            <div class="m-portlet__head-tools">
{{--                <a href="{{route('admin.withdraw-request.create')}}" class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">--}}
{{--                    <span>--}}
{{--                        <i class="fa fa-plus-circle"></i>--}}
{{--                        <span> {{__('THÊM YÊU CẦU RÚT TIỀN')}}</span>--}}
{{--                    </span>--}}
{{--                </a>--}}
            </div>
            <!-- !btn add -->
        </div>
        <!-- tab -->
        <div class="card-header tab-card-header">
            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link active show" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="All" aria-selected="true">Tất cả</a>--}}
{{--                </li>--}}
                <li class="nav-item">
                    <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Hợp tác đầu tư - Tiết kiệm</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Lãi suất - Tiền thưởng</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade p-3" id="all" role="tabpanel" aria-labelledby="all-tab">
                <div class="m-portlet__body">
                    <form class="frmFilter bg">
                        <div class="row padding_row">
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search"
                                               placeholder="{{__('Nhập mã yêu cầu , mã hợp đồng hoặc tên khách hàng')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <select class="form-control select2-fix" name="withdraw_request_status">
                                            <option value="">{{__('Chọn trạng thái')}}</option>
                                            <option value="new">{{__('Mới')}}</option>
{{--                                            <option value="inprocess">{{__('Đang xử lý')}}</option>--}}
{{--                                            <option value="confirm">{{__('Xác nhận')}}</option>--}}
                                            <option value="done">{{__('Hoàn thành')}}</option>
                                            <option value="cancel">{{__('Hủy')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <button class="btn btn-primary color_button btn-search">
                                        {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                    </button>
                                    <a href="{{route('admin.withdraw-request')}}"
                                       class="btn btn-metal  btn-search padding9x">
                                        <span><i class="flaticon-refresh"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible">
                                <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                            </div>
                        @endif
                    </form>
                    <div class="table-content">
                        @include('admin::withdraw-request.list-all')
                    </div><!-- end table-content -->
                </div>
            </div>
            <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                <div class="m-portlet__body">
                    <form class="frmFilter bg">
                        <div class="row padding_row">
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search"
                                               placeholder="{{__('Nhập mã yêu cầu , mã hợp đồng hoặc tên khách hàng')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <select class="form-control select2-fix" name="withdraw_request_status">
                                            <option value="">{{__('Chọn trạng thái')}}</option>
                                            <option value="new">{{__('Mới')}}</option>
{{--                                            <option value="inprocess">{{__('Đang xử lý')}}</option>--}}
{{--                                            <option value="confirm">{{__('Xác nhận')}}</option>--}}
                                            <option value="done">{{__('Hoàn thành')}}</option>
                                            <option value="cancel">{{__('Hủy')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <button class="btn btn-primary color_button btn-search">
                                        {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                    </button>
                                    <a href="{{route('admin.withdraw-request')}}"
                                       class="btn btn-metal  btn-search padding9x">
                                        <span><i class="flaticon-refresh"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible">
                                <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                            </div>
                        @endif
                    </form>
                    <div class="table-content m--padding-top-30">
                        @include('admin::withdraw-request.list')
                    </div><!-- end table-content -->
                </div>
            </div>
            <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                <div class="m-portlet__body">
                    <form class="frmFilter bg">
                        <div class="row padding_row">
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search"
                                               placeholder="{{__('Nhập mã yêu cầu, mã hợp đồng hoặc tên khách hàng')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <select class="form-control select2-fix" name="withdraw_request_status">
                                            <option value="">{{__('Chọn trạng thái')}}</option>
                                            <option value="new">{{__('Mới')}}</option>
{{--                                            <option value="inprocess">{{__('Đang xử lý')}}</option>--}}
{{--                                            <option value="confirm">{{__('Xác nhận')}}</option>--}}
                                            <option value="done">{{__('Hoàn thành')}}</option>
                                            <option value="cancel">{{__('Hủy')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group m-form__group">
                                    <button class="btn btn-primary color_button btn-search">
                                        {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                    </button>
                                    <a href="{{route('admin.withdraw-request')}}"
                                       class="btn btn-metal  btn-search padding9x">
                                <span>
                                <i class="flaticon-refresh"></i>
                                </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible">
                                <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                            </div>
                        @endif
                    </form>
                    <div class="table-content m--padding-top-30">
                        @include('admin::withdraw-request.list-commission')
                    </div><!-- end table-content -->
                </div>
            </div>
{{--            <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">--}}
{{--                <div class="m-portlet__body">--}}
{{--                    <form class="frmFilter bg">--}}
{{--                        <div class="row padding_row">--}}
{{--                            <div class="col-lg-3">--}}
{{--                                <div class="form-group m-form__group">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input type="text" class="form-control" name="search"--}}
{{--                                               placeholder="{{__('Nhập mã yêu cầu hoặc tên khách hàng')}}">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-3">--}}
{{--                                <div class="form-group m-form__group">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <select class="form-control select2-fix" name="withdraw_request_status">--}}
{{--                                            <option value="">{{__('Chọn trạng thái')}}</option>--}}
{{--                                            <option value="new">{{__('Mới')}}</option>--}}
{{--                                            <option value="inprocess">{{__('Đang xử lý')}}</option>--}}
{{--                                            <option value="done">{{__('Hoàn thành')}}</option>--}}
{{--                                            <option value="cancel">{{__('Hủy')}}</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-3">--}}
{{--                                <div class="form-group m-form__group">--}}
{{--                                    <button class="btn btn-primary color_button btn-search">--}}
{{--                                        {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>--}}
{{--                                    </button>--}}
{{--                                    <button onclick="loadPageStock();"--}}
{{--                                       class="btn btn-metal  btn-search padding9x">--}}
{{--                                            <span>--}}
{{--                                            <i class="flaticon-refresh"></i>--}}
{{--                                            </span>--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        @if (session('status'))--}}
{{--                            <div class="alert alert-success alert-dismissible">--}}
{{--                                <strong>{{__('Success')}} : </strong> {!! session('status') !!}.--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    </form>--}}
{{--                    <div class="table-content m--padding-top-30">--}}
{{--                        @include('admin::withdraw-request.list-stock')--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <!-- !tab -->

    </div>
@endsection
@section('after_script')
    <script>
        $(document).ready(function () {
            $('a.btn-search').attr('href','javascript:;');
            $('a.btn-search').click(function () {
                $('input , select').val('');
                $('select').select2('destroy');
                $('select').select2();
                $('button.btn-search').trigger('click');
            });
        })
    </script>
    <script src="{{asset('static/backend/js/admin/withdraw-request/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $('.select2-fix').select2();
        $(".m_selectpicker").selectpicker();
        function loadPageStock(){
            $('[name="search"]').val('');
            $('[name="withdraw_request_status"]').val('');
            $('[name="withdraw_request_status"]').select2();
            // $('.btn-search').trigger('click');
        }
    </script>
@stop
