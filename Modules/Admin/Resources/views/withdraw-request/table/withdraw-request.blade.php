<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead class="bg">
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('Mã yêu cầu')}}</th>
            <th class="ss--font-size-th">{{__('Số tiền')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Ngày yêu cầu')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($list))
            @foreach ($list as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    <td>{{$item['withdraw_request_code']}}</td>
                    <td>{{number_format($item['withdraw_request_amount'])}}</td>
                    <td>
                        @if ($item['withdraw_request_status'] == 'new')
                            {{__('Mới')}}
                        @elseif ($item['withdraw_request_status'] == 'inprocess')
                            {{__('Đang xử lý')}}
                        @elseif ($item['withdraw_request_status'] == 'confirm')
                            {{__('Xác nhận')}}
                        @elseif ($item['withdraw_request_status'] == 'done')
                            {{__('Xác nhận')}}
                        @elseif ($item['withdraw_request_status'] == 'cancel')
                            {{__('Hủy')}}
                        @endif
                    </td>
                    <td>{{$item['withdraw_request_time']}} {{$item['withdraw_request_day']}}/{{$item['withdraw_request_month']}}/{{$item['withdraw_request_year']}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $list->links('admin::withdraw-request.helpers.paging-withdraw-request') }}
