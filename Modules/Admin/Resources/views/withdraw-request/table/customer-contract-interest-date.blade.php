<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead class="bg">
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('Giá trị lãi suất')}} (vnđ)</th>
            <th class="ss--font-size-th">{{__('Giá trị lãi suất tích lũy')}} (vnđ)</th>
            <th class="ss--font-size-th">{{__('Giá trị lãi suất có thể rút')}} (vnđ)</th>
            <th class="ss--font-size-th">{{__('Tỉ lệ lãi suất theo ngày')}} (%)</th>
            <th class="ss--font-size-th">{{__('Ngày rút tiền theo kế hoach')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($list))
            @foreach ($list as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    <td>{{number_format($item['interest_amount'])}}</td>
                    <td>{{number_format($item['interest_total_amount'])}}</td>
                    <td>{{number_format($item['interest_balance_amount'])}}</td>
                    <td>{{number_format($item['interest_rate_by_day'])}}</td>
                    <td>{{\Carbon\Carbon::parse($item['withdraw_date_planning'])->format('H:i:s d-m-Y')}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $list->links('admin::withdraw-request.helpers.paging-customer-contract-interest-date') }}
