<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('#')}}</th>
            <th class="ss--font-size-th">{{__('Mã yêu cầu mua')}}</th>
            <th class="ss--font-size-th">{{__('Nguồn mua')}}</th>
            <th class="ss--font-size-th">{{__('Nhà đầu tư')}}</th>
            <th class="ss--font-size-th">{{__('SĐT ')}}</th>
            <th class="ss--font-size-th">{{__('Số cổ phiếu')}}</th>
            <th class="ss--font-size-th">{{__('Giá bán')}}</th>
            <th class="ss--font-size-th">{{__('Thành tiền')}}</th>
            <th class="ss--font-size-th">{{__('Hình thức thanh toán')}}</th>
            <th class="ss--font-size-th">{{__('Ngày khởi tạo')}}</th>
            <th class="ss--font-size-th">{{__('Trạng thái')}}</th>
            <th class="ss--font-size-th">{{__('Hành động ')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                        <td>
{{--                            @if(in_array('admin.buy-stock-request.detail', session('routeList')))--}}
                                <a href="{{route('admin.buy-stock-request.detail',['id'=>$item['stock_order_id']])}}">
                                    {{$item['stock_order_code']}}
                                </a>
{{--                            @else--}}
{{--                                {{$item['stock_order_code']}}--}}
{{--                            @endif--}}
                        </td>
                        <td>{{$item['source'] == 'publisher' ? 'Nhà phát hành' : 'Chợ'}}</td>
                        <td>{{$item['customer_name']}}</td>
                        <td>{{$item['customer_phone']}}</td>
                        <td>{{number_format($item['quantity'])}}</td>
                        <td>{{number_format($item['money_standard'])}}</td>
                        <td>{{number_format($item['total'])}}</td>
                        <td>{{$item['payment_method_name']}}</td>
                        <td>{{ isset($item['created_at']) != '' ? Carbon\Carbon::parse($item['created_at'])->format('d-m-Y') : '-' }}</td>
                        <td>
                            {{$item['process_status'] == "new" ? __("Mới") : ($item['process_status'] == "cancel" ? __("Đã huỷ") : __("Thanh toán thành công") )}}
                        </td>
                        <td>
{{--                            @if(in_array('admin.buy-bonds-request-fail.view', session('routeList')))--}}
                                <a href="{{route('admin.buy-stock-request.detail',['id'=>$item['stock_order_id']])}}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="{{__('Chi tiết')}}">
                                    <i class="flaticon-eye"></i>
                                </a>
{{--                            @endif--}}
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}