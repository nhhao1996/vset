<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_od_detail">{{__('Hình thức thu tiền')}}</th>
            <th class="tr_thead_od_detail">{{__('Số tiền thu (Vnđ)')}}</th>
            <th class="tr_thead_od_detail">{{__('Ảnh biên lai')}}</th>
            <th class="tr_thead_od_detail">{{__('Nhân viên xử lý')}}</th>
        </tr>
        </thead>
        <tbody>
        @if($list != null)
            @foreach($list as $key => $item)
                <tr>
                    <td id="payment_method_name_td">
                    </td>
                    <td>{{number_format($item['amount'],2)}}</td>
                    <td>
                        @if(isset($arrGroupImage[$item['receipt_detail_id']]))
                            @foreach($arrGroupImage[$item['receipt_detail_id']] as $keyImage => $value)
                                @if($keyImage == 0)
                                    <a href="{{$value['image_file']}}" target="_blank">
                                        Ảnh {{$keyImage+1}}
                                    </a>
                                @else
                                    <a href="{{$value['image_file']}}" target="_blank">
                                        , Ảnh {{$keyImage+1}}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td>{{$item['full_name']}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {{ $list->links('admin::buy-bonds-request.helpers.paging-receipt-detail') }}
</div>