<div class="modal fade" id="modalMakeReceipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>TẠO PHIẾU THU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="receipt-detail-add">
                    <div class="form-group">
                        <label>
                            {{__('Số tiền cần thu')}}:<b class="text-danger">*</b>
                        </label>
                        <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }}">
                            <input type="text" value="{{number_format($moneyReceipt['amount'],2)}}" class="form-control m-input "
                                   placeholder="{{__('Nhập số tiền cần thu')}}" disabled>
                            <span class="error-name text-danger"></span>
                        </div>
                    </div>
                    <input type="hidden" name="receipt_id" value="{{$moneyReceipt['receipt_id']??1}}">
                    <input type="hidden" name="stock_order_id" value="{{$stock_order_id}}">
                    <input type="hidden" name="amount" value="{{$moneyReceipt['amount']}}">
                    <input type="hidden" name="payment_method_id" value="{{$payment_method_id}}">
                    <input type="hidden" name="stock_bonus" value="{{$stock_bonus}}">
                    <!-- hinh thuc thu tien -->
                    <div class="form-group">
                            <select class="form-control" id="payment_method_type_select_box" name="payment_method_id" disabled>
                                <option value="{{$payment_method_id}}">{{$payment_method_name}}</option>
                            </select>
                        </div>
                    <!-- dropzone -->
                    <div class="form-group noselector transfer">
                        <div class="form-group">
                            <label>
                                {{__('Số tiền thanh toán (Vnđ)')}}:<b class="text-danger">*</b>
                            </label>
                            <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }}">
                                <input type="text" class="form-control" value="{{number_format($moneyReceipt['amount'],2)}}"
                                       placeholder="{{__('Nhập số tiền thanh toán')}}" disabled>
                                <span class="error-name text-danger"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group ">
                            <label>{{__('Ảnh biên lai')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('admin.buy-bonds-request.upload-dropzone')}}" id="dropzoneone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('Hình ảnh')}}
                                    </h3>
                                    <span>{{__('Chọn hình biên lai')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>HỦY</span>
                            </span>
                        </button>
                        @if($payment_method_id == '4')
                            <button type="button" onclick="BuyStockRequest.createBill()" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5 print-bill">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>TẠO HÓA ĐƠN</span>
                                </span>
                            </button>
                        @endif
                        <button type="button" onclick="BuyStockRequest.addReceiptDetail(4)" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>LƯU THÔNG TIN</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>