@extends('layout')
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/service-card.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('CHI TIẾT YÊU CẦU MUA CỔ PHIẾU')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
        .form-control-feedback {
            color : red;
        }
        #create-bill {
            overflow: auto !important;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT YÊU CẦU MUA CỔ PHIẾU')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                <!-- Thông tin của nhà đầu tư chưa đủ thì không cho xác nhận -->
                @if(
                        $detail['customer_phone'] == null ||
                        $detail['customer_residence_address'] == null ||
                        $detail['customer_ic_no'] == null ||
                        strlen(str_replace(' ','',$detail['customer_phone'] )) == 0 ||
                        strlen(str_replace(' ','',$detail['customer_ic_no'] )) == 0 ||
                        strlen(str_replace(' ','',$detail['customer_residence_address'] )) == 0

                )
                    <button type="button" onclick="BuyStockRequest.confirmFail(2)"
                            class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                        <span class="text-white">
                            <i class="fas fa-check-square"></i>
                            <span class="zxrem">  {{__('Xác nhận')}}</span>
                        </span>
                    </button>
                @elseif($detail['process_status'] != 'paysuccess' && $detail['process_status'] != 'cancel' )
                    <!-- Nếu PTTT là tiền mặt thì tạo hoá đơn, ngược lại thì ko tại, xác nhận thẳng -->
                    @if($detail['payment_method_id'] != '4' && $detail['payment_method_id'] != '1')
                            <a href="javascript:void(0)" onclick="BuyStockRequest.addReceiptDetail()"
                               class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                <span class="text-white">
                                    <i class="fas fa-check-square"></i>
                                    <span class="zxrem">  {{__('Xác nhận')}}</span>
                                </span>
                            </a>
                    @else
                        <a href="javascript:void(0)" onclick="BuyStockRequest.showPopupReceiptDetail()"
                               class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
                                <i class="fas fa-check-square"></i>
                                <span class="zxrem">  {{__('Tạo phiếu thu')}}</span>
                            </span>
                        </a>
                    @endif
                        <a href="javascript:void(0)" onclick="BuyStockRequest.cancelConfirm('cancel')"
                           class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                            <span class="text-white">
                                <span class="zxrem">Huỷ xác nhận</span>
                            </span>
                        </a>
                @endif
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
                    <div class="row form-group" hidden>
                        <div class="col-lg-8">
                            <input class="form-control" id="stock_order_id" value="{{$detail['stock_order_id']}}">

                            <input type="hidden" id="customer_id" name="customer_id" value="{{$detail['customer_id']}}">
                            <input name="total" id="total" value="{{$detail['total']}}" hidden>
                            <input type="hidden" id="payment_method_id" value="{{$detail['payment_method_id']}}" >
                            <input type="hidden" id="quantity" value="{{$detail['quantity']}}" >
                            <input type="hidden" id="source" value="{{$detail['source']}}" >
                            <input type="hidden" value="{{round($detail['quantity']*$detail['stock_bonus_rate']/100)}}" name="stock_bonus" id="stock_bonus">
                        </div>
                    </div>
                    @if(isset($seller) != null)
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Tên người bán')}}:
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{$seller['seller_name'] == null ? 'N/A' :$seller['seller_name']}}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('Mã người bán')}}:
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{$seller['seller_code'] == null ? 'N/A' :$seller['seller_code']}}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                {{__('SĐT')}}:
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{$seller['seller_phone'] == null ? 'N/A' :$seller['seller_phone']}}" disabled="">
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Mã đơn hàng')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" id="stock_order_code" name="stock_order_code" value="{{$detail['stock_order_code'] == null ? 'N/A' :$detail['stock_order_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Tên cổ phiếu')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['stock_config_code'] == null ? 'N/A' :$detail['stock_config_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số cổ phiếu')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['quantity'] == null ? 'N/A' : number_format($detail['quantity'])}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Giá bán')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['money_standard'] == null ? 'N/A' : number_format($detail['money_standard'], 2) }}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Thành tiền')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" id="total" value="{{$detail['total'] == null ? 'N/A' : number_format($detail['total'], 2) }}" disabled="">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Hình thức thanh toán')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" id="payment_method_name" value="{{$detail['payment_method_name'] == null ? 'N/A' : $detail['payment_method_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Ngày thanh toán')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['payment_date'] == null ? 'N/A' : \Carbon\Carbon::parse($detail['payment_date'])->format('H:i:s d-m-Y')}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Họ và tên nhà đầu tư')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_name'] == null ? 'N/A' : $detail['customer_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Chứng minh nhân dân nhà đầu tư')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control {{$detail['customer_ic_no'] == null || strlen(str_replace(' ','',$detail['customer_ic_no'] )) == 0 ? 'text-danger' : ''}}" value="{{$detail['customer_ic_no'] == null || strlen(str_replace(' ','',$detail['customer_ic_no'] )) == 0 ? 'N/A' : $detail['customer_ic_no']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số điện thoại nhà đầu tư')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control {{$detail['customer_phone'] == null || strlen(str_replace(' ','',$detail['customer_phone'] )) == 0 ? 'text-danger' : ''}}" value="{{$detail['customer_phone'] == null || strlen(str_replace(' ','',$detail['customer_phone'] )) == 0 ? 'N/A' : $detail['customer_phone']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Email nhà đầu tư')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control {{$detail['customer_email'] == null || strlen(str_replace(' ','',$detail['customer_email'] )) == 0 ? 'text-danger' : ''}}" value="{{$detail['customer_email']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Địa chỉ nhà đầu tư')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control {{$detail['customer_residence_address'] == null || strlen(str_replace(' ','',$detail['customer_residence_address'] )) == 0 ? 'text-danger' : ''}}" value="{{$detail['customer_residence_address'].','.$detail['district_type'].' '.$detail['district_name'].','.$detail['province_type'].' '.$detail['province_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Tặng thêm')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{isset($detail['total_stock_bonus']) != '' ? number_format($detail['total_stock_bonus']) : 'N/A'}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Số dư khả dụng')}}:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$total == null ? 'N/A' : number_format($total,2)}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- start hóa đơn -->
            <div class="m-portlet__head pl-0">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-list"></i>
                    </span>
                        <h2 class="m-portlet__head-text">
                            {{__('Danh sách phiếu thu')}}
                        </h2>

                    </div>
                </div>
            </div>

            <div class="bill">

            </div>
            <div id="append-create-receipt"></div>
            <div class="form-group m-form__group bdt_order bdb_order">
                <div class="m-section__content receipt-detail-list">

                </div>
                <input type="hidden" id="page" name="page" value="">
            </div>
            <!-- end hóa đơn -->
        </div>

        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.buy-stock-request')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('QUAY LẠI ')}}</span>
                    </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/buy-stock-request/script.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/buy-stock-request/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        new AutoNumeric.multiple('.name', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });
    </script>
    <script type="text/template" id="imageShow">
        <div class="wrap-img image-show-child m-3">
            <input type="hidden" name="img-transfer[]" value="{link_hidden}">
            <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px"
                 height="100px">
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="BuyStockRequest.remove_img(this)">
                    <i class="la la-close class_remove"></i>
                </a>
            </span>
        </div>
    </script>
    <script type="text/template" id="imageShowCash">
        <div class="wrap-img image-show-child m-3">
            <input type="hidden" name="img-transfer[]" value="{link_hidden}">
            <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px"
                 height="100px">
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="BuyStockRequest.remove_img(this)">
                    <i class="la la-close class_remove"></i>
                </a>
            </span>
        </div>
    </script>
    <script>
        BuyStockRequest.getListReceiptDetail(1);
        BuyStockRequest.dropzone();
    </script>
@stop