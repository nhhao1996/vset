@extends('layout')
@section('title_header')

@endsection
@section('content')
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                         <i class="fa fa-plus-circle"></i>
                     </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CẤU HÌNH GIỚI THIỆU')}}
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <form id="form-add">
            {!! csrf_field() !!}
            <div class="m-portlet__body">
                <h5 class="m-section__heading">{{__('Đối tượng áp dụng')}}</h5>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Đối tượng:</label>
                    <div class="col-9">
                        <div class="kt-checkbox-list ">
                            <label class="kt-checkbox d-block">
                                <input type="radio" class="from-all" name="object" value="all" checked> {{__('Tất cả')}}
                                <span></span>
                            </label>
                            <label class="kt-checkbox d-block">
                                <input type="radio" class="rank" name="object" value="code"> {{__('Referral Code')}}
                                <span></span>
                            </label>
                            <label class="kt-checkbox d-block">
                                <input type="radio" class="rank" name="object" value="link">  {{__('Referral Link')}}
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                <h5 class="m-section__heading">{{__('Cấu hình tặng thưởng')}}</h5>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="row">
                            <label class="col-lg-3 col-form-label">Voucher:</label>
                            <div class="col-9">
                                <label class="kt-checkbox d-inline-block">
                                    <input type="checkbox" class="voucher" value="voucher" name="reward[]" >
                                    <span></span>
                                </label>
                                <button type="button" class="form-control w-25 d-inline-block select-voucher" onclick="customerBroker.showPopup()" disabled data-toggle="modal" data-target="#popup_service">Chọn voucher</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 pt-3">
                        <div class="row">
                            <label class="col-lg-3 col-form-label">Tiền mặt:</label>
                            <div class="col-9">
                                <label class="kt-checkbox d-inline-block">
                                    <input type="checkbox" class="money" value="money" name="reward[]" >
                                    <span></span>
                                </label>
                                <input type="text" name="value" class="form-control w-25 d-inline-block input-money number-money" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <table class="table add_service_session">
                    <thead>
                    <th>Tên dịch vụ</th>
                    <th></th>
                    </thead>
                    <tbody class="list_add_service">

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.notification')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                            <span>
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                            </span>
                        </a>
                        <button type="button" onclick="customerBroker.submit_add(1)"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span>
                                <i class="la la-check"></i>
                                <span>{{__('LƯU THÔNG TIN')}}</span>
                        </span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- Modal action--}}
    <div id="end-point-modal"></div>
    <div id="group-modal"></div>

    <div class="modal fade" id="popup_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Danh sách dịch vụ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="search_service">
                        <div class="form-group">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="form-control" id="service_name" name="service_name" placeholder="Tên dịch vụ">
                                    </div>
                                    <div class="col-4">
                                        <select class="form-control select-fix" id="service_category" name="service_category">
                                            <option value="">Danh mục</option>
                                            @foreach($listCategoryService as $item)
                                                <option value="{{$item['service_category_id']}}">{{$item['name_vi']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-secondary" onclick="customerBroker.deleteSearch()">Xóa</button>
                                        <button type="button" class="btn btn-primary" onclick="customerBroker.searchPopup(1)">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group table-service">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="customerBroker.addListService()" data-dismiss="modal">Chọn</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script>
        var decimal_number = 0;
        var numberImage = 0;
        var numberImageMobile = 0;
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/customer-broker/vset-script.js?v='.time())}}"></script>
    <script type="text/javascript">
        $('.select-fix').select2();
    </script>
@stop
