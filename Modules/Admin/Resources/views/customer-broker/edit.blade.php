@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ NHÀ MÔI GIỚI")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }
        .icon-padding {
            padding: 10px;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"
                                 style="font-size: 13px"></i> @lang("CHỈNH SỬA NHÀ MÔI GIỚI")</span>
                    </h2>
                </div>
            </div>
        </div>
        <style>
            .nt-custome-row  .form-group{
                margin-right: 0;
                margin-left: 0;
            }
        </style>
        <form id="form-edit">
            <div class="m-portlet__body">
                <div class="row nt-custome-row">
                    <div class="form-group col-lg-6">
{{--                        // Mã khách hàng--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Mã nhà đầu tư')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" id="customer_code" name="customer_code"
                                           class="form-control m-input" disabled
                                           value="{{$item['customer_code']}}">
                                </div>
                            </div>
                        </div>
{{--                    tài khoản--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Tài khoản')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" id="phone" name="phone" disabled
                                           class="form-control m-input"
                                           value="{{$item['phone']}}">
                                </div>
                            </div>
                        </div>
{{--                        // email--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Email')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="email" name="email" class="form-control m-input" disabled
                                       placeholder="Vd: piospa@gmail.com" value="{{$item['email']}}">
                            </div>
                        </div>
{{--                        // tên khách hàng--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Tên khách hàng')}}:
                            </label>
                            <div class="input-group">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" id="full_name" name="full_name"
                                           class="form-control m-input"
                                           placeholder="@lang('Nhập tên khách hàng')"
                                           value="{{$item['full_name']}}">
                                </div>
                            </div>
                        </div>
{{--                        // mã giới thiệu--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Mã giới thiệu')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_refer_code" name="customer_refer_code" class="form-control m-input" disabled
                                       value="{{$item['customer_refer_code']}}">
{{--                                <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                                        <span><i class="flaticon-menu-1"></i></span></span>--}}
                            </div>
                        </div>
{{--                        // đã giới thiệu--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Người giới thiệu')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_refer" name="customer_refer" class="form-control m-input" disabled
                                        value="{{$item['customer_refer_name']}}">
{{--                                <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                                        <span><i class="flaticon-signs"></i></span></span>--}}
                            </div>
                        </div>
{{--                        // ngày sinh--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Ngày sinh')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="birthday" name="birthday" class="form-control m-input" readonly
                                       value="{{date("d/m/Y",strtotime($item['birthday']))}}" >
                            </div>
                        </div>
{{--                        // giới tính--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Giới tính')}}:
                            </label>
                            <div class="input-group">
                                <select id="gender" name="gender" class="form-control"
                                        style="width: 100%">
                                    <option value="male" @if($item['gender'] == 'male') selected @endif>{{__('Nam')}} </option>
                                    <option value="female" @if($item['gender'] == 'female') selected @endif>{{__('Nữ')}} </option>
                                    <option value="other" @if($item['gender'] == 'other') selected @endif>{{__('Khác')}} </option>
                                </select>
                            </div>
                        </div>
{{--                        // ngân hàng--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Ngân hàng')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="bank_name" name="bank_name" class="form-control m-input"
                                       value="{{$item['bank_account_name']}}">
                            </div>
                        </div>
{{--                            // số tài khoảng--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Số tài khoản')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="bank_account_no" name="bank_account_no" class="form-control m-input"
                                       value="{{$item['bank_account_no']}}">
                            </div>
                        </div>

                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Trạng thái')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label class="ss--switch">
                                    <input type="checkbox" name="is_actived" id="is_actived" {{$item['is_actived'] == 1 ? 'checked' :''}}>
                                    <span></span>
                                </label>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
        {{--                        // hạn thành viên--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hạng thành viên')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="rank_member" name="rank_member" class="form-control m-input"
                                       value="{{$item['member_levels_name']}}" disabled>
                            </div>
                        </div>
        {{--                        // điểm hạng--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Điểm hạng')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="point_rank" name="point_rank" class="form-control m-input"
                                       value="{{$item['point']}}" disabled>
                            </div>
                        </div>
        {{--                            // ngày tạo--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Ngày tạo')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="created_at" name="created_at" class="form-control m-input"
                                       value="{{date("d/m/Y",strtotime($item['created_at']))}}" disabled>
                            </div>
                        </div>
        {{--                        // số điện thoại liên hệ--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Số điện thoại')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="phone2" name="phone2" class="form-control m-input"
                                       value="{{$item['phone2']}}" >
                            </div>
                        </div>
{{--                        // địa chỉ thường trú--}}
                        <div class="form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Địa chỉ thường trú')}}:
                            </label>
                            <div class=" row mb-3">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <select name="contact_province_id" id="contact_province_id" class="form-control">
                                            <option value="">Chọn Tỉnh/Thành phố</option>
                                            @foreach($optionProvince as $key => $value)
                                                @if ($item['contact_province_id'] == $key)
                                                    <option value="{{$key}}" selected>{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="input-group">
                                        <input type="hidden" name="contact_district_hide" id="contact_district_hide"
                                               value="{{$item['contact_district_id']}}">
                                        <select name="contact_district_id" id="contact_district_id" class="form-control contact_district_id"
                                                title="@lang('Chọn quận / huyện')" style="width: 100%">
                                            <option value="">Chọn Quận / Huyện</option>
                                            @if($item['contact_district_id'] != null)
                                                <option value="{{$item['contact_district_id']}}" selected>
                                                    {{$item['thuongtru_dis_type'] . ' '. $item['thuongtru_dis_name']}}
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input id="contact_address" name="contact_address" class="form-control  autosizeme"
                                               placeholder="@lang('Nhập địa chỉ khách hàng')"
                                               data-autosize-on="true"
                                               style="overflow: hidden; overflow-wrap: break-word; resize: horizontal;"
                                               value="{{$item['contact_address']}}">
                                        {{--                                        <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                                        {{--                                <span><i class="la la-map-signs"></i></span></span>--}}
                                    </div>
                                </div>
                                @if ($errors->has('contact_address'))
                                    <span class="form-control-feedback">
                                     {{ $errors->first('contact_address') }}
                                </span>
                                    <br>
                                @endif
                            </div>
                        </div>
                        {{--                        // địa chỉ liên hệ--}}
                        <div class="form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Địa chỉ liên hệ')}}:
                            </label>
                            <div class=" row mb-3">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <select name="residence_province_id" id="residence_province_id" class="form-control">
                                            <option value="">Chọn Tỉnh/Thành phố</option>
                                            @foreach($optionProvince as $key => $value)
                                                @if ($item['residence_province_id'] == $key)
                                                    <option value="{{$key}}" selected>{{$value}}</option>
                                                @else
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <input type="hidden" name="residence_district_id_hide" id="residence_district_id_hide"
                                               value="{{$item['residence_district_id']}}">
                                        <select name="residence_district_id" id="residence_district_id" class="form-control residence_district_id"
                                                title="@lang('Chọn quận / huyện')" style="width: 100%">
                                            <option value="">Chọn Quận / Huyện</option>
                                            @if($item['residence_district_id'] != null)
                                                <option value="{{$item['residence_district_id']}}" selected
                                                >{{$item['lienhe_dis_type'] . ' '. $item['lienhe_dis_name']}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input id="residence_address" name="residence_address" class="form-control  autosizeme"
                                               placeholder="@lang('Nhập địa chỉ khách hàng')"
                                               data-autosize-on="true"
                                               style="overflow: hidden; overflow-wrap: break-word; resize: horizontal;"
                                               value="{{$item['residence_address']}}">
                                        {{--                                        <span class="m-input-icon__icon m-input-icon__icon--right">--}}
                                        {{--                                <span><i class="la la-map-signs"></i></span></span>--}}
                                    </div>
                                </div>
                                @if ($errors->has('residence_address'))
                                    <span class="form-control-feedback">
                                     {{ $errors->first('residence_address') }}
                                </span>
                                    <br>
                                @endif
                            </div>
                        </div>
{{--                        // số cmnd--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Số CMND')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="ic_no" name="ic_no" class="form-control m-input"
                                       value="{{$item['ic_no']}}" >
{{--                                <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                                        <span><i class="flaticon-shapes"></i></span></span>--}}
                            </div>
                        </div>
{{--                        // nơi cấp--}}
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Nơi cấp')}}:
                            </label>
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="ic_place" name="ic_place" class="form-control m-input"
                                       value="{{$item['ic_place']}}" >
{{--                                <span class="m-input-icon__icon m-input-icon__icon--right">--}}
{{--                                        <span><i class="flaticon-map-location"></i></span></span>--}}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Mặt trước')}}:
                            </label>
                            <div class="col-lg-8">
{{--                                <img src="{{$item['ic_front_image']}}" height="auto" width="100%">--}}
                                <input type="hidden" id="ic_front_image" name="ic_front_image"
                                       value="{{$item['ic_front_image']}}">
                                <input type="hidden" id="ic_front_image_upload" name="ic_front_image_upload"
                                       value="">
                                <div class="m-widget19__pic">
                                    @if($item['ic_front_image']!=null)
                                        <img class="m--bg-metal m-image img-sd" id="blah_ic_front_image"
                                             src="{{$item['ic_front_image']}}" width="220px" height="220px"
                                             alt="{{__('Hình ảnh')}}"/>
                                    @else
                                        <img class="m--bg-metal  m-image img-sd" id="blah_ic_front_image"
                                             src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                             alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                    @endif

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="getFile_ic_front_image" type='file'
                                       onchange="uploadImageICFront(this);"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('getFile_ic_front_image').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Mặt sau')}}:
                            </label>
                            <div class="col-lg-8">
{{--                                <img src="{{$item['ic_back_image']}}" height="auto" width="100%">--}}
                                <input type="hidden" id="ic_back_image" name="ic_back_image"
                                       value="{{$item['ic_back_image']}}">
                                <input type="hidden" id="ic_back_image_upload" name="ic_back_image_upload"
                                       value="">
                                <div class="m-widget19__pic">
                                    @if($item['ic_back_image']!=null)
                                        <img class="m--bg-metal m-image img-sd" id="blah_ic_back_image"
                                             src="{{$item['ic_back_image']}}" width="220px" height="220px"
                                             alt="{{__('Hình ảnh')}}"/>
                                    @else
                                        <img class="m--bg-metal  m-image img-sd" id="blah_ic_back_image"
                                             src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                             alt="{{__('Hình ảnh')}}" width="220px" height="220px"/>
                                    @endif

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="getFile" type='file'
                                       onchange="uploadImageICBack(this);"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('getFile').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="customer_id_hidden" value="{{$item['customer_id']}}">
        </form>

        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.customer-broker')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('HUỶ')}}</span>
						</span>
                    </a>
                    <button type="submit" onclick="customer.resetPass('{!! $item['customer_id'] !!}')"
                            class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<i class="la la-refresh"></i>
							<span>{{__('Reset mật khẩu')}}</span>
							</span>
                    </button>
                    <button type="button" onclick="customer.editInfoCustomer()"
                            class="btn btn-primary color_button m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
							<span>
							<i class="la la-edit"></i>
							<span>{{__('CẬP NHẬT')}}</span>
							</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @include('admin::customer-broker.modal.modal-reset-password')
    @include('admin::customer-broker.modal.modal-reset-password-success')
@stop

@section("after_style")
    {{--    <link rel="stylesheet" href="{{asset('static/backend/css/process.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">

@stop
@section('after_script')
    <script type="text/template" id="tpl-not-auto-password">
        <div class="m-input-icon m-input-icon--right">
            <input type="password" class="form-control" id="password2" name="password2"
                   placeholder="@lang('Nhập mật khẩu')">
            <a href="javascript:void(0)" onclick="customer.show_password('#password2')"
               class="m-input-icon__icon m-input-icon__icon--right">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i class="la la-eye icon-padding"></i></span>
                 </span>
            </a>
        </div>
    </script>
    <script type="text/template" id="tpl-auto-password">
        <div class="m-input-icon m-input-icon--right">
            <input type="password" class="form-control" id="password" name="password"
                   placeholder="@lang('Nhập mật khẩu')">
            <a href="javascript:void(0)" onclick="customer.show_password('#password')"
               class="m-input-icon__icon m-input-icon__icon--right">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span><i  class="la la-eye icon-padding"></i></span>
                 </span>
            </a>
        </div>
    </script>

    <script>
        // Shorthand for $( document ).ready()
        $(function () {
            $('#birthday').datepicker({
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

        });

    </script>

    <script src="{{asset('static/backend/js/admin/customer-broker/edit.js?v='.time())}}" type="text/javascript"></script>
@stop