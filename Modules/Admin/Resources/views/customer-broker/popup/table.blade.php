<table class="table table_service_popup">
    <thead>
        <th></th>
        <th>Tên dịch vụ</th>
    </thead>
    <tbody>
    @if(isset($list) && count($list) != 0)
        @foreach($list as $item)
            <tr class="tr_service">
                <td>
                    <label class="kt-checkbox d-block check">
                        <input type="checkbox" class="service_id" name="service[]" value="{{$item['service_id']}}">
                        <span></span>
                    </label>
                </td>
                <td>{{$item['service_name_vi']}}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="3">Không có dữ liệu</td>
        </tr>
    @endif
    </tbody>
</table>
@if(isset($list) && count($list) != 0)
    {{ $list->links('notification::notification.helper.pagination') }}
@endif