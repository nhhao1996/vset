<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Mã nhà đầu tư')}}</th>
            <th class="tr_thead_list">{{__('Họ và tên')}}</th>
            <th class="tr_thead_list">{{__('Email')}}</th>
            <th class="tr_thead_list">{{__('Số người giới thiệu')}}</th>
            <th class="tr_thead_list">{{__('Referral link')}}</th>
            <th class="tr_thead_list">{{__('Referral code')}}</th>
            <th class="tr_thead_list">{{__('Trạng thái')}}</th>
            <th class="tr_thead_list text-center">{{__('Hành Động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        <?php
            $color = ["success", "brand", "danger", "accent", "warning", "metal", "primary", "info"];
        ?>
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                @php($num = rand(0,7))

                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td class="text_middle">{{$item['customer_code']}}</td>
                    <td class="text_middle">
                        <div class="m-list-pics m-list-pics--sm">
                            @if($item['customer_avatar']!=null)
                                <div class="m-card-user m-card-user--sm">
                                    <div class="m-card-user__pic">
                                        <img src="{{$item['customer_avatar']}}"
                                             onerror="this.onerror=null;this.src='https://placehold.it/40x40/00a65a/ffffff/&text=' + '{{substr(str_slug($item['full_name']),0,1)}}';"
                                             class="m--img-rounded m--marginless" alt="photo" width="40px"
                                             height="40px">
                                    </div>
                                    <div class="m-card-user__details">
                                        <a href="{{route("admin.customer-broker.detail",$item['customer_id'])}}"
                                           class="m-card-user__name line-name font-name">{{$item['full_name']}}</a>
{{--                                        <span class="m-card-user__email font-sub">--}}
{{--                                                {{$item['group_name']}}--}}
{{--                                            </span>--}}
                                    </div>
                                </div>
                            @else
                                <span style="width: 150px;">
                                        <div class="m-card-user m-card-user--sm">
                                            <div class="m-card-user__pic">
                                                <div class="m-card-user__no-photo m--bg-fill-{{$color[$num]}}">
                                                    <span>
                                                        {{substr(str_slug($item['full_name']),0,1)}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="m-card-user__details">
                                                <a href="{{route("admin.customer-broker.detail",$item['customer_id'])}}"
                                                   class="m-card-user__name line-name font-name">{{$item['full_name']}}</a>
{{--                                                <span class="m-card-user__email font-sub">{{$item['group_name']}}</span>--}}
                                            </div>
                                        </div>
                                    </span>
                            @endif
                        </div>
                    </td>
                        <td>{{$item['email']}}</td>
                        <td>{{$item['total']}}</td>
                        <td>{{$item['refer_link']}}</td>
                        <td>{{$item['refer_code']}}</td>
                    <td class="text_middle">
                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                <label class="ss--switch">
                                    <input type="checkbox"
                                           {{$item['is_actived'] == 1 ? 'checked' : ''}} disabled
                                           class="manager-btn" name="">
                                    <span></span>
                                </label>
                            </span>
                    </td>
                    <td class="text-center">
                        @if(in_array('admin.customer-broker.detail', session('routeList')))
                            <a class ="a-custom" href="{{route("admin.customer-broker.detail",$item['customer_id'])}}" title="Chi tiết">
                                <i class="flaticon-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.customer-broker.edit', session('routeList')))
                            <a class="a-custom" href="{{route("admin.customer-broker.edit",$item['customer_id'])}}" title="Chỉnh sửa">
                                <i class="flaticon-edit"></i>
                            </a>
                        @endif
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }
    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }
    .a-custom .flaticon-eye{
        top: 1px;
        left: 2px;
    }
</style>
