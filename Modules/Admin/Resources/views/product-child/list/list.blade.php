<div class="table-content">
    <div class="table-responsive">
        <table class="table table-striped m-table ss--header-table">
            <thead>
            <tr class="ss--nowrap">
                <th class="ss--font-size-th">#</th>
                <th class="ss--font-size-th">TÊN</th>
                <th class="ss--font-size-th">DANH MỤC</th>
                <th class="ss--font-size-th">NHÃN</th>
                <th class="ss--font-size-th">GIÁ</th>
                <th class="ss--font-size-th ss--text-center">TÌNH TRẠNG</th>
                @if($typeTab == 'sale')
                    <th class="ss--font-size-th ss--text-center">% GIẢM GIÁ</th>
                @endif
                <th class="ss--font-size-th"></th>
            </tr>
            </thead>
            <tbody>
            @if (isset($LIST))
                @foreach ($LIST as $key => $item)
                    <tr>
                        <td>{{$stt + $key}}</td>
                        <td>{{$item['product_child_name']}}</td>
                        <td>{{$item['category_name']}}</td>
                        <td>{{$item['product_model_name']}}</td>
                        <td>
                            {{number_format($item['price'],0,"",",")}}đ
                        </td>
                        <td class="ss--text-center">
                            {{$item['is_actived'] == 1 ? 'Hoạt động' : 'Tạm ngưng'}}
                        </td>
                        @if($typeTab == 'sale')
                            <td>
                                {{$item['percent_sale']}}
                            </td>
                        @endif
                        <td>
                            <button onclick="productChild.removeList(this, '{{ $typeTab }}', '{{ $item['product_child_id'] }}')"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="Xóa"><i class="la la-trash"></i></button>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    @if(isset($LIST))
        {{ $LIST->links('helpers.paging') }}
    @endif
</div>