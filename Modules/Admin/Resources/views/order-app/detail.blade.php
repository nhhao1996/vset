@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-order.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ ĐƠN HÀNG')}}</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"></i> {{__('CHI TIẾT ĐƠN HÀNG TỪ APP')}}</span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row bdb">
                <div class="form-group col-lg-4 col_or_dt_av">
                    <div class="w-100">
                        <!--begin::Widget 14 Item-->
                        <div class="float-left cus_avatar">
                            @if($order['customer_avatar']!=null)
                                <img src="{{$order['customer_avatar']}}" alt=""
                                     width="60px" height="60px" class="img-cl">
                            @else
                                <img src="{{asset('uploads/admin/icon/person.png')}}" alt=""
                                     width="60px" class="img-cl">
                            @endif

                        </div>
                        <div class="float-right cus_or_dt">
                            <div>
                                        <span class="font-13 m--font-boldest">
                                            @if($order['customer_id']!=1)
                                                {{$order['full_name']}}
                                            @else
                                                {{__('Khách hàng vãng lai')}}
                                            @endif
                                        </span>
                            </div>
                            <div>
                                        <span class="sz_dt">
                                            @if($order['customer_id']!=1)
                                                {{__('Thành viên')}}
                                            @else
                                                {{__('Khách mới')}}
                                            @endif
                                        </span>
                            </div>
                            <div>
                                        <span class="sz_dt">
                                            @if($order['customer_id']!=1)
                                                <i class="la la-phone"></i> {{$order['phone1']}}
                                            @endif

                                        </span>
                            </div>
                        </div>


                        <!--end::Widget 14 Item-->
                    </div>
                </div>
                <div class="form-group col-lg-4 col_or_dt">
                    @if($receipt!=null)
                        <div>
                            <span class="font-13">{{__('Thu ngân')}}: <strong>{{$receipt['full_name']}}</strong></span>
                        </div>
                        <div>
                            <span class="font-13">{{__('Thời gian thu ngân')}}: <strong>{{date("H:i d/m/Y",strtotime($receipt['created_at']))}}</strong></span>
                        </div>
                    @endif
                </div>
                <div class="col-lg-4 button_tool col_or_dt">
                    @if($order['process_status']=='paysuccess')
                        <button class="btn btn-primary color_button m-btn--wide choose_cus">
                            {{__('ĐÃ THANH TOÁN')}}
                        </button>
                    @elseif($order['process_status']=='new')
                        <button class="btn btn-primary color_button m-btn--wide choose_cus">
                            {{__('CHƯA THANH TOÁN')}}
                        </button>
                    @elseif($order['process_status']=='confirmed')
                        <button class="btn btn-primary color_button m-btn--wide choose_cus">
                            {{__('ĐÃ XÁC NHẬN')}}
                        </button>
                    @elseif($order['process_status']=='pay-half')
                        <button class="btn btn-primary color_button m-btn--wide choose_cus">
                            {{__('THANH TOÁN CÒN THIẾU')}}
                        </button>
                    @endif
                </div>
            </div>
            <div class="m-section m--margin-top-10 bdb_order">
                <div class="m-section__content">
                    <div class="table-responsive">
                        <table class="table table-striped m-table">
                            <thead style="white-space: nowrap;">
                            <tr>
                                <th class="tr_thead_od_detail">{{__('TÊN DỊCH VỤ')}}</th>
                                <th class="tr_thead_od_detail">{{__('GIÁ DỊCH VỤ')}}</th>
                                <th class="tr_thead_od_detail text-center">{{__('SỐ LƯỢNG')}}</th>
                                <th class="tr_thead_od_detail text-center">{{__('GIẢM GIÁ')}}</th>
                                <th class="tr_thead_od_detail text-center">{{__('MÃ GIẢM GIÁ')}}</th>
                                <th class="tr_thead_od_detail">{{__('TỔNG TIỀN')}}</th>
                            </tr>
                            </thead>
                            <tbody style="font-size: 12px">
                            @foreach($oder_detail as $item)
                                <tr>
                                    <td>{{$item['object_name']}}</td>
                                    <td>{{number_format($item['price'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} @lang('đ')</td>
                                    <td class="text-center">{{$item['quantity']}}</td>
                                    <td class="text-center">{{number_format($item['discount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</td>
                                    <td class="text-center">
                                        @if($item['voucher_code']!=null)
                                            {{$item['voucher_code']}}
                                        @else
                                            {{__('Không có')}}
                                        @endif

                                    </td>
                                    <td>{{number_format($item['amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-md-3 w-me-40 font-13">
                        <label>{{__('Tổng tiền')}}</label>
                    </div>
                    <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{number_format($order['total'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}
                                {{__('đ')}} (@lang('đã bao gồm thuế'))</strong>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-md-3 w-me-40 font-13">
                        <label>{{__('Chiết khấu thành viên')}}</label>
                    </div>
                    <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{$order['discount_member'] != null ? number_format($order['discount_member'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0) : 0}} {{__('đ')}}</strong>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-md-3 w-me-40 font-13">
                        <label>{{__('Giảm giá')}}</label>
                    </div>
                    <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{number_format($order['discount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</strong>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-md-3 w-me-40 font-13">
                        <label>{{__('Mã giảm giá')}}</label>
                    </div>
                    <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>
                                @if($order['voucher_code']!=null)
                                    {{$order['voucher_code']}}
                                @else
                                    {{__('Không có')}}
                                @endif

                            </strong>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-md-3 w-me-40 font-13">
                        <label>{{__('Phí vận chuyển')}}</label>
                    </div>
                    <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{number_format($order['tranport_charge'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</strong>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group">
                <div class="row">
                    <div class="col-md-3 w-me-40 font-13">
                        <label>{{__('Thành tiền')}}</label>
                    </div>
                    <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{number_format($order['amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</strong>
                        </span>
                    </div>
                </div>
            </div>
            @if($receipt!=null)
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-md-3 w-me-40 font-13">
                            <label>{{__('Đã thanh toán')}}</label>
                        </div>
                        <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{number_format($receipt['amount_paid'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</strong>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-md-3 w-me-40 font-13">
                            <label>{{__('Còn nợ')}}</label>
                        </div>
                        <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right" style="color: red">
                            <strong>
{{--                                @if($receipt['amount']-$receipt['amount_paid'] < 0)--}}
                                {{--                                    0đ--}}
                                {{--                                @else--}}
                                {{number_format($order['amount']-$receipt['amount_paid'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} @lang('đ')
{{--                                @endif--}}
                            </strong>
                        </span>
                        </div>
                    </div>
                </div>
                @if($receipt['amount']-$receipt['amount_paid'] < 0)
                    <div class="form-group m-form__group">
                        <div class="row">
                            <div class="col-md-3 w-me-40 font-13">
                                <label>{{__('Tiền thòi')}}</label>
                            </div>
                            <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>
                                {{number_format($receipt['amount_return'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}
                            </strong>
                        </span>
                            </div>
                        </div>
                    </div>
                @endif
            @else
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-md-3 w-me-40 font-13">
                            <label>{{__('Đã thanh toán')}}</label>
                        </div>
                        <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right">
                            <strong>{{__('0đ')}}</strong>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-md-3 w-me-40 font-13">
                            <label>{{__('Còn nợ')}}</label>
                        </div>
                        <div class="col-md-9 w-me-60 font-13">
                        <span class="float-right" style="color: red">
                            <strong>{{number_format($order['amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}</strong>
                        </span>
                        </div>
                    </div>
                </div>
            @endif
            @if($receipt!=null)
                <div class="form-group m-form__group bdt_order bdb_order">
                    <div class="m-section__content">
                        <div class="table-responsive">
                            <table class="table table-striped m-table">
                                <thead style="white-space: nowrap;">
                                <tr>
                                    <th class="tr_thead_od_detail">{{__('HÌNH THỨC THANH TOÁN')}}</th>
                                    <th class="tr_thead_od_detail">{{__('TIỀN THANH TOÁN')}}</th>
                                    <th class="tr_thead_od_detail">{{__('MÃ THẺ')}}</th>
                                    <th class="tr_thead_od_detail">@lang('Ngày thanh toán')</th>
                                </tr>
                                </thead>
                                <tbody style="font-size: 12px">
                                @foreach($receipt_detail as $item)
                                    <tr>
                                        <td>
                                            @if($item['receipt_type']=='cash')
                                                {{__('Tiền mặt')}}
                                            @elseif($item['receipt_type']=='transfer')
                                                {{__('Chuyển khoản')}}
                                            @elseif($item['receipt_type']=='visa')
                                                {{__('Visa')}}
                                            @elseif($item['receipt_type']=='member_card')
                                                {{__('Thẻ thành viên')}}
                                            @elseif($item['receipt_type']=='member_money')
                                                {{__('Tài khoản thành viên')}}
                                            @endif
                                        </td>
                                        <td>
                                            {{number_format($item['amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} {{__('đ')}}
                                        </td>
                                        <td>
                                            @if($item['card_code']!=null)
                                                {{$item['card_code']}}
                                            @else
                                                {{__('Không có')}}
                                            @endif
                                        </td>
                                        <td>{{$item['created_at'] != null ? \Carbon\Carbon::parse($item['created_at'])->format('d/m/Y H:i') : ''}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            @endif
        </div>
        <div class="m-portlet__foot">
            <div class="m-form__actions m--align-right">
                <a href="{{route('admin.order-app')}}"
                   class="btn btn-metal bold-huy  m-btn m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('QUAY LẠI')}}</span>
						</span>
                </a>
                @if($order['process_status']=='paysuccess')
                    <button class="btn btn-info  color_button m--margin-left-10" onclick="submitPrint()">
                        <i class="la la-print"></i> {{__('IN HÓA ĐƠN')}}
                    </button>
                @endif
            </div>
        </div>

    </div>
    <form id="form-order-ss" target="_blank" action="{{route('admin.order.print-bill2')}}" method="GET">
        <input type="hidden" name="ptintorderid" id="orderiddd" value="{{$order['order_id']}}">
    </form>
@stop
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <script>
        function submitPrint() {
            $('#form-order-ss').submit();
        }
    </script>
@stop
