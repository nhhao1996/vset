<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Tiêu đề')}}</th>
            <th class="tr_thead_list">{{__('Email gửi')}}</th>
            <th class="tr_thead_list">{{__('Email nhận')}}</th>
            <th class="tr_thead_list">{{__('Trạng thái')}}</th>
            <th class="tr_thead_list">{{__('Ngày gửi')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td>{{$item['email_subject']}}</td>
                    <td>{{$item['email_from']}}</td>
                    <td>{{$item['email_to']}}</td>
                    <td>{{$item['is_run'] == 1 ? 'Đã gửi' : 'Chưa gửi'}}</td>
                    <td>{{$item['run_at'] == null ? 'N/A' : \Carbon\Carbon::parse($item['run_at'])->format('d-m-Y')}}</td>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}

