<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Thời gian')}}</th>
            <th class="ss--font-size-th">{{__('Giá trị')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                    <td data-time="{{$item->stock_chart_id}}">{{\Carbon\Carbon::parse(trim($item["time"]))->format("d-m-Y H:i")}}</td>



                    <td>
                        {{$item['value']}}
                    </td>
                    <td>
{{--                        @if(in_array('admin.stock-chart.edit', session('routeList')))--}}
                            <a onclick="StockChartHandler.showPopupEdit(event,{{$item->stock_chart_id}})" href="" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                                <i class="la la-edit"></i>
                            </a>
                        <a onclick="StockChartHandler.delete(event,{{$item->stock_chart_id}})" href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Chỉnh sửa">
                            <i class="icon-delete-file la la-close float-right"></i>

                        </a>

                        {{--                        @endif--}}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
 {{ $LIST->links('helpers.paging') }}
{{--.--}}