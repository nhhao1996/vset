@extends('layout')
@section('after_style')
    <link rel="stylesheet" href="{{ asset('static/backend/css/service-card.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static/backend/css/sinh-custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('static/backend/css/customize.css') }}">
@endsection
@section('title_header')
    <span class="title_header"><img src="{{ asset('uploads/admin/icon/icon-member.png') }}" alt="" style="height: 20px;">
        {{ __('QUẢN LÝ YÊU CẦU MUA GÓI ĐẦU TƯ - TIẾT KIỆM') }}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }

        .m-widget5 .m-widget5__item .m-widget5__pic>img {
            width: 100%
        }

        .form-control-feedback {
            color: red;
        }

        #create-bill {
            overflow: auto !important;
        }

    </style>
    <form id="frm">
        <div class="m-portlet m-portlet--head-sm">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-edit"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{ __('CHI TIẾT YÊU CẦU CHUYỂN ĐỔI TIẾT KIỆM SANG CỔ PHIẾU') }}
                        </h2>

                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <div class="ml-auto">
                        <a onclick="StockTransfer.handleSubmit()" href="#"
                            class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-md">
                            <span>
                                <span>Xác nhận</span>
                            </span>
                        </a>
                        <a href="{{route('admin.stock-transfer.getList')}}"
                            class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-md">
                            <span>
                                <span>Hủy</span>
                            </span>
                        </a>
                    </div>


                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="form-group col-lg-6">
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Mã yêu cầu
                            </label>
                            <div class="col-lg-8">
                                <input type="text" name="stock_tranfer_contract_id" id="stock_tranfer_contract_id" class="form-control"
                                    value="{{ $stock->stock_tranfer_contract_id }}" disabled="">
                                <input name="stock_tranfer_contract_code" id="stock_tranfer_contract_code" class="form-control"
                                    value="{{ $stock->stock_tranfer_contract_code }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Nhà đầu tư
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->full_name }}" disabled="">
                                {{-- <select class="form-control" id="staff_id">
                                <option value="">Chọn nhân viên hỗ trợ</option>
                                                                    <option value="1">admin</option>
                                                                    <option value="2">Ngân</option>
                                                                    <option value="3">Trương Anh</option>
                                                                    <option value="4">Võ Bá Tòng</option>
                                                                    <option value="5">Mỹ Linh</option>
                                                                    <option value="6">Võ Duy Phương</option>
                                                                    <option value="7">Hồ Đắc Thuỷ Tiên</option>
                                                                    <option value="8">Wao</option>
                                                            </select> --}}
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Mã NĐT
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->customer_code }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Số điện thoại
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->phone }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            {{-- <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-edit"></i>
                            </span>
                            <h2 class="m-portlet__head-text">
                                {{__('ẾU')}}
                            </h2>
        
                        </div> --}}

                            <h2 style="color:#027177;" class="col-form-label label col-lg-4 black-title">
                                Thông tin chuyển đổi
                            </h2>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Mã hợp đồng
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->customer_contract_code }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Giá trị hợp đồng
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->total_amount }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Ngày hết hạn hợp đồng
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->customer_contract_end_date }}" disabled="">
                            </div>
                        </div>







                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Đơn giá CP
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stockPublish->money }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Số lượng quy đổi
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->quantity }}" disabled="">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Thời gian
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{ $stock->created_at }}" disabled="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                <a href="javascript:void(0)"
                                    class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                                    <span>
                                        <span>Xem hợp đồng</span>
                                    </span>
                                </a>
                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">

                            </label>
                            <div class="col-lg-8">

                            </div>
                        </div>
                    </div>
                </div>
                <input name="customer_id" id="customer_id" value="317" hidden="">
                <input name="withdraw_request_id" id="withdraw_request_id" value="" hidden="">
                <input name="order_id" id="order_id" value="613" hidden="">
                <input name="total_amount" id="total_amount" value="20000000.000" hidden="">
                <input name="total" id="total" value="20000000.000" hidden="">
                <input type="hidden" class="payment_method_id" value="1">
                <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title ss--title m--font-bold"><i
                                        class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                                    </i>XÁC NHẬN</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>
                                        Số tiền cần thu:<b class="text-danger">*</b>
                                    </label>
                                    <div class="">
                                        <input type="text" id="amount" name="amount" class="form-control m-input name"
                                            placeholder="Nhập số tiền cần thu">
                                        <span class="error-name text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                                    <div class="m-form__actions m--align-right">
                                        <button data-dismiss="modal"
                                            class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                            <span class="ss--text-btn-mobi">
                                                <i class="la la-arrow-left"></i>
                                                <span>HỦY</span>
                                            </span>
                                        </button>
                                        <button type="button" onclick="BuyBondsRequest.add(0)"
                                            class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5">
                                            <span class="ss--text-btn-mobi">
                                                <i class="la la-check"></i>
                                                <span>LƯU THÔNG TIN</span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- hóa đơn -->
            </div>
            <div class="m-portlet__foot">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{ route('admin.buy-bonds-request') }}"
                            class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                            <span>
                                <i class="la la-arrow-left"></i>
                                <span>{{ __('QUAY LẠI ') }}</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="bill">

    </div>
@endsection
@section('after_style')
    <link rel="stylesheet" href="{{ asset('static/backend/css/son.css') }}">
    <link rel="stylesheet" href="{{ asset('static/backend/css/customize.css') }}">
@stop
@section('after_script')
    <script src="{{ asset('static/backend/js/admin/stock-transfer/stock_transfer_script.js?v=' . time()) }}"
        type="text/javascript"></script>
    <script src="{{ asset('static/backend/js/admin/buy-bonds-request/autoNumeric.min.js?v=' . time()) }}"></script>
    <script>
        new AutoNumeric.multiple('.name', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });

    </script>
    <script type="text/template" id="imageShow">
        <div class="wrap-img image-show-child m-3">
                <input type="hidden" name="img-transfer[]" value="{link_hidden}">
                <img class='m--bg-metal m-image img-sd' src='{{ asset('{link}') }}' alt='{{ __('Hình ảnh') }}' width="100px"
                     height="100px">
                <span class="delete-img-sv" style="display: block;">
                    <a href="javascript:void(0)" onclick="BuyBondsRequest.remove_img(this)">
                        <i class="la la-close class_remove"></i>
                    </a>
                </span>
            </div>
        </script>
    <script type="text/template" id="imageShowCash">
        <div class="wrap-img image-show-child m-3">
                <input type="hidden" name="img-transfer[]" value="{link_hidden}">
                <img class='m--bg-metal m-image img-sd' src='{{ asset('{link}') }}' alt='{{ __('Hình ảnh') }}' width="100px"
                     height="100px">
                <span class="delete-img-sv" style="display: block;">
                    <a href="javascript:void(0)" onclick="BuyBondsRequest.remove_img(this)">
                        <i class="la la-close class_remove"></i>
                    </a>
                </span>
            </div>
        </script>
    <script>
        BuyBondsRequest.getListReceiptDetail(1);
        BuyBondsRequest.dropzone();

    </script>

@stop
