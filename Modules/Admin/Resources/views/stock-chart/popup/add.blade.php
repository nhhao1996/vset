

    <div class="modal fade" id="popup-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title ss--title m--font-bold"><i
                            class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                        </i>Thêm giá trị</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="frmAddStockChart">
                    <div class="form-group m-form__group ">
                        <label class="black-title ">Thời gian</label>
                        <input name="time" class="form-control " placeholder="Nhập thời gian" />
                    </div>
                        <div class="form-group m-form__group ">
                            <label class="black-title ">Giá trị</label>
                            <input name="value" class="form-control " placeholder="Nhập giá trị" />
                        </div>

                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                        <div class="m-form__actions m--align-center">
                            <button data-dismiss="modal"
                                class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-arrow-left"></i>
                                    <span>Hủy</span>
                                </span>
                            </button>
                            <button id="btnAdd" onclick="StockChartHandler.add()" type="button"
                                class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>Thêm</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>


</div>
<div class="modal-footer">
    {{-- <h1>Hello </h1> --}}
    {{-- <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>Hủy</span>
                            </span>
                        </button>
                        <button id="btnPublishPopup" type="button"  class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>Phát hành</span>
                            </span>
                        </button>
                    </div>
                </div> --}}
</div>
</div>
</div>
</div>

