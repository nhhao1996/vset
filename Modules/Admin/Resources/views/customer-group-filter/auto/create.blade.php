@extends('layout')
@section('title_header')
    <span class="title_header">{{__('QUẢN LÝ CHI NHÁNH')}}</span>
@stop
@section('content')
    <style>
        .bdr {
            border-right: 1px dashed #e0e0e0 !important;
        }

        .padding-left-0 {
            padding-left: 0px;
        }

        .padding-right-0 {
            padding-right: 0px;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('THÊM NHÓM KHÁCH HÀNG ĐỘNG')}}
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
        </div>
        <div class="m-portlet__body" id="autotable">
            <div class="table-content">
                <div class="form-group row m--margin-bottom-5">
                    <div class="col-lg-2 padding-left-0">
                        <div class="form-group">
                            <label class="col-xl-12 col-lg-12 col-form-label">
                                {{__('Tên nhóm khách hàng')}}
                                <span class="color_red"></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 padding-left-0">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" placeholder="">
                            <span class="text-danger error-name"></span>
                        </div>
                    </div>
                </div>
                <div class="row A">
                    <div class="col-lg-12 padding-left-0">
                        <div class="m-portlet__body" id="autotable">
                            <div class="table-content">
                                <div class="form-group row">
                                    <label class="m--margin-right-10">
                                        {{__('Bao gồm những người đáp ứng')}}
                                    </label>
                                    <select name="A-or-and" id="A-or-and"
                                            class="form-control ss--select-2 ss-width-100pt"
                                            style="width: 20%">
                                        <option value="or">
                                            {{__('Bất kỳ')}}
                                        </option>
                                        <option value="and">
                                            {{__('Bao gồm')}}
                                        </option>
                                    </select>
                                    <label class="m--margin-left-10">
                                        {{__('điều kiện sau')}}
                                    </label>
                                </div>
                                <div class="form-group div-condition-A">

                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 padding-left-0">
                                        <button onclick="userGroupAuto.addConditionA()"
                                                class="btn btn-primary m-btn m-btn--custom m-btn--icon color_button btn-add-condition-A">
															<span>
																<i class="fa fa-plus"></i>
																<span>{{__('Thêm điều kiện')}}</span>
															</span>
                                        </button>
                                    </div>
                                </div>
                                {{--@foreach($condition as $item)--}}
                                {{--<div class="form-group row div-condition-A">--}}
                                {{--<div class="col-lg-6 padding-left-0">--}}
                                {{--<select disabled name="condition_A_{{$item['id']}}" id="condition_A_{{$item['id']}}"--}}
                                {{--class="form-control ss--select-2" style="width:100%">--}}
                                {{--<option value="{{$item['id']}}">{{$item['name']}}</option>--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-6 padding-right-0">--}}
                                {{--@if($item['id'] == 1)--}}
                                {{--<select name="A_1" id="A_1" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn nhóm khách hàng đã tạo</option>--}}
                                {{--@foreach($customerGroupDefine as $value)--}}
                                {{--<option value="{{$value['id']}}">{{$value['name']}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@elseif($item['id'] == 2)--}}
                                {{--<input type="text" name="A_2" id="A_2" class="form-control condition-A"--}}
                                {{--placeholder="Nhập số ngày">--}}
                                {{--@elseif($item['id'] == 3)--}}
                                {{--<select name="A_3" id="A_3" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn trạng thái</option>--}}
                                {{--<option value="new">Mới</option>--}}
                                {{--<option value="confirm">Xác nhận</option>--}}
                                {{--<option value="cancel">Hủy</option>--}}
                                {{--<option value="finish">Hoàn thành</option>--}}
                                {{--</select>--}}
                                {{--@elseif($item['id'] == 4)--}}
                                {{--<select name="A_4" id="A_4" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn thời gian hẹn</option>--}}
                                {{--<option value="morning">Sáng</option>--}}
                                {{--<option value="noon">Trưa</option>--}}
                                {{--<option value="afternoon">Chiều</option>--}}
                                {{--<option value="evening">Tối</option>--}}
                                {{--</select>--}}
                                {{--@elseif($item['id'] == 5)--}}
                                {{--<span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
                                {{--<label style="margin: 0 0 0 10px; padding-top: 4px">--}}
                                {{--<input name="A_5" id="A_5" type="checkbox"--}}
                                {{--class="manager-btn condition-A">--}}
                                {{--<span></span>--}}
                                {{--</label>--}}
                                {{--</span>--}}
                                {{--@elseif($item['id'] == 6)--}}
                                {{--<select name="A_6" id="A_6" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn dịch vụ</option>--}}
                                {{--@foreach($listService as $value)--}}
                                {{--<option value="{{$value['service_id']}}">{{$value['service_name']}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@elseif($item['id'] == 7)--}}
                                {{--<select name="A_7" id="A_7" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn dịch vụ</option>--}}
                                {{--@foreach($listService as $value)--}}
                                {{--<option value="{{$value['service_id']}}">{{$value['service_name']}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@elseif($item['id'] == 8)--}}
                                {{--<select name="A_8" id="A_8" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn sản phẩm</option>--}}
                                {{--@foreach($listProduct as $value)--}}
                                {{--<option value="{{$value['product_child_id']}}">{{$value['product_child_name']}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@elseif($item['id'] == 9)--}}
                                {{--<select name="A_9" id="A_9" class="form-control ss--select-2 condition-A"--}}
                                {{--style="width:100%">--}}
                                {{--<option value="">Chọn sản phẩm</option>--}}
                                {{--@foreach($listProduct as $value)--}}
                                {{--<option value="{{$value['product_child_id']}}">{{$value['product_child_name']}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}
                            </div>
                            <!-- end table-content -->

                        </div>
                    </div>
                </div>
                <div class="row B">
                    <div class="col-lg-12 padding-left-0">
                        <div class="m-portlet__body" id="autotable">
                            <div class="table-content">
                                <div class="form-group row">
                                    <label class="m--margin-right-10">
                                        {{__('Loại bỏ những người đáp ứng')}}
                                    </label>
                                    <select name="B-or-and" id="B-or-and"
                                            class="form-control ss--select-2 ss-width-100pt"
                                            style="width: 20%">
                                        <option value="or">
                                            {{__('Bất kỳ')}}
                                        </option>
                                        <option value="and">
                                            {{__('Bao gồm')}}
                                        </option>
                                    </select>
                                    <label class="m--margin-left-10">
                                        {{__('điều kiện sau')}}
                                    </label>
                                </div>
                                <div class="form-group div-condition-B">

                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 padding-left-0">
                                        <button onclick="userGroupAuto.addConditionB()"
                                                class="btn btn-primary m-btn m-btn--custom m-btn--icon color_button btn-add-condition-B">
															<span>
																<i class="fa fa-plus"></i>
																<span>{{__('Thêm điều kiện')}}</span>
															</span>
                                        </button>
                                    </div>
                                </div>
                                {{--@foreach($condition as $item)--}}
                                    {{--<div class="form-group row div-condition-B">--}}
                                        {{--<div class="col-lg-6 padding-left-0">--}}
                                            {{--<select disabled name="condition_B_{{$item['id']}}"--}}
                                                    {{--id="condition_B_{{$item['id']}}"--}}
                                                    {{--class="form-control ss--select-2" style="width:100%">--}}
                                                {{--<option value="{{$item['id']}}">{{$item['name']}}</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-lg-6 padding-right-0">--}}
                                            {{--@if($item['id'] == 1)--}}
                                                {{--<select name="B_1" id="B_1"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn nhóm khách hàng đã tạo</option>--}}
                                                    {{--@foreach($customerGroupDefine as $value)--}}
                                                        {{--<option value="{{$value['id']}}">{{$value['name']}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--@elseif($item['id'] == 2)--}}
                                                {{--<input name="B_2" id="B_2" type="text" class="form-control condition-B"--}}
                                                       {{--placeholder="Nhập số ngày">--}}
                                            {{--@elseif($item['id'] == 3)--}}
                                                {{--<select name="B_3" id="B_3"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn trạng thái</option>--}}
                                                    {{--<option value="new">Mới</option>--}}
                                                    {{--<option value="confirm">Xác nhận</option>--}}
                                                    {{--<option value="cancel">Hủy</option>--}}
                                                    {{--<option value="finish">Hoàn thành</option>--}}
                                                {{--</select>--}}
                                            {{--@elseif($item['id'] == 4)--}}
                                                {{--<select name="B_4" id="B_4"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn thời gian hẹn</option>--}}
                                                    {{--<option value="sang">Sáng</option>--}}
                                                    {{--<option value="trua">Trưa</option>--}}
                                                    {{--<option value="chieu">Chiều</option>--}}
                                                    {{--<option value="toi">Tối</option>--}}
                                                {{--</select>--}}
                                            {{--@elseif($item['id'] == 5)--}}
                                                {{--<span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
                                                {{--<label style="margin: 0 0 0 10px; padding-top: 4px">--}}
                                                {{--<input name="B_5" id="B_5" type="checkbox"--}}
                                                       {{--class="manager-btn condition-B">--}}
                                                {{--<span></span>--}}
                                                {{--</label>--}}
                                                {{--</span>--}}
                                            {{--@elseif($item['id'] == 6)--}}
                                                {{--<select name="B_6" id="B_6"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn dịch vụ</option>--}}
                                                    {{--@foreach($listService as $value)--}}
                                                        {{--<option value="{{$value['service_id']}}">{{$value['service_name']}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--@elseif($item['id'] == 7)--}}
                                                {{--<select name="B_7" id="B_7"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn dịch vụ</option>--}}
                                                    {{--@foreach($listService as $value)--}}
                                                        {{--<option value="{{$value['service_id']}}">{{$value['service_name']}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--@elseif($item['id'] == 8)--}}
                                                {{--<select name="B_8" id="B_8"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn sản phẩm</option>--}}
                                                    {{--@foreach($listProduct as $value)--}}
                                                        {{--<option value="{{$value['product_child_id']}}">{{$value['product_child_name']}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--@elseif($item['id'] == 9)--}}
                                                {{--<select name="B_9" id="B_9"--}}
                                                        {{--class="form-control ss--select-2 condition-B"--}}
                                                        {{--style="width:100%">--}}
                                                    {{--<option value="">Chọn sản phẩm</option>--}}
                                                    {{--@foreach($listProduct as $value)--}}
                                                        {{--<option value="{{$value['product_child_id']}}">{{$value['product_child_name']}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            </div>
                            <!-- end table-content -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- end table-content -->
        </div>
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.customer-group-filter')}}"
                       class="btn  btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('HỦY')}}</span>
						</span>
                    </a>
                    <button type="button" onclick="userGroupAuto.save(0)"
                            class="btn  btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-add-close m--margin-left-10">
							<span>
							<i class="la la-check"></i>
							<span>{{__('LƯU THÔNG TIN')}}</span>
							</span>
                    </button>
                    <button type="button" onclick="userGroupAuto.save(1)"
                            class="btn  btn-success color_button son-mb
                                    m-btn m-btn--icon m-btn--wide m-btn--md btn-add m--margin-left-10">
							<span>
							<i class="fa fa-plus-circle"></i>
							<span>{{__('LƯU')}} &amp; {{__('TẠO MỚI')}}</span>
							</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@endsection
@section('after_script')
    <script type="text/template" id="choose-condition-A">
        <div class="form-group row A-condition-1 div-A-1-condition">
            <div class="col-lg-4" style="padding-left: 0px">
                <select name="" id="" onchange="userGroupAuto.chooseConditionA(this)"
                        class="form-control ss--select-2 condition-A" style="width: 100%">
                    <option value="">
                        {{__('Chọn điều kiện')}}
                    </option>
                    {option}
                </select>
            </div>
            <div class="col-lg-6 div-content-condition">

            </div>
            <div class="col-lg-2">
                <button style="float: right;" type="button" onclick="userGroupAuto.removeConditionA(this)"
                        class="btn btn-secondary btn-icon ss-float-right ss-width-5rem">
                    <i class="la la-close"></i>
                </button>

            </div>
        </div>
    </script>
    <script type="text/template" id="choose-condition-B">
        <div class="form-group row B-condition-1 div-B-1-condition">
            <div class="col-lg-4" style="padding-left: 0px">
                <select name="" id="" onchange="userGroupAuto.chooseConditionB(this)"
                        class="form-control ss--select-2 condition-B" style="width: 100%">
                    <option value="">
                        {{__('Chọn điều kiện')}}
                    </option>
                    {option}
                </select>
            </div>
            <div class="col-lg-6 div-content-condition">

            </div>
            <div class="col-lg-2">
                <button style="float: right;" type="button" onclick="userGroupAuto.removeConditionB(this)"
                        class="btn btn-secondary btn-icon ss-float-right ss-width-5rem">
                    <i class="la la-close"></i>
                </button>

            </div>
        </div>
    </script>
    <script type="text/template" id="tpl-customer-group-define">
        <select name="" id=""
                class="form-control ss--select-2 chooses-condition-A" style="width: 100%">
            <option value="">
                {{__('Chọn nhóm')}}
            </option>
            @foreach($customerGroupDefine as $item)
                <option value="{{$item['id']}}">
                    {{$item['name']}}
                </option>
            @endforeach
        </select>
    </script>
    <script type="text/template" id="tpl-day-appointment">
        <input type="text" class="form-control inputmask chooses-condition-A" value="30" placeholder="{{__('Nhập số ngày')}}" title="{{__('Số ngày')}}">
    </script>
    <script type="text/template" id="tpl-status_appointment">
        <select name="" id=""
                class="form-control ss--select-2 chooses-condition-A" style="width: 100%">
            <option value="">
                {{__('Chọn trạng thái')}}
            </option>
            <option value="new">
                {{__('Mới')}}
            </option>
            <option value="confirm">
                {{__('Xác nhận')}}
            </option>
            <option value="cancel">
                {{__('Hủy')}}
            </option>
            <option value="finish">
                {{__('Hoàn thành')}}
            </option>
            <option value="wait">
                {{__('Chờ phục vụ')}}
            </option>

        </select>
    </script>
    <script type="text/template" id="tpl-time_appointment">
        <select name="" id=""
                class="form-control ss--select-2 chooses-condition-A" style="width: 100%">
            <option value="">
                {{__('Chọn thời gian')}}
            </option>
            <option value="morning">
                {{__('Sáng (07h - 12h)')}}
            </option>
            <option value="noon">
                {{__('Trưa (12h - 14h)')}}
            </option>
            <option value="afternoon">
                {{__('Chiều (14h - 18h)')}}
            </option>
            <option value="evening">
                {{__('Tối (18h - 22h)')}}
            </option>
        </select>
    </script>
    <script type="text/template" id="tpl-not_appointment">
        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
            <label style="margin: 0 0 0 10px; padding-top: 4px">
                <input type="checkbox" checked class="manager-btn chooses-condition-A">
                <span></span>
            </label>
        </span>
    </script>
    <script type="text/template" id="tpl-use_service">
        <select name="" id=""
                class="form-control ss--select-2 chooses-condition-A" multiple="multiple" style="width: 100%">
            @foreach($listService as $item)
                <option value="{{$item['service_id']}}">
                    {{$item['service_name']}}
                </option>
            @endforeach
        </select>
    </script>
    <script type="text/template" id="tpl-use_product">
        <select name="" id=""
                class="form-control ss--select-2 chooses-condition-A" multiple="multiple" style="width: 100%">
            @foreach($listProduct as $item)
                <option value="{{$item['product_child_id']}}">
                    {{$item['product_child_name']}}
                </option>
            @endforeach
        </select>
    </script>
    <script src="{{asset('static/backend/js/admin/user-group/add.js?v='.time())}}"
            type="text/javascript"></script>
@endsection
