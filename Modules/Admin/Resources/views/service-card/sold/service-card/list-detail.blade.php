<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table ss--nowrap">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--text-center ss--font-size-th">{{__('NGÀY SỬ DỤNG')}}</th>
            <th class="ss--text-center ss--font-size-th">{{__('KHÁCH HÀNG')}}</th>
            <th class="ss--text-center ss--font-size-th">{{__('MÃ ĐƠN HÀNG')}}</th>
            <th class="ss--text-center ss--font-size-th">{{__('SỐ LƯỢNG')}}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($LIST))
            @foreach($LIST as $key=>$value)
                <tr class="ss--nowrap">
                    <td class="ss--text-center ss--font-size-13">{{isset($page) ? $page*10 + $key + 1 : $key + 1}}</td>
                    <td class="ss--text-center ss--font-size-13">{{date_format(new DateTime($value['day_using']), 'd/m/Y H:i:s')}}</td>
                    <td class="ss--text-center ss--font-size-13">{{$value['customer']}}</td>
                    <td class="ss--text-center ss--font-size-13">{{$value['order_code']}}</td>
                    <td class="ss--text-center ss--font-size-13">{{$value['quantity']}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
@if(isset($LIST))
    {{ $LIST->links('admin::service-card.sold.helper-paging') }}
@endif