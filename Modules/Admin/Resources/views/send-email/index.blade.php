@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <style>
        .note-editor {
            width: 100%;
        }
        .hover_cursor:hover {
            cursor: pointer;
        }
    </style>
@endsection
@section('title_header')
    <span class="title_header text-uppercase"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        @lang('admin::send-email.index.title')
    </span>
@endsection
@section('content')
    <form id="form-update" autocomplete="off">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                        <h3 class="m-portlet__head-text">
                            @lang('admin::send-email.index.title')
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">
                                Email nhân viên
                                <span class="color_red">*</span></label>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="email_staff" id="email_staff" value="{{$config['value']}}">
                                        <span class="color_red position_error position_error_email_staff"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">
                                Số điện thoại nhân viên
                                <span class="color_red">*</span></label>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="phone_staff" id="phone_staff" value="{{$phoneStaff['value']}}">
                                        <span class="color_red position_error position_error_phone_staff"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">
                                @lang('admin::send-email.index.email_cc')
                                <span class="color_red">*</span></label>
                            <div class="col-lg-9 col-xl-9">
                                <button type="button" onclick="sendEmail.addField()" class="btn btn-primary mb-4">
                                    @lang('admin::send-email.index.add_email')
                                </button>
                                <div>
                                    <div class="row field_cc">
                                        @if(isset($emailCC) && count($emailCC) != 0)
                                            @foreach($emailCC as $key => $item)
                                                <div class="col-12 mb-3 field_email_cc_form field_email_cc_{{$key}}">
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-6">
                                                            <input type="text" class="form-control email_cc" value="{{$item['email']}}">
                                                        </div>
                                                        <div class="col-1">
{{--                                                            <span class="kt-switch kt-switch--success">--}}
{{--                                                                <label class="mb-0">--}}
{{--                                                                    <input type="checkbox" class="email_cc_check" {{$item['is_actived'] == 1 ? 'checked' : ''}}>--}}
{{--                                                                    <span></span>--}}
{{--                                                                </label>--}}
{{--                                                            </span>--}}
                                                            <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                                                <label style="margin: 0 0 0 10px; padding-top: 4px">
                                                                    <input type="checkbox" id="is_actived" class="email_cc_check" {{$item['is_actived'] == 1 ? 'checked' : ''}}>
                                                                    <span></span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                        <div class="col-1">
                                                            <i class="la la-close kt-icon-xl text-danger hover_cursor" onclick="sendEmail.deleteField('{{$key}}')"></i>
                                                        </div>
                                                    </div>
                                                    <span class="color_red position_error position_error_{{$key}}"></span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer save-attribute m--margin-right-20">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.product')}}"
                           class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                        </a>
                        <button onclick="sendEmail.save()" type="button"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                            <i class="la la-check"></i>
                            <span>{{__('LƯU')}}</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('after_script')
    <script>
        var number = "{{$sumListEmail != 0 ? $sumListEmail : 0}}";
    </script>
    <script type="text/template" id="field_email_cc">
        <div class="col-12 mb-3 field_email_cc_form field_email_cc_{number}">
            <div class="row d-flex align-items-center">
                <div class="col-6">
                    <input type="text" class="form-control email_cc">
                </div>
                <div class="col-1">
{{--                    <span class="kt-switch kt-switch--success">--}}
{{--                        <label class="mb-0">--}}
{{--                            <input type="checkbox" class="email_cc_check" checked>--}}
{{--                            <span></span>--}}
{{--                        </label>--}}
{{--                    </span>--}}
                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                            <input type="checkbox" id="is_actived" class="email_cc_check" checked>
                            <span></span>
                        </label>
                    </span>
                </div>
                <div class="col-1">
                    <i class="la la-close kt-icon-xl text-danger hover_cursor" onclick="sendEmail.deleteField('{number}')"></i>
                </div>
            </div>
            <span class="color_red position_error position_error_{number}"></span>
        </div>
    </script>

    <script>
        var name_required = "@lang('admin::send-email.validate.name_required')";
        var name_max = "@lang('admin::send-email.validate.name_max')";
        var email_required = "@lang('admin::send-email.validate.email_required')";
        var email_type = "@lang('admin::send-email.validate.email_type')";
        var email_max = "@lang('admin::send-email.validate.email_max')";
        var email_cc_required = "@lang('admin::send-email.validate.email_cc_required')";
        var email_cc_max = "@lang('admin::send-email.validate.email_cc_max')";
        var email_cc_type = "@lang('admin::send-email.validate.email_cc_type')";
        var email_success = "@lang('admin::send-email.validate.email_success')";
        var email_fail = "@lang('admin::send-email.validate.email_fail')";
        var cancel = "@lang('admin::send-email.validate.cancel')";
        var yes_text = "@lang('admin::send-email.validate.yes_text')";
        var yes = "@lang('admin::send-email.validate.yes')";
        var no = "@lang('admin::send-email.validate.no')";
        var email_using = "@lang('admin::send-email.validate.email_cc_using')";
    </script>

    <script src="{{asset('static/backend/js/admin/send-email/script.js?v='.time())}}" type="text/javascript"></script>

    <script>
        // sendEmail._init();
        var loading = new KTPortlet('form-update');


@stop
