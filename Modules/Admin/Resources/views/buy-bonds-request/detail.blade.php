@extends('layout')
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/service-card.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ YÊU CẦU MUA GÓI ĐẦU TƯ - TIẾT KIỆM')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
        .form-control-feedback {
            color : red;
        }
        #create-bill {
            overflow: auto !important;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT YÊU CẦU MUA GÓI ĐẦU TƯ - TIẾT KIỆM')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                @if(!in_array($detail['process_status'],['paysuccess','ordercancle']))
                    @if(
                            $detail['phone2_customer'] == null ||
                            $detail['residence_address_customer'] == null ||
                            $detail['ic_no_customer'] == null ||
                            strlen(str_replace(' ','',$detail['phone2_customer'] )) == 0 ||
                            strlen(str_replace(' ','',$detail['ic_no_customer'] )) == 0 ||
                            strlen(str_replace(' ','',$detail['residence_address_customer'] )) == 0

                    )
                        <button type="button" onclick="BuyBondsRequest.confirmFail(2)"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                            <span class="text-white">
{{--                                <i class="fas fa-check-square"></i>--}}
                                <span class="zxrem">  {{__('Xác nhận')}}</span>
                            </span>
                        </button>
                    @else
                        @if(($detail['payment_method_id'] == 2 || $detail['payment_method_id'] == 3) && $detail['process_status'] != 'paysuccess')
                            @if(in_array('admin.buy-bonds-request.createWallet', session('routeList')))
                                @if((double)$total < (double)$detail['total'])
                                    <button type="button" onclick="BuyBondsRequest.confirmFail(1)"
                                            class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                    <span class="text-white">
{{--                                        <i class="fas fa-check-square"></i>--}}
                                        <span class="zxrem">  {{__('Xác nhận')}}</span>
                                    </span>
                                    </button>
                                @else
                                    <a href="javascript:void(0)" onclick="BuyBondsRequest.addWallet()"
                                       class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                    <span class="text-white">
{{--                                        <i class="fas fa-check-square"></i>--}}
                                        <span class="zxrem">  {{__('Xác nhận')}}</span>
                                    </span>
                                    </a>
                                @endif
                            @endif
                        @elseif ($detail['process_status'] == 'new')
                            {{--                    <a data-toggle="modal" data-target="#modalAdd"  --}}
                            @if(in_array('admin.buy-bonds-request.createReceipt', session('routeList')))
                                <a href="javascript:void(0)" onclick="BuyBondsRequest.add(0)"
                                   class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                    <span class="text-white">
{{--                                        <i class="fas fa-check-square"></i>--}}
                                        <span class="zxrem">  {{__('Xác nhận')}}</span>
                                    </span>
                                </a>
                            @endif
                        @elseif ($detail['process_status'] != 'new' && $detail['process_status'] != 'paysuccess')
                            {{--                    <a data-toggle="modal" data-target="#modalMakeReceipt"--}}
                            @if(in_array('admin.buy-bonds-request.createReceipt', session('routeList')))
                                <a href="javascript:void(0)" onclick="BuyBondsRequest.showPopupReceiptDetail()"
                                   class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                    <span class="text-white">
{{--                                        <i class="fas fa-check-square"></i>--}}
                                        <span class="zxrem">  {{__('Tạo phiếu thu')}}</span>
                                    </span>
                                </a>
                            @endif
                        @endif
                    @endif
                    <a href="javascript:void(0)" onclick="BuyBondsRequest.cancelOrder()" class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                        <span class="text-white">
{{--                                <i class="fas fa-check-square"></i>--}}
                            <span class="zxrem">  {{__('Không xác nhận')}}</span>
                        </span>
                    </a>
                @endif

            </div>
        </div>

        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Mã đơn hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['order_code'] == null ? 'N/A' :$detail['order_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Nhân viên hỗ trợ
                        </label>
                        <div class="col-lg-8">
                            <select class="form-control" id="staff_id" {{$detail['process_status'] != 'new' ? 'disabled' : ''}}>
                                <option value="">Chọn nhân viên hỗ trợ</option>
                                @foreach($listStaff as $item)
                                    <option value="{{$item['staff_id']}}" {{$detail['staff_id'] == $item['staff_id'] ? 'selected' : ''}}>{{$item['full_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tên gói :
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['product_name_vi'] == null ? 'N/A' :$detail['product_name_vi']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            @lang('Loại gói'):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['type_bonds'] == null ? 'N/A' : $detail['type_bonds']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Trạng thái đơn hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['status'] == null ? 'N/A' : $detail['status']}}" disabled="">
                        </div>
                    </div>
                    @if($detail['process_status'] == 'ordercancle')
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Lý do (VI):
                            </label>
                            <div class="col-lg-8">
                                <textarea class="form-control"  disabled="">{{$detail['reason_vi'] == null ? 'N/A' : $detail['reason_vi']}}</textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Lý do (EN):
                            </label>
                            <div class="col-lg-8">
                                <textarea class="form-control"  disabled="">{{$detail['reason_en'] == null ? 'N/A' : $detail['reason_en']}}</textarea>
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số lượng gói:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['quantity'] == null ? 'N/A' : $detail['quantity']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                           Giá trị gói (Vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['price_standard'] == null ? 'N/A' : number_format($detail['price_standard'], 2) }}" disabled="">
                        </div>
                    </div>
{{--                    <div class="row form-group">--}}
{{--                        <label class="col-form-label label col-lg-4 black-title">--}}
{{--                            Tỉ lệ lãi xuất chuẩn (%) :--}}
{{--                        </label>--}}
{{--                        <div class="col-lg-8">--}}
{{--                            <input class="form-control" value="{{$detail['interest_rate_standard'] == null ? 'N/A' : $detail['interest_rate_standard']}}" disabled="">--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Loại đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['term_time_type'] =='1' ? 'Có kỳ hạn' : 'Không có kỳ hạn'}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                          Kỳ hạn đầu tư (tháng):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['investment_time_month'] == null ? 'N/A' : $detail['investment_time_month']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Kỳ hạn rút lãi (tháng):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['withdraw_interest_month'] == null || $detail['term_time_type'] == 0 ? 'N/A' : $detail['withdraw_interest_month']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Lãi suất  (%):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['interest_rate'] == null ? number_format(0,2) : number_format($detail['interest_rate'],2)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tổng lãi suất hàng tháng (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['month_interest'] == null ? number_format(0,2) : number_format($detail['month_interest'],2)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tổng lãi suất (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['total_interest'] == null || $detail['term_time_type'] == 0 ? number_format(0,2) : number_format($detail['total_interest'],2)}}" disabled="">
                        </div>
                    </div>
                    @if(count($listService) != 0)
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                E-voucher tặng kèm:
                            </label>
                            <div class="col-lg-8">
                                <div class="p-3" style="background:#f4f5f8">
                                    @foreach($listService as $item)
                                        <p>{{$item['service_name']}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Hình thức thanh toán:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['payment_method_name_vi'] == null ? 'N/A' : $detail['payment_method_name_vi']}}" disabled="">
                        </div>
                    </div>
                    @if($detail['payment_method_id'] == 1)
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Cú pháp giao dịch
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{$detail['order_code']}}" disabled="">
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Ngày thanh toán:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['payment_date'] == null ? 'N/A' : \Carbon\Carbon::parse($detail['payment_date'])->format('H:i:s d-m-Y')}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Họ và tên nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['full_name_customer'] == null ? 'N/A' : $detail['full_name_customer']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Chứng minh nhân dân nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control {{$detail['ic_no_customer'] == null || strlen(str_replace(' ','',$detail['ic_no_customer'] )) == 0 ? 'text-danger' : ''}}" value="{{$detail['ic_no_customer'] == null || strlen(str_replace(' ','',$detail['ic_no_customer'] )) == 0 ? 'N/A' : $detail['ic_no_customer']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số điện thoại nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control {{$detail['phone2_customer'] == null || strlen(str_replace(' ','',$detail['phone2_customer'] )) == 0 ? 'text-danger' : ''}}" value="{{$detail['phone2_customer'] == null || strlen(str_replace(' ','',$detail['phone2_customer'] )) == 0 ? 'N/A' : $detail['phone2_customer']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Email nhà đầu tư :
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control " value="{{$detail['email_customer'] == null || strlen(str_replace(' ','',$detail['email_customer'] )) == 0 ? 'N/A' : $detail['email_customer']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Địa chỉ nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            @if($detail['residence_address'] == null && $detail['lienhe_dis_name'] == null && $detail['lienhe_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control" value="{{$detail['residence_address']}}, {{$detail['lienhe_dis_type']}}  {{$detail['lienhe_dis_name']}},{{$detail['lienhe_pro_type']}} {{$detail['lienhe_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Người giới thiệu :
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['refer_name'] == null ? 'N/A' : $detail['refer_customer_code'] .' - '. $detail['refer_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tỉ lệ thưởng (%):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['commission_rate'] == null ? number_format(0,2) : number_format($detail['commission_rate'],2)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tổng tiền thưởng (vnđ) :
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['commission'] == null ? number_format(0,2) : number_format($detail['commission'],2)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tỉ lệ tặng thêm (%):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bonus_rate'] == null ? number_format(0,2) : number_format($detail['bonus_rate'],2)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tổng tiền tặng (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['bonus'] == null ? number_format(0,2) : number_format($detail['bonus'],2)}}" disabled="">
                        </div>
                    </div>
                    @if($detail['payment_method_id'] == 2 || $detail['payment_method_id'] == 3)
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Số dư khả dụng (vnđ):
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control {{(double)$total >= (double)$detail['total'] ? 'text-success' : 'text-danger'}}" value="{{number_format($total,2)}}" disabled="">
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số tiền cần thanh toán (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['total'] == null ? number_format(0,2) : number_format($detail['total'],2)}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tổng thành tiền (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['total_amount'] == null ? number_format(0,2) : number_format($detail['total_amount'],2)}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <input name="customer_id" id="customer_id" value="{{$detail['customer_id']}}" hidden>
            <input name="withdraw_request_id" id="withdraw_request_id" value="{{$detail['withdraw_request_id']}}" hidden>
            <input name="order_id" id="order_id" value="{{$detail['order_id']}}" hidden>
            <input name="total_amount" id="total_amount" value="{{$detail['total_amount']}}" hidden>
            <input name="total" id="total" value="{{$detail['total']}}" hidden>
            <input type="hidden" class="payment_method_id" value="{{$detail['payment_method_id']}}" >
            @include('admin::buy-bonds-request.popup.create-group')
{{--            @include('admin::buy-bonds-request.popup.create-receipt')--}}
            <div id="append-create-receipt"></div>
            <!-- hóa đơn -->
            <div class="m-portlet__head pl-0">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-list"></i>
                    </span>
                        <h2 class="m-portlet__head-text">
                           Danh Sách phiếu thu
                        </h2>

                    </div>
                </div>
            </div>
            <div class="form-group m-form__group bdt_order bdb_order">
                <div class="m-section__content receipt-detail-list">

                </div>
                <input type="hidden" id="page" name="page" value="">
            </div>
            <!-- hóa đơn -->
        </div>
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.buy-bonds-request')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('QUAY LẠI ')}}</span>
                    </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="bill">

    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/buy-bonds-request/script.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/buy-bonds-request/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        new AutoNumeric.multiple('.name', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });
    </script>
    <script type="text/template" id="imageShow">
        <div class="wrap-img image-show-child m-3">
            <input type="hidden" name="img-transfer[]" value="{link_hidden}">
            <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px"
                 height="100px">
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="BuyBondsRequest.remove_img(this)">
                    <i class="la la-close class_remove"></i>
                </a>
            </span>
        </div>
    </script>
    <script type="text/template" id="imageShowCash">
        <div class="wrap-img image-show-child m-3">
            <input type="hidden" name="img-transfer[]" value="{link_hidden}">
            <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px"
                 height="100px">
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="BuyBondsRequest.remove_img(this)">
                    <i class="la la-close class_remove"></i>
                </a>
            </span>
        </div>
    </script>
    <script>
        BuyBondsRequest.getListReceiptDetail(1);
        BuyBondsRequest.dropzone();
    </script>

@stop