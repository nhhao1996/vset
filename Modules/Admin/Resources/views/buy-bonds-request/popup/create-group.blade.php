<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>XÁC NHẬN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        {{__('Số tiền cần thu')}}:<b class="text-danger">*</b>
                    </label>
                    <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }}">
                        <input type="text" id="amount" name="amount" class="form-control m-input name"
                               placeholder="{{__('Nhập số tiền cần thu')}}">
                        <span class="error-name text-danger"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-arrow-left"></i>
                                    <span>HỦY</span>
                                </span>
                        </button>
                        <button type="button" onclick="BuyBondsRequest.add(0)" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>LƯU THÔNG TIN</span>
                                </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
