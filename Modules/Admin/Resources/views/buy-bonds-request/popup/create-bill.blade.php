<div class="modal" id="create-bill" tabindex="-1" role="document" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>TẠO HÓA ĐƠN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="print-bill">
                    <div class="form-group">
                        <label class=" label black-title">
                            Đơn vị thanh toán
                        </label>
                        <div>
                            <input type="text" class="form-control" id="investment_unit" name="investment_unit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Họ tên người nộp tiền
                        </label>
                        <div>
                            <input type="text" class="form-control" id="user_name" name="user_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            CMND/ CCCD
                        </label>
                        <div>
                            <input type="text" class="form-control" id="cmnd" name="cmnd">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Ngày cấp CMND/ CCCD
                        </label>
                        <div>
                            <input type="text" class="form-control" id="created" name="created">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Nơi cấp CMND/ CCCD
                        </label>
                        <div>
                            <input type="text" class="form-control" id="issued_by" name="issued_by">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Địa chỉ thường trú
                        </label>
                        <div>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Số điện thoại liên lạc
                        </label>
                        <div>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Lý do nộp tiền
                        </label>
                        <div>
                            <input type="text" class="form-control" id="reason" name="reason">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Mã đơn hàng
                        </label>
                        <div>
                            <input type="text" class="form-control" id="order_code" name="order_code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Số tiền thanh toán
                        </label>
                        <div>
                            <input type="text" class="form-control number-money" id="payment_amount" name="payment_amount">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Số tiền thanh toán(bằng chữ)
                        </label>
                        <div>
                            <input type="text" class="form-control" id="payment_amount_text" name="payment_amount_text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Họ tên người thu tiền
                        </label>
                        <div>
                            <input type="text" class="form-control" id="name_collector" name="name_collector">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" label black-title">
                            Số điện thoại
                        </label>
                        <div>
                            <input type="text" class="form-control" id="phone_collector" name="phone_collector">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>HỦY</span>
                            </span>
                        </button>
                        <button type="button" onclick="BuyBondsRequest.printBill()" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5 print-bill">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>IN HÓA ĐƠN</span>
                            </span>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
<script>
    $('#created').datepicker({
        format: 'dd-mm-yyyy',
        endDate: new Date(),
        todayHighlight: true,
    });
    new AutoNumeric.multiple('.number-money', {
        currencySymbol: '',
        decimalCharacter: '.',
        digitGroupSeparator: ',',
        decimalPlaces: 2
    });
</script>