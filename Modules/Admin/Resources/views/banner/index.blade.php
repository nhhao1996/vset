@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ POPUP KHUYẾN MÃI')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/
        .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > span , .m-checkbox.ss--m-checkbox--state-success.m-checkbox--solid > input:checked ~ span {
            background: #4fc4ca;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <form class="frmFilter" autocomplete="off">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                             <i class="la la-th-list"></i>
                        </span>
                        <h2 class="m-portlet__head-text">
                            {{__('DANH SÁCH POPUP')}}
                        </h2>

                    </div>
                </div>

                <div class="m-portlet__head-tools nt-class">
{{--                    @if(in_array('admin.banner.add',session('routeList')))--}}
                        <a href="{{route('admin.banner.add')}}"
                           class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                        <span>
						    <i class="fa fa-plus-circle m--margin-right-5"></i>
							<span> {{__('THÊM POPUP')}}</span>
                        </span>
                        </a>
{{--                    @endif--}}
                </div>

            </div>
            <div class="m-portlet__body">
                <div class="frmFilter bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" value="{{isset($FILTER['search']) ? $FILTER['search'] : ''}}"
                                           placeholder="{{__('Nhập tên popup')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control created" name="time" value="{{isset($FILTER['time']) ? $FILTER['time'] : ''}}"
                                           placeholder="{{__('Chọn thời gian')}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 form-group">
                            <select style="width: 100%" name="is_active"
                                    class="form-control m-input ss--select-2">
                                <option value="">Trạng thái</option>
                                <option value="1">Hoạt động</option>
                                <option value="0">Tạm dừng</option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button type="submit" class="btn btn-primary color_button btn-search " style="width: 150px">
                                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                                </button>
                                <a href="{{route('admin.banner')}}"
                                   class="btn btn-metal  btn-search padding9x padding9px">
                                    <span><i class="flaticon-refresh"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible">
                            <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                        </div>
                    @endif
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::banner.list')

                </div><!-- end table-content -->

            </div>
        </form>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@stop
@section('after_script')


    <script src="{{asset('static/backend/js/admin/banner/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
        $('select').select2();
        $('.created').daterangepicker({
            // changeMonth: true,
            // changeYear: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        @if(isset($FILTER['created']))
        $('.created').val('{{$FILTER['created']}}');
        @else
        $('.created').val('');
        @endif
    </script>
@stop