<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list text-center">#</th>
            <th class="tr_thead_list text-center">{{__('Tên popup')}}</th>
            <th class="tr_thead_list text-center">{{__('Thời gian bắt đầu')}}</th>
            <th class="tr_thead_list text-center">{{__('Thời gian kết thúc')}}</th>
            <th class="tr_thead_list text-center">{{__('Trạng thái')}}</th>
            <th class="tr_thead_list text-center">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle text-center">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle text-center">{{$key+1}}</td>
                    @endif
                    <td class="text_middle text-center">{{$item['name']}}</td>
                    <td class="text_middle text-center">{{\Carbon\Carbon::parse($item['from'])->format('d-m-Y')}}</td>
                    <td class="text_middle text-center">{{\Carbon\Carbon::parse($item['to'])->format('d-m-Y')}}</td>
                    <td class="text-center">
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label class="ss--switch">
                                <input type="checkbox" {{$item['is_active'] == 1 ? 'checked' : ''}} disabled="" class="manager-btn" name="">
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td class="text-center">
{{--                        @if(in_array('admin.banner.detail', session('routeList')))--}}
                            <a class ="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" href="{{route("admin.banner.detail",['id' => $item['banner_id'] ])}}" title="Chi tiết">
                                <i class="la la-eye"></i>
                            </a>
{{--                        @endif--}}

{{--                        @if(in_array('admin.banner.edit', session('routeList')))--}}
                        <a class ="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" href="{{route("admin.banner.edit",['id' => $item['banner_id'] ])}}" title="Chỉnh sửa">
                            <i class="la la-edit"></i>
                        </a>
{{--                        @endif--}}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>

    </table>
</div>
{{ $LIST->links('helpers.paging') }}
<style>
    .a-custom {
        padding: 10px;
    }
    .a-custom:hover {
        background: #4fc4ca;
        border-radius: 50px;
        padding: 10px 8px;
        text-decoration: none;
        color: white;
        transition: 0.2s;
    }
    .a-custom .flaticon-eye{
        top: 1px;
        left: 2px;
    }
</style>
