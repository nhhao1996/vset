@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <style>
        .note-editor {
            width: 100%;
        }
    </style>
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ POPUP KHUYẾN MÃI')}}
    </span>
@endsection
@section('content')
    <form id="form-banner" autocomplete="off">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                        <h3 class="m-portlet__head-text">
                            {{__('THÊM POPUP KHUYẾN MÃI')}}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">

                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Tên popup khuyến mãi')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="name" name="name" type="text" class="form-control m-input class"
                                       placeholder="{{__('Tên popup khuyến mãi')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-name"></span>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Thời gian hiển thị')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <input id="display" name="display" type="text" class="form-control m-input class"
                                       placeholder="{{__('Thời gian hiển thị')}}"
                                       aria-describedby="basic-addon1">
                            </div>
                            <span class="errs error-display"></span>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label>
                                {{__('Trạng thái')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label class="ss--switch">
                                        <input type="checkbox" id="is_actived" checked class="manager-btn">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                            <span class="errs error-display"></span>
                        </div>
                    </div>
                    <div class="col-lg-6"></div>

                    {{--                    ------------------------------------------------------------------------------------------------------------------------------------------------------------}}
                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình desktop (VI)')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="img_desktop_hidden_vi" >
                                <input type="hidden" id="img_desktop_upload_vi" name="img_desktop_vi"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="img_desktop_main_vi"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_img_desktop_main_vi" type='file'
                                       onchange="uploadImageDesktop(this,'vi');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_img_desktop_main_vi').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình desktop (EN)')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="img_desktop_hidden_en" >
                                <input type="hidden" id="img_desktop_upload_en" name="img_desktop_en"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="img_desktop_main_en"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_img_desktop_main_en" type='file'
                                       onchange="uploadImageDesktop(this,'en');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_img_desktop_main_en').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình mobile (VI)')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="img_mobile_hidden_vi" >
                                <input type="hidden" id="img_mobile_upload_vi" name="img_mobile_vi"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="img_mobile_main_vi"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_img_mobile_main_vi" type='file'
                                       onchange="uploadImageMobile(this,'vi');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_img_mobile_main_vi').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row form-group">
                            <label  class="col-form-label label col-lg-4 black-title">
                                {{__('Hình mobile (EN)')}}: <b class="text-danger">*</b>
                            </label>
                            <div class="col-lg-8">
                                <input type="hidden" id="img_mobile_hidden_en" >
                                <input type="hidden" id="img_mobile_upload_en" name="img_mobile_en"
                                       value="">
                                <div class="m-widget19__pic">
                                    <img class="m--bg-metal img-sd" id="img_mobile_main_en"
                                         src="{{asset('/static/backend/images/no-image-product.png')}}"
                                         alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                </div>
                                <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                       data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                       id="id_img_mobile_main_en" type='file'
                                       onchange="uploadImageMobile(this,'en');"
                                       class="form-control"
                                       style="display:none"/>

                                <div class="m-widget19__action" style="max-width: 155px">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('id_img_mobile_main_en').click()" style="width: 100%"
                                       class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    {{__('Tải ảnh lên')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer save-attribute m--margin-right-20">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.banner')}}"
                           class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                        </a>
                        <button onclick="banner.addBanner(1)" type="button"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                            <i class="la la-check"></i>
                            <span>{{__('LƯU THÔNG TIN')}}</span>
                            </span>
                        </button>
                        <button type="button" onclick="banner.addBanner(0)"
                                class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                             <i class="fa fa-plus-circle"></i>
                            <span> {{__('LƯU & TẠO MỚI')}}</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/banner/script.js?v='.time())}}"></script>

    <script>
        jQuery(document).ready(function () {
            $('.note-btn').attr('title', '');
            $('#display').daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY',
                },
                minDate : moment(),
                // rtl: KTUtil.isRTL(),
                // todayBtn: "linked",
                // clearBtn: true,
                todayHighlight: true,
                // templates: arrows
            });
        });
    </script>

    <script>
        dropzone();
    </script>
    <script type="text/template" id="imageShow">
        <div class="image-show-child mb-0 col-12">
            <input type="hidden" name="arrImageBanner[desktop][{numberImage}][image]" value="{link_hidden}">
            <p>{{asset('{link}')}}</p>
            <span class="delete-img-sv" style="display: block;">
                    <a href="javascript:void(0)" onclick="removeImage(this)">
                        <i class="la la-close"></i>
                    </a>
                </span>
        </div>
    </script>

    <script type="text/template" id="imageShowMobile">
        <div class="image-show-child-mobile col-12">
            <input type="hidden" name="arrImageBanner[mobile][{numberImageMobile}][image]" value="{link_hidden}">
            <p>{{asset('{link}')}}</p>

            <span class="delete-img-sv" style="display: block;">
                        <a href="javascript:void(0)" onclick="removeImageMobile(this)">
                            <i class="la la-close"></i>
                        </a>
                    </span>
        </div>
    </script>
@stop
