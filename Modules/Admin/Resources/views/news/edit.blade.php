@extends('layout')
@section('title_header')
    <span class="title_header">QUẢN LÝ BÀI VIẾT</span>
@stop
@section('content')
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                         <i class="la la-edit"></i>
                     </span>
                    <h2 class="m-portlet__head-text">
                        CHỈNH SỬA BÀI VIẾT
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <form id="form-edit">
            {!! csrf_field() !!}
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Tên đề VI:<b class="text-danger">*</b>
                            </label>
                            <input type="text" class="form-control m-input" name="title_vi" id="title_vi"
                                   placeholder="Nhập tiêu đề VI..." value="{{$item['title_vi']}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Tên đề EN:<b class="text-danger">*</b>
                            </label>
                            <input type="text" class="form-control m-input" name="title_en" id="title_en"
                                   placeholder="Nhập tiêu đề EN..." value="{{$item['title_en']}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Nội dung VI:<b class="text-danger">*</b>
                            </label>
                            <textarea class="form-control m-input" name="description_vi" id="description_vi"
                                      placeholder="Nhập nội dung VI..." rows="5">{{$item['description_vi']}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Nội dung EN:<b class="text-danger">*</b>
                            </label>
                            <textarea class="form-control m-input" name="description_en" id="description_en"
                                      placeholder="Nhập nội dung EN..." rows="5">{{$item['description_en']}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Sản phẩm liên quan:
                            </label>
                            <select class="form-control" id="product" name="product" style="width:100%;" multiple>
                                <option value="0" {{$item['product'] == 0 ? 'selected' : ''}}>Tất cả</option>
                                @foreach($optionProduct as $v)
                                    @if(in_array($v['product_child_id'], str_split($item['product'])))
                                        <option value="{{$v['product_child_id']}}"
                                                selected>{{$v['product_child_name']}}</option>
                                    @else
                                        <option value="{{$v['product_child_id']}}">{{$v['product_child_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Dịch vụ liên quan:
                            </label>
                            <select class="form-control" id="service" name="service" style="width:100%;" multiple>
                                <option value="0" {{$item['service'] == 0 ? 'selected' : ''}}>Tất cả</option>
                                @foreach($optionService as $v)
                                    @if(in_array($v['service_id'], str_split($item['service'])))
                                        <option value="{{$v['service_id']}}" selected>{{$v['service_name']}}</option>
                                    @else
                                        <option value="{{$v['service_id']}}">{{$v['service_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>
                                Nội dung chi tiết VI:
                            </label>
                            <textarea name="description_detail_vi" id="description_detail_vi"
                                      class="form-control summernote">{{$item['description_detail_vi']}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>
                                Nội dung chi tiết EN:
                            </label>
                            <textarea name="description_detail_en" id="description_detail_en"
                                      class="form-control summernote">{{$item['description_detail_en']}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <div class="row">
                                <div class="col-lg-3  w-col-mb-100">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('getFile').click()"
                                       class="btn  btn-sm m-btn--icon color">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>
                                                    Thêm ảnh bài viết
                                                </span>
                                            </span>
                                    </a>
                                </div>
                                <div class="col-lg-9  w-col-mb-100 div_avatar">
                                    <input type="hidden" id="image" name="image" value="">
                                    <input type="hidden" id="image_old" name="image_old" value="{{$item['image']}}">
                                    <div class="wrap-img avatar float-left">
                                        @if($item['image'] !=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah"
                                                 src="{{$item['image']}}"
                                                 alt="{{__('Hình ảnh')}}" width="100px" height="100px">
                                            <span class="delete-img" style="display: block">
                                                    <a href="javascript:void(0)" onclick="create.remove_avatar()">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                </span>
                                        @else
                                            <img class="m--bg-metal m-image img-sd" id="blah"
                                                 src="{{asset('uploads/admin/service_card/default/hinhanh-default3.png')}}"
                                                 alt="{{__('Hình ảnh')}}" width="100px" height="100px">
                                            <span class="delete-img">
                                                    <a href="javascript:void(0)" onclick="create.remove_avatar()">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                </span>
                                        @endif
                                    </div>
                                    <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                           data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                           id="getFile" type="file"
                                           onchange="uploadImage(this);" class="form-control"
                                           style="display:none">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>{{__('Trạng thái')}}</label>
                            <div class="input-group row">
                                <div class="col-lg-1">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input type="checkbox" id="is_actived" name="is_actived" {{$item['is_actived'] == 1 ? 'checked' : ''}}>
                                        <span></span>
                                    </label>
                                </span>
                                </div>
                                <div class="col-lg-6 m--margin-top-5">
                                    <i>{{__('Select to activate status')}}</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.new')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                            <span>
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                            </span>
                        </a>

                        <button type="button" onclick="edit.save({{$item['new_id']}})"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span>
                                <i class="la la-check"></i>
                                <span>{{__('LƯU THÔNG TIN')}}</span>
                        </span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script type="text/template" id="avatar-tpl">
        <img class="m--bg-metal m-image img-sd" id="blah"
             src="{{asset('uploads/admin/service_card/default/hinhanh-default3.png')}}"
             alt="{{__('Hình ảnh')}}" width="100px" height="100px">
        <span class="delete-img"><a href="javascript:void(0)" onclick="Voucher.remove_avatar()">
            <i class="la la-close"></i></a>
        </span>
        <input type="hidden" id="voucher_img" name="voucher_img">
    </script>
    <script type="text/template" id="imgShow">
        <div class="wrap-img image-show-child">
            <input type="hidden" name="voucher_img" value="{link_hidden}">
            <img class='m--bg-metal m-image img-sd '
                 src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px" height="100px">
            <span class="delete-img-sv" style="display: block;">
                                                    <a href="javascript:void(0)" onclick="service.remove_img(this)">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                </span>
        </div>
    </script>
    <script src="{{asset('static/backend/js/admin/news/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        edit._init();
    </script>
@stop


