<div class="modal fade" id="modalMakeReceipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>TẠO PHIẾU THU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="receipt-detail-add">
                    <div class="form-group">
                        <label>
                            {{__('Số tiền cần thu')}}:<b class="text-danger">*</b>
                        </label>
                        <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }}">
{{--                            <input type="text" value="{{number_format($moneyReceipt['amount'] - $sumMoneyReceipt,2)}}" class="form-control m-input "--}}
                            <input type="text" value="{{number_format($moneyReceipt['amount'],2)}}" class="form-control m-input "
                                   placeholder="{{__('Nhập số tiền cần thu')}}" disabled>
                            <input type="hidden" value="{{number_format($moneyReceipt['amount'],2)}}" class="form-control m-input "
                                   placeholder="{{__('Nhập số tiền cần thu')}}" >
                            <span class="error-name text-danger"></span>
                        </div>
                    </div>
                    <input type="hidden" name="receipt_id" value="{{$moneyReceipt['receipt_id']}}">
                    <input type="hidden" name="order_id" value="{{$order_id}}">
                    <!-- hinh thuc thu tien -->
                    <div class="form-group">
                            <label for="payment_method_type">Chọn hình thức thu tiền: <b class="text-danger">*</b></label>
                            <select class="form-control" id="payment_method_type_select_box">
{{--                                <option value="">Chọn hình thức thu tiền</option>--}}
                                <option value="cash" selected>Tiền mặt</option>
                                <option value="transfer">Chuyển khoản</option>
                                <option value="interest">Ví Lãi</option>
                                <option value="bonus">Ví thưởng</option>
                            </select>
                        </div>
                    <!-- hinh thuc thu tien -->
                    <!-- tien mat -->
                    <div class="form-group noselector cash">
                        <label>
                            {{__('Số tiền thanh toán bằng tiền mặt (Vnđ)')}} :<b class="text-danger">*</b>
                        </label>
                        <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }}">
{{--                            <input type="text" id="amount_receipts" name="amount" value="{{number_format($moneyReceipt['amount'] - $sumMoneyReceipt,2)}}" class="form-control m-input name"--}}
                            <input type="text" value="{{number_format($moneyReceipt['amount'],2)}}" class="form-control " disabled
                                   placeholder="{{__('Nhập số tiền thanh toán')}}">
                            <input type="hidden" id="amount_receipts" name="amount" value="{{number_format($moneyReceipt['amount'],2)}}" class="form-control m-input name"
                                   placeholder="{{__('Nhập số tiền thanh toán')}}">
                            <span class="error-name text-danger"></span>
                        </div>
{{--                        <div class="form-group m-form__group p-3 pt-3 mt-3 border">--}}
{{--                            <label>{{__('Thông tin biên lai')}}:</label>--}}
{{--                            <input type="text" class="form-control form-group user_send" placeholder="{{__('Họ tên người gửi')}}">--}}
{{--                            <input type="text" class="form-control form-group content_send" placeholder="{{__('Nội dung gửi')}}">--}}
{{--                        </div>--}}
                        <div class="form-group m-form__group pt-3">
                            <label>{{__('Ảnh biên lai')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('admin.extend-contract.upload-dropzone')}}" id="dropzoneonecash">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('Hình ảnh')}}
                                    </h3>
                                    <span>{{__('Chọn hình biên lai')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image-cash"></div>
                    </div>
                    <!-- !tien mat -->
                    <!-- dropzone -->
                    <div class="form-group noselector transfer">
                        <div class="form-group">
                            <label>
                                {{__('Số tiền thanh toán chuyển khoản  (Vnđ)')}}:<b class="text-danger">*</b>
                            </label>
                            <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }}">
                                <input type="text" id="amount_receipts" name="amount_transfer" class="form-control m-input name" value="{{number_format($moneyReceipt['amount'] - $sumMoneyReceipt,2)}}"
                                       placeholder="{{__('Nhập số tiền thanh toán')}}">
                                <span class="error-name text-danger"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group ">
                            <label>{{__('Ảnh biên lai')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('admin.extend-contract.upload-dropzone')}}" id="dropzoneone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('Hình ảnh')}}
                                    </h3>
                                    <span>{{__('Chọn hình biên lai')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image"></div>
                    </div>
                    <!-- dropzone -->
                    <!-- ví lãi -->
                    <div class="form-group noselector interest">
                        <label>
                            {{__('Số tiền thanh toán bằng ví lãi')}}:<b class="text-danger">*</b>
                        </label>
                        @if(isset($listWithrawRequestGroup) && count($listWithrawRequestGroup) != 0)
                            <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }} scroll-withdraw-request">
                                @foreach($listWithrawRequestGroup as $item)
                                    @if($item['withdraw_request_group_type'] == 'interest')
                                        <div class="col-12">
                                            <label class="kt-radio kt-radio--bold kt-radio--success">
                                                <input type="radio" name="withdraw_request_group_id_interest" checked value="{{$item['withdraw_request_group_id']}}">
                                                {{$item['withdraw_request_group_code']}}
                                                @if(isset($listWithrawRequest[$item['withdraw_request_group_id']]))
                                                    <ul>
                                                    @foreach($listWithrawRequest[$item['withdraw_request_group_id']] as $itemDetail)
                                                        <li>{{number_format($itemDetail['available_balance'])}} vnđ</li>
                                                    @endforeach
                                                    </ul>
                                                @endif
                                                <span></span>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        <span class="error-name text-danger-interest"></span>
                    </div>
                    <!-- ví lãi -->
                    <!-- Ví thưởng -->
                    <div class="form-group noselector bonus">
                        <label>
                            {{__('Số tiền thanh toán bằng ví thưởng')}}:<b class="text-danger">*</b>
                        </label>
                        @if(isset($listWithrawRequestGroup) && count($listWithrawRequestGroup) != 0)
                            <div class="{{ $errors->has('order_source_name') ? ' has-danger' : '' }} scroll-withdraw-request">
                                @foreach($listWithrawRequestGroup as $item)
                                    @if($item['withdraw_request_group_type'] == 'bonus')
                                        <div class="col-12">
                                            <label class="kt-radio kt-radio--bold kt-radio--success">
                                                <input type="radio" name="withdraw_request_group_id_bonus" checked value="{{$item['withdraw_request_group_id']}}"> {{$item['withdraw_request_group_code']}}
                                                <ul>
                                                    <li>{{number_format($item['available_balance'])}} vnđ</li>
                                                </ul>
                                                <span></span>
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                                <span class="error-name text-danger"></span>
                            </div>
                        @endif
                    </div>
                    <!-- !Ví thưởng -->
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>HỦY</span>
                            </span>
                        </button>
                        <button type="button" onclick="BuyBondsRequest.createBill()" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5 print-bill">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>TẠO HÓA ĐƠN</span>
                            </span>
                        </button>

                        <button type="button" onclick="BuyBondsRequest.addReceiptDetail()" class="ss--btn-mobiles btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 m--margin-bottom-5">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>LƯU THÔNG TIN</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>