@extends('layout') @section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection @section('title_header')
    <span class="title_header">
    <img src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;" />
    {{__('QUẢN LÝ YÊU CẦU GIA HẠN')}}
</span>
@endsection @section('content')
    <meta http-equiv="refresh" content="number" />
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="la la-th-list"></i>
                </span>
                    <h3 class="m-portlet__head-text">
                        {{__('DANH SÁCH YÊU CẦU GIA HẠN')}}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="{{__('Nhập tên, mã, sdt khách hàng')}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="contract_code_extend" placeholder="{{__('Nhập mã hợp đồng gia hạn')}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%;" name="process_status" class="form-control m-input ss--select-2">
                            <option value="">Chọn trạng thái</option>
                            <option value="new">Mới</option>
                            <option value="confirmed">Đã xác minh</option>
                            <option value="paysuccess">Thanh toán thành công</option>
{{--                            <option value="pay-half">Thanh toán 1 phần</option>--}}
{{--                            <option value="ordercomplete">Đơn hàng hoàn thành</option>--}}
{{--                            <option value="ordercancle">Đơn hàng bị hủy</option>--}}
{{--                            <option value="payfail">Thanh toán thất bại</option>--}}
{{--                            <option value="not_call">Không Kết nối</option>--}}
                        </select>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%;" name="type_bonds" class="form-control m-input ss--select-2">
                            <option value="">Loại yêu cầu</option>
                            <option value="2">Tiết Kiệm</option>
                            <option value="1">Hợp tác đầu Tư</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group" style="background-color: white;">
                            <div class="m-input-icon m-input-icon--right">
                                <input readonly="" class="form-control m-input daterange-picker" id="created_at" name="created_at" autocomplete="off" placeholder="{{__('Ngày khởi tạo')}}" />
                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                <span><i class="la la-calendar"></i></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="form-group m-form__group text-right">
                            <button class="btn btn-primary color_button btn-search">{{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i></button>
                            <a href="{{route('admin.extend-contract')}}" class="btn btn-metal btn-search padding9x">
                            <span>
                                <i class="flaticon-refresh"></i>
                            </span>
                            </a>
                        </div>
                    </div>
                </div>

                @if (session('status'))
                    <div class="alert alert-success alert-dismissible"><strong>{{__('Success')}} : </strong> {!! session('status') !!}.</div>
                @endif
            </form>
            <div class="table-content m--padding-top-30">
                @include('admin::extend-contract.list')
            </div>
            <!-- end table-content -->
        </div>
    </div>
@endsection @section('after_script')
    <script src="{{asset('static/backend/js/admin/extend-contract/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
@stop
