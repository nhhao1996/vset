<div class="modal fade" role="dialog" id="modal-receipt" style="display: none">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_index">
                    {{__('THANH TOÁN CÔNG NỢ')}}
                </h4>
            </div>
            <div class="m-widget4  m-section__content" id="load">
                <form id="form-receipt">
                    <div class="modal-body">
                        <div class="form-group m-form__group type_modal">
                            <label>{{__('Hình thức thanh toán')}}:<span style="color:red;font-weight:400">*</span></label>
                            <select class="form-control" id="receipt_type" name="receipt_type[]" multiple="multiple"
                                    style="width: 100%">
                                <option value="cash" selected>{{__('Tiền mặt')}}</option>
                                <option value="transfer">{{__('Chuyển khoản')}}</option>
                                <option value="visa">{{__('Visa')}}</option>
                                @if(isset($branchMoney))
                                    @if($branchMoney['balance'] > 0)
                                        <option value="member_money">{{__('Tài khoản thành viên')}}</option>
                                    @endif
                                @endif
                            </select>
                            <span class="error_type" style="color:#ff0000"></span>
                        </div>
                        @if(isset($branchMoney))
                            @if($branchMoney['balance'] > 0)
                                <input type="hidden" id="member_money" name="member_money" value="{{$branchMoney['balance']}}">
                            @endif
                        @endif
                        <div class="form-group m-form__group cash">
                            <div class="row">
                                <label class="col-lg-6 font-13">{{__('Tiền mặt')}}:<span
                                            style="color:red;font-weight:400">*</span></label>
                                <div class="input-group input-group-sm  col-lg-6" style="height: 30px;">
                                    <input autofocus onkeyup="index.changeAmountReceipt(this)" style="color: #008000" class="form-control m-input amount"
                                           placeholder="{{__('Nhập giá tiền')}}" aria-describedby="basic-addon1"
                                           name="amount_receipt_cash" id="amount_receipt_cash" maxlength="11"
                                           value="{{($itemReceipt['amount'] - $itemReceipt['amount_paid'])}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon1">{{__('VNĐ')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group transfer">

                        </div>
                        <div class="form-group m-form__group visa">

                        </div>
                        <div class="form-group m-form__group member_money">

                        </div>
                        <div class="form-group m-form__group">
                            <span class="error_receipt" style="color:#ff0000"></span>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 w-me-40  font-13">{{__('Tiền phải thanh toán')}}:</label>
                            <div class="col-lg-9 w-me-40 ">
                                <span class="font-13 font-weight-bold cl_receipt_amount"
                                      style="float: right;color: red">{{number_format($itemReceipt['amount'] - $itemReceipt['amount_paid'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}</span>
                                <input type="hidden" class="form-control m--font-bolder" disabled="disabled"
                                       name="receipt_amount" id="receipt_amount"
                                       value="{{($itemReceipt['amount'] - $itemReceipt['amount_paid'])}}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 w-me-40 font-13">{{__('Tổng tiền trả')}}:</label>
                            <div class="col-lg-9 w-me-40">
                                <span class="font-13 font-weight-bold cl_amount_all"
                                      style="float: right;">{{number_format($itemReceipt['amount'] - $itemReceipt['amount_paid'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}</span>
                                <input type="hidden" class="form-control m--font-info" disabled="disabled"
                                       name="amount_all" id="amount_all"
                                       value="{{($itemReceipt['amount'] - $itemReceipt['amount_paid'])}}">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 w-me-40 font-13">{{__('Còn nợ')}}:</label>
                            <div class="col-lg-9 w-me-40">
                                <span class="font-13 font-weight-bold cl_amount_rest" style="float: right;">0</span>
                                <input type="hidden" class="form-control  m--font-danger" disabled="disabled"
                                       name="amount_rest" id="amount_rest" value="0">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 w-me-40 font-13">{{__('Trả lại khách')}}:</label>
                            <div class="col-lg-9 w-me-40">
                                <span class="font-13 font-weight-bold cl_amount_return" style="float: right;">0</span>
                                <input type="hidden" class="form-control m--font-info" disabled="disabled"
                                       name="amount_return" id="amount_return" value="0">
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label>{{__('Ghi chú')}}</label>
                            <textarea class="form-control" id="note" name="note" cols="5" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button"
                                class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md ss--btn"
                                data-dismiss="modal">
                    <span>
                        <i class="la la-arrow-left"></i><span>{{__('HỦY')}}</span>
                    </span>
                        </button>
                        <button type="button" onclick="index.submit_receipt_bill('{{$itemReceipt['customer_debt_id']}}')"
                                class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10"><span>{{__('THANH TOÁN & IN HÓA ĐƠN')}}</span></button>
                        <button type="button" onclick="index.submit_receipt('{{$itemReceipt['customer_debt_id']}}')"
                                class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10"><span>{{__('THANH TOÁN')}}</span></button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
