<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Cấp độ (VI)')}}</th>
            <th class="tr_thead_list">{{__('Cấp độ (EN)')}}</th>
            <th class="tr_thead_list text-center">{{__('Số điểm quy đổi')}}</th>
{{--            <th class="tr_thead_list text-center">{{__('Giảm giá')}} ( % )</th>--}}
            <th class="tr_thead_list text-center">{{__('Trạng thái')}}</th>
            <th></th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr>
                    @if(isset($page))
                        <td>{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td>{{$key+1}}</td>
                    @endif
                    <td>{{ $item['name'] }}</td>
                    <td>{{ $item['name_en'] }}</td>
                    <td class="text-center">{{ $item['point'] }}</td>
{{--                    <td class="text-center">{{ $item['discount'] }}</td>--}}
                    <td class="text-center">
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox"
                                       disabled
                                       {{$item['is_actived'] == 1 ? 'checked' : ''}}
                                       class="manager-btn" name="">
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td>
{{--                        @if(in_array('admin.member-level.submitedit',session('routeList')))--}}
                            <a href="{{route('admin.member-level.show',['id' => $item['member_level_id']])}}" title="Chi tiết"
                               class="test m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-eye"></i>
                            </a>
                            <a href="{{route('admin.member-level.edit',['id' => $item['member_level_id']])}}" title="Chỉnh sửa"
                                    class="test m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-edit"></i>
                            </a>
{{--                        @endif--}}
{{--                        @if(in_array('admin.member-level.remove',session('routeList')))--}}
                             <button type="button" onclick="member_level.remove(this, {{$item['member_level_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="Xóa">
                                <i class="la la-trash"></i>
                            </button>
{{--                        @endif--}}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

{{ $LIST->links('helpers.paging') }}



