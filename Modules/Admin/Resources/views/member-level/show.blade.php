@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ CẤP ĐỘ")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"
                                 style="font-size: 13px"></i> @lang("CHI TIẾT CẤP ĐỘ")</span>
                    </h2>
                </div>
            </div>
        </div>
        <style>
            .nt-custome-row  .form-group{
                margin-right: 0;
                margin-left: 0;
            }
        </style>
        <form id="form">
            <div class="m-portlet__body">
                <div class="row nt-custome-row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Tên cấp độ thành viên (VI)')}}:<b class="text-danger">*</b>
                                    </label>
                                    <input placeholder="{{__('Nhập tên cấp độ (VI)')}}" type="text" name="name" value="{{$detail['name']}}" class="form-control btn-sm m-input"
                                           id="name">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Tên cấp độ thành viên (EN)')}}:<b class="text-danger">*</b>
                                    </label>
                                    <input placeholder="{{__('Nhập tên cấp độ (EN)')}}" type="text" name="name_en" value="{{$detail['name_en']}}" class="form-control btn-sm m-input"
                                           id="name_en">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Tiêu đề (VI)')}}:<b class="text-danger">*</b>
                                    </label>
                                    <input placeholder="{{__('Nhập tiêu đề (VI)')}}" type="text" name="range_content" value="{{$detail['range_content']}}" class="form-control btn-sm m-input"
                                           id="range_content">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Tiêu đề (EN)')}}:<b class="text-danger">*</b>
                                    </label>
                                    <input placeholder="{{__('Nhập tiêu đề (EN)')}}" type="text" name="range_content_en" value="{{$detail['range_content_en']}}" class="form-control btn-sm m-input"
                                           id="range_content_en">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Điểm quy đổi cấp độ')}}:<b class="text-danger">*</b>
                                    </label>
                                    <input placeholder="{{__('Nhập điểm quy đổi')}}" onkeydown="onKeyDownInput(this)" type="text" value="{{$detail['point']}}" name="point"
                                           class="form-control m-input btn-sm" id="point">
                                </div>
                            </div>
{{--                            <div class="col-lg-12">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label>--}}
{{--                                        {{__('Giảm')}}:<b class="text-danger">*</b> (%)--}}
{{--                                    </label>--}}
{{--                                    --}}{{--{!! Form::text('branch_name',null,['class' => 'form-control m-input','id'=>'name']) !!}--}}
{{--                                    <input type="text" name="discount" class="form-control m-input btn-sm" id="discount" value="{{$detail['discount']}}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-lg-12">
                                <div class="row form-group">
                                    <label class="d-block">
                                        {{__('Trạng thái')}}:
                                    </label>
                                    <div class="col-12">
                                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label>
                                            <input id="is_actived" {{$detail['is_actived'] == 1 ? 'checked' :''}} name="is_actived" type="checkbox">
                                            <span></span>
                                        </label>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row form-group">
                                    <label>
                                        {{__('Hình đại diện gói')}}:
                                    </label>
                                    <div class="col-lg-12">
                                        <input type="hidden" id="member_image_hidden" name="member_image_hidden" value="{{$detail['member_image']}}">
                                        <input type="hidden" id="member_image" name="member_image"
                                               value="">
                                        <div class="m-widget19__pic" style="width:155px">
                                            @if($detail['member_image'] == null)
                                                <img class="m--bg-metal img-sd" id="image_main"
                                                     src="{{asset('/static/backend/images/no-image-product.png')}}"
                                                     alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>
                                            @else
                                                <img class="m--bg-metal img-sd" id="image_main"
                                                     src="{{$detail['member_image']}}"
                                                     alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>
                                            @endif

                                        </div>
                                        <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                               data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                               id="id_image_main" type='file'
                                               onchange="uploadImageMain(this);"
                                               class="form-control"
                                               style="display:none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Nội dung (VI)')}}:
                                    </label>
                                    <textarea type="text" name="description" class="form-control m-input btn-sm" id="description">{!! $detail['description'] !!}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        {{__('Nội dung (EN)')}}:
                                    </label>
                                    <textarea type="text" name="description_en" class="form-control m-input btn-sm" id="description_en">{!! $detail['description_en'] !!}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="member_level_id" value="{{$detail['member_level_id']}}">
        </form>

        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.member-level')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
						<span>
						<i class="la la-arrow-left"></i>
						<span>{{__('HUỶ')}}</span>
						</span>
                    </a>

                    <a href="{{route('admin.member-level.edit',['id' => $detail['member_level_id']])}}"
                        class="btn btn-primary color_button m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                        <span>
                        <i class="la la-edit"></i>
                        <span>{{__('Sửa')}}</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>


@stop
@section("after_style")
    {{--    <link rel="stylesheet" href="{{asset('static/backend/css/process.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">

@stop
@section('after_script')
    <script>
        $('#description').summernote({
            height: 150,
            placeholder: 'Nhập nội dung...',
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]

        });
        $('input').prop('disabled',true);
        $('#description_en').summernote({
            height: 150,
            placeholder: 'Nhập nội dung...',
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]

        });
        $('#description').summernote('disable');
        $('#description_en').summernote('disable');
    </script>
    <script src="{{asset('static/backend/js/admin/member_level/script.js?v='.time())}}" type="text/javascript"></script>
@stop