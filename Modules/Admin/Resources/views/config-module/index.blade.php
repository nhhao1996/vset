@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("QUẢN LÝ HIỂN THỊ MODULE")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }
        .m-datatable__head th {
            background-color: #dff7f8 !important;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h2 class="m-portlet__head-text title_index">
                        <span><i class="la la-server"
                                 style="font-size: 13px"></i> @lang("CẤU HÌNH HIỂN THỊ MODULE")</span>
                    </h2>
                </div>
            </div>
        </div>
        <style>
            .nt-custome-row  .form-group, .row{
                margin-right: 0;
                margin-left: 0;
            }

        </style>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tên chức năng</th>
                                <th>key</th>
                                <th>route site</th>
                                <th>Ẩn</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list as $item)
                                <tr>
                                    <td>{{$item['name']}}</td>
                                    <td>{{$item['key']}}</td>
                                    <td>{{$item['route_site']}}</td>
                                    <td>
                                        <label class="m-checkbox m-checkbox--air m-checkbox--solid ss--m-checkbox--state-success mr-3" onclick="changActiveModule('{{$item['config_module_id']}}')">
                                            <input type="checkbox" id="module_{{$item['config_module_id']}}" name="is_active" {{$item['is_active'] == 1 ? 'checked' : ''}}>
                                            <span class="color_button"></span>
                                        </label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@stop
@section("after_style")
    {{--    <link rel="stylesheet" href="{{asset('static/backend/css/process.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">

@stop
@section('after_script')
    <script>
        function changActiveModule(id) {
            var is_actived = 0 ;
            if ($('#module_'+id).is(':checked')) {
                is_actived = 1;
            }

            $.ajax({
                url: laroute.route('admin.config-module.change-active'),
                method: "POST",
                dataType: "JSON",
                data: {
                    config_module_id : id,
                    is_active : is_actived
                },
                success: function (data) {
                    swal('', data.message, "success");
                },
            });
        }
    </script>
@stop