@foreach($listService as $key => $item)
    <tr id="tr_{{$item['service_id']}}">
        <td>{{$item['service_name_vi']}}</td>
        {{--                                        <td>{{$item['price_standard']}}</td>--}}
        <td>
            @if(isset($type))
                <button type="button" onclick="linkSource.removeService(this,{{$item['service_id']}},'{{$type}}')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xoá">
                    <i class="la la-trash"></i>
                </button>
            @else
                <button type="button" onclick="linkSource.removeService(this,{{$item['service_id']}})" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xoá">
                    <i class="la la-trash"></i>
                </button>
            @endif
        </td>
    </tr>
@endforeach