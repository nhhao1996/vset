<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">#</th>
            <th class="tr_thead_list">{{__('Nguồn')}}</th>
{{--            <th class="tr_thead_list">{{__('Mã nguồn')}}</th>--}}
            <th class="tr_thead_list">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($list))
            @foreach ($list as $key => $item)
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td>{{$item['source_name']}}</td>
{{--                    <td>{{$item['source']}}</td>--}}
                    <td>
{{--                        @if(in_array('admin.refer-source.edit', session('routeList')))--}}
                            <a href="{{route('admin.refer-source.edit',['id'=>$item['refer_source_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chỉnh sửa')}}">
                                <i class="la la-edit"></i>
                            </a>
{{--                        @endif--}}
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>

    </table>
</div>

