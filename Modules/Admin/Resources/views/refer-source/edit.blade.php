@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ CẤU HÌNH TẶNG THƯỞNG THEO NGUỒN')}}</span>
@stop
@section('content')

    <style>
        /*.modal-backdrop {*/
        /*position: relative !important;*/
        /*}*/

        .form-control-feedback {
            color: red;
        }

    </style>
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CẤU HÌNH TẶNG THƯỞNG CHO NGƯỜI GIỚI THIỆU QUA ')}} {{isset($source) ? $source['source_name'] : ''}}
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools nt-class">
                <button type="button" class="btn btn-primary color_button btn-search" onclick="linkSource.saveConfig('refer')">
                    Lưu
                </button>
            </div>
        </div>
        <div class="m-portlet__body">
            <form class="form-refer">
                <div class="row">
                    <div class="form-group col-lg-12">
                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                        <label>Trạng thái</label>
                        <div class="row">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox" class="manager-btn is_active_refer"  {{isset($config['refer']) && $config['refer']['is_active'] == 1 ? 'checked' :''}} >
                                <span></span>
                            </label>
                        </div>
                    </span>
                    </div>
                    <div class="form-group col-lg-12">
                        <label>Thời gian thưởng</label>
                        <div class="row">
                            <div class="col-3">
                                <input type="text" class="form-control daterange-picker" name="date_action_refer" placeholder="Chọn thời gian phát thưởng" value="{{isset($config['refer']) && $config['refer']['start'] != null ? \Carbon\Carbon::parse($config['refer']['start'])->format('d/m/Y').' - '.\Carbon\Carbon::parse($config['refer']['end'])->format('d/m/Y') : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Loại phát thưởng</label>
                        <div class="m-radio-inline type_bonus">
                            <label class="m-radio d-block">
                                <input type="radio" name="type_bonus_refer" value="money" {{isset($config['refer']) && $config['refer']['type_bonus'] == 'money' ? 'checked' : ''}}> Tiền (VNĐ)
                                <span></span>
                            </label>

                            <label class="m-radio d-block">
                                <input type="radio" name="type_bonus_refer" value="voucher" {{isset($config['refer']) && $config['refer']['type_bonus'] == 'voucher' ? 'checked' : ''}}> Voucher
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-lg-9 mt-3">
                        <input type="text" class="form-control money_refer number-money " name="money_refer" value="{{isset($config['refer']) && $config['refer']['money'] != null ? number_format($config['refer']['money'], 2 , '.',',' ): 0}}" placeholder="Nhập số tiền"  {{isset($config['refer']) && $config['refer']['type_bonus'] == 'money' ? '' : 'disabled'}}>
                    </div>
                    <div class="form-group col-lg-12">
                        <button type="button" class="btn btn-primary color_button btn-search add_service_refer voucher_refer" {{isset($config['refer']) && $config['refer']['type_bonus'] == 'voucher' ? '' : 'disabled'}} onclick="linkSource.showPopup('refer')">
                            Thêm dịch vụ
                        </button>
                        <div class="table-content m--padding-top-30">
                            <div class="table-responsive">
                                <table class="table table-striped m-table ss--header-table table-refer">
                                    <thead class="bg">
                                    <tr class="ss--nowrap">
                                        <th class="ss--font-size-th">{{__('Tên dịch vụ')}}</th>
                                        {{--                                        <th class="ss--font-size-th">{{__('Số tiền (Vnđ)')}}</th>--}}
                                        <th class="ss--font-size-th">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody class="body_tr_refer">
                                    @if(isset($listService['refer']) && $listService['refer'])
                                        @foreach($listService['refer'] as $key => $item)
                                            <tr id="tr_{{$item['service_id']}}">
                                                <td>{{$item['service_name_vi']}}</td>
                                                {{--                                        <td>{{$item['price_standard']}}</td>--}}
                                                <td>
                                                    <button type="button" onclick="linkSource.removeService(this,{{$item['service_id']}},'refer')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xoá">
                                                        <i class="la la-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CẤU HÌNH TẶNG THƯỞNG CHO NGƯỜI ĐĂNG KÝ QUA')}} {{isset($source) ? $source['source_name'] : ''}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools nt-class">
                <button type="button" class="btn btn-primary color_button btn-search" onclick="linkSource.saveConfig('register')">
                    Lưu
                </button>
            </div>
        </div>
        <div class="m-portlet__body">

            <form class="form-register">
                <div class="row">
                    <div class="form-group col-lg-12">
                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                        <label>Trạng thái</label>
                        <div class="row">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox" class="manager-btn is_active_register" {{isset($config['register']) && $config['register']['is_active'] == 1 ? 'checked' :''}} >
                                <span></span>
                            </label>
                        </div>
                    </span>
                    </div>
                    <div class="form-group col-lg-12">
                        <label>Thời gian thưởng</label>
                        <div class="row">
                            <div class="col-3">
                                <input type="text" class="form-control daterange-picker" name="date_action_register" placeholder="Chọn thời gian phát thưởng" value="{{isset($config['register']) && $config['register']['start'] != null ? \Carbon\Carbon::parse($config['register']['start'])->format('d/m/Y').' - '.\Carbon\Carbon::parse($config['register']['end'])->format('d/m/Y') : ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Loại phát thưởng</label>
                        <div class="m-radio-inline type_bonus">
                            <label class="m-radio d-block">
                                <input type="radio" name="type_bonus_register" value="money" {{isset($config['register']) && $config['register']['type_bonus'] == 'money' ? 'checked' : ''}}> Tiền (VNĐ)
                                <span></span>
                            </label>

                            <label class="m-radio d-block">
                                <input type="radio" name="type_bonus_register" value="voucher" {{isset($config['register']) && $config['register']['type_bonus'] == 'voucher' ? 'checked' : ''}}> Voucher
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-lg-9 mt-3">
                        <input type="text" class="form-control money_register number-money" name="money_register" value="{{isset($config['register']) && $config['register']['money'] != null ? number_format($config['register']['money'], 2 , '.',',' ): 0}}" placeholder="Nhập số tiền" {{isset($config['register']) && $config['register']['type_bonus'] == 'money' ? '' : 'disabled'}}>
                    </div>
                    <div class="form-group col-lg-12">
                        <button type="button" class="btn btn-primary color_button btn-search add_service_register voucher_register" {{isset($config['register']) && $config['register']['type_bonus'] == 'voucher' ? '' : 'disabled'}} onclick="linkSource.showPopup('register')">
                            Thêm dịch vụ
                        </button>
                        <div class="table-content m--padding-top-30">
                            <div class="table-responsive">
                                <table class="table table-striped m-table ss--header-table table-refer">
                                    <thead class="bg">
                                    <tr class="ss--nowrap">
                                        <th class="ss--font-size-th">{{__('Tên dịch vụ')}}</th>
                                        {{--                                        <th class="ss--font-size-th">{{__('Số tiền (Vnđ)')}}</th>--}}
                                        <th class="ss--font-size-th">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody class="body_tr_register">
                                    @if(isset($listService['register']) && $listService['register'])
                                        @foreach($listService['register'] as $key => $item)
                                            <tr id="tr_{{$item['service_id']}}">
                                                <td>{{$item['service_name_vi']}}</td>
                                                {{--                                        <td>{{$item['price_standard']}}</td>--}}
                                                <td>
                                                    <button type="button" onclick="linkSource.removeService(this,{{$item['service_id']}},'register')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xoá">
                                                        <i class="la la-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="popup_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Danh sách dịch vụ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="search_service">
                        <div class="form-group">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="form-control" id="service_name" name="service_name" placeholder="Tên dịch vụ">
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-secondary" onclick="linkSource.deleteSearch()">Xóa</button>
                                        <button type="button" class="btn btn-primary color_button" onclick="linkSource.searchPopup(1)">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group table-service">

                    </div>
                    <input type="hidden" id="type_popup" >
                    <input type="hidden" id="refer_source_id" value="{{$refer_source_id}}" >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary color_button" onclick="linkSource.addListService()" data-dismiss="modal">Chọn</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/refer-source/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        new AutoNumeric.multiple('.number-money', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });
        $(".daterange-picker").daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            buttonClasses: "m-btn btn",
            applyClass: "btn-primary",
            cancelClass: "btn-danger",
            locale: {
                format: 'DD/MM/YYYY',
                "applyLabel": "Đồng ý",
                "cancelLabel": "Thoát",
                "customRangeLabel": "Tùy chọn ngày",
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                ],
                "monthNames": [
                    "Tháng 1 năm",
                    "Tháng 2 năm",
                    "Tháng 3 năm",
                    "Tháng 4 năm",
                    "Tháng 5 năm",
                    "Tháng 6 năm",
                    "Tháng 7 năm",
                    "Tháng 8 năm",
                    "Tháng 9 năm",
                    "Tháng 10 năm",
                    "Tháng 11 năm",
                    "Tháng 12 năm"
                ],
                "firstDay": 1
            },
            // ranges: {
            //     'Hôm nay': [moment(), moment()],
            //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
            //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
            //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
            //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
            //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            // }
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
        });

    </script>
@stop