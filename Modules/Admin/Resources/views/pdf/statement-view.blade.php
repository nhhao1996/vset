<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    {{-- <meta charset="utf-8"/> --}}
    {{-- <meta charset="utf-8"> --}}
    {{--    <title>@lang('Piospa | Cung cấp giải pháp công nghê quản lý dành cho spa')</title>--}}
    <title>Vset</title>
    <meta name="description" content="Creative portlet examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 14px !important;
        }
        .table td, .table th {
            border: 1px solid #dee2e6;
        }
        .table thead{
            background: #3F874C;
            color: #fff;
        }
        .table {
            text-align: center;
        }
        .block-title p {
            margin-bottom: 0;
            padding: .75rem;
        }
        .block-title {
            background: #B6383B;
            color: #fff;
            font-size: 0.9rem;

        }
        .w-10 {
            width: 10% !important;
        }
        .w-85 {
            width: 85% !important;
        }
    </style>
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

</head>
<body>
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-12 ">
            <div class="w-10 d-inline-block">
                <img width="140px" src="{{asset('static/backend/images/LOGO-VSETGROUP.png')}}">
            </div>
            <div class="w-85 d-inline-block text-center">
                <p align="center" style="font-size: 25px !important;margin-bottom: -5px">
                    <strong>SAO KÊ THÁNG {{\Carbon\Carbon::now()->format('m/Y')}}</strong>
                </p>
            </div>

            <table class="table mb-5 mt-5">
                <thead>
                <tr>
                    <th>Chủ tài khoản</th>
                    <th>Số điện thoại</th>
                    <th>Ngày sao kê</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$user['user_name']}}</td>
                    <td>{{$user['phone']}}</td>
                    <td>{{\Carbon\Carbon::now()->format('d/m/Y')}}</td>
                </tr>
                </tbody>
            </table>

            <p class="text-center"><strong>Thông tin chi tiết</strong></p>
            {{--            Trái phiếu--}}
            <div class="text-center block-title text-center text-uppercase"><p><strong>Hợp tác đầu tư</strong></p></div>
            <table class="table">
                <thead>
                <tr>
                    <th colspan="6">Tổng tiền đầu tư : {{number_format($list['total']['bond'])}} VNĐ</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>STT</td>
                    <td>Mã hợp đồng</td>
                    <td>Hạn mức đầu tư</td>
                    <td>Lãi suất kỳ</td>
                    <td>Tổng lãi đến kỳ hiện tại</td>
                    <td>Tổng lãi có thể rút</td>
                </tr>
                    @foreach($list['bond'] as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item['customer_contract_code']}}</td>
                            <td>{{number_format($item['total_amount'])}} VNĐ</td>
                            <td>{{number_format($item['interest'])}} VNĐ</td>
                            <td>{{number_format($item['interest_total_amount_month'])}} VNĐ</td>
                            <td>{{number_format($item['interest_banlance_amount'])}} VNĐ</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{--            Tiết kiệm--}}
            <div class="text-center block-title text-uppercase"><p><strong>Tiết kiệm</strong></p></div>
            <table class="table">
                <thead>
                <tr>
                    <th colspan="6">Tổng tiền đầu tư : {{number_format($list['total']['saving'])}} VNĐ</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>STT</td>
                    <td>Mã hợp đồng</td>
                    <td>Hạn mức đầu tư</td>
                    <td>Lãi suất kỳ</td>
                    <td>Tổng lãi đến kỳ hiện tại</td>
                    <td>Tổng lãi có thể rút</td>
                </tr>
                    @foreach($list['saving'] as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item['customer_contract_code']}}</td>
                            <td>{{number_format($item['total_amount'])}} VNĐ</td>
                            <td>{{number_format($item['interest'])}} VNĐ</td>
                            <td>{{number_format($item['interest_total_amount_month'])}} VNĐ</td>
                            <td>{{number_format($item['interest_banlance_amount'])}} VNĐ</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{--            Cổ phiếu--}}
{{--            <div class="text-center block-title text-center text-uppercase"><p><strong>Cổ phiếu</strong></p></div>--}}
{{--            <table class="table">--}}
{{--                <thead>--}}
{{--                <tr>--}}
{{--                    <th colspan="6">Tổng tiền đầu tư : {{number_format($list['total']['stock'])}}</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                <tr>--}}
{{--                    <td>Mã hợp đồng</td>--}}
{{--                    <td>Cổ phiếu</td>--}}
{{--                    <td>Cổ phiếu thưởng</td>--}}
{{--                    <td>Ví cổ phiếu</td>--}}
{{--                    <td>Ví cổ tức</td>--}}
{{--                </tr>--}}
{{--                <tr>--}}
{{--                    <td>{{$list['stock']['stock_contract_code']}}</td>--}}
{{--                    <td>{{$list['stock']['stock']}} CP</td>--}}
{{--                    <td>{{$list['stock']['stock_bonus']}} CP</td>--}}
{{--                    <td>{{number_format($list['stock']['wallet_stock_money'])}} VNĐ</td>--}}
{{--                    <td>{{number_format($list['stock']['wallet_dividend_money'])}} VNĐ</td>--}}
{{--                </tr>--}}
{{--                </tbody>--}}
{{--            </table>--}}

            <div>
                <p class="text-center text-uppercase" style="font-size: 1rem !important;">
                    <strong>Thông tin liên hệ</strong>
                </p>
            </div>
            <img class="img-fluid" src="{{asset('static/backend/images/img_info.jpg')}}">
        </div>
    </div>
</div>
</body>