@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ HỢP ĐỒNG')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH HỢP ĐỒNG CẦN GIA HẠN')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>

        </div>
        <div class="m-portlet__body">
            <!-- seach -->
            <form class="frmFilter bg">
                <div class="row padding_row">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                       placeholder="{{__('Nhập mã hợp đồng hoặc chủ hợp đồng')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <select style="width: 100%" name="product_category_id"
                                class="form-control m-input ss--select-2">
                            <option value="">Chọn loại gói</option>
                            <option value="2">Tiết kiệm</option>
                            <option value="1">Hợp tác đầu tư</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <button class="btn btn-primary color_button btn-search">
                                {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                            </button>
                            <a href="{{route('admin.manager-extend-contract')}}"
                               class="btn btn-metal  btn-search padding9x padding9px">
                                <span><i class="flaticon-refresh"></i></span>
                            </a>
                        </div>
                    </div>
                </div>


                @if (session('status'))
                    <div class="alert alert-success alert-dismissible">
                        <strong>{{__('Success')}} : </strong> {!! session('status') !!}.
                    </div>
                @endif
            </form>

            <!--seach -->
            <div class="table-content m--padding-top-30">
                @include('admin::manager-extend-contract.list')

            </div><!-- end table-content -->

        </div>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/manager-extend-contract/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>
@stop