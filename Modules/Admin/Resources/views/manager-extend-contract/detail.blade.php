@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <style>
        .note-editor {
            width: 100%;
        }
        .text-bold-fix {
            font-weight: bold
        }
    </style>
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('GIA HẠN HỢP ĐỒNG')}}
    </span>
@endsection
@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="fa fa-plus-circle"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT GIA HẠN HỢP ĐỒNG')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
        </div>
        <div class="m-portlet__body">
            <form id="form-product" autocomplete="off">
                <div class="row">
                    <h4 class="col-12 form-group m-form__group mb-5">{{__('Thông tin hợp đồng')}}</h4>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Mã hợp đồng')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{$detail['customer_contract_code']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Loại hợp đồng')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{$detail['category_name_vi']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Gói đầu tư')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{$detail['product_name']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Số gói đầu tư')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{$detail['quantity']}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Đơn giá')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{number_format($detail['price_standard'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} Vnđ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Tiền nhận thêm')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{number_format($detail['bonus'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} Vnđ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Tổng giá trị hợp đồng')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{number_format($detail['total_amount'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} Vnđ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Lãi suất')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{number_format($detail['interest_rate'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} %</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Kì hạn')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="text-bold-fix">{{\Carbon\Carbon::parse($detail['customer_contract_end_date'])->format('d/m/Y')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4 class="col-12 form-group m-form__group mb-5 mt-5">{{__('Thông tin hợp đồng')}}</h4>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Gói gia hạn')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <select class="form-control select-fix changeValue" onchange="managerExtendContract.checkValue()" name="product_id" id="product_id">
                                            <option value="">Chọn gói</option>
                                            @foreach($listProduct as $item)
                                                <option value="{{$item['product_id']}}" {{$detail['product_id'] == $item['product_id'] ? 'selected' : ''}}>{{$item['product_name_vi']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Kì hạn')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <select class="form-control select-fix changeValue" onchange="managerExtendContract.checkValue()" name="investment_time_id" id="investment_time_id">
                                            <option value="">Chọn kì hạn</option>
                                            @foreach($listProductInterest as $item)
                                                <option value="{{$item['investment_time_id']}}" {{$detail['investment_time'] == $item['investment_time_month'] ? 'selected' : ''}}>{{$detail['product_category_id'] == 1 ? $item['investment_time_month'] / 12 : $item['investment_time_month']}} {{$detail['product_category_id'] == 1 ? ' năm' : ' tháng'}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Số lượng')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <input type="text" class="form-control quantity changeValueInput" onfocusout="managerExtendContract.checkValue()" id="quantity" name="quantity" placeholder="Nhập số lượng" value="{{$detail['quantity']}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Chu kì rút tiền lãi')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <select class="form-control select-fix changeValue" onchange="managerExtendContract.checkValue()" name="withdraw_interest_time_id" id="withdraw_interest_time_id">
                                            <option value="">Chọn chu kì</option>
                                            @foreach($withdrawInterestTime as $item)
                                                <option value="{{$item['withdraw_interest_time_id']}}" {{$detail['withdraw_interest_time'] == $item['withdraw_interest_month'] ? 'selected' : ''}}>{{$item['withdraw_interest_month']. __(' tháng')}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Lãi suất')}}
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="interest_rate_text">0 </span><span  class="pl-1"> %</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Số tiền được nhận thêm')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="bonus_text">0 </span><span  class="pl-1"> Vnđ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Tổng giá trị hợp đồng mới')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="total_amount_new">0 </span><span  class="pl-1"> Vnđ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Giá trị hợp đồng mới')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="total_new">0 </span><span  class="pl-1"> Vnđ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Giá trị hợp đồng cũ')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <span>{{number_format($detail['total_amount'], 0)}} </span> <span class="pl-1">{{__(' Vnđ')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Số tiền phải thanh toán')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <span class="money_payment">{{number_format(0 - $detail['total'] , isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}} </span> <span class="pl-1">{{__(' Vnđ')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Hình thức thanh toán')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <select class="form-control select-fix select_cash" name="payment">
                                            @foreach($paymentMethod as $item)
                                                <option value="{{$item['payment_method_id']}}">{{$item['payment_method_name_vi']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-3">
                                        {{__('Hình ảnh phiếu thu')}}:
                                    </label>
                                    <div class="input-group col-6">
                                        <div class="form-group m-form__group w-100">
                                            <div class="m-dropzone dropzone dz-clickable"
                                                 action="{{route('admin.manager-extend-contract.uploadDropzoneAction')}}" id="dropzoneone">
                                                <div class="m-dropzone__msg dz-message needsclick">
                                                    <h3 href="" class="m-dropzone__msg-title">
                                                        {{__('Ảnh gói hợp đồng')}}
                                                    </h3>
                                                    <span>{{__('Chọn ảnh gói hợp đồng')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row" id="upload-image">
                                            @if( isset($imageContract) && count($imageContract) > 0)
                                                @foreach($imageContract as $key => $v)
                                                    <div class="image-show-child col-12">
                                                        <input type="hidden" name="arrImage[{{$key}}][image]" value="{{$v['name']}}">
                                                        <a href="{{asset($v['image_file'])}}" target="_blank">
                                                            <p>{{asset($v['image_file'])}}</p>
                                                        </a>
                                                        <span class="delete-img-sv" style="display: block;">
                                                                <a href="javascript:void(0)" onclick="removeImage(this)">
                                                                    <i class="la la-close"></i>
                                                                </a>
                                                            </span>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="customer_id" value="{{$detail['customer_id']}}">
                <input type="hidden" name="total_old" class="total_old" value="{{$detail['total_amount']}}">
                <input type="hidden" name="product_cateogry_id" value="{{$detail['product_category_id']}}">
                <input type="hidden" name="customer_contract_code" value="{{$detail['customer_contract_code']}}">
                <input type="hidden" name="customer_contract_id" id="customer_contract_id" value="{{$detail['customer_contract_id']}}">
                <input type="hidden" name="total_new" id="total_new_submit" value="0">
            </form>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m--align-right">
                    <button type="button" onclick="managerExtendContract.createBill()" style="display: none"
                       class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10 btn-cash">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-plus"></i>
                        <span>{{__('Tạo phiếu thu')}}</span>
                        </span>
                    </button>
                    <button type="button" onclick="managerExtendContract.checkCreateContract()"
                       class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-check"></i>
                        <span>{{__('Gia hạn')}}</span>
                        </span>
                    </button>
                    <a href="{{route('admin.manager-extend-contract')}}"
                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                    </a>
                </div>
            </div>

        </div>

    </div>

    <div class="bill">

    </div>

@endsection
@section('after_script')
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
        var numberImage = 0;
    </script>

    <script type="text/template" id="imageShow">
        <div class="image-show-child mb-0 col-12">
            <input type="hidden" name="arrImage[{numberImage}][image]" value="{link_hidden}">
            <p>{link}</p>
            <span class="delete-img-sv" style="display: block;">
                    <a href="javascript:void(0)" onclick="removeImage(this)">
                        <i class="la la-close"></i>
                    </a>
                </span>
        </div>
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/manager-extend-contract/script.js?v='.time())}}"></script>
    <script>
        dropzone();
        $('.select_cash').change(function () {
            if($('.select_cash').val() == 4) {
                $('.btn-cash').show();
            } else {
                $('.btn-cash').hide();
            }
        })
    </script>
    <script>
        $('.select-fix').select2();
        new AutoNumeric.multiple('.number-money', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });

        new AutoNumeric.multiple('.quantity', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 0
        });
        managerExtendContract.checkValue();
    </script>
@stop
