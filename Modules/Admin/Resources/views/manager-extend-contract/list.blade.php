<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th>#</th>
            <th>{{__('Mã hợp đồng ')}}</th>
            <th>{{__('Tổng giá trị HĐ (vnđ)')}}</th>
            <th>{{__('Chủ hợp đồng')}}</th>
            <th>{{__('Ngày hết hạn')}}</th>
            <th>{{__('Loại gói')}}</th>
            <th>{{__('Thời gian còn lại')}}</th>
            <th>{{__('Hành động ')}}</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">

        @if(isset($LIST))
            @foreach ($LIST as $key => $item)
                @php($num = rand(0,7))
                <tr>
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td class="text_middle">
                        {{$item['customer_contract_code']}}
                    </td>
                    <td class="text_middle">{{ number_format($item['total_amount'], isset(config()->get('config.decimal_number')->value) ? 0 : 0)}}</td>
                    <td class="text_middle">{{$item['full_name']}}</td>
                    <td class="text_middle">{{ $item['customer_contract_end_date'] == 0 ? 'N/A' : date("d-m-Y",strtotime($item['customer_contract_end_date']))  }}</td>
                    <td class="text_middle">{{ $item['category_name_vi']}}</td>
                    <td class="text_middle" >
                        {{\Carbon\Carbon::parse($item['customer_contract_end_date'])->diff(\Carbon\Carbon::now())->days}} ngày
                    </td>
                    <td class="text_middle">
                        @if(\Carbon\Carbon::now()->addMonth($item['month_extend']) >= \Carbon\Carbon::parse($item['customer_contract_end_date']))
                            <a href="javascript:void(0)" onclick="managerExtendContract.checkExtendContract('{{$item['customer_contract_id']}}','{{$item['customer_contract_code']}}')" class="btn btn-primary color_button btn-search">
                                Gia hạn
                            </a>
                        @endif
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}


