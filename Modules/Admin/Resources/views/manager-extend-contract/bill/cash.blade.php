<html>
<style>
    table{
        width:100% !important;
    }
</style>
<body>
<table style="height: 57px; width: 1040px;">
    <tbody>
    <tr style="height: 103px;">
        <td style="width: 507px; height: 103px;"><img src="https://traiphieu.vsetgroup.vn/assets/img/logo.png" alt="" width="157" height="75" /></td>
        <td style="width: 517px; height: 103px; text-align: left;">
            <h4>Tập đo&agrave;n VSETGROUP</h4>
            <h4>Hotline 0902 73 9999</h4>
            <h4>49 Ho&agrave;ng Bật Đạt, Phường 15,Q.T&acirc;n B&igrave;nh, Hồ Ch&iacute; Minh</h4>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<h2 style="text-align: center;">BI&Ecirc;N NHẬN THANH TO&Aacute;N</h2>
<p style="text-align: center;">Payment receipt</p>
<p style="text-align: left;"><strong>Đơn vị thanh to&aacute;n</strong>:&nbsp; {{$data['investment_unit']}}</p>
<p style="text-align: left;"><strong>Họ t&ecirc;n người nộp tiền</strong> : {{$data['user_name']}}</p>
<table style="height: 23px; width: 1025px;">
    <tbody>
    <tr>
        <td style="width: 329px;"><strong>CMND&nbsp;</strong>: {{$data['cmnd']}}</td>
        <td style="width: 330px;"><strong>Ng&agrave;y cấp</strong> : {{$data['created']}}</td>
        <td style="width: 344px;"><strong>Nơi cấp</strong> : {{$data['issued_by']}}</td>
    </tr>
    </tbody>
</table>
<p style="text-align: left;"><strong>Địa chỉ thường tr&uacute;</strong> :&nbsp; {{$data['address']}}</p>
<p style="text-align: left;"><strong>Số điện thoại li&ecirc;n lạc</strong> :&nbsp; {{$data['phone']}}</p>
<p style="text-align: left;"><strong>L&yacute; do nộp tiền</strong> : {{$data['reason']}}</p>
<p style="text-align: left;"><strong>M&atilde; hợp đồng thanh to&aacute;n</strong> :&nbsp; {{$data['order_code']}}</p>
<p style="text-align: left;"><strong>Số tiền thanh to&aacute;n</strong> :&nbsp; {{$data['payment_amount'] != null ? $data['payment_amount'].' VND' : ''}}</p>
<p style="text-align: left;"><strong>Số tiền thanh to&aacute;n (bằng chữ)</strong> : {{$data['payment_amount_text']}}</p>
<p style="text-align: left;"><strong>Họ t&ecirc;n người thu tiền</strong> : {{$data['name_collector']}}</p>
<p style="text-align: left;"><strong>Số điện thoại</strong> : {{$data['phone_collector']}}</p>
<p style="text-align: right;">......................., <strong>ng&agrave;y</strong> ............ <strong>th&aacute;ng</strong> ........... <strong>năm</strong>...........</p>
<table style="height: 69px;" width="1027">
    <tbody>
    <tr>
        <td style="width: 505px; text-align: center;">
            <h3>Người thu tiền</h3>
            <p>K&yacute; v&agrave; ghi r&otilde; họ t&ecirc;n</p>
        </td>
        <td style="width: 506px; text-align: center;">
            <h3 style="text-align: right;">Người nộp tiền&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h3>
            <p style="text-align: right;">K&yacute; v&agrave; ghi r&otilde; họ t&ecirc;n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        </td>
    </tr>
    </tbody>
</table>
<h3 style="text-align: left;">&nbsp;</h3>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">&nbsp;</p>
</body>
</html>