<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_od_detail">Hình thức thu tiền</th>
            <th class="tr_thead_od_detail">Số tiền thu (Vnđ)</th>
            <th class="tr_thead_od_detail">Ảnh biên lai</th>
            <th class="tr_thead_od_detail">Nhân viên xử lý</th>
        </tr>
        </thead>
        <tbody>
            @foreach($list as $key => $item)
                <tr>
                    <td>
                        @if($item['receipt_type'] == 'cash')
                            {{__('Tiền mặt')}}
                        @elseif($item['receipt_type'] == 'transfer')
                            {{__('Chuyển khoản')}}
                        @elseif($item['receipt_type'] == 'member_point')
                            {{__('Tiền thưởng')}}
                        @elseif($item['receipt_type'] == 'interest')
                            {{__('Tiền lãi')}}
                        @elseif($item['receipt_type'] == 'bonus')
                            {{__('Tiền thưởng')}}
                        @endif
                    </td>
                    <td>{{number_format($item['amount'],2)}}</td>
                    <td>
                        @if(isset($arrGroupImage[$item['receipt_detail_id']]))
                            @foreach($arrGroupImage[$item['receipt_detail_id']] as $keyImage => $value)
                                @if($keyImage == 0)
                                    <a href="{{$value['image_file']}}" target="_blank">
                                        Ảnh {{$keyImage+1}}
                                    </a>
                                @else
                                    <a href="{{$value['image_file']}}" target="_blank">
                                       , Ảnh {{$keyImage+1}}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td>{{$item['full_name']}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $list->links('admin::buy-bonds-request.helpers.paging-receipt-detail') }}
</div>