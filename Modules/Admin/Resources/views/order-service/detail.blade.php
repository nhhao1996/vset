@extends('layout')
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/service-card.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ YÊU CẦU MUA DỊCH VỤ')}}</span>
@stop
@section('content')
    <style>
        input[type=file] {
            padding: 10px;
            background: #fff;
        }
        .m-widget5 .m-widget5__item .m-widget5__pic > img {
            width: 100%
        }
        .form-control-feedback {
            color : red;
        }
    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-edit"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT YÊU CẦU MUA DỊCH VỤ')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                @if(($detail['payment_method_id'] == 2 || $detail['payment_method_id'] == 3) && $detail['process_status'] != 'paysuccess')
                    @if(in_array('admin.order-service.createWallet', session('routeList')))
                        @if((double)$total < (double)$detail['total'])
                            <button type="button" onclick="OrderService.confirmFail(1)"
                               class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                <span class="text-white">
                                    <i class="fas fa-check-square"></i>
                                    <span class="zxrem">  {{__('Xác nhận')}}</span>
                                </span>
                            </button>
                        @else
                            <a href="javascript:void(0)" onclick="OrderService.addWallet()"
                               class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                <span class="text-white">
                                    <i class="fas fa-check-square"></i>
                                    <span class="zxrem">  {{__('Xác nhận')}}</span>
                                </span>
                            </a>
                        @endif
                    @endif
                @elseif ($detail['process_status'] == 'new')
                    {{--                    <a data-toggle="modal" data-target="#modalAdd"  --}}
                    @if(in_array('admin.order-service.createReceipt', session('routeList')))
                        <a href="javascript:void(0)" onclick="OrderService.add(0)"
                           class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                    <span class="text-white">
                                        <i class="fas fa-check-square"></i>
                                        <span class="zxrem">  {{__('Xác nhận')}}</span>
                                    </span>
                        </a>
                    @endif
                @elseif ($detail['process_status'] != 'new' && $detail['process_status'] != 'paysuccess')
                    {{--                    <a data-toggle="modal" data-target="#modalMakeReceipt"--}}
                    @if(in_array('admin.order-service.createReceipt', session('routeList')))
                        <a href="javascript:void(0)" onclick="OrderService.showPopupReceiptDetail()"
                           class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                        <span class="text-white">
                            <i class="fas fa-check-square"></i>
                            <span class="zxrem">  {{__('Tạo phiếu thu')}}</span>
                        </span>
                        </a>
                    @endif
                @endif

            </div>
        </div>

        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Mã dịch vụ:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['order_service_code']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tên dịch vụ :
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['service_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Trạng thái đơn hàng:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['status']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số lượng gói:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['quantity']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                           Giá trị gói (Vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{number_format($detail['price_standard'], 2) }}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Hình thức thanh toán:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['payment_method_name_vi']}}" disabled="">
                        </div>
                    </div>
                    @if($detail['payment_method_id'] == 1)
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Cú pháp giao dịch
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control" value="{{$detail['order_service_code']}}" disabled="">
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Ngày thanh toán:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{\Carbon\Carbon::parse($detail['payment_date'])->format('H:i:s d-m-Y')}}" disabled="">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Ngày bắt đầu:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{\Carbon\Carbon::parse($detail['valid_from_date'])->format('H:i:s d-m-Y')}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Ngày kết thúc:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{\Carbon\Carbon::parse($detail['valid_to_date'])->format('H:i:s d-m-Y')}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Họ và tên nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_name']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Số điện thoại nhà đầu tư:
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_phone']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Email nhà đầu tư :
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{$detail['customer_email']}}" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Địa chỉ nhà đầu tư :
                        </label>
                        <div class="col-lg-8">
{{--                            <input class="form-control" value="{{$detail['customer_residence_address']}}" disabled="">--}}
                            @if($detail['residence_address'] == null && $detail['lienhe_dis_name'] == null && $detail['lienhe_pro_name'] == null)
                                <input class="form-control" value="" disabled>
                            @else
                                <input class="form-control" value="{{$detail['residence_address']}}, {{$detail['lienhe_dis_type']}}  {{$detail['lienhe_dis_name']}},{{$detail['lienhe_pro_type']}} {{$detail['lienhe_pro_name']}}" disabled>
                            @endif
                        </div>
                    </div>
{{--                    <div class="row form-group">--}}
{{--                        <label class="col-form-label label col-lg-4 black-title">--}}
{{--                            Người giới thiệu :--}}
{{--                        </label>--}}
{{--                        <div class="col-lg-8">--}}
{{--                            <input class="form-control" value="{{$detail['refer_name']}}" disabled="">--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    @if($detail['payment_method_id'] == 2 || $detail['payment_method_id'] == 3 )
                        <div class="row form-group">
                            <label class="col-form-label label col-lg-4 black-title">
                                Số dư khả dụng (vnđ):
                            </label>
                            <div class="col-lg-8">
                                <input class="form-control {{(double)$total >= (double)$detail['total'] ? 'text-success' : 'text-danger'}}" value="{{number_format($total,2)}}" disabled="">
                            </div>
                        </div>
                    @endif
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            Tổng tiền (vnđ):
                        </label>
                        <div class="col-lg-8">
                            <input class="form-control" value="{{number_format($detail['total'],2)}}" disabled="">
                        </div>
                    </div>
                </div>
            </div>
            <input name="customer_id" id="customer_id" value="{{$detail['customer_id']}}" hidden>
            <input name="withdraw_request_id" id="withdraw_request_id" value="{{$detail['withdraw_request_id']}}" hidden>
            <input name="order_service_id" id="order_service_id" value="{{$detail['order_service_id']}}" hidden>
            <input name="total_amount" id="total_amount" value="{{$detail['total_amount']}}" hidden>
            <input name="total" id="total" value="{{$detail['total']}}" hidden>
            <input type="hidden" class="payment_method_id" value="{{$detail['payment_method_id']}}" >

            @include('admin::order-service.popup.create-group')
{{--            @include('admin::buy-bonds-request.popup.create-receipt')--}}
            <div id="append-create-receipt"></div>
            <!-- hóa đơn -->
            <div class="m-portlet__head pl-0">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-list"></i>
                    </span>
                        <h2 class="m-portlet__head-text">
                           Danh Sách phiếu thu
                        </h2>

                    </div>
                </div>
            </div>
            <div class="form-group m-form__group bdt_order bdb_order">
                <div class="m-section__content receipt-detail-list">

                </div>
                <input type="hidden" id="page" name="page" value="">
            </div>
            <!-- hóa đơn -->
        </div>
        <div class="m-portlet__foot">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.order-service')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                    <span>
                    <i class="la la-arrow-left"></i>
                    <span>{{__('QUAY LẠI ')}}</span>
                    </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/order-service/script.js?v='.time())}}"></script>
    <script src="{{asset('static/backend/js/admin/order-service/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        new AutoNumeric.multiple('.name', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });
    </script>
    <script type="text/template" id="imageShow">
        <div class="wrap-img image-show-child m-3">
            <input type="hidden" name="img-transfer[]" value="{link_hidden}">
            <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px"
                 height="100px">
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="OrderService.remove_img(this)">
                    <i class="la la-close class_remove"></i>
                </a>
            </span>
        </div>
    </script>
    <script>
        OrderService.getListReceiptDetail(1);
        OrderService.dropzone();
    </script>

@stop