@extends('layout')@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("CHỢ CỔ PHIẾU")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }

        .m-datatable__head th {
            background-color: #dff7f8 !important;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT ĐĂNG KÝ BÁN')}}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                @if($item['status'] == 'new')
                    @if((int)$item['stock'] < (int)$item['quantity'])
                        <button type="button" onclick="RegisterSale.confirmFail(3)"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                    <span class="text-white">
                                        <i class="fas fa-check-square"></i>
                                        <span class="zxrem">  {{__('Xác nhận')}}</span>
                                    </span>
                        </button>
                    @else
                        <button type="button" onclick="RegisterSale.changeStatus('new')"
                                class=" btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">
                                <span class="text-white">
                                    <i class="fas fa-check-square"></i>
                                    <span class="zxrem">  {{__('Xác nhận')}}</span>
                                </span>
                        </button>
                    @endif
                    <a href="javascript:void(0)" onclick="RegisterSale.changeStatus('cancel')"
                       class="btn btn-danger btn-sm m-btn m-btn--icon m-btn--pill ml-2">
                                    <span class="text-white">
                                        <span class="zxrem">Huỷ xác nhận</span>
                                    </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="row">
                <div class="form-group col-lg-6" hidden>
                    <div class="row form-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="stock_market_id" name="stock_market_id"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['stock_market_id']) ? $item['stock_market_id'] : 'N/A'}}">
                            </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label col-lg-4 black-title">
                            {{__('Mã nhà đầu tư')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['customer_code']) ? $item['customer_code'] : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Tài khoản')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['phone']) ? $item['phone'] : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Họ và tên')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['full_name']) ? $item['full_name'] : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Email')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['email']) ? $item['email'] : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Số cổ phiếu')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['stock']) ? number_format($item['stock']) : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Số cổ phiếu sau khi bán')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{(isset($item['stock']) && isset($item['quantity'])) ? number_format((int)$item['stock']-(int)$item['quantity']) : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Số lượng bán')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['quantity']) ? number_format($item['quantity']) : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Giá bán')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['money']) ? number_format($item['money']) : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-6 float-right">
                    <div class="row form-group">
                        <label class="col-form-label label black-title">
                            {{__('Thành tiền')}}:
                        </label>
                        <div class="input-group">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" id="customer_code" name="customer_code"
                                       class="form-control m-input" disabled
                                       value="{{isset($item['total']) ? number_format($item['total']) : 'N/A'}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin::Portlet-->
        <div class="m-portlet__body">
            <div class=" m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.stock-market.register-to-sell')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                        <span>
                            <i class="la la-arrow-left"></i>
                            <span>{{__('QUAY LẠI')}}</span>
                        </span>
                    </a>

                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
    {{--    </form>--}}
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/stock-market/script.js?v='.time())}}" type="text/javascript"></script>
@stop
