<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('#')}}</th>
            <th class="ss--font-size-th">{{__('Người đăng ký')}}</th>
            <th class="ss--font-size-th">{{__('Số lượng')}}</th>
            <th class="ss--font-size-th">{{__('Giá bán')}}</th>
            <th class="ss--font-size-th">{{__('Mua lẻ')}}</th>
            <th class="ss--font-size-th">{{__('Ngày đăng ký')}}</th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                    <td>
{{--                        @if(in_array('admin.stock-market.register-to-sell.detai', session('routeList')))--}}
                            <a href="{{route('admin.stock-market.register-to-sell.detail',['id'=>$item['stock_market_id']])}}"> {{$item['full_name']}} </a>
{{--                        @else--}}
{{--                            {{$item['full_name']}}--}}
{{--                        @endif--}}
                    </td>
                    <td>{{number_format($item['quantity'])}}</td>
                    <td>{{number_format($item['money'])}}</td>
                    <td>
                        {{$item['retail_purchase'] == "1" ? __("Có") : __("Không")}}
                    </td>
                    <td>{{ Carbon\Carbon::parse($item['created_at'])->format('d-m-Y') }}</td>
                    <td>
                        <a href="{{route('admin.stock-market.register-to-sell.detail',['id'=>$item['stock_market_id']])}}"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           title="{{__('Chi tiết')}}">
                            <i class="flaticon-eye"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}