<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('#')}}</th>
            <th class="ss--font-size-th">{{__('Người bán')}}</th>
            <th class="ss--font-size-th">{{__('Số lượng')}}</th>
            <th class="ss--font-size-th">{{__('Giá bán')}}</th>
            <th class="ss--font-size-th">{{__('Mua lẻ')}}</th>
            <th class="ss--font-size-th">{{__('Ngày bán')}}</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                        <td>{{$item['full_name']}}</td>
                        <td>{{number_format($item['quantity'])}}</td>
                        <td>{{number_format($item['money'])}}</td>
                        <td>
                            {{$item['retail_purchase'] == "1" ? __("Có") : __("Không")}}
                        </td>
                        <td>{{ Carbon\Carbon::parse($item['sell_date'])->format('d-m-Y') }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}