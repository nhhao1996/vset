<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">{{__('#')}}</th>
            <th class="ss--font-size-th">{{__('Người bán')}}</th>
            <th class="ss--font-size-th">{{__('Người mua')}}</th>
            <th class="ss--font-size-th">{{__('Số lượng')}}</th>
            <th class="ss--font-size-th">{{__('Giá bán')}}</th>
            <th class="ss--font-size-th">{{__('Ngày mua')}}</th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>

        @if (isset($LIST))
            @foreach ($LIST as $key=>$item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td class="text_middle">{{ ($page-1)*10 + $key+1}}</td>
                    @else
                        <td class="text_middle">{{$key+1}}</td>
                    @endif
                        @if($item['source'] != 'publisher')
                            <td>
                                {{-- @if(in_array('admin.stock-market.buy-stock.detail', session('routeList'))) --}}
                                    <a href="{{route('admin.stock-market.buy-stock.detail',['id'=>$item['stock_order_id']])}}">{{$item['seller_name']}}
                                    </a>
                                {{-- @else
                                    {{$item['seller_name']}}
                                @endif --}}
                            </td>
                        @else
                            <td>{{__('Nhà phát hành')}}</td>
                        @endif
                        <td>{{$item['buyer_name']}}</td>
                        <td>{{number_format($item['quantity'])}}</td>
                        <td>{{number_format($item['money_standard'])}}</td>
                        <td>{{ $item['payment_date'] != '' ? Carbon\Carbon::parse($item['payment_date'])->format('d-m-Y') : ""}}</td>
                        <td>
                            <a href="{{route('admin.stock-market.buy-stock.detail',['id'=>$item['stock_order_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="flaticon-eye"></i>
                            </a>
                        </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}