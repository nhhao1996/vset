@extends('layout')@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> @lang("CHỢ CỔ PHIẾU")</span>
@stop
@section('content')
    <style>
        .m-image {
            padding: 5px;
            max-width: 155px;
            max-height: 155px;
            background: #ccc;
        }

        .m-datatable__head th {
            background-color: #dff7f8 !important;
        }
    </style>
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHI TIẾT MUA CỔ PHIẾU')}}
                    </h3>
                </div>
            </div>
            <div>
                <ul class="nav nav-tabs text-center">
                    <li class="nav-item">
                        <a class="nav-link stock-market" style="color: #027177;"
                           href="{{route('admin.stock-market.selling')}}">{{__('ĐANG BÁN')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link stock-market active" style="color: white !important;background-color: #4fc4cb !important;"
                           href="{{route('admin.stock-market.buy-stock')}}">{{__('MUA CỔ PHIẾU')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link stock-market" style="color: #027177;"
                           href="{{route('admin.stock-market.register-to-sell')}}">{{__('ĐĂNG KÝ BÁN')}}</a>
                    </li>
                </ul>
            </div>
            <style>
                .stock-market{
                    width: 150px;
                    height: 53px;
                    padding-top: 16px;
                    font-weight: bold;
                }
            </style>
        </div>
        <div class="m-portlet__body">
            <div class="row rounded border mb-3">
                    <div class="form-group col-lg-6">
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Người bán')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['seller_name']) ? $item['seller_name'] : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Tài khoản người bán')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['seller_phone']) ? $item['seller_phone'] : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Số cổ phiếu trước khi bán')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['seller_quantity_before']) ? (int)$item['seller_quantity_before'] : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Số lượng bán')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['sell_quantity']) ? number_format($item['sell_quantity']) : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Cổ cổ phiếu sau khi bán')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['seller_quantity_after']) ? $item['seller_quantity_after'] : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label black-title">
                                    {{__('Giá bán')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['money_standard']) ? number_format($item['money_standard']) : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Người mua')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['buyer_name']) ? $item['buyer_name'] : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Tài khoản người mua')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['buyer_phone']) ? $item['buyer_phone'] : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label black-title">
                                    {{__('Số cổ phiếu trước khi mua')}}
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['buyer_quantity_before']) ? number_format($item['buyer_quantity_before']) : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Số lượng mua')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['quantity']) ? number_format($item['quantity']) : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Số cổ phiếu sau khi mua')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['buyer_quantity_after']) ? number_format($item['buyer_quantity_after']) : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Phí')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['fee']) ? $item['fee'].'%'.'('. number_format($item['total']*$item['fee']/100) .')' : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="row form-group">
                                <label class="col-form-label label   black-title">
                                    {{__('Thành tiền')}}:
                                </label>
                                <div class="input-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" id="customer_code" name="customer_code"
                                               class="form-control m-input" disabled
                                               value="{{isset($item['total']) ? number_format($item['total']) : 'N/A'}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

        <!--begin::Portlet-->
        <div class="m-portlet__body">
            <div class=" m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.stock-market.buy-stock')}}"
                       class="btn btn-metal bold-huy m-btn  m-btn--icon m-btn--wide m-btn--md">
                        <span>
                            <i class="la la-arrow-left"></i>
                            <span>{{__('THOÁT')}}</span>
                        </span>
                    </a>

                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
    {{--    </form>--}}
@stop
