@extends('layout') @section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}" />
@endsection @section('title_header')
    <span class="title_header">
    <img src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;" />
    {{__('CHỢ CỔ PHIẾU')}}
</span>
@endsection @section('content')
    <meta http-equiv="refresh" content="number" />
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotableBuyStock">
        <form class="frmFilter">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            {{__('CHỢ CỔ PHIẾU')}}
                        </h3>
                    </div>
                </div>
                <div>
                    <ul class="nav nav-tabs text-center">
                        <li class="nav-item">
                            <a class="nav-link stock-market "  style="color: #027177;"
                               href="{{route('admin.stock-market.selling')}}">{{__('ĐANG BÁN')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link stock-market active" style="color: white;background-color: #4fc4cb;"
                               href="{{route('admin.stock-market.buy-stock')}}">{{__('MUA CỔ PHIẾU')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link stock-market" style="color: #027177;"
                               href="{{route('admin.stock-market.register-to-sell')}}">{{__('ĐĂNG KÝ BÁN')}}</a>
                        </li>
                    </ul>
                </div>
                <style>
                    .stock-market{
                        width: 150px;
                        height: 53px;
                        padding-top: 16px;
                        font-weight: bold;
                    }
                </style>
            </div>
            <div class="m-portlet__body">
                <div class="bg">
                    <div class="row padding_row">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="{{__('Nhập tên người mua')}}" value="{{isset($FILTER['search']) ? $FILTER['search'] : null}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group" style="background-color: white;">
                                <div class="m-input-icon m-input-icon--right">
                                    <input readonly="" class="form-control m-input daterange-picker" id="created_at" name="created_at" autocomplete="off" placeholder="{{__('Ngày mua')}}" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <button class="btn btn-primary color_button btn-search">{{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i></button>
                                <a href="{{route('admin.stock-market.buy-stock')}}" class="btn btn-metal padding9x">
                                <span>
                                    <i class="flaticon-refresh"></i>
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-content m--padding-top-30">
                    @include('admin::stock-market.buy-stock.list')
                </div>
                <!-- end table-content -->
            </div>
        </form>
    </div>
@endsection @section('after_script')
    <script src="{{asset('static/backend/js/admin/stock-market/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
@stop
