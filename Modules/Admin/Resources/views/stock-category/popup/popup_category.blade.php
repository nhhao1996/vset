<div class="modal fade" id="popup_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>
                    @if($param['type'] == 'created' )
                        {{_('Tạo danh mục')}}
                    @elseif($param['type'] == 'detail' )
                        {{_('Chi tiết danh mục')}}
                    @elseif($param['type'] == 'edit' )
                        {{_('Chỉnh sửa danh mục')}}
                    @endif
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-category" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tên danh mục (VI)')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="bank_name" name="stock_category_title_vi"  type="text" class="form-control m-input class"
                                           placeholder="{{__('Tên danh mục (VI)')}}" {{$param['type'] == 'detail' ? 'disabled' : ''}}
                                           aria-describedby="basic-addon1" value="{{$param['type'] != 'created' ? $param['detail']['stock_category_title_vi'] : '' }}">
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tên danh mục (EN)')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="bank_name" name="stock_category_title_en"  type="text" class="form-control m-input class"
                                           placeholder="{{__('Tên danh mục (EN)')}}" {{$param['type'] == 'detail' ? 'disabled' : ''}}
                                           aria-describedby="basic-addon1" value="{{$param['type'] != 'created' ? $param['detail']['stock_category_title_en'] : '' }}">
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Trạng thái')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input type="checkbox" id="is_active" name="is_active" class="manager-btn" {{$param['type'] == 'detail' ? 'disabled' : ''}} {{$param['type'] == 'created' ? 'checked' : ($param['detail']['is_active'] == 1 ? 'checked' : '') }} >
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>
                    </div>
                    @if($param['type'] != 'created')
                        <input type="hidden" name="stock_category_id" value="{{$param['detail']['stock_category_id']}}">
                    @endif
                </form>
                <div class="save-attribute m--margin-right-20">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                        <div class="m-form__actions m--align-right">
                            <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close"
                               class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
                            </a>
                            @if($param['type'] != 'detail')
                                <a href="javascript:void(0)" onclick="stockCategory.save()"
                                   class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                    <span class="ss--text-btn-mobi">
                                    <i class="la la-check"></i>
                                    <span>{{__('LƯU THÔNG TIN')}}</span>
                                    </span>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>