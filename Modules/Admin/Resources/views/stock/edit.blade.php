@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ CỔ PHIẾU')}}</span>
@stop
@section('content')


    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH CỔ PHIẾU')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
            {{-- Begin: Tab--------------- --}}
            @include("admin::stock.include.top-nav")
{{--            End: Tab---------------------}}
        </div>

        <div class="m-portlet__body">
            <div class="table-content m--padding-top-30 ">

                <div class="form-group m-form__group w-50">
                    <label class="black-title">Tên cổ phiếu:</label>
                    {{-- {{dd($stock->toArray())}} --}}
                    <form id="frmStockConfig">
                        <input id="stock_code" name="stock_code"  class="form-control " value="{{$stock->stock_config_code}}"  />
                    </form>
                </div>


                </div>
                <div class="form-group m-form__group text-right">
                    <button id="btnAdd" class="btn btn-primary color_button btn-search">Thêm <i class="fa fa-pen ic-search m--margin-left-5"></i></button>

                </div>

                <div class="form-group m-form__group">
                    <label class="black-title">Tài liệu liên quan:</label>



                    @include('admin::stock.list')


                </div><!-- end table-content -->

            <div class="form-group m-form__group text-right">

                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.stock.list')}}" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>HỦY</span>
                        </span>
                    </a>
                    <button id="btnEdit"  type="button"  href="{{route('admin.stock.edit',$stock->stock_config_id)}}"  class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                            <i class="la la-check"></i>
                            <span>LƯU LẠI</span>
                            </span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    @include('admin::stock.popup.upload')
@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
<script type="text/template" id="imageFile">
    <div class="list-file mb-0 col-12">
        <input type="hidden" name="arrFile[]" value="{link_hidden}">
        <p>
            <span class="float-left"> {link} </span>
            <a class="float-right" href="javascript:void(0)" onclick="removeImage(this)">
                <i class="la la-close"></i>
            </a>
        </p>

    </div>
</script>
    <script>
         $(document).ready(function(){
                CustomerContractHandler.init();
            });
        var CustomerContractHandler = {
            customer_contract_id:"",
            init(){
                this.onBtnEndContract();

            },
            onBtnEndContract(){
                    $(".btnEndContract").on("click",function(){
                    CustomerContractHandler.customer_contract_id = $(this).attr("data-id");
                            Swal.fire({
                    title: "{{__('Bạn chắc chắn chứ?')}}",
                    text: "{{__('Bạn sẽ không thể hoàn nguyên điều này!')}}",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: "{{__('Đồng ý')}}"
                    }).then((result) => {
                        if(result.value){
                               CustomerContractHandler.handleEndContract();
                            }
                    // if (result.isConfirmed) {
                    //     alert("hello");
                    //     CustomerContractHandler.handleEndContract();
                    //     alert('da ket thuc hop dong');
                    //     return;
                    //     Swal.fire(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    //     )
                    // }
                    })
                        });
                },
                handleEndContract(){


                        $.ajax({
                        url: laroute.route('admin.customer-contract.endContract'),
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            customer_contract_id:parseInt(CustomerContractHandler.customer_contract_id)
                        },
                        success:function (res) {
                            swal(
                        "{{__('Hợp đồng đã kết thúc')}}",
                        '',
                        'success'
                    ).then(function(v){
                        window.location.reload();
                    });

                        }
                    });


            },



        }// end Customer Contract Handler

    </script>
    <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
    <script>
        //Begin:Dropzone-------------------

        function dropzone() {
            Dropzone.options.dropzoneonecash = {
                paramName: 'file',
                maxFilesize: 10, // MB
                maxFiles: 20,
                // acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dictRemoveFile: 'Xóa',
                dictMaxFilesExceeded: 'Bạn tải quá nhiều hình ảnh',
                dictInvalidFileType: 'Tệp không hợp lệ',
                dictCancelUpload: 'Hủy',
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime().toString() + dt.getDate().toString() + (dt.getMonth() + 1).toString() + dt.getFullYear().toString();
                    var random = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    for (let z = 0; z < 10; z++) {
                        random += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return time + "_" + random + "." + file.name.substr((file.name.lastIndexOf('.') + 1));
                },
                init: function () {
                    this.on("success", function (file, response) {
                        var a = document.createElement('span');
                        a.className = "thumb-url btn btn-primary";
                        a.setAttribute('data-clipboard-text', laroute.route('admin.customer-change-request.uploadAppendixContract'));

                        if (file.status === "success") {
                            //Xóa image trong dropzone
                            $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                                file.previewElement.remove();
                            });
                            $('#dropzoneonecash').removeClass('dz-started');
                            //Append vào div image
                            let tpl = $('#imageFile').html();
                            tpl = tpl.replace(/{link}/g, response.file);
                            tpl = tpl.replace(/{link_hidden}/g, response.file);
                            $('#upload-image-cash').append(tpl);
                        }
                    });

                }
            }
        }
        dropzone();
        var StockHandler = {

            init(){
                this.onBtnAdd();
                this.onBtnSave();
                this.onbtnEdit();
                this.onBtnDelete();

            },
            onBtnAdd(){
                $("#btnAdd").on("click",function(){

                    $('#upload-image-cash').empty();
                    $('#uploadFile').modal('show');
                })

            },
            onBtnSave(){
                $("#btnSaveFile").on("click",function(){
                    // alert("hello");
                    $("#stock_code").val($("#stock_code_view").val());
                    StockHandler.saveFileStock();
                });

            },

            saveFileStock(){

                // --------------------------------------------
                let
                data = $('#list-file-doc-related').serialize();
                let arrName = [];
                $('#dropzoneonecash')[0].dropzone.files.forEach(function (file) {
                    arrName.push(file.name);
                                
                });
                arrName = JSON.stringify(arrName);

                
                
                $.ajax({
                    url: laroute.route('admin.stock.saveFile'),
                    method: "POST",
                    dataType: "JSON",
                    data:data + "&arrName="+arrName,

                    success: function (data) {
                        if(data.error == 1){
                            swal('', data.message, "error");
                        }
                        else{
                            $('#uploadFile').modal('hide');
                            swal('', data.message, "success").then(function () {
                                location.reload();
                            });

                        }

                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }

                });

            },
            onbtnEdit(){
                $("#btnEdit").on('click', function(){

                    StockHandler.handleBtnEdit();
                })
            },
            onBtnDelete(){
                $(".icon-delete-file").on('click', function(){
                    console.log('xoa stock file');

                    $(this).parent().parent().remove();
//                     const stockFileId = $(this).attr("data-id");
//                     const data = {
//                         stock_config_file_id:stockFileId
//                     };
//                     Swal.fire({
//   title: '',
//   text: "Bạn có chắc chắn xóa tài liệu này!",
//   icon: 'warning',
//   showCancelButton: true,
//   confirmButtonColor: '#3085d6',
//   cancelButtonColor: '#d33',
//   confirmButtonText: 'Đồng ý!',
//   cancelButtonText:"Hủy",
// }).then((result) => {

//   if (result.value) {
//     StockHandler.handleDeleteFile(data);

//   }
// })


                });

            },
            handleDeleteFile(data){
                $.ajax({
                    url: laroute.route('admin.stockFile.remove'),
                    method: "POST",
                    dataType: "JSON",
                    data:data,

                    success: function (data) {
                        if(data.error == 1){
                            swal('', data.message, "error");
                        }
                        else{
                            $('#uploadFile').modal('hide');
                            swal('', data.message, "success").then(function () {
                                location.reload();
                            });

                        }

                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }

                });

            },
            validate(){

                jQuery.validator.addMethod("specialCharacter", function(value, element) {
                    var regex = /[!@#$%^&*(),.?":{}|<>]/;
                    if(value.match(regex)) return false;
                    return true;
                }, "Tên cổ phiếu không cho phép nhập các ký tự đặc biệt, có dấu");

                $("#frmStockConfig").validate({
                    rules: {
                        "stock_code": {
                            required: true,
                            specialCharacter:true

                        }

                    },
                    messages: {
                        "stock_code": {
                            required: "Yêu cầu nhập tên cổ phiếu"
                        }
                    },

                    submitHandler: function (form) { // for demo

                    }
                });

                return $("#frmStockConfig").valid();
            },
            handleBtnEdit(){
                // begin: Get Array Stock File--------------
                if(!this.validate()) return;
                var arrIdFiles = [];
                 $(".icon-delete-file").each(function(e){
                    // arrIdFiles.push($(this).attr("data-id"));
                    let id = $(this).attr("data-id");
                    arrIdFiles.push(id);
                 });
                // end: Get Array Stock File----------------

                // --------------------------------------------
                let   data = {
                        stock_config_id:$("#stock_config_id").val(),
                        stock_config_code:$("#stock_code").val(),
                        arrIdFiles:arrIdFiles
                };
                // console.log('data la gi', data);
                $.ajax({
                    url: laroute.route('admin.stock.saveStockInfo'),
                    method: "POST",
                    dataType: "JSON",
                    data:data,

                    success: function (data) {
                        if(data.error == 1){
                            swal('', data.message, "error");
                        }
                        else{
                            $('#uploadFile').modal('hide');
                            swal('', data.message, "success").then(function () {
                                location.reload();
                            });

                        }

                    },
                    // error: function (res) {
                    //     console.log('error la gi: ', res);
                    //     var mess_error = '';
                    //     jQuery.each(res.responseJSON.errors, function (key, val) {
                    //         mess_error = mess_error.concat(val + '<br/>');
                    //     });
                    //     swal.fire('', mess_error, "error");
                    // }

                });
            }


        };
        $(document).ready(function(){
            StockHandler.init();
        });

        // End:---Dropzone----------------

    </script>
{{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop