<table class="table table-striped m-table m-table--head-bg-default" style="border-collapse: collapse;">
    <thead class="bg">
    <tr>
        <th class="tr_thead_list_dt_cus">@lang('File hợp đồng')</th>
        <th class="tr_thead_list_dt_cus">@lang('Thứ tự')</th>
        <th class="tr_thead_list_dt_cus">@lang('Tải xuống')</th>
    </tr>
    </thead>
    <tbody style="font-size: 13px">
    @if(isset($listFile))
        @foreach ($listFile as $k => $v)
            <tr>
                <td>
                    <a href="{{asset($v['image_file'])}}" target="_blank">



                        <p>{{asset($v['image_file'])}}</p>
                    </a>
                </td>
                <td>{{$v['position_file']}}</td>
                <td class="nt-active-btn-download">
                  <p>  <a href="{{asset($v['image_file'])}}" class="btn btn-light-success btn-sm mr-3">
                          <i class="fa fa-download"></i> Tải xuống</a></p>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ $listFile->links('helpers.paging') }}

