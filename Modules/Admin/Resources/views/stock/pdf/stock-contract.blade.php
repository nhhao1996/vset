<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    {{-- <meta charset="utf-8"/> --}}
    {{-- <meta charset="utf-8"> --}}
    <meta charset="UTF-8">
{{--    <title>@lang('Piospa | Cung cấp giải pháp công nghê quản lý dành cho spa')</title>--}}
    <title>Vset</title>
    <meta name="description" content="Creative portlet examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
    body {
        font-family: DejaVu Sans;
    }
}
    </style>
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families": ["Roboto:300,400,500,600,700"]},
                active: function () {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!--end::Web font -->
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
    {{-- <link rel="shortcut icon" href="{{asset('static/backend/images/favicon.png')}}" type="image/x-icon"> --}}
</head>
<body>
    



<p align="center">
    <strong>CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM</strong>
</p>
<p align="center">
    <strong>Độc lập – Tự do – Hạnh phúc</strong>
</p>
<p align="center">
    <strong>*********</strong>
</p>
<p align="center">
    <strong>PHỤ LỤC HỢP ĐỒNG SỐ {{$data['numContract']}}</strong>
</p>
<p align="center">
    <strong></strong>
</p>
<p>
    - <em>Căn cứ Hợp đồng </em>
    <em>
        ……<strong>{{$data['stockContract']['stock_contract_code']}}</strong>…. giữa Công ty CP TẬP ĐOÀN VSETGROUP và Công ty/Ông/Bà
        {{$data['customer']['full_name']}};
    </em>
</p>
<p>
    - <em>Căn cứ khả năng và nhu cầu của các B</em><em>ên;</em>
</p>
<p>
    <em></em>
</p>
<p>
    Hôm nay, ngày …{{\Carbon\Carbon::now()->day}}.. tháng …{{\Carbon\Carbon::now()->month}}… năm …{{\Carbon\Carbon::now()->year}}…, tại văn phòng Công ty Cổ phần ………………………, chúng tôi gồm có:
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong>BÊN A: </strong>
    <strong>CÔNG TY CỔ PHẦN TẬP ĐOÀN VSETGROUP</strong>
</p>
<p>
    Đại diện: <strong>……………………………………. </strong>- Chức vụ: <strong>……………………………………….</strong>
</p>
<p>
    Địa chỉ : 107 Cộng Hòa, Phường 12, Quận Tân Bình, TP.HCM.
</p>
<p>
    Điện thoại : <strong>0283 903 8888</strong>
</p>
<p>
Số ĐKKD : <strong>0312706739 </strong>Cấp ngày:    <strong>26/03/2014 </strong>tại: Sở Kế hoạch và Đầu tư TP.HCM.
</p>
<p>
Số tài khoản: <strong>0911007999999 </strong>-    <span>Ngân Hàng </span><strong>Vietcombank Sài Gòn </strong>
</p>
<p>
Website :<a href="http://www.vsetgroup.com/">www.vsetgroup.com     </a>  Email :   <a href="mailto:tuvan@vsetgroup.com">tuvan@vsetgroup.com</a>
</p>
<p>
    Mã số thuế : <strong>0312706739.</strong>
</p>
<p>
    <strong>BÊN B: </strong>
    <strong>CÔNG TY …..</strong>
</p>
<p>
    Đại diện: <strong>………………………………………. </strong>- Chức vụ: <strong>………………………….</strong>
</p>
<p>
    Địa chỉ : <strong>…………………………….</strong>
</p>
<p>
    Điện thoại : <strong>…………….</strong>
</p>
<p>
    Số ĐKKD <strong>……………. </strong>Cấp ngày: <strong>……………. </strong>tại: Sở
    Kế hoạch và Đầu tư TP.HCM.
</p>
<p>
    Số tài khoản: <strong>……………………….</strong>- <span>Ngân Hàng </span><strong>………………………….</strong>
</p>
<p>
Website :<strong> ……………………………………….</strong> Email : <strong> ………………………………….</strong>
</p>
<p>
    Mã số thuế : <strong>…………….</strong>
</p>
<p>
    <strong>(*Trường hợp Bên B là cá nhân:</strong>
    <strong></strong>
</p>
<p>
    <strong>BÊN B: </strong>
    Ông/Bà: <strong>{{$data['customer']['full_name']}}</strong>
</p>
<p>
    Ngày sinh: <strong>{{\Carbon\Carbon::parse($data['customer']['birthday'])->format("d-m-Y")}}</strong>
</p>
<p>
    Giới tính: <strong>{{$data['customer']['gender']=='mail'?'Nam':'Nữ'}}</strong>
</p>
<p>
    Quốc tịch: Việt Nam
</p>
<p>
Số CMND/Căn cước công dân: <strong>{{$data['customer']['ic_no']}}</strong> Cấp ngày: <strong>{{is_null($data['customer']['ic_date'])?$data['defaultDot']:\Carbon\Carbon::parse($data['customer']['ic_date'])->format('d-m-Y')}}</strong> <br/>tại: <strong>{{$data['customer']['ic_place']}}</strong>
</p>
<p>
    Địa chỉ thường trú: <strong>{{$data['customer']['residence_address']}}</strong>
</p>
<p>
    Địa chỉ liên hệ: <strong>{{$data['customer']['contact_address']}}</strong>
</p>
<p>
    Số điện thoại liên hệ<strong>:{{$data['customer']['phone2']}}</strong>
</p>
<p>
    Hình thức thanh toán: <strong>……………</strong>
</p>
<p>
    Số Tài khoản: <strong>{{is_null($data['customer']['bank_account_no'])?$data['defaultDot']:$data['customer']['bank_account_no']}}</strong>  – Ngân hàng: <strong>{{is_null($data['customer']['bank_id'])?'……………':$data['customer']['bank_id']}}</strong>
    Chủ tài khoản: <strong>{{is_null($data['customer']['bank_account_name'])?$data['defaultDot']:$data['customer']['bank_account_name']}}</strong>
</p>
<p>
    Hai Bên thỏa thuận và cùng thống nhất ký Phụ lục Hợp đồng số {{$data['numContract']}} để sửa đổi,
    bổ sung một số nội dung của Hợp đồng  số {{$data['stockContract']['stock_contract_code']}} ngày {{\Carbon\Carbon::parse($data['stockContract']['created_at'])->day}} giữa Công ty CP
…………. và Công ty/Ông/Bà {{$data['customer']['full_name']}} (dưới đây gọi tắt là “    <strong>Hợp Đồng Số {{$data['stockContract']['stock_contract_code']}}.</strong>”) với các nội dung<strong> </strong>như
    sau:
</p>
<p>
    <strong>
        Điều 1. Sửa đổi, bổ sung một số điều của
    </strong>
  
    <strong>Hợp Đồng Số {{$data['stockContract']['stock_contract_code']}}</strong>
    <strong> như sau:</strong>
</p>
<p>
1. Sửa đổi, bổ sung thông tin của Bên B nêu tại phần đầu của    <strong>Hợp Đồng Số {{$data['stockContract']['stock_contract_code']}}</strong> như sau:
</p>
<p>
    <strong>“</strong>
    <strong>BÊN B: </strong>
    Ông/Bà: <strong> {{$data['customer']['full_name']}}</strong>
</p>
<p>
    Ngày sinh: <strong>{{is_null($data['customer']['birthday'])?$data['defaultDot']:\Carbon\Carbon::parse($data['customer']['birthday'])->format('d-m-Y')}}</strong>
</p>
<p>

    Giới tính: <strong>{{$data['customer']['gender']=='male'?'Nam':'Nữ'}}</strong>
</p>
<p>
    Quốc tịch: Việt Nam
</p>
<p>
Số CMND/Căn cước công dân: <strong>{{$data['customer']['ic_no']}}</strong>
</p>
<p>
    Địa chỉ thường trú: <strong>{{$data['customer']['residence_address']}}</strong>
</p>
<p>
    Địa chỉ liên hệ: <strong>{{$data['customer']['contact_address']}}</strong>
</p>
<p>
    Số điện thoại liên hệ<strong>:{{$data['customer']['phone2']}}</strong>
</p>
<p>
    Hình thức thanh toán: <strong>…………</strong>
</p>
<p>
    Số Tài khoản: <strong>{{is_null($data['customer']['bank_account_no'])?$data['defaultDot']:$data['customer']['bank_account_no']}}</strong>
    Chủ tài khoản: <strong>{{is_null($data['customer']['bank_account_name'])?$data['defaultDot']:$data['customer']['bank_account_name']}}</strong>.”.
</p>
<p>
    2. Bổ sung Điều 1a như sau:
</p>
<p>
    <strong>“Điều </strong>
    <strong>1</strong>
    <strong>a. </strong>
    <strong>……</strong>
</p>
<p>
    1………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………..
</p>
<p>
    2………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………..
</p>
<p>
    3. Sửa đổi Điều 5 như sau:
</p>
<p>
    <strong>“Điều </strong>
    <strong>5</strong>
    <strong>. </strong>
    <strong>……</strong>
</p>
<p>
    1………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………..
</p>
<p>
    2………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………..
</p>
<p>
    <strong>Điều 2</strong>
    <strong>.</strong>
    <strong> </strong>
    <strong>Hiệu lực thi hành </strong>
</p>
<p>
    Ngoài các nội dung được sửa đổi, bổ sung nêu tại Điều 1 của Phụ lục Hợp
đồng này thì các nội dung khác của <strong>Hợp Đồng Số {{$data['stockContract']['stock_contract_code']}}</strong>    <strong> </strong>không thay đổi và vẫn giữ nguyên giá trị pháp lý.
</p>
<p>
    Trường hợp có sự mâu thuẫn giữa các nội dung được sửa đổi, bổ sung trong
Phụ lục Hợp đồng này với các nội dung khác trong    <strong>Hợp Đồng Số {{$data['stockContract']['stock_contract_code']}}</strong><strong> </strong>thì các nội dung được sửa
đổi, bổ sung trong Phụ lục Hợp đồng này được ưu tiên giải thích, áp dụng.    <em></em>
</p>
<p>
    Phụ lục Hợp đồng này có hiệu lực kể từ ngày ký và là một phần không thể
    tách rời của <strong>Hợp Đồng Số {{$data['stockContract']['stock_contract_code']}} </strong>.<strong> </strong>Phụ lục
    Hợp đồng được lập thành …… bản chính, mỗi Bên giữ ….. bản có giá trị pháp
    lý như sau.
</p>
<p>
    Các Bên đã đọc lại, đồng ý toàn bộ nội dung và tự nguyện ký tên dưới đây.
</p>
<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="328" valign="top">
                <p align="center">
                    <strong>BÊN B</strong>
                </p>
            </td>
            <td width="329" valign="top">
                <p align="center">
                    <strong>BÊN A</strong>
                    <strong></strong>
                </p>
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
    </tbody>
</table>
    
</body>
</html>


