<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">

        <tbody style="font-size: 13px">

            @foreach($stockFiles as $s)
             <tr>
                <td>
                    <a href=" {{$s['file'] }}" >
                        {{$s['display_name'] }}

                    </a>
                </td>
                @if($isEdit)
                    <td class="pr-4"><i class="icon-delete-file la la-close float-right" data-id="{{$s['stock_config_file_id']}}" style="cursor:pointer" class="la la-close"/></td>
                @endif
            </tr>
            @endforeach



        </tbody>
    </table>
</div>
{{-- {{ $LIST->links('helpers.paging') }} --}}


