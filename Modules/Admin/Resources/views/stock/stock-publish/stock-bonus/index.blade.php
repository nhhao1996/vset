@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ CỔ PHIẾU')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('DANH SÁCH CỔ PHIẾU')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                {{-- @if(in_array('admin.customer-contract.export', session('routeList')))
                    <a href="{{route('admin.customer-contract.export')}}"
                       class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-2">
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span> {{__('Export HỢP ĐỒNG')}}</span>
                        </span>
                    </a>
                @endif --}}

{{--                <a href="{{route('admin.customer-contract.add')}}"--}}
{{--                   class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">--}}
{{--                    <span>--}}
{{--                        <i class="fa fa-plus-circle"></i>--}}
{{--                        <span> {{__('THÊM HỢP ĐỒNG')}}</span>--}}
{{--                    </span>--}}
{{--                </a>--}}

            </div>
            {{-- Begin: Tab--------------- --}}
            <div class="d-flex ml-auto">
            <div class="m-portlet__head-caption p-3">
                <div class="m-portlet__head-title">

                    <a href="{{route('admin.stock.list')}}" class="m-portlet__head-text">
                        THÔNG TIN
                    </a>

                </div>
            </div>
            <div class="m-portlet__head-caption p-3 color_button">
                <div class="m-portlet__head-title ">

                    <a style="color:#fff !important"  href="javascript::void(0)" class="m-portlet__head-text  ">
                        PHÁT HÀNH
                    </a>

                </div>
            </div>
            <div class="m-portlet__head-caption p-3 ">
                <div class="m-portlet__head-title" >
                    <a  href="{{ route('admin.stock.config-bonus.order') }}" class="m-portlet__head-text">
                        THƯỞNG
                    </a>

                </div>

            </div>
        </div>





            {{-- <div class="card-header tab-card-header ">
                {{-- <div class="row">
                    <div class="d-inline p-2 bg-primary text-white">THÔNG TIN</div>
                    <div class="d-inline p-2 bg-dark text-white">PHÁT HÀNH</div>
                    <div class="d-inline p-2 bg-dark text-white">CHI PHÍ</div>
                </div> --}}

            {{-- </div> --}}
            {{-- End: Tab------------------ --}}

        </div>
        <div class="m-portlet__body">
            <div class="table-content m--padding-top-30 ">
                @include('admin::stock.stock-publish.stock-bonus.list')
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-content m--padding-top-30 ">                

            </div>

                {{-- <div class="form-group m-form__group row">
                    <label class="black-title">Tài liệu liên quan:</label>



                {{-- @include('admin::stock.list') --}}

            {{-- </div><!-- end table-content --> --}}
            {{-- <div class="form-group m-form__group row d-flex flex-row-reverse">

                <a href="{{route('admin.stock.edit',1)}}" class="btn btn-primary color_button btn-search">Chỉnh sửa thông tin</a>
                <button onclick="window.location.href='/'" class="btn  btn-metal mr-4">Hủy <i class="fa fa-arrow-left ic-search m--margin-left-5"></i></button>

            </div> --}}

        </div>
    </div>
    @include('admin::stock.stock-publish.stock-bonus.popup.edit')
    @include('admin::stock.popup.popup_publish')
    @include('admin::stock.popup.popup-history')
   
@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
    <script>

    </script>
    {{-- <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script> --}}
    <script>
        var LIST = @json($LIST);
    </script>
    <script src="{{asset('static/backend/js/admin/stock/stock-publish/stock-bonus/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
        $('#created_at').daterangepicker({
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    </script>
{{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop