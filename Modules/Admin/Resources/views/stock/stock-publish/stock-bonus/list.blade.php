<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
            <tr class="ss--nowrap">
                <th class="ss--font-size-th">#</th>
                <th class="ss--font-size-th">{{ __('Thời gian') }}</th>
                <th class="ss--font-size-th">{{ __('CP thưởng') }}</th>
                <th class="ss--font-size-th">{{ __('Hành động') }}</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($LIST))
                @foreach ($LIST as $key => $item)
                    <tr class="ss--font-size-13 ss--nowrap">
                        @if (isset($page))
                            <td>{{ ($page - 1) * 10 + $key + 1 }}</td>
                        @else
                            <td>{{ $key + 1 }}</td>
                        @endif
                        <td>Tháng {{ $item->month }}</td>
                        <td>{{ $item->percent }}%</td>

                        <td class="text-left">
                            {{-- @if (in_array('admin.stock-publish.stock-bonus', session('routeList'))) --}}
                            <a onclick="StockBonusHandler.showPopupEdit({{$item->stock_bonus_id}})" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                href="#" title="Chỉnh sửa">
                                <i class="la la-edit"></i>
                            </a>
                            {{-- @endif --}}
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
