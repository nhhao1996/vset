@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ CỔ PHIẾU')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('PHÁT HÀNH CỔ PHIẾU')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
                {{-- @if(in_array('admin.customer-contract.export', session('routeList')))
                    <a href="{{route('admin.customer-contract.export')}}"
                       class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-2">
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span> {{__('Export HỢP ĐỒNG')}}</span>
                        </span>
                    </a>
                @endif --}}

{{--                <a href="{{route('admin.customer-contract.add')}}"--}}
{{--                   class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button">--}}
{{--                    <span>--}}
{{--                        <i class="fa fa-plus-circle"></i>--}}
{{--                        <span> {{__('THÊM HỢP ĐỒNG')}}</span>--}}
{{--                    </span>--}}
{{--                </a>--}}

            </div>
            {{-- Begin: Tab--------------- --}}
            <div class="d-flex ml-auto">
            <div class="m-portlet__head-caption p-3">
                <div class="m-portlet__head-title">

                    <a href="{{route('admin.stock.list')}}" class="m-portlet__head-text">
                        THÔNG TIN
                    </a>

                </div>
            </div>
            <div class="m-portlet__head-caption p-3 color_button">
                <div class="m-portlet__head-title ">

                    <a style="color:#fff !important"  href="javascript::void(0)" class="m-portlet__head-text  ">
                        PHÁT HÀNH
                    </a>

                </div>
            </div>
            <div class="m-portlet__head-caption p-3 ">
                <div class="m-portlet__head-title">
                    <a  href="{{ route('admin.stock.config-bonus.order') }}" class="m-portlet__head-text">
                        THƯỞNG
                    </a>

                </div>

            </div>
        </div>





            {{-- <div class="card-header tab-card-header ">
                {{-- <div class="row">
                    <div class="d-inline p-2 bg-primary text-white">THÔNG TIN</div>
                    <div class="d-inline p-2 bg-dark text-white">PHÁT HÀNH</div>
                    <div class="d-inline p-2 bg-dark text-white">CHI PHÍ</div>
                </div> --}}

            {{-- </div> --}}
            {{-- End: Tab------------------ --}}

        </div>
        <div class="m-portlet__body">
            <div class="table-content m--padding-top-30 ">
                <div class="row">
                    <div class="col-12">
                        <a id="btnPublish" href="javascript:void(0)" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm float-right d-inline">
                            Phát hành
                        </a>
                        @if($stock->is_active =="1")
                            <a id="btnStopPublish" href="javascript:void(0)" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm float-right d-inline mr-3">
                                Ngưng phát hành
                            </a>
                        @endif                        
                        <a  href="{{route('admin.stock-publish.bonus')}}" class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm float-right d-inline mr-3">
                            Cấu hình thưởng
                        </a>                    
                    </div>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">
            <div class="table-content m--padding-top-30 ">
                <form id = "frm">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Số lượng phát hành:</label>

                                    <input name="stock_id" type="hidden" value="{{$stock->stock_id}}">
                                    <input disabled  class="form-control bg-secondary " value="{{number_format($stock->quantity)}}"  />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Số lượng mua tối thiểu:</label>
                                    <input disabled name="quantity_min" class="form-control bg-secondary " value="{{number_format($stock->quantity_min)}}"  />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold ">Thành tiền:</label>
                                    <input disabled readonly  id="total_amount_view"  class="form-control bg-secondary" value="{{number_format($stock->total_amount)}}"  />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold ">Ngày phát hành:</label>
                                    <input disabled name="publish_at" value="{{is_null($stock->publish_at)?'NaN':\Carbon\Carbon::parse($stock->publish_at)->format('d/m/Y')}}" readonly  id="date_publish"  class="form-control bg-secondary"   />

                                </div>
                                <div class="form-group m-form__group text-right">
                                    <span id="btnHistory"  class="btn ss--btn-search">
                                        Lịch sử
                                    </span>
        
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Đã bán:</label>
                                    <input disabled  name="quantity_sale"  class="form-control bg-secondary " value="{{number_format($stock->quantity_sale)}}"   />
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold w-100">Trạng thái:</label>
                                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input disabled name="is_active" type="checkbox"  @if ($stock->is_active =="1") checked @endif class="manager-btn">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                                <div class="form-group m-form__group ">
                                    <label class="black-title font-weight-bold">Giá phát hành:</label>
                                    <input disabled   class="form-control bg-secondary" value="{{number_format($stock->money)}}"  />
                                </div>

                            </div>
                        </div>
                        <div class="form-group m-form__group text-right">
                            <a href="{{route('admin.stock.list-publish',$stock->stock_id)}}" class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>CHỈNH SỬA</span>
                                </span>
                            </a>

                        </div>
                 </form>


                </div>

                {{-- <div class="form-group m-form__group row">
                    <label class="black-title">Tài liệu liên quan:</label>



                {{-- @include('admin::stock.list') --}}

            {{-- </div><!-- end table-content --> --}}
            {{-- <div class="form-group m-form__group row d-flex flex-row-reverse">

                <a href="{{route('admin.stock.edit',1)}}" class="btn btn-primary color_button btn-search">Chỉnh sửa thông tin</a>
                <button onclick="window.location.href='/'" class="btn  btn-metal mr-4">Hủy <i class="fa fa-arrow-left ic-search m--margin-left-5"></i></button>

            </div> --}}

        </div>
    </div>
    @include('admin::stock.popup.popup-history')
    @include('admin::stock.popup.popup_publish')

@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
    <script>
        $(document).ready(function(){
            $('#autotable-history').PioTable({
                baseUrl: laroute.route('admin.listStockPublish')
            });
        });


    </script>
    {{-- <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script> --}}
    <script src="{{asset('static/backend/js/admin/stock/stock-publish.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
        $(".daterange-picker").daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            buttonClasses: "m-btn btn",
            applyClass: "btn-primary",
            cancelClass: "btn-danger",
            maxDate: moment().endOf("day"),
            startDate: moment().startOf("day"),
            endDate: moment().add(1, 'days'),
            locale: {
                format: 'DD/MM/YYYY',
                "applyLabel": "Đồng ý",
                "cancelLabel": "Thoát",
                "customRangeLabel": "Tùy chọn ngày",
                daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                ],
                "monthNames": [
                    "Tháng 1 năm",
                    "Tháng 2 năm",
                    "Tháng 3 năm",
                    "Tháng 4 năm",
                    "Tháng 5 năm",
                    "Tháng 6 năm",
                    "Tháng 7 năm",
                    "Tháng 8 năm",
                    "Tháng 9 năm",
                    "Tháng 10 năm",
                    "Tháng 11 năm",
                    "Tháng 12 năm"
                ],
                "firstDay": 1
            },
            // ranges: {
            //     'Hôm nay': [moment(), moment()],
            //     'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
            //     "7 ngày trước": [moment().subtract(6, "days"), moment()],
            //     "30 ngày trước": [moment().subtract(29, "days"), moment()],
            //     "Trong tháng": [moment().startOf("month"), moment().endOf("month")],
            //     "Tháng trước": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            // }
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'))
        });
    </script>
{{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop