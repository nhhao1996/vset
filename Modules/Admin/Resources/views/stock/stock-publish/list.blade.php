<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
            <tr class="ss--nowrap">
                <th class="ss--font-size-th">#</th>
                <th class="ss--font-size-th">Số lượng hiện tại đang phát hành</th>
                <th class="ss--font-size-th">Số lượng phát hành</th>
                <th class="ss--font-size-th">Số lượng thực tế đã phát hành</th>
                <th class="ss--font-size-th">Giá phát hành</th>
                <th class="ss--font-size-th">Đã bán</th>
                <th class="ss--font-size-th">Thành tiền</th>
                <th class="ss--font-size-th">Ngày phát hành</th>                                           
                
            </tr>
        </thead>
        <tbody>
            <?php $i = 1 ?>
            @foreach($LIST as $item)
            <tr class="ss--font-size-13 ss--nowrap">
                <td>{{$i++}}</td>
                <td>{{number_format($item->quantity)}}</td>
                <td>{{number_format($item->quantity_publish)}}</td>
                <td>{{number_format($item->quantity - $item->quantity_publish)}}</td>
                <td>{{number_format($item->money)}}</td>
                <td>{{number_format($item->quantity_sale)}}</td>
                <td>{{number_format(($item->quantity_sale*$item->money))}}</td>
                <td>
                    {{\Carbon\Carbon::parse($item->created_at)->format("d-m-Y")}}
{{--  --}}
                </td>                                         
              
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{--{{dd($LIST)}}--}}
@if (isset($stock))
    {{ $LIST->links('helpers.paging') }}
@endif