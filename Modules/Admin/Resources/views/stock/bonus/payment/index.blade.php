@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('QUẢN LÝ CỔ PHIẾU')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
    {{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet m-portlet--head-sm">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                        <h2 class="m-portlet__head-text">
                            {{__('CẤU HÌNH THƯỞNG CỔ PHIẾU THEO PHƯƠNG THỨC THANH TOÁN')}}
                        </h2>

                    </div>
                </div>
                <div class="m-portlet__head-tools">
                </div>
                {{-- Begin: Tab--------------- --}}
                <div class="d-flex ml-auto">
                    <div class="m-portlet__head-caption p-3">
                        <div class="m-portlet__head-title">

                            <a href="{{route('admin.stock.list')}}" class="m-portlet__head-text">
                                THÔNG TIN
                            </a>

                        </div>
                    </div>
                    <div class="m-portlet__head-caption p-3 ">
                        <div class="m-portlet__head-title ">

                            <a href="{{route('admin.stock-publish.list')}}" class="m-portlet__head-text  ">
                                PHÁT HÀNH
                            </a>

                        </div>
                    </div>
                    <div class="m-portlet__head-caption p-3  color_button">
                        <div class="m-portlet__head-title" >
                            <a style="color:#fff !important;" href="{{ route('admin.stock.config-bonus.order') }}" class="m-portlet__head-text">
                                THƯỞNG
                            </a>

                        </div>

                    </div>
                </div>
            </div>
            <div class="m-portlet__head-tools nt-class">
            </div>
            <div class="card-header tab-card-header ">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">



                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.stock.config-bonus.order')}}">Số lượng trong đơn hàng</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active show" href="{{route('admin.stock.config-bonus.payment')}}">Phương thức thanh toán</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.stock.config-bonus.transfer')}}">Gói đầu tư chuyển đổi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.stock.config-bonus.referral')}}">Cho người giới thiệu</a>
                    </li>
                </ul>
            </div>
            <div class="m-portlet__body">
                <div class="table-content m--padding-top-30 ">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group m-form__group d-flex">
                                <label class="black-title  d-inline align-self-center mb-0">Trạng thái:</label>
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm d-inline">
                                        <label style="margin: 0 0 0 10px; padding-top: 4px">
                                            <input @if($stockBonusConfig->stock_payment_method_bonus==1) checked @endif disabled="" name="is_active" type="checkbox" class="manager-btn">
                                            <span></span>
                                        </label>
                                    </span>
                            </div>

                        </div>
                        <div class="col-6">

                            <div class="form-group m-form__group ">
                                <label class="black-title ">Tiền mặt (%)</label>
{{--                                {{dd($stockBonusPayment->cash_rate)}}--}}
                                <input type="hidden" name="stock_id" value="{{number_format($stockBonusPayment->cash_rate,2)}}">
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stockBonusPayment->cash_rate)}}" placeholder="" readonly />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Ví cổ phiếu(%):</label>
                                {{-- {{dd($stockBonusPayment->stock_config_id())}} --}}
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stockBonusPayment->stock_rate,2)}}" placeholder="" readonly />
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="form-group m-form__group ">
                                <label class="black-title">Chuyển khoản (%)</label>
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stockBonusPayment->transfer_rate,2)}}" placeholder="" readonly />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title  w-100">Ví cổ tức(%)</label>
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stockBonusPayment->dividend_rate,2)}}" placeholder="" readonly />
                            </div>


                        </div>
                    </div>


                </div>
                <div class="form-group m-form__group text-right">
                    {{--                <a href="{{route('admin.stock.getCost')}}" class="btn  btn-metal">Hủy <i class="fa fa-arrow-left ic-search m--margin-left-5"></i></a>--}}
                    <a href="{{route('admin.stock.config-bonus.payment.edit')}}" class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>CHỈNH SỬA</span>
                                </span>
                    </a>
                    {{--                <a href="" class="btn btn-primary color_button btn-search">Chỉnh sửa thông tin</a>--}}
                </div>

            </div>
        </div>
        @include('admin::stock.popup.popup_publish')

        @endsection
        @section("after_style")
            {{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
            <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
            <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
            {{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
        @stop
        @section('after_script')
            <script src="{{asset('static/backend/js/admin/stock/stock-publish.js?v='.time())}}" type="text/javascript"></script>
            <script>
                $(document).ready(function(){
                    CustomerContractHandler.init();
                    new AutoNumeric.multiple('.number-money', {
                        currencySymbol: '',
                        decimalCharacter: '.',
                        digitGroupSeparator: ',',
                        decimalPlaces: 2
                    });
                });
                var CustomerContractHandler = {
                    customer_contract_id:"",
                    init(){
                        this.onBtnEndContract();

                    },
                    onBtnEndContract(){
                        $(".btnEndContract").on("click",function(){
                            CustomerContractHandler.customer_contract_id = $(this).attr("data-id");
                            Swal.fire({
                                title: "{{__('Bạn chắc chắn chứ?')}}",
                                text: "{{__('Bạn sẽ không thể hoàn nguyên điều này!')}}",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: "{{__('Đồng ý')}}"
                            }).then((result) => {
                                if(result.value){
                                    CustomerContractHandler.handleEndContract();
                                }
                            })
                        });
                    },
                    handleEndContract(){


                        $.ajax({
                            url: laroute.route('admin.customer-contract.endContract'),
                            method: 'POST',
                            dataType: 'JSON',
                            data: {
                                customer_contract_id:parseInt(CustomerContractHandler.customer_contract_id)
                            },
                            success:function (res) {
                                swal(
                                    "{{__('Hợp đồng đã kết thúc')}}",
                                    '',
                                    'success'
                                ).then(function(v){
                                    window.location.reload();
                                });

                            }
                        });


                    },



                }// end Customer Contract Handler

            </script>
            <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script>
            <script>
                $(".m_selectpicker").selectpicker();
            </script>
    {{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop