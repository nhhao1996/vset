<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{ __('Từ') }}</th>
            <th class="ss--font-size-th">{{ __('Đến') }}</th>
            <th class="ss--font-size-th">{{ __('CP thưởng') }}</th>
            <th class="ss--font-size-th">{{ __('Hành động') }}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            <?php $index=0 ?>
            @foreach ($LIST as $key => $item)
                <?php $index++ ?>
                <tr class="ss--font-size-13 ss--nowrap">
                    @if (isset($page))
                        <td>{{ ($page - 1) * 10 + $key + 1 }}</td>
                    @else
                        <td>{{ $key + 1 }}</td>
                    @endif
                    <td> {{number_format($item->stock_from,0,'.',',')}}</td>
                    <td class="stock_to"> {{number_format($item->stock_to,0,'.',',')}}</td>
                    <td>{{ number_format($item->rate, 2) }}%</td>

                    <td class="text-left">
                        {{-- @if (in_array('admin.stock-publish.stock-bonus', session('routeList'))) --}}
                        <a data-toogle="modal" data-target="#modalEdit" onclick="BonusReferalHandler.showPopupEdit('{{$item->stock_from}}','{{$item->stock_to}}','{{$item->rate}}','{{$item->stock_bonus_refer_id}}')" data-toggle="modal" data-target="#modalEdit" href="javascript:void(0)"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           href="#" title="Chỉnh sửa">
                            <i class="la la-edit"></i>
                        </a>
                        @if($item->stock_bonus_refer_id==$lastBonus->stock_bonus_refer_id)
                            <a onclick="BonusReferalHandler.onDelete({{$item->stock_bonus_refer_id}})"  href="javascript:void(0)"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               href="#" title="Xóa">
                                <i class="la la-remove"></i>
                            </a>
                        @endif
                        {{-- @endif --}}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
