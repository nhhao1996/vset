<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>Cấu hình thưởng theo số lượng trong đơn hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frmAdd" autocomplete="off">
                    <div class="form-group m-form__group row">
                        <label class="black-title col-2 d-flex align-items-center">Từ</label>
                        <input type="text" id="stock1" name="stock_from" class="form-control col-3 number-int" placeholder="Từ" value="">
                        <span class="col-1 d-flex align-items-center">Đến</span>
                        <input placeholder="Đến" type="text" id="stock2" name="stock_to" class="form-control col-3 float-right number-int" value="">

                    </div>
                    <div class="form-group m-form__group row">
                        <label class="black-title col-2 d-flex align-items-center">Tỷ lệ thưởng</label>
                        <input type="text" id="rate" name="rate" class="form-control col-3 number-percent" placeholder="Tỷ lệ thưởng" value="">


                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>Hủy</span>
                            </span>
                        </button>
                        <button  onclick="BonusHandler.add('path')" type="button"  class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>Lưu</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
