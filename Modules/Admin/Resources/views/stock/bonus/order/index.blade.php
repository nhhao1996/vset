@extends('layout')
@section('title_header')
    <span class="title_header"><img src="{{ asset('uploads/admin/icon/icon-member.png') }}" alt="" style="height: 20px;">
        {{ __('QUẢN LÝ CỔ PHIẾU') }}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }

    </style>
    {{-- @include('admin::customer.active-sv-card') --}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{ __('CẤU HÌNH THƯỞNG CỔ PHIẾU THEO SỐ LƯỢNG TRONG ĐƠN HÀNG') }}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
            {{-- Begin: Tab--------------- --}}
            @include("admin::stock.include.top-nav")
            {{-- end: Tab --}}

        </div>
        <div class="card-header tab-card-header ">
            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">



                <li class="nav-item">
                    <a class="nav-link active show" href="{{route('admin.stock.config-bonus.order')}}">Số lượng trong đơn hàng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.stock.config-bonus.payment')}}">Phương thức thanh toán</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.stock.config-bonus.transfer')}}">Gói đầu tư chuyển đổi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.stock.config-bonus.referral')}}">Cho người giới thiệu</a>
                </li>
            </ul>
        </div>
        <div class="m-portlet__head">

            <div class="m-portlet__head-tools">
                <label class="my-0 font-weight-bold">Trạng thái: </label>
            <div class="form-group m-form__group my-0">
                <span class="m-switch m-switch--icon m-switch--success m-switch--sm ">
                                    <label style="margin: 0 0 0 10px; padding-top: 4px">
                                        <input @if ($stockBonusConfig->stock_amount_bonus==1) checked @endif  name="is_active" type="checkbox" lass="manager-btn">
                                        <input type="hidden" name="stock_bonus_config_id" value="stock_amount_bonus"">

                                        <span></span>
                                    </label>
                                </span>
            </div>
            </div>
            <div class="m-portlet__head-tools">

{{--                    <label class="black-title font-weight-bold w-100">Trạng thái:</label>--}}
{{--                    <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                    <label style="">--}}
{{--                                        <input  name="is_active" type="checkbox" @if($stock->is_active==1) checked @endif  class="manager-btn">--}}

{{--                                        <span></span>--}}
{{--                                    </label>--}}
{{--                                </span>--}}

                <a type="button" onclick="BonusHandler.showPopupAdd()" data-toggle="modal" data-target="#modal" href="javascript:void(0)" class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-2">
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span>Thêm</span>
                        </span>
                </a>









            </div>

        </div>
        <div class="m-portlet__body">
            <div class="table-content m--padding-top-30 ">
                @include('admin::stock.bonus.order.list')
            </div>

{{--            <div class="table-content m--padding-top-30 ">--}}
{{--                <div class="form-group m-form__group w-50">--}}
{{--                    <label class="black-title">Tên cổ  phiếu:</label>--}}
{{--                    <input class="form-control bg-secondary" placeholder="{{ $stock->stock_config_code }}" readonly />--}}
{{--                </div>--}}

{{--            </div>--}}
{{--            <div class="form-group m-form__group">--}}
{{--                <label class="black-title">Tài liệu liên quan:</label>--}}
{{--                @include('admin::stock.list')--}}

{{--            </div><!-- end table-content -->--}}
{{--            <div class="form-group m-form__group text-right">--}}
{{--                <a href="{{ route('admin.stock.edit', 1) }}" class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">--}}
{{--                                <span class="ss--text-btn-mobi">--}}
{{--                                <i class="la la-check"></i>--}}
{{--                                <span>CHỈNH SỬA</span>--}}
{{--                                </span>--}}
{{--                </a>--}}

{{--                <a href="{{ route('admin.stock.edit', 1) }}" class="btn btn-primary color_button btn-search">CHỈNH SỬA THÔNG TIN</a>--}}
{{--            </div>--}}

        </div>
        @include('admin::stock.bonus.order.popup.add')
        @include('admin::stock.bonus.order.popup.edit')
    </div>
@endsection
@section('after_style')
    {{-- <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}"> --}}
    <link rel="stylesheet" href="{{ asset('static/backend/css/sinh-custom.css') }}">
    <link rel="stylesheet" href="{{ asset('static/backend/css/customize.css') }}">
    {{-- <link rel="stylesheet" href="{{asset('css/lightbox.css')}}"> --}}
@stop
@section('after_script')
    <script>
        $('#autotable').PioTable({
            baseUrl: laroute.route('admin.stock.config-bonus.order.list')
        });
        var $lastBonus =@json($lastBonus);
    </script>
    <script src="{{ asset('static/backend/js/admin/stock/bonus/script.js?v=' . time()) }}" type="text/javascript">
    </script>
    <script>
        $(".m_selectpicker").selectpicker();

    </script>
    {{-- <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script> --}}
@stop
