@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('CẤU HÌNH CHI PHÍ')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
    {{--    @include('admin::customer.active-sv-card')--}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet m-portlet--head-sm">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                        <h2 class="m-portlet__head-text">
                            {{__('CẤU HÌNH CHI PHÍ')}}
                        </h2>

                    </div>
                </div>
                <div class="m-portlet__head-tools">
                </div>
                {{-- Begin: Tab--------------- --}}
            </div>
            <div class="m-portlet__head-tools nt-class">
            </div>

            <div class="m-portlet__body">
                <div class="table-content m--padding-top-30 ">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí bán cổ phiếu qua chợ (%)</label>

                                <input type="hidden" name="stock_id" value="{{number_format($stock->stock_config_id,2)}}">
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stock->fee_stock_sell_market,2)}}" placeholder="" readonly />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí rút tiền cổ phiếu(%):</label>
                                {{-- {{dd($stock->stock_config_id())}} --}}
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stock->fee_withraw_stock,2)}}" placeholder="" readonly />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí chuyển đổi hợp đồng hợp tác đầu tư sang cổ phiếu (%):</label>
                                {{-- {{dd($stock->stock_config_id())}} --}}
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stock->fee_convert_bond_stock,2)}}" placeholder="" readonly />
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="form-group m-form__group ">
                                <label class="black-title">Phí chuyển nhượng cổ phiếu (%)</label>
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stock->fee_transfer_stock,2)}}" placeholder="" readonly />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title  w-100">Phí rút tiền ví cổ tức (%)</label>
                                <input  class="form-control bg-secondary number-money" value="{{number_format($stock->fee_withraw_dividend,2)}}" placeholder="" readonly />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí chuyển đổi hợp đồng tiết kiệm sang cổ phiếu (%)</label>
                                <input value="{{number_format($stock->fee_convert_saving_stock,2)}}"  class="number-money form-control bg-secondary" placeholder="" readonly />
                            </div>

                        </div>
                    </div>


                </div>
                <div class="form-group m-form__group text-right">
                    {{--                <a href="{{route('admin.stock.getCost')}}" class="btn  btn-metal">Hủy <i class="fa fa-arrow-left ic-search m--margin-left-5"></i></a>--}}
                    <a href="{{route('admin.stock.editCost')}}" class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>CHỈNH SỬA</span>
                                </span>
                    </a>
                    {{--                <a href="" class="btn btn-primary color_button btn-search">Chỉnh sửa thông tin</a>--}}
                </div>

            </div>
        </div>
        @include('admin::stock.popup.popup_publish')

        @endsection
        @section("after_style")
            {{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
            <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
            <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
            {{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
        @stop
        @section('after_script')
            <script src="{{asset('static/backend/js/admin/stock/stock-publish.js?v='.time())}}" type="text/javascript"></script>
            <script>
                $(document).ready(function(){
                    CustomerContractHandler.init();
                    new AutoNumeric.multiple('.number-money', {
                        currencySymbol: '',
                        decimalCharacter: '.',
                        digitGroupSeparator: ',',
                        decimalPlaces: 2
                    });
                });
                var CustomerContractHandler = {
                    customer_contract_id:"",
                    init(){
                        this.onBtnEndContract();

                    },
                    onBtnEndContract(){
                        $(".btnEndContract").on("click",function(){
                            CustomerContractHandler.customer_contract_id = $(this).attr("data-id");
                            Swal.fire({
                                title: "{{__('Bạn chắc chắn chứ?')}}",
                                text: "{{__('Bạn sẽ không thể hoàn nguyên điều này!')}}",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: "{{__('Đồng ý')}}"
                            }).then((result) => {
                                if(result.value){
                                    CustomerContractHandler.handleEndContract();
                                }
                            })
                        });
                    },
                    handleEndContract(){


                        $.ajax({
                            url: laroute.route('admin.customer-contract.endContract'),
                            method: 'POST',
                            dataType: 'JSON',
                            data: {
                                customer_contract_id:parseInt(CustomerContractHandler.customer_contract_id)
                            },
                            success:function (res) {
                                swal(
                                    "{{__('Hợp đồng đã kết thúc')}}",
                                    '',
                                    'success'
                                ).then(function(v){
                                    window.location.reload();
                                });

                            }
                        });


                    },



                }// end Customer Contract Handler

            </script>
            <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script>
            <script>
                $(".m_selectpicker").selectpicker();
            </script>
    {{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop