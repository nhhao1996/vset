@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-member.png')}}" alt=""
                style="height: 20px;"> {{__('CẤU HÌNH CHI PHÍ')}}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }
    </style>
{{--    @include('admin::customer.active-sv-card')--}}
<div class="m-portlet m-portlet--head-sm" id="autotable">
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                         <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CẤU HÌNH CHI PHÍ')}}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
            {{-- Begin: Tab--------------- --}}
        </div>



        <div class="m-portlet__body">
            <form id="frm">
                <div class="table-content m--padding-top-30 ">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí bán cổ phiếu qua chợ (%)</label>

                                <input type="hidden" name="stock_config_id" value="{{number_format($stock->stock_config_id,2)}}">
                                <input name="fee_stock_sell_market"  class="form-control number-percent" value="{{number_format($stock->fee_stock_sell_market,2)}}" placeholder=""  />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí rút tiền cổ phiếu(%):</label>
                                <input name="fee_withraw_stock"  class="form-control number-percent" value="{{number_format($stock->fee_withraw_stock,2)}}" placeholder=""  />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí chuyển đổi hợp đồng hợp tác đầu tư sang cổ phiếu (%):</label>
                                <input name="fee_convert_bond_stock"  class="form-control number-percent" value="{{number_format($stock->fee_convert_bond_stock,2)}}" placeholder=""  />
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="form-group m-form__group ">
                                <label class="black-title">Phí chuyển nhượng cổ phiếu (%)</label>
                                <input name="fee_transfer_stock"  class="form-control number-percent" value="{{number_format($stock->fee_transfer_stock,2)}}" placeholder=""  />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title  w-100">Phí rút tiền ví cổ tức (%)</label>
                                <input name="fee_withraw_dividend"  class="form-control number-percent" value="{{number_format($stock->fee_withraw_dividend,2)}}" placeholder=""  />
                            </div>
                            <div class="form-group m-form__group ">
                                <label class="black-title ">Phí chuyển đổi hợp đồng tiết kiệm sang cổ phiếu (%)</label>
                                <input name="fee_convert_saving_stock" value="{{number_format($stock->fee_convert_saving_stock,2)}}"  class="form-control number-percent" placeholder=""  />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="form-group m-form__group text-right">
                <a href="{{route('admin.stock.getCost')}}" class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                    <span class="ss--text-btn-mobi">
                    <i class="la la-arrow-left"></i>
                    <span>HỦY</span>
                    </span>
                </a>
                <button id="btnSave"   type="button"    class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-check"></i>
                        <span>LƯU LẠI</span>
                        </span>
                </button>
                {{-- <a href="{{route('admin.stock.getCost')}}" class="btn  btn-metal">Hủy <i class="fa fa-arrow-left ic-search m--margin-left-5"></i></a>
                <a id="btnSave" href="javascript:void(0)" class="btn btn-primary color_button btn-search">Lưu</a> --}}
            </div>

        </div>

    </div>
</div>


@endsection
@section("after_style")
{{--    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">--}}
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/lightbox.css')}}">--}}
@stop
@section('after_script')
<script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
<script src="{{asset('static/backend/js/admin/stock/edit-cost.js?v='.time())}}" type="text/javascript"></script>
    <script>

    </script>
    <script src="{{asset('static/backend/js/admin/customer-contract/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(".m_selectpicker").selectpicker();
    </script>
{{--    <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script>--}}
@stop