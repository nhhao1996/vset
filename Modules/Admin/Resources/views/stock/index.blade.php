@extends('layout')
@section('title_header')
    <span class="title_header"><img src="{{ asset('uploads/admin/icon/icon-member.png') }}" alt="" style="height: 20px;">
        {{ __('QUẢN LÝ CỔ PHIẾU') }}</span>
@stop
@section('content')

    <style>
        .form-control-feedback {
            color: red;
        }

    </style>
    {{-- @include('admin::customer.active-sv-card') --}}
    <div class="m-portlet m-portlet--head-sm" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        {{ __('THÔNG TIN CỔ PHIẾU') }}
                    </h2>

                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
          @include("admin::stock.include.top-nav")

        </div>

        <div class="m-portlet__body">

            <div class="table-content m--padding-top-30 ">
                <div class="form-group m-form__group w-50">
                    <label class="black-title">Tên cổ  phiếu:</label>
                    <input class="form-control bg-secondary" placeholder="{{ $stock->stock_config_code }}" readonly />
                </div>

            </div>
            <div class="form-group m-form__group">
                <label class="black-title">Tài liệu liên quan:</label>
                @include('admin::stock.list')

            </div><!-- end table-content -->
            <div class="form-group m-form__group text-right">
                <a href="{{ route('admin.stock.edit', 1) }}" class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>CHỈNH SỬA</span>
                                </span>
                </a>

{{--                <a href="{{ route('admin.stock.edit', 1) }}" class="btn btn-primary color_button btn-search">CHỈNH SỬA THÔNG TIN</a>--}}
            </div>

        </div>
    </div>
@endsection
@section('after_style')
    {{-- <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}"> --}}
    <link rel="stylesheet" href="{{ asset('static/backend/css/sinh-custom.css') }}">
    <link rel="stylesheet" href="{{ asset('static/backend/css/customize.css') }}">
    {{-- <link rel="stylesheet" href="{{asset('css/lightbox.css')}}"> --}}
@stop
@section('after_script')
    <script>
        $(document).ready(function() {
            CustomerContractHandler.init();
        });
        var CustomerContractHandler = {
            customer_contract_id: "",
            init() {
                this.onBtnEndContract();

            },
            onBtnEndContract() {
                $(".btnEndContract").on("click", function() {
                    CustomerContractHandler.customer_contract_id = $(this).attr("data-id");
                    Swal.fire({
                        title: "{{ __('Bạn chắc chắn chứ?') }}",
                        text: "{{ __('Bạn sẽ không thể hoàn nguyên điều này!') }}",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: "{{ __('Đồng ý') }}"
                    }).then((result) => {
                        if (result.value) {
                            CustomerContractHandler.handleEndContract();
                        }
                        // if (result.isConfirmed) {
                        //     alert("hello");
                        //     CustomerContractHandler.handleEndContract();
                        //     alert('da ket thuc hop dong');
                        //     return;
                        //     Swal.fire(
                        //     'Deleted!',
                        //     'Your file has been deleted.',
                        //     'success'
                        //     )
                        // }
                    })
                });
            },
            handleEndContract() {


                $.ajax({
                    url: laroute.route('admin.customer-contract.endContract'),
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        customer_contract_id: parseInt(CustomerContractHandler.customer_contract_id)
                    },
                    success: function(res) {
                        swal(
                            "{{ __('Hợp đồng đã kết thúc') }}",
                            '',
                            'success'
                        ).then(function(v) {
                            window.location.reload();
                        });

                    }
                });


            },



        } // end Customer Contract Handler

    </script>
    <script src="{{ asset('static/backend/js/admin/customer-contract/script.js?v=' . time()) }}" type="text/javascript">
    </script>
    <script>
        $(".m_selectpicker").selectpicker();

    </script>
    {{-- <script src="{{asset('js/lightbox.js?v='.time())}}" type="text/javascript"></script> --}}
@stop
