<div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>UPLOAD FILE TÀI LIỆU LIÊN QUAN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="list-file-doc-related">
                    {{ csrf_field() }}
                    <input type="hidden" name="stock_config_id" id="stock_config_id"  value="{{$stock->stock_config_id}}">
                    {{-- <input type="hidden" id="stock_code" name="stock_code" value="{{$stock->stock_code}}"> --}}
                    <div class="form-group noselector cash">
                        <div class="form-group m-form__group pt-3">
                            <label>{{__('File tài liệu liên quan')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('admin.stock.uploadRelatedDoc')}}" id="dropzoneonecash">
                            <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('File')}}
                                    </h3>
                                    <span>{{__('Chọn file tài liệu liên quan')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image-cash"></div>
                    </div>
                    {{-- <input type="hidden" id="appendix_contract_id" name="appendix_contract_id" value=""> --}}
                </form>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                    <div class="m-form__actions m--align-right">
                        <button data-dismiss="modal" class="btn btn-metal son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>HỦY</span>
                            </span>
                        </button>
                        <button id="btnSaveFile" type="button"  class="btn btn-primary color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md btn-edit-customer m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                                <i class="la la-check"></i>
                                <span>LƯU THÔNG TIN</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>