<div class="modal fade" id="modal-stock-publish-history" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i
                        class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>Phát hành cổ phiếu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body m-content">
                <div class="m-portlet" id="autotable-history">
                    <div class="m-portlet__body pb-0 px-0">
                        <form class="frmFilter ">
                            <div class="row ss--bao-filter">

                                <div class="col-lg-12 form-group pr-0">
                                    <div class="form-group m-form__group">
                                        <a style="font-size:1.1rem" id="btnExportStockPublish" href="{{ route('admin.stock-publish.export') }}"                                            class="btn btn-info btn-sm m-btn m-btn--icon m-btn--pill color_button mr-0 float-right">
                                            <span>
                                                <span> {{ __('Export') }}</span>
                                            </span>
                                        </a>

                                    </div>

                                </div>
                            </div>
                            <div class="row ss--bao-filter pt-0">

                                <div class="col-lg-6 form-group position-relative pl-0">
                                    <input onchange="StockPublishRespository.handlechangePublishDay(this)" type="text" class="form-control m-input daterange-picker created_at" id="publish_at"
                                        name="publish_at" autocomplete="off"
                                        placeholder="{{ __('Chọn ngày phát hành') }}">
                                    <span style="left:86%;top:22%"
                                        class="m-input-icon__icon m-input-icon__icon--right position-absolute">
                                        <span><i class="la la-calendar"></i></span></span>
                                </div>
                                <div class="col-lg-3 form-group">
                                    <div class="form-group m-form__group">
                                        <span onclick="StockPublishRespository.search()" class="btn ss--btn-search ">
                                            TÌM KIẾM
                                            <i class="fa fa-search ss--icon-search"></i>
                                        </span>

                                    </div>
                                </div>
                            </div>

                    </div>
                    </form>
                    <div class="table-content table-history ">
                        @include("admin::stock.stock-publish.list")

                    </div><!-- end table-content -->
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">

            </div>
        </div>
    </div>
</div>

