  {{-- Begin: Tab--------------- --}}
            <div class="d-flex ml-auto">
                <div class="m-portlet__head-caption p-3 @if(isset($isStockInfo)) color_button @endif">
                    <div class="m-portlet__head-title">

                        <a @if(isset($isStockInfo))style="color:#fff !important" @endif href="{{route('admin.stock.list')}}" class="m-portlet__head-text">
                            THÔNG TIN
                        </a>

                    </div>
                </div>
                <div class="m-portlet__head-caption p-3">
                    <div class="m-portlet__head-title">

                        <a href="{{ route('admin.stock-publish.list') }}" class="m-portlet__head-text">
                            PHÁT HÀNH
                        </a>

                    </div>
                </div>
                <div class="m-portlet__head-caption p-3 @if(isset($isBonus)) color_button @endif">
                    <div class="m-portlet__head-title">
                        <a @if(isset($isBonus)) style="color:#fff !important;" @endif href="{{ route('admin.stock.config-bonus.order') }}" class="m-portlet__head-text">
                            THƯỞNG
                        </a>

                    </div>
                </div>
            </div>
            {{-- end: Tab --}}