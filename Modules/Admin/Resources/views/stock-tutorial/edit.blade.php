@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ HƯỚNG DẪN')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('CHỈNH SỬA HƯỚNG DẪN')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
{{--                @if(in_array('admin.stock-news.create',session('routeList')))--}}
{{--                    <a href="{{route('admin.stock-news.create')}}"--}}
{{--                       class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">--}}
{{--                        <span>--}}
{{--						    <i class="fa fa-plus-circle m--margin-right-5"></i>--}}
{{--							<span> {{__('CHỈNH SỬA TIN TỨC')}}</span>--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                @endif--}}
            </div>
        </div>
        <div class="m-portlet__body">

            <form id="stock-tutorial">
                <div class="table-content m--padding-top-30">
                <textarea id="stock_tutorial_content" name="stock_tutorial_content" type="text" class="form-control m-input class"
                          placeholder="{{__('Thông tin hướng dẫn')}}"
                          aria-describedby="basic-addon1">{!! $detail['stock_tutorial_content'] !!}</textarea>
                </div><!-- end table-content -->
                <input type="hidden" name="stock_tutorial_key" value="tutorial">
            </form>
        </div>
        <div class="modal-footer save-attribute m--margin-right-20">
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                <div class="m-form__actions m--align-right">
                    <a href="{{route('admin.stock-tutorial')}}"
                       class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                        <span class="ss--text-btn-mobi">
                        <i class="la la-arrow-left"></i>
                        <span>{{__('HỦY')}}</span>
                        </span>
                    </a>
                    <button onclick="stockTutorial.update()" type="button"
                            class="ss--btn-mobiles m--margin-bottom-5 btn ss--button-cms-piospa ss--btn m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span class="ss--text-btn-mobi">
                            <i class="la la-check"></i>
                            <span>{{__('LƯU THÔNG TIN')}}</span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="append-form-category"></div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/stock-tutorial/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#stock_tutorial_content').summernote({
                height: 400,
                lang: 'vi-VN',
                placeholder: 'Nhập nội dung...',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'picture','file']]
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        for(let i=0; i < files.length; i++) {
                            uploadImgCk(files[i],'vi');
                        }
                    }
                },

            });
        });
    </script>
@stop
