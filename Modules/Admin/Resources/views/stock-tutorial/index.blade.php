@extends('layout')
@section("after_style")
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/sinh-custom.css')}}">
@endsection
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-product.png')}}" alt="" style="height: 20px;">
        {{__('QUẢN LÝ HƯỚNG DẪN')}}
    </span>
@endsection
@section('content')
    <meta http-equiv="refresh" content="number">
    <style>
        .modal-backdrop {
            position: relative !important;
        }
    </style>
    <div class="m-portlet" id="autotable">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                        <i class="la la-th-list"></i>
                     </span>
                    <h3 class="m-portlet__head-text">
                        {{__('THÔNG TIN HƯỚNG DẪN')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                @if(in_array('admin.stock-tutorial.edit',session('routeList')))
                    <a href="{{route('admin.stock-tutorial.edit')}}"
                       class="btn ss--button-cms-piospa m-btn m-btn--icon m-btn--pill btn_add_pc btn-sm">
                        <span>
						    <i class="fa fa-plus-circle m--margin-right-5"></i>
							<span> {{__('CHỈNH SỬA HƯỚNG DẪN')}}</span>
                        </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="m-portlet__body">

            <div class="table-content m--padding-top-30">
                {!! $detail['stock_tutorial_content'] !!}
            </div><!-- end table-content -->
        </div>
    </div>
    <div class="append-form-category"></div>
@endsection
@section('after_script')
    <script src="{{asset('static/backend/js/admin/stock-tutorial/script.js?v='.time())}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#stock_tutorial_content').summernote({
                height: 400,
                lang: 'vi-VN',
                placeholder: 'Nhập nội dung...',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'picture','file']]
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        for(let i=0; i < files.length; i++) {
                            uploadImgCk(files[i],'vi');
                        }
                    }
                },

            });
        });
    </script>
@stop
