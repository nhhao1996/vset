<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table">
        <thead>
        <tr class="ss--nowrap">
            <th class="ss--font-size-th">#</th>
            <th class="ss--font-size-th">{{__('Tên ngân hàng')}}</th>
            <th class="ss--font-size-th">{{__('Logo')}}</th>
            <th class="ss--font-size-th">{{__('Hành động')}}</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($LIST))
            @foreach ($LIST as $key => $item)
                <tr class="ss--font-size-13 ss--nowrap">
                    @if(isset($page))
                        <td>{{ (($page-1)*10 + $key + 1) }}</td>
                    @else
                        <td>{{ ($key + 1) }}</td>
                    @endif
                    <td>{{$item['bank_name']}}</td>
                    <td><img class="img-fluid" width="100px" src="{{$item['bank_icon'] != null ? $item['bank_icon'] : asset('/static/backend/images/no-image-product.png')}}"></td>

                    <td>
{{--                        <a href="{{route('admin.bank.show',['id'=>$item['bank_id']])}}"--}}
                        @if(in_array('admin.bank.detail-form-bank', session('routeList')))
                            <a href="javascript:void(0)" onclick="bank.detailBank({{$item['bank_id']}})"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="la la-eye"></i>
                            </a>
                        @endif
                        @if(in_array('admin.bank.edit-form-bank', session('routeList')))
                            <a href="javascript:void(0)" onclick="bank.editBank({{$item['bank_id']}})"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Cập nhật')}}">
                                <i class="la la-edit"></i>
                            </a>
                        @endif
                        @if(in_array('admin.bank.remove', session('routeList')))
                            <button type="button" onclick="bank.remove(this, {{$item['bank_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="{{__('Xoá')}}">
                                <i class="la la-trash"></i>
                            </button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
{{--.--}}