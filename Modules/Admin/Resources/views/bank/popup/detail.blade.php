<div class="modal fade" id="bank_detail_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ss--title m--font-bold"><i class="fa fa-plus-circle ss--icon-title m--margin-right-5">
                    </i>{{_('Chi tiết ngân hàng')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-bank" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group m-form__group">
                                <label>
                                    {{__('Tên ngân hàng')}}: <b class="text-danger">*</b>
                                </label>
                                <div class="input-group">
                                    <input id="product-code" disabled type="text" class="form-control m-input class"
                                           placeholder="{{__('Tên ngân hàng')}}"
                                           aria-describedby="basic-addon1" value="{{$detail['bank_name']}}">
                                </div>
                                <span class="errs error-product-code"></span>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="row form-group">
                                <label  class="col-form-label label col-lg-4 black-title">
                                    {{__('Logo Ngân hàng')}}:
                                </label>
                                <div class="col-lg-8">
                                    <input type="hidden" id="image_main_hidden" >
                                    <input type="hidden" id="image_main_upload" name="product_avatar"
                                           value="">
                                    <div class="m-widget19__pic">
                                        <img class="m--bg-metal img-sd" id="image_main"
                                             src="{{$detail['bank_icon'] == null ? asset('/static/backend/images/no-image-product.png') : asset($detail['bank_icon'])}}"
                                             alt="{{__('Hình ảnh')}}" width="155px" height="155px"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="save-attribute m--margin-right-20">
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit ss--width--100">
                        <div class="m-form__actions m--align-right">
                            <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close"
                               class="ss--btn-mobiles btn btn-metal m-btn m-btn--icon m-btn--wide m-btn--md ss--btn m--margin-bottom-5">
                                <span class="ss--text-btn-mobi">
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>