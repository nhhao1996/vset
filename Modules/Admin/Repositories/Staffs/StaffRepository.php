<?php
/**
 * Created by PhpStorm.
 * User: WAO
 * Date: 13/03/2018
 * Time: 1:48 CH
 */

namespace Modules\Admin\Repositories\Staffs;

use Modules\Admin\Models\RoleGroupTable;
use Modules\Admin\Models\StaffAccountTable;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Http\Api\JobsEmailLogApi;

class StaffRepository implements StaffRepositoryInterface
{

    /**
     * @var staffTable
     */
    protected $staff;
    protected $timestamps = true;
    protected $jobMail;
    protected $roleGroup;

    public function __construct(
        StaffsTable $staffs,
        JobsEmailLogApi $jobMail,
        RoleGroupTable $roleGroup
    ){
        $this->staff = $staffs;
        $this->jobMail = $jobMail;
        $this->roleGroup = $roleGroup;
    }
    /**
     *get list customer Group
     */
    public function list(array $filters = [])
    {
        $isGroup = $filters['is_group'] ?? 0;
        if (isset($filters['is_group'])) {
            unset($filters['is_group']);
        }
        $list = $this->staff->getList($filters);
        if (count($list) == 0){
            return $list;
        }
        if ($isGroup == 1) {
            return $list;
        }
        $arrId = collect($list->toArray()['data'])->pluck('staff_id');
//        lấy danh sách nhóm quyền
        $listRole = $this->roleGroup->getListByArrIdStaff($arrId);
        $groupBy = collect($listRole)->groupBy('staff_id');
        foreach ($list as $key => $value){
            if (isset($groupBy[$value['staff_id']])){
                $tmp = '';
                foreach ($groupBy[$value['staff_id']] as $keyItem => $item) {
                    if ($keyItem == 0) {
                        $tmp = $tmp.$item['name'];
                    } else {
                        $tmp = $tmp.','.$item['name'];
                    }
                }
                $value['role_name'] = $tmp;
            } else {
                $value['role_name'] = null;
            }
        }
        return $list;
    }
    /**
     * delete customer Group
     */
    public function remove($id)
    {
        $this->staff->remove($id);
    }

    /**
     * add customer Group
     * @param array $data
     * @param $password
     * @return mixed
     */
    public function add(array $data, $password)
    {

        $staff_id = $this->staff->add($data);

        $vars = [
            'username' => $data['user_name'],
            'password' => $password
        ];

        $arrInsertEmail = [
            'obj_id' => $staff_id,
            'email_type' => "create-staff",
            'email_subject' => __('Tạo mới nhân viên nhân viên'),
            'email_to' =>  $data['email'],
            'email_from' => env('MAIL_USERNAME'),
            'email_params' => json_encode($vars),
        ];
        $this->jobMail->addJob($arrInsertEmail);
//
         return $staff_id;
    }

    /*
     * edit customer Group
     */
    public function edit(array $data , $password,$id)
    {
         $this->staff->edit($data,$id);

         if ($password != null) {
             // gọi api lưu data vào jobs mail
             $detail = $this->getItem($id);
             $vars = [
                 'staff_name' => $data['full_name'],
                 'password' => $password,
             ];

             $arrInsertEmail = [
                 'obj_id' => $id,
                 'email_type' => "update-staff",
                 'email_subject' => __('Cập nhật thông tin nhân viên'),
                 'email_to' =>  $data['email'],
                 'email_from' => env('MAIL_USERNAME'),
                 'email_params' => json_encode($vars),
             ];
             $this->jobMail->addJob($arrInsertEmail);
         }

        return true;
    }

    /*
     *  update or add
     */
    public function getItem($id)
    {
        return $this->staff->getItem($id);
    }
    public function testUserName($userName, $id)
    {
        return $this->staff->testUserName($userName,$id);
    }

    /**
     * @return array|mixed
     */
    public function getStaffOption()
    {
        $array=array();
        foreach ($this->staff->getStaffOption() as $item)
        {
            $array[$item['staff_id']]=$item['full_name'];

        }
        return $array;
    }

    public function getStaffOptionWithMoney()
    {
        $array = [];
        foreach ($this->staff->getStaffOption() as $item)
        {
            $array[$item['staff_id']] = [
                'name' => $item['full_name'],
                'money' => 0
            ];

        }
        return $array;
    }

    /**
     * @return array|mixed
     */
    public function getStaffTechnician()
    {
        $array=array();
        foreach ($this->staff->getStaffTechnician() as $item)
        {
            $array[$item['staff_id']]=$item['full_name'];

        }
        return $array;
    }


}