<?php
/**
 * Created by PhpStorm.
 * User: Huy
 * Date: 11/20/2018
 * Time: 10:20 PM
 */

namespace Modules\Admin\Repositories\Order;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\DeliveryHistoryTable;
use Modules\Admin\Models\DeliveryTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\PromotionDetailTable;
use Modules\Admin\Models\PromotionObjectApplyTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReceiptTable;
use Modules\Admin\Models\PromotionDailyTimeTable;
use Modules\Admin\Models\PromotionDateTimeTable;
use Modules\Admin\Models\PromotionMonthlyTimeTable;
use Modules\Admin\Models\PromotionWeeklyTimeTable;

class OrderRepository implements OrderRepositoryInterface
{
    private $order;

    /**
     * OrderRepository constructor.
     * @param OrderTable $orders
     */
    public function __construct(OrderTable $orders)
    {
        $this->order = $orders;
    }

    /**
     * Danh sách đơn hàng
     *
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = [])
    {
        $list = $this->order->getList($filters);
        //Data Receipt
        $mReceipt = new ReceiptTable();
        $listReceipt = $mReceipt->getAllReceipt();

        $arrReceipt = [];
        foreach ($listReceipt as $item) {
            $arrReceipt[$item['order_id']] = [
                'order_id' => $item['order_id'],
                'amount_paid' => $item['amount_paid'],
                'note' => $item['note']
            ];
        }

        //Data Receipt Detail
//        $mReceiptDetail = new ReceiptDetailTable();
//        $listDetail = $mReceiptDetail->getAllReceiptDetail();
//        $arrDetail = [];
//        foreach ($listDetail as $item) {
//            $arrDetail[$item['receipt_id']] = $item['number'];
//        }
        return [
            'list' => $list,
            'receipt' => $arrReceipt,
//            'receiptDetail' => $arrDetail
        ];
    }

    public function listForContract(array $filters = []){
        $list = $this->order->getList($filters);

        return $list;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        return $this->order->add($data);
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function getItemDetail($id)
    {
        return $this->order->getItemDetail($id);
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        return $this->order->edit($data, $id);
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function remove($id)
    {
        $this->order->remove($id);
    }

    public function detailDayCustomer($id)
    {
        // TODO: Implement detailCustomer() method.
        return $this->order->detailDayCustomer($id);
    }

    public function detailCustomer($id)
    {
        return $this->order->detailCustomer($id);
    }

    public function getIndexReportRevenue()
    {
        $select = $this->order->getIndexReportRevenue();
        return $select;
    }

    public function getValueByYear($year, $startTime = null, $endTime = null)
    {
        return $this->order->getValueByYear($year, $startTime, $endTime);
    }

    public function getValueByDate($date, $field = null, $valueField = null, $field2 = null, $valueField2 = null)
    {
        $result = 0;
        $select = $this->order->getValueByDate($date, $field, $valueField, $field2, $valueField2);
        if ($select[0]['total'] != null) {
            $result = $select[0]['total'];
        }
        return $result;
    }

    //Lấy dữ liệu với tham số truyền vào(thời gian, cột)
    public function getValueByParameter($date, $filer, $valueFilter)
    {
        $result = 0;
        $select = $this->order->getValueByParameter($date, $filer, $valueFilter);
        if ($select[0]['total'] != null) {
            $result = $select[0]['total'];
        }
        return $result;
    }

    //Lấy giá trị từ ngày - đến ngày.
    public function getValueByDay($startTime, $endTime)
    {
        $result = 0;
        $select = $this->order->getValueByDay($startTime, $endTime);
        if ($select[0]['total'] != null) {
            $result = $select[0]['total'];
        }
        return $result;
    }

    //Lấy dữ liệu với tham số truyền vào(thời gian, cột) 2
    public function getValueByParameter2($startTime, $endTime, $filer, $valueFilter)
    {
        return $this->order->getValueByParameter2($startTime, $endTime, $filer, $valueFilter);
    }

    //Lấy giá trị theo năm, cột và giá trị cột truyền vào
    public function fetchValueByParameter($year, $startTime, $endTime, $field, $fieldValue)
    {
        return $this->order->fetchValueByParameter($year, $startTime, $endTime, $field, $fieldValue);
    }

    //Lấy giá trị theo năm, cột và giá trị 2 cột truyền vào
    public function fetchValueByParameter2($year, $startTime, $endTime, $field, $fieldValue, $field2, $fieldValue2)
    {
        return $this->order->fetchValueByParameter2($year, $startTime, $endTime, $field, $fieldValue, $field2, $fieldValue2);
    }


    public function getValueByDate2($date, $branch, $customer)
    {
        $result = 0;
        $select = $this->order->getValueByDate2($date, $branch, $customer);
        if ($select != null) {
            foreach ($select as $key => $value) {
                $result += $value['amount'];
            }
        }
        return $result;
    }

    //Lấy các giá trị theo created_at, branch_id và created_by
    public function getValueByDate3($date, $branch, $staff)
    {
        $result = 0;
        $select = $this->order->getValueByDate3($date, $branch, $staff);
        if ($select != null) {
            foreach ($select as $key => $value) {
                $result += $value['amount'];
            }
        }
        return $result;
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng.
    public function getDataReportGrowthByCustomer($year, $month, $operator, $customerOdd, $field = null, $valueField = null)
    {
        return $this->order->getDataReportGrowthByCustomer($year, $month, $operator, $customerOdd, $field, $valueField);
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng theo năm.
    public function getDataReportGrowthCustomerByYear($year, $operator, $customerOdd, $branch)
    {
        return $this->order->getDataReportGrowthCustomerByYear($year, $operator, $customerOdd, $branch);
    }

    //Thống kê tăng trưởng khách hàng(theo nhóm khách hàng).
    public function getValueReportGrowthByCustomerCustomerGroup($year, $branch = null)
    {
        return $this->order->getValueReportGrowthByCustomerCustomerGroup($year, $branch);
    }

    //Thống kê tăng trưởng khách hàng(theo nguồn khách hàng).
    public function getValueReportGrowthByCustomerCustomerSource($year, $branch = null)
    {
        return $this->order->getValueReportGrowthByCustomerCustomerSource($year, $branch);
    }

    //Thống kê tăng trưởng khách hàng(theo giới tính).
    public function getValueReportGrowthByCustomerCustomerGender($year, $branch = null)
    {
        return $this->order->getValueReportGrowthByCustomerCustomerGender($year, $branch);
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng(từ ngày đến ngày và/hoặc chi nhánh).
    public function getDataReportGrowthByCustomerDataBranch($startTime, $endTime, $operator, $customerOdd, $branch)
    {
        return $this->order->getDataReportGrowthByCustomerDataBranch($startTime, $endTime, $operator, $customerOdd, $branch);
    }

    //Thống kê tăng trưởng khách hàng(theo nhóm khách hàng) theo từ ngày đến ngày và/hoặc chi nhánh.
    public function getValueReportGrowthByCustomerCustomerGroupTimeBranch($startTime, $endTime, $branch = null)
    {
        return $this->order->getValueReportGrowthByCustomerCustomerGroupTimeBranch($startTime, $endTime, $branch);
    }

    //Thống kê tăng trưởng khách hàng(theo giới tính) theo từ ngày đến ngày và/hoặc chi nhánh.
    public function getValueReportGrowthByCustomerCustomerGenderTimeBranch($startTime, $endTime, $branch)
    {
        return $this->order->getValueReportGrowthByCustomerCustomerGenderTimeBranch($startTime, $endTime, $branch);
    }

    //Thống kê tăng trưởng khách hàng(theo nguồn khách hàng) theo từ ngày tới ngày và/hoặc chi nhánh.
    public function getValueReportGrowthByCustomerCustomerSourceTimeBranch($startTime, $endTime, $branch)
    {
        return $this->order->getValueReportGrowthByCustomerCustomerSourceTimeBranch($startTime, $endTime, $branch);
    }

    //Lấy dữ liệu theo năm/từ ngày đến ngày và chi nhánh
    public function getValueByYearAndBranch($year, $branch, $startTime = null, $endTime = null)
    {
        return $this->order->getValueByYearAndBranch($year, $branch, $startTime, $endTime);
    }

    //Lấy danh sách khách hàng cho tăng trưởng khách hàng theo năm cho từng chi nhánh.
    public function getDataReportGrowthCustomerByTime($startTime, $endTime, $operator, $customerOdd, $branch)
    {
        return $this->order->getDataReportGrowthCustomerByTime($startTime, $endTime, $operator, $customerOdd, $branch);
    }

    public function searchDashboard($keyword)
    {
        return $this->order->searchDashboard($keyword);
    }

    public function getAllByCondition($startTime, $endTime, $branch)
    {
        return $this->order->getAllByCondition($startTime, $endTime, $branch);
    }

    public function getCustomerDetail($id)
    {
        return $this->order->getCustomerDetail($id);
    }

    public function getValueByParameter3($startTime, $endTime, $filer, $valueFilter)
    {
        return $this->order->getValueByParameter3($startTime, $endTime, $filer, $valueFilter);
    }

    public function getValueByYear2($year, $startTime = null, $endTime = null)
    {
        return $this->order->getValueByYear2($year, $startTime, $endTime);
    }

    //Lấy giá trị theo năm, cột và giá trị cột truyền vào. Lấy tiền thanh toán trong receipt.
    public function fetchValueByParameter3(
        $year,
        $startTime,
        $endTime,
        $field,
        $fieldValue
    )
    {
        return $this->order->fetchValueByParameter3($year, $startTime, $endTime,
            $field, $fieldValue);
    }

    /**
     * Chuyển tiếp chi nhánh
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function applyBranch($input)
    {
        try {
            $this->order->edit([
                'branch_id' => $input['branch_id'],
                'is_apply' => 1
            ], $input['order_id']);

            return response()->json([
                'error' => false,
                'message' => 'Chuyển tiếp thành công'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Chuyển tiếp thất bại'
            ]);
        }
    }

    /**
     * Xóa đơn hàng cần giao
     *
     * @param $orderId
     * @return mixed|void
     */
    public function removeDelivery($orderId)
    {
        $mDelivery = new DeliveryTable();
        $mDeliveryHistory = new DeliveryHistoryTable();

        //Lấy thông tin giao hàng
        $info = $mDelivery->getInfo($orderId);

        if ($info != null) {
            //Xóa đơn hàng cần giao
            $mDelivery->edit([
                'delivery_status' => 'cancel'
            ], $orderId);
            //Xóa lịch sử giao hàng
            $mDeliveryHistory->removeAll($info['delivery_id']);
        }
    }

    /**
     * Lấy thông tin khuyến mãi của sp, dv, thẻ dv
     *
     * @param $objectType
     * @param $objectCode
     * @param $customerId
     * @param $orderSource
     * @param $promotionType
     * @param $quantity
     * @return mixed|void
     */
    public function getPromotionDetail($objectType, $objectCode, $customerId, $orderSource, $promotionType, $quantity = null)
    {
        $mPromotionDetail = new PromotionDetailTable();
        $mDaily = new PromotionDailyTimeTable();
        $mWeekly = new PromotionWeeklyTimeTable();
        $mMonthly = new PromotionMonthlyTimeTable();
        $mFromTo = new PromotionDateTimeTable();
        $mCustomer = new CustomerTable();
        $mPromotionApply = new PromotionObjectApplyTable();

        $currentDate = Carbon::now()->format('Y-m-d H:i');
        $currentTime = Carbon::now()->format('H:i');

        $price = null;
        $arrGift = [];

        $getDetail = $mPromotionDetail->getPromotionDetail($objectType, $objectCode, $promotionType);

        if ($getDetail != null) {
            //Check thời gian diễn ra chương trình
            if ($currentDate < $getDetail['start_date'] || $currentDate > $getDetail['end_date']) {
                if ($promotionType == 1) {
                    return $price;
                } else {
                    return $arrGift;
                }
            }
            //Check chi nhánh áp dụng
            if ($getDetail['branch_apply'] != 'all' &&
                !in_array(Auth()->user()->branch_id, explode(',', $getDetail['branch_apply']))) {
                if ($promotionType == 1) {
                    return $price;
                } else {
                    return $arrGift;
                }
            }
            //Check KM theo time đặc biệt
            if ($getDetail['is_time_campaign'] == 1) {
                switch ($getDetail['time_type']) {
                    case 'D':
                        $daily = $mDaily->getDailyByPromotion($getDetail['promotion_code']);

                        if ($daily != null) {
                            $startTime = Carbon::createFromFormat('H:i:s', $daily['start_time'])->format('H:i');
                            $endTime = Carbon::createFromFormat('H:i:s', $daily['end_time'])->format('H:i');
                            //Kiểm tra giờ bắt đầu, giờ kết thúc
                            if ($currentTime < $startTime || $currentTime > $endTime) {
                                if ($promotionType == 1) {
                                    return $price;
                                } else {
                                    return $arrGift;
                                }
                            }
                        }
                        break;
                    case 'W':
                        $weekly = $mWeekly->getWeeklyByPromotion($getDetail['promotion_code']);
                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['default_start_time'])->format('H:i');
                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['default_end_time'])->format('H:i');

                        switch (Carbon::now()->format('l')) {
                            case 'Monday':
                                if ($weekly['is_monday'] == 1) {
                                    if ($weekly['is_other_monday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_monday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_monday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                            case 'Tuesday':
                                if ($weekly['is_tuesday'] == 1) {
                                    if ($weekly['is_other_tuesday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_tuesday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_tuesday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                            case 'Wednesday':
                                if ($weekly['is_wednesday'] == 1) {
                                    if ($weekly['is_other_wednesday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_wednesday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_wednesday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                            case 'Thursday':
                                if ($weekly['is_thursday'] == 1) {
                                    if ($weekly['is_other_monday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_thursday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_thursday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                            case 'Friday':
                                if ($weekly['is_friday'] == 1) {
                                    if ($weekly['is_other_friday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_friday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_friday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                            case 'Saturday':
                                if ($weekly['is_saturday'] == 1) {
                                    if ($weekly['is_other_saturday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_saturday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_saturday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                            case 'Sunday':
                                if ($weekly['is_sunday'] == 1) {
                                    if ($weekly['is_other_sunday'] == 1) {
                                        $startTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_sunday_start_time'])->format('H:i');
                                        $endTime = Carbon::createFromFormat('H:i:s', $weekly['is_other_sunday_end_time'])->format('H:i');
                                    }
                                } else {
                                    if ($promotionType == 1) {
                                        return $price;
                                    } else {
                                        return $arrGift;
                                    }
                                }
                                break;
                        }
                        //Kiểm tra giờ bắt đầu, giờ kết thúc
                        if ($currentTime < $startTime || $currentTime > $endTime) {
                            if ($promotionType == 1) {
                                return $price;
                            } else {
                                return $arrGift;
                            }
                        }
                        break;
                    case 'M':
                        $monthly = $mMonthly->getMonthlyByPromotion($getDetail['promotion_code']);

                        if (count($monthly) > 0) {
                            $next = false;

                            foreach ($monthly as $v) {
                                $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $v['run_date'] . ' ' . $v['start_time'])->format('Y-m-d H:i');
                                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $v['run_date'] . ' ' . $v['end_time'])->format('Y-m-d H:i');

                                if ($currentDate > $startDate && $currentDate < $endDate) {
                                    $next = true;
                                }
                            }

                            if ($next == false) {
                                if ($promotionType == 1) {
                                    return $price;
                                } else {
                                    return $arrGift;
                                }
                            }
                        } else {
                            if ($promotionType == 1) {
                                return $price;
                            } else {
                                return $arrGift;
                            }
                        }
                        break;
                    case 'R':
                        $fromTo = $mFromTo->getDateTimeByPromotion($getDetail['promotion_code']);

                        if ($fromTo != null) {
                            $startFrom = Carbon::createFromFormat('Y-m-d H:i:s', $fromTo['form_date'] . ' ' . $fromTo['start_time'])->format('Y-m-d H:i');
                            $endFrom = Carbon::createFromFormat('Y-m-d H:i:s', $fromTo['to_date'] . ' ' . $fromTo['end_time'])->format('Y-m-d H:i');

                            if ($currentDate < $startFrom || $currentDate > $endFrom) {
                                if ($promotionType == 1) {
                                    return $price;
                                } else {
                                    return $arrGift;
                                }
                            }
                        }
                        break;
                }
            }

            //Check KM theo type = discount or gift
            if ($getDetail['promotion_type'] != $promotionType) {
                if ($promotionType == 1) {
                    return $price;
                } else {
                    return $arrGift;
                }
            }

            //Check nguồn đơn hàng
            if ($getDetail['order_source'] != 'all' && $getDetail['order_source'] != $orderSource) {
                if ($promotionType == 1) {
                    return $price;
                } else {
                    return $arrGift;
                }
            }
            //Check đối tượng áp dụng
            if ($getDetail['promotion_apply_to'] != 1 && $getDetail['promotion_apply_to'] != null) {
                //Lấy thông tin khách hàng
                $getCustomer = $mCustomer->getItem($customerId);

                if ($getCustomer == null || $getCustomer['customer_id'] == 1) {
                    if ($promotionType == 1) {
                        return $price;
                    } else {
                        return $arrGift;
                    }
                }

                if ($getCustomer['member_level_id'] == null) {
                    $getCustomer['member_level_id'] = 1;
                }


                $objectId = '';
                if ($getDetail['promotion_apply_to'] == 2) {
                    $objectId = $getCustomer['member_level_id'];
                } else if ($getDetail['promotion_apply_to'] == 3) {
                    $objectId = $getCustomer['customer_group_id'];
                } else if ($getDetail['promotion_apply_to'] == 4) {
                    $objectId = $getDetail['customer_id'];
                }

                $getApply = $mPromotionApply->getApplyByObjectId($getDetail['promotion_code'], $objectId);

                if ($getApply == null) {
                    if ($promotionType == 1) {
                        return $price;
                    } else {
                        return $arrGift;
                    }
                }
            }

            //Check quota (số tiền)
            if ($promotionType == 1) {
                return $getDetail['promotion_price'];
            } else {
                if ($quantity >= $getDetail['quantity_buy']) {
                    $multiplication = intval($quantity / $getDetail['quantity_buy']);

                    $arrGift = [
                        'gift_object_type' => $getDetail['gift_object_type'],
                        'gift_object_id' => $getDetail['gift_object_id'],
                        'gift_object_name' => $getDetail['gift_object_name'],
                        'gift_object_code' => $getDetail['gift_object_code'],
                        'quantity_gift' => $getDetail['quantity_gift'] * $multiplication
                    ];
                }
                return $arrGift;
            }
        } else {
            if ($promotionType == 1) {
                return $price;
            } else {
                return $arrGift;
            }
        }
    }
}