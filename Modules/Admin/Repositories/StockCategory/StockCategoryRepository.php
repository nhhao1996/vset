<?php


namespace Modules\Admin\Repositories\StockCategory;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Libs\SmsFpt\TechAPI\src\TechAPI\Exception;
use Modules\Admin\Models\BankTable;
use Modules\Admin\Models\StockCategoryTable;
use Modules\Admin\Models\StockNewsTable;

class StockCategoryRepository implements StockCategoryRepositoryInterface
{
    protected $mStockCategory;

    public function __construct(StockCategoryTable $mStockCategory)
    {
        $this->mStockCategory = $mStockCategory;
    }

    public function getListNew(array $filters = [])
    {
        return $this->mStockCategory->getList($filters);
    }

    public function showPopup($data)
    {

        if (isset($data['type']) && $data['type'] != 'created') {
            $data['detail'] = $this->mStockCategory->getDetail($data['stock_category_id']);
        }
        $view = view(
            'admin::stock-category.popup.popup_category',[
                'param' => $data
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function save($param)
    {
        if (isset($param['stock_category_id'])){
            $messageError = 'Cập nhật danh mục thất bại';
            $messageTrue = 'Cập nhật danh mục thành công';
        } else {
            $messageError = 'Tạo danh danh mục thất bại';
            $messageTrue = 'Tạo danh mục thành công';
        }

        try {

//            Check trùng tên
            $checkVI = $this->mStockCategory->checkName(['stock_category_title_vi' => $param['stock_category_title_vi']], isset($param['stock_category_id']) ? $param['stock_category_id'] : null);
            $messageCheck = null;
            if (count($checkVI) != 0){
                $messageCheck = 'Tên danh mục(VI) đã được sử dụng';
            }
            $checkEN = $this->mStockCategory->checkName(['stock_category_title_en' => $param['stock_category_title_en']], isset($param['stock_category_id']) ? $param['stock_category_id'] : null);
            if (count($checkEN) != 0){
                $messageCheck = $messageCheck.'<br>Tên danh mục(EN) đã được sử dụng';
            }

            if ($messageCheck != null){
                return [
                    'error' => true,
                    'message' => $messageCheck
                ];
            }

            $param['stock_category_title_vi'] = strip_tags($param['stock_category_title_vi']);
            $param['stock_category_title_en'] = strip_tags($param['stock_category_title_en']);
            $param['updated_at'] = Carbon::now();
            $param['updated_by'] = 1;
            if (isset($param['stock_category_id'])){
                $id = $param['stock_category_id'];
                unset($param['stock_category_id']);
                $this->mStockCategory->updateCategory($id,$param);
            }else {
                $param['created_at'] = Carbon::now();
                $param['created_by'] = 1;
                $this->mStockCategory->addCategory($param);
            }

            return [
                'error' => false,
                'message' => $messageTrue
            ];
        }catch (Exception $e){

            return [
                'error' => true,
                'message' => $messageError
            ];


        }

    }

    public function deleteCategory($id)
    {
        try {
            $stockNews = new StockNewsTable();
            $checkUsing = $stockNews->checkUsing($id);
            if (count($checkUsing) != 0){
                return [
                    'error' => true,
                    'message' => 'Danh mục đang được sử dụng cho bài viết'
                ];
            }
            $this->mStockCategory->deleteCategory($id);
            return [
                'error' => false,
                'message' => 'Xóa danh mục thành công'
            ];
        }catch (Exception $e){
            return [
                'error' => true,
                'message' => 'Xóa danh mục thất bại'
            ];
        }
    }
}