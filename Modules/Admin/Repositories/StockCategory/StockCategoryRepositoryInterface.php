<?php


namespace Modules\Admin\Repositories\StockCategory;


interface StockCategoryRepositoryInterface
{
    public function getListNew(array $filters = []);

    public function showPopup($data);

    public function save($data);

    public function deleteCategory($id);
}