<?php


namespace Modules\Admin\Repositories\ManagerExtendContract;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Models\ConfigTable;
use Modules\Admin\Models\CustomerContactLogTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\InvestmentTimeTable;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\PaymentMethodTable;
use Modules\Admin\Models\ProductBonusTable;
use Modules\Admin\Models\ProductInterestTable;
use Modules\Admin\Models\ProductTable;
use Modules\Admin\Models\ReceiptDetailImageTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReceiptTable;
use Modules\Admin\Models\VsetSettingTable;
use Modules\Admin\Models\WithdrawInterestTimeTable;
use Modules\Order\Models\CustomerContract;
use Modules\Order\Repositories\Order\OrderRepoException;

class ManagerExtendContractRepository implements ManagerExtendContractRepositoryInterface
{
    protected $contract;
    protected $config;
    protected $order;
    protected $productInterest;
    protected $productBonus;
    protected $product;
    protected $investmentTime;
    protected $withdrawInterest;
    protected $payment;
    protected $receiptTable;
    protected $receiptDetail;
    protected $receiptDetailImage;
    protected $setting;
    protected $memberLevel;
    protected $customer;
    protected $jobsEmailLogApi;

    public function __construct(
        CustomerContactTable $contact,
        ConfigTable $config,
        OrderTable $order,
        ProductBonusTable $productBonus,
        ProductInterestTable $productInterest,
        ProductTable $product,
        InvestmentTimeTable $investmentTime,
        WithdrawInterestTimeTable $withdrawInterest,
        PaymentMethodTable $payment,
        ReceiptTable $receiptTable,
        ReceiptDetailTable $receiptDetail,
        ReceiptDetailImageTable $receiptDetailImage,
        VsetSettingTable $setting,
        MemberLevelTable $memberLevel,
        CustomerTable $customer,
        JobsEmailLogApi $jobsEmailLogApi
    )
    {
        $this->contract = $contact;
        $this->config = $config;
        $this->order = $order;
        $this->productBonus = $productBonus;
        $this->productInterest = $productInterest;
        $this->product = $product;
        $this->investmentTime = $investmentTime;
        $this->withdrawInterest = $withdrawInterest;
        $this->payment = $payment;
        $this->receiptTable= $receiptTable;
        $this->receiptDetail = $receiptDetail;
        $this->receiptDetailImage = $receiptDetailImage;
        $this->setting = $setting;
        $this->memberLevel = $memberLevel;
        $this->customer = $customer;
        $this->jobsEmailLogApi = $jobsEmailLogApi;
    }

    const ORDER_PREFIX = "VSG-";

    public function list(array $filters = [])
    {
        $listContractExtend = $this->order->getListContractExtend();
        $filters['list_extend_contract'] = [];
        if (count($listContractExtend) != 0) {
            $filters['list_extend_contract'] = collect($listContractExtend)->pluck('contract_code_extend');
        }
        $warningExtend = $this->config->getInfoByKey('warning_extend');
        $month = 15;
        if ($warningExtend != null && ($warningExtend['value'] != null || $warningExtend['value'] != '')) {
            $month = (int)$warningExtend['value'];
        }
        $filters['manager_extend_contract'] = Carbon::now()->addDays($month)->format('Y-m-d');

        $list = $this->contract->getListAllPagination($filters);
        return $list;
    }

    public function getDetail($id)
    {
        $detail = $this->contract->getItem($id);
        $listProduct = $this->product->getListAllByCategory($detail['product_category_id']);
        $listProductInterest = $this->investmentTime->getAllByCategoryProduct($detail['product_category_id']);
        $withdrawInterestTime = $this->withdrawInterest->getAll();
        $paymentMethod = $this->payment->getAllByType('manual');
        return [
            'detail' => $detail,
            'listProduct' => $listProduct,
            'listProductInterest' => $listProductInterest,
            'withdrawInterestTime' => $withdrawInterestTime,
            'paymentMethod' => $paymentMethod
        ];
    }

    public function checkExtendContract($data)
    {
        $errorMessage = null;
        //Kiểm tra hợp đồng đã được gia hạn chưa
        $checkExtendContract = $this->order->checkExtendContract($data['customer_contract_code']);

        if ($checkExtendContract != null) {
            $errorMessage = __('Hợp đồng đã được yêu cầu gia hạn');
        }

        //Lấy thông tin hợp đồng cần gia hạn
        $getContract = $this->contract->getContractByCode($data['customer_contract_code']);

        //Check hợp đồng ko kỳ hạn thì ko cho gia hạn
        if ($getContract['term_time_type'] == 0) {
            $errorMessage = __('Hợp đồng không kỳ hạn không thể gia hạn');
        }

        //Check hợp đồng đã rút gốc chưa
        if ($getContract['is_fully_withdraw'] == 1) {
            $errorMessage =  __('Hợp đồng đã rút gốc không thể gia hạn');
        }

        //Check hợp đồng tới hạn gia hạn chưa
        $endDate = Carbon::parse($getContract['customer_contract_end_date'])->subMonths($getContract['month_extend'])->format('Y-m-d H:i:s');
        $now = Carbon::now()->format('Y-m-d H:i:s');

        if ($endDate > $now) {
            $errorMessage = __('Chưa tới thời hạn gia hạn');
        }

        if ($errorMessage == null) {
            return [
                'error' => false,
                'message' => null,
                'id' => $data['customer_contract_id']
            ];
        } else {
            return [
                'error' => true,
                'message' => $errorMessage,
            ];
        }

    }

    public function checkValue($data)
    {
        if (!isset($data['product_id']) || !isset($data['investment_time_id']) || !isset($data['quantity']) || !isset($data['withdraw_interest_time_id'])) {
            return [
                'error' => true
            ];
        }

//        Lấy thông tin sản phẩm mới
        $product = $this->product->getDetail($data['product_id']);

//        Lấy lãi suất
        $interest = $this->productInterest->getInterestRate($data['product_id'],$data['investment_time_id'],$data['withdraw_interest_time_id']);

        $totalPayment = ($product['price_standard'] * $data['quantity']) - $data['total_old'];

        return [
            'error' => false,
            'interest_rate_text' => $interest['interest_rate'],
            'bonus_text' => $product['bonus_extend'],
            'total_amount_new' => ($product['price_standard'] * $data['quantity']) + $product['bonus_extend'],
            'total_new' => $product['price_standard'] * $data['quantity'],
            'money_payment' => $totalPayment >= 0 ? $totalPayment : 0
        ];
    }

    public function printBill($data)
    {
        $data = [
            'investment_unit' => strip_tags($data['investment_unit']),
            'user_name' => strip_tags($data['user_name']),
            'cmnd' => strip_tags($data['cmnd']),
            'created' => strip_tags($data['created']),
            'issued_by' => strip_tags($data['issued_by']),
            'address' => strip_tags($data['address']),
            'phone' => strip_tags($data['phone']),
            'reason' => strip_tags($data['reason']),
            'order_code' => strip_tags($data['order_code']),
            'payment_amount' => strip_tags($data['payment_amount']),
            'payment_amount_text' => strip_tags($data['payment_amount_text']),
            'name_collector' => strip_tags($data['name_collector']),
            'phone_collector' => strip_tags($data['phone_collector']),
        ];
        $view = view('admin::manager-extend-contract.bill.cash',['data' => $data])->render();
        return [
            'view' => $view
        ];
    }

    public function checkCreateContract($data)
    {
        try {

            $check = $this->checkExtendContract($data);
           if ($check['error'] == true) {
               return $check;
           }

            if ($data['total_new'] < $data['total_old']) {
                return [
                    'error' => true,
                    'message' => __('Giá trị hợp đồng mới phải lớn hơn giá trị hợp đồng cũ'),
                ];
            }

//        Lấy thông tin ngân hàng
            $bank = $this->config->getInfoByKey('bank_vset');

//        Lấy hợp đồng cũ
            $contract = $this->contract->getDetail($data['customer_contract_id']);

//        Lấy thông tin sản phẩm
            $getProduct = $this->product->getDetail($data['product_id']);
            $quantity = (isset($data['quantity']) && $data['quantity'] != null) ? intval($data['quantity']) : 1;
            $rProInt = $this->productInterest->getInterest($data['product_cateogry_id'], $data['product_id'],(isset($data['investment_time_id'])) ? $data['investment_time_id'] : 0, (isset($data['withdraw_interest_time_id'])) ? $data['withdraw_interest_time_id'] : null);
            $productBonus = $this->productBonus->getBonus($getProduct['product_cateogry_id'],$getProduct['product_id'],$data['payment'],$data['investment_time_id']);
            $total = intval($getProduct['price_standard']) * $quantity;
            $order_code = $this->generateOrderCode();
            $totalAmount = $data['total_new'];

            //Add vào order
            $dataOrder = [
                'customer_id' => $data['customer_id'],
                "order_description" => 'Gia hạn hợp đồng ' . $data['customer_contract_code'],
                'total' => $totalAmount,
                "quantity" => $data['quantity'],
                "product_id" => $data['product_id'],
                "bonus_rate" => (isset($productBonus['bonus_rate'])) ? $productBonus["bonus_rate"] : 0,
                "bonus" => (isset($productBonus['bonus_rate'])) ? (intval($getProduct['price_standard']) * $data['quantity'] * intval($productBonus["bonus_rate"])) / 100 : 0,
                "price_standard" => $getProduct['price_standard'],
                "term_time_type" => (isset($getProduct['term_time_type'])) ? $getProduct['term_time_type'] : 1,
                "commission_rate" => (isset($rProInt['commission_rate'])) ? $rProInt['commission_rate'] : 0,
                "commission" => (isset($rProInt['commission_rate'])) ? (intval($rProInt['commission_rate']) * intval($getProduct['price_standard']) * $data['quantity']) / 100 : 0,
                "investment_time_id" => (isset($data['investment_time_id'])) ? $data['investment_time_id'] : null,
                "withdraw_interest_time_id" => (isset($data['withdraw_interest_time_id'])) ? $data['withdraw_interest_time_id'] : null,
                "payment_method_id" => $data['payment'],
                "process_status" => "paysuccess",
                "payment_date" => Carbon::now(),
                "interest_rate" => $rProInt['interest_rate'],
                "interest_rate_standard" => (isset($getProduct['interest_rate_standard'])) ? $getProduct['interest_rate_standard'] : 0,
                "customer_name" => $contract['customer_name'],
                "customer_phone" => $contract['customer_phone'],
                "customer_email" => $contract['customer_email'],
                "customer_residence_address" => $contract['customer_residence_address'],
                "customer_ic_no" => $contract['customer_ic_no'],
                "refer_id" => $contract['refer_id'],
                "total_interest" => ($rProInt != null) ? $rProInt["total_interest"] * $data['quantity'] : 0,
                "month_interest" => ($rProInt != null) ? $rProInt["month_interest"] * $data['quantity'] : 0,
                "is_extend" => 1,
                "contract_code_extend" => $data['customer_contract_code'],
                'created_at' => Carbon::now()
            ];

            $Order = $this->order->add($dataOrder);
            $editOrder = $this->order->getDetail($Order);
            //Update order code
            $this->order->edit([
                'order_code' => $order_code,
                "total_amount" => $editOrder['total'] + $editOrder['bonus']
            ], $Order);

            $dataReceipt = [
                'customer_id' => $contract['customer_id'],
                'amount' => $editOrder['total'],
                'amount_paid' => $editOrder['total'],
                'staff_id'=> Auth::id(),
                'object_id' => $Order,
                'amount_return' => 0,
                'status' =>'paid',
                'created_at' => Carbon::now(),
                'created_by' => Auth::id(),
            ];

            $idReceipt = $this->addReceipt($dataReceipt);

            $dataReceiptDetail = [
                'receipt_id' => $idReceipt,
                'staff_id' => Auth::id(),
                'receipt_type' => $data['payment'] == 1 ? 'transfer' : 'cash',
                'amount' => $editOrder['total'],
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            $idReceiptDetail = $this->receiptDetail->createReceiptDetail($dataReceiptDetail);

            if (isset($data['arrImage'])) {
                $arrImage = [];
                foreach ($data['arrImage'] as $item) {
                    $arrImage[] = [
                        'receipt_id' => $idReceipt,
                        'receipt_detail_id' => $idReceiptDetail,
                        'staff_id' => Auth::id(),
                        'image_file' => $item['image'],
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }

                $this->receiptDetailImage->createImage($arrImage);
            }

            $createStart = $contract['customer_contract_end_date'] > Carbon::now() ? Carbon::parse($contract['customer_contract_end_date']): Carbon::now();
            $orderDetail = $this->order->getById($Order);
//        Tạo hợp đồng mới
            $arrContract = [
                'customer_contract_code' => $orderDetail['order_code'],
                'order_id' => $Order,
                'customer_id' => $orderDetail['customer_id'],
                'product_id' => $orderDetail['product_id'],
                'product_code' => $orderDetail['product_code'],
                'product_name_vi' => $orderDetail['product_name_vi'],
                'product_short_name_vi' => $orderDetail['product_short_name_vi'],
                'description_vi' => $orderDetail['description_vi'],
                'product_name_en' => $orderDetail['product_name_en'],
                'product_short_name_en' => $orderDetail['product_short_name_en'],
                'description_en' => $orderDetail['description_en'],
                'product_avatar' => $orderDetail['product_avatar_vi'],
                'product_image_detail' => $orderDetail['product_image_detail_vi'],
                'quantity' => $orderDetail['quantity'],
                'price_standard' => $orderDetail['price_standard'],
                'interest_rate_standard' => $orderDetail['interest_rate_standard'],
                'term_time_type' => $orderDetail['term_time_type'],
                'investment_time' => $orderDetail['investment_time_month'],
                'withdraw_interest_time' => $orderDetail['withdraw_interest_month'],
                'interest_rate' => $orderDetail['interest_rate'],
                'commission_rate' => $orderDetail['commission_rate'],
                'month_interest' => $orderDetail['month_interest'],
                'total_interest' => $orderDetail['total_interest'],
                'bonus_rate' => $orderDetail['bonus_rate'],
                'total' => $orderDetail['total'],
                'bonus' => $orderDetail['bonus'],
                'total_amount' => $orderDetail['total_amount'],
                'payment_method' => $orderDetail['payment_method_name_vi'],
                'customer_contract_start_date' => $createStart,
                'customer_contract_end_date' => $orderDetail['term_time_type'] == 1 ? $createStart->addMonth($orderDetail['investment_time_month']) : null,
                'customer_name' => $orderDetail['customer_name'],
                'customer_phone' => $orderDetail['customer_phone'],
                'customer_email' => $orderDetail['customer_email'],
                'customer_residence_address' => $orderDetail['customer_residence_address'],
                'customer_ic_no' => $orderDetail['customer_ic_no'],
                'refer_id' => $orderDetail['refer_id'],
                'commission' => $orderDetail['commission'],
                'is_active' => 1,
                'is_deleted' => 0,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            $createCustomerContract = $this->contract->createCustomerContract($arrContract);
            $this->customerContractLog($createCustomerContract);

//                tổng đầu tư
            $invested = $this->contract->getContractByCustomer($orderDetail['customer_id']);
            $totalMoneyContract = 0;
            foreach ($invested as $value) {
                $totalMoneyContract += $value['total'];
            }
//                Lấy cách đổi điểm ở bảng setting
            $setting = $this->setting->getSettingByKey('money_point');
            $point = $totalMoneyContract/$setting['vset_setting_value'];
            $getMemberLevelId = $this->memberLevel->checkByPoint((int)$point);
            $arrOrderUpdate = [
                'point' => (int)$point,
                'member_level_id' => $getMemberLevelId['member_level_id']
            ];

            $this->customer->updateCustomer($orderDetail['customer_id'],$arrOrderUpdate);

            $vars = [
                'contract_code' => $arrContract['customer_contract_code'],
                'customer_name' => $orderDetail['customer_name'],
                'staff' => Auth::user()['full_name'],
                'confirm_date' => Carbon::now()->format('Y/m/d H:i:s'),
            ];

        $this->insertEmailApi($createCustomerContract,'contract','Tạo hợp đồng',$orderDetail['customer_email'],$vars);

        $this->sendNotificationAction('contract_extension',$orderDetail['customer_id'],$createCustomerContract);
            return [
                'error' => false,
                'message' => 'Gia hạn hợp đồng thành công'
            ];
        } catch (\Exception $e) {
            return [
                'error' => false,
                'message' => 'Gia hạn hợp đồng thất bại'
            ];
        }
    }

    public function sendNotificationAction($key,$customer,$object_id){
        $noti = [
            'key' => $key,
            'customer_id' => $customer,
            'object_id' => $object_id
        ];
        $this->jobsEmailLogApi->sendNotification($noti);
        return true;
    }

    public function insertEmailApi($idDetailRequest,$type,$subject,$email_to,$vars) {
        $insertEmail = [
            'obj_id' => $idDetailRequest,
            'email_type' => $type,
            'email_subject' => $subject,
            'email_from' => env('MAIL_USERNAME'),
            'email_to' => $email_to,
            'email_params' => json_encode($vars),
            'is_run' => 0
        ];

        $this->jobsEmailLogApi->addJob($insertEmail);

        return true;
    }

    /**
     * Log customer contract
     * @param $id
     */
    private function customerContractLog($id)
    {
        //Model customer contract
        $mCustomerContact = new CustomerContactTable();
        //Model customer contract log
        $mCustomerContactLog = new CustomerContactLogTable();
        //Chi tiết của customer contract
        $customerContract = $mCustomerContact->getItem($id);
        if ($customerContract['customer_contract_end_date'] != null) {
            //Số tháng đầu tư
            $investmentTime = $customerContract['investment_time'];
            //Kỳ hạn rút lãi -> là số tháng
            $withdrawInterestTime = $customerContract['withdraw_interest_time'];
            //Số record
            $record = (int)($investmentTime / $withdrawInterestTime);
            //Số tháng dư
            $residual = $investmentTime % $withdrawInterestTime;
            //Năm bắt đầu hợp đồng
            $year = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
                ->format('Y');
            //Tháng bắt đầu hợp đồng
            $month = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
                ->format('m');
            //Năm kết thúc hợp đồng
            $yearEnd = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
                ->format('Y');
            //Tháng bắt đầu hợp đồng
            $monthEnd = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
                ->format('m');
            $data = [];
            if ($record > 0) {
                $mT = 0;
                $interest = $withdrawInterestTime * $customerContract['month_interest'];
                for ($i = 0; $i < $record; $i++) {
                    //Tháng lãi tiếp theo
                    $mT += $withdrawInterestTime;
                    $dt = Carbon::create($year, $month, 1, 0);
                    //Cộng thêm tháng
                    $addMonth = $dt->addMonths($mT);
                    $y = $startTime = Carbon::createFromFormat(
                        'Y-m-d H:i:s', $addMonth)
                        ->format('Y');
                    $m = (int)$startTime = Carbon::createFromFormat(
                        'Y-m-d H:i:s', $addMonth)
                        ->format('m');
                    $data[] = [
                        'customer_contract_id' => $id,
                        'year' => $y,
                        'month' => $m,
                        'interest' => $interest
                    ];
                }
            }
            //Lần cuối tính lãi
            if ($residual > 0) {
                $data[] = [
                    'customer_contract_id' => $id,
                    'year' => $yearEnd,
                    'month' => $monthEnd,
                    'interest' => $residual * $customerContract['month_interest']
                ];
            }

            if ($data != []) {
                $mCustomerContactLog->addInsert($data);
            }
        }
    }

    public function generateOrderCode()
    {
        $result = $this->order->getLastOrder();
//        dd(strpos($result["order_code"],self::ORDER_PREFIX));
        if ($result == null) return self::ORDER_PREFIX . "0000500";
        if (strpos($result["order_code"], self::ORDER_PREFIX) !== false) {
//            dd(strpos($result["order_code"],self::ORDER_PREFIX));
            $arr = explode(self::ORDER_PREFIX, $result["order_code"]);
            $value = strval(intval($arr[1]) + 1);
            $zero_str = "";
            if (strlen($value) < 7) {
                for ($i = 0; $i < (7 - strlen($value)); $i++) {
                    $zero_str .= "0";
                }
            }


            return self::ORDER_PREFIX . $zero_str . $value;
        }

        return self::ORDER_PREFIX . "0000500";
    }

    // tạo phiếu thu
    public function addReceipt(array $data)
    {
        $data['receipt_code'] = $this->generateUniqueString(6);
        return $this->receiptTable->createReceipt($data);
    }

    private function generateUniqueString()
    {
        while(true) {
            $now = Carbon::today()->format('Y/m/d');
            $to = Carbon::tomorrow()->format('Y/m/d');
            $str_length = 4;
            $reservations = ReceiptTable::where('created_at', '>=', $now)
                ->where('created_at', '<=', $to)
                ->count();
            $nowYmd = Carbon::today()->format('Ymd');
            $str = substr("0000{$reservations}", - $str_length);
            $randomString =$nowYmd.'_'.$str;
            $doesCodeExist = DB::table('receipts')
                ->where('receipt_code', $randomString)
                ->count();
            if (! $doesCodeExist) {
                return $randomString;
            }
        }

    }
}