<?php


namespace Modules\Admin\Repositories\ManagerExtendContract;


interface ManagerExtendContractRepositoryInterface
{
    public function list(array $filters = []);

//    Lấy chi tiết thông tin hợp đồng cũ
    public function getDetail($id);

//    Kiểm tra hợp đồng
    public function checkExtendContract($data);

//    Kiểm tra và thay đổi giá trị hiển thị
    public function checkValue($data);

//    In biên nhận
    public function printBill($data);

//    Kiểm tra và gia hạn hợp đồng
    public function checkCreateContract($data);
}