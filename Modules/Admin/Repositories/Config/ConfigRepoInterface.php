<?php


namespace Modules\Admin\Repositories\Config;


interface ConfigRepoInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $key
     * @return mixed
     */
    public function getInfoByKey($key);

    /**
     * Lấy thông tin theo id
     * @return mixed
     */
    public function getInfoById($id);

    /**
     * Cập nhật config
     * @return mixed
     */
    public function updatekey($data);

    /**
     * lấy danh sách cấu hình chi tiết
     * @return mixed
     */
    public function getConfigDetail($id);
}