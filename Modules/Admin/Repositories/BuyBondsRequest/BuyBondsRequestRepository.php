<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\BuyBondsRequest;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Libs\SmsFpt\TechAPI\src\TechAPI\Exception;
use Modules\Admin\Models\CustomerContactLogTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\Order;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\ProductBonusServiceLogTable;
use Modules\Admin\Models\ProductBonusServiceTable;
use Modules\Admin\Models\ReceiptDetailImageTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReceiptTable;
use Modules\Admin\Models\ServiceSerialTable;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Models\VsetSettingTable;
use Modules\Admin\Models\WalletTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Admin\Repositories\BuyBondsRequest\BuyBondsRequestRepositoryInterface;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;

class BuyBondsRequestRepository implements BuyBondsRequestRepositoryInterface
{

    protected $order;
    protected $receiptTable;
    protected $receiptDetail;
    protected $receiptDetailImage;
    protected $withrawRequestGroup;
    protected $withrawRequest;
    protected $customerContract;
    protected $setting;
    protected $memberLevel;
    protected $customer;
    protected $jobsEmailLogApi;
    protected $contractInterestMonth;
    protected $contractInterestTime;
    protected $wallet;
    protected $productBonusService;
    protected $serviceSerial;
    protected $staff;
    protected $productBonusServiceLog;
    public function __construct(
        Order $order,
        ReceiptTable $receiptTable,
        ReceiptDetailTable $receiptDetail,
        ReceiptDetailImageTable $receiptDetailImage,
        WithdrawRequestGroupTable $withdrawRequestGroup,
        WithdrawRequestTable $withrawRequest,
        CustomerContactTable $customerContract,
        VsetSettingTable $setting,
        MemberLevelTable $memberLevel,
        CustomerTable $customer,
        JobsEmailLogApi $jobsEmailLogApi,
        CustomerContractInterestByMonthTable $contractInterestMonth,
        CustomerContractInterestByTimetable $contractInterestTime,
        WalletTable $wallet,
        ProductBonusServiceTable $productBonusService,
        ServiceSerialTable $serviceSerial,
        StaffsTable $staff,
        ProductBonusServiceLogTable $productBonusServiceLog
    )
    {
        $this->order = $order;
        $this->receiptTable= $receiptTable;
        $this->receiptDetail = $receiptDetail;
        $this->receiptDetailImage = $receiptDetailImage;
        $this->withrawRequestGroup = $withdrawRequestGroup;
        $this->withrawRequest = $withrawRequest;
        $this->customerContract = $customerContract;
        $this->setting = $setting;
        $this->memberLevel = $memberLevel;
        $this->customer = $customer;
        $this->jobsEmailLogApi = $jobsEmailLogApi;
        $this->contractInterestMonth = $contractInterestMonth;
        $this->contractInterestTime = $contractInterestTime;
        $this->wallet = $wallet;
        $this->productBonusService = $productBonusService;
        $this->serviceSerial = $serviceSerial;
        $this->staff = $staff;
        $this->productBonusServiceLog = $productBonusServiceLog;
    }

    //Hàm lấy danh sách
    public function list(array $filters = [])
    {
        $filters['is_extend'] = 0;
        return $this->order->getList($filters);
    }

    public function show($id){
        return $this->order->getById($id);
    }

     // tạo phiếu thu
    public function addReceipt(array $data)
    {
        $data['receipt_code'] = $this->generateUniqueString(6);
        return $this->receiptTable->add($data);
    }

    // hàm tạo chuỗi ngẫu nhiên time now + uniquestring
    private function generateUniqueString()
    {
        while(true) {
            $now = Carbon::today()->format('Y/m/d');
            $to = Carbon::tomorrow()->format('Y/m/d');
            $str_length = 4;
            $reservations = ReceiptTable::where('created_at', '>=', $now)
                ->where('created_at', '<=', $to)
                ->count();
            $nowYmd = Carbon::today()->format('Ymd');
            $str = substr("0000{$reservations}", - $str_length);
            $randomString =$nowYmd.'_'.$str;
            $doesCodeExist = DB::table('receipts')
               ->where('receipt_code', $randomString)
               ->count();
            if (! $doesCodeExist) {
                return $randomString;
            }
        }

    }

    private function generateUniqueStringContract()
    {
        while(true) {
            $date  =  new \DateTime();
            $now = $date->getTimestamp();
            $randomString = 'CT'.$now.'_'.str_random(4);
            $doesCodeExist = DB::table('customer_contract')
                ->where('customer_contract_code', $randomString)
                ->count();

            if (! $doesCodeExist) {
                return $randomString;
            }
        }
    }
    // update status order

    public function changeStatus($data)
    {
        $detail = $this->order->getDetail($data['order_id']);
        if ($data['process_status'] == 'confirm') {
            if ($detail['process_status'] != 'new') {
                return [
                    'error' => true,
                    'message' => 'Xác nhận thất bại' ,
                ];
            }
        }
        $this->order->edit($data);

        // get detail order
        $detail = $this->show($data['order_id']);
        // get detail customer
        $mCustomer = new CustomerTable();
        $mCustomer  = $mCustomer->getItem($detail['customer_id']);

        /// // gọi api lưu data send mail
        $vars = [
            'customer_name' => $mCustomer['full_name'],
            'order_type' => $detail['type_bonds'],
            'order_code' => $detail['order_code'],
            'status'      =>  'Xác nhận',
            'confirm_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'staff' => Auth::user()['full_name']
        ];

        $arrInsertEmail = [
            'obj_id' => $data['order_id'],
            'email_type' => "confirm-order",
            'email_subject' => __('Xác nhận mua gói Đầu tư/tiết kiệm'),
            'email_to' =>  $mCustomer['email'],
            'email_from' => env('MAIL_USERNAME'),
            'email_params' => json_encode($vars),
        ];

        $this->jobsEmailLogApi->addJob($arrInsertEmail);

        if (isset($data['staff_id']) && $data['staff_id'] != null) {
            $staffInfo = $this->staff->getItem($data['staff_id']);
            $varsStaff = [
                'order_code' => $detail['order_code'],
                'customer_name' => $mCustomer['full_name'],
                'staff' => $staffInfo['full_name'],
                'status' => 'Xác nhận',
            ];

            $arrInsertEmailStaff = [
                'obj_id' => $data['order_id'],
                'email_type' => "support-order",
                'email_subject' => __('Hỗ trợ khách hàng'),
                'email_to' =>  $staffInfo['email'],
                'email_from' => env('MAIL_USERNAME'),
                'email_params' => json_encode($varsStaff),
            ];
            $this->jobsEmailLogApi->addJob($arrInsertEmailStaff);
        }

        if ($detail['product_category_id'] == 1) {
            $this->sendNotificationAction('order_bond_status_A',$detail['customer_id'],$data['order_id']);
        } else if($detail['product_category_id'] == 2) {
            $this->sendNotificationAction('order_saving_status_A',$detail['customer_id'],$data['order_id']);
        }

        return true;
    }

    public function getMoneyReceipt($order_id)
    {
        return $this->receiptTable->getReceiptOrder($order_id);
    }

    public function getListStaff()
    {
        return $this->staff->getAll();
    }

    public function getListReceiptDetail($param)
    {
        $param['object_type'] = 'order';
        $list = $this->receiptDetail->getListReceipt($param);
        $arrDetailId = [];
        $arrDetailId = collect($list->toArray()['data'])->pluck('receipt_detail_id');
        $listImage = $this->receiptDetailImage->getListImage($arrDetailId);
        $arrGroupImage = [];
        if (count($listImage) != 0) {
            $arrGroupImage = collect($listImage)->groupBy('receipt_detail_id');
        }
        $view = view(
            'admin::buy-bonds-request.partial.receipt-detail-tr',
            [
                'list' => $list,
                'arrGroupImage' => $arrGroupImage
            ]
        )->render();


        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function addReceiptDetail($param)
    {
        try {
            DB::beginTransaction();
            $detail = $this->order->getDetail($param['order_id']);
            if ($detail['process_status'] != 'confirmed') {
                return [
                    'error' => true,
                    'message' => 'Tạo phiếu thu thất bại' ,
                ];
            }
            $this->createReceiptDetail($param);
            DB::commit();
            return [
                'error' => false,
                'message' => 'Tạo phiếu thu thành công'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Tạo phiếu thu thất bại'
            ];
        }
    }

    public function createReceiptDetail($param){

        try {
            $arr = [];
            $arrReceipt = [];
            $imgTransfer = [];
            $success = 0;
            $order_id = $param['order_id'];
            $receipt_id = $param['receipt_id'];
            unset($param['order_id']);
            unset($param['receipt_id']);
            //        lấy chi tiết receipt
            $receipt = $this->receiptTable->getDetail($receipt_id);
            $countMoney = 0;
            $amount_receipt_detail = 0;
//            if ($receipt['amount_paid'] == null) {
//                $receipt['amount_paid'] = 0;
//            }

            $receipt['amount_paid'] = 0;


            if ($param['receipt_type'] == 'cash') {
                $countMoney = $receipt['amount_paid'] + (double)str_replace(',', '', $param['amount']);
                $amount_receipt_detail = (double)str_replace(',', '', $param['amount']);

            } else if ($param['receipt_type'] == 'transfer') {
                $countMoney = $receipt['amount_paid'] + (double)str_replace(',', '', $param['amount_transfer']);
                $amount_receipt_detail = (double)str_replace(',', '', $param['amount_transfer']);

            } else if($param['receipt_type'] == 'interest') {
//                Lấy thông tin nhóm
                $withrawRequestGroup = $this->withrawRequestGroup->getDetailById($param['withdraw_request_group_id_interest']);
//                $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['available_balance'];
                $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['withdraw_request_amount'];
//                $amount_receipt_detail = $withrawRequestGroup['available_balance'];
                $amount_receipt_detail = $withrawRequestGroup['withdraw_request_amount'];
//                Cập nhật lại trạng thái của withdraw group
                $this->withrawRequestGroup->changeStatus($param['withdraw_request_group_id_interest'],['withdraw_request_status' => 'done']);
                $this->withrawRequest->changeStatusByWithdrawGroup($param['withdraw_request_group_id_interest'],['withdraw_request_status' => 'done']);
            } else if($param['receipt_type'] == 'bonus') {
                $withrawRequestGroup = $this->withrawRequestGroup->getDetailById($param['withdraw_request_group_id_bonus']);
//                $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['available_balance'];
                $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['withdraw_request_amount'];
//                $amount_receipt_detail = $withrawRequestGroup['available_balance'];
                $amount_receipt_detail = $withrawRequestGroup['withdraw_request_amount'];
                $this->withrawRequestGroup->changeStatus($param['withdraw_request_group_id_bonus'],['withdraw_request_status' => 'done']);
            }

            if (($countMoney - $receipt['amount']) < 0 ) {
                $arrReceipt = [
                    'amount_paid' => $countMoney,
                    'status' => 'part-paid'
                ];
                $orderStatus = 'pay-half';

                $arr = [
                    'receipt_type' => $param['receipt_type'],
                    'receipt_id' => $receipt_id,
                    'amount' => $amount_receipt_detail,
                    'staff_id' => Auth::id(),
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            } else {
                $arrReceipt = [
                    'amount_paid' => $receipt['amount'],
                    'amount_return' => $countMoney - $receipt['amount'],
                    'status' => 'paid'
                ];
                $orderStatus = 'paysuccess';

                $arr = [
                    'receipt_type' => $param['receipt_type'],
                    'receipt_id' => $receipt_id,
                    'amount' => $receipt['amount'] - $receipt['amount_paid'],
                    'staff_id' => Auth::id(),
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            $arOrder['process_status'] = $orderStatus;
            if (isset($param['staff_id'])){
                $arOrder['staff_id'] = $param['staff_id'];
            }
            if ($orderStatus == 'paysuccess') {
                $arOrder['payment_date'] = Carbon::now();
            }

            $updateReceipt = $this->receiptTable->updateReceipt($receipt_id,$arrReceipt);
            $updateOrder = $this->order->updateOrder($order_id,$arOrder);

//            chi tiết đơn hàng
            $detailOrder = $this->order->getDetail($order_id);

//        Thêm receipt detail
            $idDetailRequest = $this->receiptDetail->createReceiptDetail($arr);
//            tạo send mail
            $vars = [
                'receipt_type' => $param['receipt_type'] == 'cash' ? 'Tiền mặt':
                    ($param['receipt_type'] == 'transfer' ? 'Chuyển khoản' :
                        ($param['receipt_type'] == 'interest' ? 'Tiền lãi' :
                            ($param['receipt_type'] == 'bonus' ? 'Tiền thưởng' : ''))),
                'amount' => (int)str_replace(',', '',$param['amount']),
                'staff' => Auth::user()['full_name'],
                'order_code' => $detailOrder['order_code'],
                'customer_name' => $detailOrder['customer_name']
            ];

//        Trái phiếu
            if ($detailOrder['product_category_id'] == 1) {
                $this->sendNotificationAction('order_bond_status_S',$detailOrder['customer_id'],$order_id);
//            tiết kiệm
            } else if ($detailOrder['product_category_id'] == 2) {
                $this->sendNotificationAction('order_saving_status_S',$detailOrder['customer_id'],$order_id);
            }

            $this->insertEmailApi($idDetailRequest,'create-receipt','Tạo biên lai thanh toán',$receipt['customer_email'],$vars);

            if ($param['receipt_type'] == 'transfer' || $param['receipt_type'] == 'cash') {
                if (isset($param['img-transfer']) && count($param['img-transfer']) != 0) {
                    foreach ($param['img-transfer'] as $key => $item) {
                        $imgTransfer[$key] = [
                            'receipt_id' => $receipt_id,
                            'receipt_detail_id' => $idDetailRequest,
                            'staff_id' => Auth::id(),
                            'image_file' => url('/').'/' . $this->transferTempfileToAdminfile($item,str_replace('', '', $item)),
                            'created_by' => Auth::id(),
                            'updated_by' => Auth::id(),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                    }
                }

            }
            //        Thêm ảnh
            if (count($imgTransfer) != 0) {
                $this->receiptDetailImage->createImage($imgTransfer);
            }

//            Nếu hoàn thành tạo hợp đồng
            if ($orderStatus == 'paysuccess') {
//                lấy chi tiết đơn hàng
                $orderDetail = $this->order->getById($order_id);
                $arrContract = [
                    'customer_contract_code' => $orderDetail['order_code'],
                    'order_id' => $order_id,
                    'customer_id' => $orderDetail['customer_id'],
                    'product_id' => $orderDetail['product_id'],
                    'product_code' => $orderDetail['product_code'],
                    'product_name_vi' => $orderDetail['product_name_vi'],
                    'product_short_name_vi' => $orderDetail['product_short_name_vi'],
                    'description_vi' => $orderDetail['description_vi'],
                    'product_name_en' => $orderDetail['product_name_en'],
                    'product_short_name_en' => $orderDetail['product_short_name_en'],
                    'description_en' => $orderDetail['description_en'],
                    'product_avatar' => $orderDetail['product_avatar_vi'],
                    'product_image_detail' => $orderDetail['product_image_detail_vi'],
                    'quantity' => $orderDetail['quantity'],
                    'price_standard' => $orderDetail['price_standard'],
                    'interest_rate_standard' => $orderDetail['interest_rate_standard'],
                    'term_time_type' => $orderDetail['term_time_type'],
                    'investment_time' => $orderDetail['investment_time_month'],
                    'withdraw_interest_time' => $orderDetail['withdraw_interest_month'],
                    'interest_rate' => $orderDetail['interest_rate'],
                    'commission_rate' => $orderDetail['commission_rate'],
                    'month_interest' => $orderDetail['month_interest'],
                    'total_interest' => $orderDetail['total_interest'],
                    'bonus_rate' => $orderDetail['bonus_rate'],
                    'total' => $orderDetail['total'],
                    'bonus' => $orderDetail['bonus'],
                    'total_amount' => $orderDetail['total_amount'],
                    'payment_method' => $orderDetail['payment_method_name_vi'],
                    'customer_contract_start_date' => Carbon::now(),
                    'customer_contract_end_date' => $orderDetail['term_time_type'] == 1 ? Carbon::now()->addMonth($orderDetail['investment_time_month']) : null,
                    'customer_name' => $orderDetail['full_name_customer'],
                    'customer_phone' => $orderDetail['phone2_customer'],
                    'customer_email' => $orderDetail['email_customer'],
                    'customer_residence_address' => $orderDetail['residence_address_customer'],
                    'customer_ic_no' => $orderDetail['ic_no_customer'],
                    'refer_id' => $orderDetail['refer_id'],
                    'commission' => $orderDetail['commission'],
                    'staff_id' => $param['staff_id'],
                    'is_active' => 1,
                    'is_deleted' => 0,
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

                $filter = [
                    'created_at' => $orderDetail['created_at']
                ];
                $listService = $this->productBonusService->checkService($orderDetail['product_id'],$filter);
                if (count($listService) != 0) {
                    $listServiceAdd = [];
                    foreach ($listService as $item) {
                        if ($item['price_standard'] / 100000 < 10) {
                            $randomString = $this->generateRandomString(12 - strlen('EVB0'.($item['price_standard'] / 100000).'_'));
                            $randomString = 'EVB0'.($item['price_standard'] / 100000).'_'.$randomString;
                        } else {
                            $randomString = $this->generateRandomString(12 - strlen('EVB'.($item['price_standard'] / 100000).'_'));
                            $randomString = 'EVB'.($item['price_standard'] / 100000).'_'.$randomString;
                        }


                        $listServiceAdd[] = [
                            'service_id' => $item['service_id'],
                            'service_voucher_template' => $item['service_voucher_template_id'],
                            'customer_id' => $orderDetail['customer_id'],
                            'type' => 'gift',
                            'serial' => $randomString,
                            'service_serial_status' => 'new',
                            'is_actived' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'actived_at' => Carbon::now(),
                            'created_by' => Auth::id(),
                            'updated_by' => Auth::id(),
                        ];
                    }
                    $this->serviceSerial->addServiceSerial($listServiceAdd);
                }


                $createCustomerContract = $this->customerContract->createCustomerContract($arrContract);

                if ($orderDetail['commission'] != null && $orderDetail['refer_id'] != null) {
                    $walletInfo = [
                        'customer_contract_id' => $createCustomerContract,
                        'customer_id' => $orderDetail['refer_id'],
                        'type' => 'bonus',
                        'total_money' => $orderDetail['commission'],
                        'created_at' => Carbon::now(),
                        'created_by' => $orderDetail['refer_id']
                    ];

                    $this->wallet->createWallet($walletInfo);
                }

                $this->customerContractLog($createCustomerContract);

                $vars = [
                    'contract_code' => $arrContract['customer_contract_code'],
                    'customer_name' => $orderDetail['customer_name'],
                    'staff' => Auth::user()['full_name'],
                    'confirm_date' => Carbon::now()->format('Y/m/d H:i:s'),
                ];

                $this->insertEmailApi($createCustomerContract,'contract','Tạo hợp đồng',$receipt['customer_email'],$vars);

                $detailCustomerBefore = $this->customer->getDetail($orderDetail['customer_id']);
//                tính tổng số tiền của hợp đồng theo customer_id
//                $totalMoneyContract = $this->customerContract->countMoneyContract($orderDetail['customer_id']);

//                tổng đầu tư
                $invested = $this->customerContract->getContractByCustomer($orderDetail['customer_id']);
                $totalMoneyContract = 0;
                foreach ($invested as $value) {
                    $totalMoneyContract += $value['total'];
                }
//                Lấy cách đổi điểm ở bảng setting
                $setting = $this->setting->getSettingByKey('money_point');
                $point = $totalMoneyContract/$setting['vset_setting_value'];
                $getMemberLevelId = $this->memberLevel->checkByPoint((int)$point);
                $arrOrderUpdate = [
                    'point' => (int)$point,
                    'member_level_id' => $getMemberLevelId == null ? 1 : $getMemberLevelId['member_level_id']
                ];

                $this->customer->updateCustomer($orderDetail['customer_id'],$arrOrderUpdate);
                $detailCustomerAfter = $this->customer->getDetail($orderDetail['customer_id']);

                if ($detailOrder['payment_method_id'] == 2) {
                    $listContractWithraw = $this->withrawRequest->getListIdRequestByGroup($param['withdraw_request_group_id_interest']);
                    if (count($listContractWithraw) != 0){
                        $listContractWithraw = collect($listContractWithraw)->pluck('customer_contract_id')->toArray();
                    }

                    if (count($listContractWithraw) != 0){
                        $rCustomerContractInterest = app()->get(CustomerContractInterestRepositoryInterface::class);
                        $rCustomerContractInterest->calculateContract('time',$listContractWithraw);
                    }
                }

                if ($detailCustomerBefore['member_level_id'] != $detailCustomerAfter['member_level_id']) {

                    $vars = [
                        'customer_name' => $detailCustomerAfter['full_name'],
                        'name_rank_old' => $detailCustomerBefore['member_levels_name'],
                        'name_rank_new' => $detailCustomerAfter['member_levels_name'],
                    ];

                    $this->sendNotificationAction('customer_ranking',$orderDetail['customer_id'],$detailCustomerAfter['member_level_id']);

                    $this->insertEmailApi($orderDetail['customer_id'],'rank-user','Thay đổi hạng thành viên',$receipt['customer_email'],$vars);
                }
            }
        } catch (\Exception $e){
            dd($e->getMessage());
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function sendNotificationAction($key,$customer,$object_id){
        $noti = [
            'key' => $key,
            'customer_id' => $customer,
            'object_id' => $object_id
        ];
        $this->jobsEmailLogApi->sendNotification($noti);
        return true;
    }

    public function createWallet($param)
    {
        try {
            DB::beginTransaction();

            $data = [
                'customer_id' => $param['customer_id'],
                'amount' => $param['total'],
                'staff_id'=> Auth::id(),
                'object_id' => $param['order_id'],
                'status' =>'unpaid',
                'created_at' => Carbon::now(),
                'created_by' => Auth::id(),
            ];

//            kiểm tra số tiền còn lại
            if($param['payment_method_id'] == 2 || $param['payment_method_id'] == 3){
                $total = 0;
                if ($param['payment_method_id'] == 2){
                    $total = $this->getTotalInterest($param['order_id']);
                }else {
                    $total = $this->getTotalCommission($param['order_id'],$param['customer_id']);
                }

                if ((double)$total < (double)$param['total']){
                    return [
                        'error' => true,
                        'message' => 'Không đủ tiền để xác nhận yêu cầu'
                    ];
                }
            }

            $idReceipt = $this->addReceipt($data);

            $arrWithrawRequest = [];
            $type = ['bond','saving'];
            $status = ['new'];
            $arrWithrawRequestGroup = $this->withrawRequestGroup->getListByOrderId($param['order_id'],$type,$status);
            if (count($arrWithrawRequestGroup) != 0) {
                $keyGroupId = collect($arrWithrawRequestGroup)->pluck('withdraw_request_group_id');
//        danh sách chi tiết tiền lãi
                $listWithrawRequest = $this->withrawRequest->getListByGroupId($keyGroupId);
                if(count($listWithrawRequest) != 0) {
                    $arrWithrawRequest = collect($listWithrawRequest)->groupBy('withdraw_request_group_id');
                }
            }

            if ($param['payment_method_id'] == 2) {
                $data = [
                    'receipt_id' => $idReceipt,
                    'order_id' => $param['order_id'],
                    'amount' => $param['total'],
                    'withdraw_request_group_id_interest' => isset($arrWithrawRequestGroup) ? $arrWithrawRequestGroup[0]['withdraw_request_group_id'] :'',
                    'receipt_type' => 'interest',
                    'staff_id' => $param['staff_id']
                ];
            } else {
                $data = [
                    'receipt_id' => $idReceipt,
                    'order_id' => $param['order_id'],
                    'amount' => $param['total'],
                    'withdraw_request_group_id_bonus' => isset($arrWithrawRequestGroup) ? $arrWithrawRequestGroup[0]['withdraw_request_group_id'] :'',
                    'receipt_type' => 'bonus',
                    'staff_id' => $param['staff_id']
                ];
            }

            if (isset($param['staff_id']) && $param['staff_id'] != null) {
                $orderDetail = $this->order->getDetail($param['order_id']);
                $staffInfo = $this->staff->getItem($param['staff_id']);
                $varsStaff = [
                    'order_code' => $orderDetail['order_code'],
                    'customer_name' => $orderDetail['full_name'],
                    'staff' => $staffInfo['full_name'],
                    'status' => 'Xác nhận',
                ];

                $arrInsertEmailStaff = [
                    'obj_id' => $param['order_id'],
                    'email_type' => "support-order",
                    'email_subject' => __('Hỗ trợ khách hàng'),
                    'email_to' =>  $staffInfo['email'],
                    'email_from' => env('MAIL_USERNAME'),
                    'email_params' => json_encode($varsStaff),
                ];
                $this->jobsEmailLogApi->addJob($arrInsertEmailStaff);
            }


            $this->createReceiptDetail($data);

            DB::commit();
            return [
                'error' => false,
                'message' => 'Xác nhận thành công'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Xác nhận thất bại',
                '__message' => $e->getMessage()
            ];
        }

    }

    public function insertEmailApi($idDetailRequest,$type,$subject,$email_to,$vars) {
        $insertEmail = [
            'obj_id' => $idDetailRequest,
            'email_type' => $type,
            'email_subject' => $subject,
            'email_from' => env('MAIL_USERNAME'),
            'email_to' => $email_to,
            'email_params' => json_encode($vars),
            'is_run' => 0
        ];

        $this->jobsEmailLogApi->addJob($insertEmail);

        return true;
    }

    public function createReceipt($param)
    {
        try {
            DB::beginTransaction();
//            Chi tiết order
            $data = [
                'customer_id' => $param['customer_id'],
                'amount' => $param['total'],
                'staff_id'=> Auth::id(),
                'object_id' => $param['order_id'],
                'status' =>'unpaid',
                'created_at' => Carbon::now(),
                'created_by' => Auth::id(),
            ];
            $this->addReceipt($data);
            // change status order
            $dataChange =[
                'order_id' =>$param['order_id'],
                'process_status' => 'confirmed',
                'staff_id' => $param['staff_id']
            ];
            $this->changeStatus($dataChange);
            DB::commit();
            return response()->json([
                'error' => 0,
                'message' => __('Thêm thành công')
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 0,
                'message' => __('Thêm  thất bại'),
                '_message' => $e->getMessage()
            ]);
        }
    }

    public function showPopupReceiptDetail($param)
    {
        $moneyReceipt = $this->getMoneyReceipt($param['order_id']);

//        Tổng tiền đã thanh toán
        $sumMoneyReceipt = $this->receiptDetail->sumMoneyReceipt($moneyReceipt['receipt_id']);
        if ($sumMoneyReceipt == null) {
            $sumMoneyReceipt = 0;
        } else {
            $sumMoneyReceipt = $sumMoneyReceipt['sum_receipt'];
        }
//        Danh sách tiền lãi và hoa hồng
        $arrWithrawRequestGroup = [];
        $arrWithrawRequest = [];
        $type = ['bond','saving'];
        $status = ['new'];
        $arrWithrawRequestGroup = $this->withrawRequestGroup->getListByOrderId($param['order_id'],$type,$status);
        if (count($arrWithrawRequestGroup) != 0) {
            $keyGroupId = collect($arrWithrawRequestGroup)->pluck('withdraw_request_group_id');
//        danh sách chi tiết tiền lãi
            $listWithrawRequest = $this->withrawRequest->getListByGroupId($keyGroupId);
            if(count($listWithrawRequest) != 0) {
                $arrWithrawRequest = collect($listWithrawRequest)->groupBy('withdraw_request_group_id');
            }
        }
        $view = view(
            'admin::buy-bonds-request.popup.create-receipt',
            [
                'moneyReceipt' => $moneyReceipt,
                'order_id' => $param['order_id'],
                'listWithrawRequestGroup' => $arrWithrawRequestGroup,
                'listWithrawRequest' => $arrWithrawRequest,
                'sumMoneyReceipt' => $sumMoneyReceipt
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function getTotalInterest($order_id)
    {
//        $listId = $this->withrawRequest->getListIdContract($order_id,['voucher','cash']);
        $listId = $this->withrawRequest->getListIdContract($order_id,['cash']);
        if (count($listId) == 0) {
            return 0;
        }
//        tổng tiền lãi các hợp đồng
        $totalMoney = 0;
//        Tổng tiền rút của hợp đồng
        $totalRequest = 0;
        $arrIdContract = collect($listId)->pluck('customer_contract_id');
//        Lấy tổng số tiền lãi theo id contract
//        $totalTmp = $this->contractInterestMonth->totalBonusCustomerArr($arrIdContract);

        foreach ($arrIdContract as $item) {
            $lastInterest = $this->contractInterestTime->getInterestTotalInterest($item);
            if ($lastInterest != null) {
                $totalMoney += intval($lastInterest->interest_banlance_amount);
            }
        }

//        if ($totalTmp == null || $totalTmp['interest_banlance_amount'] == null) {
//            $totalMoney += 0;
//        } else {
//            $totalMoney += $totalTmp['interest_banlance_amount'];
//        }

//        Lấy tổng giá trị sử dụng từ ví lãi
        $totalWithdraw = $this->withrawRequestGroup->getWithdrawByGroupCustomerId($arrIdContract,['interest']);
//        return $totalMoney - $totalWithdraw;
        return $totalMoney;
    }

    public function getTotalCommission($order_id,$customer_id)
    {
//        Tính tổng tiền thưởng
        $totalBonus = 0;
//        $totalBonusTmp = $this->customerContract->totalBonusCustomer($customer_id);
        $totalBonusTmp = $this->wallet->totalBonusCustomer($customer_id);
        if ($totalBonusTmp != null || $totalBonusTmp['total_commission'] != null) {
            $totalBonus = $totalBonusTmp['total_commission'];
        }

//        Tổng tiền khách hàng đã rút bằng ví thưởng
        $totalWithdraw = 0;
        $totalWithdrawTmp = $this->withrawRequestGroup->getWithdrawByType($customer_id,'bonus');

        return $totalBonus - $totalWithdrawTmp;
    }

    public function getTotalCommissionWallet($order_id,$customer_id)
    {
//        Tính tổng tiền thưởng
        $totalBonus = 0;
        $type = ['bonus','register','refer'];
        $totalBonusTmp = $this->wallet->totalBonusCustomer($customer_id,$type);
        if ($totalBonusTmp != null || $totalBonusTmp['total_commission'] != null) {
            $totalBonus = $totalBonusTmp['total_commission'];
        }

//        Tổng tiền khách hàng đã rút bằng ví thưởng
        $totalWithdraw = 0;
        $totalWithdrawTmp = $this->withrawRequestGroup->getWithdrawByType($customer_id,'bonus');

        return $totalBonus - $totalWithdrawTmp;
    }


    /**
     * Upload image dropzone
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function uploadDropzone($input)
    {

        $time = Carbon::now();
        // Requesting the file from the form
        $image = $input['file'];

        // Getting the extension of the file
        $extension = $image->getClientOriginalExtension();
        //tên của hình ảnh
        $filename = $image->getClientOriginalName();
        //$filename = time() . str_random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . time() . "." . $extension;
        // This is our upload main function, storing the image in the storage that named 'public'
        $upload_success = $image->storeAs(TEMP_PATH, $filename, 'public');
        // If the upload is successful, return the name of directory/filename of the upload.
        if ($upload_success) {
            return response()->json($filename, 200);
        } // Else, return error 400
        else {
            return response()->json('error', 400);
        }
    }

    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    /**
     * Log customer contract
     * @param $id
     */
    private function customerContractLog($id)
    {
        //Model customer contract
        $mCustomerContact = new CustomerContactTable();
        //Model customer contract log
        $mCustomerContactLog = new CustomerContactLogTable();
        //Chi tiết của customer contract
        $customerContract = $mCustomerContact->getItem($id);
        if ($customerContract['customer_contract_end_date'] != null) {
            //Số tháng đầu tư
            $investmentTime = $customerContract['investment_time'];
        } else {
            //Số tháng đầu tư
            $investmentTime = 60;
            $customerContract['customer_contract_end_date'] = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])->addMonth($investmentTime);
        }

        //Kỳ hạn rút lãi -> là số tháng
        $withdrawInterestTime = $customerContract['withdraw_interest_time'];
        //Số record
        $record = (int)($investmentTime / $withdrawInterestTime);
        //Số tháng dư
        $residual = $investmentTime % $withdrawInterestTime;
        //Năm bắt đầu hợp đồng
        $year = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
            ->format('Y');
        //Tháng bắt đầu hợp đồng
        $month = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
            ->format('m');
        //Năm kết thúc hợp đồng
        $yearEnd = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
            ->format('Y');
        //Tháng bắt đầu hợp đồng
        $monthEnd = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
            ->format('m');
        $dayEnd = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
            ->format('d');
        $tmpD = Carbon::parse($yearEnd.'-'.$monthEnd)->endOfMonth()->format('d');
        if ($dayEnd > 30) {
            $dayEnd = $tmpD;
        }

        $weekEnd = Carbon::parse($yearEnd.'-'.$monthEnd.'-'.$dayEnd)->weekOfYear;

        $fulltime = $startTime = Carbon::createFromFormat(
            'Y-m-d', $yearEnd.'-'.$monthEnd.'-'.$dayEnd)
            ->format('Y-m-d');
        $data = [];
        if ($record > 0) {
            $mT = 0;
            $interest = $withdrawInterestTime * $customerContract['month_interest'];
            for ($i = 0; $i < $record; $i++) {
                //Tháng lãi tiếp theo
                $mT += $withdrawInterestTime;
                $dt = Carbon::create($year, $month, 1, 0);
                //Cộng thêm tháng
                $addMonth = $dt->addMonths($mT);
                $y = $startTime = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $addMonth)
                    ->format('Y');
                $m = (int)$startTime = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $addMonth)
                    ->format('m');
//                $d = (int)$startTime = Carbon::createFromFormat(
//                    'Y-m-d H:i:s', $addMonth)
//                    ->format('d');
                $d = $dayEnd;
//                    $d = (int)$startTime = Carbon::createFromFormat(
//                        'Y-m-d H:i:s', $addMonth)
//                        ->format('d');
                $tmpDay = Carbon::parse($y.'-'.$m)->endOfMonth()->format('d');
                if ($dayEnd > 30) {
                    $d = $tmpDay;
                }
                $w = Carbon::parse($y.'-'.$m.'-'.$d)->weekOfYear;
                $full = $startTime = Carbon::createFromFormat(
                    'Y-m-d', $y.'-'.$m.'-'.$d)
                    ->format('Y-m-d');

                if ($m == 2 && $d > 28) {
                    $d = Carbon::parse($y.'-'.$m)->endOfMonth()->format('d');
                    $w = Carbon::parse($y.'-'.$m.'-'.$d)->weekOfYear;
                    $full = $startTime = Carbon::createFromFormat(
                        'Y-m-d', $y.'-'.$m.'-'.$d)
                        ->format('Y-m-d');
                }

                $wY = $w.'/'.$y;
                if ($w == 53 && $m == 1) {
                    $wY = $w.'/'.($y-1);
                }

                $data[] = [
                    'customer_contract_id' => $customerContract['customer_contract_id'],
                    'customer_id' => $customerContract['customer_id'],
                    'year' => $y,
                    'month' => $m,
                    'day' => $d,
                    'week' => $w,
                    'week_year' => $wY,
                    'full_time' => $full,
                    'interest' => $interest
                ];
            }
        }
        //Lần cuối tính lãi
        if ($residual > 0) {
            $dEND = $dayEnd;
            if ($weekEnd == 2 && $dEND > 28) {
                $dEND = Carbon::parse($yearEnd.'-'.$weekEnd)->endOfMonth()->format('d');
                $weekEnd = Carbon::parse($yearEnd.'-'.$monthEnd.'-'.$dEND)->weekOfYear;
                $fulltime = $startTime = Carbon::createFromFormat(
                    'Y-m-d', $yearEnd.'-'.$monthEnd.'-'.$dEND)
                    ->format('Y-m-d');
            }

            $wYear = $weekEnd.'/'.$yearEnd;
            if ($weekEnd == 53 && $monthEnd == 1) {
                $wYear = $weekEnd.'/'.($yearEnd-1);
            }

            $data[] = [
                'customer_contract_id' => $id,
                'customer_id' => $customerContract['customer_id'],
                'year' => $yearEnd,
                'month' => $monthEnd,
                'day' => $dEND,
                'week' => $weekEnd,
                'week_year' => $wYear,
                'full_time' => $fulltime,
                'interest' => $residual * $customerContract['month_interest']
            ];
        }

        if ($data != []) {
            $mCustomerContactLog->addInsert($data);
        }
    }

    public function printBill($data)
    {
        $data = [
            'investment_unit' => strip_tags($data['investment_unit']),
            'user_name' => strip_tags($data['user_name']),
            'cmnd' => strip_tags($data['cmnd']),
            'created' => strip_tags($data['created']),
            'issued_by' => strip_tags($data['issued_by']),
            'address' => strip_tags($data['address']),
            'phone' => strip_tags($data['phone']),
            'reason' => strip_tags($data['reason']),
            'order_code' => strip_tags($data['order_code']),
            'payment_amount' => strip_tags($data['payment_amount']),
            'payment_amount_text' => strip_tags($data['payment_amount_text']),
            'name_collector' => strip_tags($data['name_collector']),
            'phone_collector' => strip_tags($data['phone_collector']),
        ];
        $view = view('admin::buy-bonds-request.bill.cash',['data' => $data])->render();
        return [
            'view' => $view
        ];
    }

    public function updateCommission()
    {
        $listContract = $this->customerContract->getAllContractBonus();
        $bonus = [];
        foreach ($listContract as $item) {
            if ($item['refer_id'] != null) {
                $bonus[] = [
                    'customer_contract_id' => $item['customer_contract_id'],
                    'customer_id' => $item['refer_id'],
                    'type' => 'bonus',
                    'total_money' => $item['commission'],
                    'created_at' => $item['created_at'],
                    'created_by' => $item['refer_id']
                ];
            }
        }
        $this->wallet->createMultipleWallet($bonus);
    }

    public function getServiceBonus($id)
    {
        return $this->productBonusServiceLog->getAllByOrder($id);
    }

//    Không xác nhận yêu cầu
    public function cancelOrder($param)
    {
        try {
            $data = [
                'process_status' => 'ordercancle',
                'reason_vi' => strip_tags($param['reason_vi']),
                'reason_en' => strip_tags($param['reason_en']),
                'staff_id' => isset($param['staff_id']) ? $param['staff_id'] : null
            ];
            $updateOrder = $this->order->updateOrder($param['order_id'],$data);

            // get detail order
            $detail = $this->show($param['order_id']);
            // get detail customer
            $mCustomer = new CustomerTable();
            $mCustomer  = $mCustomer->getItem($detail['customer_id']);

            /// // gọi api lưu data send mail
            $vars = [
                'customer_name' => $mCustomer['full_name'],
                'order_type' => $detail['type_bonds'],
                'order_code' => $detail['order_code'],
                'status'      =>  'Không xác nhận',
                'confirm_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'staff' => Auth::user()['full_name'],
                'reason' => strip_tags($param['reason_vi'])
            ];

            $arrInsertEmail = [
                'obj_id' => $param['order_id'],
                'email_type' => "confirm-order-cancel",
                'email_subject' => __('Không xác nhận mua gói Đầu tư/tiết kiệm'),
                'email_to' =>  $mCustomer['email'],
                'email_from' => env('MAIL_USERNAME'),
                'email_params' => json_encode($vars),
            ];
            $this->jobsEmailLogApi->addJob($arrInsertEmail);

            if (isset($data['staff_id']) && $data['staff_id'] != null) {
                $staffInfo = $this->staff->getItem($data['staff_id']);
                $varsStaff = [
                    'order_code' => $detail['order_code'],
                    'customer_name' => $mCustomer['full_name'],
                    'staff' => $staffInfo['full_name'],
                    'status' => 'Không xác nhận',
                    'reason' => strip_tags($param['reason_vi'])
                ];

                $arrInsertEmailStaff = [
                    'obj_id' => $param['order_id'],
                    'email_type' => "support-order",
                    'email_subject' => __('Hỗ trợ khách hàng'),
                    'email_to' =>  $staffInfo['email'],
                    'email_from' => env('MAIL_USERNAME'),
                    'email_params' => json_encode($varsStaff),
                ];
                $this->jobsEmailLogApi->addJob($arrInsertEmailStaff);
            }

            if ($detail['product_category_id'] == 1) {
                $this->sendNotificationAction('order_bond_status_C',$detail['customer_id'],$param['order_id']);
            } else if($detail['product_category_id'] == 2) {
                $this->sendNotificationAction('order_saving_status_C',$detail['customer_id'],$param['order_id']);
            }

            return [
                'error' => false,
                'message' => 'Không xác nhận yêu cầu thành công'
            ];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => 'Không xác nhận yêu cầu thất bại'
            ];
        }
    }
}