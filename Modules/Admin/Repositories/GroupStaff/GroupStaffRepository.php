<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/24/2018
 * Time: 10:40 AM
 */

namespace Modules\Admin\Repositories\GroupStaff;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Libs\FunctionLib;
use Modules\Admin\Models\GroupCustomerMapCustomerTable;
use Modules\Admin\Models\GroupCustomerTable;
use Modules\Admin\Models\GroupStaffMapCustomerTable;
use Modules\Admin\Models\GroupStaffMapStaffTable;
use Modules\Admin\Models\GroupStaffTable;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;

class GroupStaffRepository implements GroupStaffRepositoryInterface
{
    use FunctionLib;

    protected $rStaff;
    protected $mGroupStaff;
    protected $mGroupStaffMapStaff;
    protected $mGroupStaffMapCustomer;
    protected $mGroupCustomer;
    protected $rCustomer;
    protected $rCustomerMapCustomer;

    const IS_DELETED = 1;
    //Loại nhóm là xem tất cả
    const ALL = 'all';
    const GROUP = 'group';
    const DETAIL = 'detail';

    public function __construct(
        StaffRepositoryInterface $rStaff,
        GroupStaffTable $mGroupStaff,
        GroupStaffMapStaffTable $mGroupStaffMapStaff,
        GroupStaffMapCustomerTable $mGroupStaffMapCustomer,
        GroupCustomerTable $mGroupCustomer,
        CustomerRepositoryInterface $rCustomer,
        GroupCustomerMapCustomerTable $rCustomerMapCustomer
    ) {
        $this->rStaff = $rStaff;
        $this->mGroupStaff = $mGroupStaff;
        $this->mGroupStaffMapStaff = $mGroupStaffMapStaff;
        $this->mGroupStaffMapCustomer = $mGroupStaffMapCustomer;
        $this->mGroupCustomer = $mGroupCustomer;
        $this->rCustomer = $rCustomer;
        $this->rCustomerMapCustomer = $rCustomerMapCustomer;
    }

    /**
     * Danh sách nhân viên
     * @param $params
     * @return mixed|string
     * @throws \Throwable
     */
    public function loadStaff($params)
    {
        try {
            $this->stripTagData($params);
            $unique = $params['unique'];
            $data = session()->get($unique);
            $getListStaff = $this->mGroupStaffMapStaff->getListStaffId(isset($params['group_staff_id']) ? $params['group_staff_id'] : null);
            if (count($getListStaff) != 0) {
                $getListStaff = collect($getListStaff)->pluck('staff_id')->toArray();
            }
            $itemSelected = $data['item_selected'] ?? [0];
            $itemTemp = $data['item_temp'] ?? [];
            $page    = (int) ($params['page'] ?? 1);
            $isItemSelected = $params['isItemSelected'] ?? 0;
            $filter = [
                'not_in' => $itemSelected,
//                'staffs$is_actived' => $params['isActived'] ?? null,
                'staffs$is_actived' => 1,
                'staffs$is_deleted' => 0,
                //Keyword tên, email
                'keyword_FN_E' => $params['keyword'] ?? null,
                'page' => $page,
                'getListStaff' => $getListStaff,
                'is_group' => 1,
            ];
            $show = $params['show'] ?? 0;
            if ($isItemSelected == 1) {
                unset($filter['not_in']);
                unset($filter['getListStaff']);
                $filter['in'] = $itemSelected;
            }
            $page    = (int) ($params['page'] ?? 1);
            $perpage = (int) ($params['perpage'] ?? PAGING_ITEM_PER_PAGE);
            $list = $this->rStaff->list($filter);
            $currentPage = $list->toArray()['last_page'];
            if ($page > $currentPage) {
                $filter['page'] = $page - 1;
                $list = $this->rStaff->list($filter);
            }
            //Render view để append
            $view = view('admin::group-staff.append.list-staff', [
                'list' => $list,
                'itemTemp' => $itemTemp,
                'isItemSelected' => $isItemSelected,
                'page' => $page,
                'perpage' => $perpage,
                'show' => $show,
            ])
                ->render();
            return $view;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Thêm nhân viên vào session - tạm
     * @param array $params
     *
     * @return array|mixed
     */
    public function checkedItem($params = [])
    {
        $this->stripTagData($params);
        $arrayItem = $params['array_item'] ?? [];
        if ($arrayItem != []) {
            $type = $params['type'];
            $unique = $params['unique'];
            $data = session()->get($unique);
            $list = $data['item_temp'] ?? [];
            //Checked
            if ($type == 'checked') {
                $list = array_merge($list, $arrayItem);
            } else { //Uncheck
                foreach ($list as $key => $value) {
                    foreach ($arrayItem as $k => $v) {
                        if ($v == $value) {
                            unset($list[$key]);
                        }
                    }
                }
            }
            $data['item_temp'] = $list;
            session()->put($unique, $data);
            return $list;
        }
    }

    /**
     * Xóa hết khách hàng từ session - tạm
     * @param array $params
     * @return mixed
     */
    public function removeSessionTemp($params = [])
    {
        $this->stripTagData($params);
        $unique = $params['unique'];
        $data = session()->get($unique);
        $data['item_temp'] = [];
        session()->put($unique, $data);
        return $data;
    }

    /**
     * Thêm khách hàng từ session tạm và session selected
     * @param array $params
     * @return mixed
     */
    public function submitAddItem($params = [])
    {
        $this->stripTagData($params);
        $unique = $params['unique'];
        $data = session()->get($unique);
        $itemSelected = $data['item_selected'] ?? [];
        $itemTemp = $data['item_temp'] ?? [];
        if ($itemTemp != []) {
            $itemSelected = array_merge($itemSelected, $itemTemp);
            $data['item_selected'] = $itemSelected;
            $data['item_temp'] = [];
            session()->put($unique, $data);
        }
        return $data;
    }

    /**
     * Xóa khách hàng đã chọn ra khỏi session selected
     * @param array $params
     * @return mixed
     */
    public function removeItem($params = [])
    {
        $this->stripTagData($params);
        $unique = $params['unique'];
        $id = $params['id'];
        $data = session()->get($unique);
        $itemSelected = $data['item_selected'] ?? [];
        if ($itemSelected != []) {
            foreach ($itemSelected as $key => $item) {
                if ($id == $item) {
                    unset($itemSelected[$key]);
                }
            }
            $data['item_selected'] = $itemSelected;
            session()->put($unique, $data);
        }
        return $data;
    }


    /**
     * Thêm mới nhóm khách hàng
     * @param $params
     * @return array
     */
    public function store($params)
    {
        DB::beginTransaction();
        try {
            $this->stripTagData($params);
            $unique = $params['unique'];
            $data = session()->get($unique);
            $itemSelected = $data['item_selected'] ?? [];
            if ($itemSelected == []) {
                return [
                    'error' => true,
                    'array_error' => [
                        __('admin::validation.group_staff.not_item_selected')
                    ]
                ];
            } else {
                // 1 khách hàng chỉ xuất hiện 1 lần trong array selected
                $itemSelected = array_keys(array_flip($itemSelected));
            }
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $currentUser = Auth::id();
            $dataGroupCustomer = [
                'group_name' => $params['group_name'],
                'type' => self::ALL,
                'is_active' => 1,
                'is_deleted' => 0,
                'created_at' => $now,
                'created_by' => $currentUser,
                'updated_at' => $now,
                'updated_by' => $currentUser
            ];
            $groupCustomerId = $this->mGroupStaff->add($dataGroupCustomer);
            $dataGroupDetail = [];
            foreach ($itemSelected as $key => $value) {
                $dataGroupDetail[] = [
                    'group_staff_id' => $groupCustomerId,
                    'staff_id' => $value,
                ];
                if (count($dataGroupDetail) == 500) {
                    $this->mGroupStaffMapStaff->addInsert($dataGroupDetail);
                    $dataGroupDetail = [];
                }
            }
            if ($dataGroupDetail != []) {
                $this->mGroupStaffMapStaff->addInsert($dataGroupDetail);
            }
            DB::commit();
            return [
                'error' => false,
                'array_error' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Set session default các nv của nhóm nv
     * @param $id
     * @param $unique
     * @return mixed
     */
    public function setDefaultItem($id, $unique)
    {
        $customerByGroup = $this->mGroupStaffMapStaff->getByGroup($id)
            ->pluck('staff_id')->toArray();
        $data['item_selected'] = $customerByGroup;
        session()->put($unique, $data);
    }

    public function getItem($id)
    {
        $result = $this->mGroupStaff->getItem($id);
        return $result;
    }

    /**
     * Chỉnh sửa
     * @param $params
     * @return array
     */
    public function update($params)
    {
        DB::beginTransaction();
        try {
            $this->stripTagData($params);
            $id = $params['group_staff_id'];
            $unique = $params['unique'];
            $data = session()->get($unique);
            $itemSelected = $data['item_selected'] ?? [];
            if ($itemSelected == []) {
                return [
                    'error' => true,
                    'array_error' => [
                        __('admin::validation.group_staff.not_item_selected')
                    ]
                ];
            } else {
                // 1 khách hàng chỉ xuất hiện 1 lần trong array selected
                $itemSelected = array_keys(array_flip($itemSelected));
            }
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $currentUser = Auth::id();
            $dataGroupCustomer = [
                'group_name' => $params['group_name'],
//                'is_active' => $params['is_active'],
                'is_active' => 1,
                'updated_at' => $now,
                'updated_by' => $currentUser
            ];
            $this->mGroupStaff->edit($id, $dataGroupCustomer);
            $this->mGroupStaffMapStaff->remove($id);
            //Thêm ds nv vào db
            $dataGroupDetail = [];
            foreach ($itemSelected as $key => $value) {
                $dataGroupDetail[] = [
                    'group_staff_id' => $id,
                    'staff_id' => $value,
                ];
                if (count($dataGroupDetail) == 500) {
                    $this->mGroupStaffMapStaff->addInsert($dataGroupDetail);
                    $dataGroupDetail = [];
                }
            }
            if ($dataGroupDetail != []) {
                $this->mGroupStaffMapStaff->addInsert($dataGroupDetail);
            }
            DB::commit();
            return [
                'error' => false,
                'array_error' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Danh sách
     * @param $params
     * @return mixed
     */
    public function getList($params)
    {
        $list = $this->mGroupStaff->getList($params);
        return $list;
    }

    /**
     * Xóa
     * @param $params
     */
    public function destroy($params)
    {
        $id = $params['id'];
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $currentUser = Auth::id();
        $dataGroup = [
            'is_deleted' => self::IS_DELETED,
            'updated_at' => $now,
            'updated_by' => $currentUser
        ];
//        $this->mGroupStaff->edit($id, $dataGroup);
        $this->mGroupStaff->deleteStaff($id);
        $this->mGroupStaffMapStaff->deleteStaff($id);
        $this->mGroupStaffMapCustomer->deleteStaff($id);
    }

    /**
     * Option nhóm kh và kh
     * @return array|mixed
     */
    public function getOption($id)
    {
        $listGroup = $this->mGroupStaffMapCustomer->getListCustomerByRequest($id,'group');
        $listCustomerGroup = [];
//        if(count($listGroup) != 0) {
//            $listGroup = collect($listGroup)->pluck('obj_id')->toArray();
//            $listCustomerGroup = $this->rCustomerMapCustomer->getCustomerByGroupArr($listGroup);
//            if (count($listCustomerGroup) != 0) {
//                $listCustomerGroup = collect($listCustomerGroup)->pluck('customer_id')->toArray();
//            }
//        }
        $listCustomerGroup = $this->rCustomerMapCustomer->getCustomerByGroupArr();
        if (count($listCustomerGroup) != 0) {
            $listCustomerGroup = collect($listCustomerGroup)->pluck('customer_id')->toArray();
        }
        $listCustomer = $this->mGroupStaffMapCustomer->getListCustomerByRequest($id,'detail');
        if (count($listCustomer) != 0) {
            $listCustomer = collect($listCustomer)->pluck('obj_id')->toArray();
        }
        $listCustomer = array_merge($listCustomer,$listCustomerGroup);
        if (count($listGroup) != 0) {
            $listGroup = collect($listGroup)->pluck('obj_id')->toArray();
        }
        $group = $this->mGroupCustomer->getCondition([],$listGroup);
        $filter = ['perpage' => 1000000000, 'listCustomerNotIn' => $listCustomer];
        $customer = $this->rCustomer->list($filter);
        return [
            'group' => $group,
            'customer' => $customer,
        ];
    }

    /**
     * Danh sách nhóm khách hàng/ khách hàng của nhóm
     * @param $id
     * @return mixed
     */
    public function getMapCustomer($id)
    {
        $result = $this->mGroupStaffMapCustomer->getByGroup($id)
        ->pluck('group_staff_id', 'obj_id')->toArray();
        return $result;
    }
    /**
     * Update lại quyền của nhóm nhân viên
     * @param $params
     * @return array
     */
    public function updateAuthorization($params)
    {
        DB::beginTransaction();
        try {
            $this->stripTagData($params);
            $id = $params['group_staff_id'];
            $type = $params['type'];
            $groupCustomer = $params['group_customer'] ?? [];
            $customer = $params['customer'] ?? [];
            if ($type != self::ALL) {
                if ($type == self::GROUP) {
                    if ($groupCustomer == []) {
                        return [
                            'error' => true,
                            'array_error' => [
                                __('admin::validation.group_staff.not_group_customer_selected')
                            ]
                        ];
                    }
                } elseif ($type == self::DETAIL) {
                    if ($customer == []) {
                        return [
                            'error' => true,
                            'array_error' => [
                                __('admin::validation.group_staff.not_customer_selected')
                            ]
                        ];
                    }
                }
            }
            $this->mGroupStaffMapCustomer->remove($id);
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $currentUser = Auth::id();
            $dataGroup = [
                'type' => $type,
                'updated_at' => $now,
                'updated_by' => $currentUser
            ];
            $this->mGroupStaff->edit($id, $dataGroup);
            if ($type == self::GROUP) {
                $dataMap = [];
                foreach ($groupCustomer as $key => $value) {
                    $dataMap[] = [
                        'group_staff_id' => $id,
                        'obj_id' => $value,
                        'created_at' => $now,
                        'created_by' => $currentUser,
                    ];
                    if (count($dataMap) == 500) {
                        $this->mGroupStaffMapCustomer->addInsert($dataMap);
                        $dataMap = [];
                    }
                }
                if ($dataMap != []) {
                    $this->mGroupStaffMapCustomer->addInsert($dataMap);
                }
            } elseif ($type == self::DETAIL) {
                $dataMap = [];
                foreach ($customer as $key => $value) {
                    $dataMap[] = [
                        'group_staff_id' => $id,
                        'obj_id' => $value,
                        'created_at' => $now,
                        'created_by' => $currentUser,
                    ];
                    if (count($dataMap) == 500) {
                        $this->mGroupStaffMapCustomer->addInsert($dataMap);
                        $dataMap = [];
                    }
                }
                if ($dataMap != []) {
                    $this->mGroupStaffMapCustomer->addInsert($dataMap);
                }
            }
            DB::commit();
            return [
                'error' => false,
                'array_error' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }
}