<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/15/2021
 * Time: 9:34 AM
 * @author nhandt
 */

namespace Modules\Admin\Repositories\TransferRequest;


interface TransferRequestRepoInterface
{
    public function getList(array &$filters = []);
    public function getDetailTransfer($stock_order_id);
    public function changeStatus($param);
}