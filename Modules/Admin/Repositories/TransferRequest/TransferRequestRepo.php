<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/15/2021
 * Time: 9:35 AM
 * @author nhandt
 */


namespace Modules\Admin\Repositories\TransferRequest;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockOrderTable;
use Modules\Admin\Models\StockTransactionTable;
use Modules\Admin\Models\StockWalletTable;
use Modules\Admin\Repositories\Message\MessageRepoInterface;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;

class TransferRequestRepo implements TransferRequestRepoInterface
{
    private $stockOrders;
    private $stockRepo;
    public function __construct(StockOrderTable $stockOrders, StockRepositoryInterface  $stockRepo)
    {
        $this->stockOrders = $stockOrders;
        $this->stockRepo = $stockRepo;
    }

    public function getList(array &$filters = [])
    {
        return $this->stockOrders->getList($filters);
    }
    public function getDetailTransfer($stock_order_id)
    {
        return $this->stockOrders->getDetailTransfer($stock_order_id);
    }
    public function changeStatus($param)
    {
        try{
            $keyNoti = "";
            $message = "";
            $stockOrder = $this->stockOrders->getDetailTransfer($param['stock_order_id']);
            DB::beginTransaction();
            if($param['process_status'] == 'cancel'){
                $keyNoti = "stock_admin_transfer_cancel";
                $dataUpdate = [
                    'process_status' => 'cancel',
                    'reason_vi' => $param['reason_vi'],
                    'reason_en' => $param['reason_en']
                ];
                $this->stockOrders->updateStockOrder($param['stock_order_id'],$dataUpdate);//
                DB::commit();
                $message = "Huỷ xác nhận thành công";
//
            }
            else{
                $keyNoti = "stock_admin_transfer_order";
                $message = "Xác nhận thành công";
                $stockOrderId = $param['stock_order_id'];
                $transferId = $param['obj_id'];
                $receiverId = $param['customer_id'];
                $stockCustomerTable = new StockCustomerTable();
                $stockWalletTable = new StockWalletTable();
                $stockTransactionTable = new StockTransactionTable();
                $currentstockWalletTransfer = $stockCustomerTable->getStockWalletMoney($transferId);
                $currentStockTransfer = $stockCustomerTable->getStock($transferId);
                $currentStockWalletReceiver = $stockCustomerTable->getStockWalletMoney($receiverId);
                $currentStockReceiver = $stockCustomerTable->getStock($receiverId);
                $stockOrder = $this->stockOrders->getDetailTransfer($param['stock_order_id']);
                if(!$stockCustomerTable->checkExistsCustomer($receiverId)){
                    $arrStockCustomerReceiver = [
                        'customer_id' => $receiverId,
                        'wallet_stock_money' => 0,
                        'wallet_dividend_money' => 0,
                        'stock' => 0,
                        'stock_bonus' => 0,
                        'created_at' => Carbon::now(),
                        'created_by' => \auth()->id(),
                    ];
                    $stockCustomerTable->createStockCustomer($arrStockCustomerReceiver);
                }
                // ghi nhận cổ phiếu người nhận, người chuyển stock_transaction (không quan tâm hình thức chuyển)
                $arrStockTransactionReceiver = [
                    'source' => 'transfer',
                    'type' => 'add',
                    'stock_order_id' => $stockOrderId,
                    'customer_id' => $receiverId,
                    'obj_id' => $transferId,
                    'quantity_before' => (int)$currentStockReceiver['stock'],
                    'quantity' => (int)$param['quantity'],
                    'quantity_after' => (int)$currentStockReceiver['stock'] + (int)$param['quantity'],
                    'money_before' => $param['money_standard'],
                    'money_standard' => '10000',
                    'payment_method_id' => $param['payment_method_id'],
                    'date' => Carbon::now()->format('d'),
                    'month' => Carbon::now()->format('m'),
                    'year' => Carbon::now()->format('Y'),
                    'created_at' => Carbon::now(),
                    'created_by' => \auth()->id(),
                ];
                $stockTransactionTable->createTransaction($arrStockTransactionReceiver);
                $arrStockTransactionTransfer = [
                    'source' => 'transfer',
                    'type' => 'minus',
                    'stock_order_id' => $stockOrderId,
                    'customer_id' => $transferId,
                    'obj_id' => $receiverId,
                    'quantity_before' => (int)$currentStockTransfer['stock'],
                    'quantity' => (int)$param['quantity'],
                    'quantity_after' => (int)$currentStockTransfer['stock'] - (int)$param['quantity'],
                    'money_before' => $param['money_standard'],
                    'money_standard' => '10000',
                    'payment_method_id' => $param['payment_method_id'],
                    'date' => Carbon::now()->format('d'),
                    'month' => Carbon::now()->format('m'),
                    'year' => Carbon::now()->format('Y'),
                    'created_at' => Carbon::now(),
                    'created_by' => \auth()->id(),
                ];
                $stockTransactionTable->createTransaction($arrStockTransactionTransfer);

                // cộng cổ phiếu người nhận stock_customer (ko quan tâm PTTT)
                $arrStockCustomerReceiver = [
                    'stock' => (int)$currentStockReceiver['stock'] + (int)$param['quantity'],
                    'last_updated' => Carbon::now(),
                    'created_at' => Auth()->id()
                ];
                $stockCustomerTable->updateStockOrDividendWallet($arrStockCustomerReceiver,$receiverId);
                // nếu HTTT là ví cổ phiếu
                if($param['payment_method_id'] == 5){
                    // ghi nhận tiền người chuyển stock_wallet
                    $arrStockWalletTransfer = [
                        'customer_id' => $transferId,
                        'type' => 'add',
                        'source' => 'stock',
                        'total_before' => $currentstockWalletTransfer['wallet_stock_money'],
                        'total_money' => (float)$param['fee']*(float)$param['total']/100,
                        'total_after' => $currentstockWalletTransfer['wallet_stock_money'] - (float)$param['fee']*(float)$param['total']/100,
                        'day' => Carbon::now()->format('d'),
                        'month' => Carbon::now()->format('m'),
                        'year' => Carbon::now()->format('Y'),
                        'created_at' => Carbon::now(),
                        'created_by' => \auth()->id()
                    ];
                    $stockWalletTransferId = $stockWalletTable->createStockWallet($arrStockWalletTransfer);

                    // trừ cổ phiếu, cộng tiền người chuyển stock_customer
                    $arrStockCustomerTransfer = [
                        'wallet_stock_money' => $currentstockWalletTransfer['wallet_stock_money'] - (float)$param['fee']*(float)$param['total']/100,
                        'stock' => (int)$currentStockTransfer['stock'] - (int)$param['quantity'],
                        'last_updated' => Carbon::now(),
                        'created_at' => Auth()->id()
                    ];
                    $stockCustomerTable->updateStockOrDividendWallet($arrStockCustomerTransfer,$transferId);
                }
                else{
                // nếu HTTT không là ví cổ phiếu

                    // trừ cổ phiếu người chuyển stock_customer
                    $arrStockCustomerTransfer = [
                        'stock' => (int)$currentStockTransfer['stock'] - (int)$param['quantity'],
                        'last_updated' => Carbon::now(),
                        'created_at' => Auth()->id()
                    ];
                    $stockCustomerTable->updateStockOrDividendWallet($arrStockCustomerTransfer,$transferId);
                }
                // cập nhật trạng thái order
                $dataUpdateStockOrder['process_status'] = 'paysuccess';
                $dataUpdateStockOrder['payment_date'] = Carbon::now();
                $updateStockOrder = $this->stockOrders->updateStockOrder($stockOrderId,$dataUpdateStockOrder);
            }
            DB::commit();
//            todo: Tạo hoặc cập nhật hợp đồng-----------------------


                if($stockOrder->source=='market' || $stockOrder->source=='transfer'){
                    $this->stockRepo->createOrUpdateStockContract($stockOrder->customer_id, $stockOrder->quantity);
//                  todo: Người bán hoặc chuyển nhượng thì trừ cổ phiếu---------
                    $this->stockRepo->createOrUpdateStockContract($stockOrder->obj_id,-($stockOrder->quantity));
                }else{
                    $this->stockRepo->createOrUpdateStockContract($stockOrder->customer_id, $stockOrder->quantity);
                }
//                end: Tạo hoặc cập nhật hợp đồng---------------------
//            // TODO: push noti
//            $mNoti = new SendNotificationApi();
//            $mNoti->sendNotification([
//                'key' => 'stock_admin_transfer_order',
//                'customer_id' => \auth()->id(),
//                'object_id' => $stockOrderId
//            ]);

//            todo: Send Email--------------------------------------------

            $messageRepo = app()->get(MessageRepoInterface::class);
                $mCustomer = new CustomerTable();
                $customerTransfer = $mCustomer->getDetail($stockOrder['stock_order_id']);
                $customerRecieve = $mCustomer->getDetail($stockOrder['obj_id']);

            $messageRepo->sendSMS(
                $keyNoti,
                [
                    'phone'=>$customerTransfer['phone']
                ],[
                    'stock_order_id'=>$stockOrder['stock_order_id'],
                    'full_name'=>$customerTransfer['full_name']
                ]
            );


            $messageRepo->sendSMS(
                    $keyNoti,
                [
                    'phone'=>$customerRecieve['phone']
                ],[
                    'stock_order_id'=>$stockOrder['stock_order_id'],
                    'full_name'=>$customerRecieve['full_name']
                ]
            );

            // TODO: push noti
            $mNoti = new JobsEmailLogApi();
            $data =[
                'key' =>$keyNoti,
                'customer_id' =>$stockOrder['obj_id'],
                'object_id' => $param['stock_order_id'],
                'stock_order_id'=>$param['stock_order_id']
            ];
            $mNoti->sendNotification($data);

//                end: push noti--------------------------
            return [
                'error' => false,
                'message' => $message
            ];
        }
        catch (\Exception $ex){
//            dd('loi: ', $ex->getMessage());
            DB::rollBack();
            return [
                'error' => true,
                'message' => $ex->getMessage()
            ];
        }
    }
}