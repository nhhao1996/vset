<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/2/2018
 * Time: 12:13 PM
 */

namespace Modules\Admin\Repositories\Product;


interface ProductRepositoryInterface
{
    /**
     * Get product list
     *
     * @param array $filters
     */
    public function list(array $filters = []);

    /**
     * Xóa gói trái phiếu / tiết kiệm
     * @param $id
     * @return mixed
     */
    public function deleteProduct($id);

    /**
     * Lấy danh sách category product
     * @return mixed
     */
    public function getCategoryProduct();

    /**
     * Tạo gói
     * @param $data
     * @return mixed
     */
    public function createProduct($data);

    /**
     * Chỉnh sửa gói
     * @param $data
     * @return mixed
     */
    public function editProduct($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getItem($id);

    public function getListProductInterest($product_id);

    public function getProductBonus($product_id);

    /**
     * Lấy danh sách tặng thêm
     * @param $id
     * @return mixed
     */
    public function getListProductBonus($filter);

    /**
     * Thêm tiền thưởng
     * @param $data
     * @return mixed
     */
    public function addProductBonus($data);

    /**
     * Sửa tiền thưởng
     * @param $data
     * @return mixed
     */
    public function editProductBonus($data);

    /**
     * Cập nhật tiền thưởng
     * @param $data
     * @return mixed
     */
    public function editProductBonusPost($data);

    /**
     * Chi tiết tiền thưởng
     * @param $id
     * @return mixed
     */
    public function detailProductBonus($param);

    /**
     * Xóa tặng thêm
     * @param $id
     * @return mixed
     */
    public function removeProductBonus($id);

    /**
     * Lấy dánh sách lãi suất
     * @param $filter
     * @return mixed
     */
    public function getListInterestRate($filter);

    /**
     * Thêm lãi suất
     * @param $data
     * @return mixed
     */
    public function addProductInterest($data);

    /**
     * Chi tiết lãi suất
     * @param $param
     * @return mixed
     */
    public function detailProductInterest($param);

    /**
     * Chỉnh sửa lãi suất
     * @param $param
     * @return mixed
     */
    public function editProductInterest($param);

    /**
     * Cập nhật lãi suẩt
     * @param $param
     * @return mixed
     */
    public function editProductInterestPost($data);

    /**
     * Xóa lãi suất
     * @param $id
     * @return mixed
     */
    public function removeProductInterest($id);

    public function submitListInvestmentWithdraw($param);

    public function submitListProductBonus($param);

    /**
     * Lấy danh sách phương thức thanh toán
     * @return mixed
     */
    public function getListPaymentMethod();

    /**
     * Lấy danh sách kỳ hạn đầu tư
     * @return mixed
     */
    public function getListInvestmentTime($product_category_id);

    /**lấy danh sách kỳ hạn rút lãi
     * @return mixed
     */
    public function getListWithdrawInterestTime();

    public function getListImageContract($id);

    /**
     * Lấy cấu hình thưởng dịch vụ
     * @param $id
     * @return mixed
     */
    public function getConfigBonus($id);

    /**
     * Lấy danh sách dịch vụ
     * @param $id
     * @return mixed
     */
    public function getListService($id);

    /**
     * Lấy danh sách
     * @param $input
     * @return mixed
     */
    public function getListShowPopup($input);

    /**
     * Thêm dịch vụ
     * @param $param
     * @return mixed
     */
    public function addListService($data);

    /**
     * Lưu cấu hình
     * @param $param
     * @return mixed
     */
    public function updateConfig($param);

}