<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/2/2018
 * Time: 12:13 PM
 */

namespace Modules\Admin\Repositories\Product;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\InvestmentTimeTable;
use Modules\Admin\Models\PaymentMethodTable;
use Modules\Admin\Models\ProductBonusServiceConfigTable;
use Modules\Admin\Models\ProductBonusServiceTable;
use Modules\Admin\Models\ProductBonusTable;
use Modules\Admin\Models\ProductCategoryTable;
use Modules\Admin\Models\ProductChildTable;
use Modules\Admin\Models\ProductImageTable;
use Modules\Admin\Models\ProductInterestTable;
use Modules\Admin\Models\ProductTable;
use Modules\Admin\Models\ServiceTable;
use Modules\Admin\Models\WithdrawInterestTimeTable;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var ProductTable
     */
    protected $product;
    protected $categoryProduct;
    protected $productBonus;
    protected $productInterest;
    protected $paymentMethod;
    protected $investmentTime;
    protected $withdrawInterest;
    protected $timestamps = true;
    protected $productImage;
    protected $productBonusServiceConfig;
    protected $productBonusService;
    protected $service;

    public function __construct(
        ProductTable $product,
        ProductCategoryTable $categoryProduct,
        ProductBonusTable $productBonus,
        ProductInterestTable $productInterest,
        PaymentMethodTable $paymentMethod,
        InvestmentTimeTable $investmentTime,
        WithdrawInterestTimeTable $withdrawInterest,
        ProductImageTable $productImage,
        ProductBonusServiceConfigTable $productBonusServiceConfig,
        ProductBonusServiceTable $productBonusService,
        ServiceTable $service
    ) {
        $this->product = $product;
        $this->categoryProduct = $categoryProduct;
        $this->productBonus = $productBonus;
        $this->productInterest = $productInterest;
        $this->paymentMethod = $paymentMethod;
        $this->investmentTime = $investmentTime;
        $this->withdrawInterest = $withdrawInterest;
        $this->productImage = $productImage;
        $this->productBonusServiceConfig = $productBonusServiceConfig;
        $this->productBonusService = $productBonusService;
        $this->service = $service;
    }

    /**
     *get list product
     */
    public function list(array $filters = [])
    {
        if (isset($filters['created_at']) != "") {
            $filters['startTime']=[];
            $filters['endTime']=[];
            $arr_filter = explode(" - ", $filters["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTime'] =$startTime;
            $filters['endTime'] =$endTime;
            unset($filters['created_at']);
        }
        return $this->product->getList($filters);
    }

    public function deleteProduct($id)
    {
        return $this->product->deleteProduct($id);
    }

    public function getCategoryProduct()
    {
        return $this->categoryProduct->getAll();
    }

    public function createProduct($data)
    {
        try {
            $error = '' ;

            $data['price_standard'] = str_replace(',', '', $data["price_standard"]);
            $data['interest_rate_standard'] = str_replace(',', '', $data["interest_rate_standard"]);
            $data['withdraw_fee_rate_before'] = str_replace(',', '', $data["withdraw_fee_rate_before"]);
            $data['withdraw_fee_rate_ok'] = str_replace(',', '', $data["withdraw_fee_rate_ok"]);
            $data['withdraw_fee_interest_rate'] = str_replace(',', '', $data["withdraw_fee_interest_rate"]);
            $data['withdraw_min_amount'] = str_replace(',', '', $data["withdraw_min_amount"]);
//            $data['min_allow_sale'] = str_replace(',', '', $data["min_allow_sale"]);
            $data['withdraw_min_time'] = $data['product_category_id'] == 1 ? null : $data['withdraw_min_time'];

            if ($data['price_standard'] < 0 || $data['price_standard'] >= 1000000000000000) {
                $error = 'Giá trị gói phải trong khoảng 0 -> 999,999,999,999,999 <br>';
            }
            if ($data['interest_rate_standard'] < 0 || $data['interest_rate_standard'] > 100) {
                $error = $error.'Tỉ lệ lãi suất chuẩn phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_fee_rate_before'] < 0 || $data['withdraw_fee_rate_before'] > 100) {
                $error = $error.'Tỉ lệ phí rút tiền trước kỳ hạn phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_fee_rate_ok'] < 0 || $data['withdraw_fee_rate_ok'] > 100) {
                $error = $error.'Tỉ lệ phí rút tiền trước đúng kỳ hạn phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_fee_interest_rate'] < 0 || $data['withdraw_fee_interest_rate'] > 100) {
                $error = $error.'Tỉ lệ phí rút tiền lãi phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_min_amount'] < 0 || $data['withdraw_min_amount'] >= 1000000000000000) {
                $error = $error.'Số tiền tối thiểu có thể rút phải trong khoảng 0 -> 999,999,999,999,999 <br>';
            }
//            if ($data['min_allow_sale'] < 0 || $data['min_allow_sale'] >= 1000000000000000) {
//                $error = $error.'Giá trị thấp nhất có thể bán phải trong khoảng 0 -> 999,999,999,999,999 <br>';
//            }

            if ($data['bonus_extend'] < 0 || $data['bonus_extend'] >= 1000000000000000) {
                $error = $error.'Thưởng khi gia hạn hợp đồng phải trong khoảng 0 -> 999,999,999,999,999 <br>';
            }

            if ($error != '') {
                return [
                    'error' => true,
                    'message' => $error
                ];
            }

            if ($data['product_avatar_vi'] != '') {
                $data['product_avatar_vi'] = url('/').'/' .$this->transferTempfileToAdminfile($data['product_avatar_vi'], str_replace('', '', $data['product_avatar_vi']));
            }

            if ($data['product_image_detail_vi'] != '') {
                $data['product_image_detail_vi'] = url('/').'/' . $this->transferTempfileToAdminfile($data['product_image_detail_vi'], str_replace('', '', $data['product_image_detail_vi']));
            }

            if ($data['product_avatar_mobile_vi'] != '') {
                $data['product_avatar_mobile_vi'] = url('/').'/' .$this->transferTempfileToAdminfile($data['product_avatar_mobile_vi'], str_replace('', '', $data['product_avatar_mobile_vi']));
            }

            if ($data['product_image_detail_mobile_vi'] != '') {
                $data['product_image_detail_mobile_vi'] = url('/').'/' . $this->transferTempfileToAdminfile($data['product_image_detail_mobile_vi'], str_replace('', '', $data['product_image_detail_mobile_vi']));
            }


            if ($data['product_avatar_en'] != '') {
                $data['product_avatar_en'] = url('/').'/' .$this->transferTempfileToAdminfile($data['product_avatar_en'], str_replace('', '', $data['product_avatar_en']));
            }

            if ($data['product_image_detail_en'] != '') {
                $data['product_image_detail_en'] = url('/').'/' . $this->transferTempfileToAdminfile($data['product_image_detail_en'], str_replace('', '', $data['product_image_detail_en']));
            }

            if ($data['product_avatar_mobile_en'] != '') {
                $data['product_avatar_mobile_en'] = url('/').'/' .$this->transferTempfileToAdminfile($data['product_avatar_mobile_en'], str_replace('', '', $data['product_avatar_mobile_en']));
            }

            if ($data['product_image_detail_mobile_en'] != '') {
                $data['product_image_detail_mobile_en'] = url('/').'/' . $this->transferTempfileToAdminfile($data['product_image_detail_mobile_en'], str_replace('', '', $data['product_image_detail_mobile_en']));
            }


            $data['published_at'] = Carbon::createFromFormat('d/m/Y',$data['published_at'])->format('Y-m-d 0:00:00');
            $data['created_at'] = Carbon::now();
            $dataFull = [];
            $dataFull = $data;
            unset($data['arrImageContract']);

//            Tạo gói mới
            $create = $this->product->createProduct($data);

//            if (isset($dataFull['arrImageContract'])) {
//                $arrImage = [];
//                if (isset($dataFull['arrImageContract']['desktop'])) {
//                    foreach ($dataFull['arrImageContract']['desktop'] as $item) {
//                        $arrImage[] = [
//                            'product_id' => $create,
//                            'name' => url('/').'/' . $this->transferTempfileToAdminfile($item['image'], str_replace('', '', $item['image'])),
//                            'type' => 'desktop',
//                        ];
//                    }
//                }
//
//                if (isset($dataFull['arrImageContract']['mobile'])) {
//                    foreach ($dataFull['arrImageContract']['mobile'] as $item) {
//                        $arrImage[] = [
//                            'product_id' => $create,
//                            'name' => url('/').'/' . $this->transferTempfileToAdminfile($item['image'], str_replace('', '', $item['image'])),
//                            'type' => 'mobile',
//                        ];
//                    }
//                }
//
//                $createImage = $this->productImage->createImage($arrImage);
//            }

            return [
                'error' => false,
                'message' => 'Tạo gói thành công',
                'product_id' => $create
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
//                'message' => 'Tạo gói thất bại'
                'message' => $e->getMessage()
            ];
        }
    }

    public function editProduct($data)
    {
        try {
            $error = '' ;

            $data['interest_rate_standard'] = str_replace(',', '', $data["interest_rate_standard"]);
            $data['withdraw_fee_rate_before'] = str_replace(',', '', $data["withdraw_fee_rate_before"]);
            $data['withdraw_fee_rate_ok'] = str_replace(',', '', $data["withdraw_fee_rate_ok"]);
            $data['withdraw_fee_interest_rate'] = str_replace(',', '', $data["withdraw_fee_interest_rate"]);
            $data['withdraw_min_amount'] = str_replace(',', '', $data["withdraw_min_amount"]);
//            $data['min_allow_sale'] = str_replace(',', '', $data["min_allow_sale"]);
            $data['withdraw_min_time'] = $data['product_category_id'] == 1 ? null : $data['withdraw_min_time'];

//            if ($data['price_standard'] < 0) {
//                $error = 'Giá trị gói phải lớn hơn 0 <br>';
//            }

//            if ($data['price_standard'] <= 0 || $data['price_standard'] >= 1000000000000000) {
//                $error = 'Giá trị gói phải trong khoảng 1 -> 999,999,999,999,999 <br>';
//            }
            if ($data['interest_rate_standard'] < 0 || $data['interest_rate_standard'] > 100) {
                $error = $error.'Tỉ lệ lãi suất chuẩn phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_fee_rate_before'] < 0 || $data['withdraw_fee_rate_before'] > 100) {
                $error = $error.'Tỉ lệ phí rút tiền trước kỳ hạn phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_fee_rate_ok'] < 0 || $data['withdraw_fee_rate_ok'] > 100) {
                $error = $error.'Tỉ lệ phí rút tiền trước đúng kỳ hạn phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_fee_interest_rate'] < 0 || $data['withdraw_fee_interest_rate'] > 100) {
                $error = $error.'Tỉ lệ phí rút tiền lãi phải từ 0 đến 100 <br>';
            }
            if ($data['withdraw_min_amount'] < 0 || $data['withdraw_min_amount'] >= 1000000000000000) {
                $error = $error.'Số tiền tối thiểu có thể rút phải trong khoảng 0 -> 999,999,999,999,999 <br>';
            }
//            if ($data['min_allow_sale'] < 0 || $data['min_allow_sale'] >= 1000000000000000) {
//                $error = $error.'Giá trị thấp nhất có thể bán phải trong khoảng 0 -> 999,999,999,999,999 <br>';
//            }

            if ($data['bonus_extend'] < 0 || $data['bonus_extend'] >= 1000000000000000) {
                $error = $error.'Thưởng khi gia hạn hợp đồng phải trong khoảng 0 -> 999,999,999,999,999 <br>';
            }

            if ($error != '') {
                return [
                    'error' => true,
                    'message' => $error
                ];
            }

            DB::beginTransaction();
            $id= $data['product_id'];
            unset($data['product_id']);

            $checkSaveFirst = $this->productInterest->getListByProductId($id);

            if (count($checkSaveFirst) == 0 && $data['is_actived'] == '1'){
                return [
                    'error' => true,
                    'message' => 'Không thể thay đổi trạng thái khi chưa cấu hình lãi suất của gói'
                ];
            }

//            -----------------------------------------------------------------------------------------------------------
            //            Ảnh đại diện
            if ($data['product_avatar_vi'] != null) {
                $data['product_avatar_vi'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_avatar_vi'],
                        str_replace('', '', $data['product_avatar_vi'])
                    );
            } else {
                $data['product_avatar_vi'] = $data['product_avatar_hidden_vi'];
            }
//            Ảnh chi tiết
            if ($data['product_image_detail_vi'] != null) {
                $data['product_image_detail_vi'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_image_detail_vi'],
                        str_replace('', '', $data['product_image_detail_vi'])
                    );
            } else {
                $data['product_image_detail_vi'] = $data['product_image_detail_hidden_vi'];
            }
            unset($data['product_avatar_hidden_vi']);
            unset($data['product_image_detail_hidden_vi']);


//            Ảnh đại diện mobile
            if ($data['product_avatar_mobile_vi'] != null) {
                $data['product_avatar_mobile_vi'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_avatar_mobile_vi'],
                        str_replace('', '', $data['product_avatar_mobile_vi'])
                    );
            } else {
                $data['product_avatar_mobile_vi'] = $data['product_avatar_hidden_mobile_vi'];
            }
//            Ảnh chi tiết  mobile
            if ($data['product_image_detail_mobile_vi'] != null) {
                $data['product_image_detail_mobile_vi'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_image_detail_mobile_vi'],
                        str_replace('', '', $data['product_image_detail_mobile_vi'])
                    );
            } else {
                $data['product_image_detail_mobile_vi'] = $data['product_image_detail_hidden_mobile_vi'];
            }
            unset($data['product_avatar_hidden_mobile_vi']);
            unset($data['product_image_detail_hidden_mobile_vi']);
//            -----------------------------------------------------------------------------------------------------------
            //            Ảnh đại diện
            if ($data['product_avatar_en'] != null) {
                $data['product_avatar_en'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_avatar_en'],
                        str_replace('', '', $data['product_avatar_en'])
                    );
            } else {
                $data['product_avatar_en'] = $data['product_avatar_hidden_en'];
            }
//            Ảnh chi tiết
            if ($data['product_image_detail_en'] != null) {
                $data['product_image_detail_en'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_image_detail_en'],
                        str_replace('', '', $data['product_image_detail_en'])
                    );
            } else {
                $data['product_image_detail_en'] = $data['product_image_detail_hidden_en'];
            }
            unset($data['product_avatar_hidden_en']);
            unset($data['product_image_detail_hidden_en']);


//            Ảnh đại diện mobile
            if ($data['product_avatar_mobile_en'] != null) {
                $data['product_avatar_mobile_en'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_avatar_mobile_en'],
                        str_replace('', '', $data['product_avatar_mobile_en'])
                    );
            } else {
                $data['product_avatar_mobile_en'] = $data['product_avatar_hidden_mobile_en'];
            }
//            Ảnh chi tiết  mobile
            if ($data['product_image_detail_mobile_en'] != null) {
                $data['product_image_detail_mobile_en'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $data['product_image_detail_mobile_en'],
                        str_replace('', '', $data['product_image_detail_mobile_en'])
                    );
            } else {
                $data['product_image_detail_mobile_en'] = $data['product_image_detail_hidden_mobile_en'];
            }
            unset($data['product_avatar_hidden_mobile_en']);
            unset($data['product_image_detail_hidden_mobile_en']);
//            -----------------------------------------------------------------------------------------------------------

            $data['published_at'] = Carbon::createFromFormat('d/m/Y',$data['published_at'])->format('Y-m-d 0:00:00');
//            $data['price_standard'] = str_replace(',', '', $data["price_standard"]);
            $data['interest_rate_standard'] = str_replace(',', '', $data["interest_rate_standard"]);
            $data['withdraw_fee_rate_before'] = str_replace(',', '', $data["withdraw_fee_rate_before"]);
            $data['withdraw_fee_rate_ok'] = str_replace(',', '', $data["withdraw_fee_rate_ok"]);
            $data['withdraw_fee_interest_rate'] = str_replace(',', '', $data["withdraw_fee_interest_rate"]);
            $data['withdraw_min_amount'] = str_replace(',', '', $data["withdraw_min_amount"]);
//            $data['withraw_min_bonus'] = str_replace(',', '', $data["withraw_min_bonus"]);
//            $data['min_allow_sale'] = str_replace(',', '', $data["min_allow_sale"]);
            $data['bonus_extend'] = str_replace(',', '', $data["bonus_extend"]);
            $data['withdraw_min_time'] = $data['product_category_id'] == 1 ? null : $data['withdraw_min_time'];

            $dataFull = [];
            $dataFull = $data;
            unset($data['arrImageContract']);

            $this->product->editProduct($id,$data);
//            if (isset($dataFull['arrImageContract'])) {
//                $arrImage = [];
//                if (isset($dataFull['arrImageContract']['desktop'])) {
//                    foreach ($dataFull['arrImageContract']['desktop'] as $item) {
//                        $arrImage[] = [
//                            'product_id' => $id,
//                            'name' => $item['created'] == 0 ? $item['image'] : url('/').'/' . $this->transferTempfileToAdminfile($item['image'], str_replace('', '', $item['image'])),
//                            'type' => 'desktop',
//                        ];
//                    }
//                }
//
//                if (isset($dataFull['arrImageContract']['mobile'])) {
//                    foreach ($dataFull['arrImageContract']['mobile'] as $item) {
//                        $arrImage[] = [
//                            'product_id' => $id,
//                            'name' => $item['created'] == 0 ? $item['image'] : url('/').'/' . $this->transferTempfileToAdminfile($item['image'], str_replace('', '', $item['image'])),
//                            'type' => 'mobile',
//                        ];
//                    }
//                }
//                $deleteImage = $this->productImage->deleteByProductId($id);
//                $createImage = $this->productImage->createImage($arrImage);
//            }

            DB::commit();

            $result = [
                'error' => false,
                'message' => 'Cập nhật gói thành công'
            ];
            return $result;
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
            $result = [
                'error' => true,
                'message' => 'Cập nhật gói thất bại'
            ];
            return $result;
        }
    }

    public function getItem($id)
    {
        return $this->product->getDetail($id);
    }

    public function getListProductInterest($product_id)
    {
        $list = $this->productInterest->getListByProductId($product_id);

        if (count($list) != 0) {
            $arr = [];
            foreach ($list as $item) {
                $arr[$item['investment_time_id'].'-'.$item['withdraw_interest_time_id'].'-'.$item['term_time_type']] = $item;
            }
            return $arr;
        } else {
            return $list;
        }
    }

    public function getProductBonus($product_id)
    {
        $list = $this->productBonus->getListProductBonusByProductId($product_id);
        if (count($list) != 0 ) {
            $arr = [];
            foreach ($list as $value) {
                $arr[$value['payment_method_id'].'_'.$value['investment_time_id']] = $value;
            }
            return $arr;
        } else {
            return $list;
        }
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    public function getListProductBonus($filter)
    {
         $listProductBonus = $this->productBonus->getListProductBonus($filter);
        $view = view(
            'admin::product.table.product-bonus',
            [
                'list' => $listProductBonus,
                'filter' => $filter,
                'page' => $filter['page']
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function addProductBonus($data)
    {
//        kiểm tra dữ liệu trùng
        $check = $this->productBonus->checkValue($data);
        if (count($check) != 0) {
            return response()->json([
                'error' => true,
                'message' => 'Hình thức thanh toán và kỳ hạn đầu tư bị trùng'
            ]);
        }

        $data['bonus_rate'] = strip_tags(str_replace(',', '', $data['bonus_rate']));
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        $data['created_by'] = Auth::id();
        $data['updated_by'] = Auth::id();

        $addProductBonus = $this->productBonus->addProductBonus($data);

        return response()->json([
            'error' => false,
            'message' => 'Thêm tiền thưởng thành công'
        ]);
    }

    public function editProductBonus($data)
    {
        $detailProductBonus = $this->productBonus->getDetail($data['id']);
        $detailProduct = $this->getItem($data['idProduct']);
        $listPaymentMethod = $this->getListPaymentMethod();
        $listInvestmentTime = $this->getListInvestmentTime($detailProduct['product_category_id']);
        $view = view(
            'admin::product.popup.product-bonus-popup-edit',
            [
                'detailProductBonus' => $detailProductBonus,
                'listPaymentMethod' => $listPaymentMethod,
                'listInvestmentTime' => $listInvestmentTime
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function editProductBonusPost($data)
    {
        //        kiểm tra dữ liệu trùng
        $check = $this->productBonus->checkValue($data);
        if (count($check) != 0) {
            return response()->json([
                'error' => true,
                'message' => 'Hình thức thanh toán và kỳ hạn đầu tư bị trùng'
            ]);
        }

        $data['bonus_rate'] = strip_tags(str_replace(',', '', $data['bonus_rate']));
        $data['updated_at'] = Carbon::now();
        $data['updated_by'] = Auth::id();
        $id = $data['product_bonus_id'];
        unset($data['product_bonus_id']);
        $addProductBonus = $this->productBonus->updateProductBonus($id,$data);

        return response()->json([
            'error' => false,
            'message' => 'Chỉnh sửa tiền thưởng thành công'
        ]);
    }

    public function detailProductBonus($param)
    {
        $detailProductBonus = $this->productBonus->getDetail($param['id']);
        $detailProduct = $this->getItem($param['idProduct']);
        $listPaymentMethod = $this->getListPaymentMethod();
        $listInvestmentTime = $this->getListInvestmentTime($detailProduct['product_category_id']);
        $view = view(
            'admin::product.popup.product-bonus-popup-detail',
            [
                'detailProductBonus' => $detailProductBonus,
                'listPaymentMethod' => $listPaymentMethod,
                'listInvestmentTime' => $listInvestmentTime
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function removeProductBonus($id)
    {
        $delete = $this->productBonus->removeProductBonus($id);
        return response()->json([
            'error' => false,
        ]);
    }

    public function getListInterestRate($filter)
    {
        $listInterestRate = $this->productInterest->getListInterestRate($filter);
        $view = view(
            'admin::product.table.interest-rate',
            [
                'list' => $listInterestRate,
                'filter' => $filter,
                'page' => $filter['page']
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function addProductInterest($data)
    {
        $check = $this->productInterest->checkProductInterest($data);
        if (count($check) != 0){
            return response()->json([
                'error' => true,
                'message' => 'Kỳ hạn đầu tư bị trùng'
            ]);
        }

        $data['interest_rate'] = strip_tags(str_replace(',', '', $data['interest_rate']));
        $data['commission_rate'] = strip_tags(str_replace(',', '', $data['commission_rate']));
        $data['month_interest'] = strip_tags(str_replace(',', '', $data['month_interest']));
        $data['total_interest'] = strip_tags(str_replace(',', '', $data['total_interest']));
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        $data['created_by'] = Auth::id();
        $data['updated_by'] = Auth::id();

        if ($data['is_default_display'] == 1) {
//            Cập nhật sản phẩm hiển thị chính
            $this->productInterest->updateWithdrawInterestDisplay($data['product_id']);
        }

        $this->productInterest->createWithdrawInterest($data);

        return response()->json([
            'error' => false,
            'message' => 'Tạo lãi suất thành công'
        ]);
    }

    public function detailProductInterest($param)
    {
        $detailProductInterest = $this->productInterest->getDetail($param['id']);
        $detailProduct = $this->getItem($param['idProduct']);
        $listPaymentMethod = $this->getListPaymentMethod();
        $listInvestmentTime = $this->getListInvestmentTime($detailProduct['product_category_id']);
        $withdrawInterestTime = $this->getListWithdrawInterestTime();
        $view = view(
            'admin::product.popup.product-interest-popup-detail',
            [
                'detailProductInterest' => $detailProductInterest,
                'listPaymentMethod' => $listPaymentMethod,
                'listInvestmentTime' => $listInvestmentTime,
                'withdrawInterestTime' => $withdrawInterestTime
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function editProductInterest($param)
    {
        $detailProductInterest = $this->productInterest->getDetail($param['id']);
        $detailProduct = $this->getItem($param['idProduct']);
        $listPaymentMethod = $this->getListPaymentMethod();
        $listInvestmentTime = $this->getListInvestmentTime($detailProduct['product_category_id']);
        $withdrawInterestTime = $this->getListWithdrawInterestTime();
        $view = view(
            'admin::product.popup.product-interest-popup-edit',
            [
                'detailProductInterest' => $detailProductInterest,
                'listPaymentMethod' => $listPaymentMethod,
                'listInvestmentTime' => $listInvestmentTime,
                'withdrawInterestTime' => $withdrawInterestTime
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function editProductInterestPost($data)
    {
        try {
            DB::beginTransaction();
            $check = $this->productInterest->checkProductInterest($data);
            if (count($check) != 0) {
                return response()->json([
                    'error' => true,
                    'message' => 'Kỳ hạn đầu tư bị trùng'
                ]);
            }

            $data['interest_rate'] = strip_tags(str_replace(',', '', $data['interest_rate']));
            $data['commission_rate'] = strip_tags(str_replace(',', '', $data['commission_rate']));
            $data['month_interest'] = strip_tags(str_replace(',', '', $data['month_interest']));
            $data['total_interest'] = strip_tags(str_replace(',', '', $data['total_interest']));
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            $data['created_by'] = Auth::id();
            $data['updated_by'] = Auth::id();

            if ($data['is_default_display'] == 1) {
                //            Cập nhật sản phẩm hiển thị chính
                $this->productInterest->updateWithdrawInterestDisplay($data['product_id']);
            }

            $id = $data['product_interest_id'];
            $this->productInterest->updateWithdrawInterest($id, $data);
            DB::commit();
            return response()->json([
                'error' => false,
                'message' => 'Chỉnh sửa lãi suất thành công'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => false,
                'message' => 'Chỉnh sửa lãi suất thất bại'
            ]);
        }
    }

    public function removeProductInterest($id)
    {
        $delete = $this->productInterest->removeProductInterest($id);
        return response()->json([
            'error' => false,
        ]);
    }

    public function submitListInvestmentWithdraw($param)
    {
        try {
            DB::beginTransaction();
            $id = null;
            $check = 0;
            $checkSaveFirst = $this->productInterest->getListByProductId($param['product_id']);
            foreach ($param['product'] as $keyTmp => $itemTmp) {
                if($keyTmp == 1) {
                    foreach ($itemTmp as $key => $item) {
                        foreach ($item as $keyItem => $value){
                            $tmp = [];
                            if ($value['commission_rate'] == null || $value['interest_rate'] == null ) {
                                DB::rollBack();
                                return [
                                    'error' => true,
                                    'message' => 'Yêu cầu nhập tỉ lệ hoa hồng hoặc tỉ lệ lãi suất'
                                ];
                            } else if ($value['commission_rate'] < 0 || $value['interest_rate'] < 0 ) {
                                DB::rollBack();
                                return [
                                    'error' => true,
                                    'message' => 'Yêu cầu tỉ lệ hoa hồng hoặc tỉ lệ lãi suất phải lớn hơn 0'
                                ];
                            } else {
                                if ($value['product_interest_id'] != null) {
                                    $tmp = [
                                        'product_id' => $param['product_id'],
                                        'term_time_type' => isset($value['term_time_type']) ? $value['term_time_type'] : 1,
                                        'investment_time_id' => $key,
                                        'withdraw_interest_time_id' => $keyItem,
                                        'interest_rate' => $value['interest_rate'],
                                        'commission_rate' => $value['commission_rate'],
                                        'month_interest' => $value['month_interest'],
                                        'total_interest' => $value['total_interest'],
                                        'is_default_display' => isset($value['is_default_display']) ? 1 : 0,
                                        'is_deleted' => 0,
                                        'is_actived' => 1,
                                        'updated_at' => Carbon::now(),
                                        'updated_by' => Auth::id(),
                                    ];
                                    $this->productInterest->updateWithdrawInterest($value['product_interest_id'],$tmp);
                                    if (isset($value['is_default_display'])) {
                                        $check = 1;
                                    }
                                    if ($id == null){
                                        $id = $value['product_interest_id'];
                                    }
                                } else {
                                    $tmp = [
                                        'product_id' => $param['product_id'],
                                        'term_time_type' => isset($value['term_time_type']) ? $value['term_time_type'] : 1,
                                        'investment_time_id' => $key,
                                        'withdraw_interest_time_id' => $keyItem,
                                        'interest_rate' => $value['interest_rate'],
                                        'commission_rate' => $value['commission_rate'],
                                        'month_interest' => $value['month_interest'],
                                        'total_interest' => $value['total_interest'],
                                        'is_default_display' => isset($value['is_default_display']) ? 1 : 0,
                                        'is_deleted' => 0,
                                        'is_actived' => 1,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        'created_by' => Auth::id(),
                                        'updated_by' => Auth::id(),
                                    ];
                                    $idInterest = $this->productInterest->createWithdrawInterest($tmp);
                                    if (isset($value['is_default_display'])) {
                                        $check = 1;
                                    }
                                    if ($id == null){
                                        $id = $idInterest;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    foreach ($itemTmp as $keyItem => $value){
                        $tmp = [];
                        if ($value['commission_rate'] == null || $value['interest_rate'] == null ) {
                            DB::rollBack();
                            return [
                                'error' => true,
                                'message' => 'Yêu cầu nhập tỉ lệ hoa hồng hoặc tỉ lệ lãi suất'
                            ];
                        } else if ($value['commission_rate'] < 0 || $value['interest_rate'] < 0 ) {
                            DB::rollBack();
                            return [
                                'error' => true,
                                'message' => 'Yêu cầu tỉ lệ hoa hồng hoặc tỉ lệ lãi suất phải lớn hơn 0'
                            ];
                        } else {
                            $tmp = [
                                'product_id' => $param['product_id'],
                                'term_time_type' => isset($value['term_time_type']) ? $value['term_time_type'] : 1,
                                'investment_time_id' => null,
                                'withdraw_interest_time_id' => $keyItem,
                                'interest_rate' => $value['interest_rate'],
                                'commission_rate' => $value['commission_rate'],
                                'month_interest' => $value['month_interest'],
                                'total_interest' => null,
                                'is_default_display' => isset($value['is_default_display']) ? 1 : 0,
                                'is_deleted' => 0,
                                'is_actived' => 1,
                                'updated_at' => Carbon::now(),
                                'updated_by' => Auth::id(),
                            ];
                            if ($value['product_interest_id'] != null) {
                                $this->productInterest->updateWithdrawInterest($value['product_interest_id'],$tmp);
                                if (isset($value['is_default_display'])) {
                                    $check = 1;
                                }
                                if ($id == null){
                                    $id = $value['product_interest_id'];
                                }
                            } else {
                                $idInterest = $this->productInterest->createWithdrawInterest($tmp);
                                if (isset($value['is_default_display'])) {
                                    $check = 1;
                                }
                                if ($id == null){
                                    $id = $idInterest;
                                }
                            }
                        }
                    }
                }

            }

            if ($check == 0) {
                $this->productInterest->updateWithdrawInterest($id,['is_default_display' => 1]);
            }

            DB::commit();
            if (count($checkSaveFirst) != 0) {
                return [
                    'error' => false,
                    'message' => 'Cài đặt lãi suất thành công'
                ];
            } else {
                return [
                    'error' => false,
                    'message' => 'Cài đặt lãi suất thành công.Có thể thay đổi trạng thái của gói'
                ];
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'error' => true,
                'message' => 'Cài đặt lãi suất thất bại'
            ];
        }

    }

    public function submitListProductBonus($param)
    {
        try {
            DB::beginTransaction();
            foreach ($param['bonus'] as $key => $item) {
                foreach ($item as $keyItem => $value) {
                    $tmp = [];
                    if ($value == null || $value < 0) {
                        DB::rollBack();
                        return [
                            'error' => true,
                            'message' => 'Yêu cầu nhập kỳ hạn đầu tư và lớn hơn 0'
                        ];
                    } else {
                        if ($value['product_bonus'] != null) {
                            $tmp = [
                                'product_id' => $param['product_id'],
                                'payment_method_id' => $key,
                                'investment_time_id' => $keyItem,
                                'bonus_rate' => strip_tags(str_replace(',', '', $value['bonus_rate'])),
                                'is_deleted' => 0,
                                'is_actived' => 1,
                                'updated_at' => Carbon::now(),
                                'updated_by' => Auth::id(),
                            ];
                            $this->productBonus->updateProductBonus($value['product_bonus'],$tmp);
                        } else {
                            $tmp = [
                                'product_id' => $param['product_id'],
                                'payment_method_id' => $key,
                                'investment_time_id' => $keyItem,
                                'bonus_rate' => strip_tags(str_replace(',', '', $value['bonus_rate'])),
                                'is_deleted' => 0,
                                'is_actived' => 1,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'created_by' => Auth::id(),
                                'updated_by' => Auth::id(),
                            ];
                            $this->productBonus->addProductBonus($tmp);
                        }
                    }
                }
            }

            if (isset($param['bonus_withraw'])) {
                foreach ($param['bonus_withraw'] as $keyItem => $value) {
                    $tmp = [];
                    if ($value == null || $value < 0) {
                        DB::rollBack();
                        return [
                            'error' => true,
                            'message' => 'Yêu cầu nhập kỳ hạn đầu tư và lớn hơn 0'
                        ];
                    } else {
                        if ($value['product_bonus'] != null) {
                            $tmp = [
                                'product_id' => $param['product_id'],
                                'payment_method_id' => $keyItem,
                                'investment_time_id' => null,
                                'bonus_rate' => strip_tags(str_replace(',', '', $value['bonus_rate'])),
                                'is_deleted' => 0,
                                'is_actived' => 1,
                                'updated_at' => Carbon::now(),
                                'updated_by' => Auth::id(),
                            ];
                            $this->productBonus->updateProductBonus($value['product_bonus'],$tmp);
                        } else {
                            $tmp = [
                                'product_id' => $param['product_id'],
                                'payment_method_id' => $keyItem,
                                'investment_time_id' => null,
                                'bonus_rate' => strip_tags(str_replace(',', '', $value['bonus_rate'])),
                                'is_deleted' => 0,
                                'is_actived' => 1,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                                'created_by' => Auth::id(),
                                'updated_by' => Auth::id(),
                            ];
                            $this->productBonus->addProductBonus($tmp);
                        }
                    }
                }
            }


            DB::commit();
            return [
                'error' => false,
                'message' => 'Cài đặt tiền thưởng thành công'
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'error' => true,
                'message' => 'Cài đặt tiền thưởng thất bại'
            ];
        }
    }

    public function getListPaymentMethod()
    {
        return $this->paymentMethod->getAll();
    }

    public function getListInvestmentTime($product_category_id)
    {
        return $this->investmentTime->getAllByCategoryProduct($product_category_id);
    }

    public function getListWithdrawInterestTime()
    {
        return $this->withdrawInterest->getAll();
    }

    public function getListImageContract($id)
    {
        $mobile = [];
        $desktop = [];
        $list = $this->productImage->getImageByProductId($id);
        if (count($list) != 0){
            $list = collect($list)->groupBy('type');
            $mobile = isset($list['mobile']) ? $list['mobile'] : [];
            $desktop = isset($list['desktop']) ? $list['desktop'] : [];
        }
        return [
            'mobile' => $mobile,
            'desktop' => $desktop
        ];
    }

    public function getConfigBonus($id)
    {
        return $this->productBonusServiceConfig->getConfig($id);
    }

    public function getListService($id)
    {
        return $this->productBonusService->getListService($id);
    }

    public function getListShowPopup($input)
    {
        $list = $this->service->getListService($input);
        $view = view('admin::refer-source.popup.table',[
            'list' => $list,
            'filter' => $input
        ])->render();
        return [
            'view' => $view
        ];
    }

    public function addListService($data)
    {
        $listService = $this->service->getListServiceSession($data['listService']);
        $view = view('admin::refer-source.partial.table-tr',[
            'listService' => $listService,
        ])->render();
        return [
            'view' => $view
        ];
    }

    public function updateConfig($param)
    {
        if ($param['is_active'] == 1) {
            if ($param['date_action_register'] == null){
                return [
                    'error' => 1,
                    'message' => 'Yêu cầu chọn thời gian thưởng'
                ];
            }

            if (count($param['listService']) == 0) {
                return [
                    'error' => 1,
                    'message' => 'Yêu cầu thêm dịch vụ'
                ];
            }
        }

        $config['is_active'] = $param['is_active'];

        if ($param['date_action_register'] != null) {
            $arr_filter = explode(" - ",$param['date_action_register']);
            $config['start']  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
            $config['end']  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
        }

        $check = $this->productBonusServiceConfig->getConfig($param['product_id']);
        if ($check != null) {
            $this->productBonusServiceConfig->updateConfig($param['product_id'],$config);
        } else {
            $config['product_id'] = $param['product_id'];
            $this->productBonusServiceConfig->createdConfig($config);
        }

        $listService = [];
        if (count($param['listService']) != 0) {
            foreach ($param['listService'] as $item) {
                $listService[] = [
                    'product_id' => $param['product_id'],
                    'service_id' => $item,
                    'created_at' => Carbon::now(),
                    'created_by' => Auth::id()
                ];
            }
        }

        $this->productBonusService->deleteService($param['product_id']);
        $this->productBonusService->createdService($listService);
        return [
            'error' => 0,
            'message' => 'Cập nhật cấu hình thành công'
        ];
    }
}