<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\BuyBondsRequestFail;

use Carbon\Carbon;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\Order;
use Modules\Admin\Models\OrderFail;
use Modules\Admin\Models\ReceiptDetailImageTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReceiptTable;
use Modules\Admin\Models\VsetSettingTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;

class BuyBondsRequestFailRepository implements BuyBondsRequestFailRepositoryInterface
{

    protected $orderFail;
    protected $receiptTable;
    protected $receiptDetail;
    protected $receiptDetailImage;
    protected $withrawRequestGroup;
    protected $withrawRequest;
    protected $customerContract;
    protected $setting;
    protected $memberLevel;
    protected $customer;
    protected $jobsEmailLogApi;
    protected $contractInterestMonth;
    protected $contractInterestTime;
    public function __construct(
        OrderFail $orderFail,
        ReceiptTable $receiptTable,
        ReceiptDetailTable $receiptDetail,
        ReceiptDetailImageTable $receiptDetailImage,
        WithdrawRequestGroupTable $withdrawRequestGroup,
        WithdrawRequestTable $withrawRequest,
        CustomerContactTable $customerContract,
        VsetSettingTable $setting,
        MemberLevelTable $memberLevel,
        CustomerTable $customer,
        JobsEmailLogApi $jobsEmailLogApi,
        CustomerContractInterestByMonthTable $contractInterestMonth,
        CustomerContractInterestByTimetable $contractInterestTime
    )
    {
        $this->orderFail = $orderFail;
        $this->receiptTable= $receiptTable;
        $this->receiptDetail = $receiptDetail;
        $this->receiptDetailImage = $receiptDetailImage;
        $this->withrawRequestGroup = $withdrawRequestGroup;
        $this->withrawRequest = $withrawRequest;
        $this->customerContract = $customerContract;
        $this->setting = $setting;
        $this->memberLevel = $memberLevel;
        $this->customer = $customer;
        $this->jobsEmailLogApi = $jobsEmailLogApi;
        $this->contractInterestMonth = $contractInterestMonth;
        $this->contractInterestTime = $contractInterestTime;
    }

    //Hàm lấy danh sách
    public function list(array $filters = [])
    {
        $filters['is_extend'] = 0;
        return $this->orderFail->getList($filters);
    }

    //Hàm lấy danh sách
    public function listExport(array $filters = [])
    {
        $filters['is_extend'] = 0;
        return $this->orderFail->getListExport($filters);
    }

    public function show($id){
        return $this->orderFail->getById($id);
    }

    public function getTotalInterest($order_id)
    {
//        $listId = $this->withrawRequest->getListIdContract($order_id,['voucher','cash']);
        $listId = $this->withrawRequest->getListIdContract($order_id,['cash']);
        if (count($listId) == 0) {
            return 0;
        }
//        tổng tiền lãi các hợp đồng
        $totalMoney = 0;
//        Tổng tiền rút của hợp đồng
        $totalRequest = 0;
        $arrIdContract = collect($listId)->pluck('customer_contract_id');
//        Lấy tổng số tiền lãi theo id contract
//        $totalTmp = $this->contractInterestMonth->totalBonusCustomerArr($arrIdContract);

        foreach ($arrIdContract as $item) {
            $lastInterest = $this->contractInterestTime->getInterestTotalInterest($item);
            if ($lastInterest != null) {
                $totalMoney += intval($lastInterest->interest_banlance_amount);
            }
        }

//        if ($totalTmp == null || $totalTmp['interest_banlance_amount'] == null) {
//            $totalMoney += 0;
//        } else {
//            $totalMoney += $totalTmp['interest_banlance_amount'];
//        }

//        Lấy tổng giá trị sử dụng từ ví lãi
        $totalWithdraw = $this->withrawRequestGroup->getWithdrawByGroupCustomerId($arrIdContract,['interest']);
//        return $totalMoney - $totalWithdraw;
        return $totalMoney;
    }

    public function getTotalCommission($order_id,$customer_id)
    {
//        Tính tổng tiền thưởng
        $totalBonus = 0;
        $totalBonusTmp = $this->customerContract->totalBonusCustomer($customer_id);
        if ($totalBonusTmp != null || $totalBonusTmp['total_commission'] != null) {
            $totalBonus = $totalBonusTmp['total_commission'];
        }

//        Tổng tiền khách hàng đã rút bằng ví thưởng
        $totalWithdraw = 0;
        $totalWithdrawTmp = $this->withrawRequestGroup->getWithdrawByType($customer_id,'bonus');

        return $totalBonus - $totalWithdrawTmp;
    }
}