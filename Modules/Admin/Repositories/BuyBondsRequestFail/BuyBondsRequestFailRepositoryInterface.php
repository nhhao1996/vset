<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\BuyBondsRequestFail;


interface BuyBondsRequestFailRepositoryInterface
{
    public function list(array $filters = []);
    public function listExport(array $filters = []);
    public function show($id);
    public function getTotalInterest($order_id);
    public function getTotalCommission($order_id,$customer_id);

}