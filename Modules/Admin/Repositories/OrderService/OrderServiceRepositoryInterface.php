<?php

namespace Modules\Admin\Repositories\OrderService;
/**
 *Use Repository interface
 * @author ledangsinh
 * @since March 20, 2018
 */
interface OrderServiceRepositoryInterface
{
    /**
     * Get order reason cancel list
     * @param array $filters
     */
    public function listAction(array $filters = []);

    /**
     * Get item
     * @param $id
     * @return array
     */
    public function show($id);
    public function addReceipt(array $data);
    public function changeStatus($data);
    public function getMoneyReceipt($order_id);
    public function createWallet($param);
    public function getListReceiptDetail($param);
    public function addReceiptDetail($param);
    public function createReceipt($param);
    public function showPopupReceiptDetail($param);
    public function getTotalInterest($order_id);
    public function getTotalCommission($order_id,$customer_id);
    public function getTotalCommissionWallet($order_id,$customer_id);

    /**
     * Upload image dropzone
     *
     * @param $input
     * @return mixed
     */
    public function uploadDropzone($input);



}