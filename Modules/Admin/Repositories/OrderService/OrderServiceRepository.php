<?php
/**
 *Use Repository
 * @author ledangsinh
 * @since March 20, 2018
 */

namespace Modules\Admin\Repositories\OrderService;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\OrderServicesTable;
use Modules\Admin\Models\ReceiptDetailImageTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReceiptTable;
use Modules\Admin\Models\ServiceSerialTable;
use Modules\Admin\Models\WalletTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;

class OrderServiceRepository implements OrderServiceRepositoryInterface
{

    protected $orderService;
    protected $receiptTable;
    protected $receiptDetail;
    protected $receiptDetailImage;
    protected $jobsEmailLogApi;
    protected $serviceSerial;
    protected $withrawRequestGroup;
    protected $withrawRequest;
    protected $contractInterestMonth;
    protected $customerContract;
    protected $wallet;
    protected $contractInterestTime;

    public function __construct(
        OrderServicesTable $orderService,
        ReceiptTable $receiptTable,
        ReceiptDetailTable $receiptDetail,
        ReceiptDetailImageTable $receiptDetailImage,
        JobsEmailLogApi $jobsEmailLogApi,
        ServiceSerialTable $serviceSerial,
        WithdrawRequestGroupTable $withdrawRequestGroup,
        WithdrawRequestTable $withrawRequest,
        CustomerContractInterestByMonthTable $contractInterestMonth,
        CustomerContactTable $customerContract,
        WalletTable $wallet,
        CustomerContractInterestByTimetable $contractInterestTime
    )
    {
        $this->orderService = $orderService;
        $this->receiptTable= $receiptTable;
        $this->receiptDetail = $receiptDetail;
        $this->receiptDetailImage = $receiptDetailImage;
        $this->jobsEmailLogApi = $jobsEmailLogApi;
        $this->serviceSerial = $serviceSerial;
        $this->withrawRequestGroup = $withdrawRequestGroup;
        $this->withrawRequest = $withrawRequest;
        $this->contractInterestMonth = $contractInterestMonth;
        $this->customerContract = $customerContract;
        $this->wallet = $wallet;
        $this->contractInterestTime = $contractInterestTime;
    }

    /**
     * Get list order reason cancel
     */
    public function listAction(array $filters = [])
    {
        return $this->orderService->getList($filters);
    }

    public function show($id){
        return $this->orderService->getById($id);
    }

    // tạo phiếu thu
    public function addReceipt(array $data)
    {
        $data['receipt_code'] = $this->generateUniqueString(6);
        return $this->receiptTable->add($data);
    }

    // hàm tạo chuỗi ngẫu nhiên time now + uniquestring
    private function generateUniqueString()
    {
        while(true) {
            $now = Carbon::today()->format('Y/m/d');
            $to = Carbon::tomorrow()->format('Y/m/d');
            $str_length = 4;
            $reservations = ReceiptTable::where('created_at', '>=', $now)
                ->where('created_at', '<=', $to)
                ->count();
            $nowYmd = Carbon::today()->format('Ymd');
            $str = substr("0000{$reservations}", - $str_length);
            $randomString =$nowYmd.'_'.$str;
            $doesCodeExist = DB::table('receipts')
                ->where('receipt_code', $randomString)
                ->count();
            if (! $doesCodeExist) {
                return $randomString;
            }
        }

    }

    private function generateUniqueStringContract()
    {
        while(true) {
            $date  =  new \DateTime();
            $now = $date->getTimestamp();
            $randomString = 'CT'.$now.'_'.str_random(4);
            $doesCodeExist = DB::table('customer_contract')
                ->where('customer_contract_code', $randomString)
                ->count();

            if (! $doesCodeExist) {
                return $randomString;
            }
        }
    }
    // update status order

    public function changeStatus($data)
    {

        $this->orderService->edit($data['order_service_id'],['process_status' => $data['process_status']]);

        // get detail order
        $detail = $this->show($data['order_service_id']);
        // get detail customer
        $mCustomer = new CustomerTable();
        $mCustomer  = $mCustomer->getItem($detail['customer_id']);

        /// // gọi api lưu data send mail
        $vars = [
            'customer_name' => $mCustomer['full_name'],
            'order_code' => $detail['order_service_code'],
            'status'      =>  'Xác nhận',
            'confirm_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'staff' => Auth::user()['full_name']
        ];

        $arrInsertEmail = [
            'obj_id' => $data['order_service_id'],
            'email_type' => "confirm-order-service",
            'email_subject' => __('Xác nhận mua dịch vụ'),
            'email_from' => env('MAIL_USERNAME'),
            'email_to' =>  $mCustomer['email'],
            'email_params' => json_encode($vars),
        ];
        $this->jobsEmailLogApi->addJob($arrInsertEmail);
        $this->sendNotificationAction('order_service_status_A',$detail['customer_id'],$data['order_service_id']);

        return true;
    }

    public function getMoneyReceipt($order_id)
    {
        return $this->receiptTable->getReceiptOrderService($order_id);
    }

    public function createWallet($param)
    {
        try {
            DB::beginTransaction();

            $data = [
                'customer_id' => $param['customer_id'],
                'amount' => $param['total'],
                'staff_id'=> Auth::id(),
                'object_type' => 'order-service',
                'object_id' => $param['order_service_id'],
                'status' =>'unpaid',
                'created_at' => Carbon::now(),
                'created_by' => Auth::id(),
            ];

            if ($param['payment_method_id'] == 2 || $param['payment_method_id'] == 3) {
                $total = 0;
                if ($param['payment_method_id'] == 2) {
                    $total = $this->getTotalInterest($param['order_service_id']);
                } else {
                    $total = $this->getTotalCommission($param['order_service_id'],$param['customer_id']);
                }

                if ((double)$total < (double)$param['total']){
                    return [
                        'error' => true,
                        'message' => 'Không đủ tiền để xác nhận yêu cầu'
                    ];
                }
            }


            $idReceipt = $this->addReceipt($data);

            $arrWithrawRequest = [];
            $type = ['voucher'];
            $status = ['new'];
            $arrWithrawRequestGroup = $this->withrawRequestGroup->getListByOrderId($param['order_service_id'],$type,$status);
            if (count($arrWithrawRequestGroup) != 0) {
                $keyGroupId = collect($arrWithrawRequestGroup)->pluck('withdraw_request_group_id');
//        danh sách chi tiết tiền lãi
                $listWithrawRequest = $this->withrawRequest->getListByGroupId($keyGroupId);
                if(count($listWithrawRequest) != 0) {
                    $arrWithrawRequest = collect($listWithrawRequest)->groupBy('withdraw_request_group_id');
                }
            }
            if ($param['payment_method_id'] == 2) {
                $data = [
                    'receipt_id' => $idReceipt,
                    'order_service_id' => $param['order_service_id'],
                    'amount' => $param['total'],
                    'withdraw_request_group_id_interest' => isset($arrWithrawRequestGroup) && count($arrWithrawRequestGroup) != 0 ? $arrWithrawRequestGroup[0]['withdraw_request_group_id'] :'',
                    'receipt_type' => 'interest'
                ];
            } else {
                $data = [
                    'receipt_id' => $idReceipt,
                    'order_service_id' => $param['order_service_id'],
                    'amount' => $param['total'],
                    'withdraw_request_group_id_bonus' => isset($arrWithrawRequestGroup) && count($arrWithrawRequestGroup) != 0  ? $arrWithrawRequestGroup[0]['withdraw_request_group_id'] :'',
                    'receipt_type' => 'bonus'
                ];
            }


            $result = $this->createReceiptDetail($data);

            if ($result == false){
                DB::rollback();
                return [
                    'error' => true,
                    'message' => 'Xác nhận thất bại'
                ];
            }

            DB::commit();
            return [
                'error' => false,
                'message' => 'Xác nhận thành công'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Xác nhận thất bại'
            ];
        }
    }

    public function getListReceiptDetail($param)
    {
        $param['object_type'] = 'order-service';
        $list = $this->receiptDetail->getListReceipt($param);
        $arrDetailId = [];
        $arrDetailId = collect($list->toArray()['data'])->pluck('receipt_detail_id');
        $listImage = $this->receiptDetailImage->getListImage($arrDetailId);
        $arrGroupImage = [];
        if (count($listImage) != 0) {
            $arrGroupImage = collect($listImage)->groupBy('receipt_detail_id');
        }
        $view = view(
            'admin::order-service.partial.receipt-detail-tr',
            [
                'list' => $list,
                'arrGroupImage' => $arrGroupImage
            ]
        )->render();


        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function addReceiptDetail($param)
    {
        try {
            DB::beginTransaction();

            $this->createReceiptDetail($param);
            DB::commit();
            return [
                'error' => false,
                'message' => 'Tạo phiếu thu thành công'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Tạo phiếu thu thất bại'
            ];
        }
    }

    public function createReceiptDetail($param){
        try {
            $arr = [];
            $arrReceipt = [];
            $imgTransfer = [];
            $success = 0;
            $order_id = $param['order_service_id'];
            $receipt_id = $param['receipt_id'];
            unset($param['order_id']);
            unset($param['receipt_id']);
            //        lấy chi tiết receipt
            $receipt = $this->receiptTable->getDetail($receipt_id);
            $countMoney = 0;
            $amount_receipt_detail = 0;
            if ($receipt['amount_paid'] == null) {
                $receipt['amount_paid'] = 0;
            }

            if ($param['receipt_type'] == 'cash') {
                $countMoney = $receipt['amount_paid'] + (double)str_replace(',', '', $param['amount']);
                $amount_receipt_detail = (double)str_replace(',', '', $param['amount']);

            } else if ($param['receipt_type'] == 'transfer') {
                $countMoney = $receipt['amount_paid'] + (double)str_replace(',', '', $param['amount_transfer']);
                $amount_receipt_detail = (double)str_replace(',', '', $param['amount_transfer']);

            } else if($param['receipt_type'] == 'interest') {
//                Lấy thông tin nhóm
                $withrawRequestGroup = $this->withrawRequestGroup->getDetailById($param['withdraw_request_group_id_interest']);
                if ($withrawRequestGroup == null){
                    return false;
                }
//            $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['available_balance'];
                $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['withdraw_request_amount'];
//            $amount_receipt_detail = $withrawRequestGroup['available_balance'];
                $amount_receipt_detail = $withrawRequestGroup['withdraw_request_amount'];
//                Cập nhật lại trạng thái của withdraw group
                $this->withrawRequestGroup->changeStatus($param['withdraw_request_group_id_interest'],['withdraw_request_status' => 'done']);
                $this->withrawRequest->changeStatusByWithdrawGroup($param['withdraw_request_group_id_interest'],['withdraw_request_status' => 'done']);
            } else if($param['receipt_type'] == 'bonus') {
                $withrawRequestGroup = $this->withrawRequestGroup->getDetailById($param['withdraw_request_group_id_bonus']);
                if ($withrawRequestGroup == null){
                    return false;
                }
//            $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['available_balance'];
                $countMoney = $receipt['amount_paid'] + $withrawRequestGroup['withdraw_request_amount'];
//            $amount_receipt_detail = $withrawRequestGroup['available_balance'];
                $amount_receipt_detail = $withrawRequestGroup['withdraw_request_amount'];
                $this->withrawRequestGroup->changeStatus($param['withdraw_request_group_id_bonus'],['withdraw_request_status' => 'done']);
            }

            if (($countMoney - $receipt['amount']) < 0 ) {
                $arrReceipt = [
                    'amount_paid' => $countMoney,
                    'status' => 'part-paid'
                ];
                $orderStatus = 'pay-half';

                $arr = [
                    'receipt_type' => $param['receipt_type'],
                    'receipt_id' => $receipt_id,
                    'amount' => $amount_receipt_detail,
                    'staff_id' => Auth::id(),
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            } else {
                $arrReceipt = [
                    'amount_paid' => $receipt['amount'],
                    'amount_return' => $countMoney - $receipt['amount'],
                    'status' => 'paid'
                ];
                $orderStatus = 'paysuccess';

                $arr = [
                    'receipt_type' => $param['receipt_type'],
                    'receipt_id' => $receipt_id,
                    'amount' => $receipt['amount'] - $receipt['amount_paid'],
                    'staff_id' => Auth::id(),
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

            }

            $arOrder['process_status'] = $orderStatus;
            if ($orderStatus == 'paysuccess') {
                $arOrder['payment_date'] = Carbon::now();
            }

            $updateReceipt = $this->receiptTable->updateReceipt($receipt_id,$arrReceipt);
            $updateOrder = $this->orderService->edit($order_id,$arOrder);

//            chi tiết đơn hàng
            $detailOrder = $this->orderService->getDetail($order_id);

            $this->sendNotificationAction('order_service_status_S',$detailOrder['customer_id'],$order_id);
//        Thêm receipt detail
            $idDetailRequest = $this->receiptDetail->createReceiptDetail($arr);
//            tạo send mail
            $vars = [
                'receipt_type' => $param['receipt_type'] == 'cash' ? 'Tiền mặt':
                    ($param['receipt_type'] == 'transfer' ? 'Chuyển khoản' :
                        ($param['receipt_type'] == 'interest' ? 'Tiền lãi' :
                            ($param['receipt_type'] == 'bonus' ? 'Tiền thưởng' : ''))),
                'amount' => (int)str_replace(',', '',$param['amount']),
                'staff' => Auth::user()['user_name'],
                'order_code' => $detailOrder['order_service_code'],
                'customer_name' => $detailOrder['customer_name']
            ];

            $this->insertEmailApi($idDetailRequest,'create-receipt','Tạo biên lai thanh toán',$receipt['customer_email'],$vars);

//            $this->jobsEmailLogApi->addJob($insertEmail);

            if ($param['receipt_type'] == 'transfer') {
                if (isset($param['img-transfer']) && count($param['img-transfer']) != 0) {
                    foreach ($param['img-transfer'] as $key => $item) {
                        $imgTransfer[$key] = [
                            'receipt_id' => $receipt_id,
                            'receipt_detail_id' => $idDetailRequest,
                            'staff_id' => Auth::id(),
                            'image_file' => url('/').'/' . $this->transferTempfileToAdminfile($item,str_replace('', '', $item)),
                            'created_by' => Auth::id(),
                            'updated_by' => Auth::id(),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                    }
                }

            }
            //        Thêm ảnh
            if (count($imgTransfer) != 0) {
                $this->receiptDetailImage->createImage($imgTransfer);
            }

//            Nếu hoàn thành tạo hợp đồng
            if ($orderStatus == 'paysuccess') {
//                lấy chi tiết đơn hàng
                $arrService = [];
                for($i = 0 ; $i < $detailOrder['quantity'] ; $i++){
                    if ($detailOrder['price_standard'] / 1000000 < 10) {
                        $randomString = $this->generateRandomString(12 - strlen('EVB0'.($detailOrder['price_standard'] / 1000000).'_'));
                        $randomString = 'EVB0'.($detailOrder['price_standard'] / 1000000).'_'.$randomString;
                    } else {
                        $randomString = $this->generateRandomString(12 - strlen('EVB'.($detailOrder['price_standard'] / 1000000).'_'));
                        $randomString = 'EVB'.($detailOrder['price_standard'] / 1000000).'_'.$randomString;
                    }

                    $arrService[] = [
                        'service_id' => $detailOrder['service_id'],
                        'service_voucher_template' => $detailOrder['service_voucher_template_id'],
                        'order_service_id' => $detailOrder['order_service_id'],
                        'customer_id' => $detailOrder['customer_id'],
                        'serial' => $randomString,
                        'service_serial_status' => 'new',
                        'is_actived' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => Auth::id(),
                        'updated_by' =>Auth::id(),
                    ];
                }

                if (count($arrService) != 0) {
                    $createCustomerContract = $this->serviceSerial->addServiceSerial($arrService);
                }

                if ($param['receipt_type'] == 'interest') {
                    $listContractWithraw = $this->withrawRequest->getListIdRequestByGroup($param['withdraw_request_group_id_interest']);
                    if (count($listContractWithraw) != 0){
                        $listContractWithraw = collect($listContractWithraw)->pluck('customer_contract_id')->toArray();
                    }

                    if (count($listContractWithraw) != 0){
                        $rCustomerContractInterest = app()->get(CustomerContractInterestRepositoryInterface::class);
                        $rCustomerContractInterest->calculateContract('time',$listContractWithraw);
                    }
                }
            }
            return true;
        } catch (\Exception $e){
            dd($e->getMessage());
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function sendNotificationAction($key,$customer,$object_id){
        $noti = [
            'key' => $key,
            'customer_id' => $customer,
            'object_id' => $object_id
        ];
        $this->jobsEmailLogApi->sendNotification($noti);
        return true;
    }

    public function insertEmailApi ($idDetailRequest,$type,$subject,$email_to,$vars) {
        $insertEmail = [
            'obj_id' => $idDetailRequest,
            'email_type' => $type,
            'email_subject' => $subject,
            'email_from' => env('MAIL_USERNAME'),
            'email_to' => $email_to,
            'email_params' => json_encode($vars),
            'is_run' => 0
        ];

        $this->jobsEmailLogApi->addJob($insertEmail);
    }

    public function createReceipt($param)
    {
        try {
            DB::beginTransaction();
//            Chi tiết order
            $data = [
                'customer_id' => $param['customer_id'],
                'amount' => $param['total'],
                'staff_id'=> Auth::id(),
                'object_id' => $param['order_service_id'],
                'object_type' => 'order-service',
                'status' =>'unpaid',
                'created_at' => Carbon::now(),
                'created_by' => Auth::id(),
            ];
            $this->addReceipt($data);
            // change status order
            $dataChange =[
                'order_service_id' =>$param['order_service_id'],
                'process_status' => 'confirmed',
            ];
            $this->changeStatus($dataChange);
            DB::commit();
            return response()->json([
                'error' => 0,
                'message' => __('Thêm thành công')
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 0,
                'message' => __('Thêm  thất bại'),
                '_message' => $e->getMessage()
            ]);
        }
    }

    public function showPopupReceiptDetail($param)
    {
        $moneyReceipt = $this->getMoneyReceipt($param['order_service_id']);

//        Tổng tiền đã thanh toán
        $sumMoneyReceipt = $this->receiptDetail->sumMoneyReceipt($moneyReceipt['receipt_id']);
        if ($sumMoneyReceipt == null) {
            $sumMoneyReceipt = 0;
        } else {
            $sumMoneyReceipt = $sumMoneyReceipt['sum_receipt'];
        }
//        Danh sách tiền lãi và hoa hồng
        $arrWithrawRequestGroup = [];
        $arrWithrawRequest = [];
        $type = ['bond','saving'];
        $status = ['new'];
//        $arrWithrawRequestGroup = $this->withrawRequestGroup->getListByOrderId($param['order_id'],$type,$status);
//        if (count($arrWithrawRequestGroup) != 0) {
//            $keyGroupId = collect($arrWithrawRequestGroup)->pluck('withdraw_request_group_id');
////        danh sách chi tiết tiền lãi
//            $listWithrawRequest = $this->withrawRequest->getListByGroupId($keyGroupId);
//            if(count($listWithrawRequest) != 0) {
//                $arrWithrawRequest = collect($listWithrawRequest)->groupBy('withdraw_request_group_id');
//            }
//        }
        $view = view(
            'admin::order-service.popup.create-receipt',
            [
                'moneyReceipt' => $moneyReceipt,
                'order_service_id' => $param['order_service_id'],
//                'listWithrawRequestGroup' => $arrWithrawRequestGroup,
//                'listWithrawRequest' => $arrWithrawRequest,
                'sumMoneyReceipt' => $sumMoneyReceipt
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function getTotalInterest($order_id)
    {
        $listId = $this->withrawRequest->getListIdContract($order_id,['cash','bond','saving']);

        if (count($listId) == 0) {
            return 0;
        }
//        tổng tiền lãi các hợp đồng
        $totalMoney = 0;
//        Tổng tiền rút của hợp đồng
        $totalRequest = 0;
        $arrIdContract = collect($listId)->pluck('customer_contract_id');

//        Lấy tổng số tiền lãi theo id contract
        $totalTmp = $this->contractInterestMonth->totalBonusCustomerArr($arrIdContract);
//        if ($totalTmp == null || $totalTmp['interest_banlance_amount'] == null) {
//            $totalMoney += 0;
//        } else {
//            $totalMoney += $totalTmp['interest_banlance_amount'];
//        }

        foreach ($arrIdContract as $item) {
            $lastInterest = $this->contractInterestTime->getInterestTotalInterest($item);
            if ($lastInterest != null) {
                $totalMoney += intval($lastInterest->interest_banlance_amount);
            }
        }

//        Lấy tổng giá trị sử dụng từ ví lãi
        $totalWithdraw = $this->withrawRequestGroup->getWithdrawByGroupCustomerId($arrIdContract,['interest']);

//        return $totalMoney - $totalWithdraw;
        return $totalMoney;
    }

    public function getTotalCommission($order_id,$customer_id)
    {
//        Tính tổng tiền thưởng
        $totalBonus = 0;
        $type = ['bonus','register','refer'];
//        $totalBonusTmp = $this->customerContract->totalBonusCustomer($customer_id);
        $totalBonusTmp = $this->wallet->totalBonusCustomer($customer_id,$type);
        if ($totalBonusTmp != null || $totalBonusTmp['total_commission'] != null) {
            $totalBonus = $totalBonusTmp['total_commission'];
        }
//        Tổng tiền khách hàng đã rút bằng ví thưởng
        $totalWithdraw = 0;
        $totalWithdrawTmp = $this->withrawRequestGroup->getWithdrawByType($customer_id,'bonus');
        return $totalBonus - $totalWithdrawTmp;
    }

    public function getTotalCommissionWallet($order_id, $customer_id)
    {
//        Tính tổng tiền thưởng
        $totalBonus = 0;
        $type = ['bonus','register','refer'];
        $totalBonusTmp = $this->wallet->totalBonusCustomer($customer_id,$type);
        if ($totalBonusTmp != null || $totalBonusTmp['total_commission'] != null) {
            $totalBonus = $totalBonusTmp['total_commission'];
        }
//        Tổng tiền khách hàng đã rút bằng ví thưởng
        $totalWithdraw = 0;
        $totalWithdrawTmp = $this->withrawRequestGroup->getWithdrawByType($customer_id,'bonus');
        return $totalBonus - $totalWithdrawTmp;
    }


    /**
     * Upload image dropzone
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function uploadDropzone($input)
    {

//        $time = Carbon::now();
//        // Requesting the file from the form
//        // dd($input['file']);
//        $image = $input['file'];
//
//        // Getting the extension of the file
//        $extension = $image->getClientOriginalExtension();
//        //tên của hình ảnh
//        $filename = $image->getClientOriginalName();
//        //$filename = time() . str_random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . time() . "." . $extension;
//        // This is our upload main function, storing the image in the storage that named 'public'
//        $upload_success = $image->storeAs(TEMP_PATH, $filename, 'public');
//        // If the upload is successful, return the name of directory/filename of the upload.
//        if ($upload_success) {
//            return response()->json($filename, 200);
//        } // Else, return error 400
//        else {
//            return response()->json('error', 400);
//        }
    }

    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

}