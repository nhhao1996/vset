<?php


namespace Modules\Admin\Repositories\SendEmail;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\ConfigTable;
use Modules\Admin\Models\SendEmailTable;

class SendEmailRepo implements SendEmailRepoInterface
{
    protected $sendEmailTable;
    protected $config;

    public function __construct(SendEmailTable $sendEmailTable,ConfigTable $config)
    {
        $this->sendEmailTable = $sendEmailTable;
        $this->config = $config;
    }

    public function getEmailMain()
    {
        $mainEmail = $this->sendEmailTable->getEmailMain();
        return $mainEmail;
    }

    public function getEmailCC()
    {
        $emailCC = $this->sendEmailTable->getEmailCC();
        return $emailCC;
    }

    public function updateEmail($data)
    {
        try{
            DB::beginTransaction();
            if (isset($data['email_cc']) && count($data['email_cc']) != 0) {
                $checkEmail = collect($data['email_cc'])->keyBy('email');
                if (count($data['email_cc']) != count($checkEmail)) {
                    return [
                        'error' => true,
                        'message' => __('admin::send-email.validate.email_cc_using'),
                    ];
                }
            }
            $arrEmail = [];
            $this->sendEmailTable->deleteEmails();
            if (isset($data['email_cc']) && count($data['email_cc']) != 0) {
                $checkEmail = collect($data['email_cc'])->keyBy('email');
                foreach ($data['email_cc'] as $item) {
                    $tmpEmail = [
                        'email' => (string)strip_tags($item['email']),
                        'is_actived' => (string)strip_tags($item['is_actived']),
                        'type' => 'cc',
                        'created_at' => Carbon::now(),
                        'created_by' => Auth::id()
                    ];
                    array_push($arrEmail,$tmpEmail);
                }
                $this->sendEmailTable->createEmail($arrEmail);
            }

            if (isset($data['email_staff'])){
                $email =[
                    'value_vi' => $data['email_staff'],
                    'value_en' => $data['email_staff'],
                ];
                $this->config->updateByKey('email_staff',$email);
            }

            if (isset($data['phone_staff'])){
                $email =[
                    'value_vi' => $data['phone_staff'],
                    'value_en' => $data['phone_staff'],
                ];
                $this->config->updateByKey('phone_staff',$email);
            }

            DB::commit();
            return [
                'error' => false,
                'message' => __('Cập nhật thành công'),
            ];
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
            return [
                'error' => true,
                'message' => __('Cập nhật thất bại'),
            ];
        }
    }

    public function getConfig($key)
    {
        return $this->config->getInfoByKey($key);
    }

}