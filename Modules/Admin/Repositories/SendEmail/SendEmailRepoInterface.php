<?php


namespace Modules\Admin\Repositories\SendEmail;


interface SendEmailRepoInterface
{
    /**
     * lấy email chính
     * @return mixed
     */
    public function getEmailMain();

    /**
     * lấy email cc
     */
    public function getEmailCC();

    /**
     * Update email
     * @param $data
     * @return mixed
     */
    public function updateEmail($data);

    public function getConfig($key);

}