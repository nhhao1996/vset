<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/9/2021
 * Time: 3:55 PM
 * @author nhandt
 */


namespace Modules\Admin\Repositories\Stock;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\ReceiptDetailImageTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReceiptTable;
use Modules\Admin\Models\StockBonusReferTable;
use Modules\Admin\Models\StockBonusTable;
use Modules\Admin\Models\StockConfigTable;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockMarketTable;
use Modules\Admin\Models\StockOrderTable;
use Modules\Admin\Models\StockTable;
use Modules\Admin\Models\StockTransactionTable;
use Modules\Admin\Models\StockWalletTable;
use Modules\Admin\Repositories\Message\MessageRepoInterface;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;
use Modules\Stock\Models\StockBonusAmountTable;
use Modules\Stock\Models\StockBonusConfigTable;
use Modules\Stock\Models\StockBonusPaymentMethodTable;
use Symfony\Component\Mime\Message;

class StockRepo implements StockRepoInterface
{
    private $stockOrders;
    private $stockMarket;
    public $stockRepository;
    public $messageRepo;
    public $mCustomer;
    public function __construct(StockOrderTable $stockOrders,
            StockRepositoryInterface  $stockRepository,
            StockMarketTable $stockMarket,
            MessageRepoInterface $messageRepo,
            CustomerTable $mCustomer
    )
    {
        $this->stockMarket = $stockMarket;
        $this->stockOrders = $stockOrders;
        $this->stockRepository = $stockRepository;
        $this->messageRepo = $messageRepo;
        $this->mCustomer = $mCustomer;
    }

    /**
     * Danh sách yêu cầu mua cổ phiếu
     *
     * @param array $filters
     * @return mixed
     */
    public function getList(array $filters = [])
    {
        return $this->stockOrders->getList($filters);
    }

    /**
     * Chi tiết cổ phiếu
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $data = $this->stockOrders->show($id);
        if($data['source'] == 'market'){
            return $data;
        }
        $totalStockBonus = 0;
        // cp thưởng theo tháng
        $month = $data['created_at']->month;
        $mStockBonus = new StockBonusTable();
        $rate = $mStockBonus->getRate($month)['percent'];
        $stockBonusMonthly = round(floatval($rate)*intval($data['quantity'])/100);
        if($stockBonusMonthly > 0){
            $totalStockBonus += $stockBonusMonthly;
        }
        // cp thưởng theo số lượng cp đã mua
        $stockBonusAmount = $this->getStockBonusOfStockAmountBonus(intval($data['quantity']));
        if($stockBonusAmount > 0){
            $totalStockBonus += $stockBonusAmount;
        }
        // cp thưởng theo pttt
        $stockBonusPaymentMethod = $this->getStockBonusOfPaymentMethodBonus(intval($data['quantity']), $data['payment_method_id']);
        if($stockBonusPaymentMethod > 0){
            $totalStockBonus += $stockBonusPaymentMethod;
        }
        $data['total_stock_bonus'] = $totalStockBonus;
        return $data;
    }
    public function getDetailSellerMarket($stockMarketId)
    {
        return $this->stockMarket->getDetailSellerMarket($stockMarketId);
    }

    /**
     * Popup tạo phiếu thu
     *
     * @param $param
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function showPopupReceiptDetail($param)
    {
        $data = [
            'customer_id' => $param['customer_id'],
            'amount' => $param['total'],
            'staff_id'=> Auth::id(),
            'object_id' => $param['order_id'],
            'object_type' => 'stock',
            'status' =>'unpaid',
            'created_at' => Carbon::now(),
            'created_by' => Auth::id(),
        ];
        $this->addReceipt($data);
        $moneyReceipt = $this->getMoneyReceipt($param['stock_order_id']);
        $view = view(
            'admin::buy-stock-request.popup.create-receipt',
            [
                'moneyReceipt' => $moneyReceipt,
                'stock_order_id' => $param['stock_order_id'],
                'payment_method_id' => $param['payment_method_id'],
                'payment_method_name' => $param['payment_method_name'],
                'stock_bonus' => $param['stock_bonus']
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    /**
     * Thêm chi tiết phiếu thu
     *
     * @param $param
     * @return array
     */
    public function addReceiptDetail($param)
    {
        try {
            DB::beginTransaction();
            $detail = $this->stockOrders->show($param['stock_order_id']);
            if ($detail['process_status'] != 'new') {
                if($param['payment_method_id'] == 5 || $param['payment_method_id'] == 6) {
                    return [
                        'error' => true,
                        'message' => 'Xác nhận thất bại' ,
                    ];
                }
                else {
                    return [
                        'error' => true,
                        'message' => 'Tạo phiếu thu thất bại' ,
                    ];
                }

            }
            $this->createReceiptDetail($param);
            DB::commit();
            if($param['payment_method_id'] == 5 || $param['payment_method_id'] == 6) {
                return [
                    'error' => false,
                    'message' => 'Xác nhận thành công' ,
                ];
            }
            else {
                return [
                    'error' => false,
                    'message' => 'Tạo phiếu thu thành công' ,
                ];
            }
        } catch (\Exception $e) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Tạo phiếu thu thất bại'
            ];
        }
    }

    /**
     * Tạo chi tiết phiếu thu
     *
     * @param $param
     */
    public function createReceiptDetail($param){

        //param =
        /*
            ^ array:6 [
              "receipt_id" => "410"
              "stock_order_id" => "2"
              "amount" => "3000000.000"
              "payment_method_id" => "5"
              "img-transfer" => array:3 [
                0 => "16182780943811342021_N0FUELSMPN.jpg"
                1 => "16182780943831342021_R4VUH4X9JJ.jpg"
                2 => "16182780943831342021_DS2OL2RYAU.png"
              ]
              "receipt_type" => "5"
            ]
        */
        try {

            $receiptTable = new ReceiptTable();
            $receiptDetailTable = new ReceiptDetailTable();
            $receiptImageTable = new ReceiptDetailImageTable();
            //Tạo receipt detail dựa vào param (receipt)
            $receipt_id = $param['receipt_id'];
            $receipt = $receiptTable->getDetail($receipt_id);
            $arrReceiptDetail = [
                'receipt_type' => 'stock',
                'receipt_id' => $receipt_id,
                'amount' => $receipt['amount'], // chỉ thanh toán 1 lần
                'staff_id' => Auth::id(),
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $receiptDetailId = $receiptDetailTable->createReceiptDetail($arrReceiptDetail);
            //Tạp receipt image
            $imgTransfer = [];
            if (isset($param['img-transfer']) && count($param['img-transfer']) != 0) {
                foreach ($param['img-transfer'] as $key => $item) {
                    $imgTransfer[$key] = [
                        'receipt_id' => $receipt_id,
                        'receipt_detail_id' => $receiptDetailId,
                        'staff_id' => Auth::id(),
                        'image_file' => url('/').'/' . $this->transferTempfileToAdminfile($item,str_replace('', '', $item)),
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                }
            }
            //        Thêm ảnh
            if (count($imgTransfer) != 0) {
                $receiptImageTable->createImage($imgTransfer);
            }

            $stockWalletTable = new StockWalletTable();

            // tính lại giá trị wallet_dividend_money trong stock_customer
            $stockCustomerTable = new StockCustomerTable();
            // kiểm tra người mua đã có trong stock_customer hay chưa? nếu chưa thì insert 0 0 0 vào
            if(!$stockCustomerTable->checkExistsCustomer($receipt['customer_id'])){
                // insert
                $arrStockCustomer = [
                    'customer_id' => $receipt['customer_id'],
                    'wallet_stock_money' => 0,
                    'wallet_dividend_money' => 0,
                    'stock' => 0,
                    'stock_bonus' => 0,
                    'created_at' => Carbon::now(),
                    'created_by' => \auth()->id(),
                ];
                $stockCustomerTable->createStockCustomer($arrStockCustomer);
            }


            $currentStock = $stockCustomerTable->getStock($receipt['customer_id']); // stock hiện tại của người mua
            $quantityBuyStock = $this->stockOrders->getQuantity($param['stock_order_id']); // số stock muốn mua

            // Tổng tiền cuối cùng được ghi nhận của 1 người mua
            $stockWallet = $stockWalletTable->getTotalAfterOfCustomer($receipt['customer_id']); // money hiện tại của người mua
            $dividendWallet = $stockWalletTable->getTotalAfterOfCustomerDividend($receipt['customer_id']); // money hiện tại của người mua
            $orderSource = $this->stockOrders->getSource($param['stock_order_id'])['source']; // nguồn mua (chợ hoặc nhà phát hành)
            $stockMarketTable = new StockMarketTable();


            $transactionTable = new StockTransactionTable();
            // nếu nguồn bán là nhà phát hành thì không thêm giao dịch cổ phiếu của người bán, ngược lại vs chợ
            if($orderSource == 'market'){

                $sellerId = $stockMarketTable->getSellerIdByStockOrderId($param['stock_order_id']); // id của người bán
                $currentStockSeller = $stockCustomerTable->getStock($sellerId['customer_id']); // stock hiện tại của người bán
                $moneyBefore = $this->stockOrders->getMoney($param['stock_order_id']); // giá bán sẽ lưu trong transaction
                // kiểm tra người bán đã có trong stock_customer hay chưa? nếu chưa thì insert 0 0 0 vào
                if(!$stockCustomerTable->checkExistsCustomer($sellerId['customer_id'])){
                    // insert
                    $arrStockCustomer = [
                        'customer_id' => $sellerId['customer_id'],
                        'wallet_stock_money' => 0,
                        'wallet_dividend_money' => 0,
                        'stock' => 0,
                        'stock_bonus' => 0,
                        'created_at' => Carbon::now(),
                        'created_by' => \auth()->id(),
                    ];
                    $stockCustomerTable->createStockCustomer($arrStockCustomer);
                }
                $arrStockTransactionSeller = [ // ghi lại lịch sử giao dịch cổ phiếu của người bán (ghi số lượng thay đổi)
                    'source' => $orderSource,
                    'type' => 'minus',
                    'stock_order_id' => (int)$param['stock_order_id'],
                    'customer_id' => $sellerId['customer_id'],
                    'obj_id' => $receipt['customer_id'],
                    'quantity_before' => (int)$currentStockSeller['stock'],
                    'quantity' => (int)$quantityBuyStock['quantity'],
                    'quantity_after' => (int)$currentStockSeller['stock'] - (int)$quantityBuyStock['quantity'],
                    'money_before' => $moneyBefore['money_standard'],
                    'money_standard' => '10000',
                    'payment_method_id' => $param['payment_method_id'],
                    'date' => Carbon::now()->format('d'),
                    'month' => Carbon::now()->format('m'),
                    'year' => Carbon::now()->format('Y'),
                    'created_at' => Carbon::now(),
                    'created_by' => \auth()->id(),
                ];
                $transactionTable->createTransaction($arrStockTransactionSeller);
                $arrStockTransactionBuyer = [ // ghi lại lịch sử giao dịch cổ phiếu của người mua (ghi số lượng thay đổi)
                    'source' => $orderSource,
                    'type' => 'add',
                    'stock_order_id' =>  (int)$param['stock_order_id'],
                    'customer_id' => $receipt['customer_id'],
                    'obj_id' => $sellerId['customer_id'],
                    'quantity_before' => (int)$currentStock['stock'],
                    'quantity' => (int)$quantityBuyStock['quantity'],
                    'quantity_after' => (int)$currentStock['stock'] + (int)$quantityBuyStock['quantity'],
                    'money_before' => $moneyBefore['money_standard'],
                    'money_standard' => '10000',
                    'payment_method_id' => $param['payment_method_id'],
                    'date' => Carbon::now()->format('d'),
                    'month' => Carbon::now()->format('m'),
                    'year' => Carbon::now()->format('Y'),
                    'created_at' => Carbon::now(),
                    'created_by' => \auth()->id(),
                ];
                $transactionTable->createTransaction($arrStockTransactionBuyer);
            }
            else{ // nếu nguồn từ nhà phát hành thì obj_id không có
                $moneyBefore = $this->stockOrders->getMoney($param['stock_order_id']); // giá bán sẽ lưu trong transaction
                $arrStockTransactionBuyer = [ // ghi lại lịch sử giao dịch cổ phiếu của người mua (ghi số lượng thay đổi)
                    'source' => $orderSource,
                    'type' => 'add',
                    'stock_order_id' => (int)$param['stock_order_id'],
                    'customer_id' => $receipt['customer_id'],
                    'obj_id' => '',
                    'quantity_before' => (int)$currentStock['stock'],
                    'quantity' => (int)$quantityBuyStock['quantity'],
                    'quantity_after' => (int)$currentStock['stock'] + (int)$quantityBuyStock['quantity'],
                    'money_before' => $moneyBefore['money_standard'],
                    'money_standard' => '10000',
                    'payment_method_id' => $param['payment_method_id'],
                    'date' => Carbon::now()->format('d'),
                    'month' => Carbon::now()->format('m'),
                    'year' => Carbon::now()->format('Y'),
                    'created_at' => Carbon::now(),
                    'created_by' => \auth()->id(),
                ];
                $transactionTable->createTransaction($arrStockTransactionBuyer);
            }



            //nếu HTTT là ví cổ tức thì tiến hành trừ vào ví cổ tức của customer
            if($param['payment_method_id'] == 6 || $param['payment_method_id'] == 1 || $param['payment_method_id'] == 4){
                if($param['payment_method_id'] == 6){
                    // thêm stock_wallet để ghi log lại lý do trừ tiền của customer
                    // get total_before tức là total_after của lần ghi log trước đó của customer
                    $arrDividendWalletBuyer = [
                        'customer_id' => $receipt['customer_id'],
                        'type' => 'minus',
                        'source' => 'dividend',
                        'total_before' => $dividendWallet['total_after'],
                        'total_money' => $receipt['amount'],
                        'total_after' => $dividendWallet['total_after'] - (float)$receipt['amount'],
                        'created_at' => Carbon::now(),
                        'created_by' => \auth()->id()
                    ];
                    $stockWalletId = $stockWalletTable->createStockOrDividendWallet($arrDividendWalletBuyer);
                    $arrDividendCustomer = [
                        'wallet_dividend_money' => $dividendWallet['total_after'] - (float)$receipt['amount'],
                        'stock' => (int)$currentStock['stock'] + (int)$quantityBuyStock['quantity'],
                        'last_updated' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'created_by' => Auth()->id()
                    ];
                    $stockCustomerTable->updateStockOrDividendWallet($arrDividendCustomer,$receipt['customer_id']);
                }
                else{
                    $arrDividendCustomer = [
                        'stock' => (int)$currentStock['stock'] + (int)$quantityBuyStock['quantity'],
                        'last_updated' => Carbon::now(),
                        'created_at' => Auth()->id()
                    ];
                    $stockCustomerTable->updateStockOrDividendWallet($arrDividendCustomer,$receipt['customer_id']);
                }

            }
                //nếu HTTT là ví cổ phiếu thì tiến hành trừ vào ví cổ phiếu của customer
            if($param['payment_method_id'] == 5 || $param['payment_method_id'] == 1 || $param['payment_method_id'] == 4){
                if($param['payment_method_id'] == 5){

                    $type="minus"; // loại trừ tiền
                    $source="stock"; // nguồn ví cổ phiếu
                    $arrStockWalletBuyer = [ // thêm stock_wallet để ghi log lại  tiền của người mua
                        'customer_id' => $receipt['customer_id'],
                        'type' => $type,
                        'source' => $source,
                        'day' => Carbon::now()->format('d'),
                        'month' => Carbon::now()->format('m'),
                        'year' => Carbon::now()->format('Y'),
                        'total_before' => $stockWallet['total_after'],
                        'total_money' => $receipt['amount'],
                        'total_after' => $stockWallet['total_after'] - (float)$receipt['amount'],
                        'created_at' => Carbon::now(),
                        'created_by' => \auth()->id()
                    ];
                    $stockWalletId = $stockWalletTable->createStockOrDividendWallet($arrStockWalletBuyer);
                    $arrStockCustomer = [ // cập nhật lại stock và money của người mua trong stock_customer
                        'wallet_stock_money' => $stockWallet['total_after'] - (float)$receipt['amount'],
                        'stock' => (int)$currentStock['stock'] + (int)$quantityBuyStock['quantity'],
                        'last_updated' => Carbon::now(),
                        'created_at' => Auth()->id()
                    ];
                    $stockCustomerTable->updateStockOrDividendWallet($arrStockCustomer,$receipt['customer_id']);
                }
                else{ // nếu hình thức thanh toán khác ví cổ phiếu thì chỉ cập nhật lại stock trong stock_customer
                    $arrStockCustomer = [
                        'stock' => (int)$currentStock['stock'] + (int)$quantityBuyStock['quantity'],
                        'last_updated' => Carbon::now(),
                        'created_at' => Auth()->id()
                    ];
                    $stockCustomerTable->updateStockOrDividendWallet($arrStockCustomer,$receipt['customer_id']);
                }

            }

            //nếu yêu cầu mua từ chợ cộng tiền vào người bán
            if($orderSource == 'market'){
                $stockConfigTable = new StockConfigTable();
                $fee = $stockConfigTable->getFeeStockSellMarket();
                if($fee == null){
                    $fee = 0;
                }
                else{
                    $fee = $fee['fee_stock_sell_market'];
                }
                // Tổng tiền cuối cùng được ghi nhận của 1 seller
                $stockWalletSeller = $stockWalletTable->getTotalAfterOfCustomer($sellerId['customer_id']); // money hiện tại của người bán

                // nếu hình thức chuyển khoản là tiền mặt hoặc chuyển khoản thì chỉ ghi nhận lại số stock của người bán
//                if($param['payment_method_id'] == 1 || $param['payment_method_id'] == 4){
//                    $arrStockCustomer = [
//                        'stock' => (int)$currentStockSeller['stock'] - (int)$quantityBuyStock['quantity'],
//                        'last_updated' => Carbon::now(),
//                        'created_at' => Auth()->id()
//                    ];
//                }
//                else{
                $type="add"; // loại thêm tiền
                $source="stock"; // nguồn ví cổ phiếu
                $arrStockWalletSeller = [
                    'customer_id' => $sellerId['customer_id'],
                    'type' => $type,
                    'source' => $source,
                    'day' => Carbon::now()->format('d'),
                    'month' => Carbon::now()->format('m'),
                    'year' => Carbon::now()->format('Y'),
                    'total_before' => $stockWalletSeller['total_after'],
                    'total_money' => (float)$receipt['amount']*(1-(float)$fee/100),
                    'total_after' => $stockWalletSeller['total_after'] + (float)$receipt['amount']*(1-(float)$fee/100),
                    'created_at' => Carbon::now()->format("Y-m-d"),
                    'created_by' => \auth()->id()
                ];
                $stockWalletId = $stockWalletTable->createStockOrDividendWallet($arrStockWalletSeller);
                $arrStockCustomer = [
                    'wallet_stock_money' => $stockWalletSeller['total_after'] + (float)$receipt['amount']*(1-(float)$fee/100),
                    'stock' => (int)$currentStockSeller['stock'] - (int)$quantityBuyStock['quantity'],
                    'last_updated' => Carbon::now(),
                    'created_at' => Auth()->id()
                ];
//                }
                // cập nhật lại stock hoặc (stock và money) của người bán
                $stockCustomerTable->updateStockOrDividendWallet($arrStockCustomer,$sellerId['customer_id']);
            }
            //nếu thanh toán thành công thì thay đổi status và cập nhật payment date cho stock order
            $stock_order_id = $param['stock_order_id'];
            $dataUpdateStockOrder['process_status'] = 'paysuccess';
            $dataUpdateStockOrder['payment_date'] = Carbon::now();
            $updateStockOrder = $this->stockOrders->updateStockOrder($stock_order_id,$dataUpdateStockOrder);
            $dataUpdate = ['status' => 'paid'];
            $receiptTable->updateReceipt($receipt_id,$dataUpdate);

            // lấy lại chi tiết của stock_order để biết obj_id (id của nguồn mua để cập nhật status hoặc quantity sell)
            $detailOrder = $this->stockOrders->show($param['stock_order_id']);
            // cập nhật status trong market
            if($orderSource == 'market'){
                $stockMarketTable = new StockMarketTable();
                $totalQuantity = $this->stockOrders->totalQuantitySold($detailOrder['obj_id']);
                $quantity = $stockMarketTable->getQuantity($detailOrder['obj_id']);
                if((int)$totalQuantity['quantity_sold'] == (int)$quantity['quantity']){
                    $statusMarket = [
                        'status' => 'success'
                    ];
                    $stockMarketTable->edit($statusMarket,$detailOrder['obj_id']);
                }
                $keyNoti = 'stock_admin_market_order';
            }
            // cập nhật quantity_sell trong stock (của publisher)
            else{
                $stockTable = new StockTable();
                $quantity_sale = $stockTable->getQuantitySale();
                $quantitySellPublish = [
                    'quantity_sale' => (int)$quantity_sale['quantity_sale'] + (int)$quantityBuyStock['quantity']
                ];
                $stockTable->edit($quantitySellPublish,$detailOrder['obj_id']);
                $keyNoti = 'stock_admin_publish_order';
            }
            if($orderSource == 'publisher'){
                // todo: xử lý cổ phiếu thưởng
                $totalStockBonus = 0;
                // cp thưởng theo tháng
                $month = $detailOrder['created_at']->month;
                $mStockBonus = new StockBonusTable();
                $rate = $mStockBonus->getRate($month)['percent'];
                $stockBonusMonthly = round(floatval($rate)*intval($quantityBuyStock['quantity'])/100);
                if($stockBonusMonthly > 0){
                    $totalStockBonus += $stockBonusMonthly;
                    $curStockBonus = $stockCustomerTable->getStockBonus($receipt['customer_id'])['stock_bonus'];
                    $this->addStockBonusTransaction('publisher', $curStockBonus, $stockBonusMonthly, $receipt['customer_id']);
                    $this->createStockBonusCustomer($receipt['customer_id'], $curStockBonus, $stockBonusMonthly);
                }
                // cp thưởng theo số lượng cp đã mua
                $stockBonusAmount = $this->getStockBonusOfStockAmountBonus(intval($quantityBuyStock['quantity']));
                if($stockBonusAmount > 0){
                    $totalStockBonus += $stockBonusAmount;
                    $curStockBonus = $stockCustomerTable->getStockBonus($receipt['customer_id'])['stock_bonus'];
                    $this->addStockBonusTransaction('amount_stock', $curStockBonus, $stockBonusAmount, $receipt['customer_id']);
                    $this->createStockBonusCustomer($receipt['customer_id'], $curStockBonus, $stockBonusAmount);
                }
                // cp thưởng theo pttt
                $stockBonusPaymentMethod = $this->getStockBonusOfPaymentMethodBonus(intval($quantityBuyStock['quantity']), $param['payment_method_id']);
                if($stockBonusPaymentMethod > 0){
                    $totalStockBonus += $stockBonusPaymentMethod;
                    $curStockBonus = $stockCustomerTable->getStockBonus($receipt['customer_id'])['stock_bonus'];
                    $this->addStockBonusTransaction('payment_method', $curStockBonus, $stockBonusPaymentMethod, $receipt['customer_id']);
                    $this->createStockBonusCustomer($receipt['customer_id'], $curStockBonus, $stockBonusPaymentMethod);
                }
                // xử lý cp thưởng cho người giới thiệu
                $mCustomer = new CustomerTable();
                $referId = $mCustomer->getReferIdOfCustomer($receipt['customer_id']);
                $referId = isset($referId['customer_refer_id']) != '' ? $referId['customer_refer_id'] : '';
                if($referId != ''){
                    $stockBonusRefer = $this->getStockBonusOfStockReferBonus(intval($quantityBuyStock['quantity']));
                    if($stockBonusRefer > 0){
                        $totalStockBonus += $stockBonusRefer;
                        if(!$stockCustomerTable->checkExistsCustomer($referId)){ // nếu người giới thiệu chưa có thông tin cổ phiếu
                            $arrStockCustomer = [
                                'customer_id' => $referId,
                                'wallet_stock_money' => 0,
                                'wallet_dividend_money' => 0,
                                'stock' => 0,
                                'stock_bonus' => 0,
                                'created_at' => Carbon::now(),
                                'created_by' => \auth()->id(),
                            ];
                            $stockCustomerTable->createStockCustomer($arrStockCustomer);
                        }
                        $curStockBonus = $stockCustomerTable->getStockBonus($referId)['stock_bonus'];
                        $this->addStockBonusTransaction('refer', $curStockBonus, $stockBonusRefer, $referId);
                        $this->createStockBonusCustomer($referId, $curStockBonus, $stockBonusRefer);
                    }
                }
//            dd($stockBonusMonthly, $stockBonusAmount, $stockBonusPaymentMethod, $stockBonusRefer);
            }

            // TODO: push noti
            $mNoti = new JobsEmailLogApi();
            $order = $this->stockOrders->getDetailTransfer($param['stock_order_id']);

            $data =[
                'key' => $keyNoti,
                'customer_id' =>$order['customer_id'],
                'object_id' => $param['stock_order_id'],
                'stock_order_id'=>$param['stock_order_id']
            ];
            $mNoti->sendNotification($data);
            $mCustomer = new CustomerTable();
            $customer = $mCustomer->getDetail($order['customer_id']);

            if($keyNoti=="stock_admin_publish_order"){
                $this->messageRepo->sendSMS(
                    'stock_admin_publish_order',
                    [
                        'phone'=>$customer->phone
                    ],[
                        'stock_order_id'=>$stock_order_id
                    ]
                );
            }
//                end: push noti--------------------------
//          todo: Tạo hợp đồng và phụ lục hợp dđòng--------------
            $stockOrder = $this->stockOrders->getDetailTransfer($param['stock_order_id']);

            if($stockOrder->source == 'transfer'){
                $this->stockRepository->createOrUpdateStockContract($stockOrder->customer_id, $stockOrder->quantity);
//                    todo: Người chuyển nhượng hoặc bán thì update trừ đi cổ phiếu
                $this->stockRepository->createOrUpdateStockContract($stockOrder->obj_id,-($stockOrder->quantity));
            }
            else{
                $this->stockRepository->createOrUpdateStockContract($stockOrder->customer_id, $stockOrder->quantity);
            }
//        end: Tạo hợp đồng và phụ lục hợp


        } catch (\Exception $e){
            dd($e->getMessage());
        }
    }
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    /**
     * Lấy tiền của 1 đơn hàng
     *
     * @param $getReceiptStockOrder
     * @return mixed
     */
    public function getMoneyReceipt($getReceiptStockOrder)
    {
        $receiptTable = new ReceiptTable();
        return $receiptTable->getReceiptStockOrder($getReceiptStockOrder);
    }

    /**
     * Lấy danh sách chi tiết phiếu thu
     *
     * @param $param
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getListReceiptDetail($param)
    {
        $receiptDetail = new ReceiptDetailTable();
        $receiptDetailImage = new ReceiptDetailImageTable();
        $param['object_type'] = 'stock';
        $list = $receiptDetail->getListReceipt($param);
        $arrDetailId = [];
        $arrDetailId = collect($list->toArray()['data'])->pluck('receipt_detail_id');
        $listImage = $receiptDetailImage->getListImage($arrDetailId);
        $arrGroupImage = [];
        if (count($listImage) != 0) {
            $arrGroupImage = collect($listImage)->groupBy('receipt_detail_id');
        }
        $view = view(
            'admin::buy-stock-request.partial.receipt-detail-tr',
            [
                'list' => $list,
                'arrGroupImage' => $arrGroupImage
            ]
        )->render();


        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    /**
     * Xác nhận đơn hàng
     *
     * @param $stock_order_id
     * @return mixed
     */
    public function allowsConfirm($stock_order_id)
    {
        return $this->stockOrders->confirmBuyStockRequest($stock_order_id);

    }

    /**
     * Tạo phiếu thu
     *
     * @param $param
     * @return \Illuminate\Http\JsonResponse
     */
    public function createReceipt($param)
    {
        try {
            DB::beginTransaction();
//            $data = [
//                'customer_id' => $param['customer_id'],
//                'amount' => $param['total'],
//                'staff_id'=> Auth::id(),
//                'object_id' => $param['order_id'],
//                'object_type' => 'stock',
//                'status' =>'unpaid',
//                'created_at' => Carbon::now(),
//                'created_by' => Auth::id(),
//            ];
//            $this->addReceipt($data);
            DB::commit();
            return response()->json([
                'error' => 0,
                'message' => __('Thêm thành công')
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 0,
                'message' => __('Thêm  thất bại'),
                '_message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Thêm phiếu thu
     *
     * @param array $data
     * @return mixed
     */
    public function addReceipt(array $data)
    {
        $data['receipt_code'] = $this->generateUniqueString(6);
        $receiptTable = new ReceiptTable();
        return $receiptTable->add($data);
    }

    /**
     * Tạo chuối code ngẫu nhiên-
     *
     * @return string
     */
    private function generateUniqueString()
    {
        while(true) {
            $now = Carbon::today()->format('Y/m/d');
            $to = Carbon::tomorrow()->format('Y/m/d');
            $str_length = 4;
            $reservations = ReceiptTable::where('created_at', '>=', $now)
                ->where('created_at', '<=', $to)
                ->count();
            $nowYmd = Carbon::today()->format('Ymd');
            $str = substr("0000{$reservations}", - $str_length);
            $randomString =$nowYmd.'_'.$str;
            $doesCodeExist = DB::table('receipts')
                ->where('receipt_code', $randomString)
                ->count();
            if (! $doesCodeExist) {
                return $randomString;
            }
        }

    }

    /**
     * Số cổ phiếu phát hành còn lại
     *
     * @return int
     */
    public function getStockPublishRemaining(){
        $dataSold = $this->stockOrders->getStockPublishRemaining();
        $stockTable = new StockTable();
        $dataPublisher = $stockTable->getQuantity();
        if($dataSold == null){
            return (int)$dataPublisher['quantity'];
        }
        else return (int)$dataPublisher['quantity']-(int)$dataSold['quantity_sold'];
    }

    /**
     * Số cổ phiếu bán ở chợ còn lại
     *
     * @param $objId
     * @return int
     */
    public function getStockMarketRemaining($objId)
    {
        $data = $this->stockOrders->getStockMarketRemaining($objId);
        if($data == null){
            return 0;
        }
        else return (int)$data['quantity']-(int)$data['quantity_sold'];
    }

    /**
     * Huỷ xác nhận yêu cầu mua CP
     *
     * @param $param
     * @return array
     */
    public function cancelConfirm($param)
    {
        try{
            DB::beginTransaction();
            if($param['process_status'] == 'cancel'){
                $dataUpdate = [
                    'process_status' => 'cancel',
                    'reason_vi' => $param['reason_vi'],
                    'reason_en' => $param['reason_en']
                ];
                $this->stockOrders->updateStockOrder($param['stock_order_id'],$dataUpdate);
                DB::commit();
                // TODO: push noti---------------------------------
                $stockOrder = $this->stockOrders->getDetailTransfer($param['stock_order_id']);
                $mNoti = new JobsEmailLogApi();
//            $order = $this->stockOrder
//            $order = $this->stockOrder
//            $order = $this->stockOrders->getDetailTransfer($id);
                $noti =[
                    'key' =>'stock_admin_buy_request_cancel',
                    'customer_id' =>$stockOrder['customer_id'],
                    'object_id' => $param['stock_order_id'],
                    'stock_order_id'=> $param['stock_order_id']
                ];
                $customer = $this->mCustomer->getDetail($stockOrder['customer_id']);
                $mNoti->sendNotification($noti);
//                todo: chỉ send SMS cho Admin hủy yêu cầu mua từ NPH
                if($stockOrder->source == 'publisher'){
                    $this->messageRepo->sendSMS(
                        'stock_admin_publish_cancel',
                        [
                            'phone'=>$customer->phone
                        ],[
                            'stock_order_id'=>$param['stock_order_id']
                        ]


                    );
                }


//                end: push noti--------------------------
            }
                return [
                    'error' => false,
                    'message' => 'Huỷ xác nhận thành công'
                ];

            DB::commit();
            return [
                'error' => false,
                'message' => 'Huỷ xác nhận thất  bại'
            ];
        }
        catch (\Exception $ex){
            DB::rollBack();
            return [
                'error' => true,
                'message' => $ex->getMessage()
            ];
        }
    }

    /**
     * Ghi log nhận cp thưởng
     *
     * @param string $source
     * @param $currentStockBonus
     * @param $quantity
     * @param $customerId
     */
    protected function addStockBonusTransaction($source = '', $currentStockBonus, $quantity, $customerId){
        $stockTable = new StockTable();
        $stockTransaction = new StockTransactionTable();
        $currentMoneyPublisher = $stockTable->getMoney();
        $arrStockTransaction = [
            'stock_type' => 'bonus',
            'source' => $source,
            'type' => 'add',
            'stock_order_id' => '',
            'customer_id' => $customerId,
            'obj_id' => '',
            'quantity_before' => (int)$currentStockBonus,
            'quantity' => (int)$quantity,
            'quantity_after' => (int)$currentStockBonus + (int)$quantity,
            'money_before' => $currentMoneyPublisher['money'],
            'money_standard' => $currentMoneyPublisher['money_publish'],
            'payment_method_id' => '',
            'date' => Carbon::now()->format('d'),
            'month' => Carbon::now()->format('m'),
            'year' => Carbon::now()->format('Y'),
            'created_at' => Carbon::now(),
            'created_by' => \auth()->id(),
        ];
        $stockTransaction->createTransaction($arrStockTransaction);
    }

    /**
     * Lưu lại cp thưởng user
     *
     * @param $customerId
     * @param $currentStockBonus
     * @param $quantity
     */
    protected function createStockBonusCustomer($customerId, $currentStockBonus, $quantity)
    {
        $mStockCustomer = new StockCustomerTable();
        $arrStockCustomer =[
            'stock_bonus' => (int)$currentStockBonus + $quantity,
            'last_updated' => Carbon::now(),
            'created_at' => Carbon::now(),
            'created_by' => Auth()->id(),
            'updated_at' => Carbon::now(),
            'updated_by' => Auth()->id()
        ];
        $mStockCustomer->updateCustomer($customerId,$arrStockCustomer);
    }

    /**
     * Số cp thưởng dựa trên số cp đã mua
     *
     * @param $quantity
     * @return float|int
     */
    protected function getStockBonusOfStockAmountBonus($quantity)
    {
        $mStockBonusConfig = new \Modules\Admin\Models\StockBonusConfigTable();
        $status = $mStockBonusConfig->getStockBonusConfig();
        if($status['stock_amount_bonus'] == 1){
            $mStockBonusAmount = new \Modules\Admin\Models\StockBonusAmountTable();
            $data = $mStockBonusAmount->getRateWithQuantity($quantity);
            $rate = isset($data['rate']) != '' ? $data['rate'] : 0;
            return round((float)$rate*(int)$quantity/100);
        }
        else{
            return 0;
        }
    }

    /**
     * Số cp thưởng dựa vào pttt
     *
     * @param $quantity
     * @param $id
     * @return float|int
     */
    protected function getStockBonusOfPaymentMethodBonus($quantity, $id)
    {
        $mStockBonusConfig = new \Modules\Admin\Models\StockBonusConfigTable();
        $status = $mStockBonusConfig->getStockBonusConfig();
        if($status['stock_payment_method_bonus'] == 1){
            $mStockBonusAmount = new \Modules\Admin\Models\StockBonusPaymentMethodTable();
            $data = $mStockBonusAmount->getRateWithIdMethod($id);
            $rate = isset($data['rate']) != '' ? $data['rate'] : 0;
            return round((float)$rate*(int)$quantity/100);
        }
        else{
            return 0;
        }
    }

    /**
     * Số cp thưởng dựa vào pttt
     *
     * @param $quantity
     * @param $id
     * @return float|int
     */
    protected function getStockBonusOfStockReferBonus($quantity)
    {
        $mStockCustomer = new StockCustomerTable();
        $mStockBonusConfig = new \Modules\Admin\Models\StockBonusConfigTable();
        $status = $mStockBonusConfig->getStockBonusConfig();
        if($status['stock_refer_bonus'] == 1){
            $mStockBonusRefer = new StockBonusReferTable();
            $data = $mStockBonusRefer->getRateWithQuantity($quantity);
            $rate = isset($data['rate']) != '' ? $data['rate'] : 0;
            return round((float)$rate*(int)$quantity/100);
        }
        else{
            return 0;
        }
    }

}