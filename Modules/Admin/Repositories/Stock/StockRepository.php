<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:06 PM
 */

namespace Modules\Admin\Repositories\Stock;


use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\BankTable;
use Modules\Admin\Models\CustomerJourneyTable;
use Modules\Admin\Models\CustomerSearchLogTable;
use Modules\Admin\Models\CustomerSourceTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\CustomerChangeTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\GroupCustomerMapCustomerTable;
use Modules\Admin\Models\GroupStaffMapCustomerTable;
use Modules\Admin\Models\GroupStaffMapStaffTable;
use Modules\Admin\Models\GroupStaffTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\PotentialCustomerLogTable;
use Modules\Admin\Models\ProductTable;
use Modules\Admin\Models\ReferSourceTable;
use Modules\Admin\Models\ServiceTable;
use Modules\Admin\Models\StockBonusAmountTable;
use Modules\Admin\Models\StockBonusConfigTable;
use Modules\Admin\Models\StockBonusPaymentMethodTable;
use Modules\Admin\Models\StockBonusReferTable;
use Modules\Admin\Models\StockPublishHistoryTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Admin\Models\AppendixContractTable;
use Modules\Admin\Models\AppendixContractMapTable;
use Modules\Admin\Models\StockConfigTable;
use Modules\Admin\Models\StockConfigFileTable;
use Modules\Admin\Models\StockTable;
use Modules\Admin\Models\StockTransferContractTable;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockWalletTable;
use Modules\Admin\Models\StockHistoryTable;
use Modules\Admin\Models\StockHistoryDivideFileTable;
use Modules\Admin\Models\StockBonusTable;
use Modules\Admin\Models\StockChartTable;
use Modules\Admin\Models\StockContractTable;
use Modules\Admin\Models\StockContractFileTable;
use Modules\Admin\Models\StockContractSubFileTable;
use Modules\Admin\Models\StockTransactionTable;
use Modules\Admin\Repositories\Message\MessageRepoInterface;
use PDF;




class StockRepository implements StockRepositoryInterface
{
    protected $customer;
    protected $timestamps = true;
    protected $withrawRequest;
    protected $product;
    protected $service;
    protected $customerSource;
    protected $bank;
    protected $groupStaff;
    protected $customerJourney;
    protected $stockConfig;
    protected $stockFile;
    protected $stock;
    protected $stockTransfer;
    protected $customerContract;
    protected $stockCustomer;
    protected $stockWallet;
    protected $stockHistoryFile;
    protected $stockBonus;
    protected $stockChart;
    protected  $stockContract;
    protected $stockContractFile;
    protected $stockContractSubFile;
    protected $stockTransaction;
    protected $stockHistory;
    protected $stockPublishHistory;
    protected $stockBonusAmount;
    protected $stockBonusConfig;
    protected $stockBonusPayment;
    protected $stockBonusRefer;



    public function __construct(
        StockConfigTable $stockConfig,
        StockConfigFileTable $stockFile,
        StockTable $stock,
        StockTransferContractTable $stockTransfer,
        CustomerContactTable $customerContract,
        StockCustomerTable $stockCustomer,
        StockWalletTable $stockWallet ,
        StockHistoryTable $stockHistory,
        StockHistoryDivideFileTable $stockHistoryFile,
        StockBonusTable $stockBonus,
        StockChartTable  $stockChart,
        StockContractTable  $stockContract,
        StockContractFileTable  $stockContractFile,
        StockContractSubFileTable $stockContractSubFile,
        StockTransactionTable $stockTransaction,
        CustomerTable  $customer,
        StockPublishHistoryTable $stockPublishHistory,
        StockBonusAmountTable $stockBonusAmount,
        StockBonusConfigTable $stockBonusConfig,
        StockBonusPaymentMethodTable $stockBonusPayment,
        ProductTable $product,
    StockBonusReferTable $stockBOnusRefer


    )
    {
        $this->stockConfig = $stockConfig;
        $this->stockFile = $stockFile;
        $this->stock = $stock;
        $this->stockTransfer = $stockTransfer;
        $this->customerContract = $customerContract;
        $this->stockCustomer = $stockCustomer;
        $this->stockWallet = $stockWallet;
        $this->stockHistory = $stockHistory;
        $this->stockHistoryFile = $stockHistoryFile;       
        $this->stockBonus = $stockBonus;
        $this->stockChart = $stockChart;
        $this->stockContract = $stockContract;
        $this->stockContractFile = $stockContractFile;
        $this->stockContractSubFile = $stockContractSubFile;
        $this->stockTransaction = $stockTransaction;
        $this->customer = $customer;
        $this->stockPublishHistory = $stockPublishHistory;
        $this->stockBonusAmount = $stockBonusAmount;
        $this->stockBonusAmount = $stockBonusAmount;
        $this->stockBonusConfig = $stockBonusConfig;
        $this->stockBonusPayment = $stockBonusPayment;
        $this->product = $product;
        $this->stockBonusRefer = $stockBOnusRefer;
    }

    // todo: Stock FIle--------------
    public function getListStockFile(){
        return $this->stockFile->_getList();
    }
    public function addStockFile(array $data)
    {
        // dd("data o Stock Repository:", $data);
        return $this->stockFile->add($data);
    }

    // !--------Stock File-----------
   
    //todo:--------------Stock Config---------
    public function getStockConfig(){
        return $this ->stockConfig->getDetail();        
    }
    public function editStockConfig($data, $id){
        return $this->stockConfig->edit($data, $id);
    }
    // public function upDocRelated($data){    
    //     // return $this->stockFile->add($data);
    // }
    public function updateStock($id, $data){

        $data['fee_stock_sell_market'] = str_replace(',', '', $data["fee_stock_sell_market"]);
        $data['fee_withraw_stock'] = str_replace(',', '', $data["fee_withraw_stock"]);
        $data['fee_convert_bond_stock'] = str_replace(',', '', $data["fee_convert_bond_stock"]);
        $data['fee_transfer_stock'] = str_replace(',', '', $data["fee_transfer_stock"]);
        $data['fee_withraw_dividend'] = str_replace(',', '', $data["fee_withraw_dividend"]);
        $data['fee_convert_saving_stock'] = str_replace(',', '', $data["fee_convert_saving_stock"]);

        $message = '';

        if ($data['fee_stock_sell_market'] < 0 || $data['fee_stock_sell_market'] >= 100) {
            $message = 'Phí bán cổ phiếu qua chợ phải trong khoảng 0 -> 100 <br>';
        }

        if ($data['fee_withraw_stock'] < 0 || $data['fee_withraw_stock'] >= 100) {
            $message = $message.'Phí rút tiền ví cổ phiếu qua chợ phải trong khoảng 0 -> 100 <br>';
        }

        if ($data['fee_convert_bond_stock'] < 0 || $data['fee_convert_bond_stock'] >= 100) {
            $message = $message.'Phí chuyển đổi hợp đồng hợp tác đầu tư sang cổ phiếu phải trong khoảng 0 -> 100 <br>';
        }

        if ($data['fee_transfer_stock'] < 0 || $data['fee_transfer_stock'] >= 100) {
            $message = $message.'Phí chuyển nhượng cổ phiếu phải trong khoảng 0 -> 100 <br>';
        }

        if ($data['fee_withraw_dividend'] < 0 || $data['fee_withraw_dividend'] >= 100) {
            $message = $message.'Phí rút tiền ví cổ tức phải trong khoảng 0 -> 100 <br>';
        }

        if ($data['fee_convert_saving_stock'] < 0 || $data['fee_convert_saving_stock'] >= 100) {
            $message = $message.'Phí chuyển đổi hợp đồng tiết kiệm sang cổ phiếu phải trong khoảng 0 -> 100 <br>';
        }

        if ($message !== '') {
            return [
                'error' => 1,
                'message' => $message
            ];
        }

        $this->stockConfig->edit($id, $data);
        return [
            'error' => 0,
            'message' => 'Cập nhật phí thành công'
        ];
    }
    public function removeStockFile($data){        
        return $this->stockFile->remove($data);
    }   
    // !--------------------Stock------

    // todo: Phát hành cổ phiếu: Stock Publish --------
    public function createStock($data){
        $data['quantity_sale'] = 0;
        $data['stock_config_id'] = 1;
        $data['money'] = (float) (str_replace(",","",$data['money']));
        $data['quantity'] = (int) (str_replace(",","",$data['quantity']));
        $data['quantity_min'] = (int) (str_replace(",","",$data['quantity_min']));
        $data['publish_at'] = Carbon::now();
        $data['is_active'] = 1;
        $data['created_at'] = Carbon::now();
        $data['created_by'] = Auth::id();

//        $data["quantity"] = parseInt(",",$data['quantity']);
        $data["quantity"] = (int)(str_replace(",","", $data["quantity"]));

        $lastStock = $this->stock->getLastStock();
        $quantityPublishStock = is_null($lastStock)?0:$lastStock->quantity;
        $quantitySale = is_null($lastStock)?0:$lastStock->quantity_sale;
        $remainQuantity = $quantityPublishStock - $quantitySale;
//        dd($remainQuantity, $data['quantity']);
        $data['quantity_publish'] = $data['quantity'];
        $data['quantity'] = $data['quantity'] + $remainQuantity;

        // todo: Deactive All Stock before Insert-------------
        $this->stock->deActiveAllStock(["is_active"=>0]);                    
        return $this->stock->add($data);

    }
    public function getLastStockPublish(){
       $stock =  $this->stock->getLastStock();
        $stock = $stock ?? new StockTable();

       $stock['total_amount'] = $stock['money']*$stock['quantity'];
       return $stock;
    }
    public function editStockPublish($data){
        $id = $data['stock_id'];
        unset($data['stock_id']);

        if(isset($data['money'])){
            $data['money'] = (float) str_replace(",","",$data['money']);
        }
        $data['updated_at'] = Carbon::now();
        $data['updated_by'] = Auth::id();
        $this->stock->edit($data,$id);
        $dataLog = $this->stock->getLastStock()->toArray();
        $this->stockPublishHistory->add($dataLog);

        return $this->stock->edit($data,$id);
    }
    public function getListStockPublish($filters){   
        
        if (isset($filters['publish_at']) != "") {

            $filters['startTime']=[];
            $filters['endTime']=[];
            $arr_filter = explode(" - ", $filters["publish_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTime'] =$startTime;
            $filters['endTime'] =$endTime;
            unset($filters['publish_at']);
        }                 
        return $this->stock->getList($filters);
    }
    // !--------Phát hành cổ phiếu-------------
//    todo: Ghi Log Phát hành Cổ phiếu---------
public function createStockPublishHistory(){
        $stock = $this->getLastStockPublish()->toArray();
        unset($stock['total_amount']);
        return $this->stockPublishHistory->add($stock);
//        return $this->stockPublishHistory->createStockPublishHistory->add($stock);
}
//------------Ghi Log Phát hành Cổ phiếu---------

    // todo: Stock Transfer Contract------------
    public function getListStockTransfer($filters){
        if (isset($filters['created_at']) != "") {
            $filters['startTime']=[];
            $filters['endTime']=[];
            $arr_filter = explode(" - ", $filters["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTime'] =$startTime;
            $filters['endTime'] =$endTime;
            unset($filters['created_at']);
        }
        return $this->stockTransfer->getList($filters);
    }
    public function getDetailStockTransfer($id){
        return $this->stockTransfer->getDetail($id);
    }
    public function deActiveContract($id){
        $data = ["is_active"=>0];
        return $this->customerContract->updateContract($id, $data);    }
    public function confirmTransferStock($id){
        try {
            $stockTransfer = self::getDetailStockTransfer($id);
            $customer_contract_id = $stockTransfer->customer_contract_id;
            //todo: Check Valid---------------------
            $customerContract = $this->customerContract->getDetailContract($customer_contract_id);
            if ($customerContract->is_active == 0 ) {
                return ['error' => 1, 'message' => 'Hợp đồng đã ngưng hoạt động'];

            }
//            if ($customerContract->term_time_type == 1) {
//                if (Carbon::now() > Carbon::parse($customerContract->customer_contract_end_date))
//                    return ['error' => 1, 'message' => 'Hợp đồng đã hết hạn'];
//            }
            $endDate = Carbon::parse($customerContract->customer_contract_end_date);
            if ($customerContract->product_category_id == 1){
                if (!($customerContract->is_active == 1 && $endDate < Carbon::now())){
                    return ['error' => 1, 'message' => 'Hợp đồng này không đủ điều kiện chuyển đổi'];

                }
            }
            $customerContract->withdraw_min_time ??0;
            $customerContract->term_time_type ??0;
            $minTime = Carbon::parse($customerContract->customer_contract_start_date)->addMonth((int)($customerContract->withdraw_min_time));

            if ($customerContract ->product_category_id == 2){
                if (!($customerContract->is_active==1 && $minTime < Carbon::now()) && $customerContract->term_time_type == 1){
                    return ['error' => 1, 'message' => 'Hợp đồng này không đủ điều kiện chuyển đổi'];

                }
            }
//            end: Check Valid--------------------------------------------

//            todo: Deactive Customer Contract----------------------------
            $this->deActiveContract($customer_contract_id);
            //  todo:Cộng tiền vào ví--------------
            $quantityStock =$stockTransfer->quantity;
            $moneyPaid = $stockTransfer->money_paid;
            $customer_id = $stockTransfer->customer_id;

            //todo: Insert hoặc cập nhật vào bảng StockCustomer
            $stockCus = $this->stockCustomer->getStockByCustomer($customer_id);
            if ($stockCus == null) {
                $data = [
                    "customer_id" => $customer_id,
                    "wallet_stock_money" => $moneyPaid,
                    "wallet_dividend_money" => 0,
                    "last_updated" => Carbon::now(),
                    "created_at" => Carbon::now()
                ];
                $this->stockCustomer->createStockCustomer($data);
            } else {
                $stock = $stockCus->stock  + $quantityStock;
                $data = [
                    "customer_id" => $customer_id,
                    "wallet_stock_money" => ($moneyPaid + $stockCus->wallet_stock_money),
                    'stock' => $stock,
//                    "wallet_dividend_money" => 0,
                    "last_updated" => Carbon::now(),
                    "created_at" => Carbon::now()
                ];

                $this->stockCustomer->edit($data, $stockCus->stock_customer_id);
            }

            // todo: Insert hoặc ko insert bảng Stock Wallet
            $wallet = $this->stockWallet->getLastStockByCustomer($customer_id);
            $now = Carbon::now();


            if ($wallet == null) {
                $data = [
                    "customer_id" => $customer_id,
                    "type" => "add",
                    "source" => "stock",
                    "total_before" => 0,
                    "total_money" => $moneyPaid,
                    "total_after" => $moneyPaid,
                    'day' => $now->day,
                    'month' => $now->month,
                    'year' => $now->year,
                    'created_at' => $now,
                    "created_by" => Auth::id()

                ];

                $this->stockWallet->createStockWallet($data);

            } else {
                $data = [
                    "customer_id" => $customer_id,
                    "type" => "add",
                    "source" => "stock",
                    "total_before" => $wallet->total_after,
                    "total_money" => ($moneyPaid),
                    "total_after" => ($wallet->total_after + $moneyPaid),
                    'day' => $now->day,
                    'month' => $now->month,
                    'year' => $now->year,
                    'created_at' => $now,
                    "created_by" => Auth::id()
                ];
                $this->stockWallet->createStockWallet($data);
        }
//            todo: Insert vào bảng Stock Transaction--------/


            $data = [
                'stock_type'=>'basic',
                'source' => 'convert',
                'type' => 'add',
                'stock_order_id' =>$id,
                'customer_id'=>$customer_id,
                'quantity_before' => $stockCus->stock,
                'quantity' => $stockTransfer->quantity,
                'quantity_after'=>($stockCus->stock + $stockTransfer->quantity),
                'money_before'=>$stockTransfer->money,
                'money_standard'=>10000,
                'created_at'=>$now->format("Y-m-d"),
                'date'=>$now->day,
                'month'=>$now->month,
                'year'=>$now->year,
                'created_by'=>Auth::id()

            ];
            $this->stockTransaction->createTransaction($data);

            $rate = isset($customerContract->bonus_rate) != '' ? $customerContract->bonus_rate : 0;
            $bonus = round($rate * $stockTransfer->quantity / 100);
            if((int)$bonus > 0){
                $dataStockBonus = [
                    'stock_type'=>'bonus',
                    'source' => 'convert',
                    'type' => 'add',
                    'stock_order_id' =>$id,
                    'customer_id'=>$customer_id,
                    'quantity_before' => $stockCus->stock_bonus,
                    'quantity' => $bonus,
                    'quantity_after'=>($stockCus->stock_bonus + $bonus),
                    'money_before'=>$stockTransfer->money,
                    'money_standard'=>10000,
                    'created_at'=>$now->format("Y-m-d"),
                    'date'=>$now->day,
                    'month'=>$now->month,
                    'year'=>$now->year,
                    'created_by'=>Auth::id()

                ];
                $this->stockTransaction->createTransaction($dataStockBonus);
                // update stock_bonus customer
                $data = [
                    "customer_id" => $customer_id,
                    'stock_bonus' => $stockCus->stock_bonus + $bonus,
                    "last_updated" => Carbon::now(),
                    "created_at" => Carbon::now()
                ];
                $this->stockCustomer->edit($data, $stockCus->stock_customer_id);
            }
//            todo: Insert hoacwj update stock contract
            $this->createOrUpdateStockContract($customer_id, $stockTransfer->quantity);
//            end: Insert hoặc update stock contract-----


            // TODO: push noti---------------------------------
            $mNoti = new JobsEmailLogApi();
//            $order = $this->stockOrders->getDetailTransfer($param['stock_order_id']);
            $data =[
                'key' =>'stock_admin_convert_order',
                'customer_id' =>$stockTransfer['customer_id'],
                'object_id' => $stockTransfer['stock_tranfer_contract_id'],
                'stock_tranfer_contract_id'=> $stockTransfer['stock_tranfer_contract_id']
            ];
            $mNoti->sendNotification($data);
            $messageRepo = app()->get(MessageRepoInterface::class);
            $customer = $this->customer->getDetail($stockTransfer['customer_id']);
            $messageRepo->sendSMS(
                'stock_admin_convert_order',
                [
                    'phone'=>$customer->phone
                ],[
                    'stock_tranfer_contract_id'=>$stockTransfer['stock_tranfer_contract_id'],
                    'full_name'=>$customer['full_name']
                ]


            );
//                end: push noti--------------------------
            //todo: Update trạng thái đã duyệt-----------
            $this->stockTransfer->edit($id,['status'=>'approved']);
            return ['error' => 0, 'message' => 'Xác nhận chuyển đội hợp đồng thành công'];
        }catch(\Exception $e){
            return ['error'=>1,'message'=>$e->getMessage()];

        }
    }
    public function createOrUpdateStockContract($customer_id, $quantity){
        try{
            $stockContract = $this->stockContract->getStockContractByCustomer($customer_id);
            $customer = $this->customer->getDetail($customer_id);
            $mStockCustomer = new StockCustomerTable();
            $quantityStockCustomer = $mStockCustomer->getStock($customer_id)['stock'];
            //todo: Nếu ko có hợp đồng thì tạo hợp đồng
            if(is_null($stockContract)){
                $stockConfig = $this->stockConfig->getDetail();
                $lastNumStockContract = $this->stockContract->getLastStockContract();
                $stock_config_code= $stockConfig->stock_config_code;
                $sample = '0000000';
                $num = $lastNumStockContract + 1;
                $stock_contract_code =$stock_config_code .'-'.preg_replace("/$/i", $num, $sample);
//                echo $res;
                $data = [
                    'stock_contract_code'=>$stock_contract_code,
                    'customer_id'=>$customer->customer_id,
                    'quantity'=> $quantityStockCustomer,
                    'customer_name'=>$customer->full_name,
                    'customer_phone'=>$customer->phone,
                    'customer_email'=>$customer->email,
                    'customer_residence_address'=>$customer->residence_address,
                    'customer_ic_no'=>$customer->ic_no,
                    'is_active'=>1,
                    'is_deleted'=>0,
                    'created_by'=>Auth::id(),
                    'updated_by'=>Auth::id(),
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()
                ];
                $this->stockContract->add($data);
            }else{
                $data = ['quantity' => $quantityStockCustomer];
                $this->stockContract->edit($stockContract->stock_contract_id, $data);

//             todo: Tạo file phụ lục hợp đồng------------------
//             todo: Xuất file PDF
                $numContract = $this->stockContractSubFile->getQuanConOfCustomer($stockContract->stock_contract_id);
                $numContract++;
//                dd('so luong: ', $numContract);
                $infoContract = [
                    'defaultDot'=>"…………………",
                    'customer'=>$customer->toArray(),
                    'stockContract'=>$stockContract->toArray(),
                    'numContract'=>$numContract

                ];
                $time = Carbon::now()->timestamp;
                $pdf = PDF::loadView('admin::stock.pdf.stock-contract', ['data' => $infoContract]);
                $pathRelative = "uploads/Phu_luc_hop_dong_$time.pdf";
                $path = public_path('uploads/');
                $fileName = "Phu_luc_hop_dong_$time.pdf";
//            $fileName =  $post['title'] . '.' . 'pdf' ;
                $path = $path . $fileName;
                $pdf->save($path);

                $dataSubContract = [
                    'stock_contract_id'=>$stockContract->stock_contract_id,
                    'file'=>url('/').'/'.$pathRelative,
                    'created_at'=>Carbon::now(),
                    'created_by'=>Auth::id()
                ];
                $this->stockContractSubFile->add($dataSubContract);

//            // TODO: push noti
//            $mNoti = new SendNotificationApi();
//            $mNoti->sendNotification([
//                'key' => 'stock_admin_convert_order',
//                'customer_id' => \auth()->id(),
//                'object_id' => $id
//            ]);

            }

        }
        catch(\Exception $ex){
            return null;
        }


    }

    public function editStockTransfer($id,$data){
        if($data['status'] == 'cancel'){
            $stockTransfer = $this->stockTransfer->getDetail($id);
            // TODO: push noti---------------------------------
            $mNoti = new JobsEmailLogApi();
//            $order = $this->stockOrder
//            $order = $this->stockOrder
//            $order = $this->stockOrders->getDetailTransfer($id);
            $noti =[
                'key' =>'stock_admin_convert_cancel',
                'customer_id' =>$stockTransfer['customer_id'],
                'object_id' => $stockTransfer['stock_tranfer_contract_id'],
                'stock_tranfer_contract_id'=> $stockTransfer['stock_tranfer_contract_id']
            ];

            $mNoti->sendNotification($noti);
//            todo: Send Sms------------------
            $messageRepo = app()->get(MessageRepoInterface::class);
            $customer = $this->customer->getDetail($stockTransfer['customer_id']);
            $messageRepo->sendSMS(
                'stock_admin_convert_cancel',
                [
                    'phone'=>$customer->phone
                ],[
                    'stock_tranfer_contract_id'=>$stockTransfer['stock_tranfer_contract_id'],
                    'full_name'=>$customer['full_name']
                ]
            );
//                end: push noti--------------------------
        }
        $result = $this->stockTransfer->edit($id,$data);
        return $result;
    }
    // !--------Stock Transfer Contract---------
  
    
    // todo: Dividend--------
    public function getListDividend($filters){        
        $stockPublish = $this->stock->getLastStock();
        if(!is_null($stockPublish)){
            $filters['stock_id'] = $stockPublish->stock_id;
        }

        return $this->stockHistory->getList($filters);
    }
    public function removeDividendFile($arr,$id){
        $arr = json_decode($arr);
        return $this->stockHistoryFile->remove($arr,$id);

    }
    public function getListDividendFile(){
        return $this->stockHistoryFile->getListFile([]);
}
    public function addDividend($data){
        $now = Carbon::now();
        $data["date_publish"]=Carbon::parse($data["date_publish"]);
        $data['year'] = $now->year;
        $data['is_active']=0;
        $data['stock_bonus_before'] = str_replace(",","",$data['stock_bonus_before']);
        $data['stock_bonus_after'] = str_replace(",","",$data['stock_bonus_after']);
        $data['created_at']=Carbon::now();
        $data['updated_at']=Carbon::now();
        $data['created_by']=Auth::id();
        $data['updated_by']=Auth::id();

        $id = $this->stockHistory->add($data);
        // todo: noti khi chia cổ tức
        $mStockCustomer = new StockCustomerTable();
        $mNoti = new JobsEmailLogApi();
        $lstCustomer = $mStockCustomer->getListStockCustomer();
        if(count($lstCustomer) > 0){
            foreach ($lstCustomer as $item){
                $data =[
                    'key' => 'stock_admin_dividend',
                    'customer_id' => $item['customer_id'],
                    'object_id' => $id
                ];
                $mNoti->sendNotification($data);
            }
        }
        return ['error'=>0,'message'=>'Thêm cổ tức thành công'];
    }
    public function editDividend($data){
        $id =$data["stock_history_divide_id"];
        $now = Carbon::now();
//        $data["date_publish"]=Carbon::parse($data["date_publish"]);
        $data["date_publish"]=Carbon::createFromFormat("d-m-Y",$data["date_publish"]);
        $data['year'] = $now->year;
        $data['stock_bonus_before'] = str_replace(",","",$data['stock_bonus_before']);
        $data['stock_bonus_after'] = str_replace(",","",$data['stock_bonus_after']);
        $data['created_at']=Carbon::now();
        $data['updated_at']=Carbon::now();
        $data['created_by']=Auth::id();
        $data['updated_by']=Auth::id();

        $this->stockHistory->edit($id, $data);
        return ['error'=>0,'message'=>'Cập nhật cổ tức thành công'];
    }
    public function createDividendFIle($data){
        return $this->stockHistoryFile->add($data);        
    }
    // !--------Dividend-------
    
    // todo: Stock Bonus-----
    public function getListStockBonus(array $filters=[]){
        return $this->stockBonus->_getList([]);
    }
    public function editStockBonus(array $data=[]){
        $stockBonusId = $data['stock_bonus_id'];
        unset($data['stock_bonus_id']);
        return $this->stockBonus->edit($data, $stockBonusId);
    }
    // !--------Stock Bonus--

//    todo: Stock Chart-------------------------------------
public function getListStockChart(array $filters = []){
    if (isset($filters['created_at']) != "") {
        $filters['startTime']=[];
        $filters['endTime']=[];
        $arr_filter = explode(" - ", $filters["created_at"]);
        $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
        $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
        $filters['startTime'] =$startTime;
        $filters['endTime'] =$endTime;
        unset($filters['created_at']);
//        dd('filters la gi: ', $filters);
    }
    return $this->stockChart->getList($filters);
}
public function addStockChart($data){
    $time = Carbon::createFromFormat("d-m-Y H:i", $data["time"])->format("Y-m-d H:i:s");
    $filter = ['time' => $time];
        $exist = $this->stockChart->findByTime($filter);
        if($exist == null ){
            $data['time'] = Carbon::createFromFormat("d-m-Y H:i",trim($data['time']));
            $data['timestamp'] = $data['time']->timestamp;
            $data['created_at'] = Carbon::now();
             $this->stockChart->add($data);
             return ['error'=>0, 'message'=>'Thêm thành công'];
        }else{
            return ['error'=>1, 'message' => 'Thời gian đã tồn tại'];        }

}
    public function editStockChart($data){
        $time = Carbon::createFromFormat("d-m-Y H:i", $data["time"])->format("Y-m-d H:i:s");
        $filter = [
            'time' => $time,
            'stock_chart_id'=>$data['stock_chart_id']
            ];
        $exist = $this->stockChart->findByTime($filter);
        if($exist == null){
            $id = $data["stock_chart_id"];
            $data["time"] = trim($data["time"]);
            $data['time'] = Carbon::parse($data['time']);
            $data['timestamp'] = $data['time']->timestamp;
            $this->stockChart->edit($data, $id);
            return ['error'=>0,'message'=> 'Cập nhật thành công'];
        }else{
            return ['error'=> 1,'message'=> 'Thời gian đã tồn tại'];
        }








    }
    public function removeStockChart($id){
        return $this->stockChart->remove($id);
    }
//end: Stock Chart--------------
//todo: Stock Contract---------
public function listStockContract($filters){
    if (isset($filters['created_at'])) {
        $filters['startTime']=[];
        $filters['endTime']=[];
        $arr_filter = explode(" - ", $filters["created_at"]);
        $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
        $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
        $filters['startTime'] =$startTime;
        $filters['endTime'] =$endTime;
        unset($filters['created_at']);
    }
        return $this-> stockContract->getList($filters);
}
//todo: Stock Contract---------
    public function listStockContractExport($filters){
        if (isset($filters['created_at'])) {
            $filters['startTime']=[];
            $filters['endTime']=[];
            $arr_filter = explode(" - ", $filters["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTime'] =$startTime;
            $filters['endTime'] =$endTime;
            unset($filters['created_at']);
        }
        return $this-> stockContract->_getListExport($filters);
    }
public function detailStockContract($id){
        return $this->stockContract->detail($id);
}
public function createStockContractFile($data){
        return $this->stockContractFile->add($data);

}
//end: Stock Contract----------
//todo: Stock COntract FIle - Stock Contract Sub File----
public function listStockContractFile($filters){

    $list =$this->stockContractFile->getList($filters);
    return [
        'listStockContract' => $list
    ];
//        return $this->stockContractFile->getDataTable($filters);

}
    public function listStockContractSubFile($filters){
        return $this->stockContractSubFile->getList($filters);
    }
    public function listStockContractDatatable($filter){
        return $this->stockContract->getDataTable($filter);
    }
    public function getLastPositionStockContractFile($stock_contract_id){
        $filter['stock_contract_id'] = $stock_contract_id;
        $result = $this->stockContractFile->getLastPositionStockContractFile($filter);
        return $result??0;
    }
//end: Stock Contract FIle-----
//todo: Stock Bonus Amount------------------------
public function listBonusAmount($filter){
        return $this->stockBonusAmount->getList($filter);
}
public function addStockBonusAmount($data){
        try{
            $data['stock_from'] = str_replace(",","",$data['stock_from']);
            $data['stock_to'] = str_replace(",","",$data['stock_to']);
            $this->stockBonusAmount->add($data);
            return ['error'=>true, 'message'=>'Thêm thành công'];
        }catch(\Exception $ex){
            return ['error'=>false, 'message'=>'Thêm tất bại'];

        }
}
    public function editStockBonusAmount($data){
        try{
            $id = $data['stock_bonus_amount_id'];
            unset($data['stock_bonus_amount_id']);
            $this->stockBonusAmount->edit($data,$id);
            return ['error'=>true, 'message'=>'Cập nhật thành công'];
        }catch(\Exception $ex){
            return ['error'=>false, 'message'=>'Cập nhật thất bại'];

        }
    }
    public function getLastBonusAmount(){
        return $this->stockBonusAmount->getLast();
    }
    public function getLastBonusRefer(){
        return $this->stockBonusRefer->getLast();
    }
//    end: Stock BOnus Amount-----------------
//todo: Stock Config BOnus--------------------
public function editStockConfigBonus($data){
        $params = [$data['stock_bonus_config_id'] =>$data['is_active']=="true"?1:0];
        $this->stockBonusConfig->edit($params);
}
public function removeBonusOrder($id){
        $this->stockBonusAmount->remove($id);
        return ['error'=>false, 'message'=>'Đã xóa thành công'];
}
//todo: Stock BOnus COnfig-------------------
public function getStockBonusConfig(){
        return $this->stockBonusConfig->getFirst();
}
//end: Stock Config BOnus-------------------
//todo: Stock Bonus Payment-----------------
public function detailStockBonusPayment(){
        return $this->stockBonusPayment->detail();
}
public function editBonusPayment($data){
        try{
            $this->stockBonusPayment->edit($data);
            return ['error'=>false,'message'=>'Cập nhật thành công'];
        }
        catch(\Exception $ex){
            return ['error'=>true,'message'=>'Cập nhật thất bại'];
        }

}
//end: Stock Bonus Payment------------------
//todo: BOnus Transfer-----------------------
public function editBonusTransfer($data){
        $id = $data['product_id'];
        unset($data['product_id']);
        return $this->product->editProduct($id, $data);

}
//end: Bonus Transfer-----------------------
//todo: Bonus Referral----------------------
public function listBonusReferral($filter){
        return $this->stockBonusRefer->getList($filter);
}
public function addBonusReferral($data){
    $data['stock_from'] = str_replace(",","",$data['stock_from']);
    $data['stock_to'] = str_replace(",","",$data['stock_to']);
        return $this->stockBonusRefer->add($data);
}
public function editBonusReferral($data){
        $id = $data['stock_bonus_refer_id'];
        unset($data['stock_bonus_refer_id']);
        return $this->stockBonusRefer->edit($data,$id);
}
public function removeReferral($data){
        return $this->stockBonusRefer->remove($data);
}
//end: Bonus Referral----------------------


}