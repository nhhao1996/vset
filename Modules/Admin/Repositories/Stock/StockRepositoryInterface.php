<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:06 PM
 */

namespace Modules\Admin\Repositories\Stock;


interface StockRepositoryInterface
{
    public function getStockConfig();
    public function createOrUpdateStockContract($customer_id, $quantity);
    public function createStockPublishHistory();
    public function getStockBonusConfig();
   
}