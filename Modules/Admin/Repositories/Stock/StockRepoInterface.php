<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/9/2021
 * Time: 3:53 PM
 * @author nhandt
 */

namespace Modules\Admin\Repositories\Stock;


interface StockRepoInterface
{
    public function getList(array $filters = []);
    public function show($id);
    public function getDetailSellerMarket($stockMarketId);
    public function showPopupReceiptDetail($param);
    public function addReceiptDetail($param);
    public function getListReceiptDetail($param);
    public function getStockPublishRemaining();
    public function getStockMarketRemaining($objId);
    public function allowsConfirm($stock_order_id);
    public function createReceipt($param);
    public function cancelConfirm($param);
}