<?php
namespace Modules\Admin\Repositories\Report;

use App\Exports\ExportFile;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Models\CustomerContactLogTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\GroupStaffTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\OrderServicesTable;
use Modules\Admin\Models\PaymentMethodTable;
use Modules\Admin\Models\CustomerContractInterestByDateTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\ReceiptDetailTable;
use Modules\Admin\Models\ReferSourceTable;
use Modules\Admin\Models\StaffsTable;

class ReportRepository implements ReportRepositoryInterface
{
    protected $mOrder;
    protected $mOrderServices;
    protected $mPaymentMethod;
    protected $mCustomerContractInterestByDate;
    protected $mCustomerContractInterestByMonth;
    protected $mCustomerContactLog;
    protected $mReceiptDetail;
    protected $referSource;
    protected $customer;
    protected $staff;
    protected $customerContract;
    protected $groupStaff;

    //Trạng thái đơn hàng hoàn thành
    const PAYSUCCESS = 'paysuccess';
    //Product category "Gói Trái Phiếu"
    const TP = '1';
    //Product category "Tiết Kiệm"
    const TK = '2';

    public function __construct(
        OrderTable $mOrder,
        OrderServicesTable $mOrderServices,
        PaymentMethodTable $mPaymentMethod,
        CustomerContractInterestByDateTable $mCustomerContractInterestByDate,
        CustomerContractInterestByMonthTable $mCustomerContractInterestByMonth,
        CustomerContactLogTable $mCustomerContactLog,
        ReceiptDetailTable $mReceiptDetail,
        ReferSourceTable $referSource,
        CustomerTable $customer,
        StaffsTable $staff,
        CustomerContactTable $customerContract,
        GroupStaffTable $groupStaff
    )
    {
        $this->mOrder = $mOrder;
        $this->mOrderServices = $mOrderServices;
        $this->mPaymentMethod = $mPaymentMethod;
        $this->mCustomerContractInterestByDate = $mCustomerContractInterestByDate;
        $this->mCustomerContractInterestByMonth = $mCustomerContractInterestByMonth;
        $this->mCustomerContactLog = $mCustomerContactLog;
        $this->mReceiptDetail = $mReceiptDetail;
        $this->referSource = $referSource;
        $this->customer = $customer;
        $this->staff = $staff;
        $this->customerContract = $customerContract;
        $this->groupStaff = $groupStaff;
    }

    /**
     * Filter BÁO CÁO DOANH THU - THANH TOÁN
     * @param array $params
     *
     * @return array|mixed
     */
    public function filterRevenue($params = [])
    {
        $time = strip_tags($params['time']);
        $type = strip_tags($params['type']);
        $params['process_status'] = self::PAYSUCCESS;

        //Các phương thức thanh toán
        $result = [
            'chart' => [],
            'total' => [],
            'payment_method' => [],
        ];
        //Nếu tìm kiếm trong khoảng thời gian
        if ($time != null) {
            /*
            $dateTime = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])
                ->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])
                ->format('Y-m-d');
            $params['created_at'] = [$startTime . ' 00:00:00', $endTime.' 23:59:59'];
            //Data của orders
            $arrDataChart = $this->mReceiptDetail->getReportRevenue($params)
                ->toArray();
            $arrDataChartPie = $this->mReceiptDetail->getReportRevenueByMethod($params)
                ->toArray();

            $total = 0;
            foreach ($arrDataChart as $itemChart){
                $total = $total + $itemChart['total'];
            }
            return [
                'chart' => $arrDataChart,
                'total' => $total
            ];
            //Nếu loại là tất cả hoặc dịch vụ thì lấy dữ liệu từ or

             */
            //Explode khoảng ngày thành 2 phần tử
            $dateTime = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])
                ->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])
                ->format('Y-m-d');
            $params['created_at'] = [$startTime . ' 00:00:00', $endTime.' 23:59:59'];
            //Data của orders
            $params['object_type'] = 'order';
            $order = $this->mReceiptDetail->getByConditionNotTest($params)
                ->toArray();
            $data = $order;
            //Nếu loại là tất cả hoặc dịch vụ thì lấy dữ liệu từ order_services
            if ($type == null || $type == 3) {
                $params['object_type'] = 'order-service';
                $orderService = $this->mReceiptDetail->getByConditionTypeNotTest($params)
                    ->toArray();
                //Merge data của orders và order_services lại với nhau.
                $data = array_merge($order, $orderService);
            }
            //Số ngày
            $datediff = ((strtotime($endTime) - strtotime($startTime)) / (60 * 60 * 24)) + 1;
            $total = 0;
            $pMTransfer = 0;
            $pMInterest = 0;
            $pMBonus = 0;
            $pMCash = 0;
            for ($i = 0; $i < $datediff; $i++) {
                //Ngày tiếp đến d/m/Y
                $tomorrow = date('d/m/Y', strtotime($startTime . "+" . $i . " days"));
                //Ngày tiếp đến d/m
                $tomorrowDM = date('d/m', strtotime($startTime . "+" . $i . " days"));
                $tempTotal = 0;
                if ($data != []) {
                    foreach ($data as $item) {
                        $createAt = Carbon::createFromFormat('Y-m-d H:i:s', $item['created_at'])
                            ->format('d/m/Y');
                        if ($createAt == $tomorrow) {
                            $tempTotal += $item['amount'];
                            if ($item['receipt_type'] == 'transfer') {
//                                $pMTransfer += 1;
                                $pMTransfer += $item['amount'];
                            } elseif ($item['receipt_type'] == 'interest') {
//                                $pMInterest += 1;
                                $pMInterest += $item['amount'];
                            } elseif ($item['receipt_type'] == 'bonus') {
//                                $pMBonus += 1;
                                $pMBonus += $item['amount'];
                            } elseif ($item['receipt_type'] == 'cash') {
//                                $pMCash += 1;
                                $pMCash += $item['amount'];
                            }
                        }
                    }
                }
                $total += $tempTotal;
                if ($tempTotal != 0) {
                    $result['chart'][] = ['month' => $tomorrow, 'order' => $tempTotal];
                }
            }

            $pMTransfer = $pMTransfer == 0 || $total == 0 ? 0 : $pMTransfer;
            $pMInterest = $pMInterest == 0 || $total == 0 ? 0 : $pMInterest;
            $pMBonus = $pMBonus == 0 || $total == 0 ? 0 : $pMBonus;
            $pMCash = $pMCash == 0 || $total == 0 ? 0 : $pMCash;

            $result['total'] = $total;
            //Số lượng theo phương thức thanh toán
            $result['payment_method'][0] = ["Task", "Hours per Day"];
            $result['payment_method'][1] = [__('Chuyển khoản'), $pMTransfer];
            $result['payment_method'][2] = [__('Ví lãi'), $pMInterest];
            $result['payment_method'][3] = [__('Ví thưởng'), $pMBonus];
            $result['payment_method'][4] = [__('Tiền mặt'), $pMCash];

            return $result;
        } else {
            //Tìm kiếm theo năm

            //Data của orders
            $params['object_type'] = 'order';
            $order = $this->mReceiptDetail->getByConditionNotTest($params)
                ->toArray();
            $data = $order;
            //Nếu loại là tất cả hoặc dịch vụ thì lấy dữ liệu từ order_services
            if ($type == null || $type == 3) {
                $params['object_type'] = 'order-service';
                $orderService = $this->mReceiptDetail->getByConditionTypeNotTest($params)
                    ->toArray();
                //Merge data của orders và order_services lại với nhau.
                $data = array_merge($order, $orderService);
            }
            $total = 0;
            $pMTransfer = 0;
            $pMInterest = 0;
            $pMBonus = 0;
            $pMCash = 0;
            for ($i = 1; $i <= 12; $i++) {
                $tempTotal = 0;
                if ($data != []) {
                    foreach ($data as $item) {
                        if ($item['month'] == $i) {
                            $tempTotal += $item['amount'];
                            if ($item['receipt_type'] == 'transfer') {
//                                $pMTransfer += 1;
                                $pMTransfer += $item['amount'];
                            } elseif ($item['receipt_type'] == 'interest') {
//                                $pMInterest += 1;
                                $pMInterest += $item['amount'];
                            } elseif ($item['receipt_type'] == 'bonus') {
//                                $pMBonus += 1;
                                $pMBonus += $item['amount'];
                            } elseif ($item['receipt_type'] == 'cash') {
//                                $pMCash += 1;
                                $pMCash += $item['amount'];
                            }
                        }
                    }
                }
                if ($tempTotal != 0) {
                    $result['chart'][] = [
                        'month' => 'Tháng ' .$i,
                        'order' => $tempTotal
                    ];
                }
                $total += $tempTotal;
            }

            $pMTransfer = $pMTransfer == 0 || $total == 0 ? 0 : $pMTransfer/$total;
            $pMInterest = $pMInterest == 0 || $total == 0 ? 0 : $pMInterest/$total;
            $pMBonus = $pMBonus == 0 || $total == 0 ? 0 : $pMBonus/$total;
            $pMCash = $pMCash == 0 || $total == 0 ? 0 : $pMCash/$total;

            $result['total'] = $total;
            //Số lượng theo phương thức thanh toán
            $result['payment_method'][0] = ["Task", "Hours per Day"];
            $result['payment_method'][1] = [__('Chuyển khoản'), $pMTransfer];
            $result['payment_method'][2] = [__('Ví lãi'), $pMInterest];
            $result['payment_method'][3] = [__('Ví thưởng'), $pMBonus];
            $result['payment_method'][4] = [__('Tiền mặt'), $pMCash];
            $result['payment_method'][0] = ["Task", "Hours per Day"];

            return $result;
        }
    }

    /**
     * Filter BÁO CÁO LÃI SUẤT
     * @param array $params
     *
     * @return array|mixed
     */
    public function filterInterest($params = [])
    {
        $time = strip_tags($params['time']);
        $params['process_status'] = self::PAYSUCCESS;
        $result = [
            'chart_line' => [['day', __('Hợp tác đầu tư'), __('Tiết kiệm')]]
        ];
        //Tìm kiếm theo ngày
        if ($time != null) {
            //Explode khoảng ngày thành 2 phần tử
            $dateTime = explode(" - ", $time);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])
                ->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])
                ->format('Y-m-d');
            $params['created_at'] = [$startTime . ' 00:00:00', $endTime . ' 23:59:59'];
            //Data trong db
            $data = $this->mCustomerContractInterestByDate
                ->getByConditionNotTest($params);
            //Số ngày
            $datediff = ((strtotime($endTime) - strtotime($startTime)) / (60 * 60 * 24)) + 1;
            $totalFull = 0;
            for ($i = 0; $i < $datediff; $i++) {
                //Ngày tiếp đến d/m/Y
                $tomorrow = date('d/m/Y', strtotime($startTime . "+" . $i . " days"));
                //Ngày tiếp đến d/m
                $tomorrowDM = date('d/m', strtotime($startTime . "+" . $i . " days"));
                //Số tiền của trái phiếu
                $tempTP = 0;
                //Số tiền của tiết kiệm
                $tempTK = 0;
                foreach ($data as $item) {
                    $createAt = Carbon::createFromFormat('Y-m-d H:i:s', $item['created_at'])
                        ->format('d/m/Y');
                    if ($createAt == $tomorrow) {
                        if ($item['product_category_id'] == self::TP) {
                            $tempTP += $item['interest_amount'];
                            $totalFull += $item['interest_amount'];
                        } elseif ($item['product_category_id'] == self::TK) {
                            $tempTK += $item['interest_amount'];
                            $totalFull += $item['interest_amount'];
                        }
                    }
                }
                $result['chart_line'][] = [
                   'Ngày '. $tomorrowDM, $tempTP, $tempTK
                ];
            }
            //Số lượng hợp đồng của trái phiếu
            $params['product_category_id'] = self::TP;
            $contractTP = $this->mCustomerContractInterestByDate->getByContractQuantityNotTest($params);

            //Số lượng hợp đồng của tiết kiệm
            $params['product_category_id'] = self::TK;
            $contractTK = $this->mCustomerContractInterestByDate->getByContractQuantityNotTest($params);

            $result['chart_quantity'] = [
                [
                    'product_category' => __('Hợp tác đầu tư'),
                    'quantity' => $contractTP
                ],
                [
                    'product_category' => __('Tiết kiệm'),
                    'quantity' => $contractTK
                ]
            ];
            $result['totalInterest'] = number_format($totalFull,2);
            return $result;
        } else {
            //Nếu filter theo năm
            $data = $this->mCustomerContractInterestByMonth
                ->getByConditionNotTest($params);

            $totalFull = 0;
            for ($i = 1; $i <= 12; $i++) {
                //Số tiền của trái phiếu
                $tempTP = 0;
                //Số tiền của tiết kiệm
                $tempTK = 0;
                foreach ($data as $item) {
                    if ($item['interest_month'] == $i) {
                        if ($item['product_category_id'] == self::TP) {
                            $tempTP += $item['interest_amount'];
                            $totalFull += $item['interest_amount'];
                        } elseif ($item['product_category_id'] == self::TK) {
                            $tempTK += $item['interest_amount'];
                            $totalFull += $item['interest_amount'];
                        }
                    }
                }
                $result['chart_line'][] = [
                   'Tháng '. $i, $tempTP, $tempTK
                ];
            }
            //Số lượng hợp đồng của trái phiếu
            $params['product_category_id'] = self::TP;
            $contractTP = $this->mCustomerContractInterestByDate->getByContractQuantityNotTest($params);
            //Số lượng hợp đồng của tiết kiệm
            $params['product_category_id'] = self::TK;
            $contractTK = $this->mCustomerContractInterestByDate->getByContractQuantityNotTest($params);
            $result['chart_quantity'] = [
                [
                    'product_category' => __('Hợp tác đầu tư'),
                    'quantity' => $contractTP
                ],
                [
                    'product_category' => __('Tiết kiệm'),
                    'quantity' => $contractTK
                ]
            ];
            $result['totalInterest'] = number_format($totalFull,2);
            return $result;
        }
    }

    /**
     * Filter BÁO CÁO TRẢ LÃI DỰ KIẾN
     * @param array $params
     *
     * @return mixed
     */
//    public function filterExpectedInterestPayment($params = [])
//    {
//        $year = strip_tags($params['year']);
//        //Tháng hiện tại
//        $monthNow =  Carbon::now()->month;
//        if ($year != Carbon::now()->year) {
//            //Các tháng của năm
//            $arrayMonth = [];
//            for ($i = 1; $i <= 12; $i++) {
//                $arrayMonth[] = intval($i);
//            }
//            $params['array_month'] =  $arrayMonth;
//            //Dữ liệu từ db
//            $data = $this->mCustomerContactLog->getByCondition($params);
//            foreach ($arrayMonth as $key => $value) {
//                $tempTotal = 0;
//                foreach ($data as $item) {
//                    if ($value == $item['month'] && $year == $item['year']) {
//                        $tempTotal += $item['interest'];
//                    }
//                }
//                $result['chart'][] = ['month' => $value . '/' . $year, 'value' => $tempTotal];
//            }
//            return $result;
//        } else {
//            //Array tháng hiện tại của năm
//            $arrayMonth[] = $monthNow;
//            $params['array_month'] =  $arrayMonth;
//            $params['process_status'] =  self::PAYSUCCESS;
//
//            //Dữ liệu từ db
//            $data = $this->mCustomerContractInterestByDate->getByCondition($params)
//                ->toArray();
//            $result['chart'] = [];
//            foreach ($arrayMonth as $key => $value) {
//                $tempTotal = 0;
//                foreach ($data as $item) {
//                    if ($value == $item['interest_month'] && $year == $item['interest_year']) {
//                        $tempTotal += $item['interest_amount'];
//                    }
//                }
//                //Làm tròn số thập phân.
//                $tempTotal = round($tempTotal);
//                $result['chart'][] = ['month' => $value . '/' . $year, 'value' => $tempTotal];
//            }
//
//            //Các tháng còn lại của năm
//            $arrayMonth = [];
//            for ($i = $monthNow; $i <= 12; $i++) {
//                if ($i != $monthNow) {
//                    $arrayMonth[] = intval($i);
//                }
//            }
//            $params['array_month'] =  $arrayMonth;
//            //Dữ liệu từ db
//            $data = $this->mCustomerContactLog->getByCondition($params);
//            foreach ($arrayMonth as $key => $value) {
//                $tempTotal = 0;
//                foreach ($data as $item) {
//                    if ($value == $item['month'] && $year == $item['year']) {
//                        $tempTotal += $item['interest'];
//                    }
//                }
//                $result['chart'][] = ['month' => $value . '/' . $year, 'value' => $tempTotal];
//            }
//            return $result;
//        }
//    }

    public function filterExpectedInterestPayment($params = [])
    {
        $select_type = $params['select_type'];

        if ($select_type == 'day') {
            $day = $params['time-expected'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');
            //Dữ liệu từ db
            $filter = [
                'select_type' => $select_type,
                'startTime' => $startTime,
                'endTime' => $endTime
            ];
            $data = $this->mCustomerContactLog->getByConditionNotTest($filter);

            $result['chart'] = [];
            foreach ($data as $item) {
                if($item['total_interest_log'] != 0) {
                    $result['chart'][] = ['day' => Carbon::parse($item['full_time'])->format('d/m/Y'), 'value' => $item['total_interest_log']];
                }
            }
            return [
                'type' => 'day',
                'text' => 'Ngày',
                'value' => $result
            ];
        } elseif($select_type == 'week') {
            if ($params['week'] == 'week_now') {
                $week = Carbon::now()->weekOfYear;
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week' => $week,
                    'fulltime' => Carbon::now()->setISODate(Carbon::now()->year,$week),
                    'year' => Carbon::now()->year
                ];
            } elseif ($params['week'] == 'week_after') {
                $week = Carbon::now()->addWeek(1)->weekOfYear;
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week' => $week,
                    'year' => Carbon::now()->year
                ];
            } elseif ($params['week'] == 'month_after') {
                $week = Carbon::now()->addMonths(1);
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week_start' => $week->startOfMonth()->format('Y-m-d 00:00:00'),
                    'week_end' => $week->endOfMonth()->format('Y-m-d 23:59:59'),
                    'year' => $week->year,
                ];
            } elseif ($params['week'] == 'select_week') {
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week_before' => $params['week_before'],
                    'year_before' => $params['year_before'],
                    'week_after' => $params['week_after'],
                    'year_after' => $params['year_after'],
                ];
            }

            $data = $this->mCustomerContactLog->getByConditionNotTest($filter);

            $result['chart'] = [];
            foreach ($data as $item) {
                if($item['total_interest_log'] != 0) {
                    $result['chart'][] = ['week' => 'Tuần '. $item['week_year'], 'value' => $item['total_interest_log']];
                }
            }

            return [
                'type' => 'week',
                'text' => 'Tuần',
                'value' => $result
            ];
        } elseif ($select_type == 'month') {
            $month_start = isset($params['month_start']) ? $params['month_start'] : null;
            $month_end = isset($params['month_end']) ? $params['month_end'] : null;
            //Dữ liệu từ db
            $filter = [
                'select_type' => $select_type,
                'month_start' => $month_start == null ? null : Carbon::createFromFormat('m/Y',$month_start)->setTimezone('UTC')->startOfMonth()->format('Y/m/d 00:00:00'),
                'month_end' => $month_end == null ? null : Carbon::createFromFormat('m/Y',$month_end)->setTimezone('UTC')->endOfMonth()->format('Y/m/d 23:59:59'),
            ];
            $data = $this->mCustomerContactLog->getByConditionNotTest($filter);

            $result['chart'] = [];
            foreach ($data as $item) {
                if($item['total_interest_log'] != 0) {
                    $result['chart'][] = ['month' => 'Tháng '. Carbon::parse($item['full_time'])->format('m/Y'), 'value' => $item['total_interest_log']];
                }
            }

            return [
                'type' => 'month',
                'text' => 'Tháng',
                'value' => $result
            ];
        }

    }

    /**
     * Export danh sách lãi dự kiến
     * @return mixed|void
     */
    public function exportExpectedInterestPayment($params)
    {
        $select_type = $params['select_type'];

        if ($select_type == 'day') {
            $day = $params['time-expected'];
            $dateTime = explode(" - ", $day);
            $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');
            //Dữ liệu từ db
            $filter = [
                'select_type' => $select_type,
                'startTime' => $startTime,
                'endTime' => $endTime
            ];
            $data = $this->mCustomerContactLog->listByConditionNotTest($filter);
            $arr = [];
            foreach ($data as $key => $item) {
                $arr[] = [
                    $key+1,
                    $item['customer_contract_code'],
                    $item['customer_name'],
                    number_format((double)$item['total_amount'],3,".",","),
                    Carbon::parse($item['customer_contract_start_date'])->format('d/m/Y'),
                    $item['customer_contract_end_date'] != null ? Carbon::parse($item['customer_contract_end_date'])->format('d/m/Y') : null,
                    Carbon::parse($item['full_time'])->format('d/m/Y'),
                    number_format((double)$item['interest'],3,".",","),
                ];
            }

            $heading = [
                __('STT'),
                __('Mã hợp đồng'),
                __('Nhà đầu tư'),
                __('Tổng giá trị hợp đồng (VNĐ)'),
                __('Ngày bắt đầu hợp đồng'),
                __('Ngày kết thúc hợp đồng'),
                __('Ngày trả lãi dự kiến'),
                __('Tiền lãi (VNĐ)')
            ];
            if (ob_get_level() > 0) {
                ob_end_clean();
            }

            return Excel::download(new ExportFile($heading, $arr), 'Báo cáo lãi suất theo ngày.xlsx');

        } elseif($select_type == 'week') {
            if ($params['week'] == 'week_now') {
                $week = Carbon::now()->weekOfYear;
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week' => $week,
                    'fulltime' => Carbon::now()->setISODate(Carbon::now()->year,$week),
                    'year' => Carbon::now()->year
                ];
            } elseif ($params['week'] == 'week_after') {
                $week = Carbon::now()->addWeek(1)->weekOfYear;
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week' => $week,
                    'year' => Carbon::now()->year
                ];
            } elseif ($params['week'] == 'month_after') {
                $week = Carbon::now()->addMonths(1);
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week_start' => $week->startOfMonth()->format('Y-m-d 00:00:00'),
                    'week_end' => $week->endOfMonth()->format('Y-m-d 23:59:59'),
                    'year' => $week->year,
                ];
            } elseif ($params['week'] == 'select_week') {
                $filter = [
                    'select_type' => $select_type,
                    'type_week' => $params['week'],
                    'week_before' => $params['week_before'],
                    'year_before' => $params['year_before'],
                    'week_after' => $params['week_after'],
                    'year_after' => $params['year_after'],
                ];
            }

            $data = $this->mCustomerContactLog->listByConditionNotTest($filter);
            $arr = [];
            foreach ($data as $key => $item) {
                $arr[] = [
                    $key+1,
                    $item['customer_contract_code'],
                    $item['customer_name'],
                    number_format($item['total_amount'],3,".",","),
                    Carbon::parse($item['customer_contract_start_date'])->format('d/m/Y'),
                    $item['customer_contract_end_date'] != null ? Carbon::parse($item['customer_contract_end_date'])->format('d/m/Y') : null,
                    $item['week_year'],
                    number_format($item['interest'],3,".",",")
                ];
            }

            $heading = [
                __('STT'),
                __('Mã hợp đồng'),
                __('Nhà đầu tư'),
                __('Tổng giá trị hợp đồng (VNĐ)'),
                __('Ngày bắt đầu hợp đồng'),
                __('Ngày kết thúc hợp đồng'),
                __('Tuần / Năm trả lãi dự kiến'),
                __('Tiền lãi (VNĐ)')
            ];
            if (ob_get_level() > 0) {
                ob_end_clean();
            }

            return Excel::download(new ExportFile($heading, $arr), 'Báo cáo lãi suất theo tuần.xlsx');

        } elseif ($select_type == 'month') {
            $month_start = isset($params['month_start']) ? $params['month_start'] : null;
            $month_end = isset($params['month_end']) ? $params['month_end'] : null;
            //Dữ liệu từ db
            $filter = [
                'select_type' => $select_type,
                'month_start' => $month_start == null ? null : Carbon::createFromFormat('m/Y',$month_start)->setTimezone('UTC')->startOfMonth()->format('Y/m/d 00:00:00'),
                'month_end' => $month_end == null ? null : Carbon::createFromFormat('m/Y',$month_end)->setTimezone('UTC')->endOfMonth()->format('Y/m/d 23:59:59'),
            ];

            $data = $this->mCustomerContactLog->listByConditionNotTest($filter);
            $arr = [];

            foreach ($data as $key => $item) {
                $arr[] = [
                    $key+1,
                    $item['customer_contract_code'],
                    $item['customer_name'],
                    number_format($item['total_amount'],3,".",","),
                    Carbon::parse($item['customer_contract_start_date'])->format('d/m/Y'),
                    $item['customer_contract_end_date'] != null ? Carbon::parse($item['customer_contract_end_date'])->format('d/m/Y') : null,
                    $item['month'] .'/'. $item['year'],
                    number_format($item['interest'],3,".",","),
                ];
            }

            $heading = [
                __('STT'),
                __('Mã hợp đồng'),
                __('Nhà đầu tư'),
                __('Tổng giá trị hợp đồng (VNĐ)'),
                __('Ngày bắt đầu hợp đồng'),
                __('Ngày kết thúc hợp đồng'),
                __('Tháng / Năm trả lãi dự kiến'),
                __('Tiền lãi (VNĐ)')
            ];
            if (ob_get_level() > 0) {
                ob_end_clean();
            }

            return Excel::download(new ExportFile($heading, $arr), 'Báo cáo lãi suất theo tháng.xlsx');
        }
    }


    /**
     * Log customer contract
     * @param $id
     */
    public function updateReport()
    {
        //Model customer contract
        $mCustomerContact = new CustomerContactTable();
        //Model customer contract log
        $mCustomerContactLog = new CustomerContactLogTable();
        //Chi tiết của customer contract
        $customerContractList = $mCustomerContact->getAllContractUpdateLog();
        foreach ($customerContractList as $customerContract) {
            if ($customerContract['customer_contract_end_date'] != null) {
                //Số tháng đầu tư
                $investmentTime = $customerContract['investment_time'];
            } else {
                //Số tháng đầu tư
                $investmentTime = 60;
                $customerContract['customer_contract_end_date'] = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])->addMonth($investmentTime);
            }

            //Kỳ hạn rút lãi -> là số tháng
            $withdrawInterestTime = $customerContract['withdraw_interest_time'];
            //Số record
            $record = (int)($investmentTime / $withdrawInterestTime);
            //Số tháng dư
            $residual = $investmentTime % $withdrawInterestTime;
            //Năm bắt đầu hợp đồng
            $year = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
                ->format('Y');
            //Tháng bắt đầu hợp đồng
            $month = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
                ->format('m');
            //Năm kết thúc hợp đồng
            $yearEnd = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
                ->format('Y');
            //Tháng bắt đầu hợp đồng
            $monthEnd = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
                ->format('m');
            $dayEnd = $startTime = Carbon::createFromFormat(
                'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
                ->format('d');
            $tmpD = Carbon::parse($yearEnd.'-'.$monthEnd)->endOfMonth()->format('d');
            if ($dayEnd > 30) {
                $dayEnd = $tmpD;
            }
            $weekEnd = Carbon::parse($yearEnd.'-'.$monthEnd.'-'.$dayEnd)->weekOfYear;

            $fulltime = $startTime = Carbon::createFromFormat(
                'Y-m-d', $yearEnd.'-'.$monthEnd.'-'.$dayEnd)
                ->format('Y-m-d');
            $data = [];
            if ($record > 0) {
                $mT = 0;
                $interest = $withdrawInterestTime * $customerContract['month_interest'];
                for ($i = 0; $i < $record; $i++) {
                    //Tháng lãi tiếp theo
                    $mT += $withdrawInterestTime;
                    $dt = Carbon::create($year, $month, 1, 0);
                    //Cộng thêm tháng
                    $addMonth = $dt->addMonths($mT);
                    $y = $startTime = Carbon::createFromFormat(
                        'Y-m-d H:i:s', $addMonth)
                        ->format('Y');
                    $m = (int)$startTime = Carbon::createFromFormat(
                        'Y-m-d H:i:s', $addMonth)
                        ->format('m');
                    $d = $dayEnd;
//                    $d = (int)$startTime = Carbon::createFromFormat(
//                        'Y-m-d H:i:s', $addMonth)
//                        ->format('d');
                    $tmpDay = Carbon::parse($y.'-'.$m)->endOfMonth()->format('d');
                    if ($dayEnd > 30) {
                        $d = $tmpDay;
                    }
                    $w = Carbon::parse($y.'-'.$m.'-'.$d)->weekOfYear;
                    $full = $startTime = Carbon::createFromFormat(
                        'Y-m-d', $y.'-'.$m.'-'.$d)
                        ->format('Y-m-d');
                    if ($m == 2 && $d > 28) {
                        $d = Carbon::parse($y.'-'.$m)->endOfMonth()->format('d');
                        $w = Carbon::parse($y.'-'.$m.'-'.$d)->weekOfYear;
                        $full = $startTime = Carbon::createFromFormat(
                            'Y-m-d', $y.'-'.$m.'-'.$d)
                            ->format('Y-m-d');
                    }

                    $wY = $w.'/'.$y;
                    if ($w == 53 && $m == 1) {
                        $wY = $w.'/'.($y-1);
                    }

                    $data[] = [
                        'customer_contract_id' => $customerContract['customer_contract_id'],
                        'customer_id' => $customerContract['customer_id'],
                        'year' => $y,
                        'month' => $m,
                        'day' => $d,
                        'week' => $w,
                        'week_year' => $wY,
                        'full_time' => $full,
                        'interest' => $interest
                    ];
                }

            }
            //Lần cuối tính lãi
            if ($residual > 0) {
                $dEND = $dayEnd;
                if ($weekEnd == 2 && $dEND > 28) {
                    $dEND = Carbon::parse($yearEnd.'-'.$weekEnd)->endOfMonth()->format('d');
                    $weekEnd = Carbon::parse($yearEnd.'-'.$monthEnd.'-'.$dEND)->weekOfYear;
                    $fulltime = $startTime = Carbon::createFromFormat(
                        'Y-m-d', $yearEnd.'-'.$monthEnd.'-'.$dEND)
                        ->format('Y-m-d');
                }

                $wYear = $weekEnd.'/'.$yearEnd;
                if ($weekEnd == 53 && $monthEnd == 1) {
                    $wYear = $weekEnd.'/'.($yearEnd-1);
                }

                $data[] = [
                    'customer_contract_id' => $customerContract['customer_contract_id'],
                    'customer_id' => $customerContract['customer_id'],
                    'year' => $yearEnd,
                    'month' => $monthEnd,
                    'day' => $dEND,
                    'week' => $weekEnd,
                    'week_year' => $wYear,
                    'full_time' => $fulltime,
                    'interest' => $residual * $customerContract['month_interest']
                ];
            }
            if ($data != []) {

                foreach ($data as $dataFull) {
                    $idContractLog = $mCustomerContactLog->checkContractLog($dataFull);
                    if ($idContractLog != null) {
                        $mCustomerContactLog->updateContractLog($dataFull,$idContractLog['customer_contract_log_id']);
                    } else {
                        $mCustomerContactLog->addInsert($dataFull);
                    }
                }

            }
        }
    }

    public function getListSource()
    {
        return $this->referSource->getListAll();

    }

    public function rawChart($input)
    {
        $arr_filter = explode(" - ",$input['time']);
        $start  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
        $end  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
        $input['start'] = $start;
        $input['end'] = $end;
        $listCustomer = $this->customer->getListCustomerChartNew($input);

        if($input['source'] !== 'all'){
            $source = '';
            if($input['source'] == 'na'){
                $source = 'Khác';
            } elseif ($input['source'] == 'default'){
                $source = 'Link Copy';
            } elseif($input['source'] == 'facebook'){
                $source = 'Facebook';
            }elseif($input['source'] == 'zalo'){
                $source = 'Zalo';
            }elseif($input['source'] == 'twitter'){
                $source = 'Twitter';
            }elseif($input['source'] == 'wechat'){
                $source = 'Wechat';
            }
            $arrDataChart[] = ['day', $source];
        } else {
            $arrDataChart[] = ['day', 'Tất cả', 'Khác', 'Link Copy', 'Facebook','Zalo','Twitter','Wechat'];
        }

        foreach ($listCustomer as $list){
            if($input['source'] !== 'all'){
                $arrDataChart[] = [$list['date'], $list[$input['source']]];
            } else {
                $arrDataChart[] = collect($list)->values()->toArray();
            }
        }

        return [
            'error' => false,
            'chart' => $arrDataChart,
        ];

//        $listCustomerNull = $this->customer->getListCustomerChartNull($input);
//        if ($input['source'] != 'n/a' ) {
//            if (count($listCustomer) == 0) {
//                return [
//                    'error' => true
//                ];
//            }
//        } else {
//            if (count($listCustomerNull) == 0) {
//                return [
//                    'error' => true
//                ];
//            }
//        }
//        if (count($listCustomer) == 0) {
//            return [
//                'error' => true
//            ];
//        }

//        if ($input['source'] == 'all'){
//            $tmp = ['day','Tất cả','N/A'];
//        }else if ($input['source'] == 'n/a') {
//            $tmp = ['day','N/A'];
//        }else {
//            $tmp = ['day'];
//        }
//        if ($input['source'] == 'all'){
//            $tmp = ['day','Tất cả','Khác'];
//        }else if ($input['source'] == 'na') {
//            $tmp = ['day','Khác'];
//        } else {
//            $tmp = ['day'];
//        }
//        $day = [];
//        $chart = [];
//        $listSource = $this->referSource->getListAllByKey($input['source']);
//        $listSource = collect($listSource)->keyBy('source');
//        foreach ($listSource as $item) {
//            $tmp[] = $item['source_name'];
//        }
//        $chart[] = $tmp;
//        $totalValue = 0;

//        $groupCustomerNull = collect($listCustomerNull)->groupBy('date');
//        foreach (collect($listCustomer)->groupBy('date') as $keyList => $item) {
//            $groupValue = collect($item)->groupBy('utm_source');
//            $day[] = $keyList;
//            if ($input['source'] == 'all'){
//                $day[] = count($item);
//                if (count($item) > $totalValue) {
//                    $totalValue = count($item);
//                }
//
//                $temp = [];
//                if (isset($groupValue[''])) {
//                    $temp = collect($groupValue[''])->groupBy('is_referal')->toArray();
//                }
//                $day[] = isset($temp['']) ? count($temp['']) : 0;
//            }
//
//            if ($input['source'] == 'na'){
//                $temp = [];
//                if (isset($groupValue[''])) {
//                    $temp = collect($groupValue[''])->groupBy('is_referal')->toArray();
//                }
//                $day[] = isset($temp['']) ? count($temp['']) : 0;
//            }
//
//            foreach ($listSource as $key => $itemSource) {
//                if ($key == 'default' && $input['source'] != 'na'){
//                    $tmpDefault = collect($groupValue[''])->groupBy('is_referal')->toArray();
//                    $day[] = isset($tmpDefault[1]) ? count($tmpDefault[1]) : 0;
//                } else if ($key != 'all' && $input['source'] != 'na' && $key != 'default') {
//                    $day[] = isset($groupValue[$key]) ? count($groupValue[$key]) : 0;
//                }
//            }
//
//            $chart[] = $day;
//            $day = [];
//        }
//        dd($chart);
//        return [
//            'error' => false,
//            'chart' => $chart,
//            'totalValue' => $totalValue
//        ];
    }

    public function getListStaff()
    {
        return $this->staff->getAll();
    }

    public function salesFilter($input)
    {
        $listContract = $this->customerContract->getAllChart($input);
        return [
            'error' => false,
            'amcharts' => $listContract,
        ];
        if (count($listContract) == 0) {
            return [
                'error' => true
            ];
        }
        $chart[] = ['Month', 'Doanh thu'];
        $listContract = collect($listContract)->groupBy('date');
        $amcharts = [];
        foreach ($listContract as $key => $item) {
            $tmp = [];
            $tmp[] = $key;
            $total = 0;
            foreach ($item as $itemContract) {
                $total += $itemContract['total_amount'];
            }
            $amcharts[] = [
                'month' => $key,
                'order' => $total,
            ];
            $tmp[] = $total;
            $chart[] = $tmp;
        }
//        dd($amcharts);
        return [
            'error' => false,
            'chart' => $chart,
            'amcharts' => $amcharts,
        ];
    }

    public function sumSalesFilter($input)
    {
        $display = 10;
        $input['start'] = Carbon::createFromFormat('m/Y',$input['month_start_sale'])->startOfMonth()->format('Y-m-d');
        $input['end'] = Carbon::createFromFormat('m/Y',$input['month_end_sale'])->lastOfMonth()->format('Y-m-d');
        $getListSumSales = $this->customerContract->getListSumSale($input['start'],$input['end']);
        $getSumSales = $this->customerContract->getSumSale($input['start'],$input['end']);
        $getListSumSalesPage = $this->customerContract->getListSumSale($input['start'],$input['end'],$input['page'],$display);
        if (count($getListSumSales) != 0) {
            $getListSumSales = collect($getListSumSales)->keyBy('name');
        }
        if (isset($getListSumSales[''])){
            $getListSumSales['']['name'] = 'Khác';
        }
        $getListSumSales = array_values(collect($getListSumSales)->toArray());
        $view = view('admin::report.sale.table-sale',[
            'listSale' => $getListSumSalesPage,
            'page' => $input['page'],
            'display' => $display
        ])->render();
        return [
            'error' => false,
            'chart' => $getListSumSales,
            'view' => $view,
            'total' => isset($getSumSales['money']) ? number_format($getSumSales['money'],3) : number_format(0,3)
        ];
    }

    public function sumGroupFilter($input)
    {
        $display = 10;
        $input['start'] = Carbon::createFromFormat('m/Y',$input['month_start_group'])->startOfMonth()->format('Y-m-d');
        $input['end'] = Carbon::createFromFormat('m/Y',$input['month_end_group'])->lastOfMonth()->format('Y-m-d');
        $getListSumSales = $this->customerContract->getListSumSaleGroup($input['start'],$input['end']);
        $getSumSales = $this->customerContract->getSumSaleGroup($input['start'],$input['end']);
        $getListSumSalesPage = $this->customerContract->getListSumSaleGroup($input['start'],$input['end'],$input['page'],$display);
        if (count($getListSumSales) != 0) {
            $getListSumSales = collect($getListSumSales)->keyBy('name');
        }
        if (isset($getListSumSales[''])){
            $getListSumSales['']['name'] = 'Khác';
        }
        $getListSumSales = array_values(collect($getListSumSales)->toArray());
        $view = view('admin::report.sale.table-group',[
            'listSale' => $getListSumSalesPage,
            'page' => $input['page'],
            'display' => $display
        ])->render();
        return [
            'error' => false,
            'chart' => $getListSumSales,
            'view' => $view,
            'total' => isset($getSumSales['money']) ? number_format($getSumSales['money'],3) : number_format(0,3)
        ];
    }
}