<?php
namespace Modules\Admin\Repositories\Report;

interface ReportRepositoryInterface
{
    /**
     * Filter BÁO CÁO DOANH THU - THANH TOÁN
     * @param array $params
     *
     * @return mixed
     */
    public function filterRevenue($params = []);

    /**
     * Filter BÁO CÁO LÃI SUẤT
     * @param array $params
     *
     * @return mixed
     */
    public function filterInterest($params = []);

    /**
     * Filter BÁO CÁO TRẢ LÃI DỰ KIẾN
     * @param array $params
     *
     * @return mixed
     */
    public function filterExpectedInterestPayment($params = []);

    /**
     * Export danh sách lãi dự kiến
     * @return mixed
     */
    public function exportExpectedInterestPayment($params);

    /**
     * Cập nhật lại danh sách log lãi dự kiến
     * @return mixed
     */
    public function updateReport();

    /**
     * Lấy danh sách nguồn đăng ký
     * @return mixed
     */
    public function getListSource();

    /**
     * lấy dữ liệu vẽ chart
     * @param $input
     * @return mixed
     */
    public function rawChart($input);

    /**
     * Lấy danh sách nhân viên
     * @return mixed
     */
    public function getListStaff();

    /**
     * Lấy doanh số bán hàng của nhân viên sales
     * @return mixed
     */
    public function salesFilter($input);

    /**
     * Lấy tổng doanh thu của từng nhân viên
     * @param $input
     * @return mixed
     */
    public function sumSalesFilter($input);

    /**
     * Lấy tổng doanh thu của từng nhóm nhân viên
     * @param $input
     * @return mixed
     */
    public function sumGroupFilter($input);
}