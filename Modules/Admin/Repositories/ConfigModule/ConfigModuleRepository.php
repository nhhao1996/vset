<?php
/**
 * Created by PhpStorm.
 * User: SonVeratti
 * Date: 3/17/2018
 * Time: 1:17 PM
 */

namespace Modules\Admin\Repositories\ConfigModule;

use Modules\Admin\Models\ActionTable;
use Modules\Admin\Models\ConfigModuleTable;
use Modules\Admin\Repositories\ConfigModule\ConfigModuleRepositoryInterface;

class ConfigModuleRepository implements ConfigModuleRepositoryInterface
{
    protected $configModule;
    public function __construct(ConfigModuleTable $configModule)
    {
        $this->configModule = $configModule;
    }

    public function getList()
    {
        return $this->configModule->getList();
    }

    public function updateActive($input)
    {
        $id = $input['config_module_id'];
        $active = $input['is_active'];
        $update = $this->configModule->updateActive($id,$active);
        return [
            'error' => false,
            'message' => 'Thay đổi cấu hình hiển thị module thành công'
        ];
    }


}