<?php

namespace Modules\Admin\Repositories\ConfigModule;

interface ConfigModuleRepositoryInterface
{
    public function getList();

    /**
     * Cập nhật trạng thái
     * @param $input
     * @return mixed
     */
    public function updateActive($input);
}