<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 4/1/2019
 * Time: 12:00 PM
 */

namespace Modules\Admin\Repositories\LogNoti;


interface LogNotiRepositoryInterface
{
    public function list(array $filters = []);

    public function getDetail($id);
}