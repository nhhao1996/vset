<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 4/1/2019
 * Time: 12:00 PM
 */

namespace Modules\Admin\Repositories\LogNoti;


use Modules\Notification\Models\NotificationTable;

class LogNotiRepository implements LogNotiRepositoryInterface
{
    protected $noti;

    public function __construct(NotificationTable $noti)
    {
        $this->noti = $noti;
    }

    public function list(array $filters = [])
    {
        return $this->noti->getList($filters);
    }

    public function getDetail($id)
    {
        return $this->noti->getDetail($id);
    }
}