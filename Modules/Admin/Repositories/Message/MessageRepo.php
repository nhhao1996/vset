<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 07-04-02020
 * Time: 2:45 PM
 */

namespace Modules\Admin\Repositories\Message;


use Illuminate\Support\Facades\Auth;
use Modules\Admin\Models\CustomerAppointmentTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\messageAutoConfigTable;
use Modules\Admin\Models\messageDetailTable;
use Modules\Admin\Models\messageLogTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\ServiceCard;
use Modules\Admin\Models\ServiceTable;
use Modules\Admin\Models\SmsLogTable;
use Modules\Admin\Models\SmsMessageTable;
use Modules\Admin\Models\StockOrderTable;
use Modules\Admin\Models\StockTransactionTable;
use Modules\Admin\Models\StockTransferContractTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepository;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;

class MessageRepo implements MessageRepoInterface
{
    public function sendSMS($key, $data, $input) {
   try{
       $customerContractInterestRepo = app()->get(CustomerContractInterestRepositoryInterface::class);
//       $data['message'] = $this->replaceContent($key,$input);
       $dataMessage =  $this->replaceContent($key,$input);
       if (isset($data['phone']) && $data['phone'] != null) {
           $sms = new SmsLogTable();
//           $data['message'] = $this->replaceContent($key, $input);
//           todo: gọi đến send message của customer contract repo-----------
           $dataSMS = [
               "phone"=> $data['phone'],
               'message'=> $dataMessage['message'],
               'customer_name'=>$dataMessage['customer_name']
           ];
           return $customerContractInterestRepo->sendSMS($dataSMS);
//            end: ---------------------------------------------------------


           $test = $sms->add($data);           
           $url = 'https://cloudsms.vietguys.biz:4438/api/index.php';
           $param = [
               'u' => 'vsetgroup',
               'pwd' => '218rs',
               'phone' => $data['phone'],
               'sms' => $data['message'],
               'from' => 'VSETGROUP',
               'json' => '1',
           ];
           $oURL = curl_init();
           curl_setopt($oURL, CURLOPT_URL, $url);
//            curl_setopt($oURL, CURLOPT_HEADER, TRUE);
           curl_setopt($oURL, CURLOPT_POST, FALSE);
           curl_setopt($oURL, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
           curl_setopt($oURL, CURLOPT_RETURNTRANSFER, true);
           curl_setopt($oURL, CURLOPT_POSTFIELDS, json_encode($param));
           curl_exec($oURL);
//            $response = curl_getinfo($oURL, CURLINFO_HTTP_CODE);
           curl_close($oURL);
       }

   }catch(\Exception $ex){
       dd($ex->getMessage());

   }
    }
   public function replaceContent($key, $input){
        $mStockOrder = new StockOrderTable();
       $mSmsMessage = new SmsMessageTable();
       $mWithdrawGroup = new WithdrawRequestGroupTable();
       $mStockTransfer = new StockTransferContractTable();
       $message = $mSmsMessage->getMessageByKey($key);
       $mCustomer = new CustomerTable();
       $id = "";
       $fullName = isset($input['full_name'])?$input['full_name']:"";
       $money = "";

       if(isset($input['stock_order_id'])){
           $stockOrder = $mStockOrder->getDetailTransfer($input['stock_order_id']);
           if(isset($input['full_name'])){
               $fullName = $input['full_name'];
           }else{
               $customer = $mCustomer->getDetail($stockOrder['customer_id']);
               $fullName = $customer->full_name;
           }
           $id = $stockOrder->stock_order_code;
       }
       if (isset($input['withdraw_request_group_id'])){
           $withdrawGroupDetail = $mWithdrawGroup->getById($input['withdraw_request_group_id']);
           $id = $withdrawGroupDetail->withdraw_request_group_code;
           $money = $withdrawGroupDetail->withdraw_request_amount;
           $fullName = $withdrawGroupDetail->customer_name;

       }
       if(isset($input['stock_tranfer_contract_id'])){
           $stockTransfer = $mStockTransfer->getDetail($input['stock_tranfer_contract_id']);
           $id = $stockTransfer->stock_tranfer_contract_code;
           $fullName = $input['full_name'];

       }
//       dd($id, $fullName);

       $result = str_replace(
           [
               '[id]',
               '[full_name]',
               '[money]'

           ],
           [
               $id,
               $fullName,
               $money
           ], $message);
//       dd($message);
       return ['message'=>$result, "customer_name"=>$fullName];

   }

}