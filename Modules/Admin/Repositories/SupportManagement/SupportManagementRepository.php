<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\SupportManagement;

use Modules\Admin\Models\FaqTable;
use Modules\Admin\Models\FaqGroupTable;
use Modules\Admin\Repositories\SupportManagement\SupportManagementRepositoryInterface;
use Modules\Admin\Repositories\FaqGroup\FaqGroupRepositoryInterface;

class SupportManagementRepository implements SupportManagementRepositoryInterface
{

    protected $supportManagent;
    protected $FaqGroupTable;
    public function __construct(
        FaqTable $supportManagent,
        FaqGroupTable  $faqGroupTable

    )
    {
        $this->supportManagent = $supportManagent;
        $this->FaqGroupTable = $faqGroupTable;
    }

    //Hàm lấy danh sách
    public function list(array $filters = [])
    {
        return $this->supportManagent->getList($filters);
    }
    public function listGroup(array $filters = [])
    {
        $data = $this->FaqGroupTable->getList($filters);
        return $data;
    }
    public function show($id){
        return $this ->supportManagent->getById($id);
    }
}