<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\SupportManagement;


interface SupportManagementRepositoryInterface
{
    public function list(array $filters = []);
    public function listGroup(array $filters = []);
    public function show($id);
}