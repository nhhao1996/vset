<?php
/**
 * Created by PhpStorm.
 * User: SonVeratti
 * Date: 3/17/2018
 * Time: 1:17 PM
 */

namespace Modules\Admin\Repositories\AccountStatement;


use Modules\Admin\Models\AccountStatementMapContractTable;
use Modules\Admin\Models\AccountStatementTable;

class AccountStatementRepository implements AccountStatementRepositoryInterface
{
    protected $accountStatement;
    protected $accountStatementMap;
    public function __construct(AccountStatementTable $accountStatement,AccountStatementMapContractTable $accountStatementMap)
    {
        $this->accountStatement = $accountStatement;
        $this->accountStatementMap = $accountStatementMap;
    }

    public function list(array $filters = [])
    {
        $list = $this->accountStatement->getList($filters);
        return $list;
    }

    public function getDetail($id)
    {
        return $this->accountStatementMap->getDetail($id);
    }

}