<?php

namespace Modules\Admin\Repositories\AccountStatement;

interface AccountStatementRepositoryInterface
{
    /**
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = []);

    /**
     * Lấy chi tiết thông tin sao kê
     * @param $id
     * @return mixed
     */
    public function getDetail($id);
}