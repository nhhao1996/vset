<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/19/2021
 * Time: 8:48 AM
 * @author nhandt
 */

namespace Modules\Admin\Repositories\StatisticsTransaction;


interface ReportStatisticsTransactionRepoInterface
{
    public function filter($params = []);
}