<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/19/2021
 * Time: 8:49 AM
 * @author nhandt
 */


namespace Modules\Admin\Repositories\StatisticsTransaction;


use Carbon\Carbon;
use Modules\Admin\Models\PaymentMethodTable;
use Modules\Admin\Models\StockOrderTable;
use Modules\Admin\Models\StockTransferContractTable;

class ReportStatisticsTransactionRepo implements ReportStatisticsTransactionRepoInterface
{
    private $paymentMethod;
    private $stockOrder;
    private $stockTransferContract;

    const arrSource = [
        '1' => 'publisher',
        '2' => 'market',
        '3' => 'transfer',
        '4' => 'convert',
    ];

    const arrSourceName = [
        'publisher' => 'Nhà phát hành',
        'market' => 'Mua - bán ở chợ',
        'transfer' => 'Chuyển nhượng',
        'convert' => 'Chuyển đổi',
    ];
    public function __construct(PaymentMethodTable $paymentMethod,
                                StockOrderTable $stockOrder,
                                StockTransferContractTable $stockTransferContract)
    {
        $this->stockTransferContract=$stockTransferContract;
        $this->paymentMethod=$paymentMethod;
        $this->stockOrder = $stockOrder;
    }

    /**
     * Toàn bộ dữ liệu của thống kê
     *
     * @param array $params
     * @return array
     */
    public function filter($params = [])
    {
         $filter_type = $params['filter_type'];
         $time = $params['time'];
         if($filter_type == 'day'){
             $dateTime = explode(" - ", $time);
             $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])
                 ->format('Y-m-d');
             $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])
                 ->format('Y-m-d');
             $params['created_at'] = [$startTime . ' 00:00:00', $endTime.' 23:59:59'];

             $filter = [
                 'filter_type' => $filter_type,
                 'startTime' => $startTime,
                 'endTime' => $endTime
             ];
             $data = $this->stockOrder->getDataStatisticTransactionChart($filter);
             $dataTemp = $this->stockTransferContract->statisticsTransactionConvert($filter);
             if($dataTemp != null){
                 foreach ($dataTemp as $item) {
                     $dataConvert = array('source' => $item['source'],
                         'amount_transaction' => $item['amount_transaction'],
                         'date' => $item['date']);
                     array_push($data,$dataConvert);
                 }
             }
             $seriesData = [];
             $cateDate = [];
             $this->formatDataColumnChartByDay($data,$endTime,$startTime,$seriesData,$cateDate);
             return [
                 'cate' => $cateDate,
                 'chart' => $seriesData,
             ];
         }
         else{
             $year = isset($params['year']) ? $params['year'] : null;
             $startYear = '01/01/'.$year;
             $endYear = '31/12/'.$year;
             //Dữ liệu từ db
             $filter = [
                 'filter_type' => $filter_type,
                 'startTime' => $year == null ? null : Carbon::createFromFormat('d/m/Y',$startYear)->format('Y-m-d'),
                 'endTime' => $year == null ? null : Carbon::createFromFormat('d/m/Y',$endYear)->format('Y-m-d'),
             ];
             $dataYear = $this->stockOrder->getDataStatisticTransactionChart($filter);

             $dataTemp = $this->stockTransferContract->statisticsTransactionConvert($filter);
             if($dataTemp != null){
                 foreach ($dataTemp as $item) {
                     $dataConvert = array('source' => $item['source'],
                         'amount_transaction' => $item['amount_transaction'],
                         'date' => $item['date']);
                     array_push($dataYear,$dataConvert);
                 }
             }
             $seriesData = [];
             $cateDate = [];
             $this->formatDataColumnChartByYear($dataYear,$year,$seriesData,$cateDate);
             return [
                 'cate' => $cateDate,
                 'chart' => $seriesData,
             ];
         }
    }

    /**
     * Format lại data trả về theo dạng của biểu đồ
     *
     * @param $data
     * @param $year
     * @param $seriesData
     * @param $cateDate
     */
    protected function formatDataColumnChartByYear($data, $year, &$seriesData, &$cateDate){
        for($j = 1; $j < 5; $j++){
            $labelData = [];
            $labelName1 = "";
            $checkAdded = 0;
            for ($i = 1; $i <= 12; $i++) {
                $checkAdded = 0;
                if(!in_array('Tháng '.$i,$cateDate)){
                    array_push($cateDate,'Tháng '.$i);
                }
                foreach ($data as $item) {
                    if($item['source'] == self::arrSource[$j]){
                        $checkAdded = 2;
                        if($item['date'] == $i.'/'.$year){
                            $labelName1 = self::arrSourceName[$item['source']];
                            array_push($labelData,$item['amount_transaction']);
                            $checkAdded = 1;
                            break;
                        }
                    }
                }
                if($checkAdded == 2){
                    $labelName1 = self::arrSourceName[self::arrSource[$j]];
                    array_push($labelData,0);
                }
            }
            if($checkAdded != 0){
                $seriesData[] = array('name'=>$labelName1,'data'=>$labelData);
            }
        }
    }

    /**
     * Format lại data trả về theo dạng của biểu đồ
     *
     * @param $data
     * @param $endTime
     * @param $startTime
     * @param $seriesData
     * @param $cateDate
     */
    protected function formatDataColumnChartByDay($data, $endTime, $startTime, &$seriesData, &$cateDate){
        $dateDiff = ((strtotime($endTime) - strtotime($startTime)) / (60 * 60 * 24)) + 1;
        for($j = 1; $j < 5; $j++){
            $labelData = [];
            $labelName1 = "";
            $checkAdded = 0;
            for ($i = 0; $i < $dateDiff; $i++) {
                $checkAdded = 0;
                $tomorrow = date('d/m/Y', strtotime($startTime . "+" . $i . " days"));
                if(!in_array($tomorrow,$cateDate)){
                    array_push($cateDate,$tomorrow);
                }
                foreach ($data as $item) {
                    if($item['source'] == self::arrSource[$j]){
                        $checkAdded = 2;
                        if($item['date'] == $tomorrow){
                            $labelName1 = self::arrSourceName[$item['source']];
                            array_push($labelData,$item['amount_transaction']);
                            $checkAdded = 1;
                            break;
                        }
                    }
                }
                if($checkAdded == 2){
                    $labelName1 = self::arrSourceName[self::arrSource[$j]];
                    array_push($labelData,0);
                }
            }
            if($checkAdded != 0){
                $seriesData[] = array('name'=>$labelName1,'data'=>$labelData);
            }
        }
    }
}