<?php


namespace Modules\Admin\Repositories\ConfigTermInvestment;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Libs\SmsFpt\TechAPI\src\TechAPI\Exception;
use Modules\Admin\Models\InvestmentTimeTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\ProductInterestTable;
use Modules\Admin\Models\ProductTable;
use Modules\Admin\Models\WithdrawInterestTimeTable;
use Modules\Admin\Repositories\Product\ProductRepository;

class ConfigTermInvestmentRepository implements ConfigTermInvestmentRepositoryInterface
{
    protected $investmentTime;
    protected $productRepo;

    public function __construct(InvestmentTimeTable $investmentTime,ProductRepository $productRepo)
    {
        $this->investmentTime = $investmentTime;
        $this->productRepo = $productRepo;
    }

    public function list(array $filters = [])
    {
        return $this->investmentTime->getList($filters);
    }

//    Hiển thị popup cấu hình kì hạn đầu tư
    public function showPopup($param)
    {
        $productCategories = $this->getCategoryProduct();
        $getDetail = null;
        if (isset($param['id'])) {
            $getDetail = $this->investmentTime->getDetail($param['id']);
        }
        $view = view(
            'admin::config-term-investment.popup.popup',
            [
                'category' => $productCategories,
                'getDetail' => $getDetail
            ]
        )->render();

        return [
            'error' => false,
            'view'       => $view,
            'investment_time_month' => $getDetail == null ? 0 : $getDetail['investment_time_month']
        ];
    }

    public function getCategoryProduct()
    {
        return $this->productRepo->getCategoryProduct();
    }

//    Tạo kì hạn đầu tư
    public function store($param)
    {
        try {
            DB::beginTransaction();
//        if ($param['type_investment'] == 'month') {
//            $checkUnique = $this->investmentTime->checkUnique($param['product_category_id'],$param['investment_time_month']);
//        } else {
//            $checkUnique = $this->investmentTime->checkUnique($param['product_category_id'],$param['investment_time_year']);
//        }
//            $month = $param['type_investment'] == 'month' ? $param['investment_time_month'] : $param['investment_time_year'];
            $month = $param['investment_time_month'];

            $checkUnique = $this->investmentTime->checkUnique($param['product_category_id'],$month);
            if ($checkUnique != null) {
                return [
                    'error' => true,
                    'message' => 'Cấu hình kì hạn đầu tư bị trùng'
                ];
            }

            $data = [
                'product_category_id' => $param['product_category_id'],
                'investment_time_month' => $month,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
                'is_deleted' => 0,
                'is_actived' => 1
            ];

            $id = $this->investmentTime->add($data);

            //        function tạo cấu hình lãi từng gói
            $addConfig = $this->configInterestProduct($param['product_category_id'],$id,$month);
            if ($addConfig == true) {
                DB::commit();
            } else {
                DB::rollBack();
            }

            return [
                'error' => false,
                'message' => 'Tạo kì hạn đầu tư thành công'
            ];
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    public function configInterestProduct($product_category_id,$investment_time_id,$month){
        try {
            $mProduct = new ProductTable();
            $mWithrawInterest = new WithdrawInterestTimeTable();
            $mProductInterest = new ProductInterestTable();
//        danh sách sản phẩm theo loại
            $getProductByCategory = $mProduct->getListAllByCategory($product_category_id);
//        danh sách kì hạn rút lãi
            $getlistWithrawInterest = $mWithrawInterest->getAll();
            $data = [];
            foreach ($getProductByCategory as $item) {
                $product_id = $item['product_id'];
                $term_time_type = 1;
                $commission_rate = 3;
                $price_standard = $item['price_standard'];
                $interest_rate_standard = $item['interest_rate_standard'];
                foreach ($getlistWithrawInterest as $itemWithraw) {
                    $monthInterest = ($interest_rate_standard / (100*12)) * $price_standard;
                    $data[] = [
                        'product_id' => $product_id,
                        'term_time_type' => $term_time_type,
                        'investment_time_id' => $investment_time_id,
                        'withdraw_interest_time_id' => $itemWithraw['withdraw_interest_time_id'],
                        'interest_rate' => $interest_rate_standard,
                        'commission_rate' => $commission_rate,
                        'month_interest' => $monthInterest,
                        'total_interest' => $monthInterest*$month,
                        'is_deleted' => 0,
                        'is_actived' => 1,
                        'is_default_display' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by' => 1,
                        'updated_by' => 1,
                    ];
                }
            }
            $test = $mProductInterest->createProductInterest($data);

            return true;

        } catch (Exception $e) {
            return false;
        }

    }

//    Cập nhật cấu hình kì hạn đầu tư
    public function update($param)
    {
        if ($param['type_investment'] == 'month') {
            $checkUnique = $this->investmentTime->checkUnique($param['product_category_id'],$param['investment_time_month'],$param['investment_time_id']);
        } else {
            $checkUnique = $this->investmentTime->checkUnique($param['product_category_id'],$param['investment_time_year'],$param['investment_time_id']);
        }

        if ($checkUnique != null) {
            return [
                'error' => true,
                'message' => 'Cấu hình kì hạn đầu tư bị trùng'
            ];
        }

        $data = [
            'product_category_id' => $param['product_category_id'],
            'investment_time_month' => $param['type_investment'] == 'month' ? $param['investment_time_month'] : $param['investment_time_year'],
            'updated_at' => Carbon::now(),
            'updated_by' => Auth::id(),
        ];

        $this->investmentTime->updatedInvestment($data,$param['investment_time_id']);
        return [
            'error' => false,
            'message' => 'Cập nhật kì hạn đầu tư thành công'
        ];
    }

    public function delete($id)
    {
        try {
            $this->investmentTime->deleteInvestment($id);
            return [
                'error' => false,
                'message' => 'Xóa cấu hình kì hạn đầu tư thành công'
            ];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => 'Xóa cấu hình kì hạn đầu tư thất bại'
            ];
        }
    }

    public function changStatus($param)
    {
        try {
//            Check đơn hàng chưa xác nhận
            $mOrder = new OrderTable();
            $filter = ['new','confirmed'];
            $checkOrder = $mOrder->getOrderByFilter($filter);
//            if (count($checkOrder) != 0) {
//                return [
//                    'error' => true,
//                    'message' => 'Còn yêu cầu mua trái phiếu - tiết kiệm hoặc gia hạn chưa hoàn thành. Vui lòng hoàn thành yêu cầu để có thể thay đổi trạng thái'
//                ];
//            }

            if ($param['status'] == 0) {
                $data = [
                    'is_deleted' => 0,
                    'is_actived' => 1
                ];

//                $this->configInterestProduct($param['product_category_id'],$param['id'],$param['month']);
            } else {
                $data = [
                    'is_deleted' => 1,
                    'is_actived' => 0
                ];
                $mProductInterest = new ProductInterestTable();
                $getIsDisplay = $mProductInterest->getIsDisplay($param['id']);
//                $mProductInterest->deleteByInvestmentTime($param['id']);
                if (count($getIsDisplay) != 0) {
                    foreach ($getIsDisplay as $item){
//                        cập nhật hiển thị mặc dịnh cho cấu hình đầu tiên
                        $dataUpdate['is_default_display'] = 1;
                        $mProductInterest->updateByProduct($item['product_id'],$dataUpdate);
                    }
                }
            }
            $this->investmentTime->updatedInvestment($data,$param['id']);

            return [
                'error' => false,
                'message' => 'Cập nhật trạng thái thành công'
            ];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => 'Cập nhật trạng thái thất bại'
            ];
        }
    }


}