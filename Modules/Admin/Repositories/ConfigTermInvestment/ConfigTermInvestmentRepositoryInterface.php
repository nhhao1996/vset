<?php


namespace Modules\Admin\Repositories\ConfigTermInvestment;


interface ConfigTermInvestmentRepositoryInterface
{
    public function list(array $filters = []);

//    Lấy view popup để tạo , chỉnh sửa kì hạn đầu tư
    public function showPopup($param);

//    Lấy danh sách loại gói
    public function getCategoryProduct();

//    Tạo kì hạn đầu tư
    public function store($param);

//    Cập nhật kì hạn đầu tư
    public function update($param);

//    Xóa cấu hình kì hạn đầu tư
    public function delete($id);

//    Cập nhật trạng thái kì hạn đầu tư
    public function changStatus($param);
}