<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/23/2021
 * Time: 3:02 PM
 * @author nhandt
 */


namespace Modules\Admin\Repositories\StockHistory;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockHistoryDivideTable;
use Modules\Admin\Models\StockHistoryReceiveTable;
use Modules\Admin\Models\StockTable;
use Modules\Admin\Models\StockTransactionTable;
use Modules\Admin\Models\StockWalletTable;
use Modules\Admin\Repositories\Message\MessageRepoInterface;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;

class StockHistoryRepo implements StockHistoryRepoInterface
{
    private $stockHistoryDivide;
    private $stockCustomer;
    private $stockTransaction;
    private $stockRepo;
    public function __construct(StockHistoryDivideTable $stockHistoryDivide,
                                StockCustomerTable $stockCustomer,
                                StockTransactionTable $stockTransaction,
                                StockRepositoryInterface $stockRepo
    )
    {
        $this->stockHistoryDivide = $stockHistoryDivide;
        $this->stockCustomer = $stockCustomer;
        $this->stockTransaction = $stockTransaction;
        $this->stockRepo = $stockRepo;
    }

    public function testDividend()
    {
        try {
            $messageRepo = app()->get(MessageRepoInterface::class);
//            $dividend = $this->stockHistoryDivide->getHistoryShareDividendLatest();
            $dataShareDividend = $this->stockHistoryDivide->getListShareDividend();
            foreach ($dataShareDividend as $dividend){
                $datePublish = Carbon::createFromFormat('Y-m-d H:i:s', $dividend['date_publish'])->format('d/m/Y');
                $dateCurrent = Carbon::now()->format('d/m/Y');
                if($datePublish == $dateCurrent){
                    $customer = $this->stockCustomer->getListStockCustomer();
                    $stockTable = new StockTable();
                    $currentMoneyPublisher = $stockTable->getMoney();
                    foreach ($customer as $item){
                        $customerId = $item['customer_id'];
                        // get current stock
                        $currentStock = $this->stockCustomer->getStock($customerId)['stock'];
                        $currentStockBonus = $this->stockCustomer->getStockBonus($customerId)['stock_bonus'];
                        $stockBonus = 0;
                        if($dividend['stock_bonus_after'] != null &&
                            $dividend['stock_bonus_before'] != null && $dividend['stock_bonus_before'] != 0){
                            $stockBonus = (int)($currentStock / $dividend['stock_bonus_before']) * $dividend['stock_bonus_after'];
                            // add stock_transaction
                            $this->addStockBonusTransaction($customerId,$currentStockBonus,$stockBonus);
                            // TODO: insert dividend log
                            $dividendValue = $dividend['stock_bonus_before'] .':'.$dividend['stock_bonus_after'];
                            $this->addStockHistoryReceive($customerId,$dividend['stock_history_divide_id'],$dividend['stock_id'],5,$dividendValue,$stockBonus);
                        }
                        // get current money
                        $currentMoney = $this->stockCustomer->getDividendWalletMoney($customerId)['wallet_dividend_money'];
                        $moneyBonus = 0;
                        if($dividend['stock_money_rate'] != null && $dividend['stock_money_rate'] != 0){
                            $moneyBonus = $currentMoneyPublisher['money'] * $currentStock * (float)$dividend['stock_money_rate'] / 100;
                            // add stock_wallet
                            $this->addStockWallet($customerId,$currentMoney,$moneyBonus);
                            // TODO: insert dividend log
                            $this->addStockHistoryReceive($customerId,$dividend['stock_history_divide_id'],$dividend['stock_id'],4,$dividend['stock_money_rate'],$moneyBonus);
                        }
                        // update stock_customer
                        $arrStockCustomer =[
                            'wallet_dividend_money' => $currentMoney + $moneyBonus,
                            'stock_bonus' => $currentStockBonus + $stockBonus,
                            'last_updated' => Carbon::now(),
                            'created_at' => Carbon::now(),
                            'created_by' => Auth()->id(),
                            'updated_at' => Carbon::now(),
                            'updated_by' => Auth()->id()
                        ];
                        $this->stockCustomer->updateCustomer($customerId,$arrStockCustomer);
//            // TODO: push noti
//            $mNoti = new SendNotificationApi();
//            $mNoti->sendNotification([
//                'key' => 'stock_admin_divide_dividend',
//                'customer_id' => $customerId,
//                'object_id' => Carbon::now()->format('Y')
//            ]);
//             todo: send Noti to Nhận CP thưởng cho Customer-----------------------------------
                        $mNoti = new JobsEmailLogApi();
                        $data =[
                            'key' => 'stock_admin_divide_dividend',
                            'customer_id' => $item['customer_id'],
                            'object_id' => Carbon::now()->year
                        ];
                        $mNoti->sendNotification($data);
//                        todo: Send SMS-------------------------------------------------------
                        $messageRepo->sendSMS(
                            'stock_admin_divide_dividend',
                            [
                                'phone'=>$item['phone']
                            ],[
                                'full_name'=>$item['full_name']
                            ]


                        );
//            end: Send Noti to Customer---------------------------------------------------------
                    }
                    // TODO: update active stock dividend
                    $dataUpdate = ['is_active' => 1];
                    $this->stockHistoryDivide->updateHistoryShareDividend($dataUpdate,$dividend['stock_history_divide_id']);
                }

            }
        }
        catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
    public function testReceiveStockBonus(){
        try{
            $stockBonus = $this->stockTransaction->getListStockBonus();
            $customerId = 0;
            foreach($stockBonus as $item){
                $customerId = $item['customer_id'];
                // get current stock
                $currentStock = $this->stockCustomer->getStock($customerId)['stock'];
                $currentStockBonus = $this->stockCustomer->getStockBonus($customerId)['stock_bonus'];
                $arrStockCustomer =[
                    'stock' => (int)$currentStock + $item['quantity'],
                    'stock_bonus' => (int)$currentStockBonus - $item['quantity'],
                    'last_updated' => Carbon::now(),
                    'created_at' => Carbon::now(),
                    'created_by' => Auth()->id(),
                    'updated_at' => Carbon::now(),
                    'updated_by' => Auth()->id()
                ];
                $this->stockCustomer->updateCustomer($customerId,$arrStockCustomer);
                $updateIsActiveStockBonus = ["is_active" => 1];
                $this->stockTransaction->updateStatusStockBonus($updateIsActiveStockBonus,$item['stock_transaction_id']);

//         todo:Tạo  hoặc update hợp đồng và phụ lục hơp đồng----------------
                $this->stockRepo->createOrUpdateStockContract($customerId,$currentStockBonus);
                // todo: send Noti to Nhận CP thưởng cho Customer-----------------------------------
                $mNoti = new JobsEmailLogApi();
                $data =[
                    'key' => 'stock_user_receive_bonus',
                    'customer_id' =>$customerId,
                    'object_id' => Carbon::now()->year
                ];
                $mNoti->sendNotification($data);
//          end: Send Noti to Customer---------------------------------
            }
        }
        catch (\Exception $e){
            dd($e->getMessage());
        }
    }
    protected function addStockTransaction($customerId,$currentStock,$quantity){
        $stockTable = new StockTable();
        $currentMoneyPublisher = $stockTable->getMoney();
        $arrStockTransaction = [
            'stock_type' => 'dividend',
            'source' => 'dividend',
            'type' => 'add',
            'stock_order_id' => '',
            'customer_id' => $customerId,
            'obj_id' => '',
            'quantity_before' => (int)$currentStock,
            'quantity' => (int)$quantity,
            'quantity_after' => (int)$currentStock + (int)$quantity,
            'money_before' => $currentMoneyPublisher['money'],
            'money_standard' => $currentMoneyPublisher['money_publish'],
            'payment_method_id' => '',
            'date' => Carbon::now()->format('d'),
            'month' => Carbon::now()->format('m'),
            'year' => Carbon::now()->format('Y'),
            'created_at' => Carbon::now(),
            'created_by' => \auth()->id(),
        ];
        $this->stockTransaction->createTransaction($arrStockTransaction);
    }
    protected function addStockBonusTransaction($customerId,$currentStockBonus,$quantity){
        $stockTable = new StockTable();
        $currentMoneyPublisher = $stockTable->getMoney();
        $arrStockTransaction = [
            'stock_type' => 'bonus',
            'source' => 'dividend',
            'type' => 'add',
            'stock_order_id' => '',
            'customer_id' => $customerId,
            'obj_id' => '',
            'quantity_before' => (int)$currentStockBonus,
            'quantity' => (int)$quantity,
            'quantity_after' => (int)$currentStockBonus + (int)$quantity,
            'money_before' => $currentMoneyPublisher['money'],
            'money_standard' => $currentMoneyPublisher['money_publish'],
            'payment_method_id' => '',
            'date' => Carbon::now()->format('d'),
            'month' => Carbon::now()->format('m'),
            'year' => Carbon::now()->format('Y'),
            'created_at' => Carbon::now(),
            'created_by' => \auth()->id(),
        ];
        $this->stockTransaction->createTransaction($arrStockTransaction);
    }
    protected function addStockWallet($customerId,$currentMoney,$money){
        $arrStockWallet = [
            'customer_id' => $customerId,
            'type' => 'add',
            'source' => 'dividend',
            'total_before' => (float)$currentMoney,
            'total_money' => (float)$money,
            'total_after' => (float)$currentMoney + (float)$money,
            'day' => Carbon::now()->format('d'),
            'month' => Carbon::now()->format('m'),
            'year' => Carbon::now()->format('Y'),
            'created_at' => Carbon::now(),
            'created_by' => \auth()->id()
        ];
        $stockWalletTable = new StockWalletTable();
        $stockWalletTable->createStockOrDividendWallet($arrStockWallet);
    }
    protected function addStockHistoryReceive($customerId,$stockHistoryDivideId,$stockId,$paymentMehthodId,$dividend,$value){
        $stockHistoryReceiveTable = new StockHistoryReceiveTable();
        $data = [
            'stock_history_divide_id' => $stockHistoryDivideId,
            'stock_id' => $stockId,
            'customer_id' => $customerId,
            'payment_method_id' => $paymentMehthodId,
            'day' => Carbon::now()->format('d'),
            'month' => Carbon::now()->format('m'),
            'year' => Carbon::now()->format('Y'),
            'dividend' => $dividend,
            'value' => $value,
            'created_at' => Carbon::now(),
            'created_by' => auth()->id(),
            'updated_at' => Carbon::now(),
            'updated_by' => auth()->id(),
        ];
        $stockHistoryReceiveTable->createHistory($data);
    }
}