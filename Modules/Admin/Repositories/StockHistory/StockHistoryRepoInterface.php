<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/23/2021
 * Time: 3:02 PM
 * @author nhandt
 */

namespace Modules\Admin\Repositories\StockHistory;


interface StockHistoryRepoInterface
{
    public function testDividend();
    public function testReceiveStockBonus();
}