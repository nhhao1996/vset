<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 21/3/2019
 * Time: 15:09
 */

namespace Modules\Admin\Repositories\Banner;


interface BannerRepositoryInterface
{
    /**
     * Lấy danh sách popup
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = []);

    /**
     * Lấy tạo popup
     * @return mixed
     */
    public function submitAdd($param);

    /**
     * Lấy chi tiết popup
     * @param $id
     * @return mixed
     */
    public function getDetail($id);

    /**
     * Cập nhật popup
     * @param $input
     * @return mixed
     */
    public function submitEdit($param);
}
