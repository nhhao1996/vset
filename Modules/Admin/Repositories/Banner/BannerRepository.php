<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 21/3/2019
 * Time: 15:09
 */

namespace Modules\Admin\Repositories\Banner;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\BannerSliderTable;
use Modules\Admin\Models\BannerTable;
use Modules\Admin\Repositories\Banner\BannerRepositoryInterface;

class BannerRepository implements BannerRepositoryInterface
{

    protected $banner;
    public function __construct(BannerTable $banner)
    {
        $this->banner = $banner;
    }

    public function list(array $filters = [])
    {
        return $this->banner->getList($filters);
    }

    public function submitAdd($param)
    {
        try {
            $dateTime = explode(" - ", $param['display']);
            $from = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $to = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');
//            kiểm tra thời gian
            if ($param['is_actived'] == 1) {
                $params['is_actived'] = 1;
                $check = $this->banner->checkBanner(null,$from, $to, $params);
                if (count($check) != 0) {
                    return [
                        'error' => true,
                        'message' => 'Khung thời gian này đã có popup khuyến mãi khác'
                    ];
                }
            }
            $data = [
                'name' => $param['name'],
                'is_active' => $param['is_actived'],
                'from' => $from,
                'to' => $to,
                'created_at' => Carbon::now(),
                'created_by' => Auth::id(),
                'updated_at' => Carbon::now(),
                'updated_by' => Auth::id(),
            ];
            $data['img_desktop_vi'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_desktop_vi'], str_replace('', '', $param['img_desktop_vi']));
            $data['img_desktop_en'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_desktop_en'], str_replace('', '', $param['img_desktop_en']));
            $data['img_mobile_vi'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_mobile_vi'], str_replace('', '', $param['img_mobile_vi']));
            $data['img_mobile_en'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_mobile_en'], str_replace('', '', $param['img_mobile_en']));

            $this->banner->createPopup($data);

            return [
                'error' => false,
                'message' => 'Tạo popup khuyến mãi thành công'
            ];
        }catch (\Exception $e) {
            return [
               'error' => true,
               'message' => 'Tạo popup khuyến mãi thất bại'
            ];
        }
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

//    Lấy chi tiết popup khuyến mãi
    public function getDetail($id)
    {
        return $this->banner->getDetail($id);
    }

    public function submitEdit($param)
    {
        try {
            $id = $param['banner_id'];
            $dateTime = explode(" - ", $param['display']);
            $from = Carbon::createFromFormat('d/m/Y', $dateTime[0])->format('Y-m-d 00:00:00');
            $to = Carbon::createFromFormat('d/m/Y', $dateTime[1])->format('Y-m-d 23:59:59');
//            kiểm tra thời gian
            if ($param['is_actived'] == 1) {
                $check = $this->banner->checkBanner($id,$from,$to);
                if (count($check) != 0) {
                    return [
                        'error' => true,
                        'message' => 'Khung thời gian này đã có popup khuyến mãi khác'
                    ];
                }
            }
            $data = [
                'name' => $param['name'],
                'is_active' => $param['is_actived'],
                'from' => $from,
                'to' => $to,
                'updated_at' => Carbon::now(),
                'updated_by' => Auth::id(),
            ];

            if (strpos($param['img_desktop_vi'],'https://') === false) {
                $data['img_desktop_vi'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_desktop_vi'], str_replace('', '', $param['img_desktop_vi']));
            } else {
                $data['img_desktop_vi'] = $param['img_desktop_vi'];
            }

            if (strpos($param['img_desktop_en'],'https://') === false) {
                $data['img_desktop_en'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_desktop_en'], str_replace('', '', $param['img_desktop_en']));
            } else {
                $data['img_desktop_en'] = $param['img_desktop_en'];
            }

            if (strpos($param['img_mobile_vi'],'https://') === false) {
                $data['img_mobile_vi'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_mobile_vi'], str_replace('', '', $param['img_mobile_vi']));
            } else {
                $data['img_mobile_vi'] = $param['img_mobile_vi'];
            }

            if (strpos($param['img_mobile_en'],'https://') === false) {
                $data['img_mobile_en'] = url('/').'/' .$this->transferTempfileToAdminfile($param['img_mobile_en'], str_replace('', '', $param['img_mobile_en']));
            } else {
                $data['img_mobile_en'] = $param['img_mobile_en'];
            }

            $this->banner->editPopup($data,$id);

            return [
                'error' => false,
                'message' => 'Chỉnh sửa popup khuyến mãi thành công'
            ];
        }catch (\Exception $e) {
            dd($e->getMessage());
            return [
                'error' => true,
                'message' => 'Chỉnh sửa popup khuyến mãi thất bại'
            ];
        }
    }
}
