<?php

namespace Modules\Admin\Repositories\GroupCustomer;

interface GroupCustomerRepositoryInterface
{
    /**
     * Danh sách nhà đầu tư
     * @param $params
     * @return mixed
     */
    public function loadCustomer($params);

    /**
     * Thêm khách hàng vào session - tạm
     * @param $params
     * @return mixed
     */
    public function checkedCustomer($params);

    /**
     * Xóa hết khách hàng từ session - tạm
     * @param $params
     * @return mixed
     */
    public function removeSessionTemp($params);

    /**
     * Thêm khách hàng từ session tạm và session selected
     * @param $params
     * @return mixed
     */
    public function submitAddItem($params);

    /**
     * Xóa khách hàng đã chọn ra khỏi session selected
     * @param $params
     * @return mixed
     */
    public function removeItem($params);

    /**
     * Thêm mới nhóm khách hàng
     * @param $params
     * @return mixed
     */
    public function store($params);

    /**
     * Set session default các khách hàng của nhóm khách hàng
     * @param $id
     * @param $unique
     * @return mixed
     */
    public function setDefaultCustomer($id, $unique);

    /**
     * Chi tiết nhóm khách hàng
     * @param $id
     * @return mixed
     */
    public function getItem($id);

    /**
     * Chỉnh sửa
     * @param $params
     * @return mixed
     */
    public function update($params);

    /**
     * Danh sách
     * @param $params
     * @return mixed
     */
    public function getList($params);

    /**
     * Xóa
     * @param $params
     * @return mixed
     */
    public function destroy($params);
}