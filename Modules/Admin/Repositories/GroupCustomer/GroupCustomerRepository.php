<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/24/2018
 * Time: 10:40 AM
 */

namespace Modules\Admin\Repositories\GroupCustomer;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Libs\FunctionLib;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\GroupCustomerMapCustomerTable;
use Modules\Admin\Models\GroupCustomerTable;
use Modules\Admin\Models\GroupStaffMapCustomerTable;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;


class GroupCustomerRepository implements GroupCustomerRepositoryInterface
{
    use FunctionLib;
    protected $rCustomer;
    protected $mCustomer;
    protected $mGroupCustomer;
    protected $mGroupCustomerMapCustomer;
    protected $mStaffCustomer;

    const IS_DELETED = 1;

    public function __construct(
        CustomerRepositoryInterface $rCustomer,
        GroupCustomerTable $mGroupCustomer,
        GroupCustomerMapCustomerTable $mGroupCustomerMapCustomer,
        GroupStaffMapCustomerTable $mStaffCustomer,
        CustomerTable $mCustomer
    ) {
        $this->rCustomer = $rCustomer;
        $this->mGroupCustomer = $mGroupCustomer;
        $this->mGroupCustomerMapCustomer = $mGroupCustomerMapCustomer;
        $this->mStaffCustomer = $mStaffCustomer;
        $this->mCustomer = $mCustomer;
    }

    /**
     * Danh sách nhà đầu tư
     * @param $params
     * @return mixed|string
     * @throws \Throwable
     */
    public function loadCustomer($params)
    {
        try {
            $this->stripTagData($params);
            $unique = $params['unique'];
            $data = session()->get($unique);
            $getListCustomer = $this->mGroupCustomerMapCustomer->getListCustomerId(isset($params['group_customer_id']) ? $params['group_customer_id'] : null);
            if (count($getListCustomer) != 0) {
                $getListCustomer = collect($getListCustomer)->pluck('customer_id')->toArray();
            }

            $itemSelected = $data['item_selected'] ?? [0];
            $itemTemp = $data['item_temp'] ?? [];
            $page    = (int) ($params['page'] ?? 1);
            $isCustomerSelected = $params['isCustomerSelected'] ?? 0;
            $filter = [
                'not_in' => $itemSelected,
                'typeCustomer' => $params['typeCustomer'] ?? null,
//                'customers$is_actived' => $params['isActived'] ?? null,
                'customers$is_actived' => 1,
                'customers$is_deleted' => 0,
                //Keyword tên, sđt, email
                'keyword_FN_P_E' => $params['keyword'] ?? null,
                'getListCustomer' => $getListCustomer,
                'page' => $page,
            ];
            $show = $params['show'] ?? 0;
            if ($isCustomerSelected == 1) {
                unset($filter['not_in']);
                $filter['arr_refer_id'] = $itemSelected;
                unset($filter['getListCustomer']);
            }
            $page    = (int) ($params['page'] ?? 1);
            $perpage = (int) ($params['perpage'] ?? PAGING_ITEM_PER_PAGE);

            $list = $this->rCustomer->list($filter);
            $currentPage = $list->toArray()['last_page'];
            if ($page > $currentPage) {
                $filter['page'] = $page - 1;
                $list = $this->rCustomer->list($filter);
            }
            //Render view để append
            $view = view('admin::group-customer.append.list-customer', [
                'list' => $list,
                'itemTemp' => $itemTemp,
                'isCustomerSelected' => $isCustomerSelected,
                'page' => $page,
                'perpage' => $perpage,
                'show' => $show,
            ])
                ->render();
            return $view;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Thêm khách hàng vào session - tạm
     * @param array $params
     *
     * @return array|mixed
     */
    public function checkedCustomer($params = [])
    {
        $this->stripTagData($params);
        $arrayItem = $params['array_item'] ?? [];
        if ($arrayItem != []) {
            $type = $params['type'];
            $unique = $params['unique'];
            $data = session()->get($unique);
            $list = $data['item_temp'] ?? [];
            //Checked
            if ($type == 'checked') {
                $list = array_merge($list, $arrayItem);
            } else { //Uncheck
                foreach ($list as $key => $value) {
                    foreach ($arrayItem as $k => $v) {
                        if ($v == $value) {
                            unset($list[$key]);
                        }
                    }
                }
            }
            $data['item_temp'] = $list;
            session()->put($unique, $data);
            return $list;
        }
    }

    /**
     * Xóa hết khách hàng từ session - tạm
     * @param array $params
     * @return mixed
     */
    public function removeSessionTemp($params = [])
    {
        $this->stripTagData($params);
        $unique = $params['unique'];
        $data = session()->get($unique);
        $data['item_temp'] = [];
        session()->put($unique, $data);
        return $data;
    }

    /**
     * Thêm khách hàng từ session tạm và session selected
     * @param array $params
     * @return mixed
     */
    public function submitAddItem($params = [])
    {
        $this->stripTagData($params);
        $unique = $params['unique'];
        $data = session()->get($unique);
        $itemSelected = $data['item_selected'] ?? [];
        $itemTemp = $data['item_temp'] ?? [];
        if ($itemTemp != []) {
            $itemSelected = array_merge($itemSelected, $itemTemp);
            $data['item_selected'] = $itemSelected;
            $data['item_temp'] = [];
            session()->put($unique, $data);
        }
        return $data;
    }

    /**
     * Xóa khách hàng đã chọn ra khỏi session selected
     * @param array $params
     * @return mixed
     */
    public function removeItem($params = [])
    {
        $this->stripTagData($params);
        $unique = $params['unique'];
        $id = $params['id'];
        $data = session()->get($unique);
        $itemSelected = $data['item_selected'] ?? [];
        if ($itemSelected != []) {
            foreach ($itemSelected as $key => $item) {
                if ($id == $item) {
                    unset($itemSelected[$key]);
                }
            }
            $data['item_selected'] = $itemSelected;
            session()->put($unique, $data);
        }
        return $data;
    }


    /**
     * Thêm mới nhóm khách hàng
     * @param $params
     * @return array
     */
    public function store($params)
    {
        DB::beginTransaction();
        try {
            $this->stripTagData($params);
            $unique = $params['unique'];
            $data = session()->get($unique);
            $itemSelected = $data['item_selected'] ?? [];
//            if ($itemSelected == []) {
//                return [
//                    'error' => true,
//                    'array_error' => [
//                        __('admin::validation.group_customer.not_item_selected')
//                    ]
//                ];
//            } else {
//                // 1 khách hàng chỉ xuất hiện 1 lần trong array selected
//                $itemSelected = array_keys(array_flip($itemSelected));
//            }

            if (count($itemSelected) != 0) {
                $itemSelected = array_keys(array_flip($itemSelected));
            }
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $currentUser = Auth::id();
            $dataGroupCustomer = [
                'group_name' => $params['group_name'],
                'is_active' => 1,
                'is_deleted' => 0,
                'created_at' => $now,
                'created_by' => $currentUser,
                'updated_at' => $now,
                'updated_by' => $currentUser
            ];
            $groupCustomerId = $this->mGroupCustomer->add($dataGroupCustomer);
            $dataGroupDetail = [];
            $arrCustomerId = [];
            if (count($itemSelected) != 0) {
                foreach ($itemSelected as $key => $value) {
                    $dataGroupDetail[] = [
                        'group_customer_id' => $groupCustomerId,
                        'customer_id' => $value,
                    ];
                    $arrCustomerId[] = $value;
                    if (count($dataGroupDetail) == 500) {
                        $this->mGroupCustomerMapCustomer->addInsert($dataGroupDetail);
                        $dataGroupDetail = [];
                    }
                }
            }

            if ($dataGroupDetail != []) {
                $this->mGroupCustomerMapCustomer->addInsert($dataGroupDetail);
            }

            if (count($arrCustomerId) != 0) {
                $this->mCustomer->updateByArrCustomerId($arrCustomerId,['customer_group_id' => $groupCustomerId]);
            }
            DB::commit();
            return [
                'error' => false,
                'array_error' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Set session default các khách hàng của nhóm khách hàng
     * @param $id
     * @param $unique
     * @return mixed
     */
    public function setDefaultCustomer($id, $unique)
    {
        $customerByGroup = $this->mGroupCustomerMapCustomer->getByGroup($id)
            ->pluck('customer_id')->toArray();
        $data['item_selected'] = $customerByGroup;
        session()->put($unique, $data);
    }

    public function getItem($id)
    {
        $result = $this->mGroupCustomer->getItem($id);
        return $result;
    }

    /**
     * Chỉnh sửa
     * @param $params
     * @return array
     */
    public function update($params)
    {
        DB::beginTransaction();
        try {
            $this->stripTagData($params);
            $id = $params['group_customer_id'];
            $unique = $params['unique'];
            $data = session()->get($unique);
            $itemSelected = $data['item_selected'] ?? [];
//            if ($itemSelected == []) {
//                return [
//                    'error' => true,
//                    'array_error' => [
//                        __('admin::validation.group_customer.not_item_selected')
//                    ]
//                ];
//            } else {
//                // 1 khách hàng chỉ xuất hiện 1 lần trong array selected
//                $itemSelected = array_keys(array_flip($itemSelected));
//            }

            if ( count($itemSelected) != 0) {
                $itemSelected = array_keys(array_flip($itemSelected));
            }
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $currentUser = Auth::id();
            $dataGroupCustomer = [
                'group_name' => $params['group_name'],
                'updated_at' => $now,
                'updated_by' => $currentUser
            ];
            $this->mGroupCustomer->edit($id, $dataGroupCustomer);
            $this->mGroupCustomerMapCustomer->remove($id);
            //Thêm ds khách hàng vào db
            $dataGroupDetail = [];
            $arrCustomerId = [];
            if (count($itemSelected) != 0) {
                foreach ($itemSelected as $key => $value) {
                    $dataGroupDetail[] = [
                        'group_customer_id' => $id,
                        'customer_id' => $value,
                    ];
                    $arrCustomerId[] = $value;

                    if (count($dataGroupDetail) == 500) {
                        $this->mGroupCustomerMapCustomer->addInsert($dataGroupDetail);
                        $dataGroupDetail = [];
                    }
                }
            }

            if ($dataGroupDetail != []) {
                $this->mGroupCustomerMapCustomer->addInsert($dataGroupDetail);
            }

            if (count($arrCustomerId) != 0) {
                $this->mCustomer->deleteJourney($id);
                $this->mCustomer->updateByArrCustomerId($arrCustomerId,['customer_group_id' => $id]);
            }
            DB::commit();
            return [
                'error' => false,
                'array_error' => []
            ];
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Danh sách
     * @param $params
     * @return mixed
     */
    public function getList($params)
    {
        $list = $this->mGroupCustomer->getList($params);
        return $list;
    }

    /**
     * Xóa
     * @param $params
     */
    public function destroy($params)
    {
        $id = $params['id'];
        $checkDelete = $this->mStaffCustomer->checkDelete($id);
        if (count($checkDelete) != 0) {
            return [
                'error' => true,
                'message' => 'Xóa thất bại! Nhóm khách hàng đã được nhân viên quản lý'
            ];
        }
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $currentUser = Auth::id();
        $dataGroupCustomer = [
            'is_deleted' => self::IS_DELETED,
            'updated_at' => $now,
            'updated_by' => $currentUser
        ];
//        $this->mGroupCustomer->edit($id, $dataGroupCustomer);
        $this->mGroupCustomer->deleteCustomer($id);
        $this->mGroupCustomerMapCustomer->deleteCustomer($id);
        $this->mCustomer->deleteJourney($id);
        return [
            'error' => false,
            'message' => 'Xóa nhóm khách hàng thành công'
        ];
    }
}