<?php


namespace Modules\Admin\Repositories\StockNews;


interface StockNewsRepositoryInterface
{
//    Lấy danh sách tin tức phân trang
    public function getListNew(array $filters = []);
//    Danh sách danh mục
    public function getListCategory();
//    Tạo bài viết
    public function store($data);
//    Cập nhật tin tức
    public function update($data);
//    Lấy thông tin chi tiết
    public function getDetail($id);
//    Xóa tin tức
    public function removeNews($id);
}