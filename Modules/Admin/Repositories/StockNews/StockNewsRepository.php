<?php


namespace Modules\Admin\Repositories\StockNews;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Libs\SmsFpt\TechAPI\src\TechAPI\Exception;
use Modules\Admin\Models\BankTable;
use Modules\Admin\Models\StockCategoryTable;
use Modules\Admin\Models\StockNewsTable;

class StockNewsRepository implements StockNewsRepositoryInterface
{
    protected $mStockNews;

    public function __construct(StockNewsTable $mStockNews)
    {
        $this->mStockNews = $mStockNews;
    }

    public function getListNew(array $filters = [])
    {
        return $this->mStockNews->getList($filters);
    }

    public function getListCategory()
    {
        $mStockCategory = New StockCategoryTable();
        return $mStockCategory->getAll();
    }

    public function store($data)
    {
        try {

            $messageError = null;
            $checkVI = $this->mStockNews->checkName($data['stock_category_id'],['stock_news_title_vi' => $data['stock_news_title_vi']]);
            $checkEN = $this->mStockNews->checkName($data['stock_category_id'],['stock_news_title_en' => $data['stock_news_title_en']]);

            if (count($checkVI) != 0){
                $messageError = 'Tên tin tức(VI) của Danh mục đã được tạo';
            }

            if (count($checkEN) != 0){
                $messageError = $messageError.'<br>Tên tin tức(EN) của Danh mục đã được tạo';
            }

            if($messageError != null) {
                return [
                    'error' => true,
                    'message' => $messageError
                ];
            }

            $data['stock_news_title_vi'] = strip_tags($data['stock_news_title_vi']);
            $data['stock_news_title_en'] = strip_tags($data['stock_news_title_en']);

            $data['created_at'] = Carbon::now();
            $data['created_by'] = 1;
            $data['updated_at'] = Carbon::now();
            $data['updated_by'] = 1;

            $this->mStockNews->createNews($data);

            return [
                'error' => false,
                'message' => 'Tạo tin tức thành công'
            ];
        }catch (Exception $e){
            return [
                'error' => true,
                'message' => 'Tạo tin tức thất bại'
            ];
        }
    }

    public function update($data)
    {
        try {

            $id = $data['stock_news_id'];
            unset($data['stock_news_id']);
            $messageError = null;
            $checkVI = $this->mStockNews->checkName($data['stock_category_id'],['stock_news_title_vi' => $data['stock_news_title_vi']],$id);
            $checkEN = $this->mStockNews->checkName($data['stock_category_id'],['stock_news_title_en' => $data['stock_news_title_en']],$id);

            if (count($checkVI) != 0){
                $messageError = 'Tên tin tức(VI) của Danh mục đã được tạo';
            }

            if (count($checkEN) != 0){
                $messageError = $messageError.'<br>Tên tin tức(EN) của Danh mục đã được tạo';
            }

            if($messageError != null) {
                return [
                    'error' => true,
                    'message' => $messageError
                ];
            }

            $data['stock_news_title_vi'] = strip_tags($data['stock_news_title_vi']);
            $data['stock_news_title_en'] = strip_tags($data['stock_news_title_en']);

            $data['updated_at'] = Carbon::now();
            $data['updated_by'] = 1;

            $this->mStockNews->updateNews($data,$id);

            return [
                'error' => false,
                'message' => 'Cập nhật tin tức thành công'
            ];
        }catch (Exception $e){
            return [
                'error' => true,
                'message' => 'Cập nhật tin tức thất bại'
            ];
        }
    }

    public function getDetail($id)
    {
        return $this->mStockNews->getDetail($id);
    }

    public function removeNews($id)
    {
        try {
            $this->mStockNews->removeNews($id);
            return [
                'error' => false,
                'message' => 'Xóa tin tức thành công'
            ];
        }catch (Exception $e){
            return [
                'error' => true,
                'message' => 'Xóa tin tức thất bại'
            ];
        }
    }

}