<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 3:13 PM
 * @author nhandt
 */


namespace Modules\Admin\Repositories\StockMarket;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockMarketTable;
use Modules\Admin\Models\StockOrderTable;
use Modules\Admin\Models\StockTransactionTable;
use Modules\Admin\Models\StockWalletTable;

class StockMarketRepo implements StockMarketRepoInterface
{

    private $stockMarket;
    public function __construct(StockMarketTable $stockMarket)
    {
        $this->stockMarket = $stockMarket;
    }

    /**
     * Danh sách cổ phiếu đang bán
     *
     * @param array $filters
     * @return mixed
     */
    public function getList(array &$filters = [])
    {
        return $this->stockMarket->getList($filters);
    }

    /**
     * Lấy thông tin mua cổ phiếu
     *
     * @param array $filters
     * @return mixed
     */
    public function getListBuyStock(array &$filters = [])
    {
        return $this->stockMarket->getList($filters);
    }

    /**
     * Thông tin chi tiết mua cổ phiếu
     *
     * @param $stock_order_id
     * @return mixed
     */
    public function getDetailBuyStock($stock_order_id)
    {
        return $this->stockMarket->getDetailBuyStock($stock_order_id);
    }

    /**
     * Ds đăng ký bán cổ phiếu ở chợ
     *
     * @param array $filters
     * @return mixed
     */
    public function getListRegisterToSell(array &$filters = [])
    {
        return $this->stockMarket->getList($filters);
    }

    /**
     * Thông tin chi tiết đăng ký bán ở chợ
     *
     * @param $stock_market_id
     * @return mixed
     */
    public function getDetailRegisterToSell($stock_market_id)
    {
        return $this->stockMarket->getDetailRegisterToSell($stock_market_id);
    }

    public function changeStatus($param)
    {
        try{
            DB::beginTransaction();
            $keyNoti = '';
            $message = '';
            if($param['status'] == 'cancel'){
                $keyNoti = 'stock_admin_market_sell_cancel';
                $dataUpdate = [
                    'status' => 'refuse',
                    'reason_vi' => $param['reason_vi'],
                    'reason_en' => $param['reason_en']
                ];
                $this->stockMarket->edit($dataUpdate,$param['stock_market_id']);
                DB::commit();
                $message = 'Hủy xác nhận thành công';
            }
            else{
                $keyNoti = 'stock_admin_market_sell_order';
                $dataUpdate = [
                    'status' => 'selling',
                    'created_at' => Carbon::now(),
                    'created_by' => auth()->id(),
                ];
                $this->stockMarket->edit($dataUpdate,$param['stock_market_id']);
                $message = 'Xác nhận thành công';


            }
            DB::commit();
            // TODO: push noti
            $mNoti = new JobsEmailLogApi();
            $orderMarket = $this->stockMarket->getDetailRegisterToSell($param['stock_market_id']);
            $data =[
                'key' => $keyNoti,
                'customer_id' => $orderMarket['customer_id'],
                'object_id' => $param['stock_market_id'],
                'stock_market_id'=>$param['stock_market_id']
            ];
            $mNoti->sendNotification($data);
//                end: push noti--------------------------
            return [
                'error' => false,
                'message' => $message
            ];
        }
        catch (\Exception $ex){
            DB::rollBack();
            return [
                'error' => true,
                'message' => $ex->getMessage()
            ];
        }
    }
}