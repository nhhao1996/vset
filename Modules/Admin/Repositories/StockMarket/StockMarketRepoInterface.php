<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 3:12 PM
 * @author nhandt
 */

namespace Modules\Admin\Repositories\StockMarket;


interface StockMarketRepoInterface
{
    public function getList(array &$filters = []);
    public function getListBuyStock(array &$filters = []);
    public function getDetailBuyStock($stock_order_id);
    public function getListRegisterToSell(array &$filters = []);
    public function getDetailRegisterToSell($stock_market_id);
    public function changeStatus($param);
}