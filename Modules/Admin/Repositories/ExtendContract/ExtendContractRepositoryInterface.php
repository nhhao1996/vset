<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\ExtendContract;


interface ExtendContractRepositoryInterface
{
    public function list(array $filters = []);
    public function show($id);
    public function addReceipt(array $data);
    public function changeStatus($data);
    public function getMoneyReceipt($order_id);
    public function getListStaff();
    public function getListReceiptDetail($param);
    public function addReceiptDetail($param);
    public function createWallet($param);
    public function createReceipt($param);
    public function showPopupReceiptDetail($param);
    public function getTotalInterest($order_id);
    public function getTotalCommission($order_id,$customer_id);
    public function getTotalCommissionWallet($order_id,$customer_id);
    public function printBill($data);
    public function getOldContract($contract_code_extend);
    /**
     * Upload image dropzone
     *
     * @param $input
     * @return mixed
     */
    public function uploadDropzone($input);

}