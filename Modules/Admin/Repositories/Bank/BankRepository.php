<?php


namespace Modules\Admin\Repositories\Bank;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\BankTable;

class BankRepository implements BankRepositoryInterface
{
    protected $bank;

    public function __construct(BankTable $bank)
    {
        $this->bank = $bank;
    }

    public function getListNew(array $filters = [])
    {
        return $this->bank->getList($filters);
    }

    public function getDetail($id)
    {
        $detail = $this->bank->getDetail($id);
        $view = view(
            'admin::bank.popup.detail',[
                'detail' => $detail
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function getEdit($id)
    {
        $detail = $this->bank->getDetail($id);
        $view = view(
            'admin::bank.popup.edit',[
                'detail' => $detail
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function update($param)
    {
        $id = $param['bank_id'];
        unset($param['bank_id']);

        if ($param['bank_icon'] == null) {
            $param['bank_icon'] = $param['bank_icon_hidden'];
        }  else {
            $param['bank_icon'] = url('/').'/' .$this->transferTempfileToAdminfile($param['bank_icon'], str_replace('', '', $param['bank_icon']));
        }

        $param['bank_name'] = strip_tags($param['bank_name']);
        $param['updated_by'] = Auth::id();
        $param['updated_at'] = Carbon::now();
        unset($param['bank_icon_hidden']);
        // check bank name
        $dataCheckUpdate['bank_id'] = $id;
        $dataCheckUpdate['bank_name'] =$param['bank_name'];
        $bankcheck = $this->bank->checkByUpdate($dataCheckUpdate);
        if ($bankcheck > 0 ){
            return response()->json([
                'error' => true,
                'message' => __('Tên ngân hàng trùng')
            ]);
        }
        //check bank name
        $update = $this->bank->updateBank($id,$param);
        return [
            'error' => false,
            'message' => 'Cập nhật ngân hàng thành công'
        ];
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path=TEMP_PATH . '/' . $filename;
        $new_path=STORE_UPLOADS_PATH.date('Ymd').'/'.$filename;
        Storage::disk('public')->makeDirectory(STORE_UPLOADS_PATH.date('Ymd'));
        Storage::disk('public')->move($old_path,$new_path);
        return $new_path;
    }

    public function addFormBankNew()
    {
        $view = view(
            'admin::bank.popup.create'
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function addbank($data)
    {
        if ($data['bank_icon'] != null) {
            $data['bank_icon'] = url('/').'/' .$this->transferTempfileToAdminfile($data['bank_icon'], str_replace('', '', $data['bank_icon']));
        }

        $data['bank_name'] = strip_tags($data['bank_name']);
        $data['created_by'] = Auth::id();
        $data['updated_by'] = Auth::id();
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        // check bank name
        $bankcheck = $this->bank->checkByname($data['bank_name']);
        if ($bankcheck > 0 ){
            return response()->json([
                'error' => true,
                'message' => __('Tên ngân hàng trùng')
            ]);
        }
        //check bank name
        $addBanks = $this->bank->addBank($data);
        return [
            'error' => false,
            'message' => 'Tạo ngân hàng thành công'
        ];
    }

    public function deleteBank($id)
    {
        $this->bank->deleteBank($id);
        return [
            'error' => false,
            'message' => 'Xóa ngân hàng thành công'
        ];
    }
}