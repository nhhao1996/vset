<?php


namespace Modules\Admin\Repositories\Bank;


interface BankRepositoryInterface
{
    public function getListNew(array $filters = []);

    public function getDetail($id);

    public function getEdit($id);

    public function update($param);

    public function addFormBankNew();

    public function addbank($data);

    public function deleteBank($id);
}