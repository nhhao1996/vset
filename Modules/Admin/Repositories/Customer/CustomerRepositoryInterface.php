<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:06 PM
 */

namespace Modules\Admin\Repositories\Customer;


interface CustomerRepositoryInterface
{
    /**
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = []);

    /**
     * Danh sách nguồn đăng ký
     * @return mixed
     */
    public function getListReferSource();

    /**
     * Kiểm tra hợp đồng để phân loại khách hàng
     * @param $idCustomer
     * @return mixed
     */
    public function checkContract($idCustomer);

    public function getListCustomerSource();

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data);


    /**
     * @param $data
     * @return mixed
     */
    public function getCustomerSearch($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getItem($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getItemRefer($id);


    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function remove($id);

    /**
     * @return mixed
     */
    public function getCustomerOption();

    /**
     * @param $phone
     * @param $id
     * @return mixed
     */
    public function testPhone($phone, $id);

    /**
     * @param $phone
     * @return mixed
     */
    public function searchPhone($phone);

    /**
     * @param $phone
     * @return mixed
     */
    public function getCusPhone($phone);

    public function getCustomerIdName();

    /**
     * @param $yearNow
     * @return mixed
     */
    public function totalCustomer($yearNow);

    /**
     * @param $yearNow
     * @return mixed
     */
    public function totalCustomerNow($yearNow);

    /**
     * @param $year
     * @param $branch
     * @return mixed
     */
    public function filterCustomerYearBranch($year, $branch);

    /**
     * @param $year
     * @param $branch
     * @return mixed
     */
    public function filterNowCustomerBranch($year, $branch);

    /**
     * @param $time
     * @param $branch
     * @return mixed
     */
    public function filterTimeToTime($time, $branch);

    /**
     * @param $time
     * @param $branch
     * @return mixed
     */
    public function filterTimeNow($time, $branch);

    /**
     * @param $data
     * @param $birthday
     * @param $gender
     * @param $branch
     * @return mixed
     */
    public function searchCustomerEmail($data, $birthday, $gender);

    //Lấy danh sách khách hàng có ngày sinh nhật là hôm nay.
    public function getBirthdays();

    public function searchDashboard($keyword);

    /**
     * @param $id_branch
     * @param $time
     * @param $top
     * @return mixed
     */
    public function reportCustomerDebt($id_branch, $time, $top);

    /**
     * @return mixed
     */
    public function getAllCustomer();

    /**
     * lay thong tin khach hang va dia chi mac dinh (neu co)
     * @param $id
     * @return mixed
     */
    public function getCustomerAndDefaultContact($id);

    /** V_Set get detail customer
     * @param int $id
     * @return mixed
     */
    public function vSetGetDetailCustomer($id);
    // V_Set get detail customer   tab lich su hoat dong
    public function operationHistory(array $filter = []);
    public function listContractCustomer(array $filter = []);
    public function listIntroduct(array $filter = []);
    public function listContractCommission(array $filter = []);
    public function listProductView(array $filter = []);
    public function listCustomerSearch(array $filter = []);
    public function changeStatus(array $data, $id) ;
    public function submitResetPassword(array  $data, $id);

//    Danh sách nguồn khách hàng
    public function getCustomerSource();

    public function exportList($data);

    public function getListReferCustomer($customer_refer_id);

    public function getListCategoryService();

    /**
     * Lấy danh sách ngân hàng
     * @return mixed
     */
    public function getListBank();

//    Lấy danh sách hợp đồng có phụ lục
    public function getListContractChange($id);

    /**
     * Lấy danh sách cập nhật phụ lục hợp đồng
     * @param $id
     * @return mixed
     */
    public function getListAppendix($id);

    /**
     * Hủy yêu cầu thay đổi thông tin người dùng
     * @param $input
     * @return mixed
     */
    public function cancelChangeRequest($input);

//    Danh sách hành trình khách hàng
    public function getCustomerJourney();

    public function updateCustomerLogout($id);
    public function getStockByCustomer($id);
    public function checkContractById($customerId);

//    Cập nhật trạng thái hoặc tài khoản test
    public function changeStatusTest($param);
}