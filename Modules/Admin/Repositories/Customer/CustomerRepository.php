<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:06 PM
 */

namespace Modules\Admin\Repositories\Customer;


use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Models\BankTable;
use Modules\Admin\Models\CustomerDeviceTable;
use Modules\Admin\Models\CustomerJourneyTable;
use Modules\Admin\Models\CustomerSearchLogTable;
use Modules\Admin\Models\CustomerSourceTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\CustomerChangeTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\GroupCustomerMapCustomerTable;
use Modules\Admin\Models\GroupCustomerTable;
use Modules\Admin\Models\GroupStaffMapCustomerTable;
use Modules\Admin\Models\GroupStaffMapStaffTable;
use Modules\Admin\Models\GroupStaffTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\PotentialCustomerLogTable;
use Modules\Admin\Models\ProductTable;
use Modules\Admin\Models\ReferSourceTable;
use Modules\Admin\Models\ServiceTable;
use Modules\Admin\Models\WalletTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Admin\Models\AppendixContractTable;
use Modules\Admin\Models\AppendixContractMapTable;
use Modules\Admin\Models\StockTable;
use Modules\Admin\Models\StockOrderHistoryTable;
use Modules\Admin\Models\StockCustomerTable;

class CustomerRepository implements CustomerRepositoryInterface
{
    protected $customer;
    protected $customerChange;
    protected $timestamps = true;
    protected $operationHistory;
    protected $cContract;
    protected $interestByTime;
    protected $withrawRequest;
    protected $withdrawRequestGroup;
    protected $jobsEmailLogApi;
    protected $potentialCustomerLog;
    protected $product;
    protected $service;
    protected $customerSearchLog;
    protected $customerSource;
    protected $bank;
    protected $referSource;
    protected $appendixContract;
    protected $appendixContractMap;
    protected $groupStaffMapStaff;
    protected $groupStaff;
    protected $groupStaffMapCustomer;
    protected $groupCustomerMapCustomer;
    protected $customerJourney;
    protected $wallet;
    protected $customerDevice;
    protected $groupCustomer;
    protected $groupCustomerMap;
    protected $stock;
    protected $operationStockHistory;
    protected $stockCustomer;
    /**
     * CustomerRepository constructor.
     * @param CustomerTable $customers
     * @param OrderTable $operationHistory
     * @param CustomerContactTable $cContract
     * @param CustomerContractInterestByTimetable $interestByTime
     * @param WithdrawRequestGroupTable $withdrawRequestGroup
     * @param JobsEmailLogApi $jobsEmailLogApi
     */
    public function __construct(
        CustomerTable $customers,
        CustomerChangeTable $customerChange,
        OrderTable $operationHistory,
        CustomerContactTable $cContract,
        CustomerContractInterestByTimetable $interestByTime,
        WithdrawRequestGroupTable $withdrawRequestGroup,
        JobsEmailLogApi $jobsEmailLogApi,
        WithdrawRequestTable $withrawRequest,
        PotentialCustomerLogTable $potentialCustomerLog,
        ProductTable $product,
        ServiceTable $service,
        CustomerSearchLogTable $customerSearchLog,
        CustomerSourceTable $customerSource,
        BankTable $bank,
        ReferSourceTable $referSource,
        AppendixContractTable $appendixContract,
        AppendixContractMapTable $appendixContractMap,
        GroupStaffMapStaffTable $groupStaffMapStaff,
        GroupStaffTable $groupStaff,
        GroupStaffMapCustomerTable $groupStaffMapCustomer,
        GroupCustomerMapCustomerTable $groupCustomerMapCustomer,
        CustomerJourneyTable $customerJourney,
        WalletTable $wallet,
        CustomerDeviceTable $customerDevice,
        StockTable $stock,
        StockOrderHistoryTable $operationStockHistory,
        StockCustomerTable $stockCustomer,
        GroupCustomerTable $groupCustomer,
        GroupCustomerMapCustomerTable $groupCustomerMap
    )
    {
        $this->customerChange = $customerChange;
        $this->customer = $customers;
        $this->operationHistory = $operationHistory;
        $this->cContract = $cContract;
        $this->interestByTime = $interestByTime;
        $this->withdrawRequestGroup = $withdrawRequestGroup;
        $this->jobsEmailLogApi = $jobsEmailLogApi;
        $this->withrawRequest = $withrawRequest;
        $this->potentialCustomerLog = $potentialCustomerLog;
        $this->product = $product;
        $this->service = $service;
        $this->customerSearchLog = $customerSearchLog;
        $this->customerSource = $customerSource;
        $this->bank = $bank;
        $this->referSource = $referSource;
        $this->appendixContract= $appendixContract;
        $this->appendixContractMap= $appendixContractMap;
        $this->groupStaffMapStaff = $groupStaffMapStaff;
        $this->groupStaff = $groupStaff;
        $this->groupStaffMapCustomer = $groupStaffMapCustomer;
        $this->groupCustomerMapCustomer = $groupCustomerMapCustomer;
        $this->customerJourney = $customerJourney;
        $this->stock = $stock;
        $this->wallet = $wallet;
        $this->customerDevice = $customerDevice;
        $this->operationStockHistory = $operationStockHistory;
        $this->stockCustomer = $stockCustomer;
        $this->groupCustomer = $groupCustomer;
        $this->groupCustomerMap = $groupCustomerMap;

    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = [])
    {
        $listCustomer = $this->getListCustomer();
        if (Auth::user()['is_admin'] == 0) {
            if (count($listCustomer) != 0) {
                if (isset($listCustomer['type']) && $listCustomer['type'] != 'all') {
                    $filters['listCustomer'] = $listCustomer['arr'];
                }
            }
        }

        $list = $this->customer->getList($filters);

//        $arrID = collect($list->toArray()['data'])->pluck('customer_id');
//        $countMoney = $this->cContract->getItem();
        return $list;
    }

    public function getListCustomer(){
        $staff_id = Auth::id();
        $getConfgiGeneral = $this->groupStaff->getConfigGeneral($staff_id);
        if ($getConfgiGeneral == null) {
            return [];
        }
        $arr = [];
        if ($getConfgiGeneral['type'] == 'all') {
            return [
                'type' => 'all',
                'arr' => []
            ];
        } else {
            if ($getConfgiGeneral['type'] == 'detail') {
                $arr = $this->groupStaffMapCustomer->getListCustomer($getConfgiGeneral['group_staff_id']);
                if (count($arr) != 0) {
                    $arr = collect($arr)->pluck('obj_id');
                }
            }else {
                $arr = $this->groupCustomerMapCustomer->getListCustomer($getConfgiGeneral['group_staff_id']);
                if (count($arr) != 0) {
                    $arr = collect($arr)->pluck('customer_id');
                }
            }
        }

        return [
            'type' => 'customer',
            'arr' => $arr
        ];
    }

    public function getListReferSource()
    {
        return $this->referSource->getListAll();
    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function listChangeRequest(array $filters = [])
    {
        $list = $this->customerChange->getList($filters);

//        $arrID = collect($list->toArray()['data'])->pluck('customer_id');
//        $countMoney = $this->cContract->getItem();
        return $list;
    }

    public function exportList($data)
    {

        $list = $this->customer->exportList100($data);

        //Data export
        $arr_data = [];
        foreach ($list->toArray() as $key => $item) {
            $type_customer = 0;
            $getContract = $this->cContract->getContractByCustomer($item['customer_id']);
            if (count($getContract) != 0) {
                $type_customer =  1;
            }

            $refer = '';
            if ($item['utm_source'] == null){
                if ($item['customer_refer_code'] == null){
                    $refer = 'N/A';
                } else {
                    if ($item['is_referal'] == 1){
                        if ($item['customer_source_name'] == null) {
                            $refer = 'Link copy';
                        } else {
                            $refer = $item['refer_source_name'];
                        }
                    } else {
                        $refer = 'Khác';
                    }
                }
            } else {
                if ($item['is_referal'] == 1) {
                    $refer = $item['refer_source_name'];
                } else {
                    $refer = 'Khác';
                }
            }

            if (isset($data['is_top'])){
                $arr_data [] = [
                    $key+1,
                    $item['customer_code'],
                    $item['full_name'],
                    $item['email'],
                    $item['phone2'],
                    isset($item['stock']) && $item['stock'] != '' ? $item['stock'] : '0',
                    $type_customer == 1  ? __('Nhà đầu tư') : __('Tiềm năng'),
                    isset($item['sum_money_contract']) ? number_format($item['sum_money_contract']) : 0 ,
                    $item['broker_customer_id'] != null ? 'Môi giới' : null,
                    $item['is_test'] == 1 ? 'Tài khoản Test' : null,
//                $item['customer_source_name'],
                    $refer,
                    $item['customer_journey_name'] == null ? 'N/A' : $item['customer_journey_name'],
                    date("d/m/Y",strtotime($item['created_at'])),
                    $item['is_actived'] == 1 ? 'Kích hoạt' : 'Ngừng kích hoạt',
                ];
            }else {
                $arr_data [] = [
                    $key+1,
                    $item['customer_code'],
                    $item['full_name'],
                    $item['email'],
                    $item['phone2'],
                    $type_customer == 1  ? __('Nhà đầu tư') : __('Tiềm năng'),
                    isset($item['sum_money_contract']) ? number_format($item['sum_money_contract']) : 0 ,
                    $item['broker_customer_id'] != null ? 'Môi giới' : null,
                    $item['is_test'] == 1 ? 'Tài khoản Test' : null,
//                $item['customer_source_name'],
                    $refer,
                    $item['customer_journey_name'] == null ? 'N/A' : $item['customer_journey_name'],
                    date("d/m/Y",strtotime($item['created_at'])),
                    $item['is_actived'] == 1 ? 'Kích hoạt' : 'Ngừng kích hoạt',
                ];
            }

        }

        if (isset($data['is_top'])){
            $heading = [
                __('STT'),
                __('Mã nhà đầu tư'),
                __('Họ và tên'),
                __('Email'),
                __('Số điện thoại'),
                __('Số cổ phiếu'),
                __('Loại khách hàng'),
                __('Đang đầu tư (VNĐ)'),
                __('Nhà môi giới'),
                __('Tài khoản test'),
                __('Nguồn khách hàng'),
                __('Hành trình khách hàng'),
                __('Ngày tạo'),
                __('Trạng thái')
            ];
        } else {
            $heading = [
                __('STT'),
                __('Mã nhà đầu tư'),
                __('Họ và tên'),
                __('Email'),
                __('Số điện thoại'),
                __('Loại khách hàng'),
                __('Đang đầu tư (VNĐ)'),
                __('Nhà môi giới'),
                __('Tài khoản test'),
                __('Nguồn khách hàng'),
                __('Hành trình khách hàng'),
                __('Ngày tạo'),
                __('Trạng thái')
            ];
        }

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new ExportFile($heading, $arr_data), 'Danh sách khách hàng.xlsx');

    }

    public function getListBank()
    {
        return $this->bank->getAll();
    }

    public function checkContract($idCustomer)
    {
        $getContract = $this->cContract->getContractByCustomer($idCustomer);
        if (count($getContract) != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getListCustomerSource()
    {
        return $this->customerSource->getAll();
    }

     /**
     * @param array $data
     * @return mixed
     */
    public function addAppendixContract($data){
//        $data['file'] = url('/').'/'.$data['file'];
      
        return $this->appendixContract->add($data);
    }
        /**
     * @param array $data
     * @return mixed
     */
    public function addAppendixContractMap($data){
//        $data['file'] = url('/').'/'.$data['file'];
      
        return $this->appendixContractMap->addAppendix($data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        return $this->customer->add($data);
    }


    /**
     * @param $data
     * @return mixed
     */
    public function getCustomerSearch($data)
    {
        return $this->customer->getCustomerSearch($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItem($id)
    {
        return $this->customer->getItem($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItemRefer($id)
    {
        return $this->customer->getItemRefer($id);
    }


    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function edit(array $data, $id)
    {
        try {

            if (isset($data['birthday']) && $data['birthday'] != null) {
                $data['birthday'] = Carbon::createFromFormat('d/m/Y', $data['birthday'])->format('Y-m-d');
            }
            if (isset($data['ic_date']) && $data['ic_date'] != null) {
                $data['ic_date'] = Carbon::createFromFormat('d/m/Y', $data['ic_date'])->format('Y-m-d');
            }
            if ($data['ic_front_image_upload'] != null) {
                $data['ic_front_image_upload'] = url('/').'/' . $this->moveImage($data['ic_front_image_upload'], USER_CARRIER_PATH);
            }
            if ($data['ic_back_image_upload'] != null) {
                $data['ic_back_image_upload'] = url('/').'/' . $this->moveImage($data['ic_back_image_upload'], USER_CARRIER_PATH);
            }
            $detail = $this->customer->vSetGetDetailCustomer($id);

            $dataCustomer = [
                'bank_account_no' => strip_tags($data['bank_account_no']),
                'bank_account_name' => strip_tags($data['bank_name']),
                'birthday' => $data['birthday'],
                'gender' => $data['gender'],
                'phone2' => strip_tags($data['phone2']),
                'ic_no' => strip_tags($data['ic_no']),
                'ic_date' => strip_tags($data['ic_date']),
                'ic_place' => $data['ic_place'],
                'ic_front_image' => $data['ic_front_image_upload'] != null ? $data['ic_front_image_upload'] : $detail['ic_front_image'],
                'ic_back_image' => $data['ic_back_image_upload'] != null ? $data['ic_back_image_upload'] : $detail['ic_back_image'] ,
                'contact_province_id' => strip_tags($data['contact_province_id']),
                'contact_district_id' => strip_tags($data['contact_district_id']),
                'contact_address' =>$data['contact_address'],
                'residence_province_id' => strip_tags($data['residence_province_id']),
                'residence_district_id' => strip_tags($data['residence_district_id']),
                'residence_address' =>$data['residence_address'],
                'full_name' =>$data['full_name'],
                'is_actived' => $data['is_actived'],
//                'customer_source_id' => $data['customer_source_id'],
                'is_test' => $data['is_test'],
                'email' => $data['email'],
                'bank_id' => $data['bank_id'],
                'bank_account_branch' => $data['bank_account_branch'],
                'customer_group_id' => $data['customer_group_id'],
            ];

            if ($data['customer_group_id'] != null) {
//                Xóa id khách hàng có sẵn trong nhóm
                $deleteCustomer = $this->groupCustomerMap->deleteCustomerById($id);
//                thêm id khách hàng
                $dataCustomerGroup = [
                    'group_customer_id'=> $data['customer_group_id'],
                    'customer_id' => $id
                ];
                $this->groupCustomerMap->addInsert($dataCustomerGroup);

            }

            if ($data['is_actived'] == 0) {
                $dataCustomer['is_logout'] = 1;
            }

            // lưu thông tin nhà đầu tư
            $this->customer->edit($dataCustomer, $id);

//            // gọi api lưu data send mail
//            $vars = [
//                'username' => $detail['email'],
//            ];
//
//            $arrInsertEmail = [
//                'obj_id' => $id,
//                'email_type' => "update-customer",
//                'email_subject' => __('Chỉnh sửa thông tin nhà đầu tư'),
//                'email_to' =>  $detail['email'],
//                'email_from' => env('MAIL_USERNAME'),
//                'email_params' => json_encode($vars),
//            ];
//
//            $this->jobsEmailLogApi->addJob($arrInsertEmail);

            return [
                'error' => false,
                'message' => __('Chỉnh sửa thông tin nhà đầu tư thành công'),
                'id' => $id
            ];
        } catch (\Exception $ex) {
            return [
                'error' => true,
                'message' => __('Chỉnh sửa thông tin nhà đầu tư thất bại'),
                'mes' => $ex->getMessage(),
            ];
        }

    }

    
     /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function updateChangeRequest(array $data, $filters){
        // $filters = [];
        // $data['status'] ='success';
        // $filters['customer_id'] = $id;  $this->customer->edit($dataCustomer, $id);
        return $this->customerChange->edit($data,$filters);
    }
         /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function approveCustomerChangeRequest(array $data, $id){
        // $filters = [];
        // $data['status'] ='success';
        // $filters['customer_id'] = $id;
        if (isset($data['birthday'])){
            $data['birthday'] = Carbon::parse($data['birthday'])->format('Y-m-d');
        }

        $this->customerDevice->deleteDevice($id,['token' => null]);

       return $this->customer->edit($data, $id);
       
    }

    
     /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function editChangeRequest(array $data, $id)
    {
        try {

            if (isset($data['birthday']) && $data['birthday'] != null) {
                $data['birthday'] = Carbon::createFromFormat('d/m/Y', $data['birthday'])->format('Y-m-d');
            }
            if ($data['ic_front_image_upload'] != null) {
                $data['ic_front_image_upload'] = url('/').'/' . $this->moveImage($data['ic_front_image_upload'], USER_CARRIER_PATH);
            }
            if ($data['ic_back_image_upload'] != null) {
                $data['ic_back_image_upload'] = url('/').'/' . $this->moveImage($data['ic_back_image_upload'], USER_CARRIER_PATH);
            }
            $detail = $this->customer->vSetGetDetailCustomer($id);

            $dataCustomer = [
                'bank_account_no' => strip_tags($data['bank_account_no']),
                'bank_account_name' => strip_tags($data['bank_name']),
                'birthday' => $data['birthday'],
                'gender' => $data['gender'],
                'phone2' => strip_tags($data['phone2']),
                'ic_no' => strip_tags($data['ic_no']),
                'ic_place' => $data['ic_place'],
                'ic_front_image' => $data['ic_front_image_upload'] != null ? $data['ic_front_image_upload'] : $detail['ic_front_image'],
                'ic_back_image' => $data['ic_back_image_upload'] != null ? $data['ic_back_image_upload'] : $detail['ic_back_image'] ,
                'contact_province_id' => strip_tags($data['contact_province_id']),
                'contact_district_id' => strip_tags($data['contact_district_id']),
                'contact_address' =>$data['contact_address'],
                'residence_province_id' => strip_tags($data['residence_province_id']),
                'residence_district_id' => strip_tags($data['residence_district_id']),
                'residence_address' =>$data['residence_address'],
                'full_name' =>$data['full_name'],
                'is_actived' => $data['is_actived'],
                'customer_source_id' => $data['customer_source_id'],
                'is_test' => $data['is_test'],
                'email' => $data['email'],
                'bank_id' => $data['bank_id'],
                'bank_account_branch' => $data['bank_account_branch'],
            ];

            if ($data['is_actived'] == 0) {
                $dataCustomer['is_logout'] = 1;
            }

            // lưu thông tin nhà đầu tư
            $this->customerChange->edit($dataCustomer, $id);

//            // gọi api lưu data send mail
//            $vars = [
//                'username' => $detail['email'],
//            ];
//
//            $arrInsertEmail = [
//                'obj_id' => $id,
//                'email_type' => "update-customer",
//                'email_subject' => __('Chỉnh sửa thông tin nhà đầu tư'),
//                'email_to' =>  $detail['email'],
//                'email_from' => env('MAIL_USERNAME'),
//                'email_params' => json_encode($vars),
//            ];
//
//            $this->jobsEmailLogApi->addJob($arrInsertEmail);

            return [
                'error' => false,
                'message' => __('Chỉnh sửa thông tin nhà đầu tư thành công'),
                'id' => $id
            ];
        } catch (\Exception $ex) {
            return [
                'error' => true,
                'message' => __('Chỉnh sửa thông tin nhà đầu tư thất bại'),
                'mes' => $ex->getMessage(),
            ];
        }

    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function remove($id)
    {
        $this->customer->remove($id);
    }

    /**
     * @return array
     */
    public function getCustomerOption()
    {
        $array = array();
        foreach ($this->customer->getCustomerOption() as $item) {
            $array[$item['customer_id']] = $item['full_name'] . ' - ' . $item['phone1'];

        }
        return $array;
    }

    /**
     * @param $phone
     * @param $id
     * @return mixed
     */
    public function testPhone($phone, $id)
    {
        return $this->customer->testPhone($phone, $id);
    }

    /**
     * @param $phone
     * @return mixed
     */
    public function searchPhone($phone)
    {
        // TODO: Implement searchPhone() method.
        return $this->customer->searchPhone($phone);
    }

    /**
     * @param $phone
     * @return mixed
     */
    public function getCusPhone($phone)
    {
        // TODO: Implement getCusPhone() method.
        return $this->customer->getCusPhone($phone);
    }

    public function getCustomerIdName()
    {
        $array = array();
        foreach ($this->customer->getCustomerOption() as $item) {
            $array[$item['customer_id']] = $item['full_name'];
        }
        return $array;
    }

    /**
     * @return mixed
     */
    public function totalCustomer($yearNow)
    {

        return $this->customer->totalCustomer($yearNow);
    }

    /**
     * @param $yearNow
     * @return mixed
     */
    public function totalCustomerNow($yearNow)
    {
        return $this->customer->totalCustomerNow($yearNow);
    }

    /**
     * @param $year
     * @param $branch
     * @return mixed
     */
    public function filterCustomerYearBranch($year, $branch)
    {
        return $this->customer->filterCustomerYearBranch($year, $branch);
    }

    /**
     * @param $year
     * @param $branch
     */
    public function filterNowCustomerBranch($year, $branch)
    {
        return $this->customer->filterNowCustomerBranch($year, $branch);
    }

    /**
     * @param $time
     * @param $branch
     * @return mixed
     */
    public function filterTimeToTime($time, $branch)
    {
        return $this->customer->filterTimeToTime($time, $branch);
    }

    /**
     * @param $time
     * @param $branch
     * @return mixed|void
     */
    public function filterTimeNow($time, $branch)
    {
        return $this->customer->filterTimeNow($time, $branch);
    }

    public function searchCustomerEmail($data, $birthday, $gender)
    {
        // TODO: Implement searchCustomerEmail() method.
        return $this->customer->searchCustomerEmail($data, $birthday, $gender);
    }

    //Lấy danh sách khách hàng có ngày sinh nhật là hôm nay.
    public function getBirthdays()
    {
        return $this->customer->getBirthdays();
    }

    public function searchDashboard($keyword)
    {
        return $this->customer->searchDashboard($keyword);
    }

    /**
     * @param $id_branch
     * @param $time
     * @param $top
     * @return mixed
     */
    public function reportCustomerDebt($id_branch, $time, $top)
    {
        return $this->customer->reportCustomerDebt($id_branch, $time, $top);
    }

    /**
     * @return mixed
     */
    public function getAllCustomer()
    {
        return $this->customer->getAllCustomer();
    }
    public function getCustomerAndDefaultContact($id)
    {
        $mCustomerContact = new CustomerContactTable();
        $getItem = $this->customer->getItem($id);
        $contact = $mCustomerContact->getContactDefault($id);
        $data = [
            'getItem' => $getItem,
            'contact' => $contact,
        ];
        return $data;
    }
    

     /** V_Set get detail customer change request
     * @param int $id
     * @return mixed
     */
    public function getListCustomerContract($id){
        return $this->cContract->getContractByCustomer($id);        
    }


      /// => *** V_SET_BACKEND *** /// <=

    /** V_Set get detail customer change request
     * @param int $id
     * @return mixed
     */
    public function vSetGetDetailCustomerChange($id){        
        $detail = $this->customerChange->vSetGetDetailCustomerChange($id);
        $fullName = $this->customer->getFullNameCustomerRefer($detail['customer_refer_code']);
//        $detail['customer_refer_name'] = $fullName == null ? '' : $fullName['full_name'];
        /// vis đã dầu tư
        $invested = $this->cContract->getContractByCustomer($id);
        $totalInvested = 0;
        foreach ($invested as $value) {
            $totalInvested += $value['total'];
        }

        /// ví lãi
        $totalInterestWallet = 0;
//        $interestByTime = $this->interestByTime->interestWalletCustomer($id);
        $contract = $this->cContract->getAllContract($id);
        $total = 0;
        foreach ($contract as $item) {
            $lastInterest = $this->interestByTime->getInterestTotalInterest($item->customer_contract_id);
            if ($lastInterest != null) {
                $total += intval($lastInterest->interest_total_amount);
            }
        }
//        $totalInterestWallet = $interestByTime['interest_total_amount'];
        $totalInterestWallet = $total;

        // ví tiết kiệm
        $savingsWallet = 0;
        $saving = $this->cContract->getSavingTotalPrice($id);
        $total_withdraw = $this->withrawRequest->getWithdrawByType($id, "saving");

//        $savingsWallet = $saving['interest_total_amount'];
        $savingsWallet = $saving - $total_withdraw;

        // ví  thưởng
        $bonus = $this->cContract->getTotalCommission($id);
        $total_withdraw_bonus = $this->withdrawRequestGroup->getWithdrawByType($id, "bonus");
        $bonusWallet = $bonus - $total_withdraw_bonus;

//        foreach ($bonus as $value) {
//            $bonusWallet += $value['commission'];
//        }

//        tổng tiền đã rút
//        $withdrawRequestGroup = $this->withdrawRequestGroup->getSumWithraw($id,'bonus','done');
//        if ($withdrawRequestGroup['sum_withdraw_request_group'] != null) {
//            $bonusWallet = $bonusWallet - $withdrawRequestGroup['sum_withdraw_request_group'];
//        }

//  ví traasi phiếu
        $bondWallet = 0;
        $bond = $this->cContract->getBondTotalPrice($id);
        $bondWallet = $bond;

        return [
            'detail' => $detail,
            'invested' => $totalInvested,
            'interestWallet' => $totalInterestWallet,
            'savingsWallet' => $savingsWallet,
            'bonusWallet' => $bonusWallet,
            'bondWallet' => $bondWallet,
        ];

    }
        /// => *** V_SET_BACKEND *** /// <=

    /** V_Set get detail customer change request
     * @param int $id
     * @return mixed
     */
    public function vSetGetDetailCustomerChangeAfter($id){        
        $detail = $this->customerChange->vSetGetDetailCustomerChangeAfter($id);
        $fullName = $this->customer->getFullNameCustomerRefer($detail['customer_refer_code']);
//        $detail['customer_refer_name'] = $fullName == null ? '' : $fullName['full_name'];
        /// vis đã dầu tư
        $invested = $this->cContract->getContractByCustomer($id);
        $totalInvested = 0;
        foreach ($invested as $value) {
            $totalInvested += $value['total'];
        }

        /// ví lãi
        $totalInterestWallet = 0;
//        $interestByTime = $this->interestByTime->interestWalletCustomer($id);
        $contract = $this->cContract->getAllContract($id);
        $total = 0;
        foreach ($contract as $item) {
            $lastInterest = $this->interestByTime->getInterestTotalInterest($item->customer_contract_id);
            if ($lastInterest != null) {
                $total += intval($lastInterest->interest_total_amount);
            }
        }
//        $totalInterestWallet = $interestByTime['interest_total_amount'];
        $totalInterestWallet = $total;

        // ví tiết kiệm
        $savingsWallet = 0;
        $saving = $this->cContract->getSavingTotalPrice($id);
        $total_withdraw = $this->withrawRequest->getWithdrawByType($id, "saving");

//        $savingsWallet = $saving['interest_total_amount'];
        $savingsWallet = $saving - $total_withdraw;

        // ví  thưởng
        $bonus = $this->cContract->getTotalCommission($id);
        $total_withdraw_bonus = $this->withdrawRequestGroup->getWithdrawByType($id, "bonus");
        $bonusWallet = $bonus - $total_withdraw_bonus;

//        foreach ($bonus as $value) {
//            $bonusWallet += $value['commission'];
//        }

//        tổng tiền đã rút
//        $withdrawRequestGroup = $this->withdrawRequestGroup->getSumWithraw($id,'bonus','done');
//        if ($withdrawRequestGroup['sum_withdraw_request_group'] != null) {
//            $bonusWallet = $bonusWallet - $withdrawRequestGroup['sum_withdraw_request_group'];
//        }

//  ví traasi phiếu
        $bondWallet = 0;
        $bond = $this->cContract->getBondTotalPrice($id);
        $bondWallet = $bond;

        return [
            'detail' => $detail,
            'invested' => $totalInvested,
            'interestWallet' => $totalInterestWallet,
            'savingsWallet' => $savingsWallet,
            'bonusWallet' => $bonusWallet,
            'bondWallet' => $bondWallet,
        ];

    }

    public function getListCustomerContractChang($customer_id){
        return $this->cContract->getListContractChange($customer_id);
    }

      /// => *** V_SET_BACKEND *** /// <=

    /** V_Set get detail customer
     * @param int $id
     * @return mixed
     */
    public function vSetGetDetailCustomer($id)
    {
        if (Auth::user()['is_admin'] == 0) {
            $listCustomer = $this->getListCustomer();
            if (count($listCustomer) != 0) {
                if (isset($listCustomer['type']) && $listCustomer['type'] != 'all') {
                    if (!in_array($id,collect($listCustomer['arr'])->toArray())) {

                        return null;
                    }
                }
            }
        }

        $detail = $this->customer->vSetGetDetailCustomer($id);
        $listCustomer = $this->getListCustomer();
        if (count($listCustomer) != 0) {
            if (isset($listCustomer['type']) && $listCustomer['type'] != 'all') {
                $filters['listCustomer'] = $listCustomer['arr'];
            }
        }


        $fullName = $this->customer->getFullNameCustomerRefer($detail['customer_refer_code']);
        $detail['customer_refer_name'] = $fullName == null ? '' : $fullName['full_name'];
        /// vis đã dầu tư
        $invested = $this->cContract->getContractByCustomer($id);
        $totalInvested = 0;
        foreach ($invested as $value) {
            $totalInvested += $value['total'];
        }

        /// ví lãi
        $totalInterestWallet = 0;
//        $interestByTime = $this->interestByTime->interestWalletCustomer($id);
        $contract = $this->cContract->getAllContract($id);
        $total = 0;
        foreach ($contract as $item) {
            $lastInterest = $this->interestByTime->getInterestTotalInterest($item->customer_contract_id);
            if ($lastInterest != null) {
                $total += intval($lastInterest->interest_total_amount);
            }
        }
//        $totalInterestWallet = $interestByTime['interest_total_amount'];
        $totalInterestWallet = $total;

        // ví tiết kiệm
        $savingsWallet = 0;
        $saving = $this->cContract->getSavingTotalPrice($id);
        $total_withdraw = $this->withrawRequest->getWithdrawByType($id, "saving");

//        $savingsWallet = $saving['interest_total_amount'];
        $savingsWallet = $saving - $total_withdraw;

        // ví  thưởng
//        $bonus = $this->cContract->getTotalCommission($id);
        $bonus = $this->wallet->totalBonusCustomer($id);
        if ($bonus == null) {
            $bonus = 0;
        } else {
            $bonus = $bonus['total_commission'];
        }
        $total_withdraw_bonus = $this->withdrawRequestGroup->getWithdrawByType($id, "bonus");
        $bonusWallet = $bonus - $total_withdraw_bonus;

//        foreach ($bonus as $value) {
//            $bonusWallet += $value['commission'];
//        }

//        tổng tiền đã rút
//        $withdrawRequestGroup = $this->withdrawRequestGroup->getSumWithraw($id,'bonus','done');
//        if ($withdrawRequestGroup['sum_withdraw_request_group'] != null) {
//            $bonusWallet = $bonusWallet - $withdrawRequestGroup['sum_withdraw_request_group'];
//        }

//  ví traasi phiếu
        $bondWallet = 0;
        $bond = $this->cContract->getBondTotalPrice($id);
        $bondWallet = $bond;

        return [
            'detail' => $detail,
            'invested' => $totalInvested,
            'interestWallet' => $totalInterestWallet,
            'savingsWallet' => $savingsWallet,
            'bonusWallet' => $bonusWallet,
            'bondWallet' => $bondWallet,
        ];
    }

    public function operationHistory(array $filter = [])
    {
        return $this->operationHistory->getDataTable($filter);
    }
    public function operationStockHistory(array $filter = [])
    {
        return $this->operationStockHistory->getDatatable($filter);
    }

    public function listContractCustomer(array $filter = [])
    {
        return $this->cContract->getDataTable($filter);
    }

    public function listIntroduct(array $filter = [])
    {
        return $this->customer->getDataTable($filter);
    }

    public function listContractCommission(array $filter = [])
    {
//        return $this->cContract->getDataTable($filter);
        return $this->wallet->getDataTable($filter);
    }

    public function listProductView(array $filter = [])
    {
        $list = $this->potentialCustomerLog->getDataTable($filter);
        if (count($list) == 0) {
            return $list;
        }

        foreach ($list['data']  as $item){
            if ($item['type'] == 'product'){
                $name = $this->product->getDetail($item['obj_id']);
                $item['name'] = $name['product_name_vi'];
                $item['category'] = $name['category_name_vi'];
            } else {
                $name = $this->service->getItem($item['obj_id']);
                $item['name'] = $name['service_name'];
            }
        }

        return $list;
    }

    public function listCustomerSearch(array $filter = [])
    {
        $list = $this->customerSearchLog->getDataTable($filter);
        if (count($list) == 0) {
            return $list;
        }

        foreach ($list['data']  as $item){
            if ($item['type'] == 'product'){
                $name = $this->product->getDetail($item['obj_id']);
                $item['name'] = $name['product_name_vi'];
                $item['category'] = $name['category_name_vi'];
            } else {
                $name = $this->service->getItem($item['obj_id']);
                $item['name'] = $name['service_name'];
            }
        }

        return $list;
    }

    /**
     * Move ảnh từ folder temp sang folder chính
     *
     * @param $filename
     * @param $PATH
     * @return mixed|string
     */
    public function moveImage($filename, $PATH)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = $PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory($PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    public function changeStatus(array $data, $id)
    {
//        var_dump($data, $id); die;
        try {
            $dataCustomer = [
                'is_actived' => $data['is_active']
            ];
            // lưu thông tin nhà đầu tư
            $this->customer->edit($dataCustomer, $id);
            return [
                'error' => false,
                'message' => __('Thay đổi trạng thái thành công'),
                'id' => $id
            ];
        } catch (\Exception $ex) {
            return [
                'error' => true,
                'message' => __('Thay đổi trạng thái thất bại'),
                'mes' => $ex->getMessage(),
            ];
        }
    }

    public function submitResetPassword(array  $data, $id)
    {

        try {
            DB::beginTransaction();
            $pass = isset($data['password']) ? $data['password'] : '12345678';

            $dataCustomer = [
                'password' => bcrypt($pass)
            ];
            // lưu thông tin nhà đầu tư

            $this->customer->edit($dataCustomer, $id);

            $detail = $this->customer->vSetGetDetailCustomer($id);

            // lưu thông tin nhà đầu tư
//            $this->customer->edit($dataCustomer, $id);

            // gọi api lưu data send mail
            $vars = [
                'customer_name' => $detail['full_name'],
                'password' => $pass,
            ];

            $arrInsertEmail = [
                'obj_id' => $id,
                'email_type' => "update-customer",
                'email_subject' => __('Chỉnh sửa thông tin nhà đầu tư'),
                'email_to' =>  $detail['email'],
                'email_from' => env('MAIL_USERNAME'),
                'email_params' => json_encode($vars),
            ];

            $this->jobsEmailLogApi->addJob($arrInsertEmail);
            DB::commit();
            return [
                'error' => false,
                'message' => __('Reset mật khẩu thành công'),
                'id' => $id
            ];
        } catch (\Exception $ex) {
            DB::rollBack();
            return [
                'error' => true,
                'message' => __('Reset mật khẩu thất bại'),
                'mes' => $ex->getMessage(),
            ];
        }
    }

    public function getListReferCustomer($customer_refer_id)
    {
        return $this->customer->getListReferCustomer($customer_refer_id);
    }

    public function getListCategoryService()
    {
        return $this->service->getAllService();
    }

    public function getCustomerSource()
    {
        return $this->customerSource->getListCustomerSource();
    }

    public function getListContractChange($id)
    {
        return $this->appendixContract->getListContract($id);
    }

    public function getListAppendix($id)
    {
        $list = $this->appendixContractMap->getAllFile($id);
        if (count($list) != 0) {
            $list = collect($list)->groupBy('appendix_contract_id');
            return $list;
        } else {
            return [];
        }
    }

    public function cancelChangeRequest($input)
    {
        $update = $this->customerChange->cancelChangeRequest($input['customer_change_id'],strip_tags($input['reason_vi']),strip_tags($input['reason_en']));
        return [
            'error' => false,
            'message' => 'Từ chối yêu cầu thành công'
        ];
    }

    public function getCustomerJourney()
    {
//        return $this->customerJourney->getAll();
        return $this->groupCustomer->getAll();
    }

    public function updateCustomerLogout($id)
    {
        return $this->customer->edit(['is_logout' => 1], $id);
    }
    // todo: Stock Customer------
    public function getStockByCustomer($id){          
        $stockCustomer =  $this->stockCustomer->getStockByCustomer($id);
        if(!is_null($stockCustomer)){
            $stockCustomer['wallet_stock_money']=is_null($stockCustomer['wallet_stock_money'])?0:$stockCustomer['wallet_stock_money'];
            $stockCustomer['wallet_dividend_money']=is_null($stockCustomer['wallet_dividend_money'])?0:$stockCustomer['wallet_dividend_money'];

        }
        return $stockCustomer;

    }

    public function checkContractById($customerId)
    {
        $oSelect = $this->cContract->checkContractByCustomerId($customerId);
        return $oSelect;
    }

    public function changeStatusTest($param)
    {
        try {
            if ($param['type'] == 'status') {
                $this->customer->updateCustomer($param['customer_id'],['is_actived' => $param['check']]);
            } else if ($param['type'] == 'test') {
                $this->customer->updateCustomer($param['customer_id'],['is_test' => $param['check']]);
            }

            return [
                'error' => false,
                'message' => $param['type'] == 'status' ? 'Cập nhật trạng thái thành công' : 'Cập nhật tài khoản test thành công'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => $param['type'] == 'status' ? 'Cập nhật trạng thái thất bại' : 'Cập nhật tài khoản test thất bại'
            ];
        }
    }
}