<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/19/2021
 * Time: 8:48 AM
 * @author nhandt
 */

namespace Modules\Admin\Repositories\StockRevenue;


interface ReportStockRevenueRepoInterface
{
    public function filter($params = []);
}