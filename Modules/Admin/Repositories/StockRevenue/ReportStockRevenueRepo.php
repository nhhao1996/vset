<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/19/2021
 * Time: 8:49 AM
 * @author nhandt
 */


namespace Modules\Admin\Repositories\StockRevenue;


use Carbon\Carbon;
use Modules\Admin\Models\PaymentMethodTable;
use Modules\Admin\Models\StockOrderTable;

class ReportStockRevenueRepo implements ReportStockRevenueRepoInterface
{
    private $paymentMethod;
    private $stockOrder;

    const arrPaymentMethod = [
        '1' => 'Chuyển khoản',
        '2' => 'Ví Lãi',
        '3' => 'Ví Thưởng',
        '4' => 'Tiền mặt',
        '5' => 'Ví Cổ phiếu',
        '6' => 'Ví cổ tức',
    ];
    public function __construct(PaymentMethodTable $paymentMethod,StockOrderTable $stockOrder)
    {
        $this->paymentMethod=$paymentMethod;
        $this->stockOrder = $stockOrder;
    }
    public function filter($params = [])
    {
         $type_chart = $params['type_chart'];
         $filter_type = $params['filter_type'];
         $time = $params['time'];
         if($filter_type == 'day'){
             $dateTime = explode(" - ", $time);
             $startTime = Carbon::createFromFormat('d/m/Y', $dateTime[0])
                 ->format('Y-m-d');
             $endTime = Carbon::createFromFormat('d/m/Y', $dateTime[1])
                 ->format('Y-m-d');
             $params['created_at'] = [$startTime . ' 00:00:00', $endTime.' 23:59:59'];

             $filter = [
                 'type_chart' => $type_chart,
                 'filter_type' => $filter_type,
                 'startTime' => $startTime,
                 'endTime' => $endTime
             ];
             $data = $this->paymentMethod->getDataStockChart($filter);
             $seriesData = [];
             $cateDate = [];
             $this->formatDataColumnChartByDay($data,$endTime,$startTime,$seriesData,$cateDate,$type_chart);

             $paymentMethodChart = $this->paymentMethod->getPaymentMethodChart($filter);
             $dataPaymentMethodChart = $this->formatDataPaymentMethodChart($paymentMethodChart);
//             dd($data,$paymentMethodChart);

             $description = $this->stockOrder->getDataDescriptionOfStockReport($filter);
             return [
                 'cate' => $cateDate,
                 'chart' => $seriesData,
                 'text_description' => $description,
                 'payment_method_chart' => $dataPaymentMethodChart,
             ];
         }
         else{
             $month_start = isset($params['month_start']) ? $params['month_start'] : null;
             $month_end = isset($params['month_end']) ? $params['month_end'] : null;
             //Dữ liệu từ db
             $filter = [
                 'type_chart' => $type_chart,
                 'filter_type' => $filter_type,
                 'month_start' => $month_start == null ? null : Carbon::createFromFormat('m/Y',$month_start)->setTimezone('UTC')->startOfMonth()->format('Y/m/d 00:00:00'),
                 'month_end' => $month_end == null ? null : Carbon::createFromFormat('m/Y',$month_end)->setTimezone('UTC')->endOfMonth()->format('Y/m/d 23:59:59'),
             ];
             $data = $this->paymentMethod->getDataStockChart($filter);
             $seriesData = [];
             $cateDate = [];
             $this->formatDataColumnChartByMonth($data,$month_end, $month_start,$seriesData,$cateDate,$type_chart);

             $paymentMethodChart = $this->paymentMethod->getPaymentMethodChartMonth($filter);
             $dataPaymentMethodChart = $this->formatDataPaymentMethodChart($paymentMethodChart);


             $description = $this->stockOrder->getDataDescriptionOfStockReport($filter);
             return [
                 'cate' => $cateDate,
                 'chart' => $seriesData,
                 'text_description' => $description,
                 'payment_method_chart' => $dataPaymentMethodChart,
             ];
         }
    }

    /**
     * Format data chart pie payment method chart
     *
     * @param $dataPaymentMethodQuery
     * @return array
     */
    protected function formatDataPaymentMethodChart($dataPaymentMethodQuery){
        $dataPaymentMethodChart = [];
        foreach ($dataPaymentMethodQuery as $value) {
            $dataPaymentMethodChart[] = array("name"=>$value['payment_method_name'], "y"=>(float)$value['total_revenue']);
        }
        return $dataPaymentMethodChart;
    }

    /**
     * Format data của chart column vs dữ liệu theo từng ngày được filter
     *
     * @param $data
     * @param $endTime
     * @param $startTime
     * @param $seriesData
     * @param $cateDate
     * @param $type_chart
     */
    protected function formatDataColumnChartByDay($data, $endTime, $startTime, &$seriesData, &$cateDate, $type_chart){
        $dateDiff = ((strtotime($endTime) - strtotime($startTime)) / (60 * 60 * 24)) + 1;
        for($j = 1; $j < 7; $j++){
            $labelData = [];
            $labelName1 = "";
            $checkAdded = 0;
            for ($i = 0; $i < $dateDiff; $i++) {
                $checkAdded = 0;
                $tomorrow = date('d/m/Y', strtotime($startTime . "+" . $i . " days"));
                if(!in_array($tomorrow,$cateDate)){
                    array_push($cateDate,$tomorrow);
                }
                foreach ($data as $item) {
                    if($item['payment_method_id'] == $j){
                        $checkAdded = 2;
                        if($item['date'] == $tomorrow){
                            $labelName1 = $item['payment_method_name'];
                            if($type_chart != 'count-transaction'){
                                array_push($labelData,(float)$item['total_revenue']);
                            }
                            else{

                                array_push($labelData,$item['total_revenue']);
                            }
                            $checkAdded = 1;
                            break;
                        }
                    }
                }
                if($checkAdded == 2){
                    $labelName1 = self::arrPaymentMethod[$j];
                    if($type_chart != 'count-transaction'){
                        array_push($labelData,(float)0);
                    }
                    else{

                        array_push($labelData,0);
                    }
                }
            }
            if($checkAdded != 0){
                $seriesData[] = array('name'=>$labelName1,'data'=>$labelData);
            }
        }
    }

    protected function formatDataColumnChartByMonth($data, $endTime, $startTime, &$seriesData, &$cateDate, $type_chart){
//        dd($data, $endTime, $startTime, $seriesData, $cateDate, $type_chart);
        $year1 = Carbon::createFromFormat('m/Y',$startTime)->format('Y');
        $year2 = Carbon::createFromFormat('m/Y',$endTime)->format('Y');

        $month1 = Carbon::createFromFormat('m/Y',$startTime)->format('m');
        $month2 = Carbon::createFromFormat('m/Y',$endTime)->format('m');

        $monthDiff = (($year2 - $year1) * 12) + ($month2 - $month1) + 1;
        for($j = 1; $j < 7; $j++){
            $labelData = [];
            $labelName1 = "";
            $checkAdded = 0;
            for ($i = 0; $i < $monthDiff; $i++) {
                $checkAdded = 0;
                $tomorrow = Carbon::createFromFormat('m/Y',$startTime)->addMonths($i)->format('m/Y');
                if(!in_array($tomorrow,$cateDate)){
                    array_push($cateDate,$tomorrow);
                }
                foreach ($data as $item) {
                    if($item['payment_method_id'] == $j){
                        $checkAdded = 2;
                        if($item['date'] == $tomorrow){
                            $labelName1 = $item['payment_method_name'];
                            if($type_chart != 'count-transaction'){
                                array_push($labelData,(float)$item['total_revenue']);
                            }
                            else{

                                array_push($labelData,$item['total_revenue']);
                            }
                            $checkAdded = 1;
                            break;
                        }
                    }
                }
                if($checkAdded == 2){
                    $labelName1 = self::arrPaymentMethod[$j];
                    if($type_chart != 'count-transaction'){
                        array_push($labelData,(float)0);
                    }
                    else{

                        array_push($labelData,0);
                    }
                }
            }
            if($checkAdded != 0){
                $seriesData[] = array('name'=>$labelName1,'data'=>$labelData);
            }
        }
    }
}