<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\WithdrawRequest;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\ConfigTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerContractInterestByDateTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockWalletTable;
use Modules\Admin\Models\WalletTable;
use Modules\Admin\Models\WarehouseTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepository;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;
use Modules\Admin\Repositories\Message\MessageRepoInterface;
use Modules\Admin\Repositories\WithdrawRequest\WithdrawRequestRepositoryInterface;
use Modules\Admin\Http\Api\JobsEmailLogApi;

class WithdrawRequestRepository implements WithdrawRequestRepositoryInterface
{

    protected $withdrawRequest;
    protected $withdrawRequestGroup;
    protected $CustomerRequest;
    protected $contractInterestMonth;
    protected $contractInterestDate;
    protected $contractInterestTime;
    protected $jobMail;
    protected $customerContract;
    protected $wallet;
    protected $api;
    protected $rCustomerContractInterest;
    protected $stockCustomer;
    protected $stockWallet;
    protected $messageRepo;

    public function __construct(
        WithdrawRequestTable $withdrawRequest,
        CustomerTable $CustomerRequest,
        WithdrawRequestGroupTable $withdrawRequestGroup,
        CustomerContractInterestByMonthTable $contractInterestMonth,
        CustomerContractInterestByDateTable $contractInterestDate,
        JobsEmailLogApi $jobMail,
        CustomerContactTable $customerContract,
        CustomerContractInterestByTimetable $contractInterestTime,
        WalletTable $wallet,
        JobsEmailLogApi $api,
        CustomerContractInterestRepositoryInterface $rCustomerContractInterest,
        StockCustomerTable $stockCustomer,
        StockWalletTable $stockWallet,
    MessageRepoInterface $messageRepo

    )
    {
        $this->withdrawRequest = $withdrawRequest;
        $this->withdrawRequestGroup = $withdrawRequestGroup;
        $this->CustomerRequest = $CustomerRequest;
        $this->contractInterestMonth = $contractInterestMonth;
        $this->contractInterestDate = $contractInterestDate;
        $this->jobMail = $jobMail;
        $this->customerContract = $customerContract;
        $this->contractInterestTime = $contractInterestTime;
        $this->wallet = $wallet;
        $this->api = $api;
        $this->rCustomerContractInterest = $rCustomerContractInterest;
        $this->stockCustomer = $stockCustomer;
        $this->stockWallet = $stockWallet;
        $this->messageRepo = $messageRepo;
    }

//    Hàm lấy tất cả trong yêu cầu rút tiền withdraw_request
    public function listAll(array $filters = [])
    {
        $filters['all'] = 'all';
        return $this->withdrawRequest->getList($filters);
    }

    //Hàm lấy danh sách trái phiếu / tiết kiệm
    public function list(array $filters = [])
    {
//        $filters['withdraw_request_type'] = 'bond_saving';
        return $this->withdrawRequest->getList($filters);
    }

    //Hàm lấy danh sách lãi xuất / tiền thưởng
    public function listCommission(array $filters = [])
    {
//        $filters['withdraw_request_type'] = 'interest_commission';
        return $this->withdrawRequestGroup->getList($filters);
    }

    //Hàm lấy danh sách rút tiền ví cổ phiếu / cổ tức
    public function listStock(array $filters = [])
    {
        $filters['withdraw_request_group_type'] = ['stock','dividend'];
        return $this->withdrawRequestGroup->getList($filters);
    }

    public function show($id){
        return $this->withdrawRequest->getById($id);
    }

    public function getListInterest($customer_contract_id)
    {
        return $this->contractInterestDate->sumContractStatus($customer_contract_id);
    }

    public function getContract($customer_contract_id)
    {
        return $this->customerContract->getDetailContract($customer_contract_id);
    }

    public function getInterestByMonth($customer_contract_id)
    {
        return $this->contractInterestMonth->getInterestByMonth($customer_contract_id);
    }

    public function getListWithdrawGroup($customer_id,$type)
    {
        return $this->withdrawRequestGroup->getWithdrawByType($customer_id,$type);
    }

    public function totalBonusCustomer($customer_id)
    {
        return $this->customerContract->totalBonusCustomer($customer_id);
    }

    public function totalBonusCustomerWallet($customer_id)
    {
        $type = ['bonus','register','refer'];
        return $this->wallet->totalBonusCustomer($customer_id,$type);
    }

    public function showGroup($id){
        return $this->withdrawRequestGroup->getById($id);
    }

    public function getListCustomer()
    {
        $data = $this->CustomerRequest->getAllCustomerWithdraw();
        return $data;
    }

    public function totalWithdrawGroup($customer_id,$type)
    {
        return $this->withdrawRequestGroup->getWithdrawByType($customer_id,$type);
    }

    public function totalInterestMonth($customer_contract_id)
    {
        return $this->contractInterestMonth->totalBonusCustomer($customer_contract_id);
    }

    public function totalMoneyWithdrawGroup($customer_contract_id,$type)
    {
        return $this->withdrawRequestGroup->getWithdrawByGroupCustomerIdObj($customer_contract_id,$type);
    }

    public function totalMoneyWithdrawGroupFix($customer_contract_id,$type)
    {
        return $this->withdrawRequest->getWithdrawByGroupCustomerIdObjFix($customer_contract_id,$type);
    }

    public function getTotalRequest($customer_contract_id,$arrType = null)
    {
        return $this->withdrawRequest->totalRequestDone($customer_contract_id,$arrType);
    }

    public function getWithdrawDatePlaning($customer_contract_id)
    {
        return $this->contractInterestMonth->getInterestByMonth($customer_contract_id);
    }

    public function getListWithdrawRequest($filter)
    {
//        $filter['withdraw_request_status'] = 'done';
        $list = $this->withdrawRequest->getListAll($filter);
        $view = view(
            'admin::withdraw-request.table.withdraw-request',
            [
                'list' => $list,
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function getListCustomerContractInterestMonth($filter)
    {
        $list = $this->contractInterestMonth->getListAll($filter);
        $view = view(
            'admin::withdraw-request.table.customer-contract-interest-month',
            [
                'list' => $list,
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function getListCustomerContractInterestDate($filter)
    {
        $list = $this->contractInterestDate->getListAll($filter);
        $view = view(
            'admin::withdraw-request.table.customer-contract-interest-date',
            [
                'list' => $list,
            ]
        )->render();
        return response()->json([
            'error' => false,
            'view' => $view,
        ]);
    }

    public function changeStatus($param)
    {
        try {
            DB::beginTransaction();
            $id = $param['withdraw_request_id'];
            unset($param['withdraw_request_id']);
            // gọi api lưu data send mail
            // get detail order
            $detail = $this->show($id);

//            Kiểm tra yêu cầu rút gốc đã được confirm
            $checkChangeStatus = $this->withdrawRequest->getWithdrawByContract($id,$detail['customer_contract_id'],$detail['withdraw_request_type']);
            if (count($checkChangeStatus) != 0) {
                return [
                    'error' => true,
                    'message' => 'Yêu cầu đã được xác nhận'
                ];
            }

            $contract = $this->getContract($detail['customer_contract_id']);
            $totalWithdraw = $this->getTotalRequest($detail['customer_contract_id'],['bond','saving']);
            $totalWithdraw['total_withdraw_request_amount'] = $totalWithdraw['total_withdraw_request_amount'] == null ? 0 : $totalWithdraw['total_withdraw_request_amount'];
            $total = $contract['total_amount'] - $totalWithdraw['total_withdraw_request_amount'];
            if ($param['withdraw_request_status'] == 'inprocess' || $param['withdraw_request_status'] == 'done') {
                if ($detail['contract_is_active'] == 0 || $detail['contract_is_deleted'] == 1 || $total < $detail['withdraw_request_amount']){
                    if ($param['withdraw_request_status'] == 'inprocess') {
                        return [
                            'error' => true,
                            'message' => 'Không đủ tiền để xác nhận yêu cầu hoặc hợp đồng đã được gia hạn'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Không đủ tiền để hoàn thành yêu cầu hoặc hợp đồng đã được gia hạn'
                        ];
                    }
                }
            }

//            if ($param['withdraw_request_status'] == 'inprocess' && $detail['withdraw_request_status'] != 'new') {
//                return [
//                    'error' => true,
//                    'message' => 'Yêu cầu đã được xác nhận'
//                ];
//            }

//            if ($param['withdraw_request_status'] == 'done' && $detail['withdraw_request_status'] != 'inprocess') {
            if ($param['withdraw_request_status'] == 'done' && $detail['withdraw_request_status'] != 'new') {
                return [
                    'error' => true,
                    'message' => 'Yêu cầu đã được hoàn thành'
                ];
            }

            // get detail customer
            $mCustomer = new CustomerTable();
            $mCustomer  = $mCustomer->getItem($detail['customer_id']);

            $detailRequest = $this->withdrawRequest->getById($id);
            $withdrawGroupDetail = $this->withdrawRequestGroup->getById($detailRequest['withdraw_request_group_id']);

            $checkMinTime = Carbon::parse($detailRequest['customer_contract_start_date'])->addMonth($detailRequest['withdraw_min_time']);
            $checkInvestmentTime = Carbon::parse($detailRequest['customer_contract_start_date'])->addMonth($detailRequest['investment_time']);

            $updateStatus = $this->withdrawRequest->changeStatus($id,$param);
            $updateStatusGroup = $this->withdrawRequestGroup->changeStatus($detailRequest['withdraw_request_group_id'],$param);
            $message = '';
            $subject = '';
            $statusMail = '';
            $key = null;
            if ($param['withdraw_request_status'] == 'done') {
                $message = 'Hoàn thành yêu cầu rút tiền';
                $subject = 'Hoàn thành yêu cầu rút tiền';
                $statusMail = 'Hoàn thành';

                $this->customerContract->updateContract($detailRequest['customer_contract_id'],['is_active' => 0]);

                $key = $detailRequest['withdraw_request_type'] == 'bond' ? 'withdraw_bond_status_D' : 'withdraw_saving_status_D';
            } else if ($param['withdraw_request_status'] == 'inprocess') {
                $message = 'Xác nhận yêu cầu rút tiền thành công';
                $subject = 'Xác nhận yêu cầu rút tiền';
                $statusMail = 'Đang xử lý';
                $key = $detailRequest['withdraw_request_type'] == 'bond' ? 'withdraw_bond_status_A' : 'withdraw_saving_status_A';
                $dateKey = '';
                $request_datetime = Carbon::parse($detailRequest['withdraw_request_year'].'-'.$detailRequest['withdraw_request_month'].'-'.$detailRequest['withdraw_request_day'].' '.$detailRequest['withdraw_request_time']);
                if ($checkInvestmentTime > $request_datetime){
                    $dateKey = 'withdraw_saving_before';
                } else {
                    $dateKey = 'withdraw_saving_ok';
                }

                if ($detailRequest['withdraw_request_type'] == 'bond'){
                    $dateKey = 'withdraw_saving_ok';
                }

//                Lấy số ngày có thể rút tiền
                $config = new ConfigTable();
                $date_planning = $config->getInfoByKey($dateKey);
                $arrUpdate = [
                    'withdraw_date_confirm' => Carbon::now(),
                    'payment_date_planing' => Carbon::now()->addDay((int)$date_planning['value'])->format('Y-m-d')
                ];
                $this->withdrawRequest->changeStatus($id,$arrUpdate);
                $this->withdrawRequestGroup->changeStatus($detailRequest['withdraw_request_group_id'],$arrUpdate);
                $withdrawGroupDetail = $this->withdrawRequestGroup->getById($detailRequest['withdraw_request_group_id']);
                $sendSMS = [
                    'phone' => $detailRequest['customer_phone'],
                    'type' => 'confirm_withraw',
                    'withdraw_request_group_code' => $detailRequest['withdraw_request_group_code']
                ];
                DB::commit();
                $this->sendSMS($sendSMS);
            } else {
                $message = 'Không xác nhận yêu cầu rút tiền thành công';
                $subject = 'Không xác nhận yêu cầu rút tiền';
                $statusMail = 'Hủy';
                $key = $detailRequest['withdraw_request_type'] == 'bond' ? 'withdraw_bond_status_C' : 'withdraw_saving_status_C';
            }
            DB::commit();
            $this->sendNotificationAction($key,$detail['customer_id'],$id);
              // TODO: push noti---------------------------------

  //                end: push noti--------------------------
            $test = $this->addMail($id,$withdrawGroupDetail,$param,$mCustomer,$statusMail,$subject);
            return [
                'error' => false,
                'message' => $message
            ];
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Cập nhật thật bại'
            ];
        }
    }

    public function sendSMS($data) {
        $data['message'] = 'VSETGROUP kinh gui quy khach hang xac nhan thanh cong lenh rut tien cua quy khach so giao dich la '.$data['withdraw_request_group_code'];
        unset($data['withdraw_request_group_code']);
        $customerInterestRepo = new CustomerContractInterestRepository();
        $customerInterestRepo->sendSMS($data);
    }

    public function sendNotificationAction($key,$customer,$object_id){
        $noti = [
            'key' => $key,
            'customer_id' => (int)$customer,
            'object_id' => (int)$object_id
        ];
        $this->jobMail->sendNotification($noti);
        return true;
    }

    public function addMail($id,$detail,$param,$mCustomer,$statusMail,$subject) {
        ///////  gọi api lưu data send mail
        $vars = [
            'request_code'      =>  $detail['withdraw_request_group_code'],
            'status'      =>  $statusMail,
            'customer_name' => $mCustomer['full_name']
        ];

        $arrInsertEmail = [
            'obj_id' => $id,
            'email_type' => "confirm-withdraw",
            'email_subject' => $subject,
            'email_from' => env('MAIL_USERNAME'),
            'email_to' =>  $mCustomer['email'],
            'email_params' => json_encode($vars),
        ];
        $this->jobMail->addJob($arrInsertEmail);
        return true;
    }

    public function addMailGroup($id,$detail,$param,$mCustomer,$statusMail,$subject) {
        ///////  gọi api lưu data send mail
        $vars = [
            'request_code'      =>  $detail['withdraw_request_group_code'],
            'status'      =>  $statusMail,
            'customer_name' => $mCustomer['full_name']
        ];

        $arrInsertEmail = [
            'obj_id' => $id,
            'email_type' => "confirm-withdraw",
            'email_subject' => $subject,
            'email_from' => env('MAIL_USERNAME'),
            'email_to' =>  $mCustomer['email'],
            'email_params' => json_encode($vars),
        ];
        $this->jobMail->addJob($arrInsertEmail);
        return true;
    }

    public function changeStatusGroup($param)
    {

        $id = $param['withdraw_request_group_id'];
        unset($param['withdraw_request_group_id']);
        $data = $this->showGroup($id);
        $mCustomer = new CustomerTable();
        $mCustomer  = $mCustomer->getItem($data['customer_id']);
        $statusMail = '';
        if ($param['withdraw_request_status'] == 'inprocess' || $param['withdraw_request_status'] == 'done') {
//        kiểm tra giá tiền trước khi chuyển trạng thái
            $totalBonus = 0;
            if ($data['withdraw_request_group_type'] == 'bonus'){
//                $totalContract = $this->totalBonusCustomer($data['customer_id']);
                $totalContract = $this->totalBonusCustomerWallet($data['customer_id']);
                $totalWithdrawGroup = $this->totalWithdrawGroup($data['customer_id'],$data['withdraw_request_group_type']);
                $totalBonus = $totalContract['total_commission'] - $totalWithdrawGroup;

            } elseif($data['withdraw_request_group_type'] == 'interest') {
                $totalInterestMonth = $this->totalInterestMonth($data['customer_contract_id']);
                $totalWithdrawGroup = $this->totalMoneyWithdrawGroupFix($data['customer_contract_id'],'interest');
                $totalBonus = $totalInterestMonth['interest_banlance_amount'] - $totalWithdrawGroup;
                $withdraw_date_planing = $this->getWithdrawDatePlaning($data['customer_contract_id']);
            }

            if ((double)$totalBonus < (double)$data['withdraw_request_amount']) {
                if ($param['withdraw_request_status'] == 'inprocess') {
                    return [
                        'error' => true,
                        'message' => 'Không đủ tiền để xác nhận yêu cầu hoặc hợp đồng đã được gia hạn'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Không đủ tiền để hoàn thành yêu cầu hoặc hợp đồng đã được gia hạn'
                    ];
                }
            }

            if ($param['withdraw_request_status'] == 'inprocess') {
                $sendSMS = [
                    'phone' => $data['customer_phone'],
                    'type' => 'confirm_withraw',
                    'withdraw_request_group_code' => $data['withdraw_request_group_code']
                ];
                $this->sendSMS($sendSMS);
            }
        }

        $updateStatus = $this->withdrawRequestGroup->changeStatus($id,$param);
        $updateStatusChild = $this->withdrawRequest->changeStatusByWithdrawGroup($id,$param);
        $message = '';
        $statusMail = '';
        $subject = '';
        $detailRequest = $this->withdrawRequestGroup->getDetailById($id);

        $idRequest = $this->withdrawRequest->getListIdRequestByGroup($id);

        if ($param['withdraw_request_status'] == 'done') {
            $message = 'Hoàn thành yêu cầu rút tiền';
            $subject = 'Hoàn thành yêu cầu rút tiền';
            $statusMail = 'Hoàn thành';
            if ($detailRequest['withdraw_request_group_type'] == 'bonus') {
                $this->sendNotificationAction('withdraw_commission_status_D',$detailRequest['customer_id'],$id);
            } else if ($detailRequest['withdraw_request_group_type'] == 'interest'){
                foreach ($idRequest as $value) {

                    $checkContract = $this->customerContract->checkContract($value['customer_contract_id']);
                    if ($checkContract == null || Carbon::parse($checkContract['customer_contract_end_date']) < Carbon::now()) {
                        $this->rCustomerContractInterest->calculateContract('time',[$value['customer_contract_id']]);
                    }

                    $getContract = $this->customerContract->getDetail($value['customer_contract_id']);
//                    if ($getContract['is_deleted'] == 1 && $getContract['is_active'] == 1) {
//                        $this->customerContract->updateContract($value['customer_contract_id'],['is_active' => 0]);
//                    }
                    $this->sendNotificationAction('withdraw_interest_status_D',$detailRequest['customer_id'],$value['withdraw_request_id']);
                }
            }
        }  else if ($param['withdraw_request_status'] == 'inprocess') {
            $message = 'Xác nhận yêu cầu rút tiền thành công';
            $subject = 'Xác nhận yêu cầu rút tiền';
            $statusMail = 'Xác nhận';
            $config = new ConfigTable();
            if ($detailRequest['withdraw_request_group_type'] == 'bonus') {
                $this->sendNotificationAction('withdraw_commission_status_A',$detailRequest['customer_id'],$id);
                $dateKey = $config->getInfoByKey('withdraw_bonus');
                $arrUpdate = [
                    'withdraw_date_confirm' => Carbon::now(),
                    'payment_date_planing' => Carbon::now()->addDay((int)$dateKey['value'])->format('Y-m-d')
                ];
                $this->withdrawRequestGroup->changeStatus($id,$arrUpdate);
            } else if ($detailRequest['withdraw_request_group_type'] == 'interest'){
                $dateKey = $config->getInfoByKey('withdraw_interest');
                $arrUpdate = [
                    'withdraw_date_confirm' => Carbon::now(),
                    'payment_date_planing' => Carbon::now()->addDay((int)$dateKey['value'])->format('Y-m-d')
                ];
                $this->withdrawRequestGroup->changeStatus($id,$arrUpdate);
                foreach ($idRequest as $value) {
                    $this->withdrawRequest->changeStatus($value['withdraw_request_id'],$arrUpdate);
                    $this->sendNotificationAction('withdraw_interest_status_A',$detailRequest['customer_id'],$value['withdraw_request_id']);
                }
            }
        } else {
            $message = 'Không xác nhận yêu cầu rút tiền thành công';
            $subject = 'Không xác nhận yêu cầu rút tiền';
            $statusMail = 'Hủy';
            if ($detailRequest['withdraw_request_group_type'] == 'bonus') {
                $this->sendNotificationAction('withdraw_commission_status_C',$detailRequest['customer_id'],$id);
            } else if ($detailRequest['withdraw_request_group_type'] == 'interest'){
                foreach ($idRequest as $value){
                    $this->sendNotificationAction('withdraw_interest_status_C',$detailRequest['customer_id'],$value['withdraw_request_id']);
                }
            }
        }

        $this->addMailGroup($id,$data,$param,$mCustomer,$statusMail,$subject);

        return [
            'error' => false,
            'message' => $message
        ];
    }

    public function getLastContractMonth($customer_contract_id)
    {
        return $this->contractInterestMonth->getInfoInterestByMonth($customer_contract_id);
    }

    public function getLastContractTime($customer_contract_id)
    {
        return $this->contractInterestTime->lastCustomerContractInterest($customer_contract_id);
    }

    public function getWalletStock($customerId)
    {
        return $this->stockCustomer->getWalletByCustomerId($customerId);
    }

    /**
     * Xác nhận yêu cầu rút tiền từ ví cổ phiếu - ví cổ tức
     * @param $data
     * @return mixed|void
     */
    public function changeStatusStock($param)
    {
        $id = $param['withdraw_request_group_id'];
        unset($param['withdraw_request_group_id']);
        $data = $this->showGroup($id);
        $mCustomer = new CustomerTable();
        $mCustomer  = $mCustomer->getItem($data['customer_id']);
        $statusMail = '';
        if ($param['withdraw_request_status'] == 'inprocess' || $param['withdraw_request_status'] == 'done') {
//        kiểm tra giá tiền trước khi chuyển trạng thái
            $totalBonus = 0;
            $walletStock = $this->stockCustomer->getWalletByCustomerId($data['customer_id']);

            if ($data['withdraw_request_group_type'] == 'stock'){
                $totalBonus = $walletStock == null ? 0 : $walletStock['wallet_stock_money'];

            } elseif($data['withdraw_request_group_type'] == 'dividend') {
                $totalBonus = $walletStock == null ? 0 : $walletStock['wallet_dividend_money'];
            }

            if ((double)$totalBonus < (double)$data['withdraw_request_amount']) {
                if ($param['withdraw_request_status'] == 'inprocess') {
                    return [
                        'error' => true,
                        'message' => 'Không đủ tiền để xác nhận yêu cầu'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Không đủ tiền để hoàn thành yêu cầu'
                    ];
                }
            }

            if ($param['withdraw_request_status'] == 'inprocess') {
                $sendSMS = [
                    'phone' => $data['customer_phone'],
                    'type' => 'confirm_withraw',
                    'withdraw_request_group_code' => $data['withdraw_request_group_code']
                ];
//                $this->sendSMS($sendSMS);
            }
        }

        $updateStatus = $this->withdrawRequestGroup->changeStatus($id,$param);
        $updateStatusChild = $this->withdrawRequest->changeStatusByWithdrawGroup($id,$param);
        $message = '';
        $statusMail = '';
        $subject = '';
        $detailRequest = $this->withdrawRequestGroup->getDetailById($id);

        $idRequest = $this->withdrawRequest->getListIdRequestByGroup($id);

        if ($param['withdraw_request_status'] == 'done') {
            $getDetailStockCustomer = $this->stockCustomer->getWalletByCustomerId($detailRequest['customer_id']);
            $walletStockCustomer = $this->stockWallet->getWalletByCustomerId($detailRequest['customer_id'],$detailRequest['withdraw_request_group_type']);
            if ($walletStockCustomer == null) {
                $walletStockCustomer = 0;
            } else {
                $walletStockCustomer = $walletStockCustomer['total_after'];
            }
            $updateWallet = [
                'customer_id' => $detailRequest['customer_id'],
                'type' => 'minus',
                'source' => $detailRequest['withdraw_request_group_type'],
                'total_before' => $walletStockCustomer,
                'total_money' => $data['withdraw_request_amount'],
                'total_after' => $walletStockCustomer - $data['withdraw_request_amount'],
                'day' => Carbon::now()->format('d'),
                'month' => Carbon::now()->format('m'),
                'year' => Carbon::now()->format('Y'),
                'created_at' => Carbon::now(),
                'created_by' => Auth::id()
            ];

            $this->stockWallet->createWallet($updateWallet);
            $getDetailStockCustomer['wallet_'.$detailRequest['withdraw_request_group_type'].'_money'] = $getDetailStockCustomer['wallet_'.$detailRequest['withdraw_request_group_type'].'_money'] - $data['withdraw_request_amount'];
            $getDetailStockCustomer['last_updated'] = Carbon::now();
            $getDetailStockCustomer['updated_at'] = Carbon::now();
            $getDetailStockCustomer['updated_by'] = Auth::id();
            $this->stockCustomer->updateCustomer($detailRequest['customer_id'],$getDetailStockCustomer->toArray());

            $message = 'Hoàn thành yêu cầu rút tiền';
            $subject = 'Hoàn thành yêu cầu rút tiền';
            $statusMail = 'Hoàn thành';
            if ($detailRequest['withdraw_request_group_type'] == 'stock') {
                $this->sendNotificationAction('withdraw_stock_status_D',$detailRequest['customer_id'],$id);
            } else if ($detailRequest['withdraw_request_group_type'] == 'dividend'){
                $this->sendNotificationAction('withdraw_dividend_status_D',$detailRequest['customer_id'],$id);
            }
        }  else if ($param['withdraw_request_status'] == 'inprocess') {
            $message = 'Xác nhận yêu cầu rút tiền thành công';
            $subject = 'Xác nhận yêu cầu rút tiền';
            $statusMail = 'Xác nhận';
            $config = new ConfigTable();
            $day = 1;
            if ($detailRequest['withdraw_request_group_type'] == 'stock') {
                $this->sendNotificationAction('withdraw_stock_status_A',$detailRequest['customer_id'],$id);
                $arrUpdate = [
                    'withdraw_date_confirm' => Carbon::now(),
                    'payment_date_planing' => Carbon::now()->addDay($day)->format('Y-m-d')
                ];
                $this->withdrawRequestGroup->changeStatus($id,$arrUpdate);
            } else if ($detailRequest['withdraw_request_group_type'] == 'dividend'){
                $arrUpdate = [
                    'withdraw_date_confirm' => Carbon::now(),
                    'payment_date_planing' => Carbon::now()->addDay($day)->format('Y-m-d')
                ];
                $this->withdrawRequestGroup->changeStatus($id,$arrUpdate);
                $this->sendNotificationAction('withdraw_dividend_status_A',$detailRequest['customer_id'],$id);
            }
        } else {
            $message = 'Không xác nhận yêu cầu rút tiền thành công';
            $subject = 'Không xác nhận yêu cầu rút tiền';
            $statusMail = 'Hủy';
            if ($detailRequest['withdraw_request_group_type'] == 'stock') {
                $this->sendNotificationAction('withdraw_stock_status_C',$detailRequest['customer_id'],$id);
            } else if ($detailRequest['withdraw_request_group_type'] == 'dividend'){
                $this->sendNotificationAction('withdraw_dividend_status_C',$detailRequest['customer_id'],$id);
            }
        }
//        todo: Send Mail, SMS---------------------------------
        $keyNoti = "stock_admin_withdraw_" . $detailRequest['withdraw_request_group_type'] ;
        if ($detailRequest->withdraw_request_status == 'cancel') $keyNoti .= '_cancel';

//        todo: Send Noti-------------
        $mNoti = new JobsEmailLogApi();
        $noti =[
            'key' =>$keyNoti,
            'customer_id' =>$detailRequest['customer_id'],
            'object_id' => $detailRequest['withdraw_request_group_id'],
            'withdraw_request_group_id'=>$detailRequest['withdraw_request_group_id'],
            'money'=>$detailRequest['withdraw_request_amount']
        ];
//        dd($noti);
        $mNoti->sendNotification($noti);
//        end: Send Noti--------------

        $this->addMailGroup($id,$data,$param,$mCustomer,$statusMail,$subject);
        $this->messageRepo->sendSMS(
            $keyNoti,
            [
                'phone'=>$detailRequest->customer_phone
            ],[
                'withdraw_request_group_id'=>$detailRequest['withdraw_request_group_id']
            ]

        );

        return [
            'error' => false,
            'message' => $message
        ];
    }
}