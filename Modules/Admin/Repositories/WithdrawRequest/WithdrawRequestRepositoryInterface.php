<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\WithdrawRequest;


interface WithdrawRequestRepositoryInterface
{
    public function list(array $filters = []);
    public function listAll(array $filters = []);
    public function listCommission(array $filters = []);
    public function listStock(array $filters = []);
    public function show($id);
    public function getListInterest($customer_contract_id);
    public function getContract($customer_contract_id);
    public function getInterestByMonth($customer_contract_id);
    public function getListWithdrawGroup($customer_id,$type);
    public function totalBonusCustomer($customer_id);
    public function totalBonusCustomerWallet($customer_id);
    public function showGroup($id);
    public function getListCustomer();
    public function totalWithdrawGroup($customer_id,$type);
    public function totalInterestMonth($customer_contract_id);
    public function totalMoneyWithdrawGroup($customer_contract_id,$type);
    public function totalMoneyWithdrawGroupFix($customer_contract_id,$type);
    public function getTotalRequest($customer_contract_id);
    public function getWithdrawDatePlaning($customer_contract_id);

    public function getListWithdrawRequest($filter);
    public function getListCustomerContractInterestMonth($filter);
    public function getListCustomerContractInterestDate($filter);
    public function changeStatus($param);
    public function changeStatusGroup($param);
    public function getLastContractMonth($customer_contract_id);
    public function getLastContractTime($customer_contract_id);


    /**
     * Lấy số tiền trong ví cổ tức và ví cổ phiếu
     * @param $customerId
     * @return mixed
     */
    public function getWalletStock($customerId);

    /**
     * Xác nhận yêu cầu rút tiền từ ví cổ phiếu - ví cổ tức
     * @param $data
     * @return mixed
     */
    public function changeStatusStock($param);

}