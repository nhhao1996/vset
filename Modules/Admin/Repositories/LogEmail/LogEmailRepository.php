<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/20/2019
 * Time: 4:43 PM
 */

namespace Modules\Admin\Repositories\LogEmail;

use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\JobsEmailLogTable;
use Modules\Admin\Models\PointHistoryDetailTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Models\PointRewardRuleTable;

class LogEmailRepository implements LogEmailRepositoryInterface
{
    protected $jobEmail;

    public function __construct(JobsEmailLogTable $jobEmail)
    {
        $this->jobEmail = $jobEmail;
    }

    public function list(array $filters = [])
    {
        return $this->jobEmail->getList($filters);
    }
}
