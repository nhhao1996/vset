<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 9/24/2018
 * Time: 2:28 PM
 */

namespace Modules\Admin\Repositories\Department;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Models\DepartmentTable;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;


class DepartmentRepository implements DepartmentRepositoryInterface
{
    protected $department;
    protected $timestamps = true;

    public function __construct(DepartmentTable $department)
    {
        $this->department = $department;
    }

    /**
     * get list department
     */
    public function list(array $filterts = [])
    {
        return $this->department->getList($filterts);
    }

    /**
     * add department.
     */
    public function add(array $data)
    {
        $dataadd['department_name'] = strip_tags($data['department_name']);
        $dataadd['is_inactive'] = 1;
        $dataadd['created_by'] =  Auth::id();
        $dataadd['updated_by'] = Auth::id();
        $dataadd['updated_at'] = Carbon::now();
        // check bank name
        $check = $this->department->checkByname($data['department_name']);
//        dd($check);
        if ($check > 0 ){
            return response()->json([
                'error' => true,
                'message' => __('Tên chi nhánh trùng')
            ]);
        }
        //check bank name
        $adddepartment = $this->department->add($dataadd);
        return [
            'error' => false,
            'message' => 'Tạo chi nhánh thành công'
        ];
//        return $this->department->add($dataadd);
    }

    /**
     * edit department
     */
    public function edit(array $data, $id)
    {
        return $this->department->edit($data, $id);
    }

    /**
     * delete department
     */
    public function remove($id)
    {
        return $this->department->remove($id);
    }

    /**
     * Get item
     */
    public function getItem($id)
    {
        return $this->department->getItem($id);
    }

    //Get option
    public function getstaffDepartmentOption()
    {
        $array = [];
        foreach ($this->department->getstaffDepartmentOption() as $item) {
            $array[$item['department_id']] = $item['department_name'];
        }
        return $array;
    }

    /*
     * check unique department
     */
    public function check($name)
    {
        return $this->department->check($name);
    }

    /*
   * check unique department edit
   */
    public function checkEdit($id, $name)
    {
        return $this->department->checkEdit($id, $name);
    }
    /*
     * test is deleted
     */
    public function testIsDeleted($name)
    {
        return $this->department->testIsDeleted($name);
    }

    /*
     * edit by department name
     */
    public function editByName($name)
    {
        return $this->department->editByName($name);
    }

    public function adddepartment($data)
    {
        $dataadd['department_name'] = strip_tags($data['department_name']);
        $dataadd['created_by'] = Auth::id();
        $dataadd['is_inactive'] = $data['is_inactive'];
        $dataadd['updated_by'] = Auth::id();
        $dataadd['created_at'] = Carbon::now();
        $dataadd['updated_at'] = Carbon::now();
        // check bank name
        $bankcheck = $this->department->checkByname($data['department_name']);
        if ($bankcheck > 0 ){
            return response()->json([
                'error' => true,
                'message' => __('Tên phòng ban trùng')
            ]);
        }
        //check bank name
        $adddepartment = $this->department->add($dataadd);
        return [
            'error' => false,
            'message' => 'Tạo phòng ban thành công'
        ];
    }

}