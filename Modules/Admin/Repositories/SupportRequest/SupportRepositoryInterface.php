<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\SupportRequest;


interface SupportRepositoryInterface
{
    public function list(array $filters = []);

    public function editView($id);

    public function updateStatus($id,$status);

    public function delete($id);
}