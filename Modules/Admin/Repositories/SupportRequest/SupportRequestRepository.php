<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/25/2018
 * Time: 10:16 AM
 */

namespace Modules\Admin\Repositories\SupportRequest;

use Modules\Admin\Models\FaqTable;
use Modules\Admin\Models\FaqGroupTable;
use Modules\Admin\Models\SupportRequest;
use Modules\Admin\Models\SupportRequestType;
use Modules\Admin\Repositories\SupportRequest\SupportRepositoryInterface;
use Modules\Admin\Repositories\FaqGroup\FaqGroupRepositoryInterface;

class SupportRequestRepository implements SupportRepositoryInterface
{

    protected $support;
    protected $supportType;
    public function __construct(
        SupportRequest $support,SupportRequestType $supportType
    )
    {
        $this->support = $support;
        $this->supportType = $supportType;
    }

    //Hàm lấy danh sách
    public function list(array $filters = [])
    {
//        dd($this->support->getList($filters));
        $filters['page'] = (isset($filters["page"])) ? $filters["page"] :1;
        $filters['display'] = (isset($filters["display"])) ? $filters["display"] :10;
        return $this->support->getList($filters);
    }

    public function editView($id){
        $oSupport = $this->support->getItem($id);

        return $oSupport;
    }

    public function updateStatus($id,$status){
        $this->support->updateStatus($id,$status);
    }

    public function delete($id){
        $this->support->remove($id);
    }
}