<?php


namespace Modules\Admin\Repositories\StockTutorial;


interface StockTutorialRepositoryInterface
{
    public function getDetail($key);

    public function update($data);

}