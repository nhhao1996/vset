<?php


namespace Modules\Admin\Repositories\StockTutorial;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Libs\SmsFpt\TechAPI\src\TechAPI\Exception;
use Modules\Admin\Models\StockTutorialTable;

class StockTutorialRepository implements StockTutorialRepositoryInterface
{
    protected $stockTutorial;

    public function __construct(StockTutorialTable $stockTutorial)
    {
        $this->stockTutorial = $stockTutorial;
    }

    public function getDetail($key)
    {
        return $this->stockTutorial->getDetail($key);
    }

    public function update($data)
    {
        try {
            $key = $data['stock_tutorial_key'];
            unset($data['stock_tutorial_key']);
            $this->stockTutorial->updateByKey($data,$key);
            return [
                'error' => false,
                'message' => 'Chỉnh sửa thông tin hướng dẫn thành công'
            ];
        }catch (Exception $e){
            return [
                'error' => true,
                'message' => 'Chỉnh sửa thông tin hướng dẫn thất bại'
            ];
        }
    }

}