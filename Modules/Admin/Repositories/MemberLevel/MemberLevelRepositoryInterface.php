<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17/03/2018
 * Time: 2:40 PM
 */
namespace Modules\Admin\Repositories\MemberLevel;

interface MemberLevelRepositoryInterface
{

    public function list(array $filters = []);


    /**
     * Delete user
     *
     * @param number $id
     */
    public function remove($id);


    /**
     * Add user
     *
     * @param array $data
     * @return number
     */
    public function add(array $data);

    public function edit(array $param);

    public function getItem($id);
    public function getOptionMemberLevel();
    public function addMember($param);
//
//    public function getListProvinceOptions();
//
    public function testName($name,$id);

    /**
     * @param $point
     * @return mixed
     */
    public function rankByPoint($point);
}