<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17/03/2018
 * Time: 2:39 PM
 */

namespace Modules\Admin\Repositories\MemberLevel;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\MemberLevelTable;

class MemberLevelRepository implements MemberLevelRepositoryInterface
{
    protected $memberLevel;
    protected $timestamps = true;


    public function __construct(MemberLevelTable $memberLevel)
    {
        $this->memberLevel = $memberLevel;
    }


    /**
     * Lấy danh sách user
     */
    public function list(array $filters = [])
    {
        return $this->memberLevel->getList($filters);
    }


    /**
     * Xóa user
     */
    public function remove($id)
    {
        $this->memberLevel->remove($id);
    }

    /**
     * Thêm user
     */
    public function add(array $data)
    {
        return $this->memberLevel->add($data);
    }

    public function edit(array $param)
    {
        try{
            $test = $this->memberLevel->testName(str_slug($param['name']), $param['member_level_id']);
            if ($param['member_image'] != null) {
                $param['member_image'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $param['member_image'],
                        str_replace('', '', $param['member_image'])
                    );
            } else {
                $param['member_image'] = $param['member_image_hidden'];
            }

            if ($test == null) {
                $data = [
                    'name' => strip_tags($param['name']),
                    'name_en' => strip_tags($param['name_en']),
                    'range_content' => strip_tags($param['range_content']),
                    'range_content_en' => strip_tags($param['range_content_en']),
                    'slug' => str_slug(strip_tags($param['name'])),
                    'point' => strip_tags($param['point']),
//                    'discount' => strip_tags($param['discount']),
                    'is_actived' => $param['is_actived'],
                    'description' => $param['description'],
                    'description_en' => $param['description_en'],
                    'member_image' => $param['member_image'],
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'is_deleted' => 0,
                ];
                $this->memberLevel->edit($data,$param['member_level_id']);
                return response()->json([
                    'error' => false,
                    'message' => 'Cập nhật cấp độ thành công'
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => __('Cấp độ đã tồn tại')
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('Cập nhật cấp độ thất bại')
            ]);
        }
    }

    public function getItem($id)
    {

        return $this->memberLevel->getItem($id);
    }


    public function getOptionMemberLevel()
    {
        $array=array();
        foreach ($this->memberLevel->getOptionMemberLevel() as $value) { //bien $value se bang gia tri tra ve cua ham getOptionMemberLevel
            $array[$value['member_level_id']]=$value['member_level_name'];//ma ham getOptionMemberLevel truy van database tra ve MEMBER_ID va Member_name
        }           //=> bien $value se co gia tri mang [member_id,member_name]
        return $array;// sau do ta tra ve vong lap foreach theo gia tri id va name cua value

    }

    public function addMember($param)
    {
        try{
            $test = $this->memberLevel->testName(str_slug($param['name']), '0');
            if ($param['member_image'] != null) {
                $param['member_image'] = url('/').'/' . $this->transferTempfileToAdminfile(
                        $param['member_image'],
                        str_replace('', '', $param['member_image'])
                    );
            }

            if ($test == null) {
                $data = [
                    'name' => strip_tags($param['name']),
                    'name_en' => strip_tags($param['name_en']),
                    'range_content' => strip_tags($param['range_content']),
                    'range_content_en' => strip_tags($param['range_content_en']),
                    'slug' => str_slug(strip_tags($param['name'])),
                    'point' => strip_tags($param['point']),
//                    'discount' => strip_tags($param['discount']),
                    'is_actived' => isset($param['is_actived']) ? 1 : 0,
                    'description' => $param['description'],
                    'description_en' => $param['description_en'],
                    'member_image' => $param['member_image'],
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'is_deleted' => 0,
                ];
                $this->memberLevel->add($data);
                return response()->json([
                    'error' => false,
                    'message' => 'Tạo cấp độ thành công'
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => __('Cấp độ đã tồn tại')
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('Tạo cấp độ thất bại')
            ]);
        }
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    public function testName($name,$id){
        return $this->memberLevel->testName($name,$id);
    }

    /**
     * @param $point
     * @return mixed
     */
    public function rankByPoint($point)
    {
        return $this->memberLevel->rankByPoint($point);
    }
}