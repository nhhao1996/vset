<?php
namespace Modules\Admin\Repositories\ServiceSerial;

/**
 * User Repository interface
 *  
 * @author thach
 * @since   2018
 */
interface ServiceSerialRepositoryInterface
{    
    /**
     * Get Service Type list
     * 
     * @param array $filters
     */
    public function list(array $filters = []);

    public function getDetail($id);

    public function editPost($data);
} 