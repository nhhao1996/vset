<?php
/**
 * Created by PhpStorm.
 * User: WAO
 * Date: 13/03/2018
 * Time: 1:48 CH
 */

namespace Modules\Admin\Repositories\ServiceSerial;


use Modules\Admin\Models\ServiceSerialTable;

class ServiceSerialRepository implements ServiceSerialRepositoryInterface
{
    protected $serviceSerial;

    public function __construct(ServiceSerialTable $serviceSerial)
    {
        $this->serviceSerial = $serviceSerial;
    }

    /**
     *get list services Type
     */
    public function list(array $filters = [])
    {
        return $this->serviceSerial->getList($filters);
    }

    public function getDetail($id)
    {
        return $this->serviceSerial->getDetail($id);
    }

    public function editPost($data)
    {
        try{
            $id = $data['service_serial_id'];
            unset($data['service_serial_id']);
            $data['serial'] = strip_tags($data['serial']);
            if(isset($data['serial'])) {
//            Kiểm tra số serial
                $check = $this->serviceSerial->checkSerial($id,$data['serial']);
                if (count($check) != 0) {
                    return [
                        'error' => true,
                        'message' => __('Số serial đã được sử dụng')
                    ];
                }
            }
            if (strlen($data['serial']) == 0) {
                $data['serial'] = null;
            }
            $this->serviceSerial->updateServiceSerrial($id,$data);
            return [
                'error' => false,
                'message' => __('Cập nhật thành công')
            ];
        } catch (\Exception $e){
            return [
                'error' => true,
                'message' => __('Cập nhật thất bại')
            ];
        }
    }

}