<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/20/2019
 * Time: 4:43 PM
 */

namespace Modules\Admin\Repositories\ReferSource;


interface ReferSourceRepositoryInterface
{
    public function getList();

//    Lấy thông tin chi tiết
    public function getInfo($id);

    /**
     * Xóa dich vụ
     * @param $input
     * @return mixed
     */
    public function removeService($input);

    /**
     * Lấy danh sách dịch vụ
     * @param array $filter
     * @return mixed
     */
    public function getListService(array $filter=[]);

    /**
     * Thêm danh sách dịch vụ
     * @param $data
     * @return mixed
     */
    public function addListService($data);

    /**
     * Cập nhật cấu hình
     * @param $param
     * @return mixed
     */
    public function updateConfig($param);
}