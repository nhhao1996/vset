<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/20/2019
 * Time: 4:43 PM
 */

namespace Modules\Admin\Repositories\ReferSource;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\JobsEmailLogTable;
use Modules\Admin\Models\PointHistoryDetailTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Models\PointRewardRuleTable;
use Modules\Admin\Models\ReferSourceCustomerTable;
use Modules\Admin\Models\ReferSourceMapServiceTable;
use Modules\Admin\Models\ReferSourceTable;
use Modules\Admin\Models\Service;
use Modules\Admin\Models\ServiceTable;

class ReferSourceRepository implements ReferSourceRepositoryInterface
{
    protected $referSourceDB;
    protected $referSourceCustomerDB;
    protected $referSourceMapServiceDB;
    protected $service;
    public function __construct(
        ReferSourceTable $referSourceDB,
        ReferSourceCustomerTable $referSourceCustomerDB,
        ReferSourceMapServiceTable $referSourceMapServiceDB,
        ServiceTable $service
    )
    {
        $this->referSourceDB = $referSourceDB;
        $this->referSourceCustomerDB = $referSourceCustomerDB;
        $this->referSourceMapServiceDB = $referSourceMapServiceDB;
        $this->service = $service;
    }

    public function getList()
    {
        return $this->referSourceDB->getListAll();
    }

    public function getInfo($id)
    {
        $getConfig = $this->referSourceCustomerDB->getConfig($id);
        $getSource = $this->referSourceDB->getSource($id);
        if (count($getConfig) != 0) {
            $getConfig = collect($getConfig)->keyBy('type');
        }

        $getListService = $this->referSourceMapServiceDB->getListService($id);
        if (count($getListService) != 0) {
            $getListService = collect($getListService)->groupBy('type');
        }

        $service = [
            'refer' => [],
            'register' => []
        ];

        if (isset($getListService['refer'])) {
            $service['refer'] = collect($getListService['refer'])->pluck('service_id')->toArray();
        }

        if (isset($getListService['register'])) {
            $service['register'] = collect($getListService['register'])->pluck('service_id')->toArray();
        }



        return [
            'config' => $getConfig,
            'listService' => $getListService,
            'service' => $service,
            'source' => $getSource
        ];
    }


    public function removeService($input)
    {
        if ($input['type'] == 'customer') {
            $this->linkSourceServiceCustomerDB->removeCustomer($input['id']);
        } elseif($input['type'] == 'refer') {
            $this->linkSourceServiceReferDB->removeRefer($input['id']);
        }

        return [
            'error' => false,
            'message' => 'Xóa dịch vụ thành công'
        ];
    }

    public function getListService(array $filter = [])
    {
        unset($filter['type']);
        unset($filter['refer_source_id']);
        $list = $this->service->getListService($filter);
        $view = view('admin::refer-source.popup.table',[
            'list' => $list,
            'filter' => $filter
        ])->render();
        return [
            'view' => $view
        ];
    }

    public function addListService($data)
    {
        $listService = $this->service->getListServiceSession($data['listService']);
        $view = view('admin::refer-source.partial.table-tr',[
            'listService' => $listService,
            'type' => $data['type']
        ])->render();
        return [
            'view' => $view
        ];
    }

    public function updateConfig($param)
    {
//        if ($param['is_active'] == 1) {
            if ($param['date_action_' . $param['type']] == null) {
                return [
                    'error' => true,
                    'message' => 'Yêu cầu chọn thời gian phát thưởng'
                ];
            }
            if (!isset($param['type_bonus_' . $param['type']])) {
                return [
                    'error' => true,
                    'message' => 'Yêu cầu chọn loại phát thưởng'
                ];
            }
//        }

            if (isset($param['type_bonus_'.$param['type']])){
                if ($param['type_bonus_'.$param['type']] == 'money') {
                    if (isset($param['money_'.$param['type']])){
                        if ($param['money_'.$param['type']] < 0 || $param['money_'.$param['type']] >= 1000000000000000) {
                            return [
                                'error' => true,
                                'message' => 'Tiền thưởng phải trong khoảng 0 -> 999,999,999,999,999'
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Tiền thưởng phải trong khoảng 0 -> 999,999,999,999,999'
                        ];
                    }
                } else {
                    if (count($param['listService'][$param['type']]) == 0) {
                        return [
                            'error' => true,
                            'message' => 'Yêu cầu chọn dịch vụ để phát thưởng'
                        ];
                    }
                }
            }

        $checkReferCustomer = $this->referSourceCustomerDB->checkReferCustomer($param['refer_source_id'],$param['type']);
        $id = null;
        if ($checkReferCustomer != null) {
            $id = $checkReferCustomer['refer_source_customer_id'];
            unset($checkReferCustomer['refer_source_customer_id']);
            $checkReferCustomer['is_active'] = $param['is_active'];
            $checkReferCustomer['updated_at'] = Carbon::now();
            $checkReferCustomer['updated_by'] = Auth::id();
            if ($param['is_active'] == 1) {
                $arr_filter = explode(" - ",$param['date_action_'.$param['type']]);
                $checkReferCustomer['start']  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $checkReferCustomer['end']  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
            }

            if (isset($param['money_'.$param['type']])){
                $checkReferCustomer['money'] = str_replace(',', '', $param['money_'.$param['type']]);
            }

            if (isset($param['type_bonus_'.$param['type']])){
                $checkReferCustomer['type_bonus'] = $param['type_bonus_'.$param['type']];
            }

            if ($param['type_bonus_'.$param['type']] == 'voucher') {
                $checkReferCustomer['money'] = 0;
            }
            $this->referSourceCustomerDB->updateConfig($id,$checkReferCustomer);
        } else {
            $checkReferCustomer['refer_source_id'] = $param['refer_source_id'];
            $checkReferCustomer['type'] = $param['type'];
            $checkReferCustomer['is_active'] = $param['is_active'];
            $checkReferCustomer['updated_at'] = Carbon::now();
            $checkReferCustomer['updated_by'] = Auth::id();
            if ($param['is_active'] == 1) {
                $arr_filter = explode(" - ",$param['date_action_'.$param['type']]);
                $checkReferCustomer['start']  = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d 0:00:00');
                $checkReferCustomer['end']  = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d 23:59:59');
            }

            if (isset($param['money_'.$param['type']])){
                $checkReferCustomer['money'] = str_replace(',', '', $param['money_'.$param['type']]);
            }

            if (isset($param['type_bonus_'.$param['type']])){
                $checkReferCustomer['type_bonus'] = $param['type_bonus_'.$param['type']];
            }

            if ($param['type_bonus_'.$param['type']] == 'voucher') {
                $checkReferCustomer['money'] = 0;
            }
            $id = $this->referSourceCustomerDB->createConfig($checkReferCustomer);
        }

        $this->referSourceMapServiceDB->deleteService($id);
        if ($param['type_bonus_'.$param['type']] == 'voucher') {
            if (isset($param['listService'][$param['type']])) {
                $data = [];
                foreach ($param['listService'][$param['type']] as $item) {
                    $data[] = [
                        'refer_source_customer_id' => $id,
                        'service_id' => $item
                    ];
                }

                $this->referSourceMapServiceDB->addService($data);
            }
        }

        return [
            'error'=> false,
            'message' => 'Cập nhật thành công'
        ];


    }

}
