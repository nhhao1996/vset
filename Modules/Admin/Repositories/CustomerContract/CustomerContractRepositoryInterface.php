<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 10/1/2019
 * Time: 14:01
 */

namespace Modules\Admin\Repositories\CustomerContract;


interface CustomerContractRepositoryInterface
{
    /**
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = []);

    public function listExport(array $filters = []);

    /**
     * Xóa customer contract id
     * @param $id
     * @return mixed
     */
    public function deleteCustomerContract($customer_contract_id);

    /**
     * Chi tiết hợp đồng
     * @param $customer_contract_id
     * @return mixed
     */
    public function getDetail($customer_contract_id);

    public function uploadDropzone($input);

    public function updateContractSerial($input);
    /**
     * Tạo Hop đồng
     * @param $data
     * @return mixed
     */
    public function createCustomerContract($data);

    public function addAction();

    /**
     * Export Excel store
     *
     **/
    public function exportExcel(array $array,$title);

    /**
     * Data view chi tiết hợp đồng
     *
     * @param $contractId
     * @return mixed
     */
    public function dataViewDetail($contractId);

    /**
     * Danh sách file hợp đồng
     *
     * @param array $filters
     * @return mixed
     */
    public function listFile(array $filters = []);

    /**
     * Danh sách lãi suất tháng
     *
     * @param array $filters
     * @return mixed
     */
    public function listInterestMonth(array $filters = []);

    /**
     * Danh sách lãi suất ngày
     *
     * @param array $filters
     * @return mixed
     */
    public function listInterestDate(array $filters = []);

    /**
     * Danh sách lãi suất theo thời điểm
     *
     * @param array $filters
     * @return mixed
     */
    public function listInterestTime(array $filters = []);

    /**
     * Danh sách yêu cầu rút gốc
     *
     * @param array $filters
     * @return mixed
     */
    public function listWithdraw(array $filters = []);

    /**
     * Danh sách serial hợp đồng
     *
     * @param array $filters
     * @return mixed
     */
    public function listSerial(array $filters = []);

    public function confirmContract($param);

//    Lấy code hợp đồng được gia hạn
    public function getOldContract($customer_contract);

    /**
     * Danh sách lãi cần duyệt
     * @param array $filters
     * @return mixed
     */
    public function getListContractApproved(array $filters = []);

    /**
     * Xác nhận danh sách lãi
     * @param $input
     * @return mixed
     */
    public function confirmContractAll($input);

    /**
     * Danh sách export lãi hàng tháng
     * @param $input
     * @return mixed
     */
    public function listExportContractInterest($input);
}