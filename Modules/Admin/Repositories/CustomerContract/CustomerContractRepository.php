<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 10/1/2019
 * Time: 14:01
 */

namespace Modules\Admin\Repositories\CustomerContract;

use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\WriterFactory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerContractArchiveTable;
use Modules\Admin\Models\CustomerContractSerial;
use Modules\Admin\Models\Customers;
use Modules\Admin\Models\CustomerContractFile;
use Modules\Admin\Models\CustomerContractFileTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Modules\Admin\Models\CustomerContractInterestDateTable;
use Modules\Admin\Models\CustomerContractSerialTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\Order;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Models\StaffTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Admin\Repositories\CustomerContract\CustomerContractRepositoryInterface;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepository;

class CustomerContractRepository implements CustomerContractRepositoryInterface
{
    protected $customerContract;
    protected $customerContractSerial;
    protected $customerContractFile;
    protected $customer;
    protected $interestByMonth;
    protected $jobMail;
    protected $withdrawRequest;
    protected $customerContractArchive;
    protected $order;
    protected $rCustomerContractInterest;

    public function __construct(
        CustomerContactTable $customerContract,
        CustomerContractSerial $contractSerial,
        Customers $customer,
        CustomerContractFileTable $contractFileTable,
        CustomerContractInterestByMonthTable $interestByMonth,
        WithdrawRequestTable $withdrawRequest,
        CustomerContractArchiveTable $customerContractArchive,
        Order $order,
        CustomerContractInterestRepository $rCustomerContractInterest,
        JobsEmailLogApi $jobMail)
    {
        $this->customerContract = $customerContract;
        $this->customer = $customer;
        $this->customerContractSerial = $contractSerial;
        $this->customerContractFile = $contractFileTable;
        $this->interestByMonth = $interestByMonth;
        $this->jobMail = $jobMail;
        $this->withdrawRequest = $withdrawRequest;
        $this->customerContractArchive = $customerContractArchive;
        $this->order = $order;
        $this->rCustomerContractInterest = $rCustomerContractInterest;
    }

        /**
     * @param array $filters
     * @return mixed
     */

    public function endContract($filter=[]){
        return $this->customerContract->endContract($filter);

    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters=[])
    {
        if (isset($filters['created_at']) != "") {
            $filters['startTime']=[];
            $filters['endTime']=[];
            $arr_filter = explode(" - ", $filters["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTime'] =$startTime;
            $filters['endTime'] =$endTime;
            unset($filters['created_at']);
        }

        if (isset($filters['customer_contract_start_date']) != "") {
            $filters['startTimeContract']=[];
            $filters['endTimeContract']=[];
            $arr_filter = explode(" - ", $filters["customer_contract_start_date"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTimeContract'] =$startTime;
            $filters['endTimeContract'] =$endTime;
            unset($filters['customer_contract_start_date']);
        }
        $list = $this->customerContract->getList($filters);
//        foreach ($list as $item){
//            if ($item['withdraw_date_planning'] == null) {
//                $item['withdraw_date_planning'] = Carbon::parse($item['customer_contract_start_date'])->addMonth($item['withdraw_interest_time']);
//            }
//        }
//        if (count($list)) {
//            $arr = collect($list->toArray()['data'])->pluck('customer_contract_id');
//            $arrDate = $this->withdrawRequest->getListByContract($arr);
//            if (count($arrDate) != 0) {
//                $arrDate = collect($arrDate)->keyBy('customer_contract_id');
//            }
//            foreach ($list as $item){
//                if (isset($arrDate[$item['customer_contract_id']])) {
////                    $item['withdraw_date_planning'] =
//                }
//            }
//        }
        return $list;
    }

    public function listExport(array $filters = [])
    {
        if (isset($filters['created_at']) != "") {
            $filters['startTime']=[];
            $filters['endTime']=[];
            $arr_filter = explode(" - ", $filters["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTime'] =$startTime;
            $filters['endTime'] =$endTime;
            unset($filters['created_at']);
        }

        if (isset($filters['customer_contract_start_date']) != "") {
            $filters['startTimeContract']=[];
            $filters['endTimeContract']=[];
            $arr_filter = explode(" - ", $filters["customer_contract_start_date"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $filters['startTimeContract'] =$startTime;
            $filters['endTimeContract'] =$endTime;
            unset($filters['customer_contract_start_date']);
        }

        $list = $this->customerContract->getListExport($filters);
        return $list;
    }

    public function deleteCustomerContract($customer_contract_id)
    {
        $delete = $this->customerContract->deleteCustomerContract($customer_contract_id);
        return $delete;
    }

    public function getDetail($customer_contract_id)
    {
        $detail = $this->customerContract->getDetail($customer_contract_id);
        $oSerial = $this->customerContractSerial->getSerialbyProductId($detail["product_id"],$customer_contract_id);
        $file = $this->customerContractFile->getImage($customer_contract_id);
        return [
            "detail"=>$detail,
            "serial" => $oSerial,
            "file"=>$file
        ];
    }

    public function uploadDropzone($input){
        $time = Carbon::now();
        // Requesting the file from the form
        $image = $input->file('file');
        // Getting the extension of the file
        $extension = $image->getClientOriginalExtension();
        //tên của hình ảnh
        $filename = $image->getClientOriginalName();
        //$filename = time() . str_random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . time() . "." . $extension;
        // This is our upload main function, storing the image in the storage that named 'public'
        $upload_success = $image->storeAs(TEMP_PATH, $filename, 'public');
        // If the upload is successful, return the name of directory/filename of the upload.
        if ($upload_success) {
            return response()->json($filename, 200);
        } // Else, return error 400
        else {
            return response()->json('error', 400);
        }
    }

    public function updateContractSerial($input){
        try {
            DB::beginTransaction();
            $data_arr = $input->all();
            $contract_serial = isset($data_arr["serial"]) ? $data_arr["serial"] : [];
//            $serial_front_img = (isset($data_arr["serial_front_img"] )) ? $data_arr["serial_front_img"]:[];
//            $serial_back_img = (isset($data_arr["serial_back_img"] )) ?$data_arr["serial_back_img"]:[];

            $front_img = [];
            $back_img = [];

//            foreach ($serial_front_img as $key=>$item){
//                $name = $this->uploadImageContract($item);
//                $front_img[$key] = $name;
//            }
//            foreach ($serial_back_img as $key=>$item){
//                $name = $this->uploadImageContract($item);
//                $back_img[$key] = $name;
//            }


            foreach ($contract_serial as $index => $item){
                $img_front = $item['img_front_hidden'];
                $img_back = $item['img_back_hidden'];
                if ($item['img_front'] != null) {
                    $img_front = url('/').'/' .$this->transferTempfileToAdminfile($item['img_front'], str_replace('', '', $item['img_front']));
                }
                if ($item['img_back'] != null) {
                    $img_back = url('/').'/' .$this->transferTempfileToAdminfile($item['img_back'], str_replace('', '', $item['img_back']));
                }
                $data = [
                    "customer_contract_id"=>$data_arr["customer_contract_id"],
                    "product_id"=> $data_arr["productId"],
                    "product_serial_no"=> strip_tags($item["product_serial_no"]),
                    "issued_date"=>Carbon::now(),
                    "product_serial_amount"=> str_replace(',', '', $item["product_serial_amount"]),
                    "product_serial_front"=>$img_front,
                    "product_serial_back"=> $img_back,
                    "issued_by"=>Auth()->id(),
                    "created_by"=>Auth()->id(),
                    "updated_by"=>Auth()->id()
                ];

                if( isset($item["customer_contract_serial_id"]) ){
                    $this->customerContractSerial->edit($item["customer_contract_serial_id"],$data);
                }else{
                    $this->customerContractSerial->add($data);
                }
            }
            $this->customerContractFile->deleteCustomerFile($data_arr["customer_contract_id"]);
            if (isset($data_arr['contract_file'])) {
                $arr = [];
                foreach ($data_arr['contract_file'] as $key => $item) {
                    $arr[$key] = [
                        'customer_contract_id' => $data_arr["customer_contract_id"],
                        'position_file' => $key + 1,
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id(),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
                    $arr[$key]['image_file'] = $item['image'];
                    if (strpos($item['image'], '/uploads/') === false) {
                        $arr[$key]['image_file'] = url('/').'/' .$this->transferTempfileToAdminFileContract($item['image'], str_replace('', '', $item['image']));
                    }
                }

//                Xóa các file cũ
                $this->customerContractFile->createContractFile($arr);

            }

            DB::commit();
            return [
                'error' => false,
                'message' => 'Cập nhật hợp đồng thành công'
            ];
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Cập nhật hợp đồng thất bại'
            ];
        }
    }

    //Lưu file image vào folder temp
    private function uploadImageContract($file)
    {
        $time = Carbon::now();

        $file_name = rand(0, 9) . time() . date_format($time, 'd') . date_format($time, 'm') . date_format($time, 'Y') . "_contract_serial." . $file->getClientOriginalExtension();
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') ;
        Storage::disk('public')->makeDirectory($new_path);
        $file->move($new_path, $file_name);
        return $new_path . '/'.$file_name;

    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = CUSTOMER_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(CUSTOMER_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    private function transferTempfileToAdminFileContract($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = CUSTOMER_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(CUSTOMER_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    public function addAction(){

    }

    public function createCustomerContract($data)
    {
        return $this->customerContract->getAll();
    }

    public function exportExcel(array $array,$title)
    {

        $store = $this->customerContract->exportExecl($array,$title);
        $table_title=$title;
        $oExcel= WriterFactory::create(Type::XLSX);
        $oExcel->openToBrowser("order-status.xlsx");
        $oExcel->addRowWithStyle($table_title,(new StyleBuilder())->setFontBold()->setFontSize(16)->build());
        foreach ($store as $sheet)
        {
            if (!empty($sheet->created_at)) {
                $sheet->created_at = Carbon::parse($sheet->created_at)->format('d-m-Y');
            }
            if (!empty($sheet->updated_at)) {
                $sheet->updated_at = Carbon::parse($sheet->updated_at)->format('d-m-Y');
            }
            if (!empty($sheet->is_active))
            {
                if($sheet->is_active==1)
                {
                    $sheet->is_active="Đang hoạt động";
                }else{
                    $sheet->is_active="Tạm ngưng";
                }
            }
            $oExcel->addRow(get_object_vars($sheet));
        }
        $oExcel->close();
    }

    /**
     * Data view chi tiết hợp đồng
     *
     * @param $contractId
     * @return array|mixed
     */
    public function dataViewDetail($contractId)
    {
        $contractInfo = $this->customerContract->getItem($contractId);
        return [
            'item' => $contractInfo
        ];
    }

    /**
     * Danh sách file hợp đồng
     *
     * @param array $filters
     * @return array|mixed
     */
    public function listFile(array $filters = [])
    {
        $mContractFile = new CustomerContractFileTable();

        $listFile = $mContractFile->getList($filters);

        return [
            'listFile' => $listFile
        ];
    }

    /**
     * Danh sách lãi suất tháng
     *
     * @param array $filters
     * @return array|mixed
     */
    public function listInterestMonth(array $filters = [])
    {
        $mContractInterestMonth = new CustomerContractInterestByMonthTable();

        $listMonth = $mContractInterestMonth->getList($filters);

        return [
            'listMonth' => $listMonth
        ];
    }

    /**
     * Danh sách lãi suất ngày
     *
     * @param array $filters
     * @return array|mixed
     */
    public function listInterestDate(array $filters = [])
    {
        $mContractInterestDate = new CustomerContractInterestDateTable();

        $listDate = $mContractInterestDate->getList($filters);

        return [
            'listDate' => $listDate
        ];
    }

    /**
     * Danh sách lãi suất theo thời điểm
     *
     * @param array $filters
     * @return array|mixed
     */
    public function listInterestTime(array $filters = [])
    {
        $mContractInterestTime = new CustomerContractInterestByTimetable();

        $listTime = $mContractInterestTime->getList($filters);

        return [
            'listTime' => $listTime
        ];
    }

    /**
     * Danh sách yêu cầu rút gốc
     *
     * @param array $filters
     * @return array|mixed
     */
    public function listWithdraw(array $filters = [])
    {
        $filters['withdraw_request$customer_contract_id'] = $filters['customer_contract_id'];
        unset($filters['customer_contract_id']);
        $mWithdrawRequest = new WithdrawRequestTable();

        $listWithdraw = $mWithdrawRequest->getList($filters);

        return [
            'listWithdraw' => $listWithdraw
        ];
    }

    /**
     * Danh sách số serial
     *
     * @param array $filters
     * @return array|mixed
     */
    public function listSerial(array $filters = [])
    {
        $mContractSerial = new CustomerContractSerialTable();

        $listSerial = $mContractSerial->getList($filters);

        return [
            'listSerial' => $listSerial
        ];
    }



    public function confirmContract($param)
    {
        try {
            DB::beginTransaction();
            $tmp = [
                'confirmed_by' => Auth::id(),
                'confirmed_at' => Carbon::now(),
                'updated_by' => Auth::id(),
                'updated_at' => Carbon::now()
            ];

            $this->interestByMonth->updateInterestMonth($param['id'],$tmp);

            // gọi api lưu data send mail
            // get detail order
            $detail = $this->interestByMonth->getDetail($param['id']);

            // ge t detail customer
            $mCustomer = new CustomerTable();
            $mCustomer  = $mCustomer->getItem($detail['customer_id']);

            // get detail staff
            $mStaff = new StaffsTable();
            $mStaff = $mStaff->getItem(Auth::id());
            $this->rCustomerContractInterest->calculateContract('time',[$detail['customer_contract_id']],'confirm');
            ///////  gọi api lưu data send mail
            $vars = [
                'contract_code' => $detail['customer_contract_code'],
                'staff'      =>  Auth::user()['full_name'],
                'confirm_date'      =>  Carbon::now()->format('Y/m/d H:i:s'),
                'amount'      =>  $detail['interest_banlance_amount'],
                'customer_name'      =>  $mCustomer['full_name'],
            ];

            $arrInsertEmail = [
                'obj_id' => $detail['customer_contract_interest_by_month_id'],
                'email_type' => "confirm-contract-month",
                'email_subject' => __('Xác nhận tiền lãi hàng tháng'),
                'email_to' =>  $mCustomer['email'],
                'email_from' => env('MAIL_USERNAME'),
                'email_params' => json_encode($vars),
            ];
            $this->jobMail->addJob($arrInsertEmail);

            $noti = [
                'key' => 'custom_contract_confirm',
                'customer_id' => $detail['customer_id'],
                'object_id' => $param['id']
            ];
            $this->jobMail->sendNotification($noti);

            DB::commit();
            return [
                'error' => false,
                'message' => 'Xác nhận thành công'
            ];
        } catch (\Exception $e){
            DB::rollBack();
            return [
                'error' => true,
                'message' => 'Xác nhận thất bại'
            ];
        }
    }

    public function getOldContract($customer_contract)
    {
        return $this->order->checkContractExtend($customer_contract);
    }

    public function getListContractApproved(array $filters = [])
    {
        $filters['action'] = 'list';
        $list = $this->interestByMonth->getList($filters);
        return $list;
    }

    public function confirmContractAll($input)
    {
        try {
            $data = [
                'confirmed_by' => Auth::id(),
                'confirmed_at' => Carbon::now()
            ];
            $confirm = $this->interestByMonth->confirmListInterst($input['listInterest'],$data);
            $listContract = $this->interestByMonth->getListContract($input['listInterest']);
            $listContract = collect($listContract)->pluck('customer_contract_id')->toArray();
            $this->rCustomerContractInterest->calculateContract('time',$listContract,'confirm');
            return [
                'error' => false,
                'message' => 'Xác nhận lãi thành công'
            ];
        }catch (\Exception $e) {
            return [
                'error' => true,
                'message' => 'Xác nhận lãi thất bại'
            ];
        }
    }

    public function listExportContractInterest($input)
    {
        return $this->interestByMonth->getListExport($input);
    }


}