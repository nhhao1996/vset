<?php
namespace Modules\Admin\Repositories\District;

interface DistrictRepositoryInterface
{

    public function getOptionDistrict(array $filters = []);
}