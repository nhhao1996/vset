<?php

namespace Modules\Admin\Repositories\CustomerJourney;

/**
 * customer Group  Repository interface
 */
interface CustomerJourneyRepositoryInterface
{
    /**
     * Get customer Group list
     *
     * @param array $filters
     */
    public function list(array $filters = []);

    /**
     * Delete customer Group
     *
     * @param number $id
     */
    public function remove($id);

    /**
     * Add customer Group
     * @param array $data
     * @return number
     */
    public function add(array $data);

    /**
     * Update customer Group
     * @param array $data
     * @return number
     */
    public function edit(array $data, $id);

    /**
     * get item
     * @param array $data
     * @return $data
     */
    public function getItem($id);

    /*
    * test customer source
    */
    public function testCustomerSourceName($name,$id = null);
    /*
     * test customer source edit
     */
    public function testCustomerSourceNameEdit($id,$name);
    /*
     * add update customer source
     */
    public function testIsDeleted($name);
    /*
    * edit by customer source name
    */
    public function editByName($name);

}