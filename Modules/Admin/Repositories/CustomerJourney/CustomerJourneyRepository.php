<?php
/**
 * Created by PhpStorm.
 * User: nhu
 * Date: 13/03/2018
 * Time: 1:48 CH
 */

namespace Modules\Admin\Repositories\CustomerJourney;


use Modules\Admin\Models\CustomerJourneyTable;
use Modules\Admin\Models\CustomerSourceTable;


class CustomerJourneyRepository implements CustomerJourneyRepositoryInterface
{

    /**
     * @var CustomerSourceTable
     */
    protected $customerJourney;
    protected $timestamps = true;

    public function __construct(CustomerJourneyTable $customerJourney)
    {
        $this->customerJourney = $customerJourney;
    }

    /**
     *get list customers Group
     */
    public function list(array $filters = [])
    {
        return $this->customerJourney->getList($filters);
    }

    /**
     * delete customers Group
     */
    public function remove($id)
    {
        $this->customerJourney->remove($id);
    }

    /**
     * add customers Group
     */
    public function add(array $data)
    {

        return $this->customerJourney->add($data);
    }

    /*
     * edit customers Group
     */
    public function edit(array $data, $id)
    {
        return $this->customerJourney->edit($data, $id);
    }

    /*
     *  get item
     */
    public function getItem($id)
    {
        return $this->customerJourney->getItem($id);
    }


    /*
     * edit by customer source name
     */
    public function editByName($customerSourceName)
    {
        return $this->customerJourney->editByName($customerSourceName);
    }

    /*
    * test customer source
    */
    public function testCustomerSourceName($name, $id = null)
    {
        return $this->customerJourney->testCustomerJourney($name,$id);
    }

    /*
     * test customer source edit
     */
    public function testCustomerSourceNameEdit($id, $customerSourceName)
    {
        return $this->customerSource->testCustomerSourceNameEdit($id, $customerSourceName);
    }

    /*
     * add update customer source
     */
    public function testIsDeleted($customerSourceName)
    {
        return $this->customerSource->testIsDeleted($customerSourceName);
    }
}