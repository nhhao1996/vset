<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/24/2018
 * Time: 10:40 AM
 */

namespace Modules\Admin\Repositories\CustomerContractInterest;
use Illuminate\Http\Request;

interface CustomerContractInterestRepositoryInterface
{
    /**
     * @param string $time
     *
     * @return mixed
     */
    public function calculateContract($time = 'month',$customer_contract_id = [],$type = null);

    public function accountStatement();

//    Job noti cảnh báo hợp đồng sắp hết hạn
    public function warningExtend();

//    Gửi yêu cầu gợi ý tái đầu tư khi tiền lãi >= 5 triệu
    public function requestReinestmentContract();

//    Gửi thông báo tiền lãi mỗi ngày
    public function notiInterestInDay();

//    Gửi noti thông báo thưởng đến khách hàng
    public function sendNoti();
}