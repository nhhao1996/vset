<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 8/26/2020
 * Time: 3:14 PM
 */

namespace Modules\Admin\Repositories\CustomerContractInterest;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Models\AccountStatementMapContractTable;
use Modules\Admin\Models\AccountStatementTable;
use Modules\Admin\Models\CheckVersionTable;
use Modules\Admin\Models\ConfigTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerContractInterestByDateLogTable;
use Modules\Admin\Models\CustomerContractInterestByDateTable;
use Modules\Admin\Models\CustomerContractInterestByMonthTable;
use Modules\Admin\Models\CustomerContractInterestByTimeLogTable;
use Modules\Admin\Models\CustomerContractInterestByTimetable;
use Carbon\Carbon;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Models\JobsEmailLogTable;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\OrderServicesTable;
use Modules\Admin\Models\OrderTable;
use Modules\Admin\Models\ServiceSerialTable;
use Modules\Admin\Models\ServiceTable;
use Modules\Admin\Models\SmsLogTable;
use Modules\Admin\Models\StockContractTable;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Models\StockOrderTable;
use Modules\Admin\Models\StockTable;
use Modules\Admin\Models\WalletTable;
use Modules\Admin\Models\WithdrawRequestGroupTable;
use Modules\Admin\Models\WithdrawRequestTable;
use Modules\Notification\Models\ConfigNotificationTable;
use Modules\Notification\Models\NotificationCustomerGroupTable;
use Modules\Notification\Models\NotificationDetailTable;
use Modules\Notification\Models\NotificationRewardMapTable;
use Modules\Notification\Models\NotificationRewardTable;
use Modules\Notification\Models\NotificationTable;
use Modules\Notification\Models\NotificationTemplateTable;
use Modules\Notification\Models\UserTable;
use Modules\Voucher\Models\ServiceVoucherTemplateTable;
use PDF;

class CustomerContractInterestRepository implements CustomerContractInterestRepositoryInterface
{
    protected $mContract;
    protected $mCustomerContractInterestByMonth;
    protected $mWithdrawRequest;
    protected $mCustomerContractInterestByDate;
    protected $mCustomerContractInterestByDateLog;
    protected $mCustomerContractInterestByTime;
    protected $mCustomerContractInterestByTimeLog;
    protected $mCustomer;
    protected $mOrder;
    protected $mWithdrawGroup;
    protected $mOrderService;
    protected $jobsEmailLogApi;
    protected $configNoti;
    protected $orderSerivce;
    protected $config;
    protected $jobEmail;
    protected $notiDetail;
    protected $service;
    protected $wallet;
    protected $serviceSerial;
    protected $notiTemplate;
    protected $rewart;
    protected $rewartMap;
    protected $noti;
    protected $serviceVoucherTemplate;
    protected $user;
    protected $notiCustomerGroup;


    public function __construct(
        CustomerContactTable $mContract,
        CustomerContractInterestByMonthTable $mCustomerContractInterestByMonth,
        WithdrawRequestTable $mWithdrawRequest,
        CustomerContractInterestByDateTable $mCustomerContractInterestByDate,
        CustomerContractInterestByDateLogTable $mCustomerContractInterestByDateLog,
        CustomerContractInterestByTimetable $mCustomerContractInterestByTime,
        CustomerContractInterestByTimeLogTable $mCustomerContractInterestByTimeLog,
        CustomerTable $mCustomer,
        OrderTable $mOrder,
        WithdrawRequestGroupTable $mWithdrawGroup,
        OrderServicesTable $mOrderService,
        JobsEmailLogApi $jobsEmailLogApi,
        ConfigNotificationTable $configNoti,
        OrderServicesTable $orderSerivce,
        ConfigTable $config,
        JobsEmailLogTable $jobEmail,
        NotificationDetailTable $notiDetail,
        ServiceTable $service,
        WalletTable $wallet,
        ServiceSerialTable $serviceSerial,
        NotificationTemplateTable $notiTemplate,
        NotificationRewardTable $rewart,
        NotificationRewardMapTable $rewardMap,
        NotificationTable $noti,
        ServiceVoucherTemplateTable $serviceVoucherTemplate,
        UserTable $user,
        NotificationCustomerGroupTable $notiCustomerGroup
    )
    {
        $this->mContract = $mContract;
        $this->mCustomerContractInterestByMonth = $mCustomerContractInterestByMonth;
        $this->mWithdrawRequest = $mWithdrawRequest;
        $this->mCustomerContractInterestByDate = $mCustomerContractInterestByDate;
        $this->mCustomerContractInterestByDateLog = $mCustomerContractInterestByDateLog;
        $this->mCustomerContractInterestByTime = $mCustomerContractInterestByTime;
        $this->mCustomerContractInterestByTimeLog = $mCustomerContractInterestByTimeLog;
        $this->mCustomer = $mCustomer;
        $this->mOrder = $mOrder;
        $this->mWithdrawGroup = $mWithdrawGroup;
        $this->mOrderService = $mOrderService;
        $this->jobsEmailLogApi = $jobsEmailLogApi;
        $this->configNoti = $configNoti;
        $this->orderSerivce = $orderSerivce;
        $this->config = $config;
        $this->jobEmail = $jobEmail;
        $this->notiDetail = $notiDetail;
        $this->service = $service;
        $this->wallet = $wallet;
        $this->serviceSerial = $serviceSerial;
        $this->notiTemplate = $notiTemplate;
        $this->noti = $noti;
        $this->rewart = $rewart;
        $this->rewartMap = $rewardMap;
        $this->serviceVoucherTemplate = $serviceVoucherTemplate;
        $this->user = $user;
        $this->notiCustomerGroup = $notiCustomerGroup;
    }

    public function calculateContract($time = 'month',$customer_contract_id = [],$type = null)
    {
        $filter['now'] = Carbon::now()->format('Y-m-d H:i:s');
        if ($time == 'month') {
            $endDayOfMonth = Carbon::now()->endOfMonth()->day;
            $day = Carbon::now()->day;
//            $arrDay = [];
            if ($endDayOfMonth == $day) {
                for ($i = $day ; $i <= 31 ; $i++) {
                    $filter['day'][$i] = $i;
                }
            } else {
                $filter['day'][$day] = $day;
            }
        }

        /**
         * Danh sách contract phù hợp với điều kiện
         * 1. is_active = 1.
         * 2. is_deleted = 0.
         * 3. Ngày bắt đầu <= now
         * 4. kết thúc hợp đồng >= now
         */
        $listContract = $this->mContract->getAll($filter);
        switch ($time) {
            //Tính theo tháng
            case "day":
                $this->calculateContractDay($listContract);
                break;
            case "time":
                $this->calculateContractTime($listContract,$customer_contract_id,$type);
                break;
            default:
                $listContract = $this->mContract->getAll($filter);
                $this->calculateContractMonth($listContract);;
        }
    }

//    Tính lại ngày có thể rút tiền trùng với ngày bắt đầu hợp đồng
    public function withdrawDatePlanning($customerContractId,$withdrawDatePlanning,$wit){
//        Lấy ngày bắt đầu hợp đồng
        $dayStartContract = Carbon::parse($withdrawDatePlanning)->day;
//        Set lại thời gian ngày bắt đầu hợp đồng để khi thêm tháng sẽ không bị nhảy sang tháng khác
        $withdrawDatePlanning = Carbon::parse($withdrawDatePlanning)->firstOfMonth();
        $lastInterestByMonth = $this->mCustomerContractInterestByMonth->getLastInterestByMonth($customerContractId);
        if ($lastInterestByMonth != null) {
            if (Carbon::parse($lastInterestByMonth['withdraw_date_planning'])->format('Y-m-d') < Carbon::now()->format('Y-m-d')) {
                $withdrawDatePlanningTmp = Carbon::parse($lastInterestByMonth['withdraw_date_planning'])->firstOfMonth()->addMonth($wit);
            } else {
                $withdrawDatePlanningTmp = $lastInterestByMonth['withdraw_date_planning'];
            }
        } else {
            $tempTime = Carbon::parse($withdrawDatePlanning);
            //Cộng thêm số tháng withdraw_interest_time.
            $tempTime2 = $tempTime->addMonths($wit)->format('Y-m-d H:i:s');
            $withdrawDatePlanningTmp = $tempTime2;
        }

//        Kiểm tra ngày của tháng sau khi cộng thêm tháng
        if (Carbon::parse($withdrawDatePlanningTmp)->endOfMonth()->day < $dayStartContract){
            $withdrawDatePlanning = Carbon::parse($withdrawDatePlanningTmp)->endOfMonth()->format('Y-m-d 00:00:00');
        } else {
            $withdrawDatePlanning = Carbon::parse($withdrawDatePlanningTmp)->format('Y-m-'.$dayStartContract.' 00:00:00');
        }

        return $withdrawDatePlanning;
    }

    public function contractByMonth($item,$year,$month,$now){
        $customerContractId = $item['customer_contract_id'];

        $interestRateByMonth = ((double)$item['interest_rate']/(12*100));
        //interest_amount Giá trị lãi xuất interest_rate_by_month*total_amount
        $interestAmount = $interestRateByMonth * $item['total_amount'];

        //interest_total_amount Giá trị lãi xuất tích lũy n-1 +  interest_amount
        //Lãi suất tích lũy (n-1)
        $interestTotalAmountAfter = $this->mCustomerContractInterestByMonth
            ->lastCustomerContractInterest($customerContractId);
        $interestTotalAmount = $interestAmount;
        if ($interestTotalAmountAfter != null) {
            $interestTotalAmount += $interestTotalAmountAfter['interest_total_amount'];
        }
        /**
         * interest_banlance_amount
         * Giá trị lãi xuất có thể rút:
         * Tổng tất cả các tháng
         * có confirmed_by # null - tổng tiền lãi đã rút
         * (withdraw_request của contract có status là done).
         */
        //Tổng tiền tất cả các tháng confirmed_by # null
        $confirmedDB = $this->mCustomerContractInterestByMonth
            ->getConfirmByNotNullOfContract($customerContractId);
        $confirmed = 0;
        if ($confirmedDB != null && $confirmedDB['interest_amount'] != null) {
            $confirmed = $confirmedDB['interest_amount'] + $interestAmount;
        }else {
            $confirmed = $interestTotalAmount;
        }
        //Tổng tiền lãi đã rút
        $pa = [
            'customer_contract_id' => $customerContractId,
            'withdraw_request_status' => 'done',
            'withdraw_request_type' => 'interest'
        ];
        $withdrawRequestDone = 0;
        $withdrawRequestDoneDB = $this->mWithdrawRequest
            ->sumContractStatus($pa);
        if ($withdrawRequestDoneDB != null) {
            $withdrawRequestDone = $withdrawRequestDoneDB['withdraw_request_amount'];
        }
        $interestBalanceAmountTemp = $confirmed - $withdrawRequestDone;
        $interestBalanceAmount = $interestBalanceAmountTemp < 0 ? 0 : $interestBalanceAmountTemp;

        /**
         * Ngày rút tiền theo kế hoạch:
         * Last withdraw_request + customer_contract.withdraw_interest_time
         * Nếu Last withdraw_request = null
         * thì lấy customer_contract.customer_contract_start_date thế vô
         */
        $withdrawDatePlanning = $item['customer_contract_start_date'];
        $wit = (int) $item['withdraw_interest_time'];

        $withdrawDatePlanning = $this->withdrawDatePlanning($customerContractId,$withdrawDatePlanning,$wit);

//        Kiểm tra nếu lãi tháng là ngày cuối cùng thì cập nhật lại thời gian kết thúc hợp đồng để dừng hợp đồng
//        Tạo lãi ngày dựa theo tổng tiền của lãi tháng mới
//        Tạo lãi thời điểm dựa theo tổng tiền của lãi tháng mới
        if ($item['term_time_type'] == 1){
//            Lấy record ngày mới nhất
            $lastedDay = $this->mCustomerContractInterestByDate->getLasted($item['customer_contract_id']);
            $IdLastedDay = $lastedDay['customer_contract_interest_by_date_id'];
            unset($lastedDay['customer_contract_interest_by_date_id']);
            $lastedDay = collect($lastedDay)->toArray();
//            Lấy record thời điểm mới nhất
            $lastedTime = $this->mCustomerContractInterestByTime->getLasted($item['customer_contract_id']);
            $idLastedTime = $lastedTime['customer_contract_interest_by_time_id'];
            unset($lastedTime['customer_contract_interest_by_time_id']);
            $lastedTime = collect($lastedTime)->toArray();

            $dayNow = (int) Carbon::now()->format('d');
            $timeNow = Carbon::now()->format('H:i:s');

//            Cập nhật lãi ngày và thời điểm
            $lastedDay['interest_total_amount'] = $interestTotalAmount - $lastedDay['interest_amount'];
            $lastedDay['created_by'] = 1;
            $lastedDay['updated_by'] = 1;
            $lastedDay['created_at'] = $now;
            $lastedDay['updated_at'] = $now;

            $lastedTime['interest_total_amount'] = $interestTotalAmount - $lastedTime['interest_amount'];
            $lastedTime['created_by'] = 1;
            $lastedTime['updated_by'] = 1;
            $lastedTime['created_at'] = $now;
            $lastedTime['updated_at'] = $now;

            if (Carbon::parse($item['customer_contract_end_date'])->format('Y-m-d') == Carbon::now()->format('Y-m-d')){
                $this->mContract->updateContract($item['customer_contract_id'],['customer_contract_end_date' => Carbon::now()->format('Y-m-d 00:00:00')]);

                $lastedDay['interest_month'] = $month;
                $lastedDay['interest_day'] = $dayNow;
                $lastedDay['interest_total_amount'] = $interestTotalAmount;
                $lastedTime['interest_total_amount'] = $interestTotalAmount;
                $lastedTime['interest_month'] = $month;
                $lastedTime['interest_day'] = $dayNow;
                $lastedTime['interest_time'] = $timeNow;

                $this->mCustomerContractInterestByDate->add($lastedDay);
                $this->mCustomerContractInterestByTime->add($lastedTime);
            } else {
                //            Tạo record mới
                $this->mCustomerContractInterestByDate->updateInterestDate($IdLastedDay,$lastedDay);
                $this->mCustomerContractInterestByTime->updateInterestTime($idLastedTime,$lastedTime);
            }
        }

        return [
            'customer_contract_id' => $item['customer_contract_id'],
            'customer_id' => $item['customer_id'],
            'interest_year' => $year,
            'interest_month' => $month,
            'interest_rate_by_month' => $interestRateByMonth,
            'interest_amount' => $interestAmount,
            'interest_total_amount' => $interestTotalAmount,
            'interest_banlance_amount' => $item['term_time_type'] == 1 ? $interestBalanceAmount : 0,
//            'withdraw_date_planning' => $item['term_time_type'] == 1 ? $withdrawDatePlanning : $now,
            'withdraw_date_planning' => $item['term_time_type'] == 1 ? $withdrawDatePlanning : $now,
//            'withdraw_date_planning' => $now,
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => $now,
            'updated_at' => $now
        ];
    }

    /**
     * Trường hợp tính theo tháng
     * @param $listContract
     */
    private function calculateContractMonth($listContract)
    {
        try {
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $dataInsert = [];
            $year = (int) Carbon::now()->format('Y');
            $month = (int) Carbon::now()->format('m');
            //Xóa hết tất cả trong năm tháng hiện tại.
            foreach ($listContract as $item) {
                if($item['term_time_type'] == 1) {
                    //Insert vào customer_contract_interest_by_month
                    $dataInsert[] = $this->contractByMonth($item,$year,$month,$now);
                }

                if (count($dataInsert) >= 500) {
                    $this->mCustomerContractInterestByMonth->addInsert($dataInsert);
                }
            }

            if ($dataInsert != []) {
                //Insert vào customer_contract_interest_by_month
                $this->mCustomerContractInterestByMonth->addInsert($dataInsert);
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function contractByDate($item,$year,$month,$day,$now){
        $customerContractId = $item['customer_contract_id'];
        $interestRateByDate = ((double)$item['interest_rate']/(365*100));
        $interestAmount = $interestRateByDate * $item['total_amount'];

        //interest_total_amount Giá trị lãi xuất tích lũy n-1 +  interest_amount
        //Lãi suất tích lũy (n-1)
        $interestTotalAmountAfter = $this->mCustomerContractInterestByDateLog
            ->lastCustomerContractInterest($customerContractId);

        if ($interestTotalAmountAfter == null){

            $interestTotalAmountAfter = $this->mCustomerContractInterestByDate
                ->lastCustomerContractInterest($customerContractId);
        }

        $interestTotalAmount = $interestAmount;
        if ($interestTotalAmountAfter != null) {
            $interestTotalAmount += $interestTotalAmountAfter['interest_total_amount'];
        }
        /**
         * interest_banlance_amount
         * Giá trị lãi xuất có thể rút:
         * Tổng tất cả các tháng
         * có confirmed_by # null - tổng tiền lãi đã rút
         * (withdraw_request của contract có status là done).
         */
        //Tổng tiền tất cả các tháng confirmed_by # null
        $confirmedDB = $this->mCustomerContractInterestByMonth
            ->getConfirmByNotNullOfContract($customerContractId);
        $confirmed = 0;
        if ($confirmedDB != null && $confirmedDB['interest_amount'] != null) {
            $confirmed = $confirmedDB['interest_amount'];
        }
        //Tổng tiền lãi đã rút
        $pa = [
            'customer_contract_id' => $customerContractId,
            'withdraw_request_status' => 'done',
            'withdraw_request_code' => 'interest'
        ];

        $withdrawRequestDone = 0;
        $withdrawRequestDoneDB = $this->mWithdrawRequest
            ->sumContractStatus($pa);
        if ($withdrawRequestDoneDB != null) {
            $withdrawRequestDone = $withdrawRequestDoneDB['withdraw_request_amount'];
        }
        $interestBalanceAmountTemp = $confirmed - $withdrawRequestDone;
        $interestBalanceAmount = $interestBalanceAmountTemp < 0 ? 0 : $interestBalanceAmountTemp;

        /**
         * Ngày rút tiền theo kế hoạch:
         * Last withdraw_request + customer_contract.withdraw_interest_time
         * Nếu Last withdraw_request = null
         * thì lấy customer_contract.customer_contract_start_date thế vô
         */
        $withdrawDatePlanning = $item['customer_contract_start_date'];
        //Last withdraw_request của contract
        $lastWithdrawRequest = $this->mWithdrawRequest
            ->lastCustomerContract($customerContractId);
        $wit = (int) $item['withdraw_interest_time'];

        $getLastContractConfirm = $this->mCustomerContractInterestByMonth->getLastContractConfirm($item['customer_contract_id']);
        if ($getLastContractConfirm != null) {
            $withdrawDatePlanning = $getLastContractConfirm['withdraw_date_planning'];
        } else {
            $withdrawDatePlanning = $this->withdrawDatePlanning($customerContractId,$withdrawDatePlanning,$wit);
        }

        $dataTmp = [
            'customer_contract_id' => $item['customer_contract_id'],
            'customer_id' => $item['customer_id'],
            'interest_year' => $year,
            'interest_month' => $month,
            'interest_day' => $day,
            'interest_rate_by_day' => $interestRateByDate,
            'interest_amount' => $interestAmount,
            'interest_total_amount' => $interestTotalAmount,
            'interest_balance_amount' => $item['term_time_type'] == 1 ? $interestBalanceAmount : 0,
            'withdraw_date_planning' => $item['term_time_type'] == 1 ? $withdrawDatePlanning : $now,
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => $now,
            'updated_at' => $now
        ];

//        Xoá log ngày cũ
        $this->mCustomerContractInterestByDateLog->removeLogDate($item['customer_contract_id']);

//        Thêm log ngày mới
        $this->mCustomerContractInterestByDateLog->addLogDate($dataTmp);

        return $dataTmp;
    }

    /**
     * Trường hợp tính theo ngày
     * @param $listContract
     */
    private function calculateContractDay($listContract)
    {
        try {
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $dataInsert = [];
            $year = (int) Carbon::now()->format('Y');
            $month = (int) Carbon::now()->format('m');
            $day = (int) Carbon::now()->format('d');

//            $this->mCustomerContractInterestByDate
//                ->removeByMonthYearDay($year, $month, $day);
            foreach ($listContract as $item) {
                if ($item['term_time_type'] == 1) {

                    $dataInsert[] = $this->contractByDate($item,$year,$month,$day,$now);
                    if (count($dataInsert) >= 500) {
                        $this->mCustomerContractInterestByDate->addInsert($dataInsert);
                    }
                }
            }

            if ($dataInsert != []) {
                //Insert vào customer_contract_interest_by_month
                $this->mCustomerContractInterestByDate->addInsert($dataInsert);
            }
        } catch (\Exception $e) {
        }
    }
    /**
     * Trường hợp tính theo ngày
     * @param $listContract
     */
    private function calculateContractTime($listContract , $customer_contract_id = [] , $type = null)
    {
        try {
            $now = Carbon::now()->format('Y-m-d H:i:s');
            $dataInsert = [];
            $year = (int) Carbon::now()->format('Y');
            $month = (int) Carbon::now()->format('m');
            $day = (int) Carbon::now()->format('d');
            $time = Carbon::now()->format('H:i:s');

            if (count($customer_contract_id) != 0) {
                $filter['customer_contract_id'] = $customer_contract_id;
                $listContract = $this->mContract->getAllById($filter);
            }

//            Cập nhật trạng thái hợp đồng với cái hợp đồng hết hạn
            foreach ($listContract as $item) {

                if ($item['term_time_type'] == 0) {
//                        Lấy chi tiết hợp đồng
                    $detailContract = $this->mContract->getDetailContract($item['customer_contract_id']);
//                    Lấy thông tin lãi theo tháng và họp đồng mới nhất
                    $idContractByMonth = $this->mCustomerContractInterestByMonth->getInfoInterestByMonth($item['customer_contract_id']);
                    if ($idContractByMonth == null || ($idContractByMonth['interest_year'] != $year || $idContractByMonth['interest_month'] != $month)) {
//                        Tạo lãi tháng cho hợp đồng k kỳ hạn
                        $interestMonth = $this->contractByMonth($detailContract,$year,$month,$now);
                        $interestMonth['interest_total_amount'] = $idContractByMonth == null ? 0 : $idContractByMonth['interest_total_amount'];
                        $interestMonth['withdraw_date_planning'] = $now;
                        $interestMonth['confirmed_by'] = 1;
                        $interestMonth['confirmed_at'] = $now;
                        $this->mCustomerContractInterestByMonth->addInsert($interestMonth);
                    }

//                    Lấy thông tin lãi theo ngày và họp đồng mới nhất

                    $idContractByDate = $this->mCustomerContractInterestByDateLog->getInfoInterestByDate($item['customer_contract_id']);
                    if ($idContractByDate == null){
                        $idContractByDate = $this->mCustomerContractInterestByDate->getInfoInterestByDate($item['customer_contract_id']);
                    }

                    if ($idContractByDate == null || $idContractByDate['interest_year'] != $year || $idContractByDate['interest_month'] != $month || $idContractByDate['interest_day'] != $day) {

//                        Tạo lãi ngày cho hợp đồng k kỳ hạn
                        $interestDate = $this->contractByDate($detailContract,$year,$month,$day,$now);
                        $interestDate['withdraw_date_planning'] = $now;
                        $interestDate['interest_total_amount'] = $idContractByDate == null ? 0 : $idContractByDate['interest_total_amount'];
                        $this->mCustomerContractInterestByDate->addInsert($interestDate);
                    }
                }

                $customerContractId = $item['customer_contract_id'];
                //Record cuối cùng của contract trong ngày
                $interestRateByDay = ((double)$item['interest_rate']/ (365*100)*5);
                $interestRateByDate = ((double)$item['interest_rate']/ (365*24*60*100)*5);
                $interestAmount = $interestRateByDate * $item['total_amount'];

                //interest_total_amount Giá trị lãi xuất tích lũy n-1 +  interest_amount
                //Lãi suất tích lũy (n-1)

//                Lãi thời điểm theo hd

                $interestTotalAmountAfter = $this->mContract->getItemInterest($customerContractId);

                if ($interestTotalAmountAfter['by_time_total_interest'] == 0){
                    $interestTotalAmountAfter = $this->mCustomerContractInterestByTimeLog
                        ->lastCustomerContractInterest($customerContractId);
                    if ($interestTotalAmountAfter == null) {
                        $interestTotalAmountAfter = $this->mCustomerContractInterestByTime
                            ->lastCustomerContractInterest($customerContractId);
                    }

                } else {
                    $interestTotalAmountAfter['interest_total_amount'] = $interestTotalAmountAfter['by_time_total_interest'];
                }

                $interestTotalAmount = $interestAmount;
                if ($interestTotalAmountAfter != null) {
                    $interestTotalAmount += $interestTotalAmountAfter['interest_total_amount'];
                }
                /**
                 * interest_banlance_amount
                 * Giá trị lãi xuất có thể rút:
                 * Tổng tất cả các tháng
                 * có confirmed_by # null - tổng tiền lãi đã rút
                 * (withdraw_request của contract có status là done).
                 */
                //Tổng tiền tất cả các tháng confirmed_by # null
                if ($item['term_time_type'] == 1) {
                    $confirmedDB = $this->mCustomerContractInterestByMonth
                        ->getConfirmByNotNullOfContract($customerContractId);
                    $confirmed = 0;
                    if ($confirmedDB != null && $confirmedDB['interest_amount'] != null) {
                        $confirmed = $confirmedDB['interest_amount'];
                    }
                } else {
                    $confirmedDB = $this->mCustomerContractInterestByTime
                        ->lastCustomerContractInterest($customerContractId);
                    $confirmed = $interestAmount;
                    if ($confirmedDB != null && $confirmedDB['interest_total_amount'] != null) {
                        if (count($customer_contract_id) == 0) {
                            $confirmed = $confirmed + $confirmedDB['interest_total_amount'];
                        } else {
                            $confirmed = $confirmedDB['interest_total_amount'];
                        }
                    }
                }
                //Tổng tiền lãi đã rút
                $pa = [
                    'customer_contract_id' => $customerContractId,
                    'withdraw_request_status' => 'done',
                    'withdraw_request_type' => 'interest'
                ];

                $withdrawRequestDone = 0;
                $withdrawRequestDoneDB = $this->mWithdrawRequest
                    ->sumContractStatus($pa);
                if ($withdrawRequestDoneDB != null) {
                    $withdrawRequestDone = $withdrawRequestDoneDB['withdraw_request_amount'];
                }
                $interestBalanceAmountTemp = $confirmed - $withdrawRequestDone;
                $interestBalanceAmount = $interestBalanceAmountTemp < 0 ? 0 : $interestBalanceAmountTemp;

                /**
                 * Ngày rút tiền theo kế hoạch:
                 * Last withdraw_request + customer_contract.withdraw_interest_time
                 * Nếu Last withdraw_request = null
                 * thì lấy customer_contract.customer_contract_start_date thế vô
                 */
                $withdrawDatePlanning = $item['customer_contract_start_date'];
                //Last withdraw_request của contract
                $lastWithdrawRequest = $this->mWithdrawRequest
                    ->lastCustomerContract($customerContractId);
                $wit = (int) $item['withdraw_interest_time'];

                $getLastContractConfirm = $this->mCustomerContractInterestByMonth->getLastContractConfirm($item['customer_contract_id']);
                if ($getLastContractConfirm != null) {
                    $withdrawDatePlanning = $getLastContractConfirm['withdraw_date_planning'];
                } else {
                    $withdrawDatePlanning = $this->withdrawDatePlanning($customerContractId,$withdrawDatePlanning,$wit);
                }

//                Kiểm tra hợp đồng không sinh lãi được nữa
                $checkContract = 0;
//                if (count($customer_contract_id) != 0 && $type == null && (
//                    $item['is_active'] == 0 ||
//                    $item['is_deleted'] == 1 ||
//                    Carbon::parse($item['customer_contract_end_date']) < Carbon::now())
//                ){
//                    $checkContract = 1;
//                }

                if($checkContract == 1) {
                    $interestTotalAmount = $interestTotalAmount - $interestAmount;
                    $interestBalanceAmount = 0;
                }

                //Insert vào customer_contract_interest_by_month
                $tmpValue = [];
                $tmpValue = [
                    'customer_contract_id' => $item['customer_contract_id'],
                    'customer_id' => $item['customer_id'],
                    'interest_year' => $year,
                    'interest_month' => $month,
                    'interest_day' => $day,
                    'interest_rate_by_day' => $interestRateByDay,
                    'interest_time' => $time,
                    'interest_rate_by_time' => $interestRateByDate,
                    'interest_amount' => $interestAmount,
                    'interest_total_amount' => $interestTotalAmount,
                    'interest_banlance_amount' => $interestBalanceAmount,
//                    'withdraw_date_planning' => $item['term_time_type'] == 1 ? $withdrawDatePlanning : $now,
                    'withdraw_date_planning' => $item['term_time_type'] == 1 ? $withdrawDatePlanning : $now,
                    'created_by' => 1,
                    'updated_by' => 1,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];

                $dataInsert[] = $tmpValue;

                $this->mContract->updateContract($customerContractId,['by_time_total_interest' => $interestTotalAmount]);

//                Xoá log theo hợp đồng
                $this->mCustomerContractInterestByTimeLog->removeByContract($item['customer_contract_id']);
                $this->mCustomerContractInterestByTimeLog->addLog($tmpValue);

//                Kiểm tra cập nhật lãi tháng và lãi ngày
                if ($item['term_time_type'] == 0) {
//                        Lấy chi tiết hợp đồng
                    $detailContract = $this->mContract->getDetailContract($item['customer_contract_id']);
//                    Lấy thông tin lãi theo tháng và họp đồng mới nhất
                    $idContractByMonth = $this->mCustomerContractInterestByMonth->getInfoInterestByMonth($item['customer_contract_id']);
//                        Cập nhật lãi tháng
                    $idContractByMonth['interest_total_amount'] = $checkContract == 0 ? $idContractByMonth['interest_total_amount'] + $interestAmount : $idContractByMonth['interest_total_amount'];
                    $idContractByMonth['interest_banlance_amount'] = $interestBalanceAmount;
                    $id = $idContractByMonth['customer_contract_interest_by_month_id'];
                    unset($idContractByMonth['customer_contract_interest_by_month_id']);
                    $this->mCustomerContractInterestByMonth->updateInterestMonth($id,collect($idContractByMonth)->toArray());

//                    Lấy thông tin lãi theo ngày và họp đồng mới nhất

                    $idContractByDate = $this->mCustomerContractInterestByDate->getInfoInterestByDate($item['customer_contract_id']);
                    //                        Cập nhật lãi ngày
                    $idContractByDate['interest_total_amount'] = $checkContract == 0 ? $idContractByDate['interest_total_amount'] + $interestAmount : $idContractByDate['interest_total_amount'];
                    $idContractByDate['interest_balance_amount'] = $interestBalanceAmount;
                    $id = $idContractByDate['customer_contract_interest_by_date_id'];
                    unset($idContractByDate['customer_contract_interest_by_date_id']);
                    $this->mCustomerContractInterestByDate->updateInterestDate($id,collect($idContractByDate)->toArray());

                }
                if (count($dataInsert) >= 500) {
                    $this->mCustomerContractInterestByTime->addInsert($dataInsert);
                }
            }

            if ($dataInsert != []) {
                //Insert vào customer_contract_interest_by_month
                $this->mCustomerContractInterestByTime->addInsert($dataInsert);
            }
        } catch (\Exception $e) {
            dd($e->getMessage(),$e->getLine());
        }
    }

    public function accountStatement()
    {
        try {
//        Lấy danh sách khách hàng
            $listCustomer = $this->mCustomer->getAll();
//            function sao kê tài khoản theo từng tháng
            $this->accountStatementAction($listCustomer);

//            Gửi noti các ngày sinh nhật khi đến ngày sinh của khách hàng
            $this->happyBirthDay();
//
////            Gửi noti các ngày lễ đến ngày lễ tự gửi noti
            $this->eventAction();

        } catch (\Exception $e) {
            dd($e->getMessage());
        }

    }

    public function accountStatementAction($listCustomer){
        $data = [];
//            Lấy tháng hiện tại

        $endDayOfMonth = Carbon::now()->endOfMonth()->day;
        $day = Carbon::now()->day;
        $month = Carbon::now()->format('Y-m');
        $startMonth = Carbon::now()->startOfMonth()->format('d/m/Y');
        $endMonth = Carbon::now()->endOfMonth()->format('d/m/Y');
        $accountStatementM = new AccountStatementTable();
        $accountStatementMapM = new AccountStatementMapContractTable();
        $stockOrder = new StockOrderTable();
        $checkVersion = new CheckVersionTable();
        $getLinkVersion = $checkVersion->getListVersion();
        if ($day == $endDayOfMonth) {
            $insertEmail = [];
            $n = 1;

//            Lấy email nhân viên
            $emailStaff = $this->config->getInfoByKey(['email_staff']);
            $phoneStaff = $this->config->getInfoByKey(['phone_staff']);
            $insertEmailInterestInMonth = [];
            $stockCustomerDB = new StockCustomerTable();
            foreach ($listCustomer as $item) {
                $data = [];
                $data['info'] = [
                    'user_name' => $item['full_name'],
                    'phone' => $item['phone'],
                    'customer_code' => $item['customer_code'],
                    'contact_address' => $item['contact_address'],
                    'ic_no' => $item['ic_no'],
//                    'date_statement' => $startMonth.' - '.$endMonth
                    'date_statement' => Carbon::now()->format('d-m-Y')
                ];

                $data['total']['in'] = 0;
                $data['total']['out'] = 0;
                $data['total']['fee'] = 0;
                $data['list'] = [];
                foreach ($getLinkVersion as $itemLink){
                    if($itemLink['platform'] == 'android'){
                        $data['link_android'] = $itemLink['link'];
                    } else if ($itemLink['platform'] == 'ios'){
                        $data['link_ios'] = $itemLink['link'];
                    }
                }

                $listContract = $this->mContract->getListContractByCustomer($item['customer_id']);
                if (count($listContract) != 0) {
                    $accountStatement = $accountStatementM->createAccountStatement(
                        [
                            'customer_id' => $item['customer_id'],
                            'month' => $month,
                            'user_name' => $item['full_name'],
                            'date_satement' => Carbon::now(),
                            'phone' => $item['phone']
                        ]
                    );
                }
                $insertEmail = [];
                $data['pdf']['bond'] = []; //  Danh sách lãi theo trái phiếu
                $data['pdf']['saving'] = []; //  Danh sách lãi theo tiết kiệm
                $data['pdf']['stock'] = []; //  Danh sách lãi theo cổ phiếu
                $data['pdf']['total'] = [
                    'bond' => 0,
                    'saving' => 0,
                    'stock' => 0,
                ]; //  Tổng tiền theo từng loại

                $stockCustomer = $stockCustomerDB->getInfoStockByCustomer($item['customer_id']);

                //                Tổng tiền đầu tư của cổ phiếu
                $getTotalMoney = $stockOrder->getTotalMoney($item['customer_id']);

                $data['pdf']['stock'] = [
                    'stock_contract_code' => $stockCustomer != null ? $stockCustomer['stock_contract_code'] : '' ,
                    'wallet_stock_money' => $stockCustomer != null && $stockCustomer['wallet_stock_money'] != null ? $stockCustomer['wallet_stock_money'] : 0,
                    'wallet_dividend_money' => $stockCustomer != null && $stockCustomer['wallet_dividend_money'] != null ? $stockCustomer['wallet_dividend_money'] : 0,
                    'stock' => $stockCustomer != null && $stockCustomer['stock'] != null ? $stockCustomer['stock'] : 0,
                    'stock_bonus' => $stockCustomer != null && $stockCustomer['stock_bonus'] != null ? $stockCustomer['stock_bonus'] : 0,
                ];

                $data['pdf']['total']['stock'] = $getTotalMoney;
                foreach ($listContract as $itemContract) {
                    if ($itemContract['product_category_id'] == 1) {
                        $data['pdf']['bond'][] = [
                            'customer_contract_code' => $itemContract['customer_contract_code'],
                            'total_amount' => $itemContract['total_amount'],
                            'interest' => Carbon::parse($itemContract['withdraw_date_planning_month'])->format('Y-m') == Carbon::now()->format('Y-m') ? $itemContract['month_interest_amount'] : 0,
                            'interest_total_amount_month' => $itemContract['interest_total_amount_month'],
                            'interest_banlance_amount' => $itemContract['interest_banlance_amount'],
                        ];
                        $data['pdf']['total']['bond'] += $itemContract['total_amount'];
                    } else {
                        $data['pdf']['saving'][] = [
                            'customer_contract_code' => $itemContract['customer_contract_code'],
                            'total_amount' => $itemContract['total_amount'],
                            'interest' => Carbon::parse($itemContract['withdraw_date_planning_month'])->format('Y-m') == Carbon::now()->format('Y-m') ? $itemContract['month_interest_amount'] : 0,
                            'interest_total_amount_month' => $itemContract['interest_total_amount_month'],
                            'interest_banlance_amount' => $itemContract['interest_banlance_amount']
                        ];
                        $data['pdf']['total']['saving'] += $itemContract['total_amount'];
                    }

                    $data['contract'][$itemContract['customer_contract_code']] = [
                        'account_statement_id' => $accountStatement,
                        'customer_contract_id' => $itemContract['customer_contract_id'],
                        'customer_contract_code' => $itemContract['customer_contract_code'],
                        'total_before' => 0,
                        'money_in' => 0,
                        'money_out' => 0
                    ];
                    $data['contract'][$itemContract['customer_contract_code']]['total_amount'] = $itemContract['total_amount'];



                    $listInterestNow = $this->mCustomerContractInterestByMonth->getInterestContractByMonthCount($item['customer_id'],$itemContract['customer_contract_id'],$month,'accountStatement');
                    if (isset($listInterestNow['total'])) {
                        $data['contract'][$itemContract['customer_contract_code']]['money_in'] = (double)$listInterestNow['total'];
                    }
//                    Lấy số tiền lãi của tháng

                    $getTotalInterestByTime = $this->mCustomerContractInterestByMonth->getInterestContractByMonthCount($item['customer_id'],$itemContract['customer_contract_id'],$month,'accountStatement');
                    if ($getTotalInterestByTime != null) {
                        $insertEmailInterestInMonth['list'][] = [
                            'full_name' => $item['full_name'],
                            'phone' => $item['phone'],
                            'customer_contract_code' => $itemContract['customer_contract_code'],
                            'total_contract' => $itemContract['total_amount'],
                            'total_amount' => isset($getTotalInterestByTime['total']) ? $getTotalInterestByTime['total'] : 0,
                            'withdraw_date_planning' => isset($getTotalInterestByTime['withdraw_date_planning']) && $getTotalInterestByTime['withdraw_date_planning'] != '' ? Carbon::parse($getTotalInterestByTime['withdraw_date_planning'])->format('d-m-Y') : '',
                            'staff_name' => $itemContract['staff_full_name']
                        ];
                    }
                    $moneySMS = isset($getTotalInterestByTime['total']) ? $getTotalInterestByTime['total'] : 0;
                    $message = 'VSETGROUP xin thong bao. Hop dong so '.$itemContract['customer_contract_code'].', goi '.$itemContract['product_name_vi'].', vi lai '.$moneySMS.' VND.Thoi gian thong bao vao luc '.Carbon::now()->format('H:i:s d/m/Y').' .Quy khach lien he CSKH de nhan duoc tu van. Hotline: 090739999';
                    $send = [
                        'message' => $message,
                        'customer_name' => $item['full_name'],
                        'phone' => $item['phone']
                    ];
                    $this->sendSMS($send);
                }
                $pdf = PDF::loadView('admin::pdf.statement-view',['user' => $data['info'],'list' => $data['pdf']]);
                $path = 'uploads/SAO_KE_'.$item['customer_code'].'_'.Carbon::now()->format('d_m_Y').'.pdf';
                $pdf->save($path);
                $data['path_pdf'] = url('/').'/'.$path;
                $insertEmail = [
                    'obj_id' => $item['customer_id'],
                    'email_type' => "account_statement",
                    'email_subject' => 'Sao kê tài khoản',
                    'email_from' => env('MAIL_USERNAME'),
                    'email_to' => $item['email'],
                    'email_params' => json_encode($data),
                    'is_run' => 0
                ];
                if (count($listContract) != 0){
                    $accountStatementMapM->createAccountStatement(array_values($data['contract']));
                    $this->jobEmail->addJob($insertEmail);
                }
            }
            if (count($insertEmailInterestInMonth) != 0) {
                $insertEmailInterestInMonth['month'] = Carbon::now()->format('m-Y');
                $insertEmailByTime = [
                    'obj_id' => '',
                    'email_type' => "month_profit",
                    'email_subject' => 'Chốt lãi tháng',
                    'email_from' => env('MAIL_USERNAME'),
                    'email_to' => $emailStaff['value'],
                    'email_params' => json_encode($insertEmailInterestInMonth),
                    'is_run' => 0
                ];

                $this->jobEmail->addJob($insertEmailByTime);
            }

        //            Email thông báo gia hạn đầu tư
            $dayNow = Carbon::now()->format('Y-m-d');
            $config = $this->config->getInfoByKey('warning_extend');
            $dayExtend = Carbon::parse($dayNow)->addDays(15)->format('Y-m-d');
            if ($config != null ) {
                if ($config['value'] != null) {
                    $dayExtend = Carbon::parse($dayNow)->addDays($config['value'])->format('Y-m-d');
                }
            }

            $getListWarningContract = $this->mContract->listContractWaringExtend($dayExtend);
            if (count($getListWarningContract) != 0) {
                $arr = [];
                foreach ($getListWarningContract as $item) {
                    $arr['list'][] = [
                        'full_name' => $item['customer_name'],
                        'phone' => $item['customer_phone'],
                        'customer_contract_code' => $item['customer_contract_code'],
                        'total_contract' => $item['total_amount'],
                        'customer_contract_end_date' => $item['customer_contract_end_date'] != null ? Carbon::parse($item['customer_contract_end_date'])->format('d-m-Y') : '',
                        'staff_name' => $item['staff_full_name']
                    ];

                    if ($item['customer_contract_end_date'] != null) {
                        $dataWarning = [
                            'phone' => $phoneStaff['value'],
                            'customer_name' => $item['customer_name'],
                            'message' => 'VSETGROUP kinh chao quy khach. Hop dong so '.$item['customer_contract_code'].' sẽ hết hạn vào ngày '. Carbon::parse($item['customer_contract_end_date'])->format('d-m-Y') .' Nhan vien ho tro hop dong gia han so '.$phoneStaff['value'].' Tham khao cac goi dau tu tai https://traiphieu.vsetgroup.vn/'
                        ];

                        $this->sendSMS($dataWarning);
                    }

                }

                $insertEmailStaff = [
                    'obj_id' => '',
                    'email_type' => "reinvestment",
                    'email_subject' => 'Dự báo tái đầu tư',
                    'email_from' => env('MAIL_USERNAME'),
                    'email_to' => $emailStaff['value'],
                    'email_params' => json_encode($arr),
                    'is_run' => 0
                ];

                $this->jobEmail->addJob($insertEmailStaff);

            }

        }

        foreach ($listCustomer as $item) {
            $dayNow = Carbon::now()->format('Y-m-d');
            $dayExtend = Carbon::parse($dayNow)->addDays(15)->format('Y-m-d');
            $getListWarningContract = $this->mContract->listContractWaringExtendSMS($dayExtend,$item['customer_id']);
            if (count($getListWarningContract) != 0) {
                $messageContractExtend = '';
                $dataSMSExtend = [];
                foreach ($getListWarningContract as $itemContract) {
                    $messageContractExtend = $messageContractExtend.' Số:HD '.$itemContract['customer_contract_code'].' ngày hết hạn '.Carbon::parse($item['customer_contract_end_date'])->format('d-m-Y');
                }
                $dataSMSExtend = [
                    'phone' => $item['phone'],
                    'customer_name' => $item['full_name'],
                    'message' => 'VSETGROUP THÔNG BÁO.HĐ QK SẮP HẾT HẠN.'.$messageContractExtend.'.Hotline: 0907233333',
                ];

                $this->sendSMS($dataSMSExtend);
            }
        }
    }

    public function sendSMS($data) {
        if (env('APP_ENV') == 'production'){
            if (isset($data['phone']) && $data['phone'] != null) {

                $sms = new SmsLogTable();
                $sms->add($data);

                $url = env('LINK_SMS');
                $param = [
                    'u' => env('SMS_U'),
                    'pwd' => env('SMS_PWD'),
                    'phone' => $data['phone'],
//                'phone' => '0988856130',
                    'sms' => $data['message'],
                    'from' => env('SMS_FROM'),
                    'json' => '1',
                ];
                $oURL = curl_init();
                curl_setopt($oURL, CURLOPT_URL, $url);
//            curl_setopt($oURL, CURLOPT_HEADER, TRUE);
                curl_setopt($oURL, CURLOPT_POST, FALSE);
                curl_setopt($oURL, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                curl_setopt($oURL, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($oURL, CURLOPT_POSTFIELDS, json_encode($param));
                $tmp = curl_exec($oURL);
                curl_close($oURL);

                return json_decode($tmp);
            }
        }
    }

    public function happyBirthDay()
    {
        $lang = 'vi';
//            Gửi noti + sms thông báo sinh nhật
        $dayNow = Carbon::now()->format('m-d');
        $listCustomer = $this->mCustomer->getAllByBirthday($dayNow);
        $config = $this->configNoti->getInfo('customer_birthday');
        if (count($listCustomer) != 0) {
            foreach($listCustomer as $value) {

                $noti[] = [
                    'key' => 'customer_birthday',
                    'customer_id' => $value['customer_id'],
                    'object_id' => ''
                ];
                $this->insertEmailApi($value['customer_id'],'customer_birthday','Chúc mừng sinh nhật khách hàng',$value['email'],[]);
                $replaceData = $this->replaceContentNotification($config, $value['customer_id'], '',$lang);
                $this->insertNotificationLog($replaceData['dataNotificationDetail'], $replaceData['dataNotification'],$lang);
            }

//            if (count($noti) != 0) {
//                $this->jobsEmailLogApi->sendNotificationArray($noti);
//            }
        }
    }

    public function eventAction(){
        $lang = 'vi';
        $dayNow = Carbon::now()->format('m-d');
//        $mConfig = app()->get(ConfigNotificationTable::class);
        $listNoti = $this->configNoti->getNotiByDayMonth($dayNow);
        $walletItem = [];
        $serviceArr = [];
        $noti = [];
        if (count($listNoti) != 0) {
            $listCustomer = $this->mCustomer->getAll();
            if (count($listCustomer) != 0) {
                foreach ($listNoti as $item){
                    foreach ($listCustomer as $value) {
                        $config = $this->configNoti->getInfo($item['key']);

//                        Tặng thưởng
                        if ($config['type_bonus'] == 'money') {
                            $walletItem[] = [
                                'customer_id' => $value['customer_id'],
                                'type' => 'bonus',
                                'total_money' => $config['money'],
                                'created_at' => Carbon::now(),
                                'created_by' => $value['customer_id']
                            ];
                        }

                        if ($config['type_bonus'] == 'voucher') {
                            $checkService = $this->service->getDetail($config['service_id']);
                            if ($checkService != null) {
                                if ($checkService['price_standard'] / 1000000 < 10) {
                                    $randomString = $this->generateRandomString(12 - strlen('EVB0'.($checkService['price_standard'] / 1000000).'_'));
                                    $randomString = 'EVB0'.($checkService['price_standard'] / 1000000).'_'.$randomString;
                                } else {
                                    $randomString = $this->generateRandomString(12 - strlen('EVB'.($checkService['price_standard'] / 1000000).'_'));
                                    $randomString = 'EVB'.($checkService['price_standard'] / 1000000).'_'.$randomString;
                                }

                                $serviceArr[] = [
                                    'service_id' => $config['service_id'],
                                    'service_voucher_template' => $checkService['service_voucher_template_id'],
                                    'customer_id' => $value['customer_id'],
                                    'type' => 'gift',
                                    'serial' => $randomString,
                                    'service_serial_status' => 'new',
                                    'is_actived' => 1,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                    'actived_at' => Carbon::now(),
                                    'created_by' => $value['customer_id'],
                                    'updated_by' => $value['customer_id'],
                                ];
                            }
                        }

                        if (in_array($item['key'],['woman_day','vietnamese_women_day'])) {
                            if ($value['gender'] == 'female') {
                                $noti = [
                                    'key' => $item['key'],
                                    'customer_id' => $value['customer_id'],
                                    'object_id' => ''
                                ];
                                if ($item['key'] != 'customer_birthday'){
                                    $this->insertEmailApi($value['customer_id'],$item['key'],$item['name'],$value['email'],[]);

                                    $replaceData = $this->replaceContentNotification($config, $value['customer_id'], '',$lang);
                                    $this->insertNotificationLog($replaceData['dataNotificationDetail'], $replaceData['dataNotification'],$lang);
                                }
                            }
                        } else {
                            $noti = [
                                'key' => $item['key'],
                                'customer_id' => $value['customer_id'],
                                'object_id' => ''
                            ];
                            if ($item['key'] != 'customer_birthday'){
                                $this->insertEmailApi($value['customer_id'],$item['key'],$item['name'],$value['email'],[]);

                                $replaceData = $this->replaceContentNotification($config, $value['customer_id'], '',$lang);
                                $this->insertNotificationLog($replaceData['dataNotificationDetail'], $replaceData['dataNotification'],$lang);
                            }
                        }
                    }
                }
            }
        }

        if (count($walletItem) != 0) {
            $this->wallet->createMultipleWallet($walletItem);
        }

        if (count($serviceArr) != 0) {
            $this->serviceSerial->addServiceSerial($serviceArr);
        }
        return true;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function insertEmailApi($idDetailRequest,$type,$subject,$email_to,$vars) {
        $insertEmail = [
            'obj_id' => $idDetailRequest,
            'email_type' => $type,
            'email_subject' => $subject,
            'email_from' => env('MAIL_USERNAME'),
            'email_to' => $email_to,
            'email_params' => json_encode($vars),
            'is_run' => 0
        ];

        $this->jobsEmailLogApi->addJob($insertEmail);

        return true;
    }

    public function warningExtend()
    {
        $dayNow = Carbon::now()->format('Y-m-d');
        $config = $this->config->getInfoByKey('warning_extend');
        $dayExtend = Carbon::parse($dayNow)->addDays(15)->format('Y-m-d');
        if ($config != null ) {
            if ($config['value'] != null) {
                $dayExtend = Carbon::parse($dayNow)->addDays($config['value'])->format('Y-m-d');
            }
        }

        $getListWarningContract = $this->mContract->listContractWaringExtend($dayExtend);
        $arrExtend = [];
        foreach ($getListWarningContract as $item) {
            $arrExtend = [
                'key' => 'warning_extend',
                'customer_id' => $item['customer_id'],
                'object_id' => $item['customer_contract_id']
            ];
            $this->jobsEmailLogApi->sendNotification($arrExtend);
        }
    }

//    Gửi email gợi ý tái đầu tư khi lãi >= 5 triệu
    public function requestReinestmentContract()
    {
        $data = [];
//            Lấy tháng hiện tại

        $moneyRequest = 5000000;
        $startDayOfMonth = Carbon::now()->startOfMonth()->day;
        $day = Carbon::now()->day;
        $month = Carbon::now()->format('Y-m');
        $startMonth = Carbon::now()->startOfMonth()->format('d/m/Y');
        $endMonth = Carbon::now()->endOfMonth()->format('d/m/Y');
        $filter['day'] = [$day];
        $listContractNow = $this->mContract->getAll($filter);
        foreach ($listContractNow as $item) {
//            Lấy tổng lãi hàng tháng theo từng hợp đồng
            $getTotalInterest = $this->mCustomerContractInterestByMonth->getTotalInterestByMonth($item['customer_contract_id']);
//            Lấy tổng tiền sử dụng lãi theo từng hợp đồng
            $params = [
                'withdraw_request_status' => 'done',
                'customer_contract_id' => $item['customer_contract_id']
            ];
            $withdrawInterest = $this->mWithdrawGroup->sumContractStatus($params);
            if ($getTotalInterest['total'] - $withdrawInterest['withdraw_request_amount'] >= $moneyRequest) {
                $arrExtend = [
                    'key' => 'request_extend_contract',
                    'customer_id' => $item['customer_id'],
                    'object_id' => $item['customer_contract_id']
                ];
                $this->jobsEmailLogApi->sendNotification($arrExtend);
            }
        }
    }

//    Thông báo lãi trong ngày cho từng user chạy vào cuối ngày
    public function notiInterestInDay()
    {
        $listCustomer = $this->mCustomer->getAll();
        $dayStart = Carbon::now()->format('Y-m-d 00:00:00');
        $dayEnd = Carbon::now()->format('Y-m-d 23:59:59');

        foreach ($listCustomer as $item){
            $totalStart = 0 ;
            $totalEnd = 0;

//            Lấy lãi đầu ngày
            $listInterestStart = $this->mCustomerContractInterestByMonth->getInterestByDayCount($item['customer_id'],$dayStart,'before');
//            Lấy lãi cuối ngày
            $listInterestEnd = $this->mCustomerContractInterestByMonth->getInterestByDayCount($item['customer_id'],$dayEnd,'before');

//            Tổng lãi đã sử dụng đầu ngày
            $withdRawStart = $this->mWithdrawGroup->getAllByDaySum($item['customer_id'],$dayStart,'interest','before');

//            Tổng lãi đã sử dụng cuối ngày
            $withdRawEnd = $this->mWithdrawGroup->getAllByDaySum($item['customer_id'],$dayEnd,'interest','before');
            $withdRawStart['total'] = $withdRawStart['total'] == null ? 0 : $withdRawStart['total'];
            $withdRawEnd['total'] = $withdRawEnd['total'] == null ? 0 : $withdRawEnd['total'];
            if (isset($listInterestStart['total']) ){
                $totalStart = $listInterestStart['total'] - $withdRawStart['total'];
            }

            if (isset($listInterestEnd['total']) ){
                $totalEnd = $listInterestEnd['total'] - $withdRawEnd['total'];
            }

            $arrExtend = [
                'key' => 'report_interest_day',
                'customer_id' => $item['customer_id'],
                'object_id' => ''
            ];
            $this->jobsEmailLogApi->sendNotification($arrExtend);

            $dataEmail = [
                'now' => Carbon::now()->format('d/m/Y'),
                'totalStart' => $totalStart,
                'totalEnd' => $totalEnd,
                'interestUsing' => $withdRawEnd['total'] - $withdRawStart['total']
            ];
            $this->insertEmailApi($item['customer_id'],'report_interest_day','Thông báo chốt lãi mỗi ngày',$item['email'],$dataEmail);

        }
    }

    /**
     * Chỉnh nội dung thông báo phù hợp với config theo object_id
     *
     * @param $config
     * @param $userId
     * @param $objectId
     * @return array
     * @throws NotificationRepoException
     */
    private function replaceContentNotification($config, $userId, $objectId,$lang)
    {
            //Data
        $info = $this->mCustomer->getItem($userId);
        $dataNotificationDetail = [];
        $dataNotification = [];
        switch ($config['key']) {
            case 'customer_birthday':

                $genderVI = '';
                $genderEN = '';

                if ($info['gender'] == 'male') {
                    $genderVI = 'Anh';
                    $genderEN = 'Male';
                } else if ($info['gender'] == 'female') {
                    $genderVI = 'Chị';
                    $genderEN = 'Female';
                } else if ($info['gender'] == 'other') {
                    $genderVI = 'Anh/ Chị';
                    $genderEN = 'Male/ Female';
                }

//                    Vi
                $messageVI = str_replace(
                    [
                        '[gender]',
                        '[name]'
                    ],
                    [
                        $genderVI,
                        $info['full_name'],
                    ], $config['message_vi']);

                $contentVI = str_replace(
                    [
                        '[gender]',
                        '[name]',
                    ],
                    [
                        $genderVI,
                        $info['full_name']
                    ], $config['detail_content_vi']);
//                    EN
                $messageEN = str_replace(
                    [
                        '[gender]',
                        '[name]'
                    ],
                    [
                        $genderEN,
                        $info['full_name'],
                    ], $config['message_en']);

                $contentEN = str_replace(
                    [
                        '[gender]',
                        '[name]',
                    ],
                    [
                        $genderEN,
                        $info['full_name']
                    ], $config['detail_content_en']);

                //Data insert
                $dataNotificationDetail = [
                    'background' => $config['detail_background'],
                    'content_vi' => $contentVI,
                    'content_en' => $contentEN
                ];
                $dataNotification = [
                    'user_id' => $userId,
                    'notification_avatar' => $config['avatar'],
                    'notification_title_vi' => $config['title_vi'],
                    'notification_title_en' => $config['title_en'],
                    'notification_message_vi' => $messageVI,
                    'notification_message_en' => $messageEN
                ];

                $dateCheck = Carbon::now()->format('d/m/Y H:i');

//                    if ($config['send_type'] == "in_time") {
//                        $dateCheck = Carbon::parse($config['value'])->format("d/m/Y H:i");
//                    } else if ($config['send_type'] == "before" || $config['send_type'] == "after") {
////                        $dateCheck = Carbon::parse($info['date'] . $info['time'])->format('d/m/Y H:i');
//                    }

                return [
                    'dataNotificationDetail' => $dataNotificationDetail,
                    'dataNotification' => $dataNotification,
                    'dateCheck' => $dateCheck
                ];
                break;
            //Chúc mừng năm mới
            case 'happy_new_year':
            //Giải phóng miền Nam, thống nhất đất nước
            case 'liberation_south':
                //Chúc mừng giáng sinh
            case 'merry_christmas':
                //Quốc khánh 2/9
            case 'national_day':
                //Ngày phụ nữ VN
            case 'vietnamese_women_day':
                //Ngày quốc tế phụ nữ
            case 'woman_day':
                //Chúc mừng ngày QT lao động
            case 'labor_day':
                // Thông báo bảo trì hệ thống
            case 'noti_maintenance':
                $messageVI = $config['message_vi'];
                $messageEN = $config['message_en'];
                $contentVI = $config['detail_content_vi'];
                $contentEN = $config['detail_content_en'];

                //Data insert
                $dataNotificationDetail = [
                    'background' => $config['detail_background'],
                    'content_vi' => $contentVI,
                    'content_en' => $contentEN
                ];
                $dataNotification = [
                    'user_id' => $userId,
                    'notification_avatar' => $config['avatar'],
                    'notification_title_vi' => $config['title_vi'],
                    'notification_title_en' => $config['title_en'],
                    'notification_message_vi' => $messageVI,
                    'notification_message_en' => $messageEN
                ];

                $dateCheck = Carbon::now()->format('d/m/Y H:i');

                return [
                    'dataNotificationDetail' => $dataNotificationDetail,
                    'dataNotification' => $dataNotification,
                    'dateCheck' => $dateCheck
                ];
                break;

            default:
                return [
                    'dataNotificationDetail' => $dataNotificationDetail,
                    'dataNotification' => $dataNotification
                ];
                break;
        }

    }


    private function insertNotificationLog($dataNotificationDetail, $dataNotification,$lang)
    {

        if (env('APP_ENV') == 'production'){
            $idNotificationDetail = $this->notiDetail->createNotiDetailGetId($dataNotificationDetail);
            $oClient = new Client();

            $oClient->request('POST', URL_NOTI . '/notification/push', [
                'json' => [
                    'user_id' => isset($dataNotification['user_id']) ? $dataNotification['user_id'] : '',
                    'title_vi' => $dataNotification['notification_title_vi'],
                    'title_en' => $dataNotification['notification_title_en'],
                    'message_vi' => $dataNotification['notification_message_vi'],
                    'message_en' => $dataNotification['notification_message_en'],
                    'detail_id' => $idNotificationDetail,
                    'avatar' => $dataNotification['notification_avatar'],
                    'lang' => $lang,
                ]
            ]);
        }
    }

//    Gửi noti thưởng đến khách hàng
    public function sendNoti()
    {
//        Lấy danh sách thông báo đến thời gian gửi
        $sendNoti = $this->notiTemplate->getListNoti();
        $oClient = new Client();
        if (count($sendNoti) != 0) {
            foreach ($sendNoti as $item) {
//                Lấy thưởng
                $rewartConfig = $this->rewart->getListByTemplateFirst($item['notification_template_id']);
                $listUser = [];
                if ($item['from_type'] == 'all') {
                    $listUser = $this->user->getListAll();
                } else {
                    $groupCustomer = $this->notiCustomerGroup->getListByTemplateId($item['notification_template_id']);
                    if (count($groupCustomer) != 0) {
                        $groupCustomer = collect($groupCustomer)->pluck('member_level_id');
                        $listUser = $this->user->getListByMember($groupCustomer);
                    }
                }

                $bonusMoney = [];
                $bonusVoucher = [];
                foreach ($listUser as $itemUser) {
                    if ($rewartConfig['key_reward'] == 'money'){
                        $bonusMoney[] = [
                            'customer_id' => $itemUser['customer_id'],
                            'type' => 'bonus',
                            'total_money' => $rewartConfig['value'],
                            'created_at' => Carbon::now(),
                            'created_by' => $itemUser['customer_id']
                        ];
                    }

                    if ($rewartConfig['key_reward'] == 'voucher'){
                        $rewardMap = $this->rewartMap->getListByRewardId($rewartConfig['notification_reward_id']);
                        if (count($rewardMap) != 0) {
                            $rewardMap = collect($rewardMap)->pluck('service_id');
                            $listService = $this->serviceVoucherTemplate->getListService($rewardMap);
                            foreach ($listService as $itemService) {

                                if ($itemService['price_standard'] / 1000000 < 10) {
                                    $randomString = $this->generateRandomString(12 - strlen('EVB0'.($itemService['price_standard'] / 1000000).'_'));
                                    $randomString = 'EVB0'.($itemService['price_standard'] / 1000000).'_'.$randomString;
                                } else {
                                    $randomString = $this->generateRandomString(12 - strlen('EVB'.($itemService['price_standard'] / 1000000).'_'));
                                    $randomString = 'EVB'.($itemService['price_standard'] / 1000000).'_'.$randomString;
                                }

                                $bonusVoucher[] = [
                                    'service_id' => $itemService['service_id'],
                                    'service_voucher_template' => $itemService['service_voucher_template_id'],
                                    'customer_id' => $itemUser['customer_id'],
                                    'type' => 'gift',
                                    'serial' => $randomString,
                                    'service_serial_status' => 'new',
                                    'is_actived' => 1,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                    'actived_at' => Carbon::now(),
                                    'created_by' => Auth::id(),
                                    'updated_by' => Auth::id(),
                                ];
                            }
                        }
                    }

                    if (env('APP_ENV') == 'production'){
                        $oClient->request('POST', URL_NOTI . '/notification/push', [
                            'json' => [
                                'user_id' => $itemUser['customer_id'],
                                'title_vi' => $item['title_vi'],
                                'title_en' => $item['title_en'],
                                'message_vi' => $item['description_vi'],
                                'message_en' => $item['description_en'],
                                'detail_id' => $item['notification_detail_id'],
                                'avatar' => asset('static/backend/images/logoVS.jpg'),
//                            'avatar' => 'https://cms-traiphieu.vsetgroup.vn/static/backend/images/logoVS.jpg',
                                'lang' => 'vi',
                            ]
                        ]);
                    }

                }

                if (count($bonusMoney) != 0) {
                    $this->wallet->createMultipleWallet($bonusMoney);
                }

                if (count($bonusVoucher) != 0) {
                    $this->serviceSerial->addServiceSerial($bonusVoucher);
                }

                $this->notiTemplate->updateNotiTemplate($item['notification_template_id'],['send_status' => 'sent']);
            }
        }
    }

}