<?php

Route::group(['middleware' => ['web', 'auth', 'account'], 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
    Route::get('/', 'AdminController@indexAction')->name('admin');

    Route::get('nothing', function () {
        return view('admin::errors.blank');
    })->name('admin.nothing');

    //STAFF TITLE
    Route::group(['prefix' => 'staff-title'], function () {
        Route::get('/', 'StaffTitleController@indexAction')->name('admin.staff-title');
        Route::post('list', 'StaffTitleController@listAction')->name('admin.staff-title.list');

        Route::post('remove/{id}', 'StaffTitleController@removeAction')->name('admin.staff-title.remove');
        Route::get('add', 'StaffTitleController@addAction')->name('admin.staff-title.add');
        Route::post('add', 'StaffTitleController@submitAddAction')->name('admin.staff-title.submitadd');
//        Route::get('edit/{id}', 'StaffTitleController@editAction')->name('admin.staff-title.edit');
        Route::post('submit-edit', 'StaffTitleController@submitEditAction')->name('admin.staff-title.submitedit');
        Route::post('change-status', 'StaffTitleController@changeStatusAction')->name('admin.staff-title.change-status');
        Route::post('get-edit', 'StaffTitleController@editAction')->name('admin.staff-title.edit');
    });


    Route::group(['prefix' => 'member-level'], function () {

        Route::get('/', 'MemberLevelController@indexAction')->name('admin.member-level');
        Route::post('list', 'MemberLevelController@listAction')->name('admin.member-level.list');
        Route::post('remove', 'MemberLevelController@removeAction')->name('admin.member-level.remove');
        Route::get('add', 'MemberLevelController@addAction')->name('admin.member-level.add');
        Route::post('add', 'MemberLevelController@submitaddAction')->name('admin.member-level.submitadd');
        Route::get('edit/{id}','MemberLevelController@editAction')->name('admin.member-level.edit');
        Route::get('show/{id}','MemberLevelController@showAction')->name('admin.member-level.show');
//        Route::post('edit-member', 'MemberLevelController@editAction')->name('admin.member-level.edit');
        Route::post('edit-submit', 'MemberLevelController@submitEditAction')->name('admin.member-level.submitedit');
        Route::post('upload-action', 'MemberLevelController@uploadAction')->name('admin.member-level.uploadAction');
//        Route::post('change-status', 'MemberLevelController@changeStatusAction')->name('admin.member-level.change-status');
    });

    //DEPARTMENT
    Route::group(['prefix' => 'department'], function () {
        Route::get('/', 'DepartmentController@indexAction')->name('admin.department');
        Route::post('list', 'DepartmentController@listAction')->name('admin.department.list');
        Route::post('change-status', 'DepartmentController@changeStatusAction')->name('admin.department.change-status');
        Route::post('add', 'DepartmentController@add')->name('admin.department.add');
        Route::post('remove/{id}', 'DepartmentController@removeAction')->name('admin.department.remove');
        Route::post('edit', 'DepartmentController@editAction')->name('admin.department.edit');
        Route::post('edit-submit', 'DepartmentController@submitEditAction')->name('admin.department.submit-edit');
    });
    //STAFF
    Route::group(['prefix' => 'staff'], function () {
        Route::get('/', 'StaffsController@indexAction')->name('admin.staff');
        Route::post('list', 'StaffsController@listAction')->name('admin.staff.list');
        Route::get('add', 'StaffsController@addAction')->name('admin.staff.add');
        Route::post('add', 'StaffsController@submitAddAction')->name('admin.staff.submitAdd');
        Route::post('remove/{id}', 'StaffsController@removeAction')->name('admin.staff.remove');
        Route::post('change-status', 'StaffsController@changeStatusAction')->name('admin.staff.change-status');
        Route::get('edit/{id}', 'StaffsController@editAction')->name('admin.staff.edit');
        Route::post('edit', 'StaffsController@submitEditAction')->name('admin.staff.submit-edit');
        Route::post('uploads', 'StaffsController@uploadAction')->name('admin.staff.uploads');
        Route::post('deleteImage', 'StaffsController@deleteTempFileAction')->name('admin.staff.delete');
        Route::post('edit-image', 'StaffsController@editImageAction')->name('admin.staff.editimage');
        Route::get('profile', 'StaffsController@profileAction')->name('admin.staff.profile');
    });
    //PRODUCT
//    Vset
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@indexAction')->name('admin.product');
        Route::post('list', 'ProductController@listAction')->name('admin.product.list');
        Route::get('add', 'ProductController@addAction')->name('admin.product.add');
        Route::post('submit-add', 'ProductController@submitAddAction')->name('admin.product.submit-add');
        Route::post('upload', 'ProductController@uploadAction')->name('admin.product.upload');
        Route::get('edit/{id}', 'ProductController@editAction')->name('admin.product.edit');
        Route::post('edit-submit', 'ProductController@submitEditAction')->name('admin.product.submit-edit');
        Route::post('remove/{id}', 'ProductController@removeAction')->name('admin.product.remove');
        Route::post('change-status', 'ProductController@changeStatusAction')->name('admin.product.change-status');
        Route::get('detail/{id}', 'ProductController@detailAction')->name('admin.product.detail');
        Route::post('get-list-product-bonus', 'ProductController@getListProductBonus')->name('admin.product.get-list-product-bonus');
        Route::post('add-product-bonus', 'ProductController@addProductBonus')->name('admin.product.add-product-bonus');
        Route::post('add-product-interest', 'ProductController@addProductInterest')->name('admin.product.add-product-interest');
        Route::post('edit-product-bonus', 'ProductController@editProductBonus')->name('admin.product.edit-product-bonus');
        Route::post('edit-product-interest', 'ProductController@editProductInterest')->name('admin.product.edit-product-interest');
        Route::post('edit-product-bonus-post', 'ProductController@editProductBonusPost')->name('admin.product.edit-product-bonus-post');
        Route::post('edit-product-interest-post', 'ProductController@editProductInterestPost')->name('admin.product.edit-product-interest-post');
        Route::post('detail-product-bonus', 'ProductController@detailProductBonus')->name('admin.product.detail-product-bonus');
        Route::post('detail-product-interest', 'ProductController@detailProductInterest')->name('admin.product.detail-product-interest');
        Route::post('remove-product-bonus', 'ProductController@removeProductBonus')->name('admin.product.remove-product-bonus');
        Route::post('get-list-interest-rate', 'ProductController@getListInterestRate')->name('admin.product.get-list-interest-rate');
        Route::post('remove-product-interest', 'ProductController@removeProductInterest')->name('admin.product.remove-product-interest');
        Route::post('submit-list-investment-withdraw', 'ProductController@submitListInvestmentWithdraw')->name('admin.product.submit-list-investment-withdraw');
        Route::post('submit-list-product-bonus', 'ProductController@submitListProductBonus')->name('admin.product.submit-list-product-bonus');
        Route::post('get-list-service', 'ProductController@getListService')->name('admin.product.getListService');
        Route::post('/add-list-service', 'ProductController@addListService')->name('admin.product.addListService');
        Route::post('/save-config', 'ProductController@saveConfig')->name('admin.product.saveConfig');
        Route::post('/delete-service', 'ProductController@deleteService')->name('admin.product.deleteService');
    });
    /*
     * Author : Huy
     *
     * ----------------------------------------*/

    //Service bỏ
    Route::group(['prefix' => 'service'], function () {
        Route::get('/', 'ServiceController@indexAction')->name('admin.service');
        Route::post('list', 'ServiceController@listAction')->name('admin.service.list');
        Route::get('add', 'ServiceController@addAction')->name('admin.service.add');
        Route::post('submit-add', 'ServiceController@submitAddAction')->name('admin.service.submitAdd');
        Route::post('add-unit', 'ServiceController@getOptionUnitAction')->name('admin.service.getUnit');
        Route::post('option-branch', 'ServiceController@getOptionBranchAction')->name('admin.service.getBranch');
        Route::post('search-option-product', 'ServiceController@getOptionProductAction')->name('admin.search.product');
        Route::get('detail/{id}', 'ServiceController@detailAction')->name('admin.service.detail');
        Route::post('detail-branch/{id}', 'ServiceController@listBranchDetail')->name('admin.service.list-branch-detail');
        Route::post('detail-material/{id}', 'ServiceController@listMaterialDetail')->name('admin.service.list-material-detail');
        Route::post('submit-edit', 'ServiceController@submitEditAction')->name('admin.service.submitEdit');
        Route::get('edit/{id}', 'ServiceController@editAction')->name('admin.service.edit');
        Route::post('remove/{id}', 'ServiceController@removeAction')->name('admin.service.remove');
        Route::post('uploads', 'ServiceController@uploadAction')->name('admin.service.uploads');
        Route::post('upload-dropzone', 'ServiceController@uploadDropzoneAction')->name('admin.service.upload-dropzone');
        Route::post('delete-image', 'ServiceController@deleteImageAction')->name('admin.service.delete-image');
        Route::post('change-status', 'ServiceController@changeStatusAction')->name('admin.service.change-status');

    });
    //Service Category
    Route::group(['prefix' => 'service-category'], function () {
        Route::get('/', 'ServiceCategoryController@indexAction')->name('admin.service_category');
        Route::post('list', 'ServiceCategoryController@listAction')->name('admin.service_category.list');
        Route::post('add', 'ServiceCategoryController@addAction')->name('admin.service_category.submitAdd');
        Route::post('change-status', 'ServiceCategoryController@changeStatusAction')->name('admin.service_category.change-status');
        Route::post('remove/{id}', 'ServiceCategoryController@removeAction')->name('admin.service_category.remove');
        Route::post('edit', 'ServiceCategoryController@editAction')->name('admin.service_category.edit');
        Route::post('edit-submit', 'ServiceCategoryController@submitEditAction')->name('admin.service_category.submitEdit');
        Route::post('add-form', 'ServiceCategoryController@addForm')->name('admin.service_category.add-form');
    });
    //SMS MARKETING
    Route::group(['prefix' => 'sms'], function () {
        Route::get('/', 'SmsCampaignController@indexAction')->name('admin.sms.sms-campaign');
        Route::get('sms-campaign/add', 'SmsCampaignController@addAction')->name('admin.sms.sms-campaign-add');
        Route::get('sms-campaign/send-sms', 'SmsCampaignController@indexSendSms')->name('admin.sms.send-sms');
        Route::post('list', 'SmsCampaignController@listAction')->name('admin.sms.list');
        Route::post('remove/{id}', 'SmsCampaignController@removeAction')->name('admin.sms.remove');
        Route::post('get-info-sms-campaign', 'SmsCampaignController@getInfoSmsCampaign')->name('admin.sms.get-info-sms-campaign');
        Route::post('search-customer', 'SmsCampaignController@searchCustomerAction')->name('admin.sms.search-customer');

        //SMS
        Route::get('config-sms', 'SmsController@settingSmsAction')->name('admin.sms.config-sms');
        Route::post('config', 'SmsController@configSmsAction')->name('admin.sms.config');
        Route::post('get-config', 'SmsController@getConfig')->name('admin.sms.get-config');
        Route::post('active-sms-config', 'SmsController@activeSmsConfigAction')->name('admin.sms.active-sms-config');
        Route::post('setting-sms', 'SmsController@submitSettingSms')->name('admin.sms.setting-sms');

        //Campaing
        Route::post('paging', 'SmsCampaignController@pagingAction')->name('admin.campaign.paging');
        Route::post('filter', 'SmsCampaignController@filterAction')->name('admin.campaign.filter');
        Route::post("paging-filter", "SmsCampaignController@pagingFilterAction")->name("admin.campaign.paging-filter");
        Route::post("submit-add", "SmsCampaignController@submitAddAction")->name("admin.campaign.submit-add");
        Route::get("sms-campaign-edit/{id}", "SmsCampaignController@editAction")->name("admin.campaign.edit");
        Route::get("sms-campaign-detail/{id}", "SmsCampaignController@detailAction")->name("admin.campaign.detail");
        Route::post("sms-campaign-submit-edit", "SmsCampaignController@submitEditAction")->name("admin.campaign.submit-edit");
        Route::post("sms-campaign-save-log", "SmsCampaignController@submitSaveLogAction")->name("admin.campaign.sms-campaign-save-log");
        Route::post("remove-log", "SmsCampaignController@removeLogAction")->name("admin.campaign.remove-log");
        Route::get('export-file/{type}', 'SmsCampaignController@exportFileAction')->name('admin.campaign.export.file');
        Route::post('import-file-excel', 'SmsCampaignController@importFileExcelAction')->name('admin.campaign.import-file-excel');
        Route::post("detail/list", "SmsCampaignController@listDetailAction")->name("admin.campaign.detail-list");
        Route::post("detail-paging", "SmsCampaignController@pagingDetailAction")->name("admin.campaign.detail-paging");

        //test
//        Route::get('send-sms-log', 'SmsController@sendSmsAction')->name('admin.sms.send-sms-test');
        Route::post('send-code-service-card', 'SmsController@sendCodeServiceCard')->name('admin.sms.send-code-service-card');
        Route::post('send-all-code-service-card', 'SmsController@sendAllCodeServiceCard')->name('admin.sms.send-all-code-service-card');
//        Route::get('sms-fpt','SmsController@sendSmsFptAction')->name('admin.sms.fpt');
    });
    Route::group(['prefix' => 'email'], function () {
        Route::get('/', 'EmailCampaignController@indexAction')->name('admin.email');
        Route::post('list', 'EmailCampaignController@listAction')->name('admin.email.list');
        Route::post('paging', 'EmailCampaignController@pagingAction')->name('admin.email.paging');
        Route::get('add', 'EmailCampaignController@addAction')->name('admin.email.add');
        Route::post('submit-add', 'EmailCampaignController@submitAddAction')->name('admin.email.submitAdd');
        Route::get('edit/{id}', 'EmailCampaignController@editAction')->name('admin.email.edit');
        Route::post('submit-edit', 'EmailCampaignController@submitEditAction')->name('admin.email.submit-edit');
        Route::post('remove/{id}', 'EmailCampaignController@removeAction')->name('admin.email.remove');
        Route::post('search-customer', 'EmailCampaignController@searchCustomerAction')->name('admin.email.search-customer');
        Route::post('append', 'EmailCampaignController@appendAction')->name('admin.email.append-table');
        Route::post('save-log', 'EmailCampaignController@saveLogAction')->name('admin.email.save-log');
        Route::get('detail/{id}', 'EmailCampaignController@detailAction')->name('admin.email.detail');
        Route::post('detail/{id}', 'EmailCampaignController@listDetailAction')->name('admin.email.list-detail');
        Route::post('send-mail', 'EmailCampaignController@sendMailAction')->name('admin.email.send-mail');
        Route::post('filter', 'EmailCampaignController@filterAction')->name('admin.email.filter');
        Route::post('filter-paging', 'EmailCampaignController@pagingFilterAction')->name('admin.email.paging-filter');
        Route::get('export-excel', 'EmailCampaignController@exportExcelAction')->name('admin.email.export-excel');
        Route::post('import-excel', 'EmailCampaignController@importExcelAction')->name('admin.email.import-excel');
        Route::post('cancel/{id}', 'EmailCampaignController@cancelAction')->name('admin.email.cancel');
    });
    Route::group(['prefix' => 'email-auto'], function () {
        Route::get('/', 'EmailAutoController@indexAction')->name('admin.email-auto');
        Route::post('list', 'EmailAutoController@listAction')->name('admin.email-auto.list');
        Route::post('get-config', 'EmailAutoController@getConfigAction')->name('admin.email-auto.config');
        Route::post('submit-config', 'EmailAutoController@submitConfigAction')->name('admin.email-auto.submit-config');
        Route::post('change-status', 'EmailAutoController@changeStatusAction')->name('admin.email-auto.change-status');
        Route::post('get-setting', 'EmailAutoController@getSettingContentAction')->name('admin.email-auto.setting-content');
        Route::post('submit-setting', 'EmailAutoController@submitSettingContentAction')->name('admin.email-auto.submit-setting-content');
        Route::get('run', 'EmailAutoController@runAutoAction')->name('admin.email-auto.run');
        Route::post('email-template', 'EmailAutoController@emailTemplateAction')->name('admin.email-auto.email-template');
        Route::post('submit-template', 'EmailAutoController@submitEmailTemplateAction')->name('admin.email-auto.submit-template');
//        Route::get('send-email-job', 'EmailAutoController@sendEmailJobAction')->name('admin.email-auto.send-email-job');
    });
    Route::group(['prefix' => 'config'], function () {
        Route::get('/page-appointment', 'ConfigController@indexAction')->name('admin.config.page-appointment');
        Route::post('/introduction', 'ConfigController@updateIntroduction')->name('admin.config.update.introduction');
        Route::post('list-info', 'ConfigController@listInfoAction')->name('admin.config-page-appointment.list-info');
        Route::get('add-info', 'ConfigController@addInfoAction')->name('admin.config-page-appointment.add-info');
        Route::post('upload', 'ConfigController@uploadAction')->name('admin.config-page-appointment.upload');
        Route::post('submit-add-info', 'ConfigController@submitAddInfoAction')->name('admin.config-page-appointment.submit-add-info');
        Route::get('edit-info/{id}', 'ConfigController@editInfoAction')->name('admin.config-page-appointment.edit-info');
        Route::post('submit-edit-info', 'ConfigController@submitEditInfoAction')->name('admin.config-page-appointment.submit-edit-info');
        Route::post('change-status', 'ConfigController@changeStatusAction')->name('admin.config-page-appointment.change-status');
        Route::post('remove/{id}', 'ConfigController@removeInfoAction')->name('admin.config-page-appointment.remove');
        Route::post('list-banner', 'ConfigController@listBannerAction')->name('admin.config-page-appointment.list-banner');
        Route::post('submit-add-banner', 'ConfigController@submitAddBannerAction')->name('admin.config-page-appointment.submit-add-banner');
        Route::post('edit-banner', 'ConfigController@editBannerAction')->name('admin.config-page-appointment.edit-banner');
        Route::post('submit-edit-banner', 'ConfigController@submitEditBannerAction')->name('admin.config-page-appointment.submit-edit-banner');
        Route::post('remove-banner/{id}', 'ConfigController@removeBannerAction')->name('admin.config-page-appointment.remove-banner');
        Route::post('list-time-working', 'ConfigController@listTimeWorkingAction')->name('admin.config-page-appointment.list-time');
        Route::post('change-status-time', 'ConfigController@changeStatusTimeAction')->name('admin.config-page-appointment.change-status-time');
        Route::post('submit-edit-time', 'ConfigController@submitEditTimeAction')->name('admin.config-page-appointment.submit-edit-time');
        Route::post('list-menu', 'ConfigController@listRuleMenuAction')->name('admin.config-page-appointment.list-rule-menu');
        Route::post('change-status-menu', 'ConfigController@changeStatusMenuAction')->name('admin.config-page-appointment.change-status-menu');
        Route::post('submit-edit-rule-menu', 'ConfigController@submitEditRuleMenuAction')->name('admin.config-page-appointment.submit-edit-rule-menu');
        Route::post('list-booking', 'ConfigController@listRuleBookingAction')->name('admin.config-page-appointment.list-rule-booking');
        Route::post('change-status-booking', 'ConfigController@changeStatusBookingAction')->name('admin.config-page-appointment.change-status-booking');
        Route::post('list-setting-other', 'ConfigController@listRuleSettingOtherAction')->name('admin.config-page-appointment.list-setting-other');
        Route::post('change-status-setting-other', 'ConfigController@changeStatusSettingOtherAction')->name('admin.config-page-appointment.change-status-setting-other');
        Route::post('submit-edit-day', 'ConfigController@submitEditDayAction')->name('admin.config-page-appointment.submit-edit-day');
        Route::post('list-booking-extra', 'ConfigController@listBookingExtraAction')->name('admin.config-page-appointment.list-booking-extra');
        Route::post('submit-edit-booking-extra', 'ConfigController@submitEditBookingExtraAction')->name('admin.config-page-appointment.submit-edit-booking-extra');
        Route::post('upload-img-fb', 'ConfigController@uploadImgFaceBookAction')->name('admin.config-page-appointment.upload-img-fb');
        Route::post('remove-img-fb', 'ConfigController@removeImageFacbookAction')->name('admin.config-page-appointment.remove-img-fb');
    });
    Route::group(['prefix' => 'layout'], function () {
        Route::get('search-dashboard', 'LayoutController@searchDashboard')->name('admin.layout.search-dashboard');
        Route::get('search', 'LayoutController@searchIndexAction')->name('admin.layout.search-result');
        Route::post('paging-customer', 'LayoutController@pagingCustomerAction')->name('admin.layout.search.paging-customer');
        Route::post('paging-customer-appointment', 'LayoutController@pagingCustomerAppointmentAction')->name('admin.layout.search.paging-customer-appointment');
        Route::post('paging-order', 'LayoutController@pagingOrderAction')->name('admin.layout.search.paging-order');
        Route::get('detail-search', 'LayoutController@detailSearchAction')->name('admin.layout.search.detail-search');
    });
    Route::group(['prefix' => 'config-email-template'], function () {
        Route::get('/', 'ConfigEmailTemplateController@indexAction')->name('admin.config-email-template');
        Route::post('list', 'ConfigEmailTemplateController@listAction')->name('admin.config-email-template.list');
        Route::post('upload', 'ConfigEmailTemplateController@uploadAction')->name('admin.config-email-template.upload');
        Route::post('remove-image', 'ConfigEmailTemplateController@removeImage')->name('admin.config-email-template.remove-img');
        Route::post('submit-edit', 'ConfigEmailTemplateController@submitEditAction')->name('admin.config-email-template.submit-edit');
        Route::post('change-status-logo', 'ConfigEmailTemplateController@changeStatusLogoAction')->name('admin.config-email-template.change-status-logo');
        Route::post('change-status-website', 'ConfigEmailTemplateController@changeStatusWebsiteAction')->name('admin.config-email-template.change-status-website');
        Route::post('view-after', 'ConfigEmailTemplateController@viewAction')->name('admin.config-email-template.view-after');
    });
    //AUTHORIZATION
    Route::group(['prefix' => 'authorization'], function () {
        Route::get('/', 'AuthorizationController@indexAction')->name('admin.authorization');
        Route::get('edit/{id}', 'AuthorizationController@editAction')->name('admin.authorization.edit');
        Route::post('check-all-role-page', 'AuthorizationController@checkAllRolePage')->name('admin.authorization.check-all-role-page');
        Route::post('check-each-role-page', 'AuthorizationController@checkEachRolePage')->name('admin.authorization.check-each-role-page');
        Route::post('check-all-role-action', 'AuthorizationController@checkAllRoleAction')->name('admin.authorization.check-all-role-action');
        Route::post('check-each-role-action', 'AuthorizationController@checkEachRoleAction')->name('admin.authorization.check-each-role-action');
    });
    //ROLE GROUP
    Route::group(['prefix' => 'role-group'], function () {
        Route::get('/', 'RoleGroupController@indexAction')->name('admin.role-group');
        Route::post('/submit-add', 'RoleGroupController@submitAddAction')->name('admin.role-group.submitadd');
        Route::post('list', 'RoleGroupController@listAction')->name('admin.role-group.list');
        Route::post('change-status', 'RoleGroupController@changeStatusAction')->name('admin.role-group.change-status');
        Route::post('edit', 'RoleGroupController@editAction')->name('admin.role-group.edit');
        Route::post('submit-edit', 'RoleGroupController@submitEditAction')->name('admin.role-group.submit-edit');

    });
    Route::group(['prefix' => 'customer-group-filter'], function () {
        Route::get('/', 'CustomerGroupFilterController@indexAction')
            ->name('admin.customer-group-filter');
        Route::get('/add-group-define', 'CustomerGroupFilterController@addDefineAction')
            ->name('admin.customer-group-filter.add-group-define');
        Route::get('/export-excel-example', 'CustomerGroupFilterController@exportExcelAction')
            ->name('admin.customer-group-filter.export-excel-example');
        Route::post('/read-excel', 'CustomerGroupFilterController@importExcel')
            ->name('admin.customer-group-filter.read-excel');
        Route::post('/search-where-in-user', 'CustomerGroupFilterController@searchWhereInUser')
            ->name('admin.customer-group-filter.search-where-in-customer');
        Route::post('/search-all-customer', 'CustomerGroupFilterController@searchAllCustomer')
            ->name('admin.customer-group-filter.search-all-customer');
        Route::post('/add-customer-group-define', 'CustomerGroupFilterController@addCustomerGroupDefine')
            ->name('admin.customer-group-filter.add-customer-group-define');
        Route::post('/submit-add-group-define', 'CustomerGroupFilterController@submitAddGroupDefine')
            ->name('admin.customer-group-filter.submit-add-group-define');
        Route::post('list', 'CustomerGroupFilterController@listAction')
            ->name('admin.customer-group-filter.list');
        Route::get('/edit-customer-group-define/{id}', 'CustomerGroupFilterController@editUserDefine')
            ->name('admin.customer-group-filter.edit-user-define');
        Route::post('/get-customer-by-group-define', 'CustomerGroupFilterController@getCustomerByGroupDefine')
            ->name('admin.customer-group-filter.get-customer-by-group-define');
        Route::post('/update-user-define', 'CustomerGroupFilterController@updateCustomerGroupDefine')
            ->name('admin.customer-group-filter.update-user-define');
        Route::get('/detail-customer-group-define/{id}', 'CustomerGroupFilterController@detailCustomerGroupDefine')
            ->name('admin.customer-group-filter.detail-customer-group-define');
        Route::get('/add-customer-group-auto', 'CustomerGroupFilterController@addAutoAction')
            ->name('admin.customer-group-filter.add-customer-group-auto');
        Route::post('/store-customer-group-auto', 'CustomerGroupFilterController@submitAddAutoAction')
            ->name('admin.customer-group-filter.store-customer-group-auto');
        Route::post('/get-condition', 'CustomerGroupFilterController@getCondition')
            ->name('admin.customer-group-filter.get-condition');
        Route::get('/edit-customer-group-auto/{id}', 'CustomerGroupFilterController@editAutoAction')
            ->name('admin.customer-group-filter.edit-customer-group-auto');
        Route::post('/submit-edit-auto', 'CustomerGroupFilterController@submitEditAutoAction')
            ->name('admin.customer-group-filter.submit-edit-auto');
        Route::post('/get-customer-in-group-auto', 'CustomerGroupFilterController@getCustomerInGroupAuto')
            ->name('admin.customer-group-filter.get-customer-in-group-auto');
        Route::post('/get-customer-in-group-define', 'CustomerGroupFilterController@getCustomerInGroup')
            ->name('admin.customer-group-filter.get-customer-in-group-define');
        Route::get('/detail-customer-group-auto/{id}', 'CustomerGroupFilterController@detailAutoAction')
            ->name('admin.customer-group-filter.detailAutoAction');
    });
    //Cấu hình chung (Hot_search , Phân đơn hàng)
    Route::group(['prefix' => 'config'], function () {
        Route::get('/config-general', 'ConfigController@configGeneral')->name('admin.config.config-general');
        Route::get('/detail-config-general/{id}', 'ConfigController@detailConfigGeneral')->name('admin.config.detail-config-general');
        Route::get('/edit-config-general/{id}', 'ConfigController@editConfigGeneral')->name('admin.config.edit-config-general');
        Route::post('/edit-post-config-general', 'ConfigController@editPostConfigGeneral')->name('admin.config.edit-post-config-general');
    });

    Route::get('validation', function () {
        return trans('admin::validation');
    })->name('admin.validation');

//    Vset Contract
    Route::group(['prefix' => 'customer-contract'], function () {
        Route::get('/', 'CustomerContractController@indexAction')->name('admin.customer-contract');
        Route::get('list-interest-approved', 'CustomerContractController@getListInterestApproved')->name('admin.customer-contract.list-interest-approved');
        Route::post('list-interest-approved-change', 'CustomerContractController@getListInterestApprovedChange')->name('admin.customer-contract.list-interest-approved-change');
        Route::get('/list-nearly-expired', 'CustomerContractController@listNearlyExpired')->name('admin.customer-contract.list-nearly-expired');
        Route::post('/list-nearly-expired', 'CustomerContractController@postlistNearlyExpired')->name('admin.customer-contract.post-list-nearly-expired');
        Route::get('export-list-contract-interest', 'CustomerContractController@exportListContractInterest')->name('admin.customer-contract.export-list-contract-interest');
        Route::post('confirm-contract-interest', 'CustomerContractController@confirmContractInterest')->name('admin.customer-contract.confirm-contract-interest');
        Route::post('list', 'CustomerContractController@listAction')->name('admin.customer-contract.list');
        Route::get('export-excel', 'CustomerContractController@exportExcelAction')->name('admin.customer-contract.export');
        Route::get('edit/{id}', 'CustomerContractController@editAction')->name('admin.customer-contract.edit');
        Route::post('update', 'CustomerContractController@updateAction')->name('admin.customer-contract.update');
        Route::post('remove', 'CustomerContractController@removeAction')->name('admin.customer-contract.remove');
        Route::get('detail/{id}/{tab?}', 'CustomerContractController@show')->name('admin.customer-contract.detail');
        Route::post('upload-dropzone', 'CustomerContractController@uploadDropzoneAction')->name('customer-contract.upload-dropzone');
//        Route::post('update', 'CustomerContractController@updateAction')->name('admin.customer-contract.update');
        //List tag detail
        Route::post('list-file', 'CustomerContractController@listFileAction')->name('admin.customer-contract.list-file');
        Route::post('list-interest-month', 'CustomerContractController@listInterestMonthAction')
            ->name('admin.customer-contract.list-interest-month');
        Route::post('list-interest-date', 'CustomerContractController@listInterestDateAction')
            ->name('admin.customer-contract.list-interest-date');
        Route::post('list-interest-time', 'CustomerContractController@listInterestTimeAction')
            ->name('admin.customer-contract.list-interest-time');
        Route::post('list-withdraw-request', 'CustomerContractController@listWithdrawRequestAction')
            ->name('admin.customer-contract.list-withdraw-request');
        Route::post('list-contract-serial', 'CustomerContractController@listContractSerialAction')
            ->name('admin.customer-contract.list-contract-serial');
        Route::post('confirm-contract', 'CustomerContractController@confirmContract')
            ->name('admin.customer-contract.confirm-contract');
            
            Route::post('end-contract', 'CustomerContractController@endContract')->name('admin.customer-contract.endContract');
    });

    Route::group(['prefix' => 'customer-source'], function () {
        Route::get('/', 'CustomerSourceController@indexAction')->name('admin.customer-source');
        Route::post('change-status', 'CustomerSourceController@changeStatusAction')->name('admin.customer-source.change-status');
        Route::post('list', 'CustomerSourceController@listAction')->name('admin.customer-source.list');
        Route::post('remove/{id}', 'CustomerSourceController@removeAction')->name('admin.customer-source.remove');
        Route::post('add', 'CustomerSourceController@submitAddAction')->name('admin.customer-source.add');
        Route::get('edit', 'CustomerSourceController@editAction')->name('admin.customer-source.edit');
        Route::post('edit', 'CustomerSourceController@submitEditAction')->name('admin.customer-source.edit-submit');
    });

    Route::group(['prefix' => 'customer-journey'], function () {
        Route::get('/', 'CustomerJourneyController@indexAction')->name('admin.customer-journey');
        Route::post('change-status', 'CustomerJourneyController@changeStatusAction')->name('admin.customer-journey.change-status');
        Route::post('list', 'CustomerJourneyController@listAction')->name('admin.customer-journey.list');
        Route::post('remove/{id}', 'CustomerJourneyController@removeAction')->name('admin.customer-journey.remove');
        Route::post('add', 'CustomerJourneyController@submitAddAction')->name('admin.customer-journey.add');
        Route::get('edit', 'CustomerJourneyController@editAction')->name('admin.customer-journey.edit');
        Route::post('edit', 'CustomerJourneyController@submitEditAction')->name('admin.customer-journey.edit-submit');
    });

//    Cấu hình tặng thưởng khi share link
    Route::group(['prefix' => 'refer-source'], function () {
        Route::get('/', 'ReferSourceController@indexAction')->name('admin.refer-source');
        Route::get('/edit/{id}', 'ReferSourceController@editAction')->name('admin.refer-source.edit');
        Route::post('/remove', 'ReferSourceController@removeAction')->name('admin.refer-source.remove');
        Route::post('/get-list-service', 'ReferSourceController@getListService')
            ->name('admin.refer-source.getListService');
        Route::post('/add-list-service', 'ReferSourceController@addListService')
            ->name('admin.refer-source.addListService');
        Route::post('/save-config', 'ReferSourceController@saveConfig')
            ->name('admin.refer-source.saveConfig');
        Route::post('/remove-service', 'ReferSourceController@removeService')
            ->name('admin.refer-source.removeService');
    });

//    Vset
    Route::group(['prefix' => 'withdraw-request'], function () {
        Route::get('/', 'WithdrawRequestController@indexAction')->name('admin.withdraw-request');
        Route::get('/stock', 'WithdrawRequestController@indexStockAction')->name('admin.withdraw-request.stock');
        Route::post('list-all', 'WithdrawRequestController@listAllAction')->name('admin.withdraw-request.list-all');
        Route::post('list', 'WithdrawRequestController@listAction')->name('admin.withdraw-request.list');
        Route::post('list-commission', 'WithdrawRequestController@listActionCommission')->name('admin.withdraw-request.list-commission');
        Route::post('list-stock', 'WithdrawRequestController@listActionStock')->name('admin.withdraw-request.list-stock');
        Route::get('show/{id}', 'WithdrawRequestController@show')->name('admin.withdraw-request.show');
        Route::get('show-group/{id}', 'WithdrawRequestController@showGroup')->name('admin.withdraw-request.show-group');
        Route::get('show-stock/{id}', 'WithdrawRequestController@showStock')->name('admin.withdraw-request.show-stock');
        Route::get("create", "WithdrawRequestController@createAction")->name("admin.withdraw-request.create");
        Route::post("create", "WithdrawRequestController@submitCreateAction")->name("admin.withdraw-request.submitCreate");
        Route::post("get-list-withdraw-request", "WithdrawRequestController@getListWithdrawRequest")->name("admin.withdraw-request.get-list-withdraw-request");
        Route::post("change-status", "WithdrawRequestController@changeStatus")->name("admin.withdraw-request.change-status");
        Route::post("change-status-group", "WithdrawRequestController@changeStatusGroup")->name("admin.withdraw-request.change-status-group");
        Route::post("change-status-stock", "WithdrawRequestController@changeStatusStock")->name("admin.withdraw-request.change-status-stock");
        Route::post("get-list-customer-contract-interest-month", "WithdrawRequestController@getListCustomerContractInterestMonth")->name("admin.withdraw-request.get-list-customer-contract-interest-month");
        Route::post("get-list-customer-contract-interest-date", "WithdrawRequestController@getListCustomerContractInterestDate")->name("admin.withdraw-request.get-list-customer-contract-interest-date");
    });

//    Vset Support Management
    Route::group(['prefix' => 'support-management'], function () {
        Route::get('/', 'SupportManagementController@indexAction')->name('admin.support-management');
        Route::post('list', 'SupportManagementController@listAction')->name('admin.support-management.list');
        Route::get('show/{id}', 'SupportManagementController@show')->name('admin.support-management.view');
        Route::get('/group', 'SupportManagementController@indexGroupAction')->name('admin.support-management.group');
        Route::post('/group/list-group', 'SupportManagementController@listGroupAction')->name('admin.support-management.group.list-group');
    });

//    Vset Account Statement
    Route::group(['prefix' => 'account-statement'], function () {
        Route::get('/', 'AccountStatementController@indexAction')->name('admin.account-statement');
        Route::post('list', 'AccountStatementController@listAction')->name('admin.account-statement.list');
        Route::get('detail/{id}', 'AccountStatementController@detail')->name('admin.account-statement.detail');
    });

    // Vset Support
    Route::group(['prefix' => 'support'], function () {
        Route::get('/', 'SupportController@indexAction')->name('admin.support');
        Route::post('list', 'SupportController@listAction')->name('admin.support.list');
        Route::get('show/{id}', 'SupportController@showAction')->name('admin.support.show');
        Route::get('edit/{id}', 'SupportController@editAction')->name('admin.support.edit');
        Route::post('update-status', 'SupportController@updateStatus')->name('admin.support.update-status');
        Route::post('remove', 'SupportController@remove')->name('admin.support.remove');
    });

    Route::get('/update-commission', 'BuyBondsRequestController@updateCommission');

 // Quản lý yêu cầu mua trái phiếu
    Route::group(['prefix' => 'buy-bonds-request'], function () {
        Route::get('/', 'BuyBondsRequestController@indexAction')->name('admin.buy-bonds-request');
        Route::post('list', 'BuyBondsRequestController@listAction')->name('admin.buy-bonds-request.list');
        Route::get('show/{id}', 'BuyBondsRequestController@show')->name('admin.buy-bonds-request.view');
        Route::post("create-receipt", "BuyBondsRequestController@createReceipt")->name("admin.buy-bonds-request.createReceipt");
        Route::post("create-wallet", "BuyBondsRequestController@createWallet")->name("admin.buy-bonds-request.createWallet");
        //tạo phiếu thu
        Route::post('change-status', 'BuyBondsRequestController@changeStatusOderAction')->name('admin.buy-bonds-request.change-status');
        //chuyển trạng thái đơn hàng
        Route::post('upload-dropzone', 'BuyBondsRequestController@uploadDropzoneAction')->name('admin.buy-bonds-request.upload-dropzone');
        Route::post('get-list-receipt-detail', 'BuyBondsRequestController@getListReceiptDetail')->name('admin.buy-bonds-request.get-list-receipt-detail');
        Route::post('add-receipt-detail', 'BuyBondsRequestController@addReceiptDetail')->name('admin.buy-bonds-request.add-receipt-detail');
        Route::post('show-popup-receipt-detail', 'BuyBondsRequestController@showPopupReceiptDetail')->name('admin.buy-bonds-request.show-popup-receipt-detail');
        Route::post('print-bill', 'BuyBondsRequestController@printBill')->name('admin.buy-bonds-request.print-bill');
        Route::post('create-bill', 'BuyBondsRequestController@createBill')->name('admin.buy-bonds-request.create-bill');
        Route::post('cancel-order', 'BuyBondsRequestController@cancelOrder')->name('admin.buy-bonds-request.cancel-order');
        //up dảnh hóa đơn
    });

    // Quản lý yêu cầu mua trái phiếu
    Route::group(['prefix' => 'buy-bonds-request-fail'], function () {
        Route::get('/', 'BuyBondsRequestFailController@indexAction')->name('admin.buy-bonds-request-fail');
        Route::post('list', 'BuyBondsRequestFailController@listAction')->name('admin.buy-bonds-request-fail.list');
        Route::get('show/{id}', 'BuyBondsRequestFailController@show')->name('admin.buy-bonds-request-fail.view');
        Route::get('export', 'BuyBondsRequestFailController@export')->name('admin.buy-bonds-request-fail.export');
    });

    // nhandt
    // Quản lý yêu cầu mua cổ phiếu
    Route::group(['prefix' => 'buy-stock-request'], function () {
        Route::get('/', 'BuyStockRequestController@indexAction')->name('admin.buy-stock-request');
        Route::post('list', 'BuyStockRequestController@listAction')->name('admin.buy-stock-request.list');
        Route::get('detail', 'BuyStockRequestController@detailAction')->name('admin.buy-stock-request.detail');
        Route::post('get-list-receipt-detail', 'BuyStockRequestController@getListReceiptDetail')->name('admin.buy-stock-request.get-list-receipt-detail');
        Route::post('allows-confirm-and-create-receipt', 'BuyStockRequestController@allowsConfirmAndCreateReceipt')->name('admin.buy-stock-request.allows-confirm-and-create-receipt');
        Route::post('show-popup-receipt-detail', 'BuyStockRequestController@showPopupReceiptDetail')->name('admin.buy-stock-request.show-popup-receipt-detail');
        Route::post('add-receipt-detail', 'BuyStockRequestController@addReceiptDetail')->name('admin.buy-stock-request.add-receipt-detail');
        Route::post('create-bill', 'BuyStockRequestController@createBill')->name('admin.buy-stock-request.create-bill');
        Route::post('cancel-confirm', 'BuyStockRequestController@cancelConfirm')->name('admin.buy-stock-request.cancel-confirm');
    });

    //Quản lý chợ cổ phiếu
    Route::group(['prefix' => 'stock-market'], function () {
        Route::get('/selling', 'StockMarketController@indexAction')->name('admin.stock-market.selling');
        Route::post('/selling/list', 'StockMarketController@listAction')->name('admin.stock-market.selling.list');
        Route::get('/buy-stock', 'StockMarketController@indexActionBuyStock')->name('admin.stock-market.buy-stock');
        Route::post('/buy-stock/list', 'StockMarketController@listActionBuyStock')->name('admin.stock-market.buy-stock.list');
        Route::get('/buy-stock/detail', 'StockMarketController@detailActionBuyStock')->name('admin.stock-market.buy-stock.detail');
        Route::get('/register-to-sell', 'StockMarketController@indexActionRegisterToSell')->name('admin.stock-market.register-to-sell');
        Route::post('/register-to-sell/list', 'StockMarketController@listActionRegisterToSell')->name('admin.stock-market.register-to-sell.list');
        Route::get('/register-to-sell/detail', 'StockMarketController@detailActionRegisterToSell')->name('admin.stock-market.register-to-sell.detail');
        Route::post('/register-to-sell/change-status', 'StockMarketController@ChangeStatusRegisterToSell')->name('admin.stock-market.register-to-sell.change-status');

    });

    Route::group(['prefix' => 'stock-category'], function () {
        Route::get('/', 'StockCategoryController@index')->name('admin.stock-category');
        Route::post('/', 'StockCategoryController@listAction')->name('admin.stock-category.list');
        Route::post('/add-stock-category', 'StockCategoryController@addStockCategory')->name('admin.stock-category.add-stock-category');
        Route::post('/edit-stock-category', 'StockCategoryController@editStockCategory')->name('admin.stock-category.edit-stock-category');
        Route::post('/detail-stock-category', 'StockCategoryController@detailStockCategory')->name('admin.stock-category.detail-stock-category');
        Route::post('/save', 'StockCategoryController@save')->name('admin.stock-category.save');
        Route::post('remove-category', 'StockCategoryController@removeCategory')->name('admin.stock-category.remove-category');
    });

    Route::group(['prefix' => 'stock-news'], function () {
        Route::get('/', 'StockNewsController@index')->name('admin.stock-news');
        Route::post('/', 'StockNewsController@listAction')->name('admin.stock-news.list');
        Route::get('/create', 'StockNewsController@create')->name('admin.stock-news.create');
        Route::get('/edit/{id}', 'StockNewsController@edit')->name('admin.stock-news.edit');
        Route::get('/detail/{id}', 'StockNewsController@detail')->name('admin.stock-news.detail');
        Route::post('/store', 'StockNewsController@store')->name('admin.stock-news.store');
        Route::post('/update', 'StockNewsController@update')->name('admin.stock-news.update');
        Route::post('/remove', 'StockNewsController@remove')->name('admin.stock-news.remove');

        Route::post('/upload', 'StockNewsController@upload')->name('admin.stock-news.upload');
        Route::post('/uploadImage', 'StockNewsController@uploadImage')->name('admin.stock-news.uploadImage');
    });

    Route::group(['prefix' => 'stock-tutorial'], function () {
        Route::get('/', 'StockTutorialController@index')->name('admin.stock-tutorial');
        Route::get('/edit', 'StockTutorialController@edit')->name('admin.stock-tutorial.edit');
        Route::post('/update', 'StockTutorialController@update')->name('admin.stock-tutorial.update');
        Route::post('/upload', 'StockTutorialController@upload')->name('admin.stock-tutorial.upload');
    });

    // Quản lý yêu cầu chuyển nhượng
    Route::group(['prefix' => 'transfer-request'], function () {
        Route::get('/', 'TransferRequestController@indexAction')->name('admin.transfer-request');
        Route::post('/list', 'TransferRequestController@listAction')->name('admin.transfer-request.list');
        Route::get('/detail', 'TransferRequestController@detailAction')->name('admin.transfer-request.detail');
        Route::post('/change-status', 'TransferRequestController@changeStatus')->name('admin.transfer-request.change-status');
    });


//    Gia hạn hợp đồng
    Route::group(['prefix' => 'extend-contract'], function () {
        Route::get('/', 'ExtendContractController@indexAction')->name('admin.extend-contract');
        Route::post('list', 'ExtendContractController@listAction')->name('admin.extend-contract.list');
        Route::get('show/{id}', 'ExtendContractController@show')->name('admin.extend-contract.view');
        Route::post("create-receipt", "ExtendContractController@createReceipt")->name("admin.extend-contract.createReceipt");
        Route::post("create-wallet", "ExtendContractController@createWallet")->name("admin.extend-contract.createWallet");
        //tạo phiếu thu
        Route::post('change-status', 'ExtendContractController@changeStatusOderAction')->name('admin.extend-contract.change-status');
        //chuyển trạng thái đơn hàng
        Route::post('upload-dropzone', 'ExtendContractController@uploadDropzoneAction')->name('admin.extend-contract.upload-dropzone');
        Route::post('get-list-receipt-detail', 'ExtendContractController@getListReceiptDetail')->name('admin.extend-contract.get-list-receipt-detail');
        Route::post('add-receipt-detail', 'ExtendContractController@addReceiptDetail')->name('admin.extend-contract.add-receipt-detail');
        Route::post('show-popup-receipt-detail', 'ExtendContractController@showPopupReceiptDetail')->name('admin.extend-contract.show-popup-receipt-detail');
        Route::post('print-bill', 'ExtendContractController@printBill')->name('admin.extend-contract.print-bill');
        Route::post('create-bill', 'ExtendContractController@createBill')->name('admin.extend-contract.create-bill');
        //up dảnh hóa đơn
    });

    Route::group(['prefix' => 'manager-extend-contract'], function () {
        Route::get('/', 'ManagerExtendContractController@index')->name('admin.manager-extend-contract');
        Route::post('list', 'ManagerExtendContractController@listAction')->name('admin.manager-extend-contract.list');
        Route::get('extend-contract/{id}', 'ManagerExtendContractController@extendContract')->name('admin.manager-extend-contract.extendContract');
        Route::post('check-extend-contract', 'ManagerExtendContractController@checkExtendContract')->name('admin.manager-extend-contract.checkExtendContract');
        Route::post('check-value', 'ManagerExtendContractController@checkValue')->name('admin.manager-extend-contract.checkValue');
        Route::post('upload-dropzone-action', 'ManagerExtendContractController@uploadAction')->name('admin.manager-extend-contract.uploadDropzoneAction');
        Route::post('create-bill', 'ManagerExtendContractController@createBill')->name('admin.manager-extend-contract.create-bill');
        Route::post('print-bill', 'ManagerExtendContractController@printBill')->name('admin.manager-extend-contract.print-bill');
        Route::post('check-create-contract', 'ManagerExtendContractController@checkCreateContract')->name('admin.manager-extend-contract.check-create-contract');
        //up dảnh hóa đơn
    });

    
    // VSET editing request list
    Route::group(['prefix' => 'customer-change-request'], function () {
        Route::get('/', 'VsetCustomerChangeRequestController@indexAction')->name('admin.customer-change-request');
        Route::get('/export', 'VsetCustomerChangeRequestController@exportAction')->name('admin.customer-change-request.export');
        Route::post('list', 'VsetCustomerChangeRequestController@listAction')->name('admin.customer-change-request.list');
        Route::get('detail/{id}', 'VsetCustomerChangeRequestController@detailAction')->name('admin.customer-change-request.detail');
        Route::post('approveChangeRequest', 'VsetCustomerChangeRequestController@approveChangeRequest')->name('admin.customer-change-request.approve');
        Route::get('export-pdf/{id}', 'VsetCustomerChangeRequestController@exportPdf')->name('admin.customer-change-request.exportPdf');
        Route::post('/upload-appendix-contract', 'VsetCustomerChangeRequestController@uploadAppendixContract')->name('admin.customer-change-request.uploadAppendixContract');
        Route::post('/save-appendix-contract-map', 'VsetCustomerChangeRequestController@saveAppendixContractMap')->name('admin.customer-change-request.saveAppendixContractMap');
        Route::post('/cancel-request', 'VsetCustomerChangeRequestController@cancelRequest')->name('admin.customer-change-request.cancelRequest');
    });
      // VSET Stock
    // VSET Stock
    Route::group(['prefix' => 'stock'], function () {
        // todo: Stock Info-----------
        Route::get('/', 'VsetStockController@indexAction')->name('admin.stock.list');
        Route::get('edit/{id}', 'VsetStockController@edit')->name('admin.stock.edit');
        Route::post('upload', 'VsetStockController@uploadRelatedDoc')->name('admin.stock.uploadRelatedDoc');
        Route::post('editStockInfo', 'VsetStockController@saveStockInfo')->name('admin.stock.saveStockInfo');
        Route::post('save-file', 'VsetStockController@saveFile')->name('admin.stock.saveFile');
        Route::post('remove-file', 'VsetStockController@removeStockFile')->name('admin.stockFile.remove');
//        todo: Cấu hình thưởng--------------------------
        Route::group(['prefix' => 'config-bonus'], function () {
            Route::post('/edit', 'VsetStockController@editStockConfigBonus')->name('admin.stock.config-bonus.edit');
            Route::group(['prefix' => 'order'], function () {
                Route::get('/', 'VsetStockController@indexBonusOrderAction')->name('admin.stock.config-bonus.order');
                Route::post('/list', 'VsetStockController@listBonusOrder')->name('admin.stock.config-bonus.order.list');
                Route::post('/add', 'VsetStockController@addBonusOrder')->name('admin.stock.config-bonus.order.add');
                Route::post('/edit', 'VsetStockController@editBonusOrder')->name('admin.stock.config-bonus.order.edit');
                Route::post('/remove', 'VsetStockController@removeBonusOrder')->name('admin.stock.config-bonus.order.remove');

            });
            Route::group(['prefix' => 'payment-method'], function () {
                Route::get('/', 'VsetStockController@indexBonusPaymentAction')->name('admin.stock.config-bonus.payment');
                Route::get('/edit', 'VsetStockController@editBonusPaymentAction')->name('admin.stock.config-bonus.payment.edit');
                Route::post('/edit', 'VsetStockController@editBonusPayment')->name('admin.stock.config-bonus.payment.submitEdit');
            });
            Route::group(['prefix' => 'referral'], function () {
                Route::get('/', 'VsetStockController@indexBonusReferralAction')->name('admin.stock.config-bonus.referral');
                Route::post('/list', 'VsetStockController@listBonusReferral')->name('admin.stock.config-bonus.referral.list');
                Route::post('/add', 'VsetStockController@addBonusReferral')->name('admin.stock.config-bonus.referral.submitAdd');
                Route::post('/edit', 'VsetStockController@editBonusReferral')->name('admin.stock.config-bonus.referral.submitEdit');
                Route::post('/remove', 'VsetStockController@removeReferral')->name('admin.stock.config-bonus.referral.remove');
            });
            Route::group(['prefix' => 'transfer'], function () {
                Route::get('/', 'VsetStockController@indexBonusTransferAction')->name('admin.stock.config-bonus.transfer');
                Route::post('/edit', 'VsetStockController@editBonusTransfer')->name('admin.stock.config-bonus.transfer.submitEdit');
                Route::post('/list', 'VsetStockController@listBonusTransfer')->name('admin.stock.config-bonus.transfer.list');
            });

//            Route::get('/payment-method', 'VsetStockController@indexBonusPaymentAction')->name('admin.stock.config-bonus.payment');



        });
//        end Cấu hình thưởng----------------------------

        // todo: Stock Cost--------------
        Route::get('/cost', 'VsetStockController@detailStockCostAction')->name('admin.stock.getCost');
        Route::get('/edit-cost', 'VsetStockController@editStockCostAction')->name('admin.stock.editCost');
        Route::post('/edit', 'VsetStockController@editStockConfig')->name('admin.stock.editPost');
    });
    // todo: Stock Publish------------
    Route::group(['prefix' => 'stock-publish'], function () {
        Route::get('/', 'VsetStockController@indexStockPublishAction')->name('admin.stock-publish.list');
        Route::get('/edit', 'VsetStockController@getStockPublishEdit')->name('admin.stock.list-publish');
        Route::post('/edit', 'VsetStockController@editStockPublish')->name('admin.editStockPublish');
        Route::post('/list', 'VsetStockController@listHistoryPublish')->name('admin.listStockPublish');
        Route::post('/publish', 'VsetStockController@publishStock')->name('admin.stock.publish');
        Route::get('/export', 'VsetStockController@exportExcelAction')->name('admin.stock-publish.export');        

        // todo: Cấu hình thưởng--------
        Route::group(['prefix' => 'bonus'], function () {
            Route::get('/', 'VsetStockController@indexStockPublishBonusAction')->name('admin.stock-publish.bonus');
            Route::post('/edit', 'VsetStockController@submitEditStockPublishBonus')->name('admin.stock-publish.bonus.submitEdit');

            
        });

    });
    // !--------Stock Publish------------

    //todo: Stock Transfer---------------------------
    Route::group(['prefix' => 'stock-transfer'], function () {
        // todo: Stock Order-----------
        // todo: Stock Order----------
        //-
        Route::get('/', 'StockTransferContractController@indexAction')->name('admin.stock-transfer.index');
        Route::get('/saving', 'StockTransferContractController@indexAction')->name('admin.stock-transfer.getListSaving');
        Route::get('/bond', 'StockTransferContractController@getListBond')->name('admin.stock-transfer.getListBond');
        Route::get('/{id}', 'StockTransferContractController@detail')->name('admin.stock-transfer.detail');
        Route::post('/list', 'StockTransferContractController@listAction')->name('admin.stock-transfer.listSaving');
        Route::post('/confirm-transfer', 'StockTransferContractController@confirmTransfer')->name('admin.stock-transfer.confirmTransfer');
        Route::post('/cancel-request', 'StockTransferContractController@cancelRequest')->name('admin.stock-transfer.cancelRequest');
    });

    // todo: Dividend--------------------------------
    Route::group(['prefix' => 'dividend'], function () {       
        Route::get('/', 'DividendController@indexAction')->name('admin.dividend.getList');
        Route::post('/add', 'DividendController@add')->name('admin.dividend.add');
        Route::post('/edit', 'DividendController@edit')->name('admin.dividend.edit');        
        Route::post('/list', 'DividendController@listAction')->name('admin.dividend.list');
        Route::post('/upload', 'DividendController@uploadFile')->name('admin.dividend.upload');
        Route::post('/save-file', 'DividendController@saveFile')->name('admin.dividend.saveFile');
        
    });

    // !---------------------------------------------

    //todo: Stock Chart---------------------------
    Route::group(['prefix' => 'stock-chart'], function () {
        Route::get('/', 'StockChartController@indexAction')->name('admin.stock-chart.index');
        Route::post('/add', 'StockChartController@add')->name('admin.stock-chart.add');
        Route::post('/edit', 'StockChartController@edit')->name('admin.stock-chart.edit');
        Route::post('/list', 'StockChartController@listAction')->name('admin.stock-chart.list');
        Route::post('/import-excel', 'StockChartController@importExcel')->name('admin.stock-chart.import');
        Route::post('/remove', 'StockChartController@remove')->name('admin.stock-chart.remove');

    });
//  !---------------------------------------------
    //    Vset Stock Contract
    Route::group(['prefix' => 'stock-contract'], function () {
        Route::get('/', 'VsetStockController@indexStockContractAction')->name('admin.stock-contract.index');
        Route::post('/list', 'VsetStockController@listStockContractAction')->name('admin.stock-contract.list');
        Route::post('/list-datatable', 'VsetStockController@listStockContractDatatable')->name('admin.stock-contract.list-datatable');
        Route::post('/list-file', 'VsetStockController@listStockContractFileAction')->name('admin.stock-contract.list-file');
        Route::post('/list-sub-file', 'VsetStockController@listStockContractSubFileAction')->name('admin.stock-contract.list-sub-file');
        Route::get('detail/{id}', 'VsetStockController@detailStockContractAction')->name('admin.stock-contract.detail');
        Route::get('edit/{id}', 'VsetStockController@editStockContractAction')->name('admin.stock-contract.edit');
        Route::post('edit', 'VsetStockController@submitEditStockContract')->name('admin.stock-contract.submitEditStockContract');
        Route::post('/uploadFile', 'VsetStockController@uploadFileStockContract')->name('admin.stock-contract.upload');
        Route::get('export-excel', 'VsetStockController@exportStockContractExcelAction')->name('admin.stock-contract.exportExcel');


    });



    // VSET CUSTOMER
    Route::group(['prefix' => 'customer'], function () {
        Route::get('/', 'VsetCustomerController@indexAction')->name('admin.customer');
        Route::get('/export', 'VsetCustomerController@exportAction')->name('admin.customer.export');
        Route::post('list', 'VsetCustomerController@listAction')->name('admin.customer.list');
        Route::get('detail/{id}', 'VsetCustomerController@detailAction')->name('admin.customer.detail');
        Route::get('editCustomer/{id}', 'VsetCustomerController@editInfoCustomerAction')->name('admin.customer.edit');
        Route::post('update', 'VsetCustomerController@submitEditAction')->name('admin.customer.submitEdit');

        Route::get('export-excel', 'VsetCustomerController@exportExcelAction')->name('admin.customer.export-excel');
        Route::post('list-history-customer-vset', 'VsetCustomerController@operationHistory')->name('admin.customer.list-history-customer-vset');
        Route::post('list-stock-history-customer-vset', 'VsetCustomerController@operationStockHistory')->name('admin.customer.list-stock-history-customer-vset');
        Route::post('list-contract-customer-vset', 'VsetCustomerController@listContractCustomer')->name('admin.customer.list-contract-customer-vset');

        Route::post('load-district', 'VsetCustomerController@loadDistrictAction')->name('admin.customer.load-district');

        Route::post('uploads-vset', 'VsetCustomerController@uploadAction')->name('admin.customer.uploads');
        Route::post('change-status', 'VsetCustomerController@changeStatusAction')->name('admin.customer.change-status');

        Route::post('show-reset-pass', 'VsetCustomerController@showResetPassword')->name('admin.customer.show-reset-pass');
        Route::post('submit-reset-pass', 'VsetCustomerController@submitResetPassword')->name('admin.customer.submit-reset-pass');
        Route::post('list-introduct', 'VsetCustomerController@listIntroduct')->name('admin.customer.list-introduct');
        Route::post('list-contract-commission', 'VsetCustomerController@listContractCommission')->name('admin.customer.list-contract-commission');
        Route::post('change-status', 'VsetCustomerController@changeStatus')->name('admin.customer.changeStatus');
	 // VSET CUSTOMER TOP 100
	  // VSET CUSTOMER TOP 100
    Route::group(['prefix' => 'customer-top'], function () {
            Route::get('/', 'VsetCustomerController@indexAction')->name('admin.customer-top100');

    });

    Route::group(['prefix' => 'customer-broker'], function () {
        Route::get('/', 'VsetCustomerBrokerController@indexAction')->name('admin.customer-broker');
        Route::post('list', 'VsetCustomerBrokerController@listAction')->name('admin.customer-broker.list');
        Route::get('detail/{id}', 'VsetCustomerBrokerController@detailAction')->name('admin.customer-broker.detail');
        Route::get('editCustomer/{id}', 'VsetCustomerBrokerController@editInfoCustomerAction')->name('admin.customer-broker.edit');
        Route::post('update', 'VsetCustomerBrokerController@submitEditAction')->name('admin.customer-broker.submitEdit');

        Route::get('export-excel', 'VsetCustomerBrokerController@exportExcelAction')->name('admin.customer-broker.export-excel');
        Route::post('list-history-customer-vset', 'VsetCustomerBrokerController@operationHistory')->name('admin.customer-broker.list-history-customer-vset');
        Route::post('list-contract-customer-vset', 'VsetCustomerBrokerController@listContractCustomer')->name('admin.customer-broker.list-contract-customer-vset');
        Route::post('list-contract-commission', 'VsetCustomerBrokerController@listContractCommission')->name('admin.customer-broker.list-contract-commission');
        Route::post('list-introduct', 'VsetCustomerBrokerController@listIntroduct')->name('admin.customer-broker.list-introduct');

        Route::post('load-district', 'VsetCustomerBrokerController@loadDistrictAction')->name('admin.customer-broker.load-district');

        Route::post('uploads-vset', 'VsetCustomerBrokerController@uploadAction')->name('admin.customer-broker.uploads');
        Route::post('change-status', 'VsetCustomerBrokerController@changeStatusAction')->name('admin.customer-broker.change-status');

        Route::post('show-reset-pass', 'VsetCustomerBrokerController@showResetPassword')->name('admin.customer-broker.show-reset-pass');
        Route::post('submit-reset-pass', 'VsetCustomerBrokerController@submitResetPassword')->name('admin.customer-broker.submit-reset-pass');
        Route::get('/config', 'VsetCustomerBrokerController@configAction')->name('admin.customer-broker.config');
    });

    // VSET CUSTOMER
    Route::group(['prefix' => 'potential-customers'], function () {
        Route::get('/', 'VsetPotentailCustomerController@indexAction')->name('admin.potential-customers');
        Route::post('list', 'VsetPotentailCustomerController@listAction')->name('admin.potential-customers.list');
        Route::get('detail/{id}', 'VsetPotentailCustomerController@detailAction')->name('admin.potential-customers.detail');
        Route::get('editCustomer/{id}', 'VsetPotentailCustomerController@editInfoCustomerAction')->name('admin.potential-customers.edit');
        Route::post('update', 'VsetPotentailCustomerController@submitEditAction')->name('admin.potential-customers.submitEdit');

        Route::get('export-excel', 'VsetPotentailCustomerController@exportExcelAction')->name('admin.potential-customers.export-excel');
        Route::post('list-history-customer-vset', 'VsetPotentailCustomerController@operationHistory')->name('admin.potential-customers.list-history-customer-vset');
        Route::post('list-contract-customer-vset', 'VsetPotentailCustomerController@listContractCustomer')->name('admin.potential-customers.list-contract-customer-vset');
        Route::post('list-product-view', 'VsetPotentailCustomerController@listProductView')->name('admin.potential-customers.list-product-view');
        Route::post('list-customer-search', 'VsetPotentailCustomerController@listCustomerSearch')->name('admin.potential-customers.list-customer-search');

        Route::post('load-district', 'VsetPotentailCustomerController@loadDistrictAction')->name('admin.potential-customers.load-district');

        Route::post('uploads-vset', 'VsetPotentailCustomerController@uploadAction')->name('admin.potential-customers.uploads');
        Route::post('change-status', 'VsetPotentailCustomerController@changeStatusAction')->name('admin.potential-customers.change-status');

        Route::post('show-reset-pass', 'VsetPotentailCustomerController@showResetPassword')->name('admin.potential-customers.show-reset-pass');
        Route::post('submit-reset-pass', 'VsetPotentailCustomerController@submitResetPassword')->name('admin.potential-customers.submit-reset-pass');
    });

    // VSET Appointment schedule

    Route::group(['prefix' => 'customer-appointment'], function () {
        Route::get('/', 'VsetCustomerAppointmentController@indexAction')->name('customer-appointment');
        Route::post('list', 'VsetCustomerAppointmentController@listAction')->name('admin.customer-appointment.list');
        Route::get('add', 'VsetCustomerAppointmentController@addAction')->name('admin.customer-appointment.add');
//        Route::post('list', 'VsetCustomerAppointmentController@listAction')->name('admin.customer.list');
    });

//    Cấu hình ẩn hiện module
    Route::group(['prefix' => 'config-module'], function () {
        Route::get('/', 'ConfigModuleController@indexAction')->name('admin.config-module');
        Route::post('change-active', 'ConfigModuleController@changeActiveAction')->name('admin.config-module.change-active');
    });

// VSET Appointment schedule

    Route::group(['prefix' => 'banner'], function () {
        Route::get('/', 'BannerController@indexAction')->name('admin.banner');
        Route::post('list', 'BannerController@listAction')->name('admin.banner.list');
        Route::get('add', 'BannerController@addAction')->name('admin.banner.add');
        Route::get('detail/{id}', 'BannerController@detailAction')->name('admin.banner.detail');
        Route::get('edit/{id}', 'BannerController@editAction')->name('admin.banner.edit');
        Route::post('submit-add', 'BannerController@submitAddAction')->name('admin.banner.submit-add');
        Route::post('submit-edit', 'BannerController@submitEditAction')->name('admin.banner.submit-edit');
    });


    // VSet fap-group
    Route::group(['prefix' => 'faq-group'], function () {
        Route::get('/', 'FaqGroupController@index')->name('admin.faq-group.index');
        Route::post('list', 'FaqGroupController@listAction')->name('admin.faq-group.list');
        Route::get('show/{id}', 'FaqGroupController@show')
            ->name('admin.faq-group.show')
            ->where('id', '[0-9]+');
        Route::get('create', 'FaqGroupController@create')->name('admin.faq-group.create');
        Route::post('store', 'FaqGroupController@store')->name('admin.faq-group.store');
        Route::get('edit/{id}', 'FaqGroupController@edit')->name('admin.faq-group.edit')->where('id', '[0-9]+');
        Route::post('update', 'FaqGroupController@update')->name('admin.faq-group.update');
        Route::post('destroy', 'FaqGroupController@destroy')->name('admin.faq-group.destroy');
        Route::post('update-status', 'FaqGroupController@updateStatus')->name('admin.faq-group.update-status');
    });

    // VSet fap
    Route::group(['prefix' => 'faq'], function () {
        Route::get('/', 'FaqController@index')->name('admin.faq.index');
        Route::post('list', 'FaqController@listAction')->name('admin.faq.list');
        Route::get('show/{id}', 'FaqController@show')->name('admin.faq.show')
            ->where('id', '[0-9]+');
        Route::get('create', 'FaqController@create')->name('admin.faq.create');
        Route::post('store', 'FaqController@store')->name('admin.faq.store');
        Route::get('edit/{id}', 'FaqController@edit')->name('admin.faq.edit')->where('id', '[0-9]+');
        Route::post('update', 'FaqController@update')->name('admin.faq.update');
        Route::post('destroy', 'FaqController@destroy')->name('admin.faq.destroy');
        Route::post('update-status', 'FaqController@updateStatus')->name('admin.faq.update-status');
    });

//    Vset
    Route::group(['prefix' => 'bank'], function () {
        Route::get('/', 'BankController@index')->name('admin.bank');
        Route::get('create', 'BankController@create')->name('admin.bank.create');
        Route::post('add', 'BankController@add')->name('admin.bank.add');
        Route::post('list', 'BankController@listAction')->name('admin.bank.list');
        Route::post('detail-form-bank', 'BankController@detailFormBank')->name('admin.bank.detail-form-bank');
        Route::post('edit-form-bank', 'BankController@editFormBank')->name('admin.bank.edit-form-bank');
        Route::post('update', 'BankController@update')->name('admin.bank.update');
        Route::post('add-form-bank-new', 'BankController@addFormBankNew')->name('admin.bank.add-form-bank-new');
        Route::post('remove', 'BankController@remove')->name('admin.bank.remove');
    });

//    VSet order service
    Route::group(['prefix' => 'order-service'], function () {
        Route::get('/', 'OrderServiceController@indexAction')->name('admin.order-service');
        Route::post('list', 'OrderServiceController@listAction')->name('admin.order-service.list');
        Route::get('show/{id}', 'OrderServiceController@show')->name('admin.order-service.view');
        Route::post("create-receipt", "OrderServiceController@createReceipt")->name("admin.order-service.createReceipt");
        //tạo phiếu thu
        Route::post('change-status', 'OrderServiceController@changeStatusOderAction')->name('admin.order-service.change-status');
        //chuyển trạng thái đơn hàng
        Route::post('upload-dropzone', 'OrderServiceController@uploadDropzoneAction')->name('admin.order-service.upload-dropzone');
        Route::post('get-list-receipt-detail', 'OrderServiceController@getListReceiptDetail')->name('admin.order-service.get-list-receipt-detail');
        Route::post('add-receipt-detail', 'OrderServiceController@addReceiptDetail')->name('admin.order-service.add-receipt-detail');
        Route::post('show-popup-receipt-detail', 'OrderServiceController@showPopupReceiptDetail')->name('admin.order-service.show-popup-receipt-detail');
        Route::post('create-Wallet', 'OrderServiceController@createWallet')->name('admin.order-service.createWallet');
        //up dảnh hóa đơn
    });

//    VSet service serial
    Route::group(['prefix' => 'service-serial'], function () {
        Route::get('/', 'ServiceSerialController@indexAction')->name('admin.service-serial');
        Route::post('list', 'ServiceSerialController@listAction')->name('admin.service-serial.list');
        Route::get('show/{id}', 'ServiceSerialController@showAction')->name('admin.service-serial.show');
        Route::get('edit/{id}', 'ServiceSerialController@editAction')->name('admin.service-serial.edit');
        Route::post('edit-post', 'ServiceSerialController@editPost')->name('admin.service-serial.editPost');
    });

    Route::group(['prefix' => 'log-email'], function () {
        Route::get('/', 'LogEmailController@indexAction')->name('admin.log-email');
        Route::post('/list', 'LogEmailController@listAction')->name('admin.log-email.list');
    });

    Route::group(['prefix' => 'log-noti'], function () {
        Route::get('/', 'LogNotiController@indexAction')->name('admin.log-noti');
        Route::post('/list', 'LogNotiController@listAction')->name('admin.log-noti.list');
        Route::get('/show/{id}', 'LogNotiController@showAction')->name('admin.log-noti.show');
    });
    Route::group(['prefix' => 'report'], function () {
        Route::get('/revenue', 'ReportController@revenue')
            ->name('admin.report.revenue');
        Route::post('/filter-revenue', 'ReportController@filterRevenue')
            ->name('admin.report.filterRevenue');
        Route::get('/interest', 'ReportController@interest')
            ->name('admin.report.interest');
        Route::post('/filter-interest', 'ReportController@filterInterest')
            ->name('admin.report.filterInterest');
        Route::get('/expected-interest-payment', 'ReportController@expectedInterestPayment')
            ->name('admin.report.expectedInterestPayment');
        Route::get('/expected-interest-payment/export', 'ReportController@exportExpectedInterestPayment')
            ->name('admin.report.expectedInterestPayment.export');
        Route::post('/filter-expected-interest-payment', 'ReportController@filterExpectedInterestPayment')
            ->name('admin.report.filterExpectedInterestPayment');
        Route::get('/customerContractLog/{id}', 'ReportController@customerContractLog')
            ->name('admin.report.customerContractLog');

        Route::get('update-report', 'ReportController@updateReport')
            ->name('admin.report.updateReport');

        Route::get('share-link', 'ReportController@shareLink')
            ->name('admin.report.shareLink');
        Route::post('share-link-filter', 'ReportController@shareLinkFilter')
            ->name('admin.report.shareLinkFilter');
        Route::get('sales', 'ReportController@salesAction')
            ->name('admin.report.sales');
        Route::post('sales-filter', 'ReportController@salesFilter')
            ->name('admin.report.salesFilter');
        Route::post('sum-sales-filter', 'ReportController@sumSalesFilter')
            ->name('admin.report.sumSalesFilter');
        Route::post('sum-group-filter', 'ReportController@sumGroupFilter')
            ->name('admin.report.sumGroupFilter');
	Route::get('/stock-revenue', 'ReportStockRevenueController@indexAction')
            ->name('admin.report.stock-revenue');
        Route::post('/stock-revenue/filter', 'ReportStockRevenueController@filter')
            ->name('admin.report.stock-revenue.filter');

        Route::get('/statistics-transaction', 'ReportStatisticsTransactionController@indexAction')
            ->name('admin.report.statistics-transaction');
        Route::post('/statistics-transaction/filter', 'ReportStatisticsTransactionController@filter')
            ->name('admin.report.statistics-transaction.filter');
    });

    Route::group(['prefix' => 'send-email'], function () {
        Route::get('', 'SendEmailController@index')->name('admin.send-email');
        Route::post('update', 'SendEmailController@update')->name('admin.send-email.update');
    });

    Route::group(['prefix' => 'config-term-investment'], function () {
        Route::get('', 'ConfigTermInvestmentController@index')->name('admin.config-term-investment');
        Route::post('list', 'ConfigTermInvestmentController@list')->name('admin.config-term-investment.list');
        Route::post('create', 'ConfigTermInvestmentController@create')->name('admin.config-term-investment.create');
        Route::post('store', 'ConfigTermInvestmentController@store')->name('admin.config-term-investment.store');
        Route::post('update', 'ConfigTermInvestmentController@update')->name('admin.config-term-investment.update');
        Route::post('delete', 'ConfigTermInvestmentController@delete')->name('admin.config-term-investment.delete');
        Route::post('show-popup', 'ConfigTermInvestmentController@showPopup')->name('admin.config-term-investment.showPopup');
        Route::post('chang-status', 'ConfigTermInvestmentController@changStatus')->name('admin.config-term-investment.changStatus');
    });

    //Nhóm khách hàng
    Route::group(['prefix' => 'group-journeys'], function () {
        Route::get('/create', 'GroupCustomerController@create')->name('admin.group-customer.create');
        Route::post('/load-customer', 'GroupCustomerController@loadCustomer')
            ->name('admin.group-customer.loadCustomer');
        Route::post('/checked-customer', 'GroupCustomerController@checkedCustomer')
            ->name('admin.group-customer.checkedCustomer');
        Route::post('/remove-session-temp', 'GroupCustomerController@removeSessionTemp')
            ->name('admin.group-customer.removeSessionTemp');
        Route::post('/submit-add-item', 'GroupCustomerController@submitAddItem')
            ->name('admin.group-customer.submitAddItem');
        Route::post('remove-item', 'GroupCustomerController@removeItem')
            ->name('admin.group-customer.removeItem');
        Route::post('store', 'GroupCustomerController@store')
            ->name('admin.group-customer.store');
        Route::get('/edit/{id}', 'GroupCustomerController@edit')->name('admin.group-customer.edit');
        Route::post('update', 'GroupCustomerController@update')
            ->name('admin.group-customer.update');
        Route::get('/show/{id}', 'GroupCustomerController@show')->name('admin.group-customer.show');
        Route::get('/', 'GroupCustomerController@index')->name('admin.group-customer');
        Route::post('list', 'GroupCustomerController@listAction')->name('admin.group-customer.list');
        Route::post('destroy', 'GroupCustomerController@destroy')->name('admin.group-customer.destroy');
    });
    //Nhóm nhân viên
    Route::group(['prefix' => 'group-staff'], function () {
        Route::get('/create', 'GroupStaffController@create')->name('admin.group-staff.create');
        Route::post('/load-staff', 'GroupStaffController@loadStaff')
            ->name('admin.group-staff.loadStaff');
        Route::post('/checked-item', 'GroupStaffController@checkedItem')
            ->name('admin.group-staff.checkedItem');
        Route::post('/remove-session-temp', 'GroupStaffController@removeSessionTemp')
            ->name('admin.group-staff.removeSessionTemp');
        Route::post('/submit-add-item', 'GroupStaffController@submitAddItem')
            ->name('admin.group-staff.submitAddItem');
        Route::post('remove-item', 'GroupStaffController@removeItem')
            ->name('admin.group-staff.removeItem');
        Route::post('store', 'GroupStaffController@store')
            ->name('admin.group-staff.store');
        Route::get('/edit/{id}', 'GroupStaffController@edit')->name('admin.group-staff.edit');
        Route::post('update', 'GroupStaffController@update')
            ->name('admin.group-staff.update');
        Route::get('/show/{id}', 'GroupStaffController@show')->name('admin.group-staff.show');
        Route::get('/', 'GroupStaffController@index')->name('admin.group-staff');
        Route::post('list', 'GroupStaffController@listAction')->name('admin.group-staff.list');
        Route::post('destroy', 'GroupStaffController@destroy')->name('admin.group-staff.destroy');
        Route::get('/authorization/{id}', 'GroupStaffController@authorization')
            ->name('admin.group-staff.authorization');
        Route::post('update-authorization', 'GroupStaffController@updateAuthorization')
            ->name('admin.group-staff.updateAuthorization');
    });
    Route::get('translate', function () {
        $lang    = \Illuminate\Support\Facades\App::getLocale();
        if ($lang == 'en') {
            $jsonString = file_get_contents(base_path('resources/lang/en.json'));
        } else if ($lang == 'vi') {
            $jsonString = file_get_contents(base_path('resources/lang/vi.json'));
        }

        return json_decode($jsonString, true);
    })->name('translate');
});
});
    Route::group(['prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
        Route::get('confirm/{id}', 'CustomerAppointmentController@confirmAction')->name('admin.customer_appointment.confirm');
        Route::get('/error/403', function () {
            return view('authorization.not-have-access');
        })->name('authorization.not-have-access');
    });
    Route::group(['middleware' => ['web'], 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
        Route::get('send-sms-log', 'SmsController@sendSmsAction')->name('admin.sms.send-sms-test');
        Route::get('send-email-job', 'EmailAutoController@sendEmailJobAction')->name('admin.email-auto.send-email-job');
        Route::get('run-log-email', 'EmailAutoController@runAutoAction')->name('admin.email-auto.run');
        Route::get('run-log-sms', 'SmsController@sendSmsNoEvent')->name('sms.send-sms-no-event');
        Route::get('run-reset-rank', 'ResetRankController@resetRankAction')->name('reset-rank');
        Route::get('testCalculateContractMonth', 'BranchController@testCalculateContractMonth')
            ->name('admin.testCalculateContractMonth');
        Route::get('testCalculateContractDay', 'BranchController@testCalculateContractDay')
            ->name('admin.testCalculateContractDay');
        Route::get('testCalculateContractTime', 'BranchController@testCalculateContractTime')
            ->name('admin.testCalculateContractTime');
        Route::get('testAccountStatement', 'BranchController@testAccountStatement')
            ->name('admin.testCalculateContractTime');
        Route::get('testWarningExtendContract', 'BranchController@testWarningExtendContract')
            ->name('admin.testWarningExtendContract');
        Route::get('testNotiInterestInDay', 'BranchController@testNotiInterestInDay')
            ->name('admin.testNotiInterestInDay');
        Route::get('testRequestReinestmentContract', 'BranchController@testRequestReinestmentContract')
            ->name('admin.testRequestReinestmentContract');
        Route::get('testSendNoti', 'BranchController@testSendNoti')
            ->name('admin.testSendNoti');


        Route::get('testDividend', 'BranchController@testDividend')
            ->name('admin.testDividend');


        Route::get('testReceiveStockBonus', 'BranchController@testReceiveStockBonus')
            ->name('admin.testReceiveStockBonus');
    });










