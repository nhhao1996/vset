<?php


namespace Modules\Admin\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;
use Modules\Admin\Repositories\StockHistory\StockHistoryRepoInterface;

class ReceiveStockBonusTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stockHistory;

    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Nhận cổ phiếu thưởng
     * @param StockHistoryRepoInterface $stockHistory
     */
    public function handle(StockHistoryRepoInterface $stockHistory)
    {
        $this->stockHistory = $stockHistory;
        $this->stockHistory->testReceiveStockBonus();
    }
}