<?php


namespace Modules\Admin\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;

class SendNotiJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $rCustomerContractInterest;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Tính customer contract interest theo tháng
     * @param CustomerContractInterestRepositoryInterface $rCustomerContractInterest
     */
    public function handle(CustomerContractInterestRepositoryInterface $rCustomerContractInterest)
    {
        $this->rCustomerContractInterest = $rCustomerContractInterest;
        $this->rCustomerContractInterest->sendNoti();
    }
}