<?php


namespace Modules\Admin\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;
use Modules\Admin\Repositories\StockHistory\StockHistoryRepoInterface;

class DividendTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stockHistory;

    /**
     * Create a new job instance.
     *
     * @param StockHistoryRepoInterface $stockHistory
     */
    public function __construct()
    {
    }

    /**
     * Chia cổ tức
     * @param StockHistoryRepoInterface $stockHistory
     */
    public function handle(StockHistoryRepoInterface $stockHistory)
    {
        $this->stockHistory = $stockHistory;
        $this->stockHistory->testDividend();
    }
}