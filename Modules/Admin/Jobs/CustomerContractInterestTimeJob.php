<?php

namespace Modules\Admin\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepositoryInterface;

class CustomerContractInterestTimeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $rCustomerContractInterest;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Tính customer contract interest theo tháng
     * @param CustomerContractInterestRepositoryInterface $rCustomerContractInterest
     */
    public function handle(CustomerContractInterestRepositoryInterface $rCustomerContractInterest)
    {
        $this->rCustomerContractInterest = $rCustomerContractInterest;
        $this->rCustomerContractInterest->calculateContract('time');
    }
}
