<?php


namespace Modules\Admin\Http\Api;


use Modules\Admin\Models\JobsEmailLogTable;
use MyCore\Api\ApiAbstract;

class JobsEmailLogApi extends ApiAbstract
{
    public function addJob(array $data=[])
    {
        if (env('APP_ENV') == 'production'){
            $jobEmailLog = new JobsEmailLogTable();
//        return $this->baseClient('/email-log/add-job',$data,false);
            if ($data['email_from'] != null && $data['email_to'] != null && $data['email_from'] != '' && $data['email_to'] != '') {
                $data = $jobEmailLog->addJob($data);
            } else {
                $data = [];
            }
            return $data;
        }
    }

    public function sendNotification(array $data=[])
    {
        if (env('APP_ENV') == 'production') {
            return $this->baseClient('/notification/send-notification', $data);
        }
    }

    public function sendNotificationArray(array $data=[])
    {
        if (env('APP_ENV') == 'production') {
            return $this->baseClient('/notification/send-notification-array', $data, false);
//        return $this->baseClient('/notification/send-notification-array',$data);
        }
    }

    public function checkExtendContract(array $data=[]) {
        if (env('APP_ENV') == 'production') {
            return $this->baseClient('/order/check-contract-extend', $data);
        }
    }

    public function sendSMS(array $data=[]) {
        if (env('APP_ENV') == 'production') {
            return $this->baseClient('/user/send-otp', $data);
        }
    }
}