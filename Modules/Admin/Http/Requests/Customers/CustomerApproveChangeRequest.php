<?php

namespace Modules\Admin\Http\Requests\Customers;

use Illuminate\Foundation\Http\FormRequest;

class CustomerApproveChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $param = request()->all();

//        var_dump($param);die;
        $id = $param['customer_id_hidden'];
        return [
            'bank_account_no' => 'digits_between:0,100|numeric|unique:customers,bank_account_no,'.$id.',customer_id,is_deleted,0',
            'bank_name' => 'max:255',
            // 'birthday' => 'required',
            // 'gender' => 'required',
            'phone' => 'digits_between:9,11|numeric|unique:customers,phone,'.$id.',customer_id,is_deleted,0',
            'phone2' => 'digits_between:9,11|numeric|unique:customers,phone2,'.$id.',customer_id,is_deleted,0',
            'ic_no' => 'digits_between:9,12|numeric|unique:customers,ic_no,'.$id.',customer_id,is_deleted,0',
            'ic_place' => 'max:255',

//            'blah_ic_front_image' => 'required',
//            'blah_ic_back_image' => 'required',

            // 'contact_province_id' => 'required',
            // 'contact_district_id' => 'required',
            'contact_address' => 'max:255',
            'residence_address' => 'max:255',
            // 'residence_province_id' => 'required',
            // 'residence_district_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'bank_account_no.required' => __('Hãy nhập số tài khoản'),
            'bank_account_no.digits_between' => __('Số tài khoản vượt quá 100 ký tự'),
//            'bank_account_no.min' => __('Số tài khoản tối thiểu phải được 12 ký tự'),
            'bank_account_no.numeric' => __('Số tài khoản phải là ký tự số'),
            'bank_account_no.unique' => __('Số tài khoản này đã được sử dụng'),

            'bank_name.max' => __('Tên khoản ngân hàng tối đa chỉ được 255 ký tự'),
            'bank_name.required' => __('Vui lòng nhập tên tài khoản ngân hàng'),

            'birthday.required' => __('Vui lòng chọn ngày sinh'),
            'gender.required' => __('Vui lòng chọn giới tính'),

            'phone.required' => __('Vui lòng nhập số điện thoại'),
            'phone.digits_between' => __('Số điện thoại phải từ 9 đến 11 ký tự'),
//            'phone2.min' => __('Số điện thoại tối thiểu phải nhập 9 ký tự'),
            'phone.numeric' => __('Số điện thoại chỉ được nhập số'),
            'phone.unique' => __('Số điện thoại này đã được sử dụng'),

            'phone2.required' => __('Vui lòng nhập số điện thoại'),
            'phone2.digits_between' => __('Số điện thoại phải từ 9 đến 11 ký tự'),
//            'phone2.min' => __('Số điện thoại tối thiểu phải nhập 9 ký tự'),
            'phone2.numeric' => __('Số điện thoại chỉ được nhập số'),
            'phone2.unique' => __('Số điện thoại này đã được sử dụng'),

            'ic_no.required' => __('Vui lòng nhập số chứng minh nhân dân'),
            'ic_no.digits_between' => __('CMND phải từ 9 đến 12 ký tự'),
//            'ic_no.min' => __('CMND tối thiểu phải nhập 9 ký tự'),
            'ic_no.numeric' => __('CMND chỉ được nhập số'),
            'ic_no.unique' => __('CMND này đã được sử dụng'),

            'ic_place.required' => __('Vui lòng nhập nơi cấp CMND'),
            'ic_place.max' => __('Nơi cấp tối đa chỉ được nhập tối đa 255 ký tự'),

//            'blah_ic_front_image.required' => __('Vui lòng Up ảnh CMND mặt trước'),
//            'blah_ic_back_image.required' => __('Vui lòng Up ảnh CMND mặt sau'),

            'contact_province_id.required' => __('Vui lòng chọn Tỉnh/Thành phố cho địa chỉ thường trú'),
            'contact_district_id.required' => __('Vui lòng chọn Quận/Huyện cho địa chỉ thường trú'),
            'contact_address.required' => __('Vui lòng nhập địa chỉ cho địa chỉ thường trú'),
            'contact_address.max' => __('Địa chỉ cho địa chỉ thường trú tối đa chỉ được nhập 255 ký tự'),

            'residence_province_id.required' => __('Vui lòng chọn Tỉnh/Thành phố cho địa chỉ liên hệ'),
            'residence_district_id.required' => __('Vui lòng chọn Quận/Huyện cho địa chỉ liên hệ'),
            'residence_address.required' => __('Vui lòng nhập địa chỉ cho địa chỉ liên hệ'),
            'residence_address.max' => __('Địa chỉ cho địa chỉ liên hệ tối đa chỉ được nhập 255 ký tự'),
        ];
    }


}
