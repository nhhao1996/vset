<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 4/28/2020
 * Time: 6:19 PM
 */

namespace Modules\Admin\Http\Requests\Product;


use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_code' => 'unique:products,product_code,'.',product_id,is_deleted,0',
        ];
    }

    public function messages()
    {
        return [
            'product_code.unique' => __('Mã gói đã được sử dụng .Yêu cầu nhập mã khác'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'product_code' => 'strip_tags|trim',
            'product_name_vi' => 'strip_tags|trim',
            'product_name_en' => 'strip_tags|trim',
            'product_short_name_vi' => 'strip_tags|trim',
            'product_short_name_en' => 'strip_tags|trim',
            'description_vi' => 'strip_tags|trim',
            'description_en' => 'strip_tags|trim',
            'interest_rate_standard' => 'strip_tags|trim',
            'withdraw_fee_rate_before' => 'strip_tags|trim',
            'withdraw_fee_rate_ok' => 'strip_tags|trim',
            'withdraw_fee_interest_rate' => 'strip_tags|trim',
            'withdraw_min_amount' => 'strip_tags|trim',
//            'min_allow_sale' => 'strip_tags|trim',
            'staff_commission_value' => 'strip_tags|trim',
            'refer_commission_value' => 'strip_tags|trim',
            'bonus_extend' => 'strip_tags|trim',
            'month_extend' => 'strip_tags|trim',
        ];
    }
}