<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 4/28/2020
 * Time: 6:19 PM
 */

namespace Modules\Admin\Http\Requests\Department;


use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departmentName' => 'required|max:250|unique:departments,department_name',
        ];
    }

    public function messages()
    {
        return [
            'departmentName.required' => __('Hãy nhập tên phòng ban'),
            'departmentName.max' => __('Tên phòng ban tối đa 250 kí tự'),
            'departmentName.unique' => __('Tên phòng ban trùng'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'departmentName' => 'strip_tags|trim',
        ];
    }
}