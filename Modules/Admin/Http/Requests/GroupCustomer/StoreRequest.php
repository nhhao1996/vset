<?php

namespace Modules\Admin\Http\Requests\GroupCustomer;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // TODO: Implement authorize() method.
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            $rules = [
            'group_name' => 'required|max:255|unique:group_customer,group_name,NULL,group_customer_id,is_deleted,0',
        ];
        return $rules;
    }
    /**
     * Customize message
     *
     * @return array
     */
    public function messages() {
        $messages = [
            'group_name.required' => __('admin::validation.group_customer.group_name_required'),
            'group_name.max' => __('admin::validation.group_customer.group_name_max'),
            'group_name.unique' => __('admin::validation.group_customer.group_name_unique'),
        ];
        return $messages;
    }
}