<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 10/24/2019
 * Time: 10:52 AM
 */

namespace Modules\Admin\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('banner_id');
        return [
            'name' => 'required|max:255|unique:banner,name,' .$id. ',banner_id',
            'img_desktop_vi' => 'required',
            'img_desktop_en' => 'required',
            'img_mobile_vi' => 'required',
            'img_mobile_en' => 'required',
        ];
    }

    /*
     * function custom messages
     */
    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên popup',
            'name.max' => 'Tên popup vượt quá 255 ký tự',
            'name.unique' => 'Tên popup đã được sử dụng',
            'img_desktop_vi.required' => 'Vui lòng thêm hình desktop (VI)',
            'img_desktop_en.required' => 'Vui lòng thêm hình desktop (EN)',
            'img_mobile_vi.required' => 'Vui lòng thêm hình mobile (VI)',
            'img_mobile_en.required' => 'Vui lòng thêm hình mobile (EN)',
        ];
    }
}