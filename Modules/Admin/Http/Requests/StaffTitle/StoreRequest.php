<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 4/28/2020
 * Time: 6:19 PM
 */

namespace Modules\Admin\Http\Requests\StaffTitle;


use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'staffTitleName' => 'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'staffTitleName.required' => __('Yêu cầu nhập tên chức vụ'),
            'staffTitleName.max' => __('Tên chức vụ vượt quá 100 ký tự'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'staffTitleName' => 'strip_tags|trim',
            'staff_title_description' => 'strip_tags|required|max:250',
        ];
    }
}