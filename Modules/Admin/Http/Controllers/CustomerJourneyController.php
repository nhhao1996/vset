<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\ExportFile;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\CommissionLog\CommissionLogRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerGroup\CustomerGroupRepositoryInterface;
use Modules\Admin\Repositories\CustomerJourney\CustomerJourneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\CustomerSource\CustomerSourceRepositoryInterface;
use Modules\Admin\Repositories\District\DistrictRepositoryInterface;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PointHistory\PointHistoryRepoInterface;
use Modules\Admin\Repositories\PointRewardRule\PointRewardRuleRepositoryInterface;
use Modules\Admin\Repositories\Province\ProvinceRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use App\Exports\CustomerExport;
use App\Jobs\CheckMailJob;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Loyalty\LoyaltyRepositoryInterface;
use Modules\Admin\Http\Requests\Customers\CustomerUpdateRequest;

class CustomerJourneyController extends Controller
{

    protected $customerJourney;
    public function __construct
    (
        CustomerJourneyRepositoryInterface $customerJourney
    ) {
        $this->customerJourney = $customerJourney;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAction()
    {
        $get = $this->customerJourney->list();
        return view('admin::customer-journey.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
        return [

        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
            'customers$customer_group_id', 'customers$gender', 'created_at', 'birthday', 'search','is_active']);
        $list = $this->customerJourney->list($filter);
        return view('admin::customer-journey.list', [
            'LIST' => $list,
            'page' => $filter['page']
        ]);
    }

    // FUNCTION SUBMIT SUBMIT ADD
    public function submitAddAction(Request $request)
    {
        if ($request->ajax()) {
            $name = $request->name;
            $description = $request->description;
            $nameTest = $this->customerJourney->testCustomerSourceName($name);

            if (count($nameTest) != 0) {
                return response()->json([
                    'success' => 0,
                    'message_name' => 'Tên hành trình khách hàng đã được sử dụng',
                ]);
            }

            $data = [
                'name' => strip_tags($name),
                'description' => strip_tags($description),
                'is_active' => $request->is_inactive,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];

            $this->customerJourney->add($data);
            return response()->json(['success' => 1]);
        }
    }

// FUNCTION RETURN VIEW EDIT
    public function editAction(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->customerJourneyId;
            $item = $this->customerJourney->getItem($id);
            $jsonString = [
                "customer_journey_id" => $item->customer_journey_id,
                "name" => $item->name,
                "description" => $item->description,
                "is_active" => $item->is_active
            ];
            return response()->json($jsonString);
        }
    }

// function submit update customer source
    public function submitEditAction(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $description = $request->description;
        $nameTest = $this->customerJourney->testCustomerSourceName($name,$id);

        if(count($nameTest) != 0) {
            return response()->json([
                'success' => 0,
                'message_name' => 'Tên hành trình khách hàng đã được sử dụng',
            ]);
        } else {
            $data = [
                'updated_by' => Auth::id(),
                'name' => $name,
                'description' => $description,
                'is_active' => $request->is_active,
                'updated_by' => Auth::id(),
                'updated_at' => Carbon::now(),
            ];
            $this->customerJourney->edit($data, $id);
            return response()->json(['success' => 1]);
        }


    }

// function change status
    public function changeStatusAction(Request $request)
    {
        $params = $request->all();
        $data['is_actived'] = ($params['action'] == 'unPublish') ? 1 : 0;
        $this->customerJourney->edit($data, $params['id']);
        return response()->json([
            'status' => 0,
            'messages' => 'Trạng thái đã được cập nhật '
        ]);
    }

// FUNCTION DELETE ITEM
    public function removeAction($id)
    {
        $this->customerJourney->remove($id);
        return response()->json([
            'error' => 0,
            'message' => 'Remove success'
        ]);
    }
}