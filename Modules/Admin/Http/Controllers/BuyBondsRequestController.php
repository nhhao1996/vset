<?php


namespace Modules\Admin\Http\Controllers;

//VSet
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Repositories\BuyBondsRequest\BuyBondsRequestRepositoryInterface;

class BuyBondsRequestController extends Controller
{
    protected $buyBondsRequest;

    public function __construct(BuyBondsRequestRepositoryInterface $buyBondsRequest)
    {
        $this->buyBondsRequest  = $buyBondsRequest;
    }

    public function indexAction() {
        $buyBondsRequest = $this->buyBondsRequest->list();
        return view('admin::buy-bonds-request.index', [
            'LIST' => $buyBondsRequest,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $buyBondsRequest = $this->buyBondsRequest->list($filters);
        return view('admin::buy-bonds-request.list', ['LIST' => $buyBondsRequest,'page' => $filters['page']]);
    }

    /**
     * Hiển thị thông tin chi tiết
     *
     * @param int $id
     * @return Response
     * @return Response
     */
    public function show($id)
    {
        $data = $this->buyBondsRequest->show($id);
        $moneyReceipt = $this->buyBondsRequest->getMoneyReceipt($id);
        $listStaff = $this->buyBondsRequest->getListStaff();
        $listService = $this->buyBondsRequest->getServiceBonus($id);
        $total = 0 ;
//        Chuyển khoản
        if ($data['payment_method_id'] == 1 || $data['payment_method_id'] == 4) {
            $total = null;
//            ví lãi
        } else if ($data['payment_method_id'] == 2){
//            Danh sách id hợp đồng
            $total = $this->buyBondsRequest->getTotalInterest($data['order_id']);
//            ví thưởng
        } else if ($data['payment_method_id'] == 3) {
//            $total = $this->buyBondsRequest->getTotalCommission($data['order_id'],$data['customer_id']);
            $total = $this->buyBondsRequest->getTotalCommissionWallet($data['order_id'],$data['customer_id']);
        }
//        Danh sách chuyển tiền
        return view('admin::buy-bonds-request.detail', [
            'detail' => $data,
            'moneyReceipt' => $moneyReceipt,
            'total' => $total,
            'listStaff' => $listStaff,
            'listService' => $listService
        ]);
    }

    public function addReceiptDetail(Request $request) {
        $param = $request->all();
        $addReceiptDetail = $this->buyBondsRequest->addReceiptDetail($param);
        return $addReceiptDetail;
    }

    public function createWallet(Request $request){
        $param = $request->all();
        $createWallet = $this->buyBondsRequest->createWallet($param);
        return $createWallet;
    }

    public function getListReceiptDetail(Request $request) {
        $param = $request->all();
        $listReceipDetail = $this->buyBondsRequest->getListReceiptDetail($param);
        return $listReceipDetail;
    }

    public function showPopupReceiptDetail(Request $request) {
        $param = $request->all();
        $showPopup = $this->buyBondsRequest->showPopupReceiptDetail($param);
        return $showPopup;
    }

    /**
     * createReceipt
     *
     * @param int $amount
     * @return Response
     * @return Response
     */
    public function createReceipt(Request $request)
    {
        $param = $request->all();
        $createReceipt = $this->buyBondsRequest->createReceipt($param);
        return $createReceipt;
    }

    /**
     * Upload image dropzone
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadDropzoneAction(Request $request)
    {
        return $this->buyBondsRequest->uploadDropzone($request->all());
    }

    public function printBill(Request $request){
        $printBill = $this->buyBondsRequest->printBill($request->all());
        return \response()->json($printBill);
    }

    public function createBill(){
        $view = view('admin::buy-bonds-request.popup.create-bill')->render();
        return \response()->json($view);
    }

//    Cập nhật bonus
    public function updateCommission(){
        return $this->buyBondsRequest->updateCommission();
    }

//    Không xác nhận yêu cầu
    public function cancelOrder(Request $request){
        $cancelOrder = $this->buyBondsRequest->cancelOrder($request->all());
        return \response()->json($cancelOrder);
    }


}