<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\ExportFile;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\CommissionLog\CommissionLogRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerGroup\CustomerGroupRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\CustomerSource\CustomerSourceRepositoryInterface;
use Modules\Admin\Repositories\District\DistrictRepositoryInterface;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PointHistory\PointHistoryRepoInterface;
use Modules\Admin\Repositories\PointRewardRule\PointRewardRuleRepositoryInterface;
use Modules\Admin\Repositories\Province\ProvinceRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use App\Exports\CustomerExport;
use App\Jobs\CheckMailJob;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Loyalty\LoyaltyRepositoryInterface;
use Modules\Admin\Http\Requests\Customers\CustomerUpdateRequest;

class VsetCustomerController extends Controller
{
    protected $customer;
    protected $province;
    protected $district;
    protected $staff;
    protected $order;
    protected $customer_appointment;
    protected $branch;
    protected $service_card_list;
    protected $order_detail;
    protected $service_card;
    protected $receipt;
    protected $memberLevel;
    protected $loyalty;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customers
     * @param CustomerGroupRepositoryInterface $customer_groups
     * @param CustomerSourceRepositoryInterface $customer_sources
     * @param CodeGeneratorRepositoryInterface $codes
     * @param ProvinceRepositoryInterface $provinces
     * @param DistrictRepositoryInterface $districts
     * @param StaffRepositoryInterface $staffs
     * @param OrderRepositoryInterface $orders
     * @param OrderDetailRepositoryInterface $order_details
     * @param CustomerAppointmentRepositoryInterface $customer_appointments
     * @param AppointmentServiceRepositoryInterface $appointment_services
     * @param CustomerServiceCardRepositoryInterface $customer_sv_cards
     * @param BranchRepositoryInterface $branches
     * @param CustomerBranchMoneyRepositoryInterface $customer_branch_moneys
     * @param CustomerAppointmentDetailRepositoryInterface $customer_appointment_details
     * @param ServiceCardListRepositoryInterface $service_card_lists
     * @param SmsLogRepositoryInterface $smsLog
     * @param ServiceCardRepositoryInterface $service_card
     * @param ReceiptRepositoryInterface $receipt
     * @param CustomerDebtRepositoryInterface $customer_debt
     * @param CommissionLogRepositoryInterface $commission_log
     * @param OrderCommissionRepositoryInterface $order_commission
     * @param MemberLevelRepositoryInterface $memberLevel
     * @param PointHistoryRepoInterface $pointHistory
     * @param LoyaltyRepositoryInterface $loyalty
     * @param PointRewardRuleRepositoryInterface $pointReward
     */
    public function __construct
    (
        CustomerRepositoryInterface $customers,
        ProvinceRepositoryInterface $provinces,
        DistrictRepositoryInterface $districts
    ) {
        $this->customer = $customers;
        $this->province = $provinces;
        $this->district = $districts;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAction(Request $request)
    {
        // \Request::route()->getName()
        $routeName = $request->route()->getName();
        $isTop100 = false;
        $filter=[];
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword','is_broker','is_test','utm_source',
            'customers$customer_group_id', 'customers$gender', 'created', 'birthday', 'search','type_customer','customer_source_id','is_active']);

        if($routeName=="admin.customer-top100"){
            $isTop100 = true;
            $filter['is_top'] = 'is_top';
            $filter['stock_order_by'] = $request->stock_order_by;
        }

        $get = $this->customer->list($filter);
//        $listCustomerSource = $this->customer->getListCustomerSource();
        $customerJourney = $this->customer->getCustomerJourney();
        if (count($customerJourney) != 0 ) {
            $customerJourney = collect($customerJourney)->keyBy('group_customer_id');
        }
        $listCustomerSource = $this->customer->getListReferSource();
//        foreach ($get as $key => $item) {
//            $get[$key]['type_customer'] = $this->customer->checkContract($item['customer_id']);
//        }
        return view('admin::customer.index', [
            'LIST' => $get,
            'FILTER' => $filter,
            'listCustomerSource' => $listCustomerSource,
            'isTop100'=>$isTop100,
            'customerJourney' => $customerJourney
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
        return [
            'customers$customer_group_id' => [
                'data' => []
            ],
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword','is_broker','is_test','utm_source',
            'customers$customer_group_id', 'customers$gender', 'created', 'birthday', 'search','type_customer','customer_source_id','is_active']);

        $list = $this->customer->list($filter);

        foreach ($list as $key => $item) {
            $list[$key]['type_customer'] = $this->customer->checkContract($item['customer_id']);
        }
        $customerJourney = $this->customer->getCustomerJourney();
        return view('admin::customer.list', [
            'LIST' => $list,
            'page' => $filter['page'],
            'FILTER' => $filter,
            'customerJourney' => $customerJourney
        ]);
    }

    public function detailAction($id)
    {

        $detail = $this->customer->vSetGetDetailCustomer($id);
        if ($detail == null) {
            return redirect()->route('admin.customer');
        }

        $customerSource = $this->customer->getCustomerSource();
        $customerJourney = $this->customer->getCustomerJourney();

        $stockCustomer = $this->customer->getStockByCustomer($id);
        return view('admin::customer.detail', [
            'item' => $detail['detail'],
            'totalInvested' => $detail['invested'],
            'interestWalletCustomer' => $detail['interestWallet'],
            'savingsWallet' => $detail['savingsWallet'],
            'bonusWallet' => $detail['bonusWallet'],
            'bondWallet' => $detail['bondWallet'],
            'customerSource' => $customerSource,
            'customerJourney' => $customerJourney,
            'stockCustomer' => $stockCustomer
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcelAction()
    {

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new CustomerExport(), 'customer.xlsx');
    }

    //// => *** V_SET *** <= ////
    // list lịch sử hoạt động
    //detail customer
    public function operationHistory(Request $request)
    {
        $param = $request->all();
        $filter['id'] = $request->customer_id;
        $filter['pagination'] = $param['pagination'];
        $filter['pagination']['perpage'] = 10;
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->operationHistory($filter);
        unset($filter['id']);
        return response()->json($list);
    }
        // list lịch sử giao dịch cổ phiếu
    //detail customer
    public function operationStockHistory(Request $request)
    {
        $param = $request->all();
        $filter['customer_id'] = $request->customer_id;
        $filter['is_filter'] = "0";
        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->operationStockHistory($filter);
        unset($filter['customer_id']);
        $data = [
            'meta'=>['page'=>1, 'pages'=>2,'perpage'=>5,'total'=>2],
            'data'=>$list

        ];
        return response()->json($list);
    }

    // danh sách hợp đồng theo customer
    public function listContractCustomer(Request $request)
    {
        $param = $request->all();
        $filter['id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
        $filter['pagination']['perpage'] = 10;
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listContractCustomer($filter);
        unset($filter['id']);

        return response()->json($list);
    }

    public function editInfoCustomerAction($id)
    {
        $detail  = $this->customer->vSetGetDetailCustomer($id);
        if ($detail == null) {
            return redirect()->route('admin.customer');
        }
        $bank = $this->customer->getListBank();
        $customerSource = $this->customer->getCustomerSource();
        $optionProvince = $this->province->getOptionProvince();
        $customerJourney = $this->customer->getCustomerJourney();
        $checkContract = $this->customer->checkContractById($id);
        $checkContractEdit = 0;
        if (count($checkContract) != 0){
            $checkContractEdit = 1;
        }
//        dd($checkContractEdit);
        return view('admin::customer.edit', [
            'item' => $detail['detail'],
            'optionProvince' => $optionProvince,
            'customerSource' => $customerSource,
            'bank' => $bank,
            'customerJourney' => $customerJourney,
            'checkContractEdit' => $checkContractEdit
        ]);
    }

    public function loadDistrictAction(Request $request)
    {
        $filters = request()->all();
        $district = $this->district->getOptionDistrict($filters);
        $data = [];
        foreach ($district as $key => $value) {
            $data[] = [
                'id' => $value['districtid'],
                'name' => $value['name'],
                'type' => $value['type']
            ];
        }
        return response()->json([
            'optionDistrict' => $data,
            'pagination' => $district->nextPageUrl() ? true : false
        ]);
    }

    public function submitEditAction(CustomerUpdateRequest $request)
    {
        $data = $request->all();
        $id = $data['customer_id_hidden'];
        return $this->customer->edit($data, $id);
    }

    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
         return $this->customer->changeStatus($change, $change['id']);
    }

    public function showResetPassword(Request $request)
    {
        $data = $request->all();
        $result = $this->customer->vSetGetDetailCustomer($data['customer_id']);
//        var_dump($result);die;
        return view('admin::customer.popup.popup-reset-password', [
            'item' => $result
        ]);
    }

    public function submitResetPassword(Request $request)
    {
        $data = $request->all();
        return  $this->customer->submitResetPassword($data, $data['customer_id']);
    }

    public function exportAction(Request $request){
        $filter = $request->all();

        $list = $this->customer->exportList($filter);
        return $list;
    }

    public function listIntroduct(Request $request)
    {
        $param = $request->all();
        $filter['introduct_refer_id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
        $filter['pagination']['perpage'] = 10;
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listIntroduct($filter);
        unset($filter['introduct_refer_id']);

        return response()->json($list);
    }

    public function listContractCommission(Request $request)
    {
        $param = $request->all();
        $filter['refer_id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
        $filter['pagination']['perpage'] = 10;
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listContractCommission($filter);
        unset($filter['refer_id']);

        return response()->json($list);
    }

//    Cập nhật trạng thái hoặc tài khoản test
    public function changeStatus(Request $request) {
        $param = $request->all();
        $changeStatus = $this->customer->changeStatusTest($param);
        return response()->json($changeStatus);
    }
}