<?php


namespace Modules\Admin\Http\Controllers;

//VSet
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Repositories\WithdrawRequest\WithdrawRequestRepositoryInterface;

class WithdrawRequestController extends Controller
{
    protected $withdrawRequest;
    protected $customerContract;

    public function __construct(WithdrawRequestRepositoryInterface $withdrawRequest, CustomerContactTable $customerContract)
    {
        $this->withdrawRequest  = $withdrawRequest;
        $this->customerContract = $customerContract;
    }

    public function indexAction() {
        $withdrawRequestAll = $this->withdrawRequest->listAll();
        $withdrawRequest = $this->withdrawRequest->list();
        $withdrawCommission = $this->withdrawRequest->listCommission();
//        $withdrawStock = $this->withdrawRequest->listStock();

        return view('admin::withdraw-request.index', [
            'LISTALL' => $withdrawRequestAll,
            'LIST' => $withdrawRequest,
            'LISTCOMMISSION' => $withdrawCommission,
//            'LISTSTOCK' => $withdrawStock,
            'FILTER' => $this->filters(),
        ]);
    }

    public function indexStockAction() {
        $withdrawStock = $this->withdrawRequest->listStock();

        return view('admin::withdraw-request.stock', [
            'LISTSTOCK' => $withdrawStock,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAllAction(Request $request)
    {
        $filters = $request->all();
        $withdrawRequest = $this->withdrawRequest->listAll($filters);
        return view('admin::withdraw-request.list-all', ['LISTALL' => $withdrawRequest,'page' => $filters['page']]);
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $withdrawRequest = $this->withdrawRequest->list($filters);
        return view('admin::withdraw-request.list', ['LIST' => $withdrawRequest,'page' => $filters['page']]);
    }

    public function listActionCommission(Request $request)
    {
        $filters = $request->all();
        $withdrawCommission = $this->withdrawRequest->listCommission($filters);
        return view('admin::withdraw-request.list-commission',['LISTCOMMISSION' => $withdrawCommission,'page' => $filters['page']]);
    }

    public function listActionStock(Request $request)
    {
        $filters = $request->all();
        $withdrawStock = $this->withdrawRequest->listStock($filters);
        return view('admin::withdraw-request.list-stock',['LISTSTOCK' => $withdrawStock,'page' => $filters['page']]);
    }


    /**
     * Hiển thị thông tin chi tiết
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data = $this->withdrawRequest->show($id);
        if ($data['withdraw_request_type'] == 'saving' && $data['term_time_type'] == 0){
            $data['withdraw_fee_rate_ok'] = 0;
            $data['withdraw_fee_rate_before'] = 0;
        }
//        lấy số tiền đã rút của hợp đồng
        $getListInterest = $this->withdrawRequest->getTotalRequest($data['customer_contract_id']);
//        Thời gian rút tiền tối thiểu chưa đến thời gian hiện tại
        $checkMintime = 0 ;
//        Thời gian hiện tại chưa đến thời gian rút tiền
        $checkRequest = 0;
        $request_datetime = Carbon::parse($data['withdraw_request_year'].'/'.$data['withdraw_request_month'].'/'.$data['withdraw_request_day'].' '.$data['withdraw_request_time']);

        if (Carbon::parse($data['customer_contract_start_date'])->addMonth($data['withdraw_min_time'] == null ? 0 : $data['withdraw_min_time']) <= $request_datetime) {
            $checkMintime = 1 ;
        }

        if (Carbon::parse($data['customer_contract_start_date'])->addMonth($data['investment_time'] == null ? 0 : $data['investment_time'])->timestamp <= $request_datetime->timestamp || $data['term_time_type'] == 0) {
//            đúng kỳ hạn
            $checkRequest = 1;
        }

        if (in_array($data['withdraw_request_status'],['done','cancel'])) {
            $data['contract_total_amount'] =  $data['available_balance_after'];
        }
        $getListInterest['total_withdraw_request_amount'] = 0;
        return view('admin::withdraw-request.detail', [
            'detail' => $data,
            'getListInterest' => $getListInterest,
            'checkMintime' => $checkMintime,
            'checkRequest' => $checkRequest,
        ]);
    }

    public function showGroup($id)
    {
        $data = $this->withdrawRequest->showGroup($id);
//        Tổng tiền hoa hồng còn lại
        $totalBonus = 0;
        if ($data['withdraw_request_group_type'] == 'bonus'){
//            $totalContract = $this->withdrawRequest->totalBonusCustomer($data['customer_id']);
            $totalContract = $this->withdrawRequest->totalBonusCustomerWallet($data['customer_id']);
            $totalWithdrawGroup = $this->withdrawRequest->totalWithdrawGroup($data['customer_id'],$data['withdraw_request_group_type']);
            if (in_array($data['withdraw_request_status'],['done','cancel'])) {
                $totalBonus = $data['available_balance_after'];
            } else {
                $totalBonus = $totalContract['total_commission'] - $totalWithdrawGroup;
            }
            return view('admin::withdraw-request.detail-group', [
                'detail' => $data,
                'totalBonus' => $totalBonus
            ]);

        } elseif($data['withdraw_request_group_type'] == 'interest') {
            $totalInterestMoney = 0;
//            Lấy chi tiết họp đồng
            $detailContract = $this->withdrawRequest->getContract($data['customer_contract_id']);
            if ($detailContract['term_time_type'] == 1) {
                $totalInterestMonth = $this->withdrawRequest->totalInterestMonth($data['customer_contract_id']);
                $totalInterestMoney = $totalInterestMonth['interest_banlance_amount'];
            } else {
                $totalInterestMonth = $this->withdrawRequest->getLastContractTime($data['customer_contract_id']);
                $totalInterestMoney = $totalInterestMonth['interest_total_amount'];
            }

//            $totalWithdrawGroup = $this->withdrawRequest->totalMoneyWithdrawGroup($data['customer_contract_id'],'interest');
            $totalWithdrawGroup = $this->withdrawRequest->totalMoneyWithdrawGroupFix($data['customer_contract_id'],'interest');
//            $totalBonus = $totalInterestMonth['interest_banlance_amount'] - $totalWithdrawGroup;
            if (in_array($data['withdraw_request_status'],['done','cancel'])) {
                $totalBonus = $data['available_balance_after'];

            } else {
                $totalBonus = $totalInterestMoney - $totalWithdrawGroup;
            }
            $withdraw_date_planing = $this->withdrawRequest->getWithdrawDatePlaning($data['customer_contract_id']);
            $withdraw_date = Carbon::parse($data['withdraw_request_year'].'-'.$data['withdraw_request_month'].'-'.$data['withdraw_request_day'].' '.$data['withdraw_request_time']);
            return view('admin::withdraw-request.detail-group', [
                'detail' => $data,
                'totalBonus' => $totalBonus,
                'withdraw_date_planing' => $withdraw_date_planing,
                'withdraw_date' => $withdraw_date
            ]);
        }

    }

    public function showStock($id){
        $data = $this->withdrawRequest->showGroup($id);
        $getWallet = $this->withdrawRequest->getWalletStock($data['customer_id']);
        if ($data['withdraw_request_group_type'] =='stock') {
            $totalBonus =  $getWallet == null ? 0 : $getWallet['wallet_stock_money'];
        } else if ($data['withdraw_request_group_type'] =='dividend') {
            $totalBonus = $getWallet == null ? 0 : $getWallet['wallet_dividend_money'];
        }
        $withdraw_date_planing = $withdraw_date = Carbon::now();
        return view('admin::withdraw-request.detail-stock', [
            'detail' => $data,
            'totalBonus' => $totalBonus,
            'withdraw_date_planing' => $withdraw_date_planing,
            'withdraw_date' => $withdraw_date
        ]);
    }

    public function createAction(Request $request)
    {
        $dataListCustomer = $this->withdrawRequest->getListCustomer();

        return view("admin::withdraw-request.create", [
            "list_customer" => $dataListCustomer,
        ]);
    }

    public function getListWithdrawRequest(Request $request) {
        $param = $request->all();
        $getList = $this->withdrawRequest->getListWithdrawRequest($param);
        return $getList;
    }

    public function getListCustomerContractInterestMonth(Request $request) {
        $param = $request->all();
        $getList = $this->withdrawRequest->getListCustomerContractInterestMonth($param);
        return $getList;
    }

    public function getListCustomerContractInterestDate(Request $request) {
        $param = $request->all();
        $getList = $this->withdrawRequest->getListCustomerContractInterestDate($param);
        return $getList;
    }

    public function changeStatus(Request $request) {
        $param = $request->all();
        $changeStatus = $this->withdrawRequest->changeStatus($param);
        return $changeStatus;
    }

    public function changeStatusGroup(Request $request) {
        $param = $request->all();
        $changeStatusGroup = $this->withdrawRequest->changeStatusGroup($param);
        return $changeStatusGroup;
    }

    public function changeStatusStock(Request $request) {
        $param = $request->all();
        $changeStatusStock = $this->withdrawRequest->changeStatusStock($param);
        return $changeStatusStock;
    }
}