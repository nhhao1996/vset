<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/19/2021
 * Time: 8:50 AM
 * @author nhandt
 */


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\StockRevenue\ReportStockRevenueRepoInterface;

class ReportStockRevenueController extends Controller
{
    private $reportStockRevenue;
    public function __construct(ReportStockRevenueRepoInterface $reportStockRevenue)
    {
        $this->reportStockRevenue = $reportStockRevenue;
    }

    public function indexAction(Request $request){
        return view("admin::stock-revenue.index", [
        ]);
    }
    public function filter(Request $request){
        $params = $request->all();
        $result = $this->reportStockRevenue->filter($params);
        return response()->json($result);
    }
}