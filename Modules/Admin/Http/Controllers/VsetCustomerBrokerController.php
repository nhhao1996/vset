<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\ExportFile;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\CommissionLog\CommissionLogRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerGroup\CustomerGroupRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\CustomerSource\CustomerSourceRepositoryInterface;
use Modules\Admin\Repositories\District\DistrictRepositoryInterface;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PointHistory\PointHistoryRepoInterface;
use Modules\Admin\Repositories\PointRewardRule\PointRewardRuleRepositoryInterface;
use Modules\Admin\Repositories\Province\ProvinceRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use App\Exports\CustomerExport;
use App\Jobs\CheckMailJob;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Loyalty\LoyaltyRepositoryInterface;
use Modules\Admin\Http\Requests\Customers\CustomerUpdateRequest;

class VsetCustomerBrokerController extends Controller
{
    protected $customer;
    protected $province;
    protected $district;
    protected $staff;
    protected $order;
    protected $customer_appointment;
    protected $branch;
    protected $service_card_list;
    protected $order_detail;
    protected $service_card;
    protected $receipt;
    protected $memberLevel;
    protected $loyalty;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customers
     * @param CustomerGroupRepositoryInterface $customer_groups
     * @param CustomerSourceRepositoryInterface $customer_sources
     * @param CodeGeneratorRepositoryInterface $codes
     * @param ProvinceRepositoryInterface $provinces
     * @param DistrictRepositoryInterface $districts
     * @param StaffRepositoryInterface $staffs
     * @param OrderRepositoryInterface $orders
     * @param OrderDetailRepositoryInterface $order_details
     * @param CustomerAppointmentRepositoryInterface $customer_appointments
     * @param AppointmentServiceRepositoryInterface $appointment_services
     * @param CustomerServiceCardRepositoryInterface $customer_sv_cards
     * @param BranchRepositoryInterface $branches
     * @param CustomerBranchMoneyRepositoryInterface $customer_branch_moneys
     * @param CustomerAppointmentDetailRepositoryInterface $customer_appointment_details
     * @param ServiceCardListRepositoryInterface $service_card_lists
     * @param SmsLogRepositoryInterface $smsLog
     * @param ServiceCardRepositoryInterface $service_card
     * @param ReceiptRepositoryInterface $receipt
     * @param CustomerDebtRepositoryInterface $customer_debt
     * @param CommissionLogRepositoryInterface $commission_log
     * @param OrderCommissionRepositoryInterface $order_commission
     * @param MemberLevelRepositoryInterface $memberLevel
     * @param PointHistoryRepoInterface $pointHistory
     * @param LoyaltyRepositoryInterface $loyalty
     * @param PointRewardRuleRepositoryInterface $pointReward
     */
    public function __construct
    (
        CustomerRepositoryInterface $customers,
        ProvinceRepositoryInterface $provinces,
        DistrictRepositoryInterface $districts
    ) {
        $this->customer = $customers;
        $this->province = $provinces;
        $this->district = $districts;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAction()
    {
        $filter['type_customer'] = 'broker';
        $get = $this->customer->list($filter);
        if (count($get) != 0){
            $filter['arr_refer_id'] = collect($get->toArray()['data'])->pluck('customer_refer_id');
        }
        $get = $this->customer->list($filter);
        foreach ($get as $item) {
            $getListRefer = $this->customer->getListReferCustomer($item['customer_id']);
            $item['total'] = 0;
            $item['refer_code'] = 0;
            $item['refer_link'] = 0;
            $item['total'] = count($getListRefer);
            if (count($getListRefer) != 0) {
                $getListRefer = collect($getListRefer)->groupBy('is_referal');
                $item['refer_code'] = isset($getListRefer[0]) ? count($getListRefer[0]) : 0;
                $item['refer_link'] = isset($getListRefer[1]) ? count($getListRefer[1]) : 0;
            }
        }
        return view('admin::customer-broker.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
        return [
            'customers$customer_group_id' => [
                'data' => []
            ],
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
            'customers$customer_group_id', 'customers$gender', 'created_at', 'birthday', 'search']);
        $filter['type_customer'] = 'broker';
        $list = $this->customer->list($filter);
        if (count($list) != 0){
            $filter['arr_refer_id'] = collect($list->toArray()['data'])->pluck('customer_refer_id');
        }
        unset($filter['type_customer']);
        $list = $this->customer->list($filter);
        return view('admin::customer-broker.list', [
            'LIST' => $list,
            'page' => $filter['page']
        ]);
    }

    public function detailAction($id)
    {
        $detail  = $this->customer->vSetGetDetailCustomer($id);
        return view('admin::customer-broker.detail', [
            'item' => $detail['detail'],
            'totalInvested' => $detail['invested'],
            'interestWalletCustomer' => $detail['interestWallet'],
            'savingsWallet' => $detail['savingsWallet'],
            'bonusWallet' => $detail['bonusWallet'],
            'bondWallet' => $detail['bondWallet']
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcelAction()
    {

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new CustomerExport(), 'customer.xlsx');
    }

    //// => *** V_SET *** <= ////
    // list lịch sử hoạt động
    //detail customer
    public function operationHistory(Request $request)
    {
        $param = $request->all();
        $filter['id'] = $request->customer_id;
        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->operationHistory($filter);
        unset($filter['id']);
        return response()->json($list);
    }

    // danh sách hợp đồng theo customer
    public function listContractCustomer(Request $request)
    {
        $param = $request->all();
        $filter['id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listContractCustomer($filter);
        unset($filter['id']);

        return response()->json($list);
    }

    public function listContractCommission(Request $request)
    {
        $param = $request->all();
        $filter['refer_id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listContractCommission($filter);
        unset($filter['refer_id']);

        return response()->json($list);
    }

    public function listIntroduct(Request $request)
    {
        $param = $request->all();
        $filter['introduct_refer_id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listIntroduct($filter);
        unset($filter['introduct_refer_id']);

        return response()->json($list);
    }

    public function editInfoCustomerAction($id)
    {
        $detail  = $this->customer->vSetGetDetailCustomer($id);
      // dd($detail);
        $optionProvince = $this->province->getOptionProvince();
        return view('admin::customer-broker.edit', [
            'item' => $detail['detail'],
            'optionProvince' => $optionProvince,
        ]);
    }

    public function loadDistrictAction(Request $request)
    {
        $filters = request()->all();
        $district = $this->district->getOptionDistrict($filters);
        $data = [];
        foreach ($district as $key => $value) {
            $data[] = [
                'id' => $value['districtid'],
                'name' => $value['name'],
                'type' => $value['type']
            ];
        }
        return response()->json([
            'optionDistrict' => $data,
            'pagination' => $district->nextPageUrl() ? true : false
        ]);
    }

    public function submitEditAction(CustomerUpdateRequest $request)
    {
        $data = $request->all();
        $id = $data['customer_id_hidden'];
        return $this->customer->edit($data, $id);
    }

    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
         return $this->customer->changeStatus($change, $change['id']);
    }

    public function showResetPassword(Request $request)
    {
        $data = $request->all();
        $result = $this->customer->vSetGetDetailCustomer($data['customer_id']);
//        var_dump($result);die;
        return view('admin::customer-broker.popup.popup-reset-password', [
            'item' => $result
        ]);
    }

    public function submitResetPassword(Request $request)
    {
        $data = $request->all();
        return  $this->customer->submitResetPassword($data, $data['customer_id']);
    }

    public function configAction(){
        session()->forget('service_list');
        session()->put('service_list',[]);
//        $notiTypeList = $this->notification->getNotificationTypeList();
        $listCategoryService = $this->customer->getListCategoryService();
        return view('admin::customer-broker.config', [
//            'notiTypeList' => $notiTypeList,
            'listCategoryService' => $listCategoryService
        ]);
    }

    public function getListService(Request $request) {
        $param = $request->all();
        $list = $this->customer->getListService($param);
        return \response()->json($list);
    }

    public function addListService(Request $request)
    {
        $param = $request->all();
        $addList = $this->customer->addListService($param);
        return \response()->json($addList);
    }

    public function removeService(Request $request)
    {
        $param = $request->all();
        $removeService = $this->customer->removeService($param);
        return \response()->json($removeService);
    }

    public function updateConfig(Request $request){
        $param = $request->all();
        $update = $this->customer->updateConfig($param);
        return \response()->json($update);
    }
}