<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\ExportFile;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\ConfigTable;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\CommissionLog\CommissionLogRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerContractInterest\CustomerContractInterestRepository;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerGroup\CustomerGroupRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\CustomerSource\CustomerSourceRepositoryInterface;
use Modules\Admin\Repositories\District\DistrictRepositoryInterface;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PointHistory\PointHistoryRepoInterface;
use Modules\Admin\Repositories\PointRewardRule\PointRewardRuleRepositoryInterface;
use Modules\Admin\Repositories\Province\ProvinceRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use App\Exports\CustomerExport;
use App\Jobs\CheckMailJob;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Loyalty\LoyaltyRepositoryInterface;
use Modules\Admin\Http\Requests\Customers\CustomerUpdateRequest;
use Modules\Admin\Http\Requests\Customers\CustomerApproveChangeRequest;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use App;
use PDF;

class VsetCustomerChangeRequestController extends Controller
{
    protected $customer;
    protected $province;
    protected $district;
    protected $staff;
    protected $order;
    protected $customer_appointment;
    protected $branch;
    protected $service_card_list;
    protected $order_detail;
    protected $service_card;
    protected $receipt;
    protected $memberLevel;
    protected $loyalty;
    protected $api;
    protected $customerContractInterest;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customers
     * @param CustomerGroupRepositoryInterface $customer_groups
     * @param CustomerSourceRepositoryInterface $customer_sources
     * @param CodeGeneratorRepositoryInterface $codes
     * @param ProvinceRepositoryInterface $provinces
     * @param DistrictRepositoryInterface $districts
     * @param StaffRepositoryInterface $staffs
     * @param OrderRepositoryInterface $orders
     * @param OrderDetailRepositoryInterface $order_details
     * @param CustomerAppointmentRepositoryInterface $customer_appointments
     * @param AppointmentServiceRepositoryInterface $appointment_services
     * @param CustomerServiceCardRepositoryInterface $customer_sv_cards
     * @param BranchRepositoryInterface $branches
     * @param CustomerBranchMoneyRepositoryInterface $customer_branch_moneys
     * @param CustomerAppointmentDetailRepositoryInterface $customer_appointment_details
     * @param ServiceCardListRepositoryInterface $service_card_lists
     * @param SmsLogRepositoryInterface $smsLog
     * @param ServiceCardRepositoryInterface $service_card
     * @param ReceiptRepositoryInterface $receipt
     * @param CustomerDebtRepositoryInterface $customer_debt
     * @param CommissionLogRepositoryInterface $commission_log
     * @param OrderCommissionRepositoryInterface $order_commission
     * @param MemberLevelRepositoryInterface $memberLevel
     * @param PointHistoryRepoInterface $pointHistory
     * @param LoyaltyRepositoryInterface $loyalty
     * @param PointRewardRuleRepositoryInterface $pointReward
     */
    public function __construct
    (
        CustomerRepositoryInterface $customers,
        ProvinceRepositoryInterface $provinces,
        DistrictRepositoryInterface $districts,
        JobsEmailLogApi $api,
        CustomerContractInterestRepository $customerContractInterest
    ) {
        $this->customer = $customers;
        $this->province = $provinces;
        $this->district = $districts;
        $this->api = $api;
        $this->customerContractInterest = $customerContractInterest;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword','is_broker','is_test','status',
            'customers$customer_group_id', 'customers$gender', 'created', 'birthday', 'search','type_customer','customer_source_id']);
        $get = $this->customer->listChangeRequest($filter);
        $listCustomerSource = $this->customer->getListCustomerSource();
        return view('admin::customer-change-request.index', [
            'LIST' => $get,
            'FILTER' => $filter,
            'listCustomerSource' => $listCustomerSource
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
        return [
            'customers$customer_group_id' => [
                'data' => []
            ],
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword','is_broker','is_test','status',
            'customers$customer_group_id', 'customers$gender', 'created', 'birthday', 'search','type_customer','customer_source_id']);

        $list = $this->customer->listChangeRequest($filter);

//        foreach ($list as $key => $item) {
//            $list[$key]['type_customer'] = $this->customer->checkContract($item['customer_id']);
//        }
        return view('admin::customer-change-request.list', [
            'LIST' => $list,
            'page' => $filter['page'],
            'FILTER' => $filter,
        ]);
    }
    public function exportPdf($id){
    }
    public function saveAppendixContractMap(Request $request){      
        $param = $request->all();
        if (!isset($param['arrFile']) || count($param['arrFile']) == 0) {
            return ['error' => 1, 'message' =>'Yêu cầu thêm file phụ lục'];
        }
        $data = [];
        foreach ($param['arrFile'] as $item) {
            $data[] = [
                'appendix_contract_id'=>$param['appendix_contract_id'],
                "file"=>$item,
                "created_at"=>Carbon::now()->format("Y-m-d"),
                "created_by"=>Auth::id()
            ];
        }

        $this->customer->addAppendixContractMap($data);
        return ['error' => 0, 'message' =>'Thêm phụ lục thành công'];

    }
    public function uploadAppendixContract(Request $request){
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }

    /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . "_file." . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);
        return url('/').'/' . $this->transferTempfileToAdminfile($file_name, str_replace('', '', $file_name));
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = CUSTOMER_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(CUSTOMER_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }


    public function approveChangeRequest(Request  $request){
        try{

        // dd('approve Change Request', $request->all());
        // todo:Approve Change Request-----------
        $item = $request->all();
        $id = $request->customer_id;
        $phone1 = $request->phone;
        $phone2 = $request->phone2;
        $data = $request->only([
            'customer_change_id',
            'full_name', 
            'birthday',
            'phone',
            'gender',
            'phone',
//            'phone2',
//            'email',
//            'password',
            'ic_no',
            'ic_date',
            'ic_place',
            'ic_front_image',
            'ic_back_image',
            'contact_province_id',
            'contact_district_id',
            'contact_address',
            'residence_province_id',
            'residence_district_id',
            'residence_address',
            'bank_id',
            'bank_account_no',
            'bank_account_name',
            'bank_account_branch',
            'customer_avatar',

            ]);

        // todo: Lấy customer_change_id-----------
        $customerChangeId = $data['customer_change_id'];
        unset($data['customer_change_id']);

        $arr = [
            'full_name',
            'birthday',
            'phone',
//            'phone2',
//            'email',
            'gender',
//            'password',
            'ic_no',
            'ic_date',
            'ic_place',
            'ic_front_image',
            'ic_back_image',
            'contact_province_id',
            'contact_district_id',
            'contact_address',
            'residence_province_id',
            'residence_district_id',
            'residence_address',
            'bank_id',
            'bank_account_no',
            'bank_account_name',
            'bank_account_branch',
            'customer_avatar'
        ];

        foreach ($arr as $item){
            if ($data[$item] == null){
                unset($data[$item]);
            }
        }


//
        //Lưu thông tin vào Database------------------
        if (isset($data['password'])){
            $data['password'] = $data['password'];
        }
        $checkUpdate = 0;
        if (isset($data['phone'])){
            $checkUpdate = 1;
        }

        $config = new ConfigTable();

        $bank = $config->getInfoByKey('bank_vset');
        $bank = json_decode($bank['value'], true);

        $dataStatus = ['status' => 'success'];
        $filters = ['customer_id' => $id];
        $this->customer->updateChangeRequest($dataStatus, $filters);
        $this->customer->approveCustomerChangeRequest($data,$id);
        // todo: Export Pdf----------------
        $time = Carbon::now()->timestamp;
        $path = public_path("uploads");
        $data  = $this->customer->vSetGetDetailCustomerChangeAfter($customerChangeId)["detail"];
        //todo: Get List Contract of Customer-----------------------
        $contracts = $this->customer->getListCustomerContractChang($id);
        $i = 0;
        $dataAppendixContract = [];
        foreach($contracts as $contract){
            if($contract["customer_contract_end_date"] == null || Carbon::now() < Carbon::parse($contract["customer_contract_end_date"])){
                $data['contract'] = $contract->toArray();

                $i++;
                $pdf = PDF::loadView('admin::customer-change-request.contract-appendix', ['data' => $data,'bank' => $bank]);
                $path = "uploads/Phu_luc_hop_dong_".$i."_$time.pdf";
                $pdf->save($path);

                //todo: Insert Appendix Contract--------------
                $dataAppendixContract[] = [
                    'customer_contract_id' => $contract->customer_contract_id,
                    'customers_change_id'=>$customerChangeId,
                    'file'=>url('/').'/'.$path,
                    'created_by'=> Auth::id(),
                    'created_at'=>Carbon::now()->format("Y-m-d"),
                    'updated_at'=>Carbon::now()->format("Y-m-d"),
                    'updated_by'=> Auth::id()
                ];
            }

        }

        if (count($dataAppendixContract) != 0){
            $this->customer->addAppendixContract($dataAppendixContract);
        }

        $this->customer->updateCustomerLogout($id);
        $oUser = $this->customer->getItem($id);

        if ($oUser != null && $oUser['phone'] != null){
            if ($checkUpdate == 1) {
                $sendSMS = [
                    'phone' => $oUser['phone'],
                    'message' => 'Vsetgroup xin thong bao, thong tin thay doi tai khoan cua quy khach da duoc thay doi thanh cong. Hotline tu van 0907239999',
                    'customer_name' => $oUser['full_name']
                ];
            } else {
                $sendSMS = [
                    'phone' => $oUser['phone'],
                    'message' => 'Vsetgroup xin thong bao, thong tin thay doi tai khoan cua quy khach da duoc thay doi thanh cong. Hotline tu van 0907239999',
                    'customer_name' => $oUser['full_name']
                ];
            }
            $statusSend = $this->customerContractInterest->sendSMS($sendSMS);
            if ($statusSend == null || $statusSend->error == 1) {
                $sendSMS = [
                    'phone' => $oUser['phone'],
                    'message' => 'Vsetgroup xin thong bao, thong tin thay doi tai khoan cua khach da duoc thay doi thanh cong. Hotline tu van 0907239999',
                    'customer_name' => $oUser['full_name']
                ];
                $statusSend = $this->customerContractInterest->sendSMS($sendSMS);
            }
        }
        // !--------Export Pdf-------------
        return ['error' => 0, 'message' =>'Thành công'];

        // !-------Approve Change Request-------

        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }
    

    public function detailAction($id)
    {        
        $detail  = $this->customer->vSetGetDetailCustomerChange($id);
        $detailAfter  = $this->customer->vSetGetDetailCustomerChangeAfter($id);
        $customerSource = $this->customer->getCustomerSource();
        $listContract = $this->customer->getListContractChange($id);
        $listAppendix = $this->customer->getListAppendix($id);
        return view('admin::customer-change-request.detail', [
            'item' => $detail['detail'],
            'itemAfter'=>$detailAfter['detail'],
            'totalInvested' => $detail['invested'],
            'interestWalletCustomer' => $detail['interestWallet'],
            'savingsWallet' => $detail['savingsWallet'],
            'bonusWallet' => $detail['bonusWallet'],
            'bondWallet' => $detail['bondWallet'],
            'customerSource' => $customerSource,
            'listContract' => $listContract,
            'listAppendix' => $listAppendix
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcelAction()
    {

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new CustomerExport(), 'customer.xlsx');
    }

    //// => *** V_SET *** <= ////
    // list lịch sử hoạt động
    //detail customer
    public function operationHistory(Request $request)
    {
        $param = $request->all();
        $filter['id'] = $request->customer_id;
        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->operationHistory($filter);
        unset($filter['id']);
        return response()->json($list);
    }

    // danh sách hợp đồng theo customer
    public function listContractCustomer(Request $request)
    {
        $param = $request->all();
        $filter['id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listContractCustomer($filter);
        unset($filter['id']);

        return response()->json($list);
    }

    public function editInfoCustomerAction($id)
    {
        $detail  = $this->customer->vSetGetDetailCustomer($id);
        $bank = $this->customer->getListBank();
        $customerSource = $this->customer->getCustomerSource();
        $optionProvince = $this->province->getOptionProvince();
        return view('admin::customer.edit', [
            'item' => $detail['detail'],
            'optionProvince' => $optionProvince,
            'customerSource' => $customerSource,
            'bank' => $bank
        ]);
    }

    public function loadDistrictAction(Request $request)
    {
        $filters = request()->all();
        $district = $this->district->getOptionDistrict($filters);
        $data = [];
        foreach ($district as $key => $value) {
            $data[] = [
                'id' => $value['districtid'],
                'name' => $value['name'],
                'type' => $value['type']
            ];
        }
        return response()->json([
            'optionDistrict' => $data,
            'pagination' => $district->nextPageUrl() ? true : false
        ]);
    }

    public function submitEditAction(CustomerUpdateRequest $request)
    {
        $data = $request->all();
        $id = $data['customer_id_hidden'];
        return $this->customer->edit($data, $id);
    }

    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
         return $this->customer->changeStatus($change, $change['id']);
    }

    public function showResetPassword(Request $request)
    {
        $data = $request->all();
        $result = $this->customer->vSetGetDetailCustomer($data['customer_id']);
//        var_dump($result);die;
        return view('admin::customer.popup.popup-reset-password', [
            'item' => $result
        ]);
    }

    public function submitResetPassword(Request $request)
    {
        $data = $request->all();
        return  $this->customer->submitResetPassword($data, $data['customer_id']);
    }

    public function exportAction(Request $request){
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword','is_broker','is_test',
            'customers$customer_group_id', 'customers$gender', 'created', 'birthday', 'search','type_customer','customer_source_id']);

        $list = $this->customer->exportList($filter);
        return $list;
    }

    public function listIntroduct(Request $request)
    {
        $param = $request->all();
        $filter['introduct_refer_id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listIntroduct($filter);
        unset($filter['introduct_refer_id']);

        return response()->json($list);
    }

    public function listContractCommission(Request $request)
    {
        $param = $request->all();
        $filter['refer_id'] = $request->customer_id;


        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->customer->listContractCommission($filter);
        unset($filter['refer_id']);

        return response()->json($list);
    }

    public function cancelRequest(Request $request){
        $param = $request->all();
        $cancel = $this->customer->cancelChangeRequest($param);
        return $cancel;
    }
}