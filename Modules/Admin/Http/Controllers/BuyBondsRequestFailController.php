<?php


namespace Modules\Admin\Http\Controllers;

//VSet
use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\BuyBondsRequestFail\BuyBondsRequestFailRepositoryInterface;

class BuyBondsRequestFailController extends Controller
{
    protected $buyBondsRequestFail;

    public function __construct(BuyBondsRequestFailRepositoryInterface $buyBondsRequestFail)
    {
        $this->buyBondsRequestFail  = $buyBondsRequestFail;
    }

    public function indexAction(Request $request) {
        $request->session()->forget('param_order_fail');
        $request->session()->put('param_order_fail',[]);
        $buyBondsRequest = $this->buyBondsRequestFail->list();
        return view('admin::buy-bonds-request-fail.index', [
            'LIST' => $buyBondsRequest,
            'FILTER' => $request->all(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $request->session()->put('param_order_fail',$filters);
        $buyBondsRequest = $this->buyBondsRequestFail->list($filters);
        return view('admin::buy-bonds-request-fail.list', [
            'LIST' => $buyBondsRequest,
            'page' => $filters['page']
        ]);
    }

    /**
     * Hiển thị thông tin chi tiết
     *
     * @param int $id
     * @return Response
     * @return Response
     */
    public function show($id)
    {
        $data = $this->buyBondsRequestFail->show($id);
        $total = 0 ;
//        Chuyển khoản
        if ($data['payment_method_id'] == 1 || $data['payment_method_id'] == 4) {
            $total = null;
//            ví lãi
        } else if ($data['payment_method_id'] == 2){
//            Danh sách id hợp đồng
            $total = $this->buyBondsRequestFail->getTotalInterest($data['order_id']);
//            ví thưởng
        } else if ($data['payment_method_id'] == 3) {
            $total = $this->buyBondsRequestFail->getTotalCommission($data['order_id'],$data['customer_id']);
        }

//        Danh sách chuyển tiền
        return view('admin::buy-bonds-request-fail.detail', [
            'detail' => $data,
            'total' => $total
        ]);
    }

    public function export(Request $request) {
        $param = $request->session()->get('param_order_fail');
        $buyBondsRequest = $this->buyBondsRequestFail->listExport($param);
        $data = [];
        $heading = [
            __('STT'),
            __('Mã đơn hàng'),
            __('Tên nhà đầu tư'),
            __('Số điện thoại nhà đầu tư'),
            __('Số lượng'),
            __('Tổng tiền'),
            __('Loại gói'),
            __('Tên gói'),
            __('Mã gói'),
            __('Loại thanh toán'),
            __('Ngày tạo'),
        ];
        foreach ($buyBondsRequest as $key => $item) {
            $data[] = [
                $key+1,
                $item['order_code'],
                $item['customer_name'],
                $item['customer_phone'],
                $item['quantity'],
                number_format($item['total'],2),
                $item['type_bonds'],
                $item['product_name_vi'],
                $item['product_code'],
                $item['payment_method_name_vi'],
                $item['created_at'] == null ? 'N/A' : Carbon::parse($item['created_at'])->format('d-m-Y'),
            ];
        }
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new ExportFile($heading, $data), 'Yêu cầu mua lỗi.xlsx');

    }

}