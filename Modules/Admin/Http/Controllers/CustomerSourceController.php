<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\ExportFile;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\CommissionLog\CommissionLogRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerGroup\CustomerGroupRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\CustomerSource\CustomerSourceRepositoryInterface;
use Modules\Admin\Repositories\District\DistrictRepositoryInterface;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PointHistory\PointHistoryRepoInterface;
use Modules\Admin\Repositories\PointRewardRule\PointRewardRuleRepositoryInterface;
use Modules\Admin\Repositories\Province\ProvinceRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use App\Exports\CustomerExport;
use App\Jobs\CheckMailJob;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Loyalty\LoyaltyRepositoryInterface;
use Modules\Admin\Http\Requests\Customers\CustomerUpdateRequest;

class CustomerSourceController extends Controller
{

    protected $customerSource;
    public function __construct
    (
        CustomerSourceRepositoryInterface $customerSource
    ) {
        $this->customerSource = $customerSource;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAction()
    {
        $get = $this->customerSource->list();
        return view('admin::customer-source.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
        return [

        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
            'customers$customer_group_id', 'customers$gender', 'created_at', 'birthday', 'search','is_actived']);
        $list = $this->customerSource->list($filter);
        return view('admin::customer-source.list', [
            'LIST' => $list,
            'page' => $filter['page']
        ]);
    }

    // FUNCTION SUBMIT SUBMIT ADD
    public function submitAddAction(Request $request)
    {
        if ($request->ajax()) {
            $customerSourceName = $request->customer_source_name;
            $customerSourceDescription = $request->customer_source_description;
            $testCustomerSourceName = $this->customerSource->testCustomerSourceName($customerSourceName);
            if ($this->customerSource->testIsDeleted($customerSourceName) != null) {
                $this->customerSource->editByName($customerSourceName);
                return response()->json(['success' => 1]);
            } else {
                if ($testCustomerSourceName != null) {
                    return response()->json([
                        'success' => 0,
                        'message_name' => 'Tên nguồn khách hàng đã được sử dụng',
                    ]);
                }

                $data = [
                    'customer_source_name' => $customerSourceName,
                    'customer_source_description' => $customerSourceDescription,
//                    'customer_source_type' => $request->customer_source_type,
                    'is_actived' => $request->is_inactive,
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id(),
                    'slug'=>str_slug($customerSourceName)
                ];
                $this->customerSource->add($data);
                return response()->json(['success' => 1]);
            }
        }
    }

// FUNCTION RETURN VIEW EDIT
    public function editAction(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->customerSourceId;
            $item = $this->customerSource->getItem($id);
            $jsonString = [
                "customer_source_id" => $item->customer_source_id,
                "customer_source_name" => $item->customer_source_name,
                "customer_source_description" => $item->customer_source_description,
//                "customer_source_type" => $item->customer_source_type,
                "is_actived" => $item->is_actived,
                'slug'=>str_slug($item->customer_source_name),
            ];
            return response()->json($jsonString);
        }
    }

// function submit update customer source
    public function submitEditAction(Request $request)
    {
        $id = $request->id;
        $customerSourceName = $request->customer_source_name;
        $customerSourceDescription = $request->customer_source_description;
        $testCustomerSourceName = $this->customerSource->testCustomerSourceNameEdit($id, $customerSourceName);
        $testIsDeleted = $this->customerSource->testIsDeleted($customerSourceName);
        if ($request->parameter == 0) {
            if ($testIsDeleted != null) {
                //Tồn tại nguồn khách hàng trong db. is_deleted = 1.
                return response()->json(['status' => 2]);
            } else {
                if ($testCustomerSourceName != null) {
                    return response()->json([
                        'success' => 0,
                        'message_name' => 'Tên nguồn khách hàng đã được sử dụng',
                    ]);
                }

                $data = [
                    'updated_by' => Auth::id(),
                    'customer_source_name' => $customerSourceName,
                    'customer_source_description' => $customerSourceDescription,
//                        'customer_source_type' => $request->customer_source_type,
                    'is_actived' => $request->is_actived,
                    'slug'=>str_slug($customerSourceName),
                    'updated_by' => Auth::id(),
                    'updated_at' => Carbon::now(),
                ];
                $this->customerSource->edit($data, $id);
                return response()->json(['success' => 1]);
            }
        } else {
            //Kích hoạt lại nguồn khách hàng.
            $this->customerSource->edit(['is_deleted' => 0], $testIsDeleted->customer_source_id);
            return response()->json(['success' => 3]);
        }

    }

// function change status
    public function changeStatusAction(Request $request)
    {
        $params = $request->all();
        $data['is_actived'] = ($params['action'] == 'unPublish') ? 1 : 0;
        $this->customerSource->edit($data, $params['id']);
        return response()->json([
            'status' => 0,
            'messages' => 'Trạng thái đã được cập nhật '
        ]);
    }

// FUNCTION DELETE ITEM
    public function removeAction($id)
    {
        $this->customerSource->remove($id);
        return response()->json([
            'error' => 0,
            'message' => 'Remove success'
        ]);
    }
}