<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\SendEmail\SendEmailRepoInterface;

class SendEmailController extends Controller
{
    protected $request;
    protected $sendEmail;

    public function __construct(SendEmailRepoInterface $sendEmail, Request $request)
    {
        $this->sendEmail = $sendEmail;
        $this->request = $request;
    }

    public function index()
    {
        $getEmail = $this->sendEmail->getEmailMain();
        $emailCC = $this->sendEmail->getEmailCC();
        $sumListEmail = count($emailCC);
        $config = $this->sendEmail->getConfig('email_staff');
        $phoneStaff = $this->sendEmail->getConfig('phone_staff');
        return view('admin::send-email.index', [
            'emailMain' => $getEmail,
            'emailCC' => $emailCC,
            'sumListEmail' => $sumListEmail,
            'config' => $config,
            'phoneStaff' => $phoneStaff
        ]);
    }

    public function update()
    {
        $param = $this->request->all();
        $update = $this->sendEmail->updateEmail($param);
        return response()->json($update);
    }
}