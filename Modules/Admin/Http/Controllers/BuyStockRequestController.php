<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/8/2021
 * Time: 4:24 PM
 * @author nhandt
 */


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Models\StockCustomerTable;
use Modules\Admin\Repositories\Stock\StockRepoInterface;

class BuyStockRequestController extends Controller
{
    private $stockOrders;
    public function __construct(StockRepoInterface $stockOrders)
    {
        $this->stockOrders = $stockOrders;
    }
    public function indexAction(Request $request) {
        $filters = $request->all();
        $data = $this->stockOrders->getList($filters);
        return view('admin::buy-stock-request.index', [
            'LIST' => $data,
            'FILTER' => $request->all(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $data = $this->stockOrders->getList($filters);
        return view('admin::buy-stock-request.list', [
            'LIST' => $data,
            'page' => $filters['page']
        ]);
    }
    public function detailAction(Request $request)
    {
        $data = $this->stockOrders->show($request->id);
        $stockCustomerTable = new StockCustomerTable();
        $stockWallet = null;
        if($data['payment_method_id'] == 5){
            $stockWallet = $stockCustomerTable->getStockWalletMoney($data['customer_id'])['wallet_stock_money'];
        }
        elseif($data['payment_method_id'] == 6){
            $stockWallet = $stockCustomerTable->getDividendWalletMoney($data['customer_id'])['wallet_dividend_money'];
        }
        $seller = null;
        if($data['source'] == 'market'){
            $seller = $this->stockOrders->getDetailSellerMarket($data['obj_id']);
        }
        return view('admin::buy-stock-request.detail', [
            'detail' => $data,
            'seller' => $seller,
            'total' => $stockWallet
        ]);
    }

    /**
     * View thêm phiếu thu
     *
     * @param Request $request
     * @return mixed
     */
    public function showPopupReceiptDetail(Request $request) {
        $param = $request->all();
        $showPopup = $this->stockOrders->showPopupReceiptDetail($param);
        return $showPopup;
    }

    /**
     * Thêm phiếu thu
     *
     * @param Request $request
     * @return mixed
     */
    public function addReceiptDetail(Request $request) {
        $param = $request->all();
        $detail = $request->all();
        $data = $this->stockOrders->show($request->stock_order_id);
        // cổ phiếu còn lại trong đợt phát hành
        $stockPublishRemaining = $this->stockOrders->getStockPublishRemaining();
        // cổ phiếu còn lại ở chợ
        $stockMarketRemaining = $this->stockOrders->getStockMarketRemaining($data['obj_id']);
        $stockCustomerTable = new StockCustomerTable();

        $seller = null;
        $currStock = 0;
        if($data['source'] == 'market'){
            $seller = $this->stockOrders->getDetailSellerMarket($data['obj_id']);
            $currStock = $stockCustomerTable->getStock($seller['seller_id'])['stock'];
        }
        $stockWallet = null;
        if($data['payment_method_id'] == 5){
            $stockWallet = $stockCustomerTable->getStockWalletMoney($data['customer_id'])['wallet_stock_money'];
        }
        elseif($data['payment_method_id'] == 6){
            $stockWallet = $stockCustomerTable->getDividendWalletMoney($data['customer_id'])['wallet_dividend_money'];
        }
        if($detail['source'] == 'publisher' && (int)$stockPublishRemaining < (int)$detail['quantity']){
            return [
                'error' => true,
                'message' => __('Số cổ phiếu còn lại từ nhà phát hành không đủ')
            ];
        }
        elseif($detail['source'] == 'market' && (int)$stockMarketRemaining < (int)$detail['quantity']){
            return [
                'error' => true,
                'message' => __('Số cổ phiếu còn lại của người bán ở chợ không đủ')
            ];
        }
        elseif($detail['payment_method_id'] != 1 && $detail['payment_method_id'] != 4)
        {
            if((double)$stockWallet < (double)$detail['total']){
                return [
                    'error' => true,
                    'message' => __('Không đủ tiền để xác nhận yêu cầu mua cổ phiếu')
                ];
            }
        }
        elseif($detail['source'] == 'market' && (int)$currStock < (int)$detail['quantity']){
            return [
                'error' => true,
                'message' => __('Số cổ phiếu còn lại của người bán không đủ')
            ];
        }
        $addReceiptDetail = $this->stockOrders->addReceiptDetail($param);
        return $addReceiptDetail;
    }

    /**
     * Danh sách phiếu thu
     *
     * @param Request $request
     * @return mixed
     */
    public function getListReceiptDetail(Request $request) {
        $param = $request->all();
        $listReceipDetail = $this->stockOrders->getListReceiptDetail($param);
        return $listReceipDetail;
    }

    /**
     * Huỷ xác nhận yêu cầu mua cổ phiếu
     *
     * @param Request $request
     * @return mixed
     */
    public function cancelConfirm(Request $request){
        $param = $request->all();
        $changeStatusStock = $this->stockOrders->cancelConfirm($param);
        return $changeStatusStock;
    }

    /**
     * Tạo bill
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function createBill(){
        $view = view('admin::buy-stock-request.popup.create-bill')->render();
        return \response()->json($view);
    }
}