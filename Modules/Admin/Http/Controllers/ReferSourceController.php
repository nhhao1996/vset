<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\ReferSource\ReferSourceRepositoryInterface;

class ReferSourceController extends Controller
{

    protected $referSource;
    public function __construct(
        ReferSourceRepositoryInterface $referSource
    )
    {
        $this->referSource = $referSource;
    }

    public function indexAction()
    {
        $list = $this->referSource->getList();
        return view('admin::refer-source.index', [
            'list' => $list
        ]);
    }

    public function editAction($id, Request $request)
    {
        $info = $this->referSource->getInfo($id);
        $request->session()->forget('listService');
        $request->session()->put('listService',$info['service']);
        return view('admin::refer-source.edit', [
            'config' => $info['config'],
            'listService' => $info['listService'],
            'refer_source_id' => $id,
            'source' => $info['source']
        ]);

    }

    public function removeAction(Request $request) {
        $remove = $this->referSource->removeService($request->all());
        return $remove;
    }

    public function getListService(Request $request) {
        $param = $request->all();
        if ($param['type'] == 'refer') {
            $param['service_list'] = $request->session()->get('listService')['refer'];
        } else if ($param['type'] == 'register') {
            $param['service_list'] = $request->session()->get('listService')['register'];
        }

        $list = $this->referSource->getListService($param);
        return \response()->json($list);
    }

    public function addListService(Request $request)
    {
        $param = $request->all();

        if ($param['type'] == 'refer') {
            $data = $request->session()->get('listService');
            $data['refer'] = array_merge($data['refer'],$param['listService']);
            $request->session()->put('listService',$data);
        } else if ($param['type'] == 'register') {
            $data = $request->session()->get('listService');
            $data['register'] = array_merge($data['register'],$param['listService']);
            $request->session()->put('listService',$data);
        }

        $addList = $this->referSource->addListService($param);
        return \response()->json($addList);
    }

    public function saveConfig(Request $request) {
        $param = $request->all();
        $param['listService'] = $request->session()->get('listService');
        $update = $this->referSource->updateConfig($param);
        return response()->json($update);

    }

    public function removeService(Request $request){
        $param = $request->all();
        $listServiceProduct = $request->session()->get('listService');
        $delete = array_search($param['service_id'],$listServiceProduct[$param['type']]);
        unset($listServiceProduct[$param['type']][$delete]);
        $request->session()->put('listService',$listServiceProduct);
        return response()->json();
    }

}