<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\Bank\BankRepositoryInterface;

class BankController extends Controller
{
    protected $bank;

    public function __construct(BankRepositoryInterface $bank)
    {
        $this->bank = $bank;
    }

    public function index()
    {
        $filter = request()->all();
        $data = $this->bank->getListNew($filter);

        return view('admin::bank.index', [
            'LIST' => $data,
            'FILTER' => $filter,
        ]);
    }

    // FUNCTION  FILTER LIST ITEM
    protected function filters()
    {
    }

    /**
     * Danh sách hỗ trợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filters = $request->only(['page', 'search_keyword']);
        $data = $this->bank->getListNew($filters);
        return view('admin::bank.list', [
                'LIST' => $data,
                'FILTER' => $filters,
            ]
        );
    }

    public function create() {
        return view('admin::bank.create');
    }

    public function detailFormBank(Request $request) {
        $param = $request->all();
        $detail = $this->bank->getDetail($param['id']);
        return $detail;
    }

    public function editFormBank(Request $request) {
        $param = $request->all();
        $edit = $this->bank->getEdit($param['id']);
        return $edit;
    }

    public function add(Request $request) {
        $param = $request->all();
        $add = $this->bank->addbank($param);
        return $add;
    }

    public function addFormBankNew() {
        $addForm = $this->bank->addFormBankNew();
        return $addForm;
    }

    public function edit($id) {
        $detail = $this->bank->getDetail($id);
        return view('admin::bank.edit', [
                'detail' => $detail,
            ]
        );
    }

    public function update(Request $request) {
        $param = $request->all();
        $updateBank = $this->bank->update($param);
        return $updateBank;
    }

    public function remove(Request $request) {
        $param = $request->all();
        $delete = $this->bank->deleteBank($param['id']);
        return $delete;
    }
}