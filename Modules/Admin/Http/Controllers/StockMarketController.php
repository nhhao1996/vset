<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/13/2021
 * Time: 3:08 PM
 * @author nhandt
 */


namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Repositories\StockMarket\StockMarketRepoInterface;

class StockMarketController extends Controller
{
    private $stockMarket;
    public function __construct(StockMarketRepoInterface $stockMarket)
    {
        $this->stockMarket = $stockMarket;
    }

    /**
     * Ds đang bán ở chợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function indexAction(Request $request) {
        $filters = $request->all();
        $filters['status_route'] = 'selling';
        $data = $this->stockMarket->getList($filters);
        return view('admin::stock-market.selling.index', [
            'LIST' => $data,
            'FILTER' => $request->all(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    /**
     * Ds đang bán ở chợ có filter
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filters = $request->all();
        $filters['status_route'] = 'selling';
        $data = $this->stockMarket->getList($filters);
        return view('admin::stock-market.selling.list', [
            'LIST' => $data,
            'page' => $filters['page']
        ]);
    }

    /**
     * Ds mua cổ phiếu ở chợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function indexActionBuyStock(Request $request) {
        $filters = $request->all();
        $filters['status_route'] = 'buy-stock';
        $data = $this->stockMarket->getListBuyStock($filters);
        return view('admin::stock-market.buy-stock.index', [
            'LIST' => $data,
            'FILTER' => $request->all(),
        ]);
    }

    /**
     * Ds cổ phiếu mua ở chợ có filter
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listActionBuyStock(Request $request)
    {
        $filters = $request->all();
        $filters['status_route'] = 'buy-stock';
        $data = $this->stockMarket->getListBuyStock($filters);
        return view('admin::stock-market.buy-stock.list', [
            'LIST' => $data,
            'page' => $filters['page']
        ]);
    }

    /**
     * Chi tiết mua cổ phiếu ở chợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function detailActionBuyStock(Request $request){
        $stock_order_id = $request->id;
        $data = $this->stockMarket->getDetailBuyStock($stock_order_id);
        return view('admin::stock-market.buy-stock.detail', [
            'item' => $data,
        ]);
    }

    /**
     * Ds mua cổ phiếu ở chợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function indexActionRegisterToSell(Request $request) {
        $filters = $request->all();
        $filters['status_route'] = 'register-to-sell';
        $data = $this->stockMarket->getListRegisterToSell($filters);
        return view('admin::stock-market.register-to-sell.index', [
            'LIST' => $data,
            'FILTER' => $request->all(),
        ]);
    }

    /**
     * Ds cổ phiếu mua ở chợ có filter
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listActionRegisterToSell(Request $request)
    {
        $filters = $request->all();
        $filters['status_route'] = 'register-to-sell';
        $data = $this->stockMarket->getListRegisterToSell($filters);
        return view('admin::stock-market.register-to-sell.list', [
            'LIST' => $data,
            'page' => $filters['page']
        ]);
    }

    /**
     * Chi tiết mua cổ phiếu ở chợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function detailActionRegisterToSell(Request $request){
        $stock_market_id = $request->id;
        $data = $this->stockMarket->getDetailRegisterToSell($stock_market_id);
        return view('admin::stock-market.register-to-sell.detail', [
            'item' => $data,
        ]);
    }

    /**
     * Xác nhận hoặc huỷ yêu cầu bán cổ phiếu
     *
     * @param Request $request
     * @return mixed
     */
    public function ChangeStatusRegisterToSell(Request $request){
        $param = $request->all();
        $changeStatusStock = $this->stockMarket->changeStatus($param);
        return $changeStatusStock;
    }
}