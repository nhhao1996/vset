<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/15/2021
 * Time: 9:32 AM
 * @author nhandt
 */


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\TransferRequest\TransferRequestRepoInterface;

class TransferRequestController extends Controller
{
    private $stockTransfer;
    public function __construct(TransferRequestRepoInterface $stockTransfer)
    {
        $this->stockTransfer = $stockTransfer;
    }

    /**
     * Ds yêu cầu chuyển nhượng
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function indexAction(Request $request) {
        $filters = $request->all();
        $filters['type_list'] = 'transfer-request';
        $data = $this->stockTransfer->getList($filters);
        return view('admin::transfer-request.index', [
            'LIST' => $data,
            'FILTER' => $request->all(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    /**
     * Ds yêu cầu chuyển nhượng có filter
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filters = $request->all();
        $filters['type_list'] = 'transfer-request';
        $data = $this->stockTransfer->getList($filters);
        return view('admin::transfer-request.list', [
            'LIST' => $data,
            'page' => $filters['page']
        ]);
    }

    /**
     * Chi tiết yêu cầu chuyển nhượng
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function detailAction(Request $request){
        $stock_order_id = $request->id;
        $data = $this->stockTransfer->getDetailTransfer($stock_order_id);
        return view('admin::transfer-request.detail',[
            'item' => $data
        ]);
    }

    /**
     * Xác nhận hoặc huỷ yêu cầu chuyển nhượng
     *
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request){
        $param = $request->all();
        $changeStatusStock = $this->stockTransfer->changeStatus($param);
        return $changeStatusStock;
    }
}