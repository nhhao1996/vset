<?php


namespace Modules\Admin\Http\Controllers;


use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\Product\StoreRequest;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Repositories\CustomerContract\CustomerContractRepositoryInterface;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;
use Maatwebsite\Excel\Excel;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
//use Barryvdh\DomPDF\PDF;
use PDF;


class StockTransferContractController extends Controller
{
    protected $stockContract;
    protected $excel;
    protected $order;
    protected $staff;  
    protected $stock;

    public function __construct(
      
        CustomerContractRepositoryInterface $stockContract,
        OrderRepositoryInterface $orders,
        StockRepositoryInterface $stock,
        
        Excel $excel,
        StaffsTable $staff
       
    )
    {
       
        // $this->stockContract = $stockContract;
        // $this->excel = $excel;
        // $this->order = $orders;
        // $this->staff = $staff;       
        // $this->stock=$stock;
        $this->stock = $stock; 
    }
    public function indexAction()
  {
     
       $filters = request()->all();
       $filters['product_category_id'] = 2;
      $list = $this->stock->getListStockTransfer($filters);
//        dd($list);


            
        return view('admin::stock-transfer.index', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
            'type'=>'saving',
            'isEdit'=>false
//            'page' => $filter['page']

        ]);
    }
    public function getListBond()
    {
       
         $filters = request()->all();
          $list = $this->stock->getListStockTransfer(['product_category_id'=>1]);


          return view('admin::stock-transfer.index', [
              'LIST' => $list,
              'FILTER' => $this->filters(),
              'type' => 'bond',
              'isEdit'=>false
  //            'page' => $filter['page']
  
          ]);
      }
    public function listAction(Request $request){
        $filters = $request->all();

        $lists = $this->stock->getListStockTransfer($filters); 
        return view('admin::stock-transfer.list', ['LIST' => $lists, 'page' => $filters['page']]);
    }
    public function detail($id)
    {// //        $filter
        $stockPublish = $this->stock->getLastStockPublish();
        $stock = $this->stock->getDetailStockTransfer($id);          
        // dd($stock);
        $list = [];
        return view('admin::stock-transfer.detail', [
            'LIST' => $list,           
            'stock'=>$stock,
            'stockPublish'=>$stockPublish,
            'isEdit'=>true
        ]);
       
    }
    public function editAction($id){
        $stockPublish = $this->stock->getLastStockPublish();
        $stock = $this->stock->getDetailStockTransfer($id);
        $list = [];
        return view('admin::stock-transfer.edit', [
            'LIST' => $list,
            'stock'=>$stock,
            'stockPublish'=>$stockPublish,
            'isEdit'=>true
        ]);

    }
//     public function edit($id)
//     {
// // //        $filter = request()->all();
//         $stock = $this->stock->getDetailStockTransfer($id);  
//         // dd($stock);
//         $list = [];
//         return view('admin::stock-transfer.edit', [
//             'LIST' => $list,           
//             'stocks'=>$stock,
//             'isEdit'=>true/           

//         ]);
       
//     }
    public function uploadRelatedDoc(Request $request){
        
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }
        /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . "_file." . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);
        return url('/').'/' . $this->transferTempfileToAdminfile($file_name, str_replace('', '', $file_name));
    }
     //Chuyển file từ folder temp sang folder chính
     private function transferTempfileToAdminfile($filename)
     {
         $old_path = TEMP_PATH . '/' . $filename;
         $new_path = STOCK_UPLOADS_PATH . date('Ymd') . '/' . $filename;
         Storage::disk('public')->makeDirectory(STOCK_UPLOADS_PATH . date('Ymd'));
         Storage::disk('public')->move($old_path, $new_path);
         return $new_path;
     }
     public function saveFile(Request $request){      
        $param = $request->all();
        if (!isset($param['arrFile']) || count($param['arrFile']) == 0) {
            return ['error' => 1, 'message' =>'Yêu cầu thêm tài liệu thành công'];
        }
        $data = [];
        foreach ($param['arrFile'] as $item) {
            $data[] = [
                'stock_config_id'=>$param['stock_config_id'],
                "file"=>$item,
                "created_at"=>Carbon::now()->format("Y-m-d"),
                "created_by"=>Auth::id()
            ];
        }

        $this->stock->addStockFile($data);
        return ['error' => 0, 'message' =>'Thêm tài liệu liên quan thành công'];

    }
    public function saveStockInfo(Request $request){
       
        $params = $request->all();       
        $stock_config_id = $params['stock_config_id']; 
        $arrIdFiles = $params['arrIdFiles'];
        unset($params['arrIdFiles']);
        $this->stock->removeStockFile($arrIdFiles);
        $this->stock->updateStock($stock_config_id,$params);      
        return ['error'=>0, 'message'=>'Lưu thông tin thành công'];

    }
    public function editStock(Request $request){
       
        $params = $request->all();       
        $stock_config_id = $params['stock_config_id'];                        
        $this->stock->updateStock($stock_config_id,$params);      
        return ['error'=>0, 'message'=>'Cập nhật thành công'];

    }  
   
    public function confirmTransfer(Request $request){
        $id = $request->stock_tranfer_contract_id;

        return $this->stock->confirmTransferStock($id);     
    }
    public function cancelRequest(Request $request){
        $param = $request->all();
        $data = [
            'reason_vi' => $param['reason_vi'],
            'reason_en' => $param['reason_en'],
            'status' => 'cancel'
        ];
        $this->stock->editStockTransfer($param['stock_tranfer_contract_id'],$data);
        return ['error'=>true, 'message'=>'Hủy yêu cầu chuyển đổi thành công'];
    }

    protected function filters()
    {
        return [
            'products$is_actived' => [
                'data' => [
                    '' => __('Trạng thái hiển thị'),
                    1 => __('Hoạt động'),
                    0 => __('Tạm ngưng')
                ]
            ]
        ];
    }

  


}