<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\ConfigModule\ConfigModuleRepositoryInterface;

class ConfigModuleController extends Controller
{

    protected $configModule;
    public function __construct(ConfigModuleRepositoryInterface $configModule)
    {
        $this->configModule = $configModule;
    }

    public function indexAction()
    {
        $list = $this->configModule->getList();
        return view('admin::config-module.index', [
            'list' => $list
        ]);
    }

    public function changeActiveAction(Request $request){
        $update = $this->configModule->updateActive($request->all());
        return $update;
    }

}