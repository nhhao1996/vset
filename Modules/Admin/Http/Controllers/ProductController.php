<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 10/2/2018
 * Time: 12:33 PM
 */

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Requests\Product\StoreRequest;
use Modules\Admin\Http\Requests\Product\UpdateRequest;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\MapProductAttribute\MapProductAttributeRepositoryInterface;
use Modules\Admin\Repositories\Product\ProductRepositoryInterface;
use Modules\Admin\Repositories\ProductAttribute\ProductAttributeRepositoryInterface;
use Modules\Admin\Repositories\ProductAttributeGroup\ProductAttributeGroupRepositoryInterface;
use Modules\Admin\Repositories\ProductBranchPrice\ProductBranchPriceRepositoryInterface;
use Modules\Admin\Repositories\ProductCategory\ProductCategoryRepositoryInterface;
use Modules\Admin\Repositories\ProductChild\ProductChildRepositoryInterface;
use Modules\Admin\Repositories\ProductImage\ProductImageRepositoryInterface;
use Modules\Admin\Repositories\ProductInventory\ProductInventoryRepositoryInterface;
use Modules\Admin\Repositories\ProductModel\ProductModelRepositoryInterface;
use Modules\Admin\Repositories\Supplier\SupplierRepositoryInterface;
use Modules\Admin\Repositories\Unit\UnitRepositoryInterface;
//VSet
class ProductController extends Controller
{
    protected $product;

    public function __construct(
        ProductRepositoryInterface $product
    )

    {
        $this->product = $product;
    }

    public function indexAction()
    {
        $productList = $this->product->list();
        $listCategory = $this->product->getCategoryProduct();
        return view('admin::product.index', [
            'LIST' => $productList,
            'FILTER' => $this->filters(),
            'listCategory' => $listCategory
        ]);
    }

    protected function filters()
    {
        return [
            'products$is_actived' => [
                'data' => [
                    '' => __('Trạng thái hiển thị'),
                    1 => __('Hoạt động'),
                    0 => __('Tạm ngưng')
                ]
            ]
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->only(['page', 'display', 'search_type', 'search_keyword', 'created_at','products$is_actived','product_category_id']);
        $productList = $this->product->list($filters);
        $listCategory = $this->product->getCategoryProduct();
        return view('admin::product.list',
            [
                'LIST' => $productList,
                'page' => $filters['page'],
                'listCategory' => $listCategory
            ]
        );
    }

    public function removeAction($id)
    {
       $delete = $this->product->deleteProduct($id);
       return $delete;
    }

    public function addAction()
    {
        $listCategory = $this->product->getCategoryProduct();
        return view('admin::product.add', [
            'category' => $listCategory
        ]);
    }

    public function submitAddAction(StoreRequest $request)
    {
        $param = $request->all();
        $createProduct = $this->product->createProduct($param);
        return $createProduct;
    }

    public function editAction($id)
    {
        $product = $this->product->getItem($id);
        $listCategory = $this->product->getCategoryProduct();
//        lấy danh sách phương thức thanh toán
        $listPaymentMethod = $this->product->getListPaymentMethod();
        $listInvestmentTime = $this->product->getListInvestmentTime($product['product_category_id']);
        $withdrawInterestTime = $this->product->getListWithdrawInterestTime();
        $image = $this->product->getListImageContract($id);
        return view('admin::product.edit', [
            'category' => $listCategory,
            'detail' => $product,
            'listPaymentMethod' => $listPaymentMethod,
            'listInvestmentTime' => $listInvestmentTime,
            'withdrawInterestTime' => $withdrawInterestTime,
            'imageContractMobile' => $image['mobile'],
            'imageContract' => $image['desktop'],
        ]);
    }

    public function detailAction($id,Request $request) {

        $product = $this->product->getItem($id);
        $productInterest = $this->product->getListProductInterest($id);
        $productBonus = $this->product->getProductBonus($id);
        $listCategory = $this->product->getCategoryProduct();
        $listPaymentMethod = $this->product->getListPaymentMethod();
        $listInvestmentTime = $this->product->getListInvestmentTime($product['product_category_id']);
        $withdrawInterestTime = $this->product->getListWithdrawInterestTime();
        $image = $this->product->getListImageContract($id);
        $config = $this->product->getConfigBonus($id);
        $listService = $this->product->getListService($id);
        $request->session()->forget('listServiceProduct');
        $request->session()->put('listServiceProduct',collect($listService)->pluck('service_id')->toArray());
        $param = $request->all();
        return view('admin::product.detail', [
            'category' => $listCategory,
            'detail' => $product,
            'listPaymentMethod' => $listPaymentMethod,
            'listInvestmentTime' => $listInvestmentTime,
            'withdrawInterestTime' => $withdrawInterestTime,
            'productInterest' => $productInterest,
            'productBonus' => $productBonus,
            'imageContractMobile' => $image['mobile'],
            'imageContract' => $image['desktop'],
            'config' => $config,
            'listService' => $listService,
            'tab' => isset($param['tab']) ? $param['tab'] : null
        ]);
    }

    public function getListProductBonus(Request $request) {
//        danh sách tặng thêm
        $filter = $request->all();
        $listProductBonus = $this->product->getListProductBonus($filter);
        return $listProductBonus;
    }

    public function addProductBonus(Request $request) {
        $param = $request->all();
        $addProductBonus = $this->product->addProductBonus($param);
        return $addProductBonus;
    }

    public function editProductBonus(Request $request) {
        $param = $request->all();
        $editProductBonus = $this->product->editProductBonus($param);
        return $editProductBonus;
    }

    public function editProductBonusPost(Request $request) {
        $param = $request->all();
        $editProductBonusPost = $this->product->editProductBonusPost($param);
        return $editProductBonusPost;
    }

    public function detailProductBonus(Request $request) {
        $param = $request->all();
        $detailProductBonus = $this->product->detailProductBonus($param);
        return $detailProductBonus;
    }

    public function removeProductBonus(Request $request) {
        $param = $request->all();
        $removeProductBonus = $this->product->removeProductBonus($param['id']);
    }

    public function getListInterestRate(Request $request) {
//        danh sách tặng thêm
        $filter = $request->all();
        $listInterestRate = $this->product->getListInterestRate($filter);
        return $listInterestRate;
    }

    public function addProductInterest(Request $request) {
        $param = $request->all();
        $addProductInterest = $this->product->addProductInterest($param);
        return $addProductInterest;
    }

    public function detailProductInterest(Request $request) {
        $param = $request->all();
        $detailProductInterest = $this->product->detailProductInterest($param);
        return $detailProductInterest;
    }

    public function editProductInterest(Request $request) {
        $param = $request->all();
        $editProductInterest = $this->product->editProductInterest($param);
        return $editProductInterest;
    }

    public function editProductInterestPost(Request $request) {
        $param = $request->all();
        $editProductInterestPost = $this->product->editProductInterestPost($param);
        return $editProductInterestPost;
    }

    public function removeProductInterest(Request $request) {
        $param = $request->all();
        $removeProductBonus = $this->product->removeProductInterest($param['id']);
    }

    public function submitListInvestmentWithdraw(Request $request) {
        $param = $request->all();
        $submit = $this->product->submitListInvestmentWithdraw($param);
        return $submit;
    }

    public function submitListProductBonus(Request $request){
        $param = $request->all();
        $submit = $this->product->submitListProductBonus($param);
        return $submit;
    }

    public function submitEditAction(UpdateRequest $request) {
        $param = $request->all();
        $editProduct = $this->product->editProduct($param);
        return $editProduct;
    }
    /**
     * Upload image
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAction(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }

    /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . "_product." . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);
        return $file_name;
    }

    public function getListService(Request $request){
        $param = $request->all();
        $param['service_list'] = $request->session()->get('listServiceProduct');
        $showPopup = $this->product->getListShowPopup($param);
        return $showPopup;
    }

    public function addListService(Request $request){
        $param = $request->all();
        $data = $request->session()->get('listServiceProduct');
        $data = array_merge($data,$param['listService']);
        $request->session()->put('listServiceProduct',$data);

        return $this->product->addListService($param);
    }

    public function saveConfig(Request $request) {
        $param = $request->all();
        $param['listService'] = $request->session()->get('listServiceProduct');
        $update = $this->product->updateConfig($param);
        return response()->json($update);

    }

    public function deleteService(Request $request) {
        $param = $request->all();
        $listServiceProduct = $request->session()->get('listServiceProduct');
        $delete = array_search($param['service_id'],$listServiceProduct);
        unset($listServiceProduct[$delete]);
        $request->session()->put('listServiceProduct',$listServiceProduct);
        return response()->json();
    }
}