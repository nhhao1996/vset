<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\ServiceSerial\ServiceSerialRepositoryInterface;

class ServiceSerialController extends Controller
{
    protected $serviceSerial;
    protected $request;

    public function __construct(ServiceSerialRepositoryInterface $serviceSerial,Request $requests)
    {
        $this->serviceSerial = $serviceSerial;
        $this->request = $requests;
    }

    public function indexAction()
    {
        $get = $this->serviceSerial->list();
        return view('admin::service-serial.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->all();
        $list = $this->serviceSerial->list($filter);
        return view('admin::service-serial.list', [
            'LIST' => $list,
            'page' => $filter['page']
        ]);
    }

    public function showAction($id) {
        $detail = $this->serviceSerial->getDetail($id);
        return view('admin::service-serial.detail', [
            'detail' => $detail
        ]);
    }

    public function editAction($id) {
        $detail = $this->serviceSerial->getDetail($id);
        return view('admin::service-serial.edit', [
            'detail' => $detail
        ]);
    }

    public function editPost() {
        $param = $this->request->all();
        $edit = $this->serviceSerial->editPost($param);
        return $edit;
    }
}