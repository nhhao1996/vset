<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 10/12/2018
 * Time: 10:19 AM
 */

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Repositories\ServiceCategory\ServiceCategoryRepositoryInterface;
use function GuzzleHttp\Psr7\str;

class ServiceCategoryController extends Controller
{
    protected $service_category;

    public function __construct(ServiceCategoryRepositoryInterface $service_categories)
    {
        $this->service_category = $service_categories;
    }

    public function indexAction()
    {
        $get = $this->service_category->list();

        return view('admin::service-category.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),

        ]);
    }

    protected function filters()
    {
        return [
            'is_actived' => [
                'data' => [
                    '' => __('Chọn trạng thái'),
                    1 => __('Hoạt động'),
                    0 => __('Tạm ngưng')
                ]
            ]
        ];
    }

    //function view list
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword', 'is_actived']);
        $list = $this->service_category->list($filter);
        return view('admin::service-category.list', ['LIST' => $list, 'page' => $filter['page']]);
    }

    //function add
    public function addAction(Request $request)
    {
        $name_vi = $request->name_vi;
        $name_en = $request->name_en;
        $testVI = $this->service_category->testNameVI(str_slug($name_vi), 0);
        $testEN = $this->service_category->testNameEN(str_slug($name_en), 0);
        if ($testVI == null && $testEN == null) {
            $data = [
                'name_vi' => strip_tags($name_vi),
                'name_en' => strip_tags($name_en),
                'slug_vi' => str_slug($name_vi),
                'slug_en' => str_slug($name_en),
                'description_vi' => strip_tags($request->description_vi),
                'description_en' => strip_tags($request->description_en),
                'is_actived' => $request->is_actived,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];
            $this->service_category->add($data);
            $option = $this->service_category->getOptionServiceCategory();
            return response()->json([
                'status' => 1,
                'close' => $request->close,
                'optionCategory' => $option
            ]);
        } else {
            return response()->json([
                'status' => 0
            ]);
        }

    }

    //Thay đổi status
    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
        $data['is_actived'] = ($change['action'] == 'unPublish') ? 1 : 0;
        $this->service_category->edit($data, $change['id']);
        return response()->json([
            'status' => 0
        ]);
    }

    //Function xóa
    public function removeAction($id)
    {
        $check = $this->service_category->checkUsing($id);
        if (count($check) == 0) {
            $this->service_category->remove($id);
            return response()->json([
                'error' => 0,
                'message' => 'Xóa thành công'
            ]);
        } else {
            return response()->json([
                'error' => 1,
                'message' => 'Loại dịch vụ đã được sử dụng không thể xóa'
            ]);
        }

    }

    //function get edit
    public function editAction(Request $request)
    {
        $id = $request->id;
        $item = $this->service_category->getItem($id);
        $data = [
            'service_category_id' => $item->service_category_id,
            'name_vi' => $item->name_vi,
            'name_en' => $item->name_en,
            'description_vi' => $item->description_vi,
            'description_en' => $item->description_en,
            'is_actived' => $item->is_actived
        ];
        $view = view('admin::service-category.edit',['detail' => $data])->render();
        return response()->json($view);
    }

    public function addForm(){
        $view = view('admin::service-category.add')->render();
        return response()->json($view);
    }

    //function submit edit
    public function submitEditAction(Request $request)
    {
        $id = $request->id;
        $name_vi = $request->name_vi;
        $name_en = $request->name_en;
        $testVI = $this->service_category->testNameVI(str_slug($name_vi), $id);
        $testEN = $this->service_category->testNameEN(str_slug($name_en), $id);
        if ($testVI == null && $testEN == null) {
            $data = [
                'name_vi' => $name_vi,
                'name_en' => $name_en,
                'slug_vi'=>str_slug($name_vi),
                'slug_en'=>str_slug($name_en),
                'description_vi' => $request->description_vi,
                'description_en' => $request->description_en,
                'is_actived' => $request->is_actived,
                'updated_by' => Auth::id(),
            ];
            $this->service_category->edit($data, $id);
            return response()->json([
                'status' => 1
            ]);
        } else {
            return response()->json([
                'status' => 0
            ]);
        }
    }
}