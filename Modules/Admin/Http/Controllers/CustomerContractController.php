<?php


namespace Modules\Admin\Http\Controllers;


use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\Product\StoreRequest;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Repositories\CustomerContract\CustomerContractRepositoryInterface;
use Maatwebsite\Excel\Excel;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;


class CustomerContractController extends Controller
{
    protected $customerContract;
    protected $excel;
    protected $order;
    protected $staff;

    public function __construct(
        CustomerContractRepositoryInterface $customerContract,
        OrderRepositoryInterface $orders,
        Excel $excel,
        StaffsTable $staff
    )
    {
        $this->customerContract = $customerContract;
        $this->excel = $excel;
        $this->order = $orders;
        $this->staff = $staff;
    }
    public function indexAction()
    {
        $list = $this->customerContract->list();       
        return view('admin::customer-contract.index', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
        ]);
    }
    public function listNearlyExpired(Request $request)
    {
        $filter = request()->all();
        $filter['nearly_expired'] = 1;
        $filter['time_expired'] = 3;
        $list = $this->customerContract->list($filter);
        return view('admin::customer-contract.contract-nearly-expired', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
        ]);
    }
    public function endContract(Request $request){
       $filter = $request ->all();
       return $this->customerContract->endContract($filter);
    }

    protected function filters()
    {

        return [

        ];
    }

    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword','active','customer_contract_start_date',
            'created_at', 'search', 'contract_status', 'contract_value', 'is_active','is_deleted','product_category_id','time_expired']);
        if ($request->session()->has('filter_contract')) {
            session()->forget('filter_contract');
        }
        $request->session()->put('filter_contract', $filter);
        $list = $this->customerContract->list($filter);
        return view('admin::customer-contract.list',
            ['LIST' => $list,
            'page' =>$filter['page']]);
    }

    public function postlistNearlyExpired(Request $request)
    {
        $filter = request()->all();
//        $filter['active'] = 0;
        $filter['nearly_expired'] = 1;
        if (!isset($filter['time_expired'])){
            $filter['time_expired'] = 3;
        }

        $list = $this->customerContract->list($filter);
        return view('admin::customer-contract.list-contract-nearly-expired',
            ['LIST' => $list,
                'page' =>$filter['page']]);

    }

    //export Excel
    public function exportExcelAction(Request $request)
    {
//        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
//            'created_at', 'search', 'contract_status', 'contract_value', 'is_active']);
//        unset($filter['_token']);
        $filter = [];
        if($request->session()->has('filter_contract')) {
            $filter = $request->session()->get('filter_contract');
        }

        $heading = [
            'STT',
            'Số hợp đồng',
            'Họ và tên nhà đầu tư',
            'Số điện thoại nhà đầu tư',
            'Địa chỉ liên hệ',
            'Số lượng gói',
            'Loại gói',
            'Tổng giá trị HĐ',
            'Lãi suất (%/năm)',
            'Ngày nhận lãi',
            'Ngày bắt đầu hợp đồng',
            'Ngày hết hạn hợp đồng',
            'Trạng thái'
        ];
        $list = $this->customerContract->listExport($filter);
        $data = [];
        foreach ($list as $key => $item) {
            $data [] = [
                $key+1,
                $item['customer_contract_code'],
                $item['full_name'],
                $item['customer_phone'],
                $item['contact_address_contact'].'-'.$item['district_name_contact'].'-'.$item['province_name_contact'],
                $item['quantity'],
                $item['category_name_vi'],
                number_format($item['total_amount'],2),
                $item['month_interest'] == "0.00" ? "0" : number_format($item['month_interest']*12,2),
                $item['withdraw_date_planning'] == null ? Carbon::parse($item['customer_contract_start_date'])->addMonth($item['withdraw_interest_time'])->format('d-m-Y') : Carbon::parse($item['withdraw_date_planning'])->format('d-m-Y'),
                $item['customer_contract_start_date'] == null ? 'N/A' : Carbon::parse($item['customer_contract_start_date'])->format('d-m-Y'),
                $item['customer_contract_end_date'] == 0 ? 'N/A' : Carbon::parse($item['customer_contract_end_date'])->format('d-m-Y'),
//                $item['is_deleted'] == 1 ? 'Ngừng hoạt động' : 'Hoạt động'
            ];

            if ($item['customer_contract_end_date'] != null && $item['customer_contract_end_date'] != ''){
                if (\Carbon\Carbon::now() < \Carbon\Carbon::parse($item['customer_contract_end_date']) && $item['is_active'] == 1) {
                    $data[$key]['is_deleted'] = 'Đang hoạt động';
                } else {
                    $data[$key]['is_deleted'] = 'Đã hết hạn';
                }
            } else {
                if ($item['is_active'] == 1) {
                    $data[$key]['is_deleted'] = 'Đang hoạt động';
                } else {
                    $data[$key]['is_deleted'] = 'Đã hết hạn';
                }
            }
        }
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
//        $this->excel->download(new ExportFile($heading, $data), 'test.xlsx');
        return \Maatwebsite\Excel\Facades\Excel::download(new ExportFile($heading, $data), 'Danh sách hợp đồng.xlsx');
    }

    public function removeAction(Request $request) {
        $param = $request->all();
        $delete = $this->customerContract->deleteCustomerContract($param['id']);
        return $delete;
    }

    public function editAction(Request $request,$id) {
        $param = $request->all();
        $edit = $this->customerContract->getDetail($id);
        $oldContract = $this->customerContract->getOldContract($edit['detail']['customer_contract_code']);
        $staff = $this->staff->getStaffOption();
        return view('admin::customer-contract.edit', [
            'item' => $edit["detail"],
            "serial"=>$edit["serial"],
            "file"=>$edit["file"],
            "staff"=>$staff,
            'oldContract' => $oldContract
        ]);
    }

    /**
     * Upload image dropzone
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadDropzoneAction(Request $request)
    {
        return $this->customerContract->uploadDropzone($request);
    }

    public function updateAction(Request $request){
        $update = $this->customerContract->updateContractSerial($request);
        return $update;
    }

    public function addAction() {

        $data = $this->order->listForContract();
        return view('admin::customer-contract.add', [
            "LIST"=>$data,
        ]);
    }

    public function submitAddAction(StoreRequest $request)
    {
        $param = $request->all();
        $createCustomeContract = $this->customerContract->createCustomeContract($param);
        return $createCustomeContract;
    }

    /**
     * Chi tiết hợp đồng
     * @param $contractId
     * @param string $tab
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show($contractId, $tab = '')
    {
        $data = $this->customerContract->dataViewDetail($contractId);
        $data['oldContract'] = $this->customerContract
            ->getOldContract($data['item']['customer_contract_code']);

        $data['tab'] = $tab;
        return view('admin::customer-contract.detail', $data);
    }

    /**
     * Danh sách file hợp đồng
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listFileAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'customer_contract_id']);

        $list = $this->customerContract->listFile($filter);

        return view('admin::customer-contract.list-file', $list);
    }

    /**
     * Danh sách lãi suất tháng
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listInterestMonthAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'customer_contract_id']);

        $list = $this->customerContract->listInterestMonth($filter);

        return view('admin::customer-contract.list-interest-month', $list);
    }

    /**
     * Danh sách lãi suất ngày
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listInterestDateAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'customer_contract_id']);

        $list = $this->customerContract->listInterestDate($filter);

        return view('admin::customer-contract.list-interest-date', $list);
    }

    /**
     * Danh sách lãi suất theo thời điểm
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listInterestTimeAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'customer_contract_id']);

        $list = $this->customerContract->listInterestTime($filter);

        return view('admin::customer-contract.list-interest-time', $list);
    }

    /**
     * Danh sách yêu cầu rút gốc
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listWithdrawRequestAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'customer_contract_id']);

        $list = $this->customerContract->listWithdraw($filter);

        return view('admin::customer-contract.list-withdraw', $list);
    }

    /**
     * Danh sách số serial
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listContractSerialAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'customer_contract_id']);

        $list = $this->customerContract->listSerial($filter);

        return view('admin::customer-contract.list-serial', $list);
    }

    public function confirmContract(Request $request) {
        $param = $request->all();
        $confirm = $this->customerContract->confirmContract($param);
        return $confirm;
    }

//    Danh sách lãi cần duyệt
    public function getListInterestApproved(Request $request){
        $request->session()->forget('list_contract_interest');
        $request->session()->put('list_contract_interest',[]);
        $list = $this->customerContract->getListContractApproved();
        return view('admin::customer-contract.contract-approved', [
            'list' => $list
        ]);
    }

    public function getListInterestApprovedChange(Request $request)
    {
        $filter = $request->all();
        $request->session()->put('list_contract_interest',$filter);
        $list = $this->customerContract->getListContractApproved($filter);
        return view('admin::customer-contract.list-contract-approved',
            ['list' => $list,
                'page' =>$filter['page']]);
    }

    public function confirmContractInterest(Request $request){
        $confirm = $this->customerContract->confirmContractAll($request->all());
        return $confirm;
    }

    public function exportListContractInterest(Request $request){
        $param = $request->session()->get('list_contract_interest');
        $buyBondsRequest = $this->customerContract->listExportContractInterest($param);

        $data = [];

        foreach ($buyBondsRequest as $item) {
            $data[] = [
                $item['customer_contract_code'],
                $item['full_name'],
                $item['phone'],
                $item['ic_no'],
                $item['interest_month'].'/'.$item['interest_year'],
                number_format($item['interest_rate_by_month'], 3),
                number_format($item['interest_amount'], 2),
                number_format($item['interest_total_amount'], 2),
                number_format($item['interest_banlance_amount'], 2),
                Carbon::parse($item['withdraw_date_planning'])->format('H:i d/m/Y')
            ];
        }

        $heading = [
            __('Số hợp đồng '),
            __('Họ và tên nhà đầu tư'),
            __('Số điện thoại'),
            __('CMND'),
            __('Lãi suất tháng'),
            __('Tỉ lệ lãi suất tháng (%)'),
            __('Giá trị lãi suất (Vnđ)'),
            __('Giá Trị Lãi Suất Tích Lũy (Vnđ)'),
            __('Giá Trị Lãi Suất Có Thể Rút (Vnđ)'),
            __('Ngày Rút Theo Kế Hoạch'),
        ];
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return \Maatwebsite\Excel\Facades\Excel::download(new ExportFile($heading,$data), 'list-contract-interest.xlsx');
    }


}