<?php


namespace Modules\Admin\Http\Controllers;


use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\Product\StoreRequest;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Repositories\CustomerContract\CustomerContractRepositoryInterface;
use Modules\Admin\Repositories\Product\ProductRepositoryInterface;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Maatwebsite\Excel\Excel;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\PDF;



class VsetStockController extends Controller
{
    protected $stockContract;
    protected $excel;
    protected $order;
    protected $staff;
    protected $stock;
    protected $customerContract;
    protected $customer;

    public function __construct(

        CustomerContractRepositoryInterface $customerContract,
        OrderRepositoryInterface $orders,
        StockRepositoryInterface $stock,
        CustomerRepositoryInterface $customer,
        ProductRepositoryInterface $product,
        Excel $excel,
        StaffsTable $staff

    )
    {

        $this->customerContract = $customerContract;
        $this->excel = $excel;
        $this->order = $orders;
        $this->staff = $staff;
        $this->stock=$stock;
        $this->customer = $customer;
        $this->product = $product;

    }
    protected function filters()
    {

        return [

        ];
    }

    public function indexAction()
  {
        
        $stock = $this->stock->getStockConfig();
        $stockFiles = $this->stock->getListStockFile();

        $list = [];     
    return view('admin::stock.index', [
            'LIST' => $list,
            'stock'=>$stock,
            'isEdit'=>false,
            'stockFiles'=>$stockFiles,
            'isStockInfo'=>true

        ]);
    }
    public function edit()
    {
        $filter = request()->all();
        $stock = $this->stock->getStockConfig();
        $stockFiles = $this->stock->getListStockFile();        
        $list = [];
        return view('admin::stock.edit', [
            'LIST' => $list,
            'stock'=>$stock,
            'stockFiles'=>$stockFiles,
            'isEdit'=>true,
            'isStockInfo'=>true

//

        ]);

    }
    public function uploadRelatedDoc(Request $request){

        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }
        /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . "_file." . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);
        return url('/').'/' . $this->transferTempfileToAdminfile($file_name, str_replace('', '', $file_name));
    }
     //Chuyển file từ folder temp sang folder chính
     private function transferTempfileToAdminfile($filename)
     {
         $old_path = TEMP_PATH . '/' . $filename;
         $new_path = STOCK_UPLOADS_PATH . date('Ymd') . '/' . $filename;
         Storage::disk('public')->makeDirectory(STOCK_UPLOADS_PATH . date('Ymd'));
         Storage::disk('public')->move($old_path, $new_path);
         return $new_path;
     }
     public function saveFile(Request $request){
        $param = $request->all();
        $arrName = json_decode($param['arrName']);
        if (!isset($param['arrFile']) || count($param['arrFile']) == 0) {
            return ['error' => 1, 'message' =>'Yêu cầu thêm tài liệu thành công'];
        }
        $data = [];
        
        $i = 0;
        foreach ($param['arrFile'] as $item) {
            $data[] = [
                'stock_config_id'=>$param['stock_config_id'],
                "file"=>$item,
                "display_name"=> $arrName[$i],
                "created_at"=>Carbon::now()->format("Y-m-d"),
                "created_by"=>Auth::id()
            ];
            $i++;
        }

        $this->stock->addStockFile($data);
        return ['error' => 0, 'message' =>'Thêm tài liệu liên quan thành công'];

    }
    public function saveStockInfo(Request $request){       
        $params = $request->all();        
        $stock_config_id = $params['stock_config_id'];
        $arrIdFiles = isset($params['arrIdFiles'])?$params['arrIdFiles']:[];
        $this->stock->removeStockFile($arrIdFiles);
        $this->stock->editStockConfig($params, $stock_config_id);
        return ['error'=>0, 'message'=>'Lưu thông tin thành công'];

    }
    public function editStock(Request $request){
        dd("helo");
        $params = $request->all();
        $stock_config_id = $params['stock_config_id'];
        $update = $this->stock->updateStock($stock_config_id,$params);
        return $update;

    }
    public function editStockConfig(Request $request){       
        $params = $request->all();       
        $stock_config_id = $params['stock_config_id'];  
        unset($params['stock_config_id']); 
        $this->stock->editStockConfig($params, $stock_config_id);
        return ['error'=>0, 'message'=>'Lưu thông tin thành công'];

    }
    public function removeStockFile(Request $request){
        $param = $request->all();
        $this->stock->removeStockFIle($param['arrIdFiles']);
        return ['error'=>0,'message'=>'Bạn đã xóa tài liệu thành công'];

    }

    // todo: Stock Publish------------------------------------------
    public function indexStockPublishAction(){        
        $filter = request()->all();
        $list = $this->stock->getListStockPublish([]);                               
        $stock = $this->stock->getLastStockPublish();
        return view('admin::stock.stock-publish.index', [
            'LIST'=>$list,
            'stock'=>$stock,
            'isEdit'=>false
        ]);
    }
    public function listHistoryPublish(Request $request){
        $filters = $request->all();        
        $list=$this->stock->getListStockPublish($filters);               
        return view('admin::stock.stock-publish.list', [
            'LIST' => $list,
            'stock'=>true
        ]);
    }
    public function getStockPublishEdit(){
        $stock = $this->stock->getLastStockPublish();        
        return view('admin::stock.stock-publish.edit', [
            'stock'=>$stock,
            'isEdit'=>false
            
        ]);
    }
    public function editStockPublish(Request $request){
        $data = $request->all();
        $this->stock->editStockPublish($data);
        return ['error'=>0, 'message'=>'Cập nhật thành công'];                                 

    }
    public function publishStock(Request $request){
        $params = $request->all();
        // dd("params la gi: ", $params);
        $params["is_active"] = isset($params["is_active"])?1:0;
        $this->stock->createStock($params);
        $this->stock->createStockPublishHistory();


        return ["error"=>0, "message"=>'Đã phát hành cổ phiếu thành công'];

    }
    
    //export Excel
    public function exportExcelAction(Request $request)
    {
        $filters = $request->all();//
        $heading = [
            'STT',
            'Số lượng hiện tài đang phát hành',
            'Số lượng phát hành',
            'Số lượng thực tế đã phát hành',
            'Giá phát hành',            
            'Đã bán',
            'Thành tiền',
            'Ngày Phát hành'           
        ];
        $list=$this->stock->getListStockPublish($filters);
        $data = [];
        foreach ($list as $key => $item) {
            $data [] = [
                $key+1,
                number_format($item['quantity']),
                number_format($item['quantity_publish']),
                number_format($item['quantity'] - $item['quantity_publish']),
                number_format($item['money']),                
                number_format($item["quantity_sale"]),                               
                number_format($item['quantity_sale']*$item['money']),
                Carbon::parse($item['created_at'])->format("d-m-Y")
               
            ];
        }
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
    //    return $this->excel->download(new ExportFile($heading, $data), 'test.xlsx');                
        return \Maatwebsite\Excel\Facades\Excel::download(new ExportFile($heading, $data),"Lich_su_phat_hanh_co_phieu.xlsx");
    }
    public function indexStockPublishBonusAction(){        
        $filter = request()->all();        
        $list = $this->stock->getListStockBonus([]);                                       
        return view('admin::stock.stock-publish.stock-bonus.index', [
            'LIST'=>$list,            
            'isEdit'=>false
        ]);
    }
    public function submitEditStockPublishBonus(Request $request){    
        $validator = Validator::make($request->all(), [
            'percent' => 'required|min:0|max:100',     
            
        ]);  
        if ($validator->fails()) {
            // Session::flash('error', $validator->messages()->first());
            return ['error'=>1, 'message'=>$validator->messages()->first()];
            // return redirect()->back()->withInput();
       }              
        $data = $request->all(); 
        $this->stock->editStockBonus($data); 
        return ['error'=>0,'message'=>'Cập nhật thành công'] ;
    }
    // !--------Stock Publish---------------------------------------


    // todo:Stock Cost-----------------------------------------------
    public function detailStockCostAction()
    {
        $stock = $this->stock->getStockConfig();
        // dd($stock);
        $list = [];
        return view('admin::stock.list-cost', [
            'LIST' => $list,
            'stock'=>$stock,
            'isEdit'=>false
        ]);
      }
      public function editStockCostAction()
      {
          $stock = $this->stock->getStockConfig();
          $list = [];
          return view('admin::stock.edit-cost', [
              'LIST' => $list,
              'stock'=>$stock,
              'isEdit'=>false
          ]);
        }
    // !-----------Stock Cost-------------------------------------------

//    todo: Stock Contract------------
    public function indexStockContractAction(){

        $list = $this->stock->listStockContract([]);
        return view('admin::stock-contract.index', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
        ]);
    }
    public function listStockContractAction(Request $request){
        $filters = $request->all();
        $list = $this->stock->listStockContract($filters);

        return view('admin::stock-contract.list', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
        ]);

}
    public function checkAddress($str){
        $sample = $str;
        $str = preg_replace('/\s/','',$str);
        if(substr_count($str,',')==2) return 'N/A';
        else if(substr_count($str,',')==1) return preg_replace('/,/','',$str);
        else return $sample;
    }
public function detailStockContractAction($id){
        $stockContract = $this->stock->detailStockContract($id);
        $customerId = $stockContract->customer_id;
    $customer = $this->customer->vSetGetDetailCustomer($customerId)['detail'];
//    todo: Lấy tên cổ phiếu
    $arr = explode("-", $stockContract['stock_contract_code']);
    $stock_config_code = $arr[0];
    $stockContract['stock_config_code'] = $stock_config_code;

    $disNameContact = $customer->lienhe_dis_type . " " .$customer->lienhe_dis_name;
    $proNameContact = $customer->lienhe_pro_type . " " . $customer->lienhe_pro_name;
    $disNameResidence = $customer->thuongtru_dis_type . " " . $customer->thuongtru_dis_name;
    $proNameResidence = $customer->thuongtru_pro_type . " " . $customer->thuongtru_pro_name;
    $customer->contact_address = $this->checkAddress($customer->contact_address . ', ' . $disNameContact . ', ' . $proNameContact);
    $customer->residence_address = $this->checkAddress($customer->residence_address . ', ' . $disNameResidence . ', ' . $proNameResidence);

    return view('admin::stock-contract.detail', [
        'FILTER' => $this->filters(),
        'customer'=>$customer,
        'item'=>$stockContract

    ]);
}
    public function editStockContractAction($id){
        $stockContract = $this->stock->detailStockContract($id);
        $customerId = $stockContract->customer_id;
        $customer = $this->customer->vSetGetDetailCustomer($customerId)['detail'];
//        todo: Lấy tên cổ phiếu
    $arr = explode("-", $stockContract['stock_contract_code']);
    $stock_config_code = $arr[0];
    $stockContract['stock_config_code'] = $stock_config_code;

        $disNameContact = $customer->lienhe_dis_type . " " .$customer->lienhe_dis_name;
        $proNameContact = $customer->lienhe_pro_type . " " . $customer->lienhe_pro_name;
        $disNameResidence = $customer->thuongtru_dis_type . " " . $customer->thuongtru_dis_name;
        $proNameResidence = $customer->thuongtru_pro_type . " " . $customer->thuongtru_pro_name;
        $customer->contact_address = $this->checkAddress($customer->contact_address . ',' . $disNameContact . ',' . $proNameContact);
        $customer->residence_address = $this->checkAddress($customer->residence_address . ', ' . $disNameResidence . ', ' . $proNameResidence);
        return view('admin::stock-contract.edit', [
            'FILTER' => $this->filters(),
            'customer'=>$customer,
            'item'=>$stockContract

        ]);
    }
public function listStockContractFileAction(Request  $request){
    $filter = $request->only(['page', 'display', 'customer_contract_id']);
    $list = $this->stock->listStockContractFile($filter);

    return view('admin::stock-contract.list-file',['listFile'=>$list['listStockContract']]);

//            $list = $this->stock->listStockContractFile([]);
//    return view('admin::stock-contract.list-file', [
//        'FILTER' => $this->filters(),
//        'listFile'=>$list
//    ]);
}
    public function listStockContractSubFileAction(Request $request){

        $param = $request->all();
//        dd($param);
        $list = $this->stock->listStockContractSubFile($param);


        return view('admin::stock-contract.list-sub-file', [
            'FILTER' => $this->filters(),
            'listSubFile'=>$list
        ]);
    }
    public function listStockContractDatatable(Request $request){
        $param = $request->all();
        $filter['stock_contract.customer_id'] = $request->customer_id;

        $filter['pagination'] = $param['pagination'];
//        $filter['search'] = $param['query']['search_history'];

        $list = $this->stock->listStockContractDatatable($filter);
        unset($filter['id']);

        return response()->json($list);
    }
    public function exportStockContractExcelAction(Request $request){
        $filters = $request->all();
        $heading = [
            'STT',
            'Mã hợp đồng',
            'Họ và tên nhà đầu tư',
            'Số điện thoại nhà đầu tư',
            'Địa chỉ liên hệ',
            'Số lượng cổ phiếu hiện tại',
            'Ngày tạo hợp đồng'
        ];
        $list=$this->stock->listStockContractExport($filters);
        $data = [];
        foreach ($list as $key => $item) {
            $data [] = [
                $key+1,
                $item['stock_contract_code'],
                $item['customer_name'],
                $item['customer_phone'],
                $item['customer_contact_address'],
                number_format($item['quantity']),
                $item['created_at']

            ];
        }
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        //    return $this->excel->download(new ExportFile($heading, $data), 'test.xlsx');
        return \Maatwebsite\Excel\Facades\Excel::download(new ExportFile($heading, $data),"Danh_sach_hop_dong_co_phieu.xlsx");
    }
//    -------------Upload File----------
    /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */


    public function uploadFileStockContract(Request $request){
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }
    public function submitEditStockContract(Request $request){
        $param = $request->all();

        if (!isset($param['arrFile']) || count($param['arrFile']) == 0) {
            return ['error' => 1, 'message' =>'Yêu cầu thêm file hợp đồng'];
        }
        $data = [];
        $positionFile = $this->stock->getLastPositionStockContractFile($param['stock_contract_id']);
        foreach ($param['arrFile'] as $item) {
            $positionFile++;
            $data[] = [
                'stock_contract_id'=>$param['stock_contract_id'],
                "image_file"=>$item,
                "created_at"=>Carbon::now()->format("Y-m-d"),
                "created_by"=>Auth::id(),
                "position_file"=>$positionFile
            ];
        }
        $this->stock->createStockContractFile($data);

        return ['error' => 0, 'message' =>'Cập nhật hợp đồng thành công'];
    }
//    todo: Stock BOnus Amount--------------
    public function indexBonusOrderAction(Request $request){
        $filter = $request->all();
        $items = $this->stock->listBonusAmount($filter);
        $stockBonusConfig = $this->stock->getStockBonusConfig();
        $lastBonus =$this->stock->getLastBonusAmount();
//        dd($stockBonusConfig);
        return view('admin::stock.bonus.order.index', [
            'LIST' => $items,
            'max'=>count($items),
            'stockBonusConfig'=>$stockBonusConfig,
            'isBonus'=>true,
            'lastBonus'=>$lastBonus



        ]);

    }
    public function removeBonusOrder(Request $req){
        $params = $req->all();
        $id = $params['stock_bonus_amount_id'];
        return $this->stock->removeBonusOrder($id);
    }
    //    end: Stock Bonus Amount------------------
//    todo: Stock Bonus Config---------------------------
    public function listBonusOrder(Request $request){
        $filter = $request->all();
        $items = $this->stock->listBonusAmount($filter);
        $lastBonus =$this->stock->getLastBonusAmount();

        return view('admin::stock.bonus.order.list', [
            'LIST' => $items,
            'page'=>$filter['page'],
            'lastBonus'=>$lastBonus


        ]);

    }
//    todo: Stock BOnus Payment------------------
    public function indexBonusPaymentAction(){
        $stockBonusPayment = $this->stock->detailStockBonusPayment();
        $stockBonusConfig = $this->stock->getStockBonusConfig();
        return view('admin::stock.bonus.payment.index', [
            'stockBonusPayment'=>$stockBonusPayment,
            'stockBonusConfig'=>$stockBonusConfig

        ]);
    }
    public function editBonusPaymentAction(){
        $stockBonusPayment = $this->stock->detailStockBonusPayment();
        $stockBonusConfig = $this->stock->getStockBonusConfig();
        return view('admin::stock.bonus.payment.edit', [
            'stockBonusPayment'=>$stockBonusPayment,
            'stockBonusConfig'=>$stockBonusConfig

]);

}
    public function editBonusPayment(Request $req){
        $data = $req->all();
        $dataConfig = ['stock_bonus_config_id'=>'stock_payment_method_bonus',
            'is_active'=>$data['stock_payment_method_bonus']
            ];
        $this->stock->editStockConfigBonus($dataConfig);
        return $this->stock->editBonusPayment($data);

    }

//    end: Stock Bonus Payment-----------------
//todo: Bonus Transfer-------------------------
    public function listBonusTransfer(Request $request){
        $filter = $request->all();
        $productList = $this->product->list($filter);
        $listCategory = $this->product->getCategoryProduct($filter);
        $stock = $this->stock->getStockConfig();
        return view('admin::stock.bonus.transfer.list', [
            'LIST' => $productList,
            'FILTER' => $this->filters(),
            'listCategory' => $listCategory,
            'stock' => $stock,
            'page'=>$filter['page']
        ]);

        $stockFiles = $this->stock->getListStockFile();

        $list = [];
        return view('admin::stock.bonus.transfer.index', [
            'LIST' => $list,
            'stock'=>$stock,
            'isEdit'=>false,
            'stockFiles'=>$stockFiles

        ]);
    }
    public function indexBonusTransferAction(){
        $productList = $this->product->list();
        $listCategory = $this->product->getCategoryProduct();
        $stock = $this->stock->getStockConfig();
        return view('admin::stock.bonus.transfer.index', [
            'LIST' => $productList,
            'FILTER' => $this->filters(),
            'listCategory' => $listCategory,
            'stock' => $stock
        ]);

        $stockFiles = $this->stock->getListStockFile();

        $list = [];
        return view('admin::stock.bonus.transfer.index', [
            'LIST' => $list,
            'stock'=>$stock,
            'isEdit'=>false,
            'stockFiles'=>$stockFiles

        ]);
    }
    public function editBonusTransfer(Request $req){
        $data = $req->all();
        $this->stock->editBonusTransfer($data);
        return ['error'=>false,'message'=>'Cập nhật thành công'];
    }
//    end: Bonus Transfer----------------------------
    public function addBonusOrder(Request $req){
        $params = $req->all();
        return $this->stock->addStockBonusAmount($params);
    }
    public function editBonusOrder(Request $req){
        $params = $req->all();
        return $this->stock->editStockBonusAmount($params);
    }
//    todo: Bonus Referral--------------------------------
    public function indexBonusReferralAction(Request $request){

        $stockBonusConfig = $this->stock->getStockBonusConfig();
        $stockFiles = $this->stock->getListStockFile();
        $filter = $request->all();
        $list = $this->stock->listBonusReferral($filter);
        $lastBonus = $this->stock->getlastBonusRefer();
        return view('admin::stock.bonus.referral.index', [
            'LIST' => $list,
            'stockBonusConfig'=>$stockBonusConfig,
            'lastBonus'=>$lastBonus

        ]);

    }
    public function listBonusReferral(Request $req){
        $filter= $req->all();
        $list = $this->stock->listBonusReferral($filter);
        $lastBonus = $this->stock->getlastBonusRefer();
        return view('admin::stock.bonus.referral.list', [
            'LIST' => $list,
            'page'=>$filter['page'],
            'lastBonus'=>$lastBonus

        ]);


    }
    public function addBonusReferral(Request $req){
        $data = $req->all();
        $this->stock->addBonusReferral($data);
        return ['error'=>false, 'message'=>'Thêm thành công'];
    }
    public function editBonusReferral(Request $req){
        $data = $req->all();
        try{
            $this->stock->editBonusReferral($data);
            return ['error'=>false,'message'=>'Cập nhật thành công'];
        }   catch(\Exception $ex){
            return ['error'=>true,'message'=>'Cập nhật thất bại'];
        }
    }
//    end: Stock Referal-------------------
    public function editStockConfigBonus(Request $req){
        $data = $req->all();
        return $this->stock->editStockConfigBonus($data);
    }
    public function removeReferral(Request $req){
        try{
            $data = $req->all();
            $this->stock->removeReferral($data);
            return ['error'=>true,'message'=>'Đã xóa thành công'];
        }
        catch (\Exception $ex){
            return ['error'=>true,'message'=>'Xóa thất bại'];
        }

    }


//    end: Stock Bonus Config--------------------------------
  



// end: Stock Contract----------------
}