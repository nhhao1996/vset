<?php


namespace Modules\Admin\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\StockNewsTable;
use Modules\Admin\Repositories\StockCategory\StockCategoryRepositoryInterface;
use Modules\Admin\Repositories\StockNews\StockNewsRepositoryInterface;

class StockNewsController extends Controller
{
    protected $stockNews;

    public function __construct(StockNewsRepositoryInterface $stockNews)
    {
        $this->stockNews = $stockNews;
    }

    public function index()
    {
        $filter = request()->all();
        $data = $this->stockNews->getListNew($filter);

        return view('admin::stock-news.index', [
            'LIST' => $data,
            'FILTER' => $filter,
        ]);
    }

    // FUNCTION  FILTER LIST ITEM
    protected function filters()
    {
    }

    /**
     * Danh sách hỗ trợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filters = $request->all();
        $data = $this->stockNews->getListNew($filters);
        return view('admin::stock-news.list', [
                'LIST' => $data,
                'FILTER' => $filters,
            ]
        );
    }

    public function create(){
        $listCategory = $this->stockNews->getListCategory();
        return view('admin::stock-news.create', [
            'listCategory' => $listCategory
        ]);
    }

    public function edit($id){
        $detail = $this->stockNews->getDetail($id);
        $listCategory = $this->stockNews->getListCategory();
        return view('admin::stock-news.edit', [
            'listCategory' => $listCategory,
            'detail' => $detail
        ]);
    }

    public function detail($id){
        $detail = $this->stockNews->getDetail($id);
        $listCategory = $this->stockNews->getListCategory();
        return view('admin::stock-news.detail', [
            'listCategory' => $listCategory,
            'detail' => $detail
        ]);
    }

    public function remove(Request $request){
        $param = $request->all();
        $remove = $this->stockNews->removeNews($param['stock_news_id']);
        return response()->json($remove);
    }

    public function upload(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }

    }

    public function uploadImage(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }

    }

    //Lưu file image vào folder temp
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() . date_format($time, 'd') . date_format($time, 'm') . date_format($time, 'Y') . "_blog." . $file->getClientOriginalExtension();
        $file->move(TEMP_PATH, $file_name);
        $file_name = asset($this->transferTempfileToAdminfile($file_name));
        return $file_name;
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = NEW_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(NEW_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    public function store(Request $request){
        $param = $request->all();
        $store = $this->stockNews->store($param);
        return response()->json($store);
    }

    public function update(Request $request){
        $param = $request->all();
        $store = $this->stockNews->update($param);
        return response()->json($store);
    }

}