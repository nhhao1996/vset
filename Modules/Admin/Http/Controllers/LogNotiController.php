<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\LogNoti\LogNotiRepositoryInterface;

class LogNotiController extends Controller
{
    protected $logNoti;

    public function __construct(LogNotiRepositoryInterface $logNoti)
    {
        $this->logNoti = $logNoti;
    }

    public function indexAction() {
        $logNoti = $this->logNoti->list();
        return view('admin::log-noti.index', [
            'LIST' => $logNoti,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $logNoti = $this->logNoti->list($filters);
        return view('admin::log-noti.list', ['LIST' => $logNoti,'page' => $filters['page']]);
    }

    public function showAction($id){
        $detail = $this->logNoti->getDetail($id);
        return view('admin::log-noti.detail', [
            'detail' => $detail,
        ]);
    }
}