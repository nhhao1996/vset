<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 11/2/2018
 * Time: 4:09 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\ExportFile;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\MemberLevelTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\CommissionLog\CommissionLogRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerGroup\CustomerGroupRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\CustomerSource\CustomerSourceRepositoryInterface;
use Modules\Admin\Repositories\District\DistrictRepositoryInterface;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PointHistory\PointHistoryRepoInterface;
use Modules\Admin\Repositories\PointRewardRule\PointRewardRuleRepositoryInterface;
use Modules\Admin\Repositories\Province\ProvinceRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use App\Exports\CustomerExport;
use App\Jobs\CheckMailJob;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Loyalty\LoyaltyRepositoryInterface;

class CustomerController extends Controller
{
    protected $customer;
    protected $customer_group;
    protected $customer_source;
    protected $code;
    protected $province;
    protected $district;
    protected $staff;
    protected $order;
    protected $customer_appointment;
    protected $appointment_service;
    protected $customer_service_card;
    protected $branch;
    protected $customer_branch_money;
    protected $customer_appointment_detail;
    protected $service_card_list;
    protected $smsLog;
    protected $order_detail;
    protected $service_card;
    protected $receipt;
    protected $customer_debt;
    protected $commission_log;
    protected $order_commission;
    protected $memberLevel;
    protected $pointHistory;
    protected $loyalty;
    protected $pointReward;
    protected $loyaltyApi;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customers
     */
    public function __construct
    (
        CustomerRepositoryInterface $customers,
        CustomerGroupRepositoryInterface $customer_groups,
        CustomerSourceRepositoryInterface $customer_sources,
        CodeGeneratorRepositoryInterface $codes, ProvinceRepositoryInterface $provinces,
        DistrictRepositoryInterface $districts, StaffRepositoryInterface $staffs,
        OrderRepositoryInterface $orders, OrderDetailRepositoryInterface $order_details,
        CustomerAppointmentRepositoryInterface $customer_appointments,
        AppointmentServiceRepositoryInterface $appointment_services,
        CustomerServiceCardRepositoryInterface $customer_sv_cards, BranchRepositoryInterface $branches,
        CustomerBranchMoneyRepositoryInterface $customer_branch_moneys,
        CustomerAppointmentDetailRepositoryInterface $customer_appointment_details,
        ServiceCardListRepositoryInterface $service_card_lists,
        SmsLogRepositoryInterface $smsLog,
        ServiceCardRepositoryInterface $service_card,
        ReceiptRepositoryInterface $receipt,
        CustomerDebtRepositoryInterface $customer_debt,
        CommissionLogRepositoryInterface $commission_log,
        OrderCommissionRepositoryInterface $order_commission,
        MemberLevelRepositoryInterface $memberLevel,
        PointHistoryRepoInterface $pointHistory,
        LoyaltyRepositoryInterface $loyalty,
        PointRewardRuleRepositoryInterface $pointReward,
        LoyaltyApi $loyaltyApi
    ) {
        $this->customer = $customers;
        $this->customer_group = $customer_groups;
        $this->customer_source = $customer_sources;
        $this->code = $codes;
        $this->province = $provinces;
        $this->district = $districts;
        $this->staff = $staffs;
        $this->order = $orders;
        $this->order_detail = $order_details;
        $this->customer_appointment = $customer_appointments;
        $this->appointment_service = $appointment_services;
        $this->customer_service_card = $customer_sv_cards;
        $this->branch = $branches;
        $this->customer_branch_money = $customer_branch_moneys;
        $this->customer_appointment_detail = $customer_appointment_details;
        $this->service_card_list = $service_card_lists;
        $this->smsLog = $smsLog;
        $this->service_card = $service_card;
        $this->receipt = $receipt;
        $this->customer_debt = $customer_debt;
        $this->commission_log = $commission_log;
        $this->order_commission = $order_commission;
        $this->memberLevel = $memberLevel;
        $this->pointHistory = $pointHistory;
        $this->loyalty = $loyalty;
        $this->pointReward = $pointReward;
        $this->loyaltyApi = $loyaltyApi;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAction()
    {
        $get = $this->customer->list();
//        print_r($get);die;
        return view('admin::customer.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
//        $optionGroup = $this->customer_group->getOption();
//        $group = (["" => __('Chọn nhóm')]) + $optionGroup;
        return [
            'customers$customer_group_id' => [
                'data' => []
            ],
//            'customers$gender' => [
//                'data' => [
//                    '' => 'Chọn giới tính',
//                    'male' => 'Nam',
//                    'female' => 'Nữ',
//                    'other' => 'Khác'
//                ]
//            ]
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
            'customers$customer_group_id', 'customers$gender', 'created_at', 'birthday', 'search']);
        $list = $this->customer->list($filter);
        return view('admin::customer.list', [
            'LIST' => $list,
            'page' => $filter['page']
        ]);
    }

    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
        $data['is_actived'] = ($change['action'] == 'unPublish') ? 1 : 0;
        $this->customer->edit($data, $change['id']);
        return response()->json([
            'status' => 0
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addAction()
    {
        $optionCustomerGroup = $this->customer_group->getOption();
        $optionCustomerSource = $this->customer_source->getOption();
        $code = $this->code->generateServiceCardCode("");
        $optionProvice = $this->province->getOptionProvince();
        return view('admin::customer.add', [
            'optionGroup' => $optionCustomerGroup,
            'optionSource' => $optionCustomerSource,
            'code' => $code,
            'optionProvince' => $optionProvice
        ]);
    }

    public function loadDistrictAction(Request $request)
    {
        $filters = request()->all();
        $district = $this->district->getOptionDistrict($filters);
        $data = [];
        foreach ($district as $key => $value) {
            $data[] = [
                'id' => $value['districtid'],
                'name' => $value['name'],
                'type' => $value['type']
            ];
        }
        return response()->json([
            'optionDistrict' => $data,
            'pagination' => $district->nextPageUrl() ? true : false
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitAddCustomerGroupAction(Request $request)
    {
        $group_name = $request->group_name;
        $test = $this->customer_group->testName($group_name, 0);
        if ($test['group_name'] == '') {
            $data = [
                'group_name' => $group_name,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id()
            ];
            //Insert customer group
            $groupId = $this->customer_group->add($data);
            //Update group_uuid
            $this->customer_group->edit([
                'group_uuid' => 'CUSTOMER_GROUP_' . date('dmY') . sprintf("%02d", $groupId)
            ], $groupId);

            $optionGroup = $this->customer_group->getOption();
            return response()->json([
                'status' => 1,
                'close' => $request->close,
                'optionGroup' => $optionGroup
            ]);
        } else {
            return response()->json([
                'status' => 0,
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitAddCustomerReferAction(Request $request)
    {
        $test_phone1 = $this->customer->testPhone($request->phone1, 0);
        if ($test_phone1 != '') {
            return response()->json([
                'phone_error' => 1,
                'message' => __('Số điện thoại đã tồn tại')
            ]);
        }
        $staff = $this->staff->getItem(Auth::id());
        $data = [
            'full_name' => $request->full_name,
            'customer_code' => 'cs' . $this->code->generateServiceCardCode(""),
            'phone1' => $request->phone1,
            'address' => $request->address,
            'gender' => 'other',
            'customer_source_id' => 1,
            'branch_id' => $staff['branch_id'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
            'is_actived' => 1
        ];

        $id_add = $this->customer->add($data);
        $day_code = date('dmY');
        if ($id_add < 10) {
            $id_add = '0' . $id_add;
        }
        $data_code = [
            'customer_code' => 'KH_' . $day_code . $id_add
        ];
        $this->customer->edit($data_code, $id_add);
        return response()->json([
            'status' => 1,
            'close' => $request->close
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchCustomerReferAction(Request $request)
    {
        $data = $request->all();
        if (isset($data['search'])) {
            $value = $this->customer->getCustomerSearch($data['search']);
            $search = [];
            foreach ($value as $item) {
                $search['results'][] = [
                    'id' => $item['customer_id'],
                    'text' => $item['full_name'] . " - " . $item['phone1']
                ];
            }
            return response()->json([
                'search' => $search,
                'pagination' => $value->nextPageUrl() ? true : false
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitAddAction(Request $request)
    {
        DB::beginTransaction();
        try {
            $phone1 = $request->phone1;
            $phone2 = $request->phone2;
            $staff = $this->staff->getItem(Auth::id());
            $data = [
                'customer_group_id' => $request->customer_group_id,
                'full_name' => $request->full_name,
//            'customer_code' => 'cs' . $this->code->generateServiceCardCode(""),
                'branch_id' => $staff['branch_id'],
//            'birthday' => $birthday,
                'gender' => 'other',
                'phone1' => $request->phone1,
                'phone2' => $request->phone2,
                'email' => $request->email,
                'facebook' => $request->facebook,
                'province_id' => $request->province_id,
                'district_id' => $request->district_id,
                'address' => $request->address,
                'customer_source_id' => 1,
                'customer_refer_id' => $request->customer_refer_id,
//            'customer_avatar'
                'note' => $request->note,
                'is_actived' => 1,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
                'postcode' => $request->postcode
            ];
            if ($request->year != null && $request->month != null && $request->day != null) {
                $birthday = $request->year . '-' . $request->month . '-' . $request->day;
                $data['birthday'] = $birthday;
                if ($birthday > date("Y-m-d")) {
                    return response()->json([
                        'error_birthday' => 1,
                        'message' => __('Ngày sinh không hợp lệ')
                    ]);
                }
            }

            if ($request->customer_source_id != null) {
                $data['customer_source_id'] = $request->customer_source_id;
            }
            if ($request->gender != null) {
                $data['gender'] = $request->gender;
            }
            $test_phone1 = $this->customer->testPhone($phone1, 0);
            $test_phone2 = $this->customer->testPhone($phone2, 0);
            if ($test_phone1 != "") {
                return response()->json([
                    'error_phone1' => 1,
                    'message' => __('Số điện thoại 1 đã tồn tại')
                ]);
            }
            if ($phone2 != '') {
                if ($test_phone2 != "") {
                    return response()->json([
                        'error_phone2' => 1,
                        'message' => __('Số điện thoại 2 đã tồn tại')
                    ]);
                }
            }

            if ($request->customer_avatar != null) {
                $data['customer_avatar'] = url('/').'/' .$this->transferTempfileToAdminfile($request->customer_avatar, str_replace('', '', $request->customer_avatar));
            }

            $id_add = $this->customer->add($data);
            $day_code = date('dmY');
            if ($id_add < 10) {
                $id_add = '0' . $id_add;
            }
            $data_code = [
                'customer_code' => 'KH_' . $day_code . $id_add
            ];
            $this->customer->edit($data_code, $id_add);
            //Config Rule Refer
            if ($request->customer_refer_id != null) {
                $configRefer = $this->pointReward->getRuleByCode('refer');
                if ($configRefer['is_actived'] == 1) {
                    //Plus Point Event Refer
                    $this->loyaltyApi->plusPointEvent(['customer_id' => $request->customer_refer_id, 'rule_code' => 'refer', 'object_id' => '']);
                }
            }
            CheckMailJob::dispatch('is_event', 'new_customer', $id_add);
            $this->smsLog->getList('new_customer', $id_add);
            DB::commit();
            //Send notification KH mới
            $mNoti = new SendNotificationApi();
            $mNoti->sendNotification([
                'key' => 'customer_W',
                'customer_id' => $id_add,
                'object_id' => ''
            ]);

            return response()->json([
                'success' => 1,
                'message' => __('Thêm khách hàng thành công')
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    //function upload image
    public function uploadAction(Request $request)
    {
        $this->validate($request, [
            "customer_avatar" => "mimes:jpg,jpeg,png,gif|max:10000"
        ], [
            "customer_avatar.mimes" => __('File này không phải file hình'),
            "customer_avatar.max" => __('File quá lớn')
        ]);
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }

    //Lưu file image vào folder temp
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() . date_format($time, 'd') . date_format($time, 'm') . date_format($time, 'Y') . "_customer." . $file->getClientOriginalExtension();
        $file->move(TEMP_PATH, $file_name);
        return $file_name;

    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = CUSTOMER_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(CUSTOMER_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    //function delete image
    public function deleteTempFileAction(Request $request)
    {

        Storage::disk("public")->delete(TEMP_PATH . '/' . $request->input('filename'));
        return response()->json(['success' => '1']);
    }

    public function detailAction($id)
    {
        $staff = $this->staff->getItem(Auth::id());
        $getItem = $this->customer->getItem($id);
        $getItemRefer = $this->customer->getItemRefer($id);
        $detail_order = $this->order->detailCustomer($id);

        $appointment = $this->customer_appointment->detailDayCustomer($id);
        $list_card = $this->customer_service_card->memberCardDetail($id, $staff['branch_id']);
        $list_commission = $this->order_commission->getCommissionByCustomer($id);
        $list_commission_log = $this->commission_log->getLogByCustomer($id);

        $stockCustomer = $this->customer->getStockByCustomer($id);        

        $arr_order = [];
        $day_appointment = [];
        $arr_appointment = [];
        $arr_app_sv = [];
        $arr_sv_card = [];
        $arr_sv_card_detail = [];
        $arr_debt = [];

        if (count($detail_order) > 0) {
            foreach ($detail_order as $key => $value) {
                $arr_order[] = [
                    'order_id' => $value['order_id'],
                    'order_code' => $value['order_code'],
                    'amount' => $value['amount'],
                    'status' => $value['process_status'],
                    'branch_name' => $value['branch_name'],
                    'created_at' => date("d/m/Y", strtotime($value['created_at'])),
                    'object_name' => $this->order_detail->getItem($value['order_id']),
                    'note' => $value['note'],
                    'order_description' => $value['order_description']
                ];

            }
        }
        if (count($appointment) > 0) {
            foreach ($appointment as $i) {
                $day_appointment[] = $i['date'];
                $detail_appointment = $this->customer_appointment->detailCustomer($i['date'], $id);
                foreach ($detail_appointment as $k => $v) {
                    $arr_appointment[] = [
                        'customer_appointment_id' => $v['customer_appointment_id'],
                        'date' => date("d/m/Y", strtotime($v['date'])),
                        'time' => date("H:i", strtotime($v['time'])),
                        'status' => $v['status'],
                        'customer_quantity' => $v['customer_quantity']
                    ];
                    $app_sv = $this->customer_appointment_detail->getItem($v['customer_appointment_id']);
                    foreach ($app_sv as $itemSV) {
                        if ($itemSV['service_id'] != null)
                            $arr_app_sv[] = [
                                'service_name' => $itemSV['service_name'],
                                'customer_appointment_id' => $itemSV['customer_appointment_id']
                            ];
                    }

                }

            }
        }
        if (count($list_card) > 0) {
            foreach ($list_card as $kc => $vc) {
                $arr_sv_card[] = [
                    'card_name' => $vc['name'],
                    'is_actived' => $vc['is_actived'],
                    'card_code' => $vc['code'],
                    'price' => $vc['price'],

                ];
            }
        }

        //Kiểm tra tiền trong tài khoản
        $check_money = $this->customer_branch_money->getPriceBranch($id, $staff['branch_id']);
        if (isset($check_money)) {
            $customer_money = $check_money['balance'];
            $commission_money = $check_money['commission_money'];
        } else {
            $customer_money = 0;
            $commission_money = 0;
        }
        //Kiểm tra công nợ
        $amountDebt = $this->customer_debt->getItemDebt($id);

        $debt = 0;
        if (count($amountDebt) > 0) {
            foreach ($amountDebt as $item) {
                $debt += $item['amount'] - $item['amount_paid'];

                $arr_debt[] = [
                    'customer_debt_id' => $item['customer_debt_id'],
                    'order_code' => $item['order_code'],
                    'amount' => $item['amount'],
                    'amount_paid' => $item['amount_paid'],
                    'full_name' => $item['full_name'],
                    'created_at' => $item['created_at'],
                    'status' => $item['status']
                ];
            }
        }
        if ($getItem != null) {
            if ($getItem['member_level_id'] != 4) {
                $memberLevel = $this->memberLevel->getItem((int)($getItem['member_level_id']) + 1);
            } else {
                $memberLevel = $this->memberLevel->getItem(4);
            }
            $pointHistory = $this->pointHistory->getHistory($id);

            return view('admin::customer.detail', [
                'item' => $getItem,
                'itemRefer' => $getItemRefer,
                'arrOrder' => $arr_order,
                'dayAppointment' => $day_appointment,
                'arrAppointment' => $arr_appointment,
                'arrAppSv' => $arr_app_sv,
                'arrSvCard' => $arr_sv_card,
                'customer_money' => $customer_money,
                'amountDebt' => $debt,
                'arr_debt' => $arr_debt,
                'commission_money' => $commission_money,
                'list_commission' => $list_commission,
                'list_log' => $list_commission_log,
                'memberLevel' => $memberLevel,
                'pointHistory' => $pointHistory,
            ]);
        } else {
            return redirect()->route('admin.customer');
        }
    }

    public function editAction($id)
    {
        $getItem = $this->customer->getItem($id);
        if ($getItem['birthday'] != null) {
            $birthday = explode('/', date("d/m/Y", strtotime($getItem['birthday'])));
            $day = $birthday[0];
            $month = $birthday[1];
            $year = $birthday[2];
        } else {
            $day = null;
            $month = null;
            $year = null;
        }
        $getItemRefer = $this->customer->getItemRefer($id);
        $optionCustomerGroup = $this->customer_group->getOption();
        $optionCustomerSource = $this->customer_source->getOption();
        $optionProvince = $this->province->getOptionProvince();
        if ($getItem != null) {
            return view('admin::customer.edit', [
                'item' => $getItem,
                'optionGroup' => $optionCustomerGroup,
                'optionSource' => $optionCustomerSource,
                'getRefer' => $getItemRefer,
                'optionProvince' => $optionProvince,
                'day' => $day,
                'month' => $month,
                'year' => $year
            ]);
        } else {
            return redirect()->route('admin.customer');
        }
    }

    public function loadBirthdayAction(Request $request)
    {
        $id = $request->id;
        $getItem = $this->customer->getItem($id);

        $birthday = explode('-', date('Y-m-d', strtotime($getItem['birthday'])));
        return response()->json([
            'day' => $birthday[2],
            'month' => $birthday[1],
            'year' => $birthday[0]
        ]);
    }

    public function submitEditAction(Request $request)
    {
        $id = $request->id;
        $phone1 = $request->phone1;
        $phone2 = $request->phone2;
        $data = [
            'customer_group_id' => $request->customer_group_id,
            'full_name' => $request->full_name,
            'gender' => $request->gender,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'email' => $request->bank_name,
            'bank_account_name'=> $request->phone2,
            'facebook' => $request->facebook,
            'province_id' => $request->province_id,
            'district_id' => $request->district_id,
            'address' => $request->address,
            'customer_source_id' => $request->customer_source_id,
            'customer_refer_id' => $request->customer_refer_id,
//            'customer_avatar'
            'note' => $request->note,
            'is_actived' => $request->is_actived,
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
            'postcode' => $request->postcode
        ];
        if ($request->year != null && $request->month != null && $request->day != null) {
            if ($request->month < 10) {
                $month = '0' . $request->month;
            } else {
                $month = $request->month;
            }
            $birthday = $request->year . '-' . $month . '-' . $request->day;
            $data['birthday'] = $birthday;
            if ($birthday > date("Y-m-d")) {
                return response()->json([
                    'error_birthday' => 1,
                    'message' => __('Ngày sinh không hợp lệ')
                ]);
            }
        }

        $test_phone1 = $this->customer->testPhone($phone1, $id);
        $test_phone2 = $this->customer->testPhone($phone2, $id);
        if ($test_phone1 != "") {
            return response()->json([
                'error_phone1' => 1,
                'message' => __('Số điện thoại 1 đã tồn tại')
            ]);
        }
        if ($phone2 != '') {
            if ($test_phone2 != "") {
                return response()->json([
                    'error_phone2' => 1,
                    'message' => __('Số điện thoại 2 đã tồn tại')
                ]);
            }
        }

        if ($request->customer_avatar_upload != null) {
            $data['customer_avatar'] = url('/').'/' .$this->transferTempfileToAdminfile($request->customer_avatar_upload, str_replace('', '', $request->customer_avatar_upload));
        } else {
            $data['customer_avatar'] = $request->customer_avatar;
        }


        $this->customer->edit($data, $id);
        return response()->json([
            'success' => 1,
            'message' => __('Cập nhật khách hàng thành công')
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeAction($id)
    {
        $this->customer->remove($id);
        return response()->json([
            'error' => 0,
            'message' => 'Remove success'
        ]);
    }

    public function checkCardAction(Request $request)
    {
        $day_active = date('d/m/Y');
        $code = $request->card;
        $staff_branch = $this->staff->getItem(Auth::id());
        $list_card = $this->service_card_list->searchActiveCard($code, $staff_branch['branch_id']);
        $data_card = [];
        if ($list_card != null) {
            if ($list_card['date_using'] != 0) {
                $date_now_int = strftime("%d/%m/%Y", strtotime(date("Y-m-d", strtotime(date("Y-m-d") . '+ ' . $list_card['date_using'] . 'days'))));
            } else {
                $date_now_int = 0;
            }

            $data_card = [
                'card_code' => $list_card['code'],
                'service_card_list_id' => $list_card['service_card_list_id'],
                'name_code' => $list_card['name_code'],
                'card_type' => $list_card['card_type'],
                'money' => $list_card['money'],
                'name_sv' => $list_card['name_sv'],
                'expired_day' => $date_now_int,
                'number_using' => $list_card['number_using'],
                'service_card_id' => $list_card['service_card_id']
            ];

        }
        return response()->json([
            'data_card' => $data_card,
            'day' => $day_active
        ]);

    }

    public function submitActiveCardAction(Request $request)
    {
        $staff = $this->staff->getItem(Auth::id());
        $table_card = $request->table_card;
        $customer_id = $request->customer_id;
        if ($table_card != null) {
            $aData = array_chunk($table_card, 8, false);
            foreach ($aData as $key => $value) {
                $data = [
                    'service_card_id' => $value[6],
                    'card_code' => $value[0],
                    'customer_id' => $customer_id,
                    'actived_date' => Carbon::createFromFormat('d/m/Y', $value[1])->format('Y-m-d'),
                    'updated_by' => Auth::id(),
                    'is_actived' => 1,
                    'branch_id' => $staff['branch_id'],
                    'number_using' => $value[5],
                    'count_using' => 0,
                    'created_by' => Auth::id()
                ];
                if ($value[2] != 0) {
                    $data['expired_date'] = Carbon::createFromFormat('d/m/Y', $value[2])->format('Y-m-d');
                }
                if ($value[3] == 'money') {
                    $data['count_using'] = 1;
                    $list_customer = $this->customer->getItem($customer_id);
                    $data_cus = [
                        'account_money' => $list_customer['account_money'] + $value[4]
                    ];
                    $this->customer->edit($data_cus, $customer_id);
                    $data['number_using'] = 1;
                    $data['count_using'] = 1;
                }
                //Thêm thẻ dv đã active
                $this->customer_service_card->add($data);
                $data_active = [
                    'is_actived' => 1,
                    'updated_by' => Auth::id(),
                    'actived_at' => date('Y-m-d')
                ];
                //Update active trong service card list thành 1
                $this->service_card_list->edit($data_active, $value[7]);
                //Get giá tiền chi nhánh
                $customer_branch = $this->customer_branch_money->getPriceBranch($request->customer_id, $staff['branch_id']);
                if ($customer_branch != null) {
                    $data_money_edit = [
                        'total_money' => $customer_branch['total_money'] + $value[4],
                        'balance' => $customer_branch['total_money'] + $value[4] - $customer_branch['total_using'],
                        'updated_by' => Auth::id()
                    ];
                    $this->customer_branch_money->edit($data_money_edit, $request->customer_id, $staff['branch_id']);

                } else {
                    $data_money = [
                        'total_money' => $value[4],
                        'total_using' => 0,
                        'balance' => $value[4],
                        'customer_id' => $request->customer_id,
                        'branch_id' => $staff['branch_id'],
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id()
                    ];

                    $this->customer_branch_money->add($data_money);
                }
            }
            return response()->json([
                'success' => 1,
                'message' => __('Active thẻ dịch vụ thành công')
            ]);
        } else {
            return response()->json([
                'card_null' => 1,
                'message' => __('Thẻ dịch vụ rỗng')
            ]);
        }

    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcelAction()
    {

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new CustomerExport(), 'customer.xlsx');
    }

    /**
     * Import danh sách khách hàng
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function importExcelAction(Request $request)
    {
        $file = $request->file('file');

        if (isset($file)) {
            $typeFileExcel = $file->getClientOriginalExtension();
            if ($typeFileExcel == "xlsx") {
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($file);

                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if ($key == 1) {

                        } elseif ($key != 1 && $row[0] != '' && $row[1] != '') {
//                            $data = [
//                                'name' => $row[0],
//                                'provinceid' => $row[1],
//                                'location' => $row[4] . ' , ' . $row[5],
//                                'postcode' => $row[2],
//                                'type' => $row[3],
//                                'local_government_area' => $row[6]
//                            ];
//                            DB::table('district')->insert($data);
                            //check sdt dưới 10 số thì loại
                            if (strlen($row[5]) >= 10) {
                                //format phone
                                $change_array = [
                                    '016' => '03',
                                    '0120' => '070',
                                    '0121' => '079',
                                    '0122' => '077',
                                    '0126' => '076',
                                    '0128' => '078',
                                    '0123' => '083',
                                    '0124' => '084',
                                    '0125' => '085',
                                    '0127' => '081',
                                    '0129' => '082',
                                    '0188' => '058',
                                    '0186' => '056',
                                    '0199' => '059'
                                ];
                                $phone = $row[5];
                                foreach ($change_array as $key => $value) {
                                    if (strstr($phone, $key)) {
                                        $phone = str_replace($key, $value, $phone);
                                    }
                                }

                                //check sdt tồn tại
                                $check_phone = $this->customer->getCusPhone($phone);

                                if (!isset($check_phone)) {
                                    //check customer group
                                    $check_customer_group = $this->customer_group->testGroupName(str_slug($row[4]));
                                    if (isset($check_customer_group)) {
                                        $id_group = $check_customer_group['customer_group_id'];
                                    } else {
                                        $id_group = $this->customer_group->add([
                                            'group_name' => $row[4],
                                            'created_by' => Auth::id(),
                                            'updated_by' => Auth::id(),
                                            'slug' => str_slug($row[4])
                                        ]);
                                    }
                                    $gender = '';
                                    if ($row[3] == 1) {
                                        $gender = 'male';
                                    } else if ($row[3] == 0) {
                                        $gender = 'female';
                                    }
                                    if ($row[6] != '') {
                                        $birthday = Carbon::createFromFormat('d/m/Y', $row[6])->format('Y-m-d');
                                    } else {
                                        $birthday = null;
                                    }
                                    if ($row[1] != '') {
                                        $created_at = Carbon::createFromFormat('d/m/Y', $row[1])->format('Y-m-d');
                                    } else {
                                        $created_at = date('Y-m-d H:i');
                                    }
                                    if (isset($row[7]) &&  $row['7'] != '') {
                                        $address = strip_tags($row[7]);
                                    } else {
                                        $address = '';
                                    }
                                    if (isset($row[8]) && $row[8] != '') {
                                        $note = strip_tags($row[8]);
                                    } else {
                                        $note = '';
                                    }
                                    $arrayCustomer = [
                                        'full_name' => $row[2],
                                        'branch_id' => Auth::user()->branch_id,
                                        'phone1' => $phone,
                                        'gender' => $gender,
                                        'customer_group_id' => $id_group,
                                        'birthday' => $birthday,
                                        'address' => $address,
                                        'note' => $note,
                                        'is_actived' => 1,
                                        'created_at' => $created_at,
                                        'updated_at' => date('Y-m-d'),
                                        'created_by' => Auth::id(),
                                        'updated_by' => Auth::id(),
                                    ];
                                    //Thêm khách hàng
                                    $id_add = $this->customer->add($arrayCustomer);

                                    $day_code = date('dmY');
                                    if ($id_add < 10) {
                                        $id_add = '0' . $id_add;
                                    }
                                    $data_code = [
                                        'customer_code' => 'KH_' . $day_code . $id_add
                                    ];

                                    //Cập nhật mã khách hàng
                                    $this->customer->edit($data_code, $id_add);

                                } else {
                                    //check customer group
                                    $check_customer_group = $this->customer_group->testGroupName(str_slug($row[4]));
                                    if (isset($check_customer_group)) {
                                        $id_group = $check_customer_group['customer_group_id'];
                                    } else {
                                        $id_group = $this->customer_group->add([
                                            'group_name' => $row[4],
                                            'created_by' => Auth::id(),
                                            'updated_by' => Auth::id(),
                                            'slug' => str_slug($row[4])
                                        ]);
                                    }
                                    $gender = '';
                                    if ($row[3] == 1) {
                                        $gender = 'male';
                                    } else if ($row[3] == 0) {
                                        $gender = 'female';
                                    }
                                    if ($row[6] != '') {
                                        $birthday = Carbon::createFromFormat('d/m/Y', $row[6])->format('Y-m-d');
                                    } else {
                                        $birthday = null;
                                    }
                                    if ($row[1] != '') {
                                        $created_at = Carbon::createFromFormat('d/m/Y', $row[1])->format('Y-m-d');
                                    } else {
                                        $created_at = date('Y-m-d H:i');
                                    }
                                    if (isset($row[7]) &&  $row['7'] != '') {
                                        $address = strip_tags($row[7]);
                                    } else {
                                        $address = '';
                                    }
                                    if (isset($row[8]) && $row[8] != '') {
                                        $note = strip_tags($row[8]);
                                    } else {
                                        $note = '';
                                    }
                                    $arrayCustomer = [
                                        'full_name' => $row[2],
//                                        'branch_id' => Auth::user()->staff_id,
//                                        'phone1' => $phone,
                                        'gender' => $gender,
                                        'customer_group_id' => $id_group,
                                        'birthday' => $birthday,
                                        'address' => $address,
                                        'note' => $note,
                                        'created_at' => $created_at,
//                                        'updated_at' => date('Y-m-d'),
                                        'updated_by' => Auth::id(),
                                        'is_actived' => 1,
                                        'is_deleted' => 0
                                    ];
                                    $this->customer->edit($arrayCustomer, $check_phone['customer_id']);
                                }
                            }
                        }
                    }
                }
                $reader->close();
            }
            return response()->json([
                'success' => 1,
                'message' => __('Import thông tin khách hàng thành công')
            ]);
        }
    }

    /**
     *  Modal form thẻ liệu trình
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function formProcessCardAction(Request $request)
    {
        $optionServiceCard = $this->service_card->getOption();

        $view = \View::make('admin::customer.pop.modal-process-card', [
            'customer_id' => $request->id,
            'optionServiceCard' => $optionServiceCard
        ])->render();
        return response()->json([
            'url' => $view
        ]);
    }

    /**
     *  Chọn dịch vụ load ngày kích hoạt
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function chooseServiceCardAction(Request $request)
    {
        $service_card = $this->service_card->getItemDetail($request->service_card_id);
        return response()->json([
            'service_card' => $service_card
        ]);
    }

    /**
     * Chọn ngày kích hoạt load ngày hết hạn
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeActiveDateAction(Request $request)
    {
        $service_card = $this->service_card->getItemDetail($request->service_card_id);
        $format_active_date = Carbon::createFromFormat('d/m/Y', $request->actived_date)->format('Y-m-d');
        if ($service_card['date_using'] != 0) {
            $date = Carbon::parse($format_active_date)->addDay($service_card['date_using']);
            $expired_date = date_format($date, "d/m/Y");
        } else {
            $expired_date = 0;
        }
        return response()->json([
            'expired_date' => $expired_date
        ]);
    }

    /**
     * Tạo thẻ liệu trình
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitProcessCard(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'service_card_id' => 'required',
            'actived_date' => 'required',
            'expired_date' => 'required',
            'count_using' => 'required|numeric',
            'end_using' => 'required|numeric'
        ], [
            'service_card_id.required' => __('Hãy chọn thẻ dịch vụ'),
            'actived_date.required' => __('Hãy chọn ngày kích hoạt'),
            'expired_date.required' => __('Hãy chọn ngày hết hạn'),
            'count_using.required' => __('Hãy nhập số lần sử dụng'),
            'count_using.numeric' => __('Số lần sử dụng không hợp lệ'),
            'end_using.required' => __('Hãy nhập số lần còn lại'),
            'end_using.numeric' => __('Số lần còn lại không hợp lệ')
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                '_error' => $validator->errors()->all(),
                'message' => __('Thêm thất bại')
            ]);
        } else {
            $actived_date = Carbon::createFromFormat('d/m/Y', $request->actived_date)->format('Y-m-d');
            $expired_date = Carbon::createFromFormat('d/m/Y', $request->expired_date)->format('Y-m-d');

            if ($actived_date > date('Y-m-d')) {
                return response()->json([
                    'error' => true,
                    'message' => __('Ngày kích hoạt vượt quá ngày hiện tại')
                ]);
            }

            if ($actived_date > $expired_date) {
                return response()->json([
                    'error' => true,
                    'message' => __('Ngày kích hoạt không hợp lệ')
                ]);
            }

            $service_card = $this->service_card->getItemDetail($request->service_card_id);
            $code = $this->code->generateCardListCode();

            $arr_card_list = [
                'branch_id' => Auth::user()->branch_id,
                'service_card_id' => $request->service_card_id,
                'code' => $code,
                'order_code' => 'THEMBANGTAY',
                'price' => $service_card['price'],
                'is_actived' => 1
            ];

            //insert service card list
            $this->service_card_list->add($arr_card_list);

            $arr_customer_card = [
                'customer_id' => $request->customer_id,
                'branch_id' => Auth::user()->branch_id,
                'card_code' => $code,
                'service_card_id' => $request->service_card_id,
                'actived_date' => $actived_date,
                'expired_date' => $expired_date,
                'number_using' => intval($request->count_using) + intval($request->end_using),
                'count_using' => $request->count_using
            ];
            if ($service_card['service_card_type'] == 'money') {
                $arr_customer_card['money'] = $service_card['money'];

                $customer = $this->customer->getItem($request->customer_id);
                $data = [
                    'account_money' => $customer['account_money'] + $service_card['money']
                ];
                //Cập nhật tiền trong tài khoản khách hàng
                $this->customer->edit($data, $request->customer_id);

                //Check tài khoản KH theo chi nhánh
                $customer_branch = $this->customer_branch_money->getPriceBranch($request->customer_id, Auth::user()->branch_id);
                if ($customer_branch != null) {
                    $data_money_edit = [
                        'total_money' => $customer_branch['total_money'] + $service_card['money'],
                        'balance' => $customer_branch['total_money'] + $service_card['money'] - $customer_branch['total_using'],
                        'updated_by' => Auth::id()
                    ];
                    $this->customer_branch_money->edit($data_money_edit, $request->customer_id, Auth::user()->branch_id);
                } else {
                    $data_money = [
                        'total_money' => $service_card['money'],
                        'total_using' => 0,
                        'balance' => $service_card['money'],
                        'customer_id' => $request->customer_id,
                        'branch_id' => Auth::user()->branch_id,
                        'created_by' => Auth::id(),
                        'updated_by' => Auth::id()
                    ];
                    $this->customer_branch_money->add($data_money);
                }
            }
            //insert customer service card
            $this->customer_service_card->add($arr_customer_card);
            return response()->json([
                'error' => false,
                'message' => __('Tạo thẻ liệu trình thành công')
            ]);
        }
    }

    /**
     * Nhập công nợ ban đầu
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function enterDebtAction(Request $request)
    {
        $amountDebt = $request->amountDebt;
        $idCustomer = $request->idCustomer;
        $data = [
            'customer_id' => $idCustomer,
            'status' => 'unpaid',
            'amount' => str_replace(',', '', $amountDebt),
            'amount_paid' => 0,
            'order_id' => 0,
            'staff_id' => Auth::id(),
            'debt_type' => 'first',
            'created_by' => Auth::id(),
            'note' => strip_tags($request->note),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::id()
        ];
        $debt_id = $this->customer_debt->add($data);
        //update debt code
        $day_code = date('dmY');
        if ($debt_id < 10) {
            $debt_id = '0' . $debt_id;
        }
        $debt_code = [
            'debt_code' => 'CN_' . $day_code . $debt_id
        ];
        $this->customer_debt->edit($debt_code, $debt_id);

        if ($debt_id) {
            return response()->json([
                'error' => false,
                'message' => __('Nhập công nợ thành công.')
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => __('Nhập công nợ thất bại.')
            ]);
        }

    }

    /**
     * Form quy đổi tiền hoa hồng
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commissionAction(Request $request)
    {
        $view = \View::make('admin::customer.pop.modal-commission', [
            'customer_id' => $request->customer_id,
            'commission_money' => $request->commission_money
        ])->render();
        return response()->json([
            'url' => $view
        ]);
    }

    public function submitCommissionAction(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'money' => 'required',
            'type' => 'required'
        ], [
            'money.required' => __('Hãy nhập tiền quy đổi'),
            'type.required' => __('Hãy chọn hình thức quy đổi'),
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                '_error' => $validator->errors()->all(),
                'message' => __('Quy đổi thất bại')
            ]);
        } else {
            $money = str_replace(",", "", $request->money);
            $get_customer_money = $this->customer_branch_money
                ->getPriceBranch($request->customer_id, Auth::user()->branch_id);
            if ($money > $get_customer_money['commission_money']) {
                return response()->json([
                    'error' => true,
                    '_error' => __('Tiền quy đổi lớn hơn tiền còn lại'),
                    'message' => __('Quy đổi thất bại')
                ]);
            }
            $data_customer_money = [
                'commission_money' => intval($get_customer_money['commission_money']) - intval($money)
            ];
            if ($request->type == 'tranfer_money') {
                $data_customer_money['total_money'] = intval($get_customer_money['total_money']) + intval($money);
                $data_customer_money['balance'] = intval($get_customer_money['balance']) + intval($money);
            }
            //update commission money
            $this->customer_branch_money->edit($data_customer_money, $request->customer_id, Auth::user()->branch_id);
            //insert commission log
            $data_log = [
                'customer_id' => $request->customer_id,
                'branch_id' => Auth::user()->branch_id,
                'money' => $money,
                'type' => $request->type,
                'note' => $request->note,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id()
            ];
            $this->commission_log->add($data_log);

            return response()->json([
                'error' => false,
                'message' => __('Quy đổi thành công')
            ]);
        }
    }

    public function getInfoCustomerDetail(Request $request)
    {
        $id = $request->id;
        $getItem = $this->customer->getItem($id);
        return response()->json($getItem);
    }

    /**
     * Cộng điểm cho sự kiện sinh nhật KH
     * @return \Illuminate\Http\JsonResponse
     */
    public function loyaltyEventBirthday()
    {
        DB::beginTransaction();
        try {
            //Cộng điểm loyalty
            $listCustomer = $this->customer->getBirthdays();
            if (count($listCustomer) > 0) {
                foreach ($listCustomer as $item) {
                    $this->loyalty->plusPointEvent(
                        [
                            'customer_id' => $item['customer_id'],
                            'rule_code' => 'birthday',
                            'object_id' => ''
                        ]
                    );
                }
            }
            DB::commit();
            return response()->json([
                'error' => false,
                'message' => 'Success'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function exportExcelAll()
    {
        $arr_customer = [];
        //Danh sách khách hàng
        $list_customer = $this->customer->getAllCustomer();
        foreach ($list_customer as $item) {
            $gender = '';
            if ($item['gender'] == 'other') {
                $gender = 'Khác';
            } elseif ($item['gender'] == 'male') {
                $gender = 'Nam';
            } elseif ($item['gender'] == 'female') {
                $gender = 'Nữ';
            }
            $arr_customer [] = [
                'customer_id' => $item['customer_id'],
                'name' => $item['full_name'],
                'phone' => $item['phone1'],
                'gender' => $gender,
                'service_name' => $this->customer_service_card->getCustomerCardAll($item['customer_id'])
            ];
        }
        //Data export
        $arr_data = [];
        foreach ($arr_customer as $key => $item) {
            $card = '';
            if (count($item['service_name']) > 0) {
                foreach ($item['service_name'] as $key1 => $item1) {
                    if ($key1 > 0) {
                        $card .= ' , ' . $item1['card_name'];
                    } else {
                        $card .= $item1['card_name'];
                    }
                }
            }
            $arr_data [] = [
                $key+1,
                'name' => $item['name'],
                'phone' => $item['phone'],
                'card' => $card
            ];
        }
        $heading = [
            __('STT'),
            __('Họ & Tên'),
            __('Số điện thoại'),
            __('Thẻ dịch vụ')
        ];
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new ExportFile($heading, $arr_data), 'customer-all.xlsx');
    }

    /**
     * lay thong tin khach hang va dia chi mac dinh (neu co)
     * @param Request $request
     */
    public function getCustomerAndDefaultContact(Request $request)
    {
        $idCus = $request->id;
        return $this->customer->getCustomerAndDefaultContact($idCus);
    }
}