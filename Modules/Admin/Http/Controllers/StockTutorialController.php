<?php


namespace Modules\Admin\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Repositories\StockTutorial\StockTutorialRepositoryInterface;

class StockTutorialController extends Controller
{
    protected $stockTutorial;

    public function __construct(StockTutorialRepositoryInterface $stockTutorial)
    {
        $this->stockTutorial = $stockTutorial;
    }

    public function index(){
        $detail = $this->stockTutorial->getDetail('tutorial');
        return view('admin::stock-tutorial.index', [
            'detail' => $detail
        ]);
    }

    public function edit(){
        $detail = $this->stockTutorial->getDetail('tutorial');
        return view('admin::stock-tutorial.edit', [
            'detail' => $detail
        ]);
    }

    public function update(Request $request){
        $param = $request->all();
        $update = $this->stockTutorial->update($param);
        return response()->json($update);
    }

    public function upload(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }

    }

    //Lưu file image vào folder temp
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() . date_format($time, 'd') . date_format($time, 'm') . date_format($time, 'Y') . "_blog." . $file->getClientOriginalExtension();
        $file->move(TEMP_PATH, $file_name);
        $file_name = asset($this->transferTempfileToAdminfile($file_name));
        return $file_name;
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = NEW_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(NEW_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

}