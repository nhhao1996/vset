<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Models\StockNewsTable;
use Modules\Admin\Repositories\StockCategory\StockCategoryRepositoryInterface;

class StockCategoryController extends Controller
{
    protected $stockCategory;

    public function __construct(StockCategoryRepositoryInterface $stockCategory)
    {
        $this->stockCategory = $stockCategory;
    }

    public function index()
    {
        $filter = request()->all();
        $data = $this->stockCategory->getListNew($filter);

        return view('admin::stock-category.index', [
            'LIST' => $data,
            'FILTER' => $filter,
        ]);
    }

    // FUNCTION  FILTER LIST ITEM
    protected function filters()
    {
    }

    /**
     * Danh sách hỗ trợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filters = $request->all();
        $data = $this->stockCategory->getListNew($filters);
        return view('admin::stock-category.list', [
                'LIST' => $data,
                'FILTER' => $filters,
            ]
        );
    }

    public function addStockCategory(Request $request) {
        $param = $request->all();
        $add = $this->stockCategory->showPopup($param);
        return $add;
    }

    public function editStockCategory(Request $request) {
        $param = $request->all();
        $add = $this->stockCategory->showPopup($param);
        return $add;
    }

    public function detailStockCategory(Request $request) {
        $param = $request->all();
        $add = $this->stockCategory->showPopup($param);
        return $add;
    }


    public function save(Request $request) {
        $param = $request->all();
        $save = $this->stockCategory->save($param);
        return response()->json($save);
    }

    public function removeCategory(Request $request) {
        $param = $request->all();
        $delete = $this->stockCategory->deleteCategory($param['id']);
        return response()->json($delete);
    }
}