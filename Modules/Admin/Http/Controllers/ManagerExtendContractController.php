<?php


namespace Modules\Admin\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Repositories\ManagerExtendContract\ManagerExtendContractRepositoryInterface;

class ManagerExtendContractController extends Controller
{
    protected $request;
    protected $managerExtendContract;
    public function __construct(Request $request , ManagerExtendContractRepositoryInterface $managerExtendContract)
    {
        $this->request = $request;
        $this->managerExtendContract = $managerExtendContract;
    }

    public function index() {
        $list = $this->managerExtendContract->list();
        return view('admin::manager-extend-contract.index', [
            'LIST' => $list,
            'FILTER' => $this->filters(),

        ]);
    }

    //function filter
    protected function filters()
    {
        return [
        ];
    }

    //function view list
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
            'created_at', 'search', 'contract_status', 'contract_value', 'is_active','product_category_id']);
        $list = $this->managerExtendContract->list($filter);
        return view('admin::manager-extend-contract.list', ['LIST' => $list, 'page' => $filter['page']]);
    }

    public function extendContract($id) {
        $detail = $this->managerExtendContract->getDetail($id);

        return view('admin::manager-extend-contract.detail', [
            'detail' => $detail['detail'],
            'listProduct' => $detail['listProduct'],
            'listProductInterest' => $detail['listProductInterest'],
            'withdrawInterestTime' => $detail['withdrawInterestTime'],
            'paymentMethod' => $detail['paymentMethod']
        ]);
    }

    public function checkExtendContract() {
        $param = $this->request->all();
        $check = $this->managerExtendContract->checkExtendContract($param);
        return response()->json($check);
    }

    public function checkValue(){
        $param = $this->request->all();
        $checkValue = $this->managerExtendContract->checkValue($param);
        return response()->json($checkValue);
    }

    public function createBill(){
        $view = view('admin::manager-extend-contract.popup.create-bill')->render();
        return \response()->json($view);
    }

    public function printBill(Request $request){
        $printBill = $this->managerExtendContract->printBill($request->all());
        return \response()->json($printBill);
    }

    /**
     * Upload image
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAction(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            $new_path = $this->transferTempfile($file);
            return response()->json(["file" => url('/').'/' . $new_path, "success" => "1"]);
        }
    }

    /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . "_product." . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);
        return $file_name;
    }

    private function transferTempfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STORE_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STORE_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

//    Kiểm tra và gia hạn hợp đồng
    public function checkCreateContract(){
        $param = $this->request->all();
        $checkCreateContract = $this->managerExtendContract->checkCreateContract($param);
        return response()->json($checkCreateContract);
    }
}