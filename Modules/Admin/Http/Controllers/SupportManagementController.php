<?php


namespace Modules\Admin\Http\Controllers;

//VSet
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Repositories\SupportManagement\SupportManagementRepositoryInterface;

class SupportManagementController extends Controller
{
    protected $supportManagent;

    public function __construct(SupportManagementRepositoryInterface $supportManagent)
    {
        $this->supportManagent  = $supportManagent;
    }

    public function indexAction() {
        $supportManagent = $this->supportManagent->list();

        return view('admin::support-management.index', [
            'LIST' => $supportManagent,
            'FILTER' => $this->filters(),
        ]);
    }

    public function indexGroupAction() {
        $data = $this->supportManagent->listGroup();
        return view('admin::support-management.group-index', [
            'LIST' => $data,
            'FILTER' => $this->filters(),
        ]);
    }

    public function listGroupAction(Request $request)
    {
        $filters = $request->all();
        $supportManagent = $this->supportManagent->list($filters);
        return view('admin::support-management.group-list', ['LIST' => $supportManagent]);
    }
    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $supportManagent = $this->supportManagent->list($filters);
        return view('admin::support-management.list', ['LIST' => $supportManagent]);
    }

    /**
     * Hiển thị thông tin chi tiết
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data = $this->supportManagent->show($id);

        return view('admin::support-management.detail', [
            'detail' => $data
        ]);
    }
}