<?php

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\GroupCustomer\StoreRequest;
use Modules\Admin\Http\Requests\GroupCustomer\UpdateRequest;
use Modules\Admin\Repositories\Customer\CustomerRepositoryInterface;
use Modules\Admin\Repositories\GroupCustomer\GroupCustomerRepositoryInterface;

class GroupCustomerController extends Controller
{
    protected $rGroupCustomer;

    public function __construct(GroupCustomerRepositoryInterface $rGroupCustomer)
    {
       $this->rGroupCustomer = $rGroupCustomer;
    }

    /**
     * Chuyển sang trang tạo nhóm khách hàng
     */
    public function create()
    {
        $unique = Carbon::now()->format('YmdHisu');
        return view('admin::group-customer.create', ['unique' => $unique]);
    }

    /**
     * Danh sách nhà đầu tư
     * @param Request $request
     * @return mixed
     */
    public function loadCustomer(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->loadCustomer($params);
        return $result;
    }

    /**
     * Thêm khách hàng vào session - tạm
     * @param Request $request
     * @return mixed
     */
    public function checkedCustomer(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->checkedCustomer($params);
        return $result;
    }

    /**
     * Xóa hết khách hàng từ session - tạm
     * @param Request $request
     * @return mixed
     */
    public function removeSessionTemp(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->removeSessionTemp($params);
        return $result;
    }

    /**
     * Thêm khách hàng từ session tạm và session selected
     * @param Request $request
     * @return mixed
     */
    public function submitAddItem(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->submitAddItem($params);
        return $result;
    }

    /**
     * Xóa khách hàng đã chọn ra khỏi session selected
     * @param Request $request
     * @return mixed
     */
    public function removeItem(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->removeItem($params);
        return $result;
    }

    /**
     * Thêm mới nhóm khách hàng
     * @param StoreRequest $request
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->store($params);
        return $result;
    }

    /**
     * Chuyển sang trang chỉnh sửa nhóm khách hàng
     */
    public function edit($id)
    {
        $unique = Carbon::now()->format('YmdHisu');
        $this->rGroupCustomer->setDefaultCustomer($id, $unique);
        $groupCustomer = $this->rGroupCustomer->getItem($id);
        if ($groupCustomer == null) {
            return redirect()->route('admin.group-customer');
        }
        return view('admin::group-customer.edit', [
            'unique' => $unique,
            'groupCustomer' => $groupCustomer,
        ]);
    }

    /**
     * Chỉnh sửa nhóm khách hàng
     * @param StoreRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->update($params);
        return $result;
    }

    /**
     * Chuyển sang trang chỉnh chi tiết nhóm khách hàng
     */
    public function show($id)
    {
        $unique = Carbon::now()->format('YmdHisu');
        $this->rGroupCustomer->setDefaultCustomer($id, $unique);
        $groupCustomer = $this->rGroupCustomer->getItem($id);
        if ($groupCustomer == null) {
            return redirect()->route('admin.group-customer');
        }
        return view('admin::group-customer.show', [
            'unique' => $unique,
            'groupCustomer' => $groupCustomer,
        ]);
    }

    public function index(Request $request)
    {
        $filter = $request->only([
            'page', 'display',
            'group_customer$is_active', 'search',
        ]);
        $list = $this->rGroupCustomer->getList($filter);
        return view('admin::group-customer.index', [
            'LIST' => $list,
            'FILTER' => $filter,
        ]);
    }

    /**
     * Danh sách
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filter = $request->only([
            'page', 'display',
            'group_customer$is_active', 'search',
        ]);
        $list = $this->rGroupCustomer->getList($filter);
        return view('admin::group-customer.list', [
            'LIST' => $list,
            'page' => $filter['page'],
            'FILTER' => $filter,
        ]);
    }

    /**
     * Xóa
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupCustomer->destroy($params);
        return $result;
    }
}