<?php


namespace Modules\Admin\Http\Controllers;


use App\Exports\ExportFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\Product\StoreRequest;
use Modules\Admin\Models\StaffsTable;
use Modules\Admin\Repositories\CustomerContract\CustomerContractRepositoryInterface;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;
use Maatwebsite\Excel\Excel;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Imports\StockChartImport;





class StockChartController extends Controller
{
    protected $stockContract;
    protected $excel;
    protected $order;
    protected $staff;
    protected $stock;

    public function __construct(

        CustomerContractRepositoryInterface $stockContract,
        OrderRepositoryInterface $orders,
        StockRepositoryInterface $stock,
        Excel $excel,
        StaffsTable $staff
    )
    {

        $this->stockContract = $stockContract;
        $this->excel = $excel;
        $this->order = $orders;
        $this->staff = $staff;
        $this->stock=$stock;
    }
    public function indexAction(Request $request)
  {
      $filters = $request->all();
      $filters = request()->all();
      $list = $this->stock->getListStockChart($filters);

      return view('admin::stock-chart.index', [
          'LIST' => $list,
          'FILTER' => $this->filters(),
          'type'=>'saving',
          'isEdit'=>false
//            'page' => $filter['page']

      ]);
    }
    public function listAction(Request $request)
    {
        $filters = request()->all();
        $list = $this->stock->getListStockChart($filters);

        return view('admin::stock-chart.list', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
//            'page' => $filter['page']

        ]);
    }
    protected function filters()
    {
        return [
            'products$is_actived' => [
                'data' => [
                    '' => __('Trạng thái hiển thị'),
                    1 => __('Hoạt động'),
                    0 => __('Tạm ngưng')
                ]
            ]
        ];
    }
    public function add(Request $request){
        $this->validate($request,[
            'time'=>'required',
            'value'=>'required|min:0',
        ]);
        $data = $request ->all();
        return  $this->stock->addStockChart($data);

    }
    public function edit(Request $request){
        $this->validate($request,[
            'time'=>'required',
            'value'=>'required|min:0'
        ]);
        $data = $request ->all();
        return $this->stock->editStockChart($data);
    }
    public function  importExcel(Request $request){
        //Kiểm tra file
        if ($request->hasFile('file')) {
            \Maatwebsite\Excel\Facades\Excel::import(new StockChartImport,request()->file("file"));
            $file = $request->file;
//            $file->move('uploads/admin/stock-chart', $file->getClientOriginalName());
            return ['error'=>0, 'message' => "Import thành công"];
        }
        return ['error'=>1, 'message'=> 'Vui lòng chọn file import'];
    }
    public function remove(Request $request){
        $id = $request -> stock_chart_id;
        $this->stock->removeStockChart($id);
        return ['error'=>0, 'message'=>"Xóa thành công"];
}
public function getFileMau(){
        return "/uploads/admin/stock-chart/file_mau/stock_chart.xls";
}



}