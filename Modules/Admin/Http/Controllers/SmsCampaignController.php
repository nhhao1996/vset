<?php
/**
 * Created by PhpStorm.
 * User: LE DANG SINH
 * Date: 1/29/2019
 * Time: 10:33 AM
 */

namespace Modules\Admin\Http\Controllers;


use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Repositories\Branch\BranchRepositoryInterface;
use Modules\Admin\Repositories\BrandName\BrandNameRepositoryInterFace;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\Department\DepartmentRepositoryInterface;
use Modules\Admin\Repositories\SendSms\SendSmsRepository;
use Modules\Admin\Repositories\SmsCampaign\SmsCampaignRepositoryInterface;
use Modules\Admin\Repositories\SmsExportExcel\CollectionExport;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;

class SmsCampaignController extends Controller
{
    protected $brandName;
    protected $smsCampaign;
    protected $branch;
    protected $customers;
    protected $staff;
    protected $smsLog;
    protected $depa;
    protected $sendSms;

    public function __construct(
        BrandNameRepositoryInterFace $brandName,
        SmsCampaignRepositoryInterface $smsCampaign,
        BranchRepositoryInterface $branch,
        CustomerRepository $customers,
        StaffRepositoryInterface $staff,
        SmsLogRepositoryInterface $smsLog,
        DepartmentRepositoryInterface $depa,
        SendSmsRepository $sendSms
    )
    {
        $this->brandName = $brandName;
        $this->smsCampaign = $smsCampaign;
        $this->branch = $branch;
        $this->customers = $customers;
        $this->staff = $staff;
        $this->smsLog = $smsLog;
        $this->depa = $depa;
        $this->sendSms = $sendSms;
    }

    //function view index
    public function indexAction()
    {
        $brandName = $this->brandName->getOption();
        $list = $this->smsCampaign->list2();

        $smsLog = $this->smsLog->getAll();

        $listSmsCampaign = $this->listSmsCampaign($list, $smsLog);
        $result = collect($listSmsCampaign)->forPage(1, 10);
        return view('admin::marketing.sms.campaign.index', [
            'FILTER' => $this->filters(),
            'brandName' => $brandName,
            'page' => 1,
            'LIST' => $result,
            'data' => $listSmsCampaign,
        ]);
    }

    private function listSmsCampaign($campaign, $smsLog)
    {
        $result = [];
        if ($campaign != null && $smsLog != null) {
            foreach ($campaign as $item) {
                $sentBy = '';
                $queryStaff = $this->staff->getItem($item['sent_by']);
                if ($queryStaff != null) {
                    $sentBy = $queryStaff->full_name;
                }
                $result[] = [
                    'campaign_id' => $item['campaign_id'],
                    'sms_campaign_name' => $item['name'],
                    'status' => $item['status'],
                    'created_by' => $item['full_name'],
                    'sent_by' => $sentBy,
                    'created_at' => $item['created_at'],
                    'time_sent' => $item['time_sent'],
                ];
            }
            foreach ($result as $key => $value) {
                $totalMessage = 0;
                $messageSuccess = 0;
                $messageError = 0;

                foreach ($smsLog as $kk => $vv) {
                    if ($value['campaign_id'] == $vv['campaign_id']) {
                        $totalMessage += 1;
                        if ($vv['error_code'] != null) {
                            $messageError += 1;
                        } else {
                            if ($vv['sms_status'] == 'sent') {
                                $messageSuccess += 1;
                            }
                        }
                    }
                }
                $result[$key]['totalMessage'] = $totalMessage;
                $result[$key]['messageSuccess'] = $messageSuccess;
                $result[$key]['messageError'] = $messageError;
            }

            return $result;
        }
    }

    protected function filters()
    {
        $staff = $this->staff->getStaffOption();
        return [
            'created_by' => [
                'data' => (['' => __('Chọn người tạo')]) + $staff
            ],
            'sent_by' => [
                'data' => (['' => __('Chọn người gửi')]) + $staff
            ],
            'status' => [
                'data' => [
                    '' => __('Chọn trạng thái'),
                    'new' => __('Mới'),
                    'sent' => __('Hoàn thành'),
                    'cancel' => __('Hủy')
                ]
            ],
        ];
    }

    public function listAction(Request $request)
    {
//        $filters = $request->only(['page', 'display', 'search_type', 'search_keyword', 'created_by', 'status']);
//        $list = $this->smsCampaign->list2($filters);
//        return view('admin::marketing.sms.campaign.list', [
//                'LIST' => $list,
//                'FILTER' => $this->filters(),
//                'page' => $filters['page']
//            ]
//        );
    }

    public function submitAddAction(Request $request)
    {
        $name = $request->name;
        $content = $request->contents;
        $dateTime = $request->dateTime;
        $isNow = $request->isNow;
        $slug = str_slug($name);
        $checkSlug = $this->smsCampaign->checkSlugName($slug, 0);
        $dateTimeNow = date('Ymd');
        if ($checkSlug != null) {
            return response()->json(['error' => 'slug']);
        } else {
            $data = [
                'name' => $name,
                'status' => 'new',
                'content' => $content,
                'slug' => $slug,
                'code' => '',
                'value' => $dateTime,
                'is_now' => $isNow,
                'created_by' => Auth::id(),
            ];
            if ($isNow == 1) {
                unset($data['value']);
            }
            $insert = $this->smsCampaign->add($data);
            $this->smsCampaign->edit(['code' => 'CD_' . $dateTimeNow . $insert], $insert);
            return response()->json(['error' => 0, 'id' => $insert]);
        }
    }

    public function indexSendSms()
    {
        $smsCampaign = $this->smsCampaign->getOptionCustomerCare();
        $branch = $this->branch->getBranch();
        return view('admin::marketing.sms.campaign.send-sms', [
            'smsCampaign' => $smsCampaign,
            'branch' => $branch,
        ]);
    }

    public function removeAction($id)
    {
        $this->smsCampaign->remove($id);
        $this->smsLog->cancelLogCampaign($id);
        return response()->json([
            'error' => 0,
            'message' => 'Remove success'
        ]);
    }

    public function getInfoSmsCampaign(Request $request)
    {
        $data = $this->smsCampaign->getItem($request->id);
        if ($data != null) {
            $brandName = '';
            $getItemBandName = $this->brandName->getItem($data->brandname_id);
            if ($getItemBandName != null) {
                $brandName = $getItemBandName->name;
            }
            $result = [
                'brandname_id' => $brandName,
                'value' => $data->value, 'status' => $data->status
            ];
            return response()->json($result);
        }
    }

    public function searchCustomerAction(Request $request)
    {
        $keyword = $request->keyword;
        $birthday = $request->birthday;
        $gender = $request->gender;
        $branchId = $request->branch;
        if ($birthday != '') {
            $birthdayFormat = Carbon::createFromFormat('d/m/Y', $birthday)->format('Y-m-d');
        } else {
            $birthdayFormat = null;
        }
        $data = $this->customers->searchCustomerEmail($keyword, $birthdayFormat, $gender, $branchId);

        $result = [];
        foreach ($data as $item) {
            $result[] = [
                'full_name' => $item['full_name'],
                'customer_id' => $item['customer_id'],
                'phone' => $item['phone'],
                'birthday' => date("d/m/Y", strtotime($item['birthday'])),
                'gender' => $item['gender'],
                'branch_name' => $item['branch_name']
            ];
        }
        return response()->json(['result' => $result]);
    }

    //Phân trang (tất cả item).
    public function pagingAction(Request $request)
    {
        $page = $request->page;
//        var_dump($page);
        //Danh sách chi nhánh.
        $list = $this->smsCampaign->list2();

        $smsLog = $this->smsLog->getAll();

        $listSmsCampaign = $this->listSmsCampaign($list, $smsLog);
        $result = collect($listSmsCampaign)->forPage($page, 10);

        $contents = view('admin::marketing.sms.campaign.paging.paging', [
            'data' => $listSmsCampaign,
            'LIST' => $result,
            'page' => $page
        ])->render();
        return $contents;
    }

    public function addAction()
    {
//        $branch = $this->branch->getBranch();
        $branchOption = $this->branch->getBranch();
        return view('admin::marketing.sms.campaign.add', ['branch' => $branchOption]);
    }

    public function filterAction(Request $request)
    {
        $keyWord = $request->keyWord;
        $createdBy = $request->createdBy;
        $sentBy = $request->sentBy;
        $status = $request->status;
        $daySent = null;

        if ($request->daySent != null) {
            $daySent = Carbon::createFromFormat('d/m/Y', $request->daySent)->format('Y-m-d');
        }
        $createdAt = null;
        if ($request->createdAt != null) {
            $createdAt = Carbon::createFromFormat('d/m/Y', $request->createdAt)->format('Y-m-d');
        }

        $filters = ['search_keyword' => $keyWord,
            'created_by' => $createdBy,
            'sent_by' => $sentBy,
            'status' => $status,
            'day_sent' => $daySent,
            'created_at' => $createdAt];
        $listCampaign = $this->smsCampaign->getListCampaign($filters);

        $smsLog = $this->smsLog->getAll();

        $listSmsCampaign = $this->listSmsCampaign($listCampaign, $smsLog);

        $result = collect($listSmsCampaign)->forPage(1, 10);
        $contents = view('admin::marketing.sms.campaign.filter', [
            'data' => $listSmsCampaign,
            'LIST' => $result,
            'page' => 1
        ])->render();
        return $contents;
    }

    public function pagingFilterAction(Request $request)
    {
        $page = intval($request->page);
        $keyWord = $request->keyWord;
        $createdBy = $request->createdBy;
        $sentBy = $request->sentBy;
        $status = $request->status;
        $daySent = $request->daySent;
        $createdAt = $request->createdAt;
        $filters = ['search_keyword' => $keyWord,
            'created_by' => $createdBy,
            'sent_by' => $sentBy,
            'status' => $status,
            'day_sent' => $daySent,
            'created_at' => $createdAt];
        $listCampaign = $this->smsCampaign->getListCampaign($filters);

        $smsLog = $this->smsLog->getAll();

        $listSmsCampaign = $this->listSmsCampaign($listCampaign, $smsLog);

        $result = collect($listSmsCampaign)->forPage($page, 10);
        $contents = view('admin::marketing.sms.campaign.paging.paging-filter', [
            'data' => $listSmsCampaign,
            'LIST' => $result,
            'page' => $page
        ])->render();
        return $contents;
    }

    public function editAction($id)
    {
        $campaign = $this->smsCampaign->getItem($id);
        $branch = $this->branch->getBranchOption();
        $listLog = $this->smsLog->getLogCampaign($id);

        if ($campaign != null) {
            if ($campaign->status == 'new') {
                $daySent = '';
                $timeSent = '';
                if ($campaign->value != null) {
                    $daySent = explode(' ', $campaign->value)[0];
                    $timeSent = explode(' ', $campaign->value)[1];
                }
                return view('admin::marketing.sms.campaign.edit',
                    [
                        'campaign' => $campaign,
                        'daySent' => $daySent,
                        'timeSent' => $timeSent,
                        'id' => $id,
                        'branch' => $branch,
                        'listLog' => $listLog
                    ]);
            } else {
                return redirect()->route('admin.sms.sms-campaign');
            }
        } else {
            return redirect()->route('admin.sms.sms-campaign');
        }
    }

    public function detailAction($id)
    {
        $campaign = $this->smsCampaign->getItem($id);
        $campaignDetail = $this->smsLog->getLogDetailCampaign($id);
        $result = collect($campaignDetail)->forPage(1, 10);
        if ($campaign != null) {
            $logSuccess = 0;
            $logError = 0;
            $listLog = [];
            foreach ($campaignDetail as $key => $value) {
                if ($value['error_code'] == null) {
                    if ($value['sms_status'] == 'sent') {
                        $logSuccess += 1;
                    }
                } else {
                    $logError += 1;
                }
//                    $temp['customer'] = $value['customer_name'];
//                    $temp['phone'] = $value['phone'];
//                    $temp['message'] = $value['message'];
//                    $temp['created_by'] = '';
//                    $temp['sent_by'] = '';
//
//                    $createdBy = $this->staff->getItem($value['created_by']);
//                    if ($createdBy != null) {
//                        $temp['created_by'] = $createdBy->full_name;
//                    }

//                    $sentBy = $this->staff->getItem($value['created_by']);
//                    if ($createdBy != null) {
//                    $temp['sent_by'] = null;
//                    }

//                    $temp['sms_status'] = $value['sms_status'];
//                    $temp['created_at'] = $value['created_at'];
//                    $temp['time_sent'] = $value['time_sent'];
//                    $temp['error_code'] = $value['error_code'];
//                    $listLog[] = $temp;
            }
            return view('admin::marketing.sms.campaign.detail',
                [
                    'campaign' => $campaign,
                    'data' => $campaignDetail,
                    'logSuccess' => $logSuccess,
                    'logError' => $logError,
                    'totalSms' => count($campaignDetail),
                    "page" => 1,
                    'LIST' => $result
                ]);
        } else {
            return redirect()->route('admin.sms.sms-campaign');
        }
    }

    public function submitEditAction(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $content = $request->contents;
        $dateTime = $request->dateTime;
        $isNow = $request->isNow;
        $slug = str_slug($name);
        $checkSlug = $this->smsCampaign->checkSlugName($slug, $id);
        $dateTimeNow = date('Y-m-d H:i:s');

        if ($checkSlug != null) {
            return response()->json(['error' => 'slug']);
        } else {
            $data = [
                'name' => $name,
                'status' => 'new',
                'content' => $content,
                'slug' => $slug,
                'value' => $dateTime,
                'is_now' => $isNow,
                'updated_by' => Auth::id(),
                'updated_at' => $dateTimeNow,
            ];
            if ($isNow == 1) {
                $data['value'] = '';
            }
            $this->smsCampaign->edit($data, $id);
            return response()->json(['error' => 0]);
        }
    }

    public function submitSaveLogAction(Request $request)
    {
        $arrayLogDelete = $request->arrayLogDelete;
        $array = $request->array;
        $campaignId = $request->id;

        $campaign = $this->smsCampaign->getItem($campaignId);
        $time = '';

        if ($campaign->is_now == 0) {
            $time = Carbon::createFromFormat('d/m/Y H:i', $campaign->value)
                ->format('Y-m-d H:i');
        }
        if ($campaign->is_now == 1) {
            $time = date('Y-m-d H:i:s');
        }
        if ($array != null) {
            $result = array_chunk($array, 4, false);

            foreach ($result as $key => $value) {
                $data = [
                    'brandname' => '',
                    'campaign_id' => $campaignId,
                    'phone' => $value[2],
                    'customer_name' => $value[1],
                    'message' => $value[3],
                    'sms_status' => 'new',
                    'sms_type' => '',
                    'error_code' => '',
                    'time_sent' => $time,
                    'created_by' => Auth::id(),
                    'created_at' => date('Y-m-d H:i:s'),
                ];

                if ($campaign->is_now == 1) {
                    $data['time_sent'] = null;
                    $this->smsLog->add($data);
                } else {
                    $this->smsLog->add($data);
                }
            }
        }

        if ($arrayLogDelete != null) {
            foreach ($arrayLogDelete as $key => $value) {
                $this->smsLog->remove($value);
            }
        }
        return response()->json(['error' => 0]);
    }

    public function removeLogAction(Request $request)
    {
        $this->smsLog->remove($request->id);
        return response()->json(['error' => 0]);
    }

    //export file sample excel
    public function exportFileAction($type)
    {
//        return Excel::download(new CollectionExport(), 'file_mau.csv');
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new CollectionExport(), 'file_mau.xlsx');
//        return Excel::download(new CollectionExport(), 'export.csv');
//        return Excel::download(new CollectionExport(), function($excel) {
//
//            $excel->sheet('Sheetname', function($sheet) {
//
//                $sheet->fromArray(array(
//                    array('data1', 'data2'),
//                    array('data3', 'data4')
//                ));
//
//            });
//
//        })->export('xls');
    }


    public function importFileExcelAction(Request $request)
    {
        $file = $request->file('file');

        if (isset($file)) {
//            $desFile = UPLOAD_FILE_EXCEL . basename($file->getClientOriginalName());
            $typeFileExcel = $file->getClientOriginalExtension();
            $arrayCustomer = [];
            if ($typeFileExcel == "xlsx") {
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($file);
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if ($key == 1) {

                        } elseif ($key != 1 && $row[0] != '' && $row[1] != '') {
                            $arrayCustomer[] = [
                                'customer_name' => $row[0],
                                'phone' => $row[1],
                                'birthday' => '',
                                'gender' => '',

                            ];

                        }
                    }
                }

                $reader->close();
            }
            return response()->json($arrayCustomer);
        }
    }

    public function listDetailAction(Request $request)
    {
//        var_dump(1);
//        $filter = $request->only(['id', 'page']);
//        $card_list = $this->smsLog->getLogDetailCampaign($filter['id'],$filter["service_card_id"]);
//        return view('admin::service-card.inc.table-card-list', ['LIST' => $card_list]);
    }

    public function pagingDetailAction(Request $request)
    {
        $campaignDetail = $this->smsLog->getLogDetailCampaign($request->id);
        $result = collect($campaignDetail)->forPage($request->page, 10);
        $contents = view('admin::marketing.sms.campaign.paging.paging-detail', [
            'data' => $campaignDetail,
            'LIST' => $result,
            'page' => $request->page
        ])->render();
        return $contents;
    }
}