<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 10/23/2019
 * Time: 1:55 PM
 */

namespace Modules\Admin\Http\Controllers;

use App\Exports\CustomerGroupExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\CustomerGroupFilter\StoreRequest;
use Modules\Admin\Repositories\CustomerGroupFilter\CustomerGroupFilterRepositoryInterface;
use Modules\Admin\Http\Requests\CustomerGroupFilter\UpdateRequest;

class CustomerGroupFilterController extends Controller
{
    protected $customerGroupFilter;

    public function __construct(CustomerGroupFilterRepositoryInterface $customerGroupFilter)
    {
        $this->customerGroupFilter = $customerGroupFilter;
    }

    /**
     * Index
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function indexAction()
    {
        $customGroupList = $this->customerGroupFilter->list();
        return view('admin::customer-group-filter.index', [
            'LIST' => $customGroupList
        ]);
    }

    /**
     * Tải file mẫu
     * @return mixed
     */
    public function exportExcelAction()
    {

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        return Excel::download(new CustomerGroupExport(), 'Khách hàng.xlsx');
    }

    public function addDefineAction()
    {
        return view('admin::customer-group-filter.user-define.create', []);
    }

    /**
     * Thêm khách hàng vào nhóm từ file excel
     * @param Request $request
     * @return mixed
     */
    public function importExcel(Request $request)
    {
        $file = $request->file('file');
        $arrayPhoneExist = [];
        $result = $this->customerGroupFilter->importExcel($file, $arrayPhoneExist);
        return $result;
    }

    /**
     * Tìm kiếm danh sách khách hàng trong mảng
     * @param Request $request
     * @return mixed
     */
    public function searchWhereInUser(Request $request)
    {
        $data = $request->all();
        return $this->customerGroupFilter->searchWhereInUser($data);
    }

    /**
     * Tìm kiếm tất cả khách hàng.
     * @param Request $request
     * @return mixed
     */
    public function searchAllCustomer(Request $request)
    {
        $filters = $request->all();
        $result = $this->customerGroupFilter->searchAllCustomer($filters);
        return $result;
    }

    /**
     * Thêm khách hàng đã chọn cho group.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function addCustomerGroupDefine(Request $request)
    {
        $data = ($request->all());
        return $this->customerGroupFilter->addCustomerGroupDefine($data);
    }

    public function submitAddGroupDefine(StoreRequest $request)
    {
        $data = $request->all();
        return $this->customerGroupFilter->submitAddGroupDefine($data);
    }

    public function listAction(Request $request)
    {
        $filter = $request->only(
            ['page', 'display', 'search_type', 'search_keyword',
             'filter_group_type']
        );
        $list = $this->customerGroupFilter->list($filter);

        return view(
            'admin::customer-group-filter.list', [
                'LIST' => $list,
                'page' => $filter['page']
            ]
        );
    }

    /**
     * Load sang trang edit nhóm động.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUserDefine($id)
    {
        $data = $this->customerGroupFilter->getItem($id);
        if ($data != null) {
            $user = $this->customerGroupFilter->getCustomerByGroupDefine($id);
            return view(
                'admin::customer-group-filter.user-define.edit', [
                    'id'   => $id,
                    'user' => $user,
                    'data' => $data
                ]
            );
        } else {
            return redirect()->route('admin.customer-group-filter');
        }
    }

    /**
     * Danh sách khách hàng của nhóm động.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerByGroupDefine(Request $request)
    {
        $id = $request->id;
        $user = $this->customerGroupFilter->getCustomerByGroupDefine($id);

        return response()->json($user);
    }

    /**
     * Cập nhật nhóm tự định nghĩa.
     *
     * @param UpdateRequest $request
     *
     * @return mixed
     */
    public function updateCustomerGroupDefine(UpdateRequest $request)
    {
        $data = $request->all();
        return $this->customerGroupFilter->updateCustomerGroupDefine($data);
    }

    /**
     * Load sang trang edit nhóm động.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detailCustomerGroupDefine($id)
    {
        $data = $this->customerGroupFilter->getItem($id);
        if ($data != null) {
            $user = $this->customerGroupFilter->getCustomerByGroupDefine($id);
            return view(
                'admin::customer-group-filter.user-define.detail', [
                    'id'   => $id,
                    'user' => $user,
                    'data' => $data
                ]
            );
        } else {
            return redirect()->route('admin.customer-group-filter');
        }
    }

    /**
     * Load sang trang thêm nhóm khách hàng động
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function addAutoAction()
    {
        $condition = $this->customerGroupFilter->getCondition();
        $customerGroupDefine
            = $this->customerGroupFilter->getCustomerGroupDefine();
        $listService = $this->customerGroupFilter->getListAllService();
        $listProduct = $this->customerGroupFilter->getListAllProduct();

        return view(
            'admin::customer-group-filter.auto.create', [
                'condition'           => $condition,
                'customerGroupDefine' => $customerGroupDefine,
                'listService'         => $listService,
                'listProduct'         => $listProduct
            ]
        );
    }

    public function getCondition(Request $request)
    {
        $array = $request->arrayCondition;
        $getCondition = $this->customerGroupFilter->getCondition($array);
        return response()->json($getCondition);
    }

    /**
     * Thêm mới nhóm tự động
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    public function submitAddAutoAction(StoreRequest $request)
    {
        $data = $request->all();
        $result = $this->customerGroupFilter->submitAddAutoAction($data);
        return $result;
    }

    /**
     * Load sang trang chỉnh sửa nhóm khách hàng động
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function editAutoAction($id)
    {
        $condition = $this->customerGroupFilter->getCondition();
        $customerGroupDefine
            = $this->customerGroupFilter->getCustomerGroupDefine();
        $listService = $this->customerGroupFilter->getListAllService();
        $listProduct = $this->customerGroupFilter->getListAllProduct();
        $customerGroup = $this->customerGroupFilter->getItem($id);
        $customerGroupDetail = $this->customerGroupFilter->getCustomerGroupDetail($id);
        $arrayConditionA = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
            '8' => 0,
            '9' => 0,];
        $arrayConditionB = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
            '8' => 0,
            '9' => 0,];
        foreach ($customerGroupDetail as $item) {
            if ($item['group_type'] == 'A') {
               if (isset($arrayConditionA[$item['condition_id']])) {
                   if ($item['condition_id'] == 1) {
                       $arrayConditionA[$item['condition_id']] = $item['customer_group_define_id'];
                   } elseif ($item['condition_id'] == 2) {
                       $arrayConditionA[$item['condition_id']] = $item['day_appointment'];
                   } elseif ($item['condition_id'] == 3) {
                       $arrayConditionA[$item['condition_id']] = $item['status_appointment'];
                   } elseif ($item['condition_id'] == 4) {
                       $arrayConditionA[$item['condition_id']] = $item['time_appointment'];
                   } elseif ($item['condition_id'] == 5) {
                       $arrayConditionA[$item['condition_id']] = $item['not_appointment'];
                   } elseif ($item['condition_id'] == 6) {
                       $arrayConditionA[$item['condition_id']] = explode(',', $item['use_service']);
                   } elseif ($item['condition_id'] == 7) {
                       $arrayConditionA[$item['condition_id']] = explode(',', $item['not_use_service']);
                   } elseif ($item['condition_id'] == 8) {
                       $arrayConditionA[$item['condition_id']] = explode(',', $item['use_product']);
                   } elseif ($item['condition_id'] == 9) {
                       $arrayConditionA[$item['condition_id']] = explode(',', $item['not_use_product']);
                   }
               }
            } elseif ($item['group_type'] == 'B') {
                if (isset($arrayConditionB[$item['condition_id']])) {
                    if ($item['condition_id'] == 1) {
                        $arrayConditionB[$item['condition_id']] = $item['customer_group_define_id'];
                    } elseif ($item['condition_id'] == 2) {
                        $arrayConditionB[$item['condition_id']] = $item['day_appointment'];
                    } elseif ($item['condition_id'] == 3) {
                        $arrayConditionB[$item['condition_id']] = $item['status_appointment'];
                    } elseif ($item['condition_id'] == 4) {
                        $arrayConditionB[$item['condition_id']] = $item['time_appointment'];
                    } elseif ($item['condition_id'] == 5) {
                        $arrayConditionB[$item['condition_id']] = $item['not_appointment'];
                    } elseif ($item['condition_id'] == 6) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['use_service']);
                    } elseif ($item['condition_id'] == 7) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['not_use_service']);
                    } elseif ($item['condition_id'] == 8) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['use_product']);
                    } elseif ($item['condition_id'] == 9) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['not_use_product']);
                    }
                }
            }
        }

        if ($customerGroup != null) {
            return view(
                'admin::customer-group-filter.auto.edit', [
                    'condition'            => $condition,
                    'customerGroupDefine'  => $customerGroupDefine,
                    'listService'          => $listService,
                    'listProduct'          => $listProduct,
                    'customerGroup'        => $customerGroup,
                    'customerGroupDetail' => $customerGroupDetail,
                    'arrayConditionA' => $arrayConditionA,
                    'arrayConditionB' => $arrayConditionB,
                ]
            );
        } else {
            return redirect()->route('admin.customer-group-filter');
        }
    }

    /**
     * Chỉnh sửa nhóm khách hàng tự động
     * @param StoreRequest $request
     * @return mixed
     */
    public function submitEditAutoAction(UpdateRequest $request)
    {
        $data = $request->all();
        $result = $this->customerGroupFilter->submitEditAutoAction($data);
        return $result;
    }

    public function getCustomerInGroupAuto(Request $request)
    {
        $id = intval($request->id);
        return $this->customerGroupFilter->getCustomerInGroupAuto($id);
    }

    public function getCustomerInGroup(Request $request)
    {
        $id = intval($request->id);
        return $this->customerGroupFilter->getCustomerInGroup($id);
    }

    /**
     * Load sang trang chi tiết nhóm khách hàng động
     * @param $id
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|mixed
     */
    public function detailAutoAction($id)
    {
        $condition = $this->customerGroupFilter->getCondition();
        $customerGroupDefine
            = $this->customerGroupFilter->getCustomerGroupDefine();
        $listService = $this->customerGroupFilter->getListAllService();
        $listProduct = $this->customerGroupFilter->getListAllProduct();
        $customerGroup = $this->customerGroupFilter->getItem($id);
        $customerGroupDetail = $this->customerGroupFilter->getCustomerGroupDetail($id);
        $arrayConditionA = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
            '8' => 0,
            '9' => 0,];
        $arrayConditionB = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
            '8' => 0,
            '9' => 0,];
        foreach ($customerGroupDetail as $item) {
            if ($item['group_type'] == 'A') {
                if (isset($arrayConditionA[$item['condition_id']])) {
                    if ($item['condition_id'] == 1) {
                        $arrayConditionA[$item['condition_id']] = $item['customer_group_define_id'];
                    } elseif ($item['condition_id'] == 2) {
                        $arrayConditionA[$item['condition_id']] = $item['day_appointment'];
                    } elseif ($item['condition_id'] == 3) {
                        $arrayConditionA[$item['condition_id']] = $item['status_appointment'];
                    } elseif ($item['condition_id'] == 4) {
                        $arrayConditionA[$item['condition_id']] = $item['time_appointment'];
                    } elseif ($item['condition_id'] == 5) {
                        $arrayConditionA[$item['condition_id']] = $item['not_appointment'];
                    } elseif ($item['condition_id'] == 6) {
                        $arrayConditionA[$item['condition_id']] = explode(',', $item['use_service']);
                    } elseif ($item['condition_id'] == 7) {
                        $arrayConditionA[$item['condition_id']] = explode(',', $item['not_use_service']);
                    } elseif ($item['condition_id'] == 8) {
                        $arrayConditionA[$item['condition_id']] = explode(',', $item['use_product']);
                    } elseif ($item['condition_id'] == 9) {
                        $arrayConditionA[$item['condition_id']] = explode(',', $item['not_use_product']);
                    }
                }
            } elseif ($item['group_type'] == 'B') {
                if (isset($arrayConditionB[$item['condition_id']])) {
                    if ($item['condition_id'] == 1) {
                        $arrayConditionB[$item['condition_id']] = $item['customer_group_define_id'];
                    } elseif ($item['condition_id'] == 2) {
                        $arrayConditionB[$item['condition_id']] = $item['day_appointment'];
                    } elseif ($item['condition_id'] == 3) {
                        $arrayConditionB[$item['condition_id']] = $item['status_appointment'];
                    } elseif ($item['condition_id'] == 4) {
                        $arrayConditionB[$item['condition_id']] = $item['time_appointment'];
                    } elseif ($item['condition_id'] == 5) {
                        $arrayConditionB[$item['condition_id']] = $item['not_appointment'];
                    } elseif ($item['condition_id'] == 6) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['use_service']);
                    } elseif ($item['condition_id'] == 7) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['not_use_service']);
                    } elseif ($item['condition_id'] == 8) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['use_product']);
                    } elseif ($item['condition_id'] == 9) {
                        $arrayConditionB[$item['condition_id']] = explode(',', $item['not_use_product']);
                    }
                }
            }
        }

        if ($customerGroup != null) {
            return view(
                'admin::customer-group-filter.auto.detail', [
                    'condition'            => $condition,
                    'customerGroupDefine'  => $customerGroupDefine,
                    'listService'          => $listService,
                    'listProduct'          => $listProduct,
                    'customerGroup'        => $customerGroup,
                    'customerGroupDetail' => $customerGroupDetail,
                    'arrayConditionA' => $arrayConditionA,
                    'arrayConditionB' => $arrayConditionB,
                ]
            );
        } else {
            return redirect()->route('admin.customer-group-filter');
        }
    }
}