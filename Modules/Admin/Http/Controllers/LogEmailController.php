<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\LogEmail\LogEmailRepositoryInterface;

class LogEmailController extends Controller
{
    protected $logEmail;

    public function __construct(LogEmailRepositoryInterface $logEmail)
    {
        $this->logEmail = $logEmail;
    }

    public function indexAction() {
        $logEmail = $this->logEmail->list();

        return view('admin::log-email.index', [
            'LIST' => $logEmail,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $logEmail = $this->logEmail->list($filters);
        return view('admin::log-email.list', ['LIST' => $logEmail,'page' => $filters['page']]);
    }
}