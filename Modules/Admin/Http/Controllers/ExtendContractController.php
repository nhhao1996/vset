<?php


namespace Modules\Admin\Http\Controllers;

//VSet
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Repositories\BuyBondsRequest\BuyBondsRequestRepositoryInterface;
use Modules\Admin\Repositories\ExtendContract\ExtendContractRepositoryInterface;

class ExtendContractController extends Controller
{
    protected $extenContract;

    public function __construct(ExtendContractRepositoryInterface $extenContract)
    {
        $this->extenContract  = $extenContract;
    }

    public function indexAction() {
        $buyBondsRequest = $this->extenContract->list();
        return view('admin::extend-contract.index', [
            'LIST' => $buyBondsRequest,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $buyBondsRequest = $this->extenContract->list($filters);
        return view('admin::extend-contract.list', ['LIST' => $buyBondsRequest,'page' => $filters['page']]);
    }

    /**
     * Hiển thị thông tin chi tiết
     *
     * @param int $id
     * @return Response
     * @return Response
     */
    public function show($id)
    {
        $data = $this->extenContract->show($id);
        $oldContract = $this->extenContract->getOldContract($data['contract_code_extend']);
        $moneyReceipt = $this->extenContract->getMoneyReceipt($id);
        $listStaff = $this->extenContract->getListStaff();
        $total = 0 ;
//        Chuyển khoản
        if ($data['payment_method_id'] == 1 || $data['payment_method_id'] == 4) {
            $total = null;
//            ví lãi
        } else if ($data['payment_method_id'] == 2){
//            Danh sách id hợp đồng
            $total = $this->extenContract->getTotalInterest($data['order_id']);
//            ví thưởng
        } else if ($data['payment_method_id'] == 3) {
//            $total = $this->extenContract->getTotalCommission($data['order_id'],$data['customer_id']);
            $total = $this->extenContract->getTotalCommissionWallet($data['order_id'],$data['customer_id']);
        }

//        Danh sách chuyển tiền
        return view('admin::extend-contract.detail', [
            'detail' => $data,
            'moneyReceipt' => $moneyReceipt,
            'total' => $total,
            'oldContract' => $oldContract,
            'listStaff' => $listStaff,
        ]);
    }

    public function addReceiptDetail(Request $request) {
        $param = $request->all();
        $addReceiptDetail = $this->extenContract->addReceiptDetail($param);
        return $addReceiptDetail;
    }

    public function createWallet(Request $request){
        $param = $request->all();
        $createWallet = $this->extenContract->createWallet($param);
        return $createWallet;
    }

    public function getListReceiptDetail(Request $request) {
        $param = $request->all();
        $listReceipDetail = $this->extenContract->getListReceiptDetail($param);
        return $listReceipDetail;
    }

    public function showPopupReceiptDetail(Request $request) {
        $param = $request->all();
        $showPopup = $this->extenContract->showPopupReceiptDetail($param);
        return $showPopup;
    }

    /**
     * createReceipt
     *
     * @param int $amount
     * @return Response
     * @return Response
     */
    public function createReceipt(Request $request)
    {
        $param = $request->all();
        $createReceipt = $this->extenContract->createReceipt($param);
        return $createReceipt;
    }

    /**
     * Upload image dropzone
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadDropzoneAction(Request $request)
    {
        return $this->extenContract->uploadDropzone($request->all());
    }

    public function printBill(Request $request){
        $printBill = $this->extenContract->printBill($request->all());
        return \response()->json($printBill);
    }

    public function createBill(){
        $view = view('admin::extend-contract.popup.create-bill')->render();
        return \response()->json($view);
    }


}