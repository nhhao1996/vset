<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17/03/2018
 * Time: 2:33 PM
 */

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Repositories\MemberLevel\MemberLevelRepositoryInterface;

class MemberLevelController extends Controller
{
    protected $memberLevel;

    public function __construct(MemberLevelRepositoryInterface $memberLevel)
    {
        $this->memberLevel = $memberLevel;
    }

    /**
     * Trang chính
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function indexAction(Request $request)
    {
        $memberLevelList = $this->memberLevel->list();
        return view('admin::member-level.index', [
            'LIST' => $memberLevelList,
            'FILTER' => $this->filters()
        ]);
    }

    /**
     * Khai báo filter
     *
     * @return array
     */
    protected function filters()
    {
        return [
            'is_actived' => [
                'data' => [
                    '' => __('Chọn trạng thái'),
                    1 => __('Hoạt động'),
                    0 => __('Tạm ngưng')
                ]
            ]
        ];
    }

    /**
     * Ajax danh sách user
     *
     * @param Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function listAction(Request $request)
    {
        $filters = $request->only(['page', 'display', 'search_type', 'search_keyword', 'is_actived']);
        $memberLevelList = $this->memberLevel->list($filters);
        return view('admin::member-level.list', [
            'LIST' => $memberLevelList,
            'page' => $filters['page']
        ]);
    }

    public function addAction(){
        return view('admin::member-level.add', [
        ]);
    }


    /**
     * Xử lý thêm user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submitaddAction(Request $request)
    {
        $param = $request->all();
        $add = $this->memberLevel->addMember($param);
        return $add;
    }

    public function showAction(Request $request)
    {
        $id = $request->id;
        $item = $this->memberLevel->getItem($id);

        $data = [
            'member_level_id' => $item->member_level_id,
            'name' => $item->name,
            'point' => $item->point,
            'discount' => $item->discount,
            'is_actived' => $item->is_actived,
            'description' => $item->description
        ];
        return view('admin::member-level.show', [
            'detail' => $item,
        ]);
    }

    //function edit
    public function editAction(Request $request)
    {
        $id = $request->id;
        $item = $this->memberLevel->getItem($id);

        $data = [
            'member_level_id' => $item->member_level_id,
            'name' => $item->name,
            'point' => $item->point,
            'discount' => $item->discount,
            'is_actived' => $item->is_actived,
            'description' => $item->description
        ];
        return view('admin::member-level.edit', [
            'detail' => $item,
        ]);
    }

    public function submitEditAction(Request $request)
    {
        $param = $request->all();
        $edit = $this->memberLevel->edit($param);
        return $edit;
    }

    //function thay doi trang thai
    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
        $data['is_actived'] = ($change['action'] == 'unPublish') ? 1 : 0;
        $this->memberLevel->edit($data, $change['id']);
        return response()->json([
            'status' => 0
        ]);
    }

    //function remove
    public function removeAction(Request $request)
    {
        $param = $request->all();
        $this->memberLevel->remove($param['id']);
        return response()->json([
            'error' => 0,
            'message' => 'Xóa thành công'
        ]);
    }

    /**
     * Upload image
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAction(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));
            return response()->json(["file" => $file, "success" => "1"]);
        }
    }

    /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . "_member." . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);
        return $file_name;
    }

}