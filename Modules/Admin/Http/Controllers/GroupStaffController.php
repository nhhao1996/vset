<?php

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\GroupStaff\StoreRequest;
use Modules\Admin\Http\Requests\GroupStaff\UpdateRequest;
use Modules\Admin\Repositories\GroupStaff\GroupStaffRepositoryInterface;

class GroupStaffController extends Controller
{
    protected $rGroupStaff;

    public function __construct(GroupStaffRepositoryInterface $rGroupStaff)
    {
       $this->rGroupStaff = $rGroupStaff;
    }

    /**
     * Chuyển sang trang tạo nhóm khách hàng
     */
    public function create()
    {
        $unique = Carbon::now()->format('YmdHisu');
        return view('admin::group-staff.create', ['unique' => $unique]);
    }

    /**
     * Danh sách nhân viên
     * @param Request $request
     * @return mixed
     */
    public function loadStaff(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->loadStaff($params);
        return $result;
    }

    /**
     * Thêm nhân viên vào session - tạm
     * @param Request $request
     * @return mixed
     */
    public function checkedItem(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->checkedItem($params);
        return $result;
    }

    /**
     * Xóa hết khách hàng từ session - tạm
     * @param Request $request
     * @return mixed
     */
    public function removeSessionTemp(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->removeSessionTemp($params);
        return $result;
    }

    /**
     * Thêm khách hàng từ session tạm và session selected
     * @param Request $request
     * @return mixed
     */
    public function submitAddItem(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->submitAddItem($params);
        return $result;
    }

    /**
     * Xóa khách hàng đã chọn ra khỏi session selected
     * @param Request $request
     * @return mixed
     */
    public function removeItem(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->removeItem($params);
        return $result;
    }

    /**
     * Thêm mới nhóm khách hàng
     * @param StoreRequest $request
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->store($params);
        return $result;
    }

    /**
     * Chuyển sang trang chỉnh sửa nhóm khách hàng
     */
    public function edit($id)
    {
        $unique = Carbon::now()->format('YmdHisu');
        $this->rGroupStaff->setDefaultItem($id, $unique);
        $group = $this->rGroupStaff->getItem($id);
        if ($group == null) {
            return redirect()->route('admin.group-staff');
        }
        return view('admin::group-staff.edit', [
            'unique' => $unique,
            'group' => $group,
        ]);
    }

    /**
     * Chỉnh sửa nhóm khách hàng
     * @param StoreRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->update($params);
        return $result;
    }

    /**
     * Chuyển sang trang chỉnh chi tiết nhóm nhân viên
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show($id)
    {
        $unique = Carbon::now()->format('YmdHisu');
        $this->rGroupStaff->setDefaultItem($id, $unique);
        $group = $this->rGroupStaff->getItem($id);
        if ($group == null) {
            return redirect()->route('admin.group-staff');
        }
        return view('admin::group-staff.show', [
            'unique' => $unique,
            'group' => $group,
        ]);
    }

    /**
     * Danh sách nhóm
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index(Request $request)
    {
        $filter = $request->only([
            'page', 'display',
            'group_staff$is_active', 'search',
        ]);
        $list = $this->rGroupStaff->getList($filter);
        return view('admin::group-staff.index', [
            'LIST' => $list,
            'FILTER' => $filter,
        ]);
    }

    /**
     * Danh sách
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filter = $request->only([
            'page', 'display',
            'group_staff$is_active', 'search',
        ]);
        $list = $this->rGroupStaff->getList($filter);
        return view('admin::group-staff.list', [
            'LIST' => $list,
            'page' => $filter['page'],
            'FILTER' => $filter,
        ]);
    }

    /**
     * Xóa
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->destroy($params);
        return $result;
    }

    /**
     * Chuyển sang trang phân quyền cho nhóm
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function authorization($id)
    {
        $unique = Carbon::now()->format('YmdHisu');
        $group = $this->rGroupStaff->getItem($id);
        $option = $this->rGroupStaff->getOption($id);
        $mapCustomer = $this->rGroupStaff->getMapCustomer($id);
        if ($group == null) {
            return redirect()->route('admin.group-staff');
        }
        return view('admin::group-staff.authorization', [
            'unique' => $unique,
            'group' => $group,
            'option' => $option,
            'mapCustomer' => $mapCustomer,
        ]);
    }

    /**
     * Update lại quyền của nhóm nhân viên
     * @param Request $request
     * @return mixed
     */
    public function updateAuthorization(Request $request)
    {
        $params = $request->all();
        $result = $this->rGroupStaff->updateAuthorization($params);
        return $result;
    }
}