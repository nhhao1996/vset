<?php


namespace Modules\Admin\Http\Controllers;

//VSet
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Models\CustomerTable;
use Modules\Admin\Repositories\Stock\StockRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class DividendController extends Controller
{
    protected $withdrawRequest;
    protected $customerContract;
    protected $stock;

    public function __construct(StockRepositoryInterface $stock, CustomerContactTable $customerContract)
    {
        // $this->withdrawRequest  = $withdrawRequest;
        $this->customerContract = $customerContract;
        $this->stock = $stock;
    }

    public function indexAction() {       
        $stockConfig = $this->stock->getStockConfig();
        $stockPublish = $this->stock->getLastStockPublish();
        $list = $this->stock->getListDividend([]);
        $listFile = $this->stock->getListDividendFile([]);
//        dd($listFile);

        return view('admin::dividend.index', [
            'stockConfig'=>$stockConfig,
            'stockPublish'=>$stockPublish,
            'LIST' => $list,
            'LISTFILE'=>$listFile,
            // 'LISTCOMMISSION' => $withdrawCommission,
            // 'LISTSTOCK' => $withdrawStock,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {
        return [
        ];
    }


    public function listAction(Request $request)
    {
        $filters = $request->all();
        $list = $this->stock->getListDividend($filters);  
        $stockConfig = $this->stock->getStockConfig();  
return view('admin::dividend.list', [
    'LIST' => $list,
    'stockConfig'=>$stockConfig
]); 
  
}
public function add(Request $request){       
    $validate = $request->validate([
        'date_publish' => 'required',        
    ]);
    $params = $request ->all();    
    return $this->stock->addDividend($params);
}
public function edit(Request $request){       
    $validate = $request->validate([
        'date_publish' => 'required',
    ]);
    $params = $request ->all();
    return $this->stock->editDividend($params);
}

// todo: Upload File-----------
public function uploadFile(Request $request){

    if ($request->file('file') != null) {
        $file = $this->uploadImageTemp($request->file('file'));
        return response()->json(["file" => $file, "success" => "1"]);
    }
}
    /**
 * Lưu ảnh vào folder temp
 *
 * @param $file
 * @return string
 */
private function uploadImageTemp($file)
{
    $time = Carbon::now();
    $file_name = rand(0, 9) . time() .
        date_format($time, 'd') .
        date_format($time, 'm') .
        date_format($time, 'Y') . "_file." . $file->getClientOriginalExtension();

    $file->move(TEMP_PATH, $file_name);
    return url('/').'/' . $this->transferTempfileToAdminfile($file_name, str_replace('', '', $file_name));
}
 //Chuyển file từ folder temp sang folder chính
 private function transferTempfileToAdminfile($filename)
 {
     $old_path = TEMP_PATH . '/' . $filename;
     $new_path = DIVIDEND_UPLOADS_PATH . date('Ymd') . '/' . $filename;
     Storage::disk('public')->makeDirectory(DIVIDEND_UPLOADS_PATH . date('Ymd'));
     Storage::disk('public')->move($old_path, $new_path);
     return $new_path;
 }
 public function saveFile(Request $request){ 
    $param = $request->all();
    $arrName = json_decode($param['arrName']);
    $arrFileId = $param['arrFileId'];
    $stockHistoryDivideId = $param['stock_history_divide_id'];
    $this->stock->removeDividendFile($arrFileId, $stockHistoryDivideId);

    if (count($arrName) == 0) return ['error' => 0, 'message' =>'Lưu file thành công'];
    if (!isset($param['arrFile']) || count($param['arrFile']) == 0) {
        return ['error' => 0, 'message' =>'Yêu cầu thêm tài liệu thành công'];
    }
    $data = [];
    $i = 0;
    foreach ($param['arrFile'] as $item) {
        $data[] = [
            'stock_history_divide_id'=>$param['stock_history_divide_id'],
            "file"=>$item,
            'display_name' => $arrName[$i],
            "created_at"=>Carbon::now()->format("Y-m-d"),
            "created_by"=>Auth::id()
        ];
        $i++;
    }

    $this->stock->createDividendFIle($data);
    return ['error' => 0, 'message' =>'Lưu file thành công'];

}
// !!-------Upload File--------
}