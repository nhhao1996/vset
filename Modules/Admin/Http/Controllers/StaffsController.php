<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/29/2018
 * Time: 10:37 AM
 */

namespace Modules\Admin\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Repositories\Department\DepartmentRepositoryInterface;
use Modules\Admin\Repositories\MapRoleGroupStaff\MapRoleGroupStaffRepositoryInterface;
use Modules\Admin\Repositories\RoleGroup\RoleGroupRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use Modules\Admin\Repositories\StaffTitle\StaffTitleRepositoryInterface;

class StaffsController extends Controller
{
    protected $staff;
    protected $department;
    protected $staff_title;
    protected $roleGroup;
    protected $mapRoleGroupStaff;

    public function __construct(
        StaffRepositoryInterface $staffs,
        DepartmentRepositoryInterface $departments,
        StaffTitleRepositoryInterface $staff_titles,
        RoleGroupRepositoryInterface $roleGroup,
        MapRoleGroupStaffRepositoryInterface $mapRoleGroupStaff
    )
    {
        $this->staff = $staffs;
        $this->department = $departments;
        $this->staff_title = $staff_titles;
        $this->roleGroup = $roleGroup;
        $this->mapRoleGroupStaff = $mapRoleGroupStaff;
    }

    //function view index
    public function indexAction()
    {
        $getStaff = $this->staff->list();
        return view('admin::staffs.index', [
            'LIST' => $getStaff,
            'FILTER' => $this->filters(),

        ]);
    }

    //function filter
    protected function filters()
    {
        return [
            'staffs$is_actived' => [
                'data' => [
                    '' => __('Chọn trạng thái'),
                    1 => __('Hoạt động'),
                    0 => __('Tạm ngưng')
                ]
            ]
        ];
    }

    //function view list
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword', 'staffs$is_actived', 'search']);
        $staffList = $this->staff->list($filter);
        return view('admin::staffs.list', ['LIST' => $staffList, 'page' => $filter['page']]);
    }

    //function add view
    public function addAction()
    {
//        $getDepar = $this->department->getStaffDepartmentOption();
//        $getTitle = $this->staff_title->getStaffTitleOption();
        $roleGroup = $this->roleGroup->getOptionActive();
        return view('admin::staffs.add', [
//            'depart' => $getDepar,
//            'title' => $getTitle,
            'roleGroup' => $roleGroup
        ]);
    }

    //function add
    public function submitAddAction(Request $request)
    {
        try{
            DB::beginTransaction();
            $data = [
                'full_name' => $request->full_name,
                'phone1' => $request->phone,
                'gender' => $request->gender,
//                'staff_title_id' => $request->staff_title_id,
//                'department_id' => $request->department_id,
                'address' => $request->address,
                'email' => $request->email,
                'user_name' => $request->user_name,
//                'is_admin' => $request->is_admin,
                'is_admin' => 0,
                'password' => Hash::make($request->password),
                'is_actived' => 1,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id()
            ];
            if ($request->year != null && $request->month != null && $request->day != null) {
                $birthday = $request->year . '-' . $request->month . '-' . $request->day;
                $data['birthday'] = $birthday;
                if ($birthday > date("Y-m-d")) {
                    return response()->json([
                        'error_birthday' => 1,
                        'message' => __('Ngày sinh không hợp lệ')
                    ]);
                }
            }
            $test_user = $this->staff->testUserName($request->user_name, 0);
            if ($test_user != "") {
                return response()->json([
                    'error_user' => 1,
                    'message' => __('Tài khoản đã tồn tại')
                ]);
            }
            if ($request->staff_avatar != null) {
                $data['staff_avatar'] = url('/').'/' . $this->transferTempfileToAdminfile($request->staff_avatar, str_replace('', '', $request->staff_avatar));
            }
            $roleGroup = $request->roleGroup;

            $password = $request->password;
            $idStaff = $this->staff->add($data, $password);

            //Nhóm quyền.
            if ($roleGroup != null) {
                foreach ($roleGroup as $key => $value) {
                    $checkMapRoleGroup = $this->mapRoleGroupStaff->checkIssetMap($value, $idStaff);
                    if ($checkMapRoleGroup == null) {
                        $data = [
                            'role_group_id' => $value,
                            'staff_id' => $idStaff,
                            'is_actived' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $this->mapRoleGroupStaff->add($data);
                    }
                }
            }
            DB::commit();
            return response()->json([
                'success' => 1,
                'message' => __('Thêm thành công')
            ]);
        } catch (\Exception $e){
            DB::rollBack();
            return response()->json([
                'success' => 0,
                'message' => __('Thêm thất bại')
            ]);
        }

    }

    //function upload image
    public function uploadAction(Request $request)
    {
        $this->validate($request, [
            "staff_avatar" => "mimes:jpg,jpeg,png,gif|max:10000"
        ], [
            "staff_avatar.mimes" => __("File này không phải file hình"),
            "staff_avatar.max" => __("File quá lớn")
        ]);
        $file = $this->uploadImageTemp($request->file('file'));
        return response()->json(["file" => $file, "success" => "1"]);
    }

    //Lưu file image vào folder temp
    private function uploadImageTemp($file)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() . date_format($time, 'd') . date_format($time, 'm') . date_format($time, 'Y') . "_staff." . $file->getClientOriginalExtension();
        $file->move(TEMP_PATH, $file_name);
        return $file_name;

    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = STAFF_UPLOADS_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(STAFF_UPLOADS_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    //function delete image
    public function deleteTempFileAction(Request $request)
    {
        Storage::disk("public")->delete(TEMP_PATH . '/' . $request->input('filename'));
        return response()->json(['success' => '1']);
    }

    //function xóa
    public function removeAction($id)
    {
        $this->staff->remove($id);
        return response()->json([
            'error' => 0,
            'message' => 'Remove success'
        ]);
    }

    //function change status
    public function changeStatusAction(Request $request)
    {
        $change = $request->all();
        $data['is_actived'] = ($change['action'] == 'unPublish') ? 1 : 0;
        $this->staff->edit($data, $change['id']);
        return response()->json([
            'status' => 0
        ]);
    }


    //function get item khi edit
    public function editAction($id)
    {
        $item = $this->staff->getItem($id);
        if ($item['birthday'] != null) {
            $birthday = explode('/', date("d/m/Y", strtotime($item['birthday'])));
            $day = $birthday[0];
            $month = $birthday[1];
            $year = $birthday[2];
        } else {
            $day = null;
            $month = null;
            $year = null;
        }

//        $getDepar = $this->department->getStaffDepartmentOption();
//        $getTitle = $this->staff_title->getStaffTitleOption();
        $roleGroup = $this->roleGroup->getOptionActive();
        $mapGroupStaff = $this->mapRoleGroupStaff->getRoleGroupByStaffId($id);

        $arrayMapRoleGroupStaff = [];
        if (count($mapGroupStaff) > 0) {
            foreach ($mapGroupStaff as $values) {
                $arrayMapRoleGroupStaff[] = $values['role_group_id'];
            }
        }

        return view('admin::staffs.edit', compact('item'), [
//            'depart' => $getDepar,
//            'title' => $getTitle,
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'roleGroup' => $roleGroup,
            'arrayMapRoleGroupStaff' => $arrayMapRoleGroupStaff
        ]);


    }

    public function editImageAction(Request $request)
    {
        $id = $request->id;
        $item = $this->staff->getItem($id);
        $data = [
            'staff_avatar' => $item->staff_avatar
        ];
        return response()->json($data);
    }

    //function submit edit
    public function submitEditAction(Request $request)
    {
        try{
            DB::beginTransaction();
            $id = $request->staff_id;

            //Nhóm quyền.
            $idRoleGroup = [];
            $roleGroup = $request->roleGroup;
            if ($roleGroup != null) {
                $this->mapRoleGroupStaff->removeByUser($id);
                foreach ($roleGroup as $key => $value) {
                    $data = [
                        'role_group_id' => $value,
                        'staff_id' => $id,
                        'is_actived' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $idRol = $this->mapRoleGroupStaff->add($data);
                    $idRoleGroup[] = $idRol;
                }
            } else {
                $this->mapRoleGroupStaff->removeByUser($id);
            }
            $data = [
                'full_name' => $request->full_name,
                'phone1' => $request->phone,
                'gender' => $request->gender,
//                'staff_title_id' => $request->staff_title_id,
//                'department_id' => $request->department_id,
                'address' => $request->address,
                'email' => $request->email,
//            'birthday'=>$birthday,
                'user_name' => $request->user_name,
//                'is_admin' => $request->is_admin,
                'is_actived' => $request->is_actived
            ];
            if ($request->password != null) {
                $data['password'] = Hash::make($request->password);
            }

            if ($request->year != null && $request->month != null && $request->day != null) {
                $birthday = $request->year . '-' . $request->month . '-' . $request->day;
                $data['birthday'] = $birthday;
                if ($birthday > date("Y-m-d")) {
                    return response()->json([
                        'error_birthday' => 1,
                        'message' => __('Ngày sinh không hợp lệ')
                    ]);
                }
            }
            $test_user = $this->staff->testUserName($request->user_name, $id);
            if ($test_user != "") {
                return response()->json([
                    'error_user' => 1,
                    'message' => __('Tài khoản đã tồn tại')
                ]);
            }
            if ($request->staff_avatar_upload != '') {
                $data['staff_avatar'] = url('/').'/' .$this->transferTempfileToAdminfile($request->staff_avatar_upload, str_replace('', '', $request->staff_avatar_upload));
            } else {
                $data['staff_avatar'] = $request->staff_avatar;
            }

            $this->staff->edit($data ,$request->password , $id);
            DB::commit();
            return response()->json([
                'success' => 1,
                'message' => __('Cập nhật thành công')
            ]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json([
                'success' => 0,
                'message' => __('Cập nhật thất bại')
            ]);
        }
    }

    //function get profile.
    public function profileAction()
    {
        $id = Auth::id();
        $item = $this->staff->getItem($id);
        if ($item['birthday'] != null) {
            $birthday = explode('/', date("d/m/Y", strtotime($item['birthday'])));
            $day = $birthday[0];
            $month = $birthday[1];
            $year = $birthday[2];
        } else {
            $day = null;
            $month = null;
            $year = null;
        }

//        $getDepar = $this->department->getStaffDepartmentOption();
//        $getTitle = $this->staff_title->getStaffTitleOption();
        $roleGroup = $this->roleGroup->getOptionActive();
        $mapGroupStaff = $this->mapRoleGroupStaff->getRoleGroupByStaffId($id);

        $arrayMapRoleGroupStaff = [];
        if (count($mapGroupStaff) > 0) {
            foreach ($mapGroupStaff as $values) {
                $arrayMapRoleGroupStaff[] = $values['role_group_id'];
            }
        }

        return view('admin::staffs.edit', compact('item'), [
//            'depart' => $getDepar,
//            'title' => $getTitle,
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'roleGroup' => $roleGroup,
            'arrayMapRoleGroupStaff' => $arrayMapRoleGroupStaff
        ]);


    }

}
//