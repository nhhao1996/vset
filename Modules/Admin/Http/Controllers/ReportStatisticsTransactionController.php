<?php
/**
 * Created by PhpStorm   .
 * User: nhandt
 * Date: 4/19/2021
 * Time: 8:50 AM
 * @author nhandt
 */


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Repositories\StatisticsTransaction\ReportStatisticsTransactionRepoInterface;
use Modules\Admin\Repositories\StockRevenue\ReportStockRevenueRepoInterface;

class ReportStatisticsTransactionController extends Controller
{
    private $reportStatisticsTransaction;
    public function __construct(ReportStatisticsTransactionRepoInterface $reportStatisticsTransaction)
    {
        $this->reportStatisticsTransaction = $reportStatisticsTransaction;
    }

    public function indexAction(Request $request){
        return view("admin::statistics-transaction.index", [
        ]);
    }
    public function filter(Request $request){
        $params = $request->all();
        $result = $this->reportStatisticsTransaction->filter($params);
        return response()->json($result);
    }
}