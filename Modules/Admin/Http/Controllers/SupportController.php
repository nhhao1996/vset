<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Models\SupportRequest;
use Modules\Admin\Repositories\SupportRequest\SupportRepositoryInterface;

class SupportController extends Controller
{
    protected $support;

    public function __construct(SupportRepositoryInterface $support)
    {
        $this->support  = $support;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $filters = $request->all();
        $support = $this->support->list($filters);
//        dd($support);
        return view('admin::support.index', [
            'LIST' => $support,
            'FILTER' => $filters,
        ]);
    }

    public function listAction(Request $request)
    {
        $filters = $request->all();
        $support = $this->support->list($filters);
        return view('admin::support.list', [
            'LIST' => $support,
            'FILTER' => $filters,
            'page' => $filters['page']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function showAction($id)
    {
        $support = $this->support->editView($id);
        if ($support != null) {
            return view('admin::support.show', [
                'item' => $support
            ]);
        }else{
            return redirect()->route('admin.support');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function editAction($id)
    {
        $support = $this->support->editView($id);
        if ($support != null) {
            return view('admin::support.edit', [
                'item' => $support
            ]);
        }else{
            return redirect()->route('admin.support');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request)
    {

        $this->support->updateStatus($request->input("id"),$request->input("status"));

        return response()->json([
            'success' => 1,
            'message' => __('Cập nhật hỗ trợ thành công')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {
        $this->support->delete($request->input("id"));

        return response()->json([
            'success' => 1,
            'message' => __('Cập nhật khách hàng thành công')
        ]);
    }
}
