<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Repositories\OrderService\OrderServiceRepositoryInterface;

class OrderServiceController extends Controller
{
    protected $orderService;
    protected $request;

    public function __construct(OrderServiceRepositoryInterface $orderService,Request $request)
    {
        $this->orderService = $orderService;
        $this->request = $request;
    }

    public function indexAction()
    {
        $get = $this->orderService->listAction();
        return view('admin::order-service.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    public function filters()
    {
        return [

        ];
    }

    public function listAction()
    {
        $filter = $this->request->all();
        $list = $this->orderService->listAction($filter);
        return view('admin::order-service.list', [
            'LIST' => $list,
            'page' => $filter['page']
        ]);
    }

    /**
     * Hiển thị thông tin chi tiết
     *
     * @param int $id
     * @return Response
     * @return Response
     */
    public function show($id)
    {
        $data = $this->orderService->show($id);

        $moneyReceipt = $this->orderService->getMoneyReceipt($id);
        //        Chuyển khoản
        if ($data['payment_method_id'] == 1) {
            $total = null;
//            ví lãi
        } else if ($data['payment_method_id'] == 2){
//            Danh sách id hợp đồng
            $total = $this->orderService->getTotalInterest($data['order_service_id']);
//            ví thưởng
        } else if ($data['payment_method_id'] == 3) {
//            $total = $this->orderService->getTotalCommission($data['order_service_id'],$data['customer_id']);
            $total = $this->orderService->getTotalCommissionWallet($data['order_service_id'],$data['customer_id']);
        }
//        Danh sách chuyển tiền
        return view('admin::order-service.detail', [
            'detail' => $data,
            'moneyReceipt' => $moneyReceipt,
            'total' => $total
        ]);
    }

    public function createWallet(Request $request){
        $param = $request->all();
        $createWallet = $this->orderService->createWallet($param);
        return $createWallet;
    }

    public function addReceiptDetail() {
        $param = $this->request->all();
        $addReceiptDetail = $this->orderService->addReceiptDetail($param);
        return $addReceiptDetail;
    }

    public function getListReceiptDetail() {
        $param = $this->request->all();
        $listReceipDetail = $this->orderService->getListReceiptDetail($param);
        return $listReceipDetail;
    }

    public function showPopupReceiptDetail() {
        $param = $this->request->all();
        $showPopup = $this->orderService->showPopupReceiptDetail($param);
        return $showPopup;
    }
//
//    /**
//     * createReceipt
//     *
//     * @param int $amount
//     * @return Response
//     * @return Response
//     */
    public function createReceipt()
    {
        $param = $this->request->all();
        $createReceipt = $this->orderService->createReceipt($param);
        return $createReceipt;
    }
//
//    /**
//     * Upload image dropzone
//     *
//     * @param Request $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function uploadDropzoneAction()
//    {
//        return $this->orderService->uploadDropzone($this->request->all());
//    }
}