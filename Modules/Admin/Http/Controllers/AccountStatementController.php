<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Repositories\AccountStatement\AccountStatementRepositoryInterface;

class AccountStatementController extends Controller
{
    protected $accountStatement;
    public function __construct(AccountStatementRepositoryInterface $accountStatement)
    {
        $this->accountStatement = $accountStatement;
    }

    public function indexAction(Request $request)
    {

        $get = $this->accountStatement->list();
        return view('admin::account-statement.index', [
            'LIST' => $get,
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search']);

        $list = $this->accountStatement->list($filter);
        return view('admin::account-statement.list', [
            'LIST' => $list,
            'page' => $filter['page'],
            'FILTER' => $filter,
        ]);
    }

    public function detail($id) {
        $detail = $this->accountStatement->getDetail($id);
        return view('admin::account-statement.detail', [
            'detail' => $detail,
        ]);
    }

}
