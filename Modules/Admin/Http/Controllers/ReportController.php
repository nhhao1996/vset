<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 9/24/2018
 * Time: 10:37 AM
 */

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Models\CustomerContactLogTable;
use Modules\Admin\Models\CustomerContactTable;
use Modules\Admin\Repositories\Report\ReportRepositoryInterface;
use Carbon\Carbon;

class ReportController extends Controller
{
    protected $rReport;
    public function __construct(ReportRepositoryInterface $rReport)
    {
        $this->rReport = $rReport;
    }

    /**
     * Chuyển trang BÁO CÁO DOANH THU - THANH TOÁN
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function revenue()
    {
        return view('admin::report.revenue.index');
    }

    /**
     * Filter BÁO CÁO DOANH THU - THANH TOÁN
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterRevenue(Request $request)
    {
        $params = $request->all();
        $result = $this->rReport->filterRevenue($params);
        return response()->json($result);
    }

    /**
     *
     * Chuyển trang BÁO CÁO LÃI SUẤT
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function interest()
    {
        return view('admin::report.interest.index');
    }

    /**
     * Filter BÁO CÁO LÃI SUẤT
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterInterest(Request $request)
    {
        $params = $request->all();
        $result = $this->rReport->filterInterest($params);
        return response()->json($result);
    }

    /**
     * Chuyển trang BÁO CÁO TRẢ LÃI DỰ KIẾN
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function expectedInterestPayment()
    {
        return view('admin::report.expected-interest-payment.index');
    }

    /**
     * Filter BÁO CÁO TRẢ LÃI DỰ KIẾN
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterExpectedInterestPayment(Request $request)
    {
        $params = $request->all();
        $request->session()->put('filter_expect', $params);
        $result = $this->rReport->filterExpectedInterestPayment($params);
        return response()->json($result);
    }

    public function customerContractLog($id)
    {
        //Model customer contract
        $mCustomerContact = new CustomerContactTable();
        //Model customer contract log
        $mCustomerContactLog = new CustomerContactLogTable();
        //Chi tiết của customer contract
        $customerContract = $mCustomerContact->getItem($id);
        //Số tháng đầu tư
        $investmentTime = $customerContract['investment_time'];
        //Kỳ hạn rút lãi -> là số tháng
        $withdrawInterestTime = $customerContract['withdraw_interest_time'];
        //Số record
        $record = (int)($investmentTime / $withdrawInterestTime);
        //Số tháng dư
        $residual = $investmentTime % $withdrawInterestTime;
        //Năm bắt đầu hợp đồng
        $year = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
            ->format('Y');
        //Tháng bắt đầu hợp đồng
        $month = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_start_date'])
            ->format('m');
        //Năm kết thúc hợp đồng
        $yearEnd = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
            ->format('Y');
        //Tháng kết thúc hợp đồng
        $monthEnd = $startTime = Carbon::createFromFormat(
            'Y-m-d H:i:s', $customerContract['customer_contract_end_date'])
            ->format('m');
        $data = [];
        if ($record > 0) {
            $mT = 0;
            $interest = $withdrawInterestTime * $customerContract['month_interest'];
            for ($i = 0; $i < $record; $i++) {
                //Tháng lãi tiếp theo
                $mT += $withdrawInterestTime;
                $dt = Carbon::create($year, $month, 1, 0);
                //Cộng thêm tháng
                $addMonth = $dt->addMonths($mT);
                $y = $startTime = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $addMonth)
                    ->format('Y');
                $m = (int)$startTime = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $addMonth)
                    ->format('m');
                $data[] = [
                    'customer_contract_id' => $id,
                    'year' => $y,
                    'month' => $m,
                    'interest' => $interest
                ];
            }
        }
        //Lần cuối tính lãi
        if ($residual > 0) {
            $data[] = [
                'customer_contract_id' => $id,
                'year' => $yearEnd,
                'month' => $monthEnd,
                'interest' => $residual * $customerContract['month_interest']
            ];
        }
        if ($data != []) {
            $mCustomerContactLog->addInsert($data);
        }
    }

    public function exportExpectedInterestPayment(Request $request)
    {
        $param = [];
        if ($request->session()->has('filter_expect')) {
            $param = $request->session()->get('filter_expect');
        }

        $result = $this->rReport->exportExpectedInterestPayment($param);
        return $result;
    }

    public function updateReport()
    {
        $result = $this->rReport->updateReport();
        return $result;
    }

    /**
     * Chuyển trang BÁO CÁO NHÀ ĐẦU TƯ ĐĂNG KÝ
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function shareLink(){
        $listSource = $this->rReport->getListSource();
        return view('admin::report.share-link.index',['listSource' => $listSource]);
    }

    public function shareLinkFilter(Request $request){
        $param = $request->all();
        $rawChart = $this->rReport->rawChart($param);
        return $rawChart;
    }

    public function salesAction(){
        $listStaff = $this->rReport->getListStaff();
        return view('admin::report.sale.index',['listStaff' => $listStaff]);
    }

    public function salesFilter(Request $request){
        $param = $request->all();
        $sales = $this->rReport->salesFilter($param);
        return $sales;
    }

    public function sumSalesFilter(Request $request){
        $param = $request->all();
        $sumSales = $this->rReport->sumSalesFilter($param);
        return $sumSales;
    }

    public function sumGroupFilter(Request $request){
        $param = $request->all();
        $sumGroup = $this->rReport->sumGroupFilter($param);
        return $sumGroup;
    }

}