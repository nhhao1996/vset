<?php

namespace Modules\Admin\Http\Controllers;

use App\Jobs\CheckMailJob;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Modules\Admin\Http\Api\BookingApi;
use Modules\Admin\Http\Api\LoyaltyApi;
use Modules\Admin\Http\Api\SendNotificationApi;
use Modules\Admin\Models\CustomerAppointmentLogTable;
use Modules\Admin\Models\CustomerAppointmentTable;
use Modules\Admin\Models\PointHistoryTable;
use Modules\Admin\Models\PromotionLogTable;
use Modules\Admin\Repositories\AppointmentService\AppointmentServiceRepositoryInterface;
use Modules\Admin\Repositories\AppointmentSource\AppointmentSourceRepositoryInterface;
use Modules\Admin\Repositories\CodeGenerator\CodeGeneratorRepositoryInterface;
use Modules\Admin\Repositories\ConfigPrintBill\ConfigPrintBillRepositoryInterface;
use Modules\Admin\Repositories\Customer\CustomerRepository;
use Modules\Admin\Repositories\CustomerAppointment\CustomerAppointmentRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentDetail\CustomerAppointmentDetailRepositoryInterface;
use Modules\Admin\Repositories\CustomerAppointmentTime\CustomerAppointmentTimeRepositoryInterface;
use Modules\Admin\Repositories\CustomerBranchMoney\CustomerBranchMoneyRepositoryInterface;
use Modules\Admin\Repositories\CustomerDebt\CustomerDebtRepositoryInterface;
use Modules\Admin\Repositories\CustomerServiceCard\CustomerServiceCardRepositoryInterface;
use Modules\Admin\Repositories\InventoryOutput\InventoryOutputRepositoryInterface;
use Modules\Admin\Repositories\InventoryOutputDetail\InventoryOutputDetailRepositoryInterface;
use Modules\Admin\Repositories\Notification\NotificationRepoInterface;
use Modules\Admin\Repositories\Order\OrderRepositoryInterface;
use Modules\Admin\Repositories\OrderApp\OrderAppRepoInterface;
use Modules\Admin\Repositories\OrderCommission\OrderCommissionRepositoryInterface;
use Modules\Admin\Repositories\OrderDetail\OrderDetailRepositoryInterface;
use Modules\Admin\Repositories\PrintBillLog\PrintBillLogRepositoryInterface;
use Modules\Admin\Repositories\ProductBranchPrice\ProductBranchPriceRepositoryInterface;
use Modules\Admin\Repositories\ProductChild\ProductChildRepositoryInterface;
use Modules\Admin\Repositories\ProductInventory\ProductInventoryRepositoryInterface;
use Modules\Admin\Repositories\Receipt\ReceiptRepositoryInterface;
use Modules\Admin\Repositories\ReceiptDetail\ReceiptDetailRepositoryInterface;
use Modules\Admin\Repositories\Room\RoomRepositoryInterface;
use Modules\Admin\Repositories\Service\ServiceRepositoryInterface;
use Modules\Admin\Repositories\ServiceBranchPrice\ServiceBranchPriceRepositoryInterface;
use Modules\Admin\Repositories\ServiceCard\ServiceCardRepositoryInterface;
use Modules\Admin\Repositories\ServiceCardList\ServiceCardListRepositoryInterface;
use Modules\Admin\Repositories\ServiceMaterial\ServiceMaterialRepositoryInterface;
use Modules\Admin\Repositories\SmsConfig\SmsConfigRepositoryInterface;
use Modules\Admin\Repositories\SmsLog\SmsLogRepositoryInterface;
use Modules\Admin\Repositories\SpaInfo\SpaInfoRepositoryInterface;
use Modules\Admin\Repositories\Staffs\StaffRepositoryInterface;
use Modules\Admin\Repositories\Voucher\VoucherRepositoryInterface;
use Modules\Admin\Repositories\Warehouse\WarehouseRepositoryInterface;

class VsetCustomerAppointmentController extends Controller
{
    protected $customer_appointment;
    protected $customer;
    protected $staff;
    protected $room;
    protected $service;
    protected $appointment_service;
    protected $customer_appointment_time;
    protected $code;
    protected $customer_branch_money;
    protected $customer_service_card;
    protected $order;
    protected $voucher;
    protected $receipt;
    protected $order_detail;
    protected $receipt_detail;
    protected $service_card;
    protected $customer_appointment_detail;
    protected $appointment_source;
    protected $service_card_list;
    protected $service_branch_price;
    protected $smsLog;
    protected $spaInfo;
    protected $configPrintBill;
    protected $printBillLog;
    protected $inventoryOutput;
    protected $warehouse;
    protected $inventoryOutputDetail;
    protected $productInventory;
    protected $productChild;
    protected $serviceMaterial;
    protected $product_branch_price;
    protected $smsConfig;
    protected $customer_debt;
    protected $order_commission;

    public function __construct(
        CustomerAppointmentRepositoryInterface $customer_appointments,
        CustomerRepository $customers, StaffRepositoryInterface $staffs,
        RoomRepositoryInterface $rooms, ServiceRepositoryInterface $services,
        AppointmentServiceRepositoryInterface $appointment_services,
        CustomerAppointmentTimeRepositoryInterface $customer_appointment_times,
        CodeGeneratorRepositoryInterface $codes,
        CustomerBranchMoneyRepositoryInterface $customer_branch_moneys,
        CustomerServiceCardRepositoryInterface $customer_service_cards,
        OrderRepositoryInterface $orders,
        VoucherRepositoryInterface $vouchers,
        ReceiptRepositoryInterface $receipts,
        OrderDetailRepositoryInterface $order_details,
        ReceiptDetailRepositoryInterface $receipt_details,
        ServiceCardRepositoryInterface $service_cards,
        CustomerAppointmentDetailRepositoryInterface $customer_appointment_details,
        AppointmentSourceRepositoryInterface $appointment_sources,
        ServiceCardListRepositoryInterface $service_card_lists,
        ServiceBranchPriceRepositoryInterface $service_branch_prices,
        SmsLogRepositoryInterface $smsLog,
        SpaInfoRepositoryInterface $spaInfo,
        ConfigPrintBillRepositoryInterface $configPrintBill,
        PrintBillLogRepositoryInterface $printBillLog,
        InventoryOutputRepositoryInterface $inventoryOutput,
        WarehouseRepositoryInterface $warehouse,
        InventoryOutputDetailRepositoryInterface $inventoryOutputDetail,
        ProductInventoryRepositoryInterface $productInventory,
        ProductChildRepositoryInterface $productChild,
        ServiceMaterialRepositoryInterface $serviceMaterial,
        ProductBranchPriceRepositoryInterface $product_branch_prices,
        SmsConfigRepositoryInterface $smsConfig,
        CustomerDebtRepositoryInterface $customer_debt,
        OrderCommissionRepositoryInterface $order_commission
    )
    {
        $this->customer_appointment = $customer_appointments;
        $this->customer = $customers;
        $this->staff = $staffs;
        $this->room = $rooms;
        $this->service = $services;
        $this->appointment_service = $appointment_services;
        $this->customer_appointment_time = $customer_appointment_times;
        $this->code = $codes;
        $this->customer_branch_money = $customer_branch_moneys;
        $this->customer_service_card = $customer_service_cards;
        $this->order = $orders;
        $this->receipt = $receipts;
        $this->voucher = $vouchers;
        $this->order_detail = $order_details;
        $this->receipt_detail = $receipt_details;
        $this->service_card = $service_cards;
        $this->customer_appointment_detail = $customer_appointment_details;
        $this->appointment_source = $appointment_sources;
        $this->service_card_list = $service_card_lists;
        $this->service_branch_price = $service_branch_prices;
        $this->smsLog = $smsLog;
        $this->spaInfo = $spaInfo;
        $this->configPrintBill = $configPrintBill;
        $this->printBillLog = $printBillLog;
        $this->inventoryOutput = $inventoryOutput;
        $this->warehouse = $warehouse;
        $this->inventoryOutputDetail = $inventoryOutputDetail;
        $this->productInventory = $productInventory;
        $this->productChild = $productChild;
        $this->serviceMaterial = $serviceMaterial;
        $this->product_branch_price = $product_branch_prices;
        $this->smsConfig = $smsConfig;
        $this->customer_debt = $customer_debt;
        $this->order_commission = $order_commission;
    }

    public function indexAction()
    {
        $get = $this->customer_appointment->list();
        return view('admin::customer-appointment.index', [
            'LIST' => $get,
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * @return array
     */
    protected function filters()
    {
//        $time = $this->customer_appointment_time->getTimeOption();
//        $group = (["" => __("Chọn khung giờ")]) + $time;
        return [
            'customer_appointments$customer_appointment_id' => [
                'data' => ''
            ]
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search_type', 'search_keyword',
            'search']);
        $list = $this->customer_appointment->list($filter);
        return view('admin::customer-appointment.list', ['LIST' => $list]);
    }

    public function addAction()
    {
        return view('admin::customer-appointment.add', []);
    }
}