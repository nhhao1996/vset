<?php

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\ConfigTermInvestment\StoreRequest;
use Modules\Admin\Repositories\ConfigTermInvestment\ConfigTermInvestmentRepositoryInterface;
use Modules\Admin\Repositories\Product\ProductRepository;

class ConfigTermInvestmentController extends Controller
{

    protected $configTermInvestment;
    protected $productRepo;

    public function __construct(ConfigTermInvestmentRepositoryInterface $configTermInvestment,ProductRepository $productRepo)
    {
        $this->configTermInvestment = $configTermInvestment;
        $this->productRepo = $productRepo;
    }


    public function index()
    {
        $filter = request()->all();
        $data = $this->configTermInvestment->list($filter);
//        Loại gói
        $productCategories = $this->productRepo->getCategoryProduct();
        return view('admin::config-term-investment.index', [
            'LIST' => $data,
            'productCategories' => $productCategories,
            'FILTER' => $this->filters(),
        ]);
    }

    // FUNCTION  FILTER LIST ITEM
    protected function filters()
    {
    }

    /**
     * Danh sách hỗ trợ
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function list(Request $request)
    {
        $filters = $request->all();
//        $filters = $request->only(['page', 'display', 'keyword_faq$faq_title', 'faq$is_actived','faq$faq_group']);

        $data = $this->configTermInvestment->list($filters);
//        Loại gói
        $productCategories = $this->configTermInvestment->getCategoryProduct();
        return view('admin::config-term-investment.list', [
                'LIST' => $data,
                'productCategories' => $productCategories,
                'FILTER' => $filters,
                'page' => $filters['page']
            ]
        );
    }

//    Hiển thị popup cấu hình kì hạn đầu tư
    public function showPopup(Request $request){
        $param = $request->all();
        $showPopup = $this->configTermInvestment->showPopup($param);
        return \response()->json($showPopup);
    }

//    Tạo kì hạn đầu tư theo gói
    public function store(Request $request){
        $param = $request->all();
        $store = $this->configTermInvestment->store($param);
        return \response()->json($store);
    }

//    Cập nhật cấu hình kì hạn đầu tư
    public function update(Request $request){
        $param = $request->all();
        $store = $this->configTermInvestment->update($param);
        return \response()->json($store);
    }

    public function delete(Request $request){
        $param  = $request->all();
        $delete = $this->configTermInvestment->delete($param['id']);
        return \response()->json($delete);
    }

    public function changStatus(Request $request){
        $param  = $request->all();
        $changStatus = $this->configTermInvestment->changStatus($param);
        return \response()->json($changStatus);
    }


}
