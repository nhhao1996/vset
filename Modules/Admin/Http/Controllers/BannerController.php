<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\Banner\StoreRequest;
use Modules\Admin\Http\Requests\Banner\UpdateRequest;
use Modules\Admin\Repositories\Banner\BannerRepositoryInterface;

class BannerController extends Controller
{
    protected $banner;

    public function __construct(
        BannerRepositoryInterface $banner
    )
    {
        $this->banner = $banner;
    }
    public function indexAction()
    {
        $list = $this->banner->list();
        return view('admin::banner.index', [
            'LIST' => $list,
            'FILTER' => $this->filters(),
        ]);
    }

    protected function filters()
    {

        return [

        ];
    }

    public function listAction(Request $request)
    {
        $filter = $request->only(['page', 'display', 'search','is_active','time']);
        $list = $this->banner->list($filter);
        return view('admin::banner.list',
            ['LIST' => $list,
            'page' =>$filter['page']]);
    }

    public function addAction()
    {
        return view('admin::banner.add', [
        ]);
    }

    public function submitAddAction(StoreRequest $request) {
        $param = $request->all();
        $submitAdd = $this->banner->submitAdd($param);
        return $submitAdd;
    }

    public function detailAction($id) {
        $detail = $this->banner->getDetail($id);
        return view('admin::banner.detail', [
            'detail' => $detail
        ]);
    }

    public function editAction($id) {
        $detail = $this->banner->getDetail($id);
        return view('admin::banner.edit', [
            'detail' => $detail
        ]);
    }

    public function submitEditAction(UpdateRequest $request) {
        $param = $request->all();
        $submitEdit = $this->banner->submitEdit($param);
        return $submitEdit;
    }
}