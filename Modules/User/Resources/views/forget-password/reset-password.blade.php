@extends('layout-login')

@section('content')
    <div class="m-login__container">
        <div class="m-login__logo">
            <a href="#">
                <img src="{{asset('/static/backend/images/LOGO-VSETGROUP.png')}}">
            </a>
            <hr class="m-login-border">
        </div>

        <div class="change-password-view">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    @lang('Quên mật khẩu') ?
                </h3>
                <div class="m-login__desc">
                    @lang('Nhập mật khẩu mới để thay đổi mật khẩu')
                </div>
            </div>
            <form id="form-change-password">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="m-input-icon m-input-icon--left password-error">
                        <input id="new_password" class="form-control m-input m-input--pill m-login__form-input--last m--form-login-new new-password" type="password" placeholder="@lang('Mật Khẩu mới')" name="password" autocomplete="off">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                            <span>
                                <i class="la la-lock"></i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="m-input-icon m-input-icon--left re_password-error">
                        <input class="form-control m-input m-input--pill m-login__form-input--last m--form-login-new" type="password" placeholder="@lang('Xác nhận mật khẩu')" name="re_password" autocomplete="off">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                            <span>
                                <i class="la la-lock"></i>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="kt-login__actions pt-4">
                    <a style="border-color: #d7d7d7" id="kt_login_forgot_cancel"
                       class="btn-return-login"
                       href="{{route('login')}}">
                        @lang('Đăng nhập')
                    </a>&nbsp;&nbsp;
                    <button id="submit-forget-password" type="button"
                            style="background: #4fc4ca;border: 0"
                            class="btn-return-login btn-update-pasword float-right"
                            onclick="forgetPassword.changePassword('{{$token}}')">
                        @lang('Cập nhật')
                    </button>
                </div>
                <input type="hidden" name="token" value="{{$token}}">
            </form>
        </div>
    </div>
@stop

@section('after_script')

@stop