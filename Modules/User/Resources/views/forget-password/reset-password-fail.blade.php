@extends('layout-login')

@section('content')
    <div class="m-login__container">
        <div class="m-login__logo">
            <a href="#">
                <img src="{{asset('/static/backend/images/LOGO-VSETGROUP.png')}}">
            </a>
            <hr class="m-login-border">
        </div>

        <div class="change-password-view">
            <div class="m-login__head">
{{--                <h3 class="m-login__title">--}}
{{--                    @lang('Quên mật khẩu') ?--}}
{{--                </h3>--}}

            </div>
            <form id="form-change-password">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="m-input-icon m-input-icon--left password-error">
                        <p class="text-danger font-weight-bold text-center" style="font-size: 16px">Link thay đổi mật khẩu không đúng hoặc đã hết hiệu lực</p>
                    </div>
                </div>

                <div class="kt-login__actions pt-4 text-center">
                    <a style="border-color: #d7d7d7" id="kt_login_forgot_cancel"
                       class="btn-return-login"
                       href="{{route('login')}}">
                        @lang('Đăng nhập')
                    </a>
                </div>
            </form>
        </div>
    </div>
@stop

@section('after_script')

@stop