<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function () {
    Route::get('/', 'IndexController@indexAction')->name('user');

//        Route::get('/', 'DashbroadController@index')->name('dashbroad');
});

Route::group(['middleware' => ['web'], 'namespace' => 'Modules\User\Http\Controllers'], function () {
    Route::get('/login', 'LoginController@indexAction')->name('login');
    Route::post('/login', 'LoginController@postLogin')->name('login');
    Route::get('/sendEmailResetPassword', 'LoginController@sendEmailResetPassword')->name('sendEmailResetPassword');
    Route::match(['get'], '/logout', 'LoginController@logoutAction')->name('logout');
    Route::get('menu', 'MenuController@menuAction')->name('menu');
    Route::get('/reset-password/{token}', 'LoginController@resetPassword')->name('reset-password');
    Route::post('/submit-reset-password', 'LoginController@submitResetPassword')
        ->name('submit-reset-password');
});
