<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 10/4/2019
 * Time: 16:17
 */

namespace Modules\User\Repositories\AdminMenu;


use Modules\User\Models\AdminMenuTable;

class AdminMenuRepository implements AdminMenuRepositoryInterface
{
    protected $admin_menu;
    protected $timestamps = true;

    public function __construct(AdminMenuTable $admin_menu)
    {
        $this->admin_menu = $admin_menu;
    }

    public function groupCategory($menu_category_id)
    {
        // TODO: Implement groupCategory() method.
        return $this->admin_menu->groupCategory($menu_category_id);
    }
}