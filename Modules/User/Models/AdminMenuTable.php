<?php
/**
 * Created by PhpStorm.
 * User: Mr Son
 * Date: 10/4/2019
 * Time: 16:18
 */

namespace Modules\User\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class AdminMenuTable extends Model
{
    use ListTableTrait;
    protected $table = 'admin_menu';
    protected $primaryKey = 'admin_menu_id';
    protected $fillable = [
        'admin_menu_id', 'admin_menu_name', 'admin_menu_category_id', 'admin_menu_route',
        'admin_menu_icon', 'admin_menu_img', 'admin_menu_position', 'created_at', 'updated_at'
    ];

    public function groupCategory($menu_category_id)
    {
        $ds = $this->select(
            'admin_menu_id',
            'admin_menu_name',
            'admin_menu_category_id',
            'admin_menu_route', 'admin_menu_icon', 'admin_menu_img')
            ->where('admin_menu_category_id', $menu_category_id)
            ->orderBy('admin_menu_position','asc')
            ->get();
        return $ds;
    }
}