<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

/**
 * User Model
 *
 * @author isc-daidp
 * @since Feb 23, 2018
 */
class UserTable extends Model
{
    use ListTableTrait;

    protected $table = 'staffs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'password', 'is_inactive'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Build query table
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function _getList()
    {
        return $this->select('staff_id', 'full_name', 'email', 'is_inactive', 'date_last_login');
    }


    /**
     * Remove user
     *
     * @param number $id
     */
    public function remove($id)
    {
        $this->where($this->primaryKey, $id)->delete();
    }


    /**
     * Insert user to database
     *
     * @param array $data
     * @return number
     */
    public function add(array $data)
    {
        $oUser = $this->create($data);
        return $oUser->id;
    }

    public function getItem($id)
    {
        $ds = $this->select('full_name', 'gender', 'phone1', 'staff_avatar')->where('staff_id', $id)->first();
        return $ds;
    }
}