<?php


namespace Modules\User\Models;


use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class StaffsForgotPassTable extends Model
{
    use ListTableTrait;
    protected $table = 'staffs_forgot_pass';
    protected $primaryKey = 'forgot_pass_id';

    protected $fillable
        = [
            'forgot_pass_id',
            'staff_id',
            'code',
            'expire',
            'is_actived',
            'created_at',
            'updated_at'
        ];

    public function store(array $data)
    {
        $oUser = $this->create($data);
        return $oUser->forgot_pass_id;
    }

    public function unActiveForgotPass($adminId)
    {
        $select = $this->where('staff_id', $adminId)
            ->update(['is_actived' => 0]);
        return $select;
    }

    public function getItem($code)
    {
        $select = $this->where('code', $code)
            ->where('is_actived', 1)
            ->where('expire', '>=', date('Y-m-d H:i:s'))
            ->first();
        return $select;
    }

    public function getItemCode($code)
    {
        $select = $this->select(
            'staffs.staff_id as id',
            'staffs.email as email'
        )
            ->leftJoin('staffs', 'staffs.staff_id', '=', 'staffs_forgot_pass.staff_id')
            ->where('staffs_forgot_pass.code', $code)
            ->first();
        return $select;
    }
}