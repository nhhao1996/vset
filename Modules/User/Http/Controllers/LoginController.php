<?php

namespace Modules\User\Http\Controllers;

use App\Mail\ResetPassword;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Modules\Admin\Http\Api\JobsEmailLogApi;
use Modules\Admin\Models\ActionTable;
use Modules\Admin\Models\MapRoleGroupStaffTable;
use Modules\Admin\Models\PageTable;
use Modules\Admin\Models\StaffsTable;
use Modules\User\Models\StaffsForgotPassTable;
use Modules\User\Repositories\AdminMenu\AdminMenuRepositoryInterface;
use Modules\User\Repositories\AdminMenuCategory\AdminMenuCategoryRepositoryInterface;
use Illuminate\Support\Facades\Validator;

/**
 * Login page
 *
 * @author isc-daidp
 * @since Feb 23, 2018
 */
class LoginController extends Controller
{
    protected $admin_menu_category;
    protected $admin_menu;

    public function __construct(
        AdminMenuCategoryRepositoryInterface $admin_menu_category,
        AdminMenuRepositoryInterface $admin_menu
    )
    {
        $this->admin_menu_category = $admin_menu_category;
        $this->admin_menu = $admin_menu;

    }

    /**
     * Form login
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAction()
    {
        if (Auth::check()) {
            return redirect()->route(LOGIN_HOME_PAGE);
        }

        return view('user::login.index');
    }

    /**
     * Xử lý login
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 1,
                'message' => 'Yêu cầu nhập tài khoản đăng nhập'
            ]);
        }

        $certifications = $request->only(['user_name', 'password']);
        if (Auth::attempt($certifications)) {

            //Phân quyền
            $pages = new PageTable();
            $actions = new ActionTable();
            $mapRoleGroupStaff = new MapRoleGroupStaffTable();
//                $staffTitleId = Auth::user()->staff_title_id;
            $isAdmin = Auth::user()->is_admin;
            $staffId = Auth::user()->staff_id;
            if (!$request->session()->has('routeList')) {
                if ($isAdmin != 1) {
                    $getRolePage = $mapRoleGroupStaff->getRolePageByStaff($staffId);
                    $getRoleAction = $mapRoleGroupStaff->getRoleActionByStaff($staffId);
                    $arrayRole = array_merge($getRolePage, $getRoleAction);
                    $request->session()->put('routeList', $arrayRole);
                } else {
                    $getRolePage = $pages->getAllRoute();
                    $getRoleAction = $actions->getAllRoute();
                    $arrayRole = array_merge($getRolePage, $getRoleAction);
                    $request->session()->put('routeList', $arrayRole);
                }
            }

            //Menu
            $getCategory = $this->admin_menu_category->getAll();
            $data = [];
            foreach ($getCategory as $item) {
                $check = $this->admin_menu->groupCategory($item['menu_category_id']);
                $tmp = 0;
                foreach ($check as $itemCheck){
                    if (in_array($itemCheck['admin_menu_route'],$arrayRole)){
                        $tmp = 1;
                    }
                }
                if ($tmp == 1) {
                    $data[] = [
                        'menu_category_name' => $item['menu_category_name'],
                        'menu_category_icon' => $item['menu_category_icon'],
                        'menu_category_id' => $item['menu_category_id'],
                        'menu' => $this->admin_menu->groupCategory($item['menu_category_id'])
                    ];
                }

            }
            session(['key' => $data]);
            // Authentication passed...
            return response()->json([
                'error' => 0,
                'message' => __('Đăng nhập thành công.'),
                'url' => route(LOGIN_HOME_PAGE)
            ]);
        }

        return response()->json([
            'error' => 1,
            'message' => __('Username hoặc password không đúng.')
        ]);
    }

    public function sendEmailResetPassword(Request $request) {
        $email = strip_tags($request->email);
        $staff = new StaffsTable();
        $getStaff = $staff->getStaffsByEmail($email);
        if ($getStaff == null || $getStaff['is_deleted'] == 1) {
            return response()->json([
                'error' => 1,
                'message' => 'Tài khoản không tồn tại',
            ]);
        } else if($getStaff['is_actived'] == 0) {
            return response()->json([
                'error' => 1,
                'message' => 'Tài khoản đang bị khóa',
            ]);
        }
        $name = $getStaff['full_name'];
        $ran_string = Str::random(32);
        $forgotPass = new StaffsForgotPassTable();
        $forgotPass->unActiveForgotPass($getStaff['id']);
        $dt = Carbon::now();
        $data = [
            'staff_id' => $getStaff['staff_id'],
            'code' => $ran_string,
            'expire' => $dt->addDays(1)->toDateTimeString(),
            'is_actived' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $vars = [
            'link' => route('reset-password', ['token' => $ran_string]),
            'staff_name' => $name,
        ];
        $forgotPass->store($data);

        $insertEmail = [
            'obj_id' => $getStaff['staff_id'],
            'email_type' => "forgot-password",
            'email_subject' => 'Quên mật khẩu',
            'email_from' => env('MAIL_USERNAME'),
            'email_to' => $request->email,
            'email_params' => json_encode($vars),
            'is_run' => 0
        ];

        $jobsEmailLogApi = new JobsEmailLogApi();
        $jobsEmailLogApi->addJob($insertEmail);

        //viết hàm gửi email

//        Mail::send('user::forget-password.email-reset-pass', $vars, function ($message) use ($email) {
//            $message
//                ->to(strip_tags($email))
//                ->subject('Quên mật khẩu');
//        });

        return response()->json([
            'error' => false,
            'message' => 'Gửi email thành công'
        ]);
    }

    public function logoutAction(Request $request)
    {
        if ($request->session()->has('routeList')) {
            $request->session()->forget('routeList');
        }
        Auth::logout();

        return redirect()->route('login');
    }

    public function resetPassword($token) {
        $forgotPass = new StaffsForgotPassTable();
        $getUser = $forgotPass->getItem($token);
        if ($getUser == null) {
            return view('user::forget-password.reset-password-fail', [

            ]);
        }
        return view('user::forget-password.reset-password', [
            'user' => $getUser,
            'token' => $token,
        ]);
    }

    public function submitResetPassword(Request $request) {
        try {
            DB::beginTransaction();
            $param = $request->all();
            $param['password'] = Hash::make(strip_tags($param['password']));
            $forgotPass = new StaffsForgotPassTable();
            $user = $forgotPass->getItemCode($param['token']);
            $staff = new StaffsTable();
//        cập nhật user
            $updateUser = $staff->edit(['password' => $param['password']],$user['id']);
//            Cập nhật forgot pass
            $updateForgotUser = $forgotPass->unActiveForgotPass($user['id']);
            DB::commit();
            return response()->json([
                'error' => false,
                'message' => 'Cập nhật mật khẩu thành công'
            ]);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => 'Cập nhật mật khẩu thất bại'
            ]);
        }
    }
}
