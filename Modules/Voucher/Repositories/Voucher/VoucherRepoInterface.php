<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/26/2020
 * Time: 4:47 PM
 */

namespace Modules\Voucher\Repositories\Voucher;


interface VoucherRepoInterface
{
    /**
     * Danh sách gói dịch vụ
     *
     * @param array $filters
     * @return mixed
     */
    public function list(array $filters = []);

    /**
     * Lấy các option
     *
     * @return mixed
     */
    public function getOption();

    /**
     * Data view thêm gói dịch vụ
     *
     * @return mixed
     */
    public function dataViewCreate();

    /**
     * Upload image dropzone
     *
     * @param $input
     * @return mixed
     */
    public function uploadDropzone($input);

    /**
     * Thêm gói dịch vụ
     *
     * @param $input
     * @return mixed
     */
    public function store($input);

    /**
     * Data view chỉnh sửa gói dịch vụ
     *
     * @param $serviceId
     * @return mixed
     */
    public function dataViewEdit($serviceId);

    /**
     * Chỉnh sửa gói dịch vụ
     *
     * @param $input
     * @return mixed
     */
    public function update($input);

    /**
     * Thay đổi trạng thái gói dịch vụ
     *
     * @param $input
     * @return mixed
     */
    public function changeStatus($input);

    /**
     * Xóa gói dịch vụ
     *
     * @param $input
     * @return mixed
     */
    public function destroy($input);
}