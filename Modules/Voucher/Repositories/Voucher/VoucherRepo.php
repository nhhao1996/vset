<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/26/2020
 * Time: 4:47 PM
 */

namespace Modules\Voucher\Repositories\Voucher;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Voucher\Models\ServiceCategoryTable;
use Modules\Voucher\Models\ServiceImageTable;
use Modules\Voucher\Models\ServiceTable;
use Modules\Voucher\Models\ServiceVoucherTemplateTable;

class VoucherRepo implements VoucherRepoInterface
{
    protected $voucher;

    public function __construct(
        ServiceTable $voucher
    ) {
        $this->voucher = $voucher;
    }

    const IS_DELETE = 1;
    const SERVICE_PREFIX = "SV";

    /**
     * Danh sách gói dịch vụ
     *
     * @param array $filters
     * @return array|mixed
     */
    public function list(array $filters = [])
    {
        $list = $this->voucher->getList($filters);

        return [
            'list' => $list
        ];
    }

    /**
     * Lấy các option
     *
     * @return array|mixed
     */
    public function getOption()
    {
        $mServiceCategory = new ServiceCategoryTable();

        $optionCategory = $mServiceCategory->getOption();

        return [
            'optionCategory' => $optionCategory
        ];
    }

    /**
     * Data thêm gói dịch vụ
     *
     * @return mixed|void
     */
    public function dataViewCreate()
    {
        $mServiceCategory = new ServiceCategoryTable();

        $optionCategory = $mServiceCategory->getOption();

        return [
            'optionCategory' => $optionCategory
        ];
    }

    /**
     * Upload image dropzone
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function uploadDropzone($input)
    {
        $time = Carbon::now();
        // Requesting the file from the form
        $image = $input->file('file');
        // Getting the extension of the file
        $extension = $image->getClientOriginalExtension();
        //tên của hình ảnh
        $filename = $image->getClientOriginalName();
        //$filename = time() . str_random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . time() . "." . $extension;
        // This is our upload main function, storing the image in the storage that named 'public'
        $upload_success = $image->storeAs(TEMP_PATH, $filename, 'public');
        // If the upload is successful, return the name of directory/filename of the upload.
        if ($upload_success) {
            return response()->json($filename, 200);
        } // Else, return error 400
        else {
            return response()->json('error', 400);
        }
    }

    /**
     * Thêm gói dịch vụ
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function store($input)
    {
        DB::beginTransaction();
        try {
            $mServiceImage = new ServiceImageTable();
            $mServiceTemplate = new ServiceVoucherTemplateTable();
            $mServiceCategory = new ServiceCategoryTable();

            $serviceImage = [];
            $serviceTemplate = [];
            $checkActiveTemplate = [];

            if (str_replace(',', '', $input['price_standard']) < 0) {
                return response()->json([
                    'error' => true,
                    'message' => __('Giá bán phải lớn hơn 0'),
                ]);
            }

            $getServiceCategory = $mServiceCategory->getNameByCategoryId($input['service_category_id']);

            $checkNameServiceVI = $this->voucher->checkName('vi',$getServiceCategory['name_vi'].str_replace(',', '', $input['price_standard']));

            if (count($checkNameServiceVI) != 0) {
                return response()->json([
                    'error' => true,
                    'message' => __('Giá bán theo nhà cung cấp đã được tạo vui lòng nhập giá trị khác')
                ]);
            }

            $serviceCode = $this->generateOrderCode();

            $data = [
//                'service_name_vi' => strip_tags($input['service_name_vi']),
//                'service_name_en' => strip_tags($input['service_name_en']),
                'service_name_vi' => $getServiceCategory['name_vi'].' '.$input['price_standard'].' VND',
                'service_name_en' => $getServiceCategory['name_en'].' '.$input['price_standard'].' VND',
                'slug' => str_slug($getServiceCategory['name_vi'].' '.str_replace(',', '', $input['price_standard'])),
                'service_category_id' => $input['service_category_id'],
                'price_standard' => str_replace(',', '', $input['price_standard']),
                'service_avatar_list' => $input['service_avatar_list'] != null ? url('/').'/' . $this->moveImage($input['service_avatar_list'], VOUCHER_VSET_PATH) : null,
                'service_avatar_list_mobile' => $input['service_avatar_list_mobile'] != null ? url('/').'/' . $this->moveImage($input['service_avatar_list_mobile'], VOUCHER_VSET_PATH) : null,
                'service_avatar_home' => $input['service_avatar_home'] != null ? url('/').'/' . $this->moveImage($input['service_avatar_home'], VOUCHER_VSET_PATH) : null,
                'service_avatar' => $input['service_avatar'] != null ? url('/').'/' . $this->moveImage($input['service_avatar'], VOUCHER_VSET_PATH) : null,
                'service_avatar_mobile' => $input['service_avatar_mobile'] != null ? url('/').'/' . $this->moveImage($input['service_avatar_mobile'], VOUCHER_VSET_PATH) : null,
                'description' => strip_tags($input['description']),
                'description_en' => strip_tags($input['description_en']),
                'detail_description' => strip_tags($input['detail_description']),
                'detail_description_en' => strip_tags($input['detail_description_en']),
                'is_actived' => strip_tags($input['is_actived']),
                'created_by' => Auth()->id()
            ];
            //Insert service
            $serviceId = $this->voucher->add($data);
            //Update service code
            $this->voucher->edit([
//                'service_code' => 'SV_' . date('dmY') . sprintf("%02d", $serviceId)
                'service_code' => $serviceCode
            ], $serviceId);

            if (isset($input['serviceImage']) && count($input['serviceImage']) > 0) {
                foreach ($input['serviceImage'] as $v) {
                    $serviceImage [] = [
                        'service_id' => $serviceId,
                        'name' => $v['image'] != null ? url('/').'/' . $this->moveImage($v['image'], VOUCHER_VSET_PATH) : null,
                        'created_by' => Auth()->id(),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ];
                }
            }
            //Insert service image
            $mServiceImage->insert($serviceImage);

            if (isset($input['template']) && count($input['template']) > 0) {
                foreach ($input['template'] as $v) {
                    $formDate = Carbon::createFromFormat('d/m/Y H:i', $v['valid_from_date'])->format('Y-m-d H:i');
                    $toDate = Carbon::createFromFormat('d/m/Y H:i', $v['valid_to_date'])->format('Y-m-d H:i');

                    if ($formDate >= $toDate) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Ngày hết hạn phải lớn hơn ngày kết thúc'),
                        ]);
                    }

                    $serviceTemplate [] = [
                        'service_id' => $serviceId,
                        'valid_from_date' => $formDate,
                        'valid_to_date' => $toDate,
                        'is_actived' => $v['is_actived'],
                        'created_by' => Auth()->id(),
                        'updated_by' => Auth()->id(),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ];

                    $checkActiveTemplate [] = $v['is_actived'];
                }
            }

            if (in_array(1, $checkActiveTemplate) == false && isset($serviceTemplate[0]['is_actived'])) {
                $serviceTemplate[0]['is_actived'] = 1;
            }
            //Insert service template
            $mServiceTemplate->insert($serviceTemplate);

            DB::commit();
            return response()->json([
                'error' => false,
                'message' => __('Thêm thành công')
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => __('Thêm thất bại'),
                '_message' => $e->getMessage()
            ]);
        }
    }

    public function generateOrderCode()
    {
        $result = $this->voucher->getLastOrder();
//        dd(strpos($result["order_code"],self::ORDER_PREFIX));
        if ($result == null) return self::SERVICE_PREFIX . "0100";
        if (strpos($result["service_code"], self::SERVICE_PREFIX) !== false) {
//            dd(strpos($result["order_code"],self::ORDER_PREFIX));
            $arr = explode(self::SERVICE_PREFIX, $result["service_code"]);
            $value = strval(intval($arr[1]) + 1);
            $zero_str = "";
            if (strlen($value) < 4) {
                for ($i = 0; $i < (4 - strlen($value)); $i++) {
                    $zero_str .= "0";
                }
            }


            return self::SERVICE_PREFIX . $zero_str . $value;
        }

        return self::SERVICE_PREFIX . "0100";
    }

    /**
     * Move ảnh từ folder temp sang folder chính
     *
     * @param $filename
     * @param $PATH
     * @return mixed|string
     */
    public function moveImage($filename, $PATH)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = $PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory($PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    /**
     * Data view chỉnh sửa gói dịch vụ
     *
     * @param $serviceId
     * @return mixed|void
     */
    public function dataViewEdit($serviceId)
    {
        $mServiceCategory = new ServiceCategoryTable();
        $mServiceImage = new ServiceImageTable();
        $mServiceTemplate = new ServiceVoucherTemplateTable();

        $infoService = $this->voucher->getItem($serviceId);
        $serviceImage = $mServiceImage->getImage($infoService['service_id']);
        $serviceTemplate = $mServiceTemplate->getTemplate($infoService['service_id']);

        //Option
        $optionCategory = $mServiceCategory->getOption();

        return [
            'optionCategory' => $optionCategory,
            'item' => $infoService,
            'serviceImage' => $serviceImage,
            'serviceTemplate' => $serviceTemplate
        ];
    }

    /**
     * Chỉnh sửa gói dịch vụ
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function update($input)
    {
        DB::beginTransaction();
        try {
            $mServiceImage = new ServiceImageTable();
            $mServiceTemplate = new ServiceVoucherTemplateTable();
            $mServiceCategory = new ServiceCategoryTable();

            $serviceImage = [];
            $serviceTemplate = [];
            $checkActiveTemplate = $input['templateIsActive'];

            if (in_array(1, $checkActiveTemplate) == false) {
                if (isset($input['templateOld']) && count($input['templateOld']) > 0) {
                    $input['templateOld'][0]['is_actived'] = 1;
                } else {
                    $input['template'][0]['is_actived'] = 1;
                }
            }

            if (str_replace(',', '', $input['price_standard']) < 0) {
                return response()->json([
                    'error' => true,
                    'message' => __('Giá bán phải lớn hơn 0'),
                ]);
            }

            $getServiceCategory = $mServiceCategory->getNameByCategoryId($input['service_category_id']);

            $checkNameServiceVI = $this->voucher->checkName('vi',$getServiceCategory['name_vi'].str_replace(',', '', $input['price_standard']),$input['service_id']);

            if (count($checkNameServiceVI) != 0) {
                return response()->json([
                    'error' => true,
                    'message' => __('Giá bán theo nhà cung cấp đã được tạo vui lòng nhập giá trị khác')
                ]);
            }

            $data = [
//                'service_name_vi' => strip_tags($input['service_name_vi']),
//                'service_name_en' => strip_tags($input['service_name_en']),
                'service_name_vi' => $getServiceCategory['name_vi'].' '.$input['price_standard'].' VND',
                'service_name_en' => $getServiceCategory['name_en'].' '.$input['price_standard'].' VND',
                'slug' => str_slug($getServiceCategory['name_vi'].' '.str_replace(',', '', $input['price_standard'])),
                'service_category_id' => $input['service_category_id'],
                'price_standard' => str_replace(',', '', $input['price_standard']),
                'description' => strip_tags($input['description']),
                'description_en' => strip_tags($input['description_en']),
                'detail_description' => strip_tags($input['detail_description']),
                'detail_description_en' => strip_tags($input['detail_description_en']),
                'is_actived' => strip_tags($input['is_actived']),
                'updated_by' => Auth()->id()
            ];

            if ($input['service_avatar_list'] != null) {
                $data['service_avatar_list'] = url('/').'/' . $this->moveImage($input['service_avatar_list'], VOUCHER_VSET_PATH);
            }

            if ($input['service_avatar_list_mobile'] != null) {
                $data['service_avatar_list_mobile'] = url('/').'/' . $this->moveImage($input['service_avatar_list_mobile'], VOUCHER_VSET_PATH);
            }

            if ($input['service_avatar_home'] != null) {
                $data['service_avatar_home'] = url('/').'/' . $this->moveImage($input['service_avatar_home'], VOUCHER_VSET_PATH);
            }

            if ($input['service_avatar'] != null) {
                $data['service_avatar'] = url('/').'/' . $this->moveImage($input['service_avatar'], VOUCHER_VSET_PATH);
            }

            if ($input['service_avatar_mobile'] != null) {
                $data['service_avatar_mobile'] = url('/').'/' . $this->moveImage($input['service_avatar_mobile'], VOUCHER_VSET_PATH);
            }

            //Update service
            $this->voucher->edit($data, $input['service_id']);

            //Remove service_image
            $mServiceImage->removeImage($input['service_id']);

            if (isset($input['serviceImage']) && count($input['serviceImage']) > 0) {
                foreach ($input['serviceImage'] as $v) {
                    if ($v['type'] == 1) {
                        $name = $v['image'];
                    } else {
                        $name = $v['image'] != null ? url('/').'/' . $this->moveImage($v['image'], VOUCHER_VSET_PATH) : null;
                    }

                    $serviceImage [] = [
                        'service_id' => $input['service_id'],
                        'name' => $name,
                        'created_by' => Auth()->id(),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ];
                }
            }
            //Insert service image
            $mServiceImage->insert($serviceImage);

            //Delete service template
            if (isset($input['arrTemplateRemove']) && count($input['arrTemplateRemove']) > 0) {
                foreach ($input['arrTemplateRemove'] as $v) {
                    $mServiceTemplate->edit([
                       'is_deleted' => self::IS_DELETE
                    ], $v['service_voucher_template_id']);
                }
            }

            //Update service template old
            if (isset($input['templateOld']) && count($input['templateOld']) > 0) {
                foreach ($input['templateOld'] as $v) {
                    $formDate = Carbon::createFromFormat('d/m/Y H:i', $v['valid_from_date'])->format('Y-m-d H:i');
                    $toDate = Carbon::createFromFormat('d/m/Y H:i', $v['valid_to_date'])->format('Y-m-d H:i');

                    if ($formDate >= $toDate) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Ngày hết hạn phải lớn hơn ngày kết thúc'),
                        ]);
                    }

                    $mServiceTemplate->edit([
                        'service_id' => $input['service_id'],
                        'valid_from_date' => $formDate,
                        'valid_to_date' => $toDate,
                        'is_actived' => $v['is_actived'],
                        'updated_by' => Auth()->id(),
                    ], $v['service_voucher_template_id']);
                }
            }

            if (isset($input['template']) && count($input['template']) > 0) {
                foreach ($input['template'] as $v) {
                    $formDate = Carbon::createFromFormat('d/m/Y H:i', $v['valid_from_date'])->format('Y-m-d H:i');
                    $toDate = Carbon::createFromFormat('d/m/Y H:i', $v['valid_to_date'])->format('Y-m-d H:i');

                    if ($formDate >= $toDate) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Ngày hết hạn phải lớn hơn ngày kết thúc'),
                        ]);
                    }

                    $serviceTemplate [] = [
                        'service_id' => $input['service_id'],
                        'valid_from_date' => $formDate,
                        'valid_to_date' => $toDate,
                        'is_actived' => $v['is_actived'],
                        'created_by' => Auth()->id(),
                        'updated_by' => Auth()->id(),
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ];
                }
            }
            //Insert service template
            $mServiceTemplate->insert($serviceTemplate);

            DB::commit();
            return response()->json([
                'error' => false,
                'message' => __('Chỉnh sửa thành công')
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => __('Chỉnh sửa thất bại'),
                '_message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Thay đổi trạng thái
     *
     * @param $input
     * @return array|mixed
     */
    public function changeStatus($input)
    {
        try {
            $this->voucher->edit($input, $input['service_id']);

            return response()->json([
                'error' => false,
                'message' => __('Thay đổi trạng thái thành công')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('Thay đổi trạng thái thất bại')
            ]);
        }
    }

    /**
     * Xóa gói dịch vụ
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function destroy($input)
    {
        try {
            $this->voucher->edit([
                'is_deleted' => self::IS_DELETE
            ], $input['service_id']);

            return response()->json([
                'error' => false,
                'message' => __('Xóa thành công')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('Xóa thất bại')
            ]);
        }
    }
}