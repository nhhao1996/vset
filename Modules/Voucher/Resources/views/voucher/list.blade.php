<div class="table-responsive">
    <table class="table table-striped m-table m-table--head-bg-default">
        <thead class="bg">
        <tr>
            <th class="tr_thead_list">@lang('HÌNH ẢNH')</th>
            <th class="tr_thead_list">@lang('NHÀ CUNG CẤP')</th>
            <th class="tr_thead_list">@lang('MÃ GÓI')</th>
            <th class="tr_thead_list">@lang('TÊN GÓI')</th>
            <th class="tr_thead_list">@lang('GIÁ BÁN (vnđ)')</th>
            <th class="tr_thead_list">@lang('TRẠNG THÁI')</th>
            <th class="tr_thead_list">@lang('NGÀY TẠO')</th>
            <th class="tr_thead_list">@lang('HÀNH ĐỘNG')</th>
        </tr>
        </thead>
        <tbody style="font-size: 13px">
        @if (count($LIST) > 0)
            @foreach($LIST as $item)
                <tr>
                    <td>
                        <div class="m-widget5__pic">
                            <img src="{{$item['service_avatar']}}"
                                 onerror="this.onerror=null;this.src='{{asset('static/backend/images/default-placeholder.png')}}';"
                                 class="m-widget7__img" alt="photo" width="50px" height="50px">
                        </div>
                    </td>
                    <td>{{$item['category_name']}}</td>
                    <td>{{$item['service_code']}}</td>
                    <td>
                        @if(in_array('voucher.detail', session('routeList')))
                            <a href="{{route('voucher.detail', $item['service_id'])}}">
                                {{$item['service_name_vi']}}
                            </a>
                        @else
                            {{$item['service_name_vi']}}
                        @endif
                    </td>
                    <td>
                        {{number_format($item['price_standard'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}
                    </td>
                    <td>
                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                            <label style="margin: 0 0 0 10px; padding-top: 4px">
                                <input type="checkbox"
                                       disabled
                                       class="manager-btn" {{$item['is_actived'] == 1 ? 'checked' : ''}}>
                                <span></span>
                            </label>
                        </span>
                    </td>
                    <td>{{\Carbon\Carbon::parse($item['created_at'])->format('d/m/Y H:i')}}</td>
                    <td>
                    @if(in_array('voucher.edit', session('routeList')))
                        <a href="{{route('voucher.edit', $item['service_id'])}}"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill"
                           title="@lang('Chỉnh sửa')">
                            <i class="la la-edit"></i>
                        </a>
                    @endif
                    @if(in_array('voucher.destroy', session('routeList')))
                        <a href="javascript:void(0)" onclick="listService.remove('{{$item['service_id']}}')"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill"
                           title="@lang('Xóa')">
                            <i class="la la-trash"></i>
                        </a>
                    @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
