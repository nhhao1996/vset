@extends('layout')
@section('title_header')
    <span class="title_header">@lang('QUẢN LÝ GÓI DỊCH VỤ')</span>
@stop
@section('content')
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                         <i class="la la-edit"></i>
                     </span>
                    <h2 class="m-portlet__head-text">
                        @lang('CHỈNH SỬA GÓI DỊCH VỤ')
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <form id="form-edit">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Nhà cung cấp'):<b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <select class="form-control" id="service_category_id" name="service_category_id">
                                    <option></option>
                                    @foreach($optionCategory as $v)
                                        <option value="{{$v['service_category_id']}}"
                                                {{$item['service_category_id'] == $v['service_category_id'] ? 'selected' : ''}}>{{$v['name_vi']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Tên gói (VI)'):<b class="text-danger">*</b>
                            </label>
{{--                            <input type="text" class="form-control m-input" id="service_name_vi" name="service_name_vi"--}}
                            <input type="text" class="form-control m-input" disabled
                                   value="{{$item['service_name_vi']}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Tên gói (EN)'):<b class="text-danger">*</b>
                            </label>
{{--                            <input type="text" class="form-control m-input" id="service_name_en" name="service_name_en"--}}
                            <input type="text" class="form-control m-input" disabled
                                   value="{{$item['service_name_en']}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Giá bán'):<b class="text-danger">*</b>
                            </label>
                            <input type="text" class="form-control m-input"
                                   id="price_standard" name="price_standard"
                                   value="{{number_format($item['price_standard'] != null ? $item['price_standard'] : 0, isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0)}}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Mô tả ngắn (VI)'):
                            </label>
                            <textarea class="form-control m-input" id="description" name="description"
                                      rows="5">{{$item['description']}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Mô tả ngắn (EN)'):
                            </label>
                            <textarea class="form-control m-input" id="description_en" name="description_en"
                                      rows="5">{{$item['description_en']}}</textarea>
                        </div>
                        <div class="m-form__group form-group row">
                            <div class="col-6">
                                <div class="row">
                                    <label class="col-lg-4 col-form-label">@lang('Ảnh chi tiết'):</label>
                                    <div class="col-lg-8">
                                        <div class="form-group m-form__group m-widget19">
                                            <div class="m-widget19__pic" style="max-width: 170px">
                                                <img class="m--bg-metal m-image img-sd" id="blah" height="150px"
                                                     src="{{$item['service_avatar'] != null ? $item['service_avatar'] : "https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"}}"
                                                     alt="Hình ảnh"/>
                                            </div>
                                            <input type="hidden" id="service_avatar" name="service_avatar">
                                            <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                   data-msg-accept="Hình ảnh không đúng định dạng"
                                                   id="getFile" type='file'
                                                   onchange="uploadAvatar(this);"
                                                   class="form-control"
                                                   style="display:none"/>
                                            <div class="m-widget19__action" style="max-width: 170px">
                                                <a href="javascript:void(0)"
                                                   onclick="document.getElementById('getFile').click()"
                                                   class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    @lang('Tải ảnh lên')
                                                </span>
                                            </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <label class="col-lg-4 col-form-label">@lang('Ảnh chi tiết (mobile)'):</label>
                                    <div class="col-lg-8">
                                        <div class="form-group m-form__group m-widget19">
                                            <div class="m-widget19__pic" style="max-width: 170px">
                                                <img class="m--bg-metal m-image img-sd" id="blah_mobile" height="150px"
                                                     src="{{$item['service_avatar_mobile'] != null ? $item['service_avatar_mobile'] : "https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"}}"
                                                     alt="Hình ảnh"/>
                                            </div>
                                            <input type="hidden" id="service_avatar_mobile" name="service_avatar_mobile">
                                            <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                   data-msg-accept="Hình ảnh không đúng định dạng"
                                                   id="getFileMobile" type='file'
                                                   onchange="uploadAvatarMobile(this);"
                                                   class="form-control"
                                                   style="display:none"/>
                                            <div class="m-widget19__action" style="max-width: 170px">
                                                <a href="javascript:void(0)"
                                                   onclick="document.getElementById('getFileMobile').click()"
                                                   class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    @lang('Tải ảnh lên')
                                                </span>
                                            </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <div class="col-6">
                                <div class="row">
                                    <label class="col-lg-4 col-form-label">@lang('Ảnh trang danh sách'):</label>
                                    <div class="col-lg-8">
                                        <div class="form-group m-form__group m-widget19">
                                            <div class="m-widget19__pic" style="max-width: 170px">
                                                <img class="m--bg-metal m-image img-sd" id="blah_list" height="150px"
                                                     src="{{$item['service_avatar_list'] != null ? $item['service_avatar_list'] : "https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"}}"
                                                     alt="Hình ảnh"/>
                                            </div>
                                            <input type="hidden" id="service_avatar_list" name="service_avatar_list">
                                            <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                   data-msg-accept="Hình ảnh không đúng định dạng"
                                                   id="getFileList" type='file'
                                                   onchange="uploadAvatarPage(this,'list');"
                                                   class="form-control"
                                                   style="display:none"/>
                                            <div class="m-widget19__action" style="max-width: 170px">
                                                <a href="javascript:void(0)"
                                                   onclick="document.getElementById('getFileList').click()"
                                                   class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    @lang('Tải ảnh lên')
                                                </span>
                                            </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <label class="col-lg-4 col-form-label">@lang('Ảnh trang danh sách (mobile)'):</label>
                                    <div class="col-lg-8">
                                        <div class="form-group m-form__group m-widget19">
                                            <div class="m-widget19__pic" style="max-width: 170px">
                                                <img class="m--bg-metal m-image img-sd" id="blah_list_mobile" height="150px"
                                                     src="{{$item['service_avatar_list_mobile'] != null ? $item['service_avatar_list_mobile'] : "https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"}}"
                                                     alt="Hình ảnh"/>
                                            </div>
                                            <input type="hidden" id="service_avatar_list_mobile" name="service_avatar_list_mobile">
                                            <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                   data-msg-accept="Hình ảnh không đúng định dạng"
                                                   id="getFileListMobile" type='file'
                                                   onchange="uploadAvatarPage(this,'list-mobile');"
                                                   class="form-control"
                                                   style="display:none"/>
                                            <div class="m-widget19__action" style="max-width: 170px">
                                                <a href="javascript:void(0)"
                                                   onclick="document.getElementById('getFileListMobile').click()"
                                                   class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    @lang('Tải ảnh lên')
                                                </span>
                                            </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <div class="col-6">
                                <div class="row">
                                    <label class="col-lg-4 col-form-label">@lang('Ảnh trang chủ'):</label>
                                    <div class="col-lg-8">
                                        <div class="form-group m-form__group m-widget19">
                                            <div class="m-widget19__pic" style="max-width: 170px">
                                                <img class="m--bg-metal m-image img-sd" id="blah_home" height="150px"
                                                     src="{{$item['service_avatar_home'] != null ? $item['service_avatar_home'] : "https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"}}"
                                                     alt="Hình ảnh"/>
                                            </div>
                                            <input type="hidden" id="service_avatar_home" name="service_avatar_home">
                                            <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                                   data-msg-accept="Hình ảnh không đúng định dạng"
                                                   id="getFileHome" type='file'
                                                   onchange="uploadAvatarPage(this,'home');"
                                                   class="form-control"
                                                   style="display:none"/>
                                            <div class="m-widget19__action" style="max-width: 170px">
                                                <a href="javascript:void(0)"
                                                   onclick="document.getElementById('getFileHome').click()"
                                                   class="btn  btn-sm m-btn--icon color w-100">
                                            <span class="m--margin-left-20">
                                                <i class="fa fa-camera"></i>
                                                <span>
                                                    @lang('Tải ảnh lên')
                                                </span>
                                            </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Mô tả chi tiết (VI)'):
                            </label>
                            <textarea class="summernote form-control" rows="20" name="detail_description"
                                      id="detail_description">{{$item['detail_description']}}</textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                @lang('Mô tả chi tiết (EN)'):
                            </label>
                            <textarea class="summernote form-control" rows="20" name="detail_description_en"
                                      id="detail_description_en">{{$item['detail_description_en']}}</textarea>
                        </div>
                        <div class="form-group m-form__group d-none">
                            <label>{{__('Ảnh kèm theo')}}:</label>
                            <div class="m-dropzone dropzone dz-clickable"
                                 action="{{route('admin.service.upload-dropzone')}}" id="dropzoneone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 href="" class="m-dropzone__msg-title">
                                        {{__('Hình ảnh')}}
                                    </h3>
                                    <span>{{__('Chọn hình dịch vụ')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="upload-image">
                            @if(count($serviceImage) > 0)
                                @foreach($serviceImage as $v)
                                    <div class="wrap-img image-show-child">
                                        <input type="hidden" name="img-sv" value="{{$v['name']}}">
                                        <input type="hidden" name="type" value="1">
                                        <img class='m--bg-metal m-image img-sd' src='{{$v['name']}}'
                                             alt='{{__('Hình ảnh')}}' width="100px"
                                             height="100px">
                                        <span class="delete-img-sv" style="display: block;">
                                            <a href="javascript:void(0)" onclick="view.remove_img(this)">
                                                <i class="la la-close"></i>
                                            </a>
                                        </span>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>
                                {{__('Trạng thái')}}:
                            </label>
                            <div class="input-group">
                                <span class="m-switch m-switch--icon m-switch--success m-switch--sm">
                                    <label>
                                        <input name="is_actived" type="checkbox" class="is_actived" {{$item['is_actived'] == 1 ? 'checked' :''}}>
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <div class="table-responsive">
                        <table class="table" id="table-template">
                            <thead class="bg">
                            <tr>
                                <th class="tr_thead_list">@lang('Ngày bắt đầu')</th>
                                <th class="tr_thead_list">@lang('Ngày hết hạn')</th>
                                <th class="tr_thead_list">@lang('Trạng thái')</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($serviceTemplate) > 0)
                                @foreach($serviceTemplate as $k => $v)
                                    <tr class="tr_template_old">
                                        <td>
                                            <input type="hidden" class="number" value="{{$k+1}}">
                                            <input type="hidden" class="service_voucher_template_id" value="{{$v['service_voucher_template_id']}}">
                                            <input type="text" class="form-control m-input valid_from_date" readonly=""
                                                   name="run_date" placeholder="@lang('Ngày bắt đầu')"
                                                   value="{{\Carbon\Carbon::parse($v['valid_from_date'])->format('d/m/Y H:i')}}">
                                            <span class="error_valid_from_date_{{$k+1}} color_red"></span>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control m-input valid_to_date" readonly=""
                                                   name="run_date" placeholder="@lang('Ngày hết hạn')"
                                                   value="{{\Carbon\Carbon::parse($v['valid_to_date'])->format('d/m/Y H:i')}}">
                                            <span class="error_valid_to_date_{{$k+1}} color_red"></span>
                                        </td>
                                        <td>
                                            <label class="m-radio m-radio--state-success">
                                                @if($v['is_actived'] == 1)
                                                    <input type="radio" name="is_actived" checked>
                                                @else
                                                    <input type="radio" name="is_actived">
                                                @endif
                                                <span></span>
                                            </label>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="view.removeTrOld(this)"
                                               class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                               title="Delete">
                                                <i class="la la-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="form-group m-form__group">
                            <span class="error_table_template color_red"></span>
                        </div>
                        <a class="btn  btn-sm m-btn--icon color" href="javascript:void(0)" onclick="view.addTemplate()">
                            <i class="la la-plus"></i> @lang('Thêm template')
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('voucher')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                            <span>
                                <i class="la la-arrow-left"></i>
                                <span>@lang('HỦY')</span>
                            </span>
                        </a>
                        <button type="button" onclick="edit.save({{$item['service_id']}})"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span>
                                <i class="la la-check"></i>
                                <span>@lang('LƯU THÔNG TIN')</span>
                        </span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
    <script src="{{asset('static/backend/js/voucher/script.js?v='.time())}}" type="text/javascript"></script>
    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        view._init();
        view.dropzone();
        stt = $('#table-template >tbody tr').length;
    </script>
    <script type="text/template" id="imageShow">
        <div class="wrap-img image-show-child">
            <input type="hidden" name="img-sv" value="{link_hidden}">
            <input type="hidden" name="type" value="0">
            <img class='m--bg-metal m-image img-sd' src='{{asset('{link}')}}' alt='{{__('Hình ảnh')}}' width="100px"
                 height="100px">
            <span class="delete-img-sv" style="display: block;">
                <a href="javascript:void(0)" onclick="view.remove_img(this)">
                    <i class="la la-close"></i>
                </a>
            </span>
        </div>
    </script>
    <script type="text/template" id="tpl-tr-template">
        <tr class="tr_template">
            <td>
                <input type="hidden" class="number" value="{stt}">
                <input type="text" class="form-control m-input valid_from_date" readonly=""
                       name="run_date" placeholder="@lang('Ngày bắt đầu')">
                <span class="error_valid_from_date_{stt} color_red"></span>
            </td>
            <td>
                <input type="text" class="form-control m-input valid_to_date" readonly=""
                       name="run_date" placeholder="@lang('Ngày hết hạn')">
                <span class="error_valid_to_date_{stt} color_red"></span>
            </td>
            <td>
                <label class="m-radio m-radio--state-success">
                    <input type="radio" name="is_actived">
                    <span></span>
                </label>
            </td>
            <td>
                <a href="javascript:void(0)" onclick="view.removeTr(this)"
                   class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                   title="Delete">
                    <i class="la la-trash"></i>
                </a>
            </td>
        </tr>
    </script>
@stop


