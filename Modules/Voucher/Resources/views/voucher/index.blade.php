@extends('layout')
@section('title_header')
    <span class="title_header"><img
                src="{{asset('uploads/admin/icon/icon-order.png')}}" alt=""
                style="height: 20px;"> @lang('QUẢN LÝ GÓI DỊCH VỤ')</span>
@stop
@section('content')
    <style>
        .modal-backdrop {
            position: relative !important;
        }

        .form-control-feedback {
            color: red;
        }

    </style>
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="flaticon-list-1"></i>
                    </span>
                    <h2 class="m-portlet__head-text">
                        @lang("DANH SÁCH GÓI DỊCH VỤ")
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                @if(in_array('voucher.create',session('routeList')))
                    <a href="{{route('voucher.create')}}"
                       class="btn btn-primary m-btn btn-sm color_button m-btn--icon m-btn--pill btn_add_pc">
                                    <span>
                                        <i class="fa fa-plus-circle"></i>
                                        <span> @lang('TẠO GÓI DỊCH VỤ')</span>
                                    </span>
                    </a>
                    <a href="{{route('voucher.create')}}" class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill
                                 color_button btn_add_mobile"
                       style="display: none">
                        <i class="fa fa-plus-circle" style="color: #fff"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="m-portlet__body">
            <div id="autotable">
                <form class="frmFilter bg">
                    <div class="row padding_row">

                    </div>
                    <div class="padding_row row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="search"
                                               placeholder="@lang("Nhập tên dịch vụ hoặc mã dịch vụ")">
                                    </div>
                                </div>
                                @php $i = 0; @endphp
                                @foreach ($FILTER as $name => $item)
                                    @if ($i > 0 && ($i % 4 == 0))
                            </div>
                            <div class="form-group m-form__group row align-items-center">

                                @endif
                                @php $i++; @endphp
                                <div class="col-lg-2 form-group input-group">
                                    @if(isset($item['text']))
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            {{ $item['text'] }}
                                        </span>
                                        </div>
                                    @endif
                                    {!! Form::select($name, $item['data'], $item['default'] ?? null, ['class' => 'form-control m-input m_selectpicker']) !!}
                                </div>
                                @endforeach
                                <div class="col-lg-3 form-group">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input readonly class="form-control m-input daterange-picker"
                                               style="background-color: #fff"
                                               id="created_at"
                                               name="created_at"
                                               autocomplete="off" placeholder="@lang('Ngày tạo')">
                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-calendar"></i></span></span>
                                    </div>
                                </div>
                                <div class="col-lg-4 form-group">
                                    <button class="btn btn-primary color_button btn-search" >
                                        @lang('TÌM KIẾM') <i class="fa fa-search ic-search m--margin-left-5"></i>
                                    </button>
                                    <a href="{{route('voucher')}}" class="btn btn-metal  btn-search padding9x padding9px">
                                        <span><i class="flaticon-refresh"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-content m--padding-top-30">
                    @include('voucher::voucher.list')
                </div>
            </div>
        </div>
    </div>
    <div id="my-modal"></div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/voucher/list.js?v='.time())}}" type="text/javascript"></script>

    <script>
        $(".m_selectpicker").selectpicker();

        // $.getJSON(laroute.route('translate'), function (json) {
            var arrRange = {};
            arrRange["Hôm nay"] = [moment(), moment()];
            arrRange["Hôm qua"] = [moment().subtract(1, "days"), moment().subtract(1, "days")];
            arrRange["7 ngày trước"] = [moment().subtract(6, "days"), moment()];
            arrRange["30 ngày trước"] = [moment().subtract(29, "days"), moment()];
            arrRange["Trong tháng"] = [moment().startOf("month"), moment().endOf("month")];
            arrRange["Tháng trước"] = [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")];

            $("#created_at").daterangepicker({
                autoUpdateInput: false,
                autoApply: true,
                // buttonClasses: "m-btn btn",
                // applyClass: "btn-primary",
                // cancelClass: "btn-danger",

                // maxDate: moment().endOf("day"),
                // startDate: moment().startOf("day"),
                // endDate: moment().add(1, 'days'),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY',
                    "customRangeLabel": 'Tùy chọn ngày',
                    daysOfWeek: [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Tháng 1 năm",
                        "Tháng 2 năm",
                        "Tháng 3 năm",
                        "Tháng 4 năm",
                        "Tháng 5 năm",
                        "Tháng 6 năm",
                        "Tháng 7 năm",
                        "Tháng 8 năm",
                        "Tháng 9 năm",
                        "Tháng 10 năm",
                        "Tháng 11 năm",
                        "Tháng 12 năm"
                    ],
                    "firstDay": 1
                },
                // ranges: arrRange
            });
        // });
    </script>
@stop