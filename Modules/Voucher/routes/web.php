<?php

Route::group(['middleware' => ['web', 'auth', 'account'], 'prefix' => 'admin', 'namespace' => 'Modules\Voucher\Http\Controllers'], function () {
    Route::group(['prefix' => 'services'], function () {
        Route::get('', 'VoucherController@index')->name('voucher');
        Route::post('list', 'VoucherController@listAction')->name('voucher.list');
        Route::get('create', 'VoucherController@create')->name('voucher.create');
        Route::post('upload-dropzone', 'VoucherController@uploadDropzoneAction')->name('voucher.upload-dropzone');
        Route::post('store', 'VoucherController@store')->name('voucher.store');
        Route::get('edit/{id}', 'VoucherController@edit')->name('voucher.edit');
        Route::post('update', 'VoucherController@update')->name('voucher.update');
        Route::post('change-status', 'VoucherController@updateStatusAction')->name('voucher.change-status');
        Route::post('destroy', 'VoucherController@destroy')->name('voucher.destroy');
        Route::get('detail/{id}', 'VoucherController@show')->name('voucher.detail');
    });

});
