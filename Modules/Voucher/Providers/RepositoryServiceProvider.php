<?php
/**
 * Created by PhpStorm.
 * User: tuanva
 * Date: 2019-03-26
 * Time: 10:37
 */

namespace Modules\Voucher\Providers;


use Illuminate\Support\ServiceProvider;
use Modules\Voucher\Repositories\Voucher\VoucherRepo;
use Modules\Voucher\Repositories\Voucher\VoucherRepoInterface;


class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(VoucherRepoInterface::class, VoucherRepo::class);
    }
}