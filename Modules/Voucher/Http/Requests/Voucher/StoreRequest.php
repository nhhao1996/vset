<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/27/2020
 * Time: 3:16 PM
 */

namespace Modules\Voucher\Http\Requests\Voucher;


use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'service_name_vi' => 'required|max:250|unique:services,service_name_vi,'.',service_id,is_deleted,0',
//            'service_name_en' => 'required|max:250|unique:services,service_name_en,'.',service_id,is_deleted,0',
            'description' => 'max:250',
            'service_category_id' => 'required',
            'price_standard' => 'required'
        ];
    }

    /*
     * function custom messages
     */
    public function messages()
    {
        return [
            'service_name_vi.required' => __('Hãy nhập tên gói dịch vụ (VI)'),
            'service_name_vi.max' => __('Tên gói dịch vụ tối đa 250 kí tự (VI)'),
            'service_name_vi.unique' => __('Tên gói dịch vụ đã tồn tại (VI)'),
            'service_name_en.required' => __('Hãy nhập tên gói dịch vụ (EN)'),
            'service_name_en.max' => __('Tên gói dịch vụ tối đa 250 kí tự (EN)'),
            'service_name_en.unique' => __('Tên gói dịch vụ đã tồn tại (EN)'),
            'description.max' => __('Mô tả ngắn tối đa 250 kí tự (VI)'),
            'description_en.max' => __('Mô tả ngắn tối đa 250 kí tự (EN)'),
            'service_category_id.required' => __('Hãy chọn nhà cung cấp'),
            'price_standard.required' => __('Hãy nhập giá bán'),
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'service_name_vi' => 'strip_tags|trim',
            'service_name_en' => 'strip_tags|trim',
            'description' => 'strip_tags|trim',
            'description_en' => 'strip_tags|trim',
            'detail_description' => 'strip_tags|trim',
            'detail_description_en' => 'strip_tags|trim',
            'service_category_id' => 'strip_tags|trim',
            'price_standard' => 'strip_tags|trim',
        ];
    }
}