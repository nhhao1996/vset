<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/26/2020
 * Time: 2:33 PM
 */

namespace Modules\Voucher\Http\Controllers;


use Illuminate\Http\Request;
use Modules\User\Http\Controllers\Controller;
use Modules\Voucher\Http\Requests\Voucher\StoreRequest;
use Modules\Voucher\Http\Requests\Voucher\UpdateRequest;
use Modules\Voucher\Repositories\Voucher\VoucherRepoInterface;

class VoucherController extends Controller
{
    protected $voucher;

    public function __construct(
        VoucherRepoInterface $voucher
    )
    {
        $this->voucher = $voucher;
    }

    /**
     * Danh sách gói dịch vụ
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        $data = $this->voucher->list();

        return view('voucher::voucher.index', [
            'LIST' => $data['list'],
            'FILTER' => $this->filters(),
        ]);
    }

    /**
     * Render các option filter
     *
     * @return array
     */
    protected function filters()
    {
        $getOption = $this->voucher->getOption();

        $category = array_combine(
            array_column($getOption['optionCategory'], 'service_category_id'),
            array_column($getOption['optionCategory'], 'name_vi')
        );

        $groupCategory = (['' => __('Chọn nhà cung cấp')]) + $category;

        return [
            'services$service_category_id' => [
                'data' => $groupCategory
            ]
        ];
    }

    /**
     * Ajax load filter, phân trang CTKM
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filter = $request->only([
            'page',
            'display',
            'search',
            'services$service_category_id',
            'services$is_actived',
            'created_at'
        ]);

        $data = $this->voucher->list($filter);

        return view('voucher::voucher.list', [
            'LIST' => $data['list'],
            'page' => $filter['page']
        ]);
    }

    /**
     * View thêm gói dịch vụ
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        $data = $this->voucher->dataViewCreate();
        return view('voucher::voucher.create', $data);
    }

    /**
     * Upload image dropzone
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadDropzoneAction(Request $request)
    {
        return $this->voucher->uploadDropzone($request->all());
    }

    /**
     * Thêm gói dịch vụ
     *
     * @param StoreRequest $request
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        return $this->voucher->store($request->all());
    }

    /**
     * View chỉnh sửa gói dịch vụ
     *
     * @param $serviceId
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function edit($serviceId)
    {
        $data = $this->voucher->dataViewEdit($serviceId);

        return view('voucher::voucher.edit', $data);
    }

    /**
     * Chỉnh sửa gói dịch vụ
     *
     * @param UpdateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request)
    {
        return $this->voucher->update($request->all());
    }

    /**
     * Thay đổi trạng thái
     *
     * @param Request $request
     * @return mixed
     */
    public function updateStatusAction(Request $request)
    {
        return $this->voucher->changeStatus($request->all());
    }

    /**
     * Xóa gói dịch vụ
     *
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {
        return $this->voucher->destroy($request->all());
    }

    /**
     * Chi tiết gói dịch vụ
     *
     * @param $serviceId
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show($serviceId)
    {
        $data = $this->voucher->dataViewEdit($serviceId);

        return view('voucher::voucher.detail', $data);
    }
}