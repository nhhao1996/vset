<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/26/2020
 * Time: 3:20 PM
 */

namespace Modules\Voucher\Models;


use Illuminate\Database\Eloquent\Model;

class ServiceImageTable extends Model
{
    protected $table = "service_images";
    protected $primaryKey = "service_image_id";
    protected $fillable = [
        "service_image_id",
        "service_id",
        "name",
        "type",
        "created_at",
        "created_by",
        "updated_at"
    ];

    /**
     * Lấy thông tin hình ảnh của gói dịch vụ
     *
     * @param $serviceId
     * @return mixed
     */
    public function getImage($serviceId)
    {
        return $this
            ->select(
                "service_image_id",
                "service_id",
                "name"
            )
            ->where("service_id", $serviceId)
            ->get();
    }

    /**
     * Xóa hình ảnh gói dịch vụ
     *
     * @param $serviceId
     * @return mixed
     */
    public function removeImage($serviceId)
    {
        return $this->where("service_id", $serviceId)->delete();
    }
}