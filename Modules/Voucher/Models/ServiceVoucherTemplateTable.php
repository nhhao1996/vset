<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/27/2020
 * Time: 4:29 PM
 */

namespace Modules\Voucher\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ServiceVoucherTemplateTable extends Model
{
    protected $table = "service_voucher_template";
    protected $primaryKey = "service_voucher_template_id";
    protected $fillable = [
        "service_voucher_template_id",
        "service_id",
        "valid_from_date",
        "valid_to_date",
        "is_actived",
        "is_deleted",
        "updated_by",
        "created_by",
        "created_at",
        "updated_at",
        "description"
    ];

    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;

    /**
     * Lấy thông tin template service
     *
     * @param $serviceId
     * @return mixed
     */
    public function getTemplate($serviceId)
    {
        return $this
            ->select(
                "service_voucher_template_id",
                "service_id",
                "valid_from_date",
                "valid_to_date",
                "is_actived"
            )
            ->where("service_id", $serviceId)
            ->where("is_deleted", self::NOT_DELETE)
            ->get();
    }

    /**
     * Chỉnh sửa template service
     *
     * @param array $data
     * @param $templateId
     * @return mixed
     */
    public function edit(array $data, $templateId)
    {
        return $this->where("service_voucher_template_id", $templateId)->update($data);
    }

    public function getListService($serviceId){
        $oSelect = $this
            ->join('services','services.service_id',$this->table.'.service_id')
            ->whereIn($this->table.'.service_id',$serviceId)
            ->where($this->table.'.valid_from_date','<=',Carbon::now())
            ->where($this->table.'.valid_to_date','>',Carbon::now())
            ->where($this->table.'.is_actived',1)
            ->where($this->table.'.is_deleted',0)
            ->select($this->table.'.*','services.price_standard')
            ->get();
        return $oSelect;
    }
}