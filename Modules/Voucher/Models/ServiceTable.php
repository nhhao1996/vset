<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/26/2020
 * Time: 2:40 PM
 */

namespace Modules\Voucher\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class ServiceTable extends Model
{
    use ListTableTrait;
    protected $table = "services";
    protected $primaryKey = "service_id";
    protected $fillable = [
        "service_id",
        "service_name_vi",
        "service_name_en",
        "slug",
        "service_category_id",
        "service_code",
        "service_avatar_home",
        "service_avatar_list",
        "service_avatar_list_mobile",
        "service_avatar",
        "service_avatar_mobile",
        "price_standard",
        "description",
        "description_en",
        "created_by",
        "updated_by",
        "created_at",
        "updated_at",
        "is_deleted",
        "is_actived",
        "detail_description",
        "detail_description_en",
        "type_refer_commission",
        "refer_commission_value",
        "staff_commission_value",
        "is_sale",
        "service_type",
        "time",
        "have_material",
        "type_staff_commission"
    ];

    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;

    /**
     * Danh sách gói dịch vụ
     *
     * @param array $filter
     * @return mixed
     */
    public function _getList(&$filter = [])
    {
        $ds = $this
            ->select(
                "{$this->table}.service_id",
                "{$this->table}.service_name_vi",
                "{$this->table}.service_name_en",
                "service_categories.name_vi as category_name",
                "{$this->table}.service_code",
                "{$this->table}.service_avatar_home",
                "{$this->table}.service_avatar_list",
                "{$this->table}.service_avatar_list_mobile",
                "{$this->table}.service_avatar",
                "{$this->table}.service_avatar_mobile",
                "{$this->table}.price_standard",
                "{$this->table}.is_actived",
                "{$this->table}.created_at"
            )
            ->join("service_categories", "service_categories.service_category_id", "=", "{$this->table}.service_category_id")
            ->where("{$this->table}.is_deleted", self::NOT_DELETE)
            ->where("service_categories.is_deleted", self::NOT_DELETE)
            ->orderBy("{$this->table}.service_id", "desc");
        // filter tên DV, mã DV
        if (isset($filter["search"]) != "") {
            $search = $filter["search"];
            $ds->where(function ($query) use ($search) {
                $query->where("{$this->table}.service_code", "like", "%" . $search . "%")
                    ->orWhere("{$this->table}.service_name_vi", "like", "%" . $search . "%");
            });
            unset($filter["search"]);
        }

        // filter ngày tạo
        if (isset($filter["created_at"]) && $filter["created_at"] != "") {
            $arr_filter = explode(" - ", $filter["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $ds->whereBetween("{$this->table}.created_at", [$startTime . ' 00:00:00', $endTime . ' 23:59:59']);
            unset($filter["created_at"]);
        }

        return $ds;
    }

    /**
     * Thêm gói dịch vụ
     *
     * @param array $data
     * @return mixed
     */
    public function add(array $data)
    {
        return $this->create($data)->service_id;
    }

    /**
     * Chỉnh sửa gói dịch vụ
     *
     * @param array $data
     * @param $serviceId
     * @return mixed
     */
    public function edit(array $data, $serviceId)
    {
        return $this->where("service_id", $serviceId)->update($data);
    }

    /**
     * Lấy thông tin gói dịch vụ
     *
     * @param $serviceId
     * @return mixed
     */
    public function getItem($serviceId)
    {
        return $this
            ->select(
                "service_id",
                "service_name_vi",
                "service_name_en",
                "service_category_id",
                "service_code",
                "service_avatar_home",
                "service_avatar_list",
                "service_avatar_list_mobile",
                "service_avatar",
                "service_avatar_mobile",
                "price_standard",
                "description",
                "description_en",
                "detail_description",
                "detail_description_en",
                "is_actived"
            )
            ->where("service_id", $serviceId)
            ->first();
    }

    public function getListService($filter = [])
    {
        $page = (int)(isset($filter['page']) ? $filter['page'] : 1);
        $display = (int)(isset($filter['perpage']) ? $filter['perpage'] : 10);
        unset($filter['perpage']);
        unset($filter['page']);

        $oSelect = $this
            ->where('is_actived',self::IS_ACTIVE)
            ->where('is_deleted',self::NOT_DELETE);

        if(isset($filter['service_name'])) {
            $oSelect = $oSelect->where('service_name_vi','like','%'.$filter['service_name'].'%');
            unset($filter['service_name']);
        }

        if(isset($filter['service_category'])) {
            $oSelect = $oSelect->where('service_category_id',$filter['service_category']);
            unset($filter['service_category']);
        }

        if(isset($filter['service_list'])) {
            $oSelect = $oSelect->whereNotIn('service_id',$filter['service_list']);
            unset($filter['service_list']);
        }

        return $oSelect->paginate($display, $columns = ['*'], $pageName = 'page', $page);

    }

    public function getListServiceSession($data){
        $oSelect = $this
            ->where('is_actived',self::IS_ACTIVE)
            ->whereIn('service_id',$data)
            ->get();
        return $oSelect;
    }

    public function getAllList(){
        $oSelect = $this
            ->where('is_actived',self::IS_ACTIVE)
            ->where('is_deleted',self::NOT_DELETE);

        return $oSelect->get();
    }

    public function checkName($lang,$name,$id = null){
        $oSelect = $this->where('service_name_'.$lang,$name);
        if($id != null) {
            $oSelect = $oSelect->where('service_id','<>',$id);
        }

        return $oSelect->get();
    }

    public function getLastOrder(){
        $oSelect = $this->select("{$this->table}.service_code")->latest("created_at")->first();

        return $oSelect;
    }
}