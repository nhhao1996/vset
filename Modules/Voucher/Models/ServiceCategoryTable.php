<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 8/26/2020
 * Time: 5:31 PM
 */

namespace Modules\Voucher\Models;


use Illuminate\Database\Eloquent\Model;

class ServiceCategoryTable extends Model
{
    protected $table = "service_categories";
    protected $primaryKey = "service_category_id";

    const IS_ACTIVE = 1;
    const NOT_DELETE = 0;

    /**
     * Lấy option loại gói dịch vụ
     *
     * @return mixed
     */
    public function getOption()
    {
        return $this
            ->select(
                "service_category_id",
                "name_vi",
                "name_en"
            )
            ->where("is_actived", self::IS_ACTIVE)
            ->where("is_deleted", self::NOT_DELETE)
            ->get()->toArray();
    }

    public function getNameByCategoryId($service_category_id){
        $oSelect = $this->where('service_category_id',$service_category_id)->first();
        return $oSelect;
    }
}