<?php


namespace Modules\Notification\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class NotificationCustomerGroupTable extends Model
{
    use ListTableTrait;
    public $timestamps = false;
    protected $table = "notification_customer_group";
    protected $primaryKey = 'notification_customer_group_id';
    protected $fillable = [
        "notification_customer_group_id",
        "notification_template_id",
        "member_level_id"
    ];

    public function insertGroup($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function getListByTemplateId($id){
        $oSelect = $this->where('notification_template_id',$id)->get();
        return $oSelect;
    }

    public function deleteGroup($notification_template_id){
        $oSelect = $this->where('notification_template_id',$notification_template_id)->delete();
        return $oSelect;
    }
}