<?php


namespace Modules\Notification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class NotificationTable extends Model
{
    use ListTableTrait;
    protected $table = 'notification';
    protected $primaryKey = 'notification_id';
    protected $fillable=[
        'notification_id',
        'notification_detail_id',
        'user_id',
        'notification_avatar',
        'notification_title',
        'notification_message',
        'is_read',
        'created_at',
        'updated_at',
    ];


    public function _getList(&$filter = [])
    {
        $select = $this
            ->join('customers','customers.customer_id','notification.user_id');
        if (isset($filter['search']) != "") {
            $search = $filter['search'];
            $select = $select->where(function ($query) use ($search) {
                $query->where('customers.full_name', 'like', '%' . $search . '%')
                    ->orWhere('notification.notification_title_vi', 'like', '%' . $search . '%')
                    ->orWhere('notification.notification_message_vi', 'like', '%' . $search . '%');
            });
            unset($filter['search']);
        }

        if (isset($filter['is_read']) != "") {
            $select = $select->where('is_read',$filter['is_read']);
            unset($filter['is_read']);
        }
        if (isset($filter['created_at']) != "") {
            $arr_filter = explode(" - ", $filter["created_at"]);
            $startTime = Carbon::createFromFormat('d/m/Y', $arr_filter[0])->format('Y-m-d');
            $endTime = Carbon::createFromFormat('d/m/Y', $arr_filter[1])->format('Y-m-d');
            $select = $select->whereBetween('notification.created_at', [$startTime. ' 00:00:00', $endTime. ' 23:59:59']);
            unset($filter['created_at']);
        }

        return $select->select('notification.*','customers.full_name')->orderBy('notification_id','DESC');
    }

    /**
     * Danh sách đã click thông báo
     *
     * @return mixed
     */
    public function getListIsRead($filter)
    {
        $oSelect = $this->selectRaw('count(*) as total_is_read, notification_detail_id')
            ->where('is_read', 1)
            ->groupBy('notification_detail_id');

        if (isset($filter['tenant_id'])) {
            $oSelect->where('tenant_id', $filter['tenant_id']);
        }
        return $oSelect->get();
    }

    /**
     * Danh sách đã gửi
     *
     * @return mixed
     */
    public function getListIsSend($filter)
    {
        $oSelect = $this->selectRaw('count(*) as total_is_send, notification_detail_id')
            ->groupBy('notification_detail_id');

        if (isset($filter['tenant_id'])) {
            $oSelect->where('tenant_id', $filter['tenant_id']);
        }
        return $oSelect->get();
    }

    public function getFirst($detail_id)
    {
        return $this->where('notification_detail_id', $detail_id)->first();
    }

    public function getDetail($id)
    {
        return $this
            ->join('customers','customers.customer_id','notification.user_id')
            ->where('notification.notification_id', $id)
            ->select('notification.*','customers.full_name')
            ->first();
    }

    public function getAllByDetailTemplate($arrId)
    {
        $result = $this
            ->select('notification_detail_id')
            ->selectRaw('SUM(CASE WHEN is_read=1 THEN 1 ELSE 0 END) as is_read')
            ->selectRaw('SUM(CASE WHEN is_read=0 THEN 1 ELSE 0 END) as is_not_read')
            ->selectRaw('COUNT(*) as total')
            ->whereIn('notification_detail_id', $arrId)
            ->groupBy('notification_detail_id')->get();
        if($result->count()){
            return $result->keyBy('notification_detail_id')->toArray();
        }
        return [];
    }

    public function updateAvatar($link){
        $oSelect = $this->whereNull('notification_avatar')->update(['notification_avatar' => $link]);
        return $oSelect;
    }
}
