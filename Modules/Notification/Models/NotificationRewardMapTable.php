<?php


namespace Modules\Notification\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class NotificationRewardMapTable extends Model
{
    use ListTableTrait;
    public $timestamps = false;
    protected $table = "notification_reward_map";
    protected $primaryKey = 'notification_reward_map_id';
    protected $fillable = [
        "notification_reward_map_id",
        "notification_reward_id",
        "service_id",
        "created_by",
        "created_at",
        "updated_at",
        "updated_by",
    ];

    public function insertRewardMap($data) {
        $oSelect = $this->insert($data);
        return $oSelect;
    }

    public function getListByRewardId($notification_reward_id) {
        $oSelect = $this
            ->join('services','services.service_id','notification_reward_map.service_id')
            ->where('notification_reward_map.notification_reward_id',$notification_reward_id)
            ->select('notification_reward_map.*','services.service_name_vi')
            ->get();
        return $oSelect;
    }

    public function deleteRewardMap($notification_reward_id){
        $oSelect = $this->where('notification_reward_id',$notification_reward_id)->delete();
        return $oSelect;
    }
}