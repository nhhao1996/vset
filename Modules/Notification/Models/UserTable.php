<?php

namespace Modules\Notification\Models;

use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class UserTable extends Model
{
    use ListTableTrait;
    protected $table = 'customers';
    protected $primaryKey = 'customer_id';
    protected $fillable = [
        'customer_id',
        'full_name',
        'email',
        'phone1',
    ];

    /**
     * Lấy tất cả
     *
     * @return mixed
     */
    public function getList()
    {
        return $this->get();
    }

    public function getListAll($member_level_id = null) {
        $oSelect = $this
            ->where('is_actived',1)
            ->where('is_deleted',0);
        if ($member_level_id != null) {
            $oSelect = $oSelect->where('member_level_id',$member_level_id);
        }
        return $oSelect->get();
    }

    public function getListByMember($groupLevel){
        $oSelect = $this
            ->where('is_actived',1)
            ->where('is_deleted',0)
            ->whereIn('member_level_id',$groupLevel);
        return $oSelect->get();
    }
}
