<?php


namespace Modules\Notification\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class NotificationTemplateTable extends Model
{
    use ListTableTrait;
    public $timestamps = false;
    protected $table = "notification_template";
    protected $primaryKey = 'notification_template_id';
    protected $fillable = [
        "notification_template_id",
        "notification_detail_id",
        "title_vi",
        "title_en",
        "title_short",
        "description_vi",
        "description_en",
        "action_group",
        "action_name",
        "from_type",
        "from_type_object",
        "send_type",
        "send_at",
        "schedule_option",
        "schedule_value",
        "schedule_value_type",
        "send_status",
        "is_actived"
    ];

    public function _getList($filters = []) {
        $oSelect = $this;
        if (isset($filters['search_keyword'])){
            $oSelect = $oSelect->where('title_vi','like','%'.$filters['search_keyword'].'%');
            unset($filters['search_keyword']);
        }
        if (isset($filters['is_actived']) && $filters['is_actived'] != null){
            $oSelect = $oSelect->where('is_actived',$filters['is_actived']);
            unset($filters['is_actived']);
        }
        if (isset($filters['send_status']) && $filters['send_status'] != null){
            $oSelect = $oSelect->where('send_status',$filters['send_status']);
            unset($filters['send_status']);
        }
        $oSelect = $oSelect->orderBy('notification_template_id','DESC');
        return $oSelect;
    }

    public function createNotiTemplate($data) {
        return $this->insertGetId($data);
    }

    public function updateNotiTemplate($id,$data){
        $oSelect = $this->where('notification_template_id',$id)->update($data);
    }

    public function updateNoti($id, $notiDetail) {
        $oSelect = $this->where('notification_template_id',$id)->update(['notification_detail_id'=> $notiDetail]);
        return $oSelect;
    }

    public function getDetail($id) {
        $oSelect = $this
            ->join('notification_detail','notification_detail.notification_detail_id','notification_template.notification_detail_id')
            ->where('notification_template_id',$id)
            ->select($this->table.'.*','notification_detail.background')
            ->first();
        return $oSelect;
    }

    public function getListNoti(){
        $oSelect = $this
            ->where('send_at','<=',Carbon::now())
            ->where('send_status','pending')
            ->get();
        return $oSelect;
    }

    public function deleteTemplate($id){
        $oSelect = $this->where('notification_template_id',$id)->delete();
        return $oSelect;
    }
}