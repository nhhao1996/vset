<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 15-04-02020
 * Time: 3:24 PM
 */

namespace Modules\Notification\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ConfigNotificationTable extends Model
{
    protected $table = "config_notification";
//    protected $primaryKey = "key";
    protected $fillable = [
        "key",
        "name",
        "config_notification_group_id",
        "is_active",
        "display_sort",
        "send_type",
        "schedule_unit",
        "value",
        "created_at",
        "updated_at",
        "updated_by"
    ];

    /**
     * Lấy danh sách cấu hình thông báo
     *
     * @return mixed
     */
    public function getConfig()
    {
        return $this
            ->select(
                "{$this->table}.key",
                "{$this->table}.name",
                "{$this->table}.config_notification_group_id",
                "{$this->table}.is_active",
                "{$this->table}.send_type",
                "{$this->table}.schedule_unit",
                "{$this->table}.value",
                "notification_template_auto.title_vi",
                "notification_template_auto.title_en",
                "notification_template_auto.message_vi",
                "notification_template_auto.message_en",
                "notification_template_auto.avatar",
                "notification_template_auto.detail_background",
                "notification_template_auto.detail_content_vi",
                "notification_template_auto.detail_content_en",
                "notification_template_auto.type_bonus",
                "notification_template_auto.money",
                "notification_template_auto.service_id"
            )
            ->join("notification_template_auto", "notification_template_auto.key", "=", "{$this->table}.key")
            ->where('config_notification.is_active',1)
            ->get();
    }

    /**
     * Lấy thông tin cấu hình
     *
     * @param $key
     * @return mixed
     */
    public function getInfo($key)
    {
        return $this
            ->select(
                "{$this->table}.key",
                "{$this->table}.name",
                "{$this->table}.config_notification_group_id",
                "{$this->table}.is_active",
                "{$this->table}.send_type",
                "{$this->table}.schedule_unit",
                "{$this->table}.value",
                "notification_template_auto.title_vi",
                "notification_template_auto.title_en",
                "notification_template_auto.message_vi",
                "notification_template_auto.message_en",
                "notification_template_auto.avatar",
                "notification_template_auto.detail_background",
                "notification_template_auto.detail_content_vi",
                "notification_template_auto.detail_content_en",
                "notification_template_auto.type_bonus",
                "notification_template_auto.money",
                "notification_template_auto.service_id"
            )
            ->join("notification_template_auto", "notification_template_auto.key", "=", "{$this->table}.key")
            ->where("{$this->table}.key", $key)
            ->where("{$this->table}.is_active", 1)
            ->first();
    }

    /**
     * Chỉnh sửa cấu hình
     *
     * @param array $data
     * @param $key
     * @return mixed
     */
    public function edit(array $data, $key)
    {
        return $this->where("key", $key)->update($data);
    }

    public function getNotiByDay($day){
        $oSelect = $this
            ->where('is_active',1)
            ->where('send_type','immediately')
            ->where(DB::raw("(DATE_FORMAT(value,'%Y-%m-%d') )"),$day)->get();
        return $oSelect;
    }

    public function getNotiByDayMonth($day){
        $oSelect = $this
            ->where('is_active',1)
            ->where('send_type','immediately')
            ->where(DB::raw("(DATE_FORMAT(value,'%m-%d') )"),$day)->get();
        return $oSelect;
    }


}