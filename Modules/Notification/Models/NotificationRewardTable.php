<?php


namespace Modules\Notification\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use MyCore\Models\Traits\ListTableTrait;

class NotificationRewardTable extends Model
{
    use ListTableTrait;
    public $timestamps = false;
    protected $table = "notification_reward";
    protected $primaryKey = 'notification_reward_id';
    protected $fillable = [
        "notification_reward_id",
        "notification_template_id",
        "key_reward",
        "value"
    ];

    public function deleteReward($notification_template_id){
        $oSelect = $this->where('notification_template_id',$notification_template_id)->delete();
        return $oSelect;
    }

    public function insertReward($data) {
        $oSelect = $this->insertGetId($data);
        return $oSelect;
    }

    public function getListByTemplateId($id){
        $oSelect = $this->where('notification_template_id',$id)->get();
        return $oSelect;
    }

    public function getListByTemplateByTemplateId($id){
        $oSelect = $this->where('notification_template_id',$id)->first();
        return $oSelect;
    }

    public function getListByTemplateFirst($id){
        $oSelect = $this->where('notification_template_id',$id)->first();
        return $oSelect;
    }
}