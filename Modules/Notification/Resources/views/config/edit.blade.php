@extends('layout')
@section('title_header')
    {{--    <span class="title_header">QUẢN LÝ CHI NHÁNH</span>--}}
@stop
@section('content')
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                         <i class="la la-edit"></i>
                     </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHỈNH SỬA CẤU HÌNH THÔNG BÁO TỰ ĐỘNG')}}
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <form id="form-edit">
            {!! csrf_field() !!}
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                {{__('Tên thông báo')}}:<b class="text-danger">*</b>
                            </label>
                            <input type="text" name="name" class="form-control m-input" id="name"
                                   value="{{$item['name']}}" placeholder="{{__('Nhập tên thông báo')}}..." disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                {{__('Loại gửi')}}:<b class="text-danger">*</b>
                            </label>
                            <div class="input-group">
                                <select class="form-control" style="width: 100%" id="send_type" name="send_type"
                                        onchange="index.changeType(this)">
                                    <option></option>
                                    <option value="immediately" {{$item['send_type'] == "immediately" ? "selected" : ""}}>
                                        {{__('Gửi ngay')}}
                                    </option>
{{--                                    <option value="before" {{$item['send_type'] == "before" ? "selected" : ""}}>{{__('Gửi trước')}}--}}
{{--                                    </option>--}}
{{--                                    <option value="after" {{$item['send_type'] == "after" ? "selected" : ""}}>{{__('Gửi sau')}}--}}
{{--                                    </option>--}}
{{--                                    <option value="in_time" {{$item['send_type'] == "in_time" ? "selected" : ""}}>{{__('Trong khoảng thời gian')}}--}}
{{--                                    </option>--}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                {{__('Đơn vị cộng thêm')}}:
                            </label>
                            <div class="input-group">
                                <select class="form-control" style="width: 100%" id="schedule_unit" name="schedule_unit"
                                        {{in_array( $item['send_type'], ['immediately', 'in_time']) ? 'disabled' : ''}}>
                                    <option></option>
                                    <option value="day" {{$item['schedule_unit'] == "day" ? "selected" : ""}}>{{__('Ngày')}}
                                    </option>
                                    <option value="hour" {{$item['schedule_unit'] == "hour" ? "selected" : ""}}>{{__('Giờ')}}
                                    </option>
                                    <option value="minute" {{$item['schedule_unit'] == "minute" ? "selected" : ""}}>
                                        {{__('Phút')}}
                                    </option>
                                </select>
                                <span class="error_schedule_unit" style="color:#FF0000;"></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                Giá trị:
                            </label>
                            <div class="input-group" id="div-value">
                                <input type="text" name="value" class="form-control m-input" id="value"
                                       value="{{$item['value']}}" placeholder="{{__('Nhập giá trị')}}..."
                                        {{in_array( $item['send_type'], ['immediately', 'in_time']) ? 'disabled' : ''}}>
                            </div>
                            <span class="error_value" style="color:#FF0000;"></span>
                        </div>
                        <div class="form-group m-form__group ">
                            <div class="row">
                                <div class="col-lg-3 w-col-mb-100">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('getFile').click()"
                                       class="btn  btn-sm m-btn--icon color">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>
                                                    {{__('Thêm avatar')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                                <div class="col-lg-9 w-col-mb-100 div_avatar">
                                    <input type="hidden" id="avatar_old" name="avatar_old" value="{{$item['avatar']}}">
                                    <div class="wrap-img avatar float-left">
                                        @if($item['avatar']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blah"
                                                 src="{{$item['avatar']}}"
                                                 alt="{{__('Hình ảnh')}}" width="100px" height="100px">
                                            <span class="delete-img" id="delete-avatar" style="display: block">
                                                    <a href="javascript:void(0)" onclick="index.remove_avatar()">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                 </span>
                                        @else
                                            <img class="m--bg-metal m-image img-sd" id="blah"
                                                 src="{{asset('uploads/admin/service_card/default/hinhanh-default3.png')}}"
                                                 alt="{{__('Hình ảnh')}}" width="100px" height="100px">
                                            <span class="delete-img" id="delete-avatar">
                                                    <a href="javascript:void(0)" onclick="index.remove_avatar()">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                    </span>
                                        @endif
                                        <input type="hidden" id="avatar" name="avatar">
                                    </div>
                                    <div class="form-group m-form__group float-left m--margin-left-20 warning_img">
                                        <label for="">{{__('Kích thước')}}: <b class="image-info image-size">
                                                @if($item['avatar']!=null)
                                                    {{$widthAvatar.'x'.$heightAvatar.'px'}}
                                                @endif
                                            </b>
                                        </label>
                                        <br>
                                        <label for="">{{__('Dung lượng')}}: <b
                                                    class="image-info image-capacity">
                                                @if($item['avatar']!=null)
                                                    {{$sizeAvatar.'kb'}}
                                                @endif
                                            </b>
                                        </label><br>
                                        <label for="">{{__('Cảnh báo')}}: <b class="image-info">{{__('Tối đa')}} 10MB (10240KB)</b>
                                        </label><br>
                                        <span class="error_img" style="color:red;"></span>
                                    </div>
                                    <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                           data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                           id="getFile"
                                           type="file"
                                           onchange="uploadAvatar(this);" class="form-control"
                                           style="display:none">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group ">
                            <div class="row">
                                <div class="col-lg-3 w-col-mb-100">
                                    <a href="javascript:void(0)"
                                       onclick="document.getElementById('getFileBackground').click()"
                                       class="btn  btn-sm m-btn--icon color">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>
                                                    {{__('Thêm background')}}
                                                </span>
                                            </span>
                                    </a>
                                </div>
                                <div class="col-lg-9 w-col-mb-100 div_avatar">
                                    <input type="hidden" id="detail_background_old" name="detail_background_old"
                                           value="{{$item['detail_background']}}">
                                    <div class="wrap-img background float-left">
                                        @if($item['detail_background']!=null)
                                            <img class="m--bg-metal m-image img-sd" id="blahBg"
                                                 src="{{$item['detail_background']}}"
                                                 alt="{{__('Hình ảnh')}}" width="100px" height="100px">
                                            <span class="delete-img" id="delete-bg" style="display: block">
                                                    <a href="javascript:void(0)" onclick="index.remove_background()">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                 </span>
                                        @else
                                            <img class="m--bg-metal m-image img-sd" id="blahBg"
                                                 src="{{asset('uploads/admin/service_card/default/hinhanh-default3.png')}}"
                                                 alt="{{__('Hình ảnh')}}" width="100px" height="100px">
                                            <span class="delete-img" id="delete-bg">
                                                    <a href="javascript:void(0)" onclick="index.remove_background()">
                                                        <i class="la la-close"></i>
                                                    </a>
                                                    </span>
                                        @endif
                                        <input type="hidden" id="detail_background" name="detail_background">
                                    </div>
                                    <div class="form-group m-form__group float-left m--margin-left-20 warning_img">
                                        <label for="">{{__('Kích thước')}}: <b class="image-info image-size-bg">
                                                @if($item['detail_background']!=null)
                                                    {{$widthBackground.'x'.$heightBackground.'px'}}
                                                @endif
                                            </b>
                                        </label>
                                        <br>
                                        <label for="">{{__('Dung lượng')}}: <b
                                                    class="image-info-bg image-capacity-bg">
                                                @if($item['detail_background']!=null)
                                                    {{$sizeBackground.'kb'}}
                                                @endif
                                            </b>
                                        </label><br>
                                        <label for="">{{__('Cảnh báo')}}: <b class="image-info">{{__('Tối đa')}} 10MB (10240KB)</b>
                                        </label><br>
                                        <span class="error_bg" style="color:red;"></span>
                                    </div>
                                    <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"
                                           data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"
                                           id="getFileBackground"
                                           type="file"
                                           onchange="uploadBackground(this);" class="form-control"
                                           style="display:none">
                                </div>
                            </div>
                        </div>
                        @if(in_array($item['key'],[
                                'customer_birthday',
                                'happy_new_year',
                                'woman_day',
                                'liberation_south',
                                'labor_day',
                                'national_day',
                                'vietnamese_women_day',
                                'merry_christmas',
                                ]))
                            <div class="form-group col-12 d-none">
                                <label>
                                    {{__('Cấu hình tặng thưởng')}}:
                                </label>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <label class="col-lg-3 col-form-label">Voucher:</label>
                                            <div class="col-9">
                                                <label class="kt-checkbox d-inline-block">
                                                    <input type="radio" class="voucher" value="voucher" name="type_bonus" {{$item['type_bonus'] == 'voucher' ? 'checked' : ''}}>
                                                    <span></span>
                                                </label>
                                                <select class="form-control w-50 d-inline-block select-voucher" {{$item['type_bonus'] == 'voucher' ? '' : 'disabled'}}>
                                                    <option value="">Chọn voucher</option>
                                                    @foreach($listCategoryService as $itemService)
                                                        <option value="{{$itemService['service_id']}}" {{$item['service_id'] == $itemService['service_id'] ? 'selected' : ''}}>{{$itemService['service_name_vi']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 pt-3">
                                        <div class="row">
                                            <label class="col-lg-3 col-form-label">Tiền mặt (Vnđ):</label>
                                            <div class="col-9">
                                                <label class="kt-checkbox d-inline-block">
                                                    <input type="radio" class="money" value="money" name="type_bonus" {{$item['type_bonus'] == 'money' ? 'checked' : ''}}>
                                                    <span></span>
                                                </label>
                                                <input type="text" name="value" class="form-control w-50 d-inline-block input-money number-money" value="{{number_format($item['money'], 2)}}" {{$item['type_bonus'] == 'money' ? '' : 'disabled'}}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 pt-3">
                                        <button type="button" class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md cancel_bonus">
                                            Hủy thưởng
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                {{__('Tiêu đề (VI)')}}:<b class="text-danger">*</b>
                            </label>
                            <input type="text" name="title_vi" class="form-control m-input" id="title_vi"
                                   value="{{$item['title_vi']}}" placeholder="Nhập {{__('Tiêu đề (VI)')}}...">
                            <span class="error-name-vi"></span>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="black_title">
                                {{__('Tiêu đề (EN)')}}:<b class="text-danger">*</b>
                            </label>
                            <input type="text" name="title_en" class="form-control m-input" id="title_en"
                                   value="{{$item['title_en']}}" placeholder="Nhập {{__('Tiêu đề (EN)')}}...">
                            <span class="error-name-vi"></span>
                        </div>
                        <div class="form-group">
                            <label>
                                {{__('Nội dung tin nhắn (VI)')}}:<b class="text-danger">*</b>
                            </label>
                            <textarea placeholder="{{__('Nhập nội dung thông báo (VI)')}}" rows="5" cols="40"
                                      name="message_vi" id="message_vi" class="form-control">{{$item['message_vi']}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>
                                {{__('Nội dung tin nhắn (EN)')}}:<b class="text-danger">*</b>
                            </label>
                            <textarea placeholder="{{__('Nhập nội dung thông báo (EN)')}}" rows="5" cols="40"
                                      name="message_en" id="message_en" class="form-control">{{$item['message_en']}}</textarea>
                        </div>
                        <div class="form-group row">
                            @if (in_array($item['key'],
                            ['order_bond_status_W',
                            'order_bond_status_C',
                            'order_bond_status_S',
                            'order_bond_status_A',
                            'order_saving_status_W',
                            'order_saving_status_A',
                            'order_saving_status_C',
                            'order_saving_status_S',
                            'order_service_status_C',
                            'order_service_status_S',
                            ]))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_vi('[order_code]')">{{__('Mã đơn hàng (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_en('[order_code]')">{{__('Mã đơn hàng (EN)')}}</a>
                            @elseif(in_array($item['key'], ['order_service_status_W', 'order_service_status_A']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_vi('[order_service_code]')">{{__('Mã dịch vụ (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_en('[order_service_code]')">{{__('Mã dịch vụ (EN)')}}</a>
                            @elseif(in_array($item['key'], ['withdraw_interest_status_A', 'withdraw_saving_status_A','withdraw_commission_status_A','withdraw_interest_status_C','withdraw_saving_status_C']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_vi('[withdraw_request_code]')">{{__('Mã rút tiền (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_vi('[withdraw_request_code]')">{{__('Mã rút tiền (EN)')}}</a>
                            @elseif(in_array($item['key'], ['customer_ranking', 'customer_birthday','customer_W']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_vi('[name]')">{{__('Tên (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_en('[name]')">{{__('Tên (EN)')}}</a>
                                @if(in_array($item['key'], ['customer_birthday']))
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_txa_vi('[gender]')">{{__('Giới tính (VI)')}}</a>
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_txa_en('[gender]')">{{__('Giới tính (EN)')}}</a>
                                @endif
                            @elseif(in_array($item['key'], ['custom_contract_confirm']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_txa_vi('[customer_contract_code]')">{{__('Mã hợp đồng (VI)')}}
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_txa_en('[customer_contract_code]')">{{__('Mã hợp đồng (EN)')}}</a>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>
                                {{__('Nội dung chi tiết (VI)')}}:<b class="text-danger">*</b>
                            </label>
                            <textarea placeholder="{{__('Nhập nội dung thông báo (VI)')}}" rows="5" cols="40"
                                      name="detail_content_vi" id="detail_content_vi"
                                      class="form-control">{{$item['detail_content_vi']}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>
                                {{__('Nội dung chi tiết (EN)')}}:<b class="text-danger">*</b>
                            </label>
                            <textarea placeholder="{{__('Nhập nội dung thông báo (EN)')}}" rows="5" cols="40"
                                      name="detail_content_en" id="detail_content_en"
                                      class="form-control">{{$item['detail_content_en']}}</textarea>
                        </div>
                        <div class="form-group row">
                            @if (in_array($item['key'], ['order_status_B', 'order_status_C', 'order_status_D', 'order_status_I', 'order_status_S', 'order_status_W']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[order_code]')">{{__('Mã đơn hàng (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[order_code]')">{{__('Mã đơn hàng (EN)')}}</a>
                            @elseif(in_array($item['key'], ['appointment_C', 'appointment_R', 'appointment_W', 'appointment_U']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[branch_name]')">{{__('Chi nhánh (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[time]')"> {{__('Thời gian đặt (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[date]')">{{__('Ngày đặt (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[branch_name]')">{{__('Chi nhánh (EN)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[time]')"> {{__('Thời gian đặt (EN)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[date]')">{{__('Ngày đặt (EN)')}}</a>
                                @if($item['key'] == 'appointment_U')
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_txa_vi('[appointment_code]')">{{__('Mã lịch hẹn (VI)')}}</a>
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_txa_en('[appointment_code]')">{{__('Mã lịch hẹn (EN)')}}</a>
                                @endif
                            @elseif(in_array($item['key'], ['service_card_expired', 'service_card_nearly_expired', 'service_card_over_number_used']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[name]')">{{__('Tên thẻ (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[name]')">{{__('Tên thẻ (EN)')}}</a>
                                @if($item['key'] != 'service_card_over_number_used')
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_vi('[expired_date]')">{{__('Ngày hết hạn (VI)')}}</a>
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_en('[expired_date]')">{{__('Ngày hết hạn (EN)')}}</a>
                                @endif
                            @elseif(in_array($item['key'], ['customer_ranking', 'customer_W']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[name]')">{{__('Tên khách hàng (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[name]')">{{__('Tên khách hàng (EN)')}}</a>
                            @elseif(in_array($item['key'], ['delivery_W']))
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_vi('[delivery_history_code]')">{{__('Mã giao hàng (VI)')}}</a>
                                <a href="javascript:void(0)"
                                   class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                   style="color: black;" onclick="index.append_para_ck_en('[delivery_history_code]')">{{__('Mã giao hàng (EN)')}}</a>
                            @endif
                            @if(strpos($item['message_vi'],'[id]'))
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_vi('[id]')">{{__('Mã giao dịch (VI)')}}</a>
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_en('[id]')">{{__('Mã giao dịch(EN)')}}</a>
                                @endif
                                @if(strpos($item['message_vi'],'[full_name]'))
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_vi('[full_name]')">{{__('Tên(VI)')}}</a>
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_en('[full_name]')">{{__('Tên(EN)')}}</a>
                                @endif
                                @if(strpos($item['message_vi'],'[money]'))
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_vi('[money]')">{{__('Số tiền(VI)')}}</a>
                                    <a href="javascript:void(0)"
                                       class="btn m--margin-left-10 btn-sm ss--btn-parameter ss--font-weight-200"
                                       style="color: black;" onclick="index.append_para_ck_en('[money]')">{{__('Số tiền(EN)')}}</a>
                                @endif
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('config')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                            <span>
                                <i class="la la-arrow-left"></i>
                                <span>{{__('HỦY')}}</span>
                            </span>
                        </a>
                        <button type="button" onclick="index.save('{{$item['key']}}')"
                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">
                            <span>
                                <i class="la la-check"></i>
                                <span>{{__('LƯU THÔNG TIN')}}</span>
                        </span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/sinh-custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('static/backend/css/customize.css')}}">
    <style>
        .m-image {
            max-width: 99%;
        }
    </style>
@stop
@section('after_script')
    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script type="text/template" id="avatar-tpl">
        <img class="m--bg-metal m-image img-sd" id="blah"
             src="{{asset('uploads/admin/service_card/default/hinhanh-default3.png')}}"
             alt="{{__('Hình ảnh')}}" width="100px" height="100px">
        <span class="delete-img" id="delete-avatar"><a href="javascript:void(0)" onclick="index.remove_avatar()">
            <i class="la la-close"></i></a>
        </span>
        <input type="hidden" id="avatar" name="avatar" value="">
    </script>
    <script type="text/template" id="background-tpl">
        <img class="m--bg-metal m-image img-sd" id="blah"
             src="{{asset('uploads/admin/service_card/default/hinhanh-default3.png')}}"
             alt="{{__('Hình ảnh')}}" width="100px" height="100px">
        <span class="delete-img" id="delete-bg"><a href="javascript:void(0)" onclick="index.remove_background()">
            <i class="la la-close"></i></a>
        </span>
        <input type="hidden" id="detail_background" name="detail_background" value="">
    </script>
    <script type="text/template" id="input-val-tpl">
        <input type="text" name="value" class="form-control m-input" id="value" placeholder="{{__('Nhập giá trị')}}...">
    </script>
    <script type="text/template" id="input-time-tpl">
        <input type="text" id="value" name="value" class="form-control m-input" readonly placeholder="{{__('Nhập giá trị')}}">
    </script>
    <script src="{{asset('static/backend/js/notification/config/script.js?v='.time())}}"
            type="text/javascript"></script>
    <script>
        index._init();
        new AutoNumeric.multiple('.number-money', {
            currencySymbol: '',
            decimalCharacter: '.',
            digitGroupSeparator: ',',
            decimalPlaces: 2
        });
        $('#detail_content_vi').summernote({
            height: 150,
            placeholder: '{{__('Nhập nội dung')}}...',
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });
        $('#detail_content_en').summernote({
            height: 150,
            placeholder: '{{__('Nhập nội dung')}}...',
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });
    </script>
@stop


