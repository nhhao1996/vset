@foreach($list as $item)
    <tr>
        <td>{{$item['service_name_vi']}}</td>
        <td>
            <button type="button" onclick="script.removeService({{$item['service_id']}})" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                <i class="la la-trash"></i>
            </button>
        </td>
    </tr>
@endforeach