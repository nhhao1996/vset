<div id="autotable-product">
    <form class="frmFilter bg">
        <div class="row padding_row form-group">
            <div class="col-lg-4">
                <div class="form-group m-form__group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="{{__('Nhập tên sản phẩm')}}">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 form-group">
{{--                <div class="row">--}}
{{--                    <div class="col-lg-12">--}}
{{--                        <div class="row">--}}
{{--                            @php $i = 0; @endphp--}}
{{--                            @foreach ($FILTER as $name => $item)--}}
{{--                                @if ($i > 0 && ($i % 4 == 0))--}}
{{--                        </div>--}}
{{--                        <div class="form-group m-form__group row align-items-center">--}}
{{--                            @endif--}}
{{--                            @php $i++; @endphp--}}
{{--                            <div class="col-lg-12 input-group">--}}
{{--                                @if(isset($item['text']))--}}
{{--                                    <div class="input-group-append">--}}
{{--                        <span class="input-group-text">--}}
{{--                            {{ $item['text'] }}--}}
{{--                        </span>--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                                {!! Form::select($name, $item['data'], $item['default'] ?? null, ['class' => 'form-control m-input']) !!}--}}
{{--                            </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="col-lg-2 form-group">
                <button class="btn btn-primary color_button btn-search">
                    {{__('TÌM KIẾM')}} <i class="fa fa-search ic-search m--margin-left-5"></i>
                </button>
            </div>
        </div>
    </form>
    <div class="form-group table-responsive">
        <table class="table table-striped m-table m-table--head-bg-default">
            <thead class="bg">
            <tr>
                <th class="tr_thead_list"></th>
                <th class="tr_thead_list">{{__('Hình ảnh')}}</th>
                <th class="tr_thead_list">{{__('Tên sản phẩm"')}}</th>
                <th class="tr_thead_list">{{__('Mã sản phẩm')}}</th>
                <th class="tr_thead_list">{{__('Giá')}}</th>
                <th class="tr_thead_list">{{__('Đơn vị tính')}}</th>
            </tr>
            </thead>
            <tbody style="font-size: 13px">
            @if(isset($LIST))
                @foreach ($LIST as $key => $item)
                    <tr>
                        <td>

                        </td>
                        <td></td>
                        <td>{{$item['product_name']}}</td>
                        <td>{{$item['product_code']}}</td>
                        <td>{{number_format($item['cost'])}}</td>
                        <td>{{$item['unit_name']}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    {{ $LIST->links('helpers.paging') }}
</div>
