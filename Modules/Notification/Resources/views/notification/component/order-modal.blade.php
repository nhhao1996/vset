<button type="button" id="action-button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_1" style="display: none;">Launch Modal</button>
<div class="modal fade show" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $trans['create']['detail_form']['order']['title'] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <div class="form-group">
                    <div class="row">
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="order_code" type="text"
                                   placeholder="@lang('product::order.index.ORDER_CODE')">
                        </div>
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="customer_name"
                                   type="text"
                                   placeholder="@lang('product::order.index.CUSTOMER_NAME')">
                        </div>
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="phone" type="text"
                                   placeholder="@lang('product::order.index.PHONE')">
                        </div>
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="store_name" type="text"
                                   placeholder="@lang('product::order.index.STORE_NAME')">
                        </div>
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="brand_company"
                                   type="text"
                                   placeholder="@lang('product::order.index.BRAND_COMPANY')">
                        </div>
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="product_name"
                                   type="text"
                                   placeholder="@lang('product::order.index.PRODUCT_NAME')">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="sku_sale" type="text"
                                   placeholder="@lang('product::order.index.SKU')">
                        </div>
                        <div class="form-group col-lg-2">
                            <input class="form-control " name="address" type="text"
                                   placeholder="@lang('product::order.index.ADDRESS')">
                        </div>
                        <div class="form-group col-lg-4">
                            <div class='input-group'>
                                <label class="col-form-label padding-right">@lang('product::order.index.TIME_ORDER')</label>
                                <input type='text' id='time_order' class="form-control" name="time_order"
                                       readonly placeholder="@lang('product::order.index.PLACEHOLDER_TIME')"
                                />
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-4">
                            <div class='input-group'>
                                <label class="col-form-label padding-right">@lang('product::order.index.TIME_SHIP') </label>
                                <input type='text' id='time_ship' class="form-control" name="time_ship" value=""
                                       readonly placeholder="@lang('product::order.index.PLACEHOLDER_TIME')"
                                />
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-clearfix">
                        <div class="form-group float-right">
                            <button type="button" class="btn btn-primary" id="submit-search" style="float: right">
                                {{ $trans['create']['detail_form']['order']['index']['BTN_SEARCH'] }}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="kt-section__content" id="item-list">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-btn">
                    {{ $trans['create']['detail_form']['order']['index']['BTN_CLOSE'] }}
                </button>
                <button type="button" class="btn btn-primary" id="choose-order">
                    {{ $trans['create']['detail_form']['order']['index']['BTN_ADD'] }}
                </button>
            </div>
        </div>
    </div>
</div>