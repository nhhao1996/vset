<div class="form-group">
    <div class="kt-section__content">
        <table class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>{{ $trans['create']['detail_form']['market']['index']['TABLE_CAMPAIGN_NAME'] }}</th>
                    <th>{{ $trans['create']['detail_form']['market']['index']['TABLE_CAMPAIGN_TYPE'] }}</th>
                    <th>{{ $trans['create']['detail_form']['market']['index']['TABLE_TIME_REGISTER'] }}</th>
                    <th>{{ $trans['create']['detail_form']['market']['index']['TABLE_TIME_MARKETING'] }}</th>
                    <th>{{ $trans['create']['detail_form']['market']['index']['TABLE_DISPLAY'] }}</th>
                    <th class="text-center">
                        {{ $trans['create']['detail_form']['market']['index']['TABLE_HIEU_LUC'] }}
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($detailListPaging))
                    @foreach($detailListPaging as $item)
                        <tr>
                            <td>
                                <label class="kt-radio">
                                    <input type="radio" class="order-radio" name="market_radio"
                                           value="{{ $item['campaign_id'] }}" data-name="{{ $item['campaign_description'] }}"
                                    >
                                    <span></span>
                                </label>
                            </td>
                            <td>{{subString($item['campaign_description'])}}</td>
                            <td>
                                @if($item['campaign_type'] == 1)
                                    On-Invoice
                                @elseif($item['campaign_type'] == 2)
                                    @if($item['is_require_result_image'] == 1)
                                        Display
                                    @else
                                        Accummulative
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if($item['start_reg_date'] != null && $item['end_reg_date'] != null)
                                    @lang('Từ') {{date("d/m/Y",strtotime($item['start_reg_date']))}} <br>
                                    @lang('Đến') {{date("d/m/Y",strtotime($item['end_reg_date']))}}
                                @endif
                            </td>
                            <td>
                                @if($item['start_date'] != null && $item['end_date'] != null)
                                    @lang('Từ') {{date("d/m/Y",strtotime($item['start_date']))}} <br>
                                    @lang('Đến') {{date("d/m/Y",strtotime($item['end_date']))}}
                                @endif
                            </td>
                            <td>
                                <span class="kt-switch kt-switch--success">
                                    <label>
                                        <input type="checkbox"
                                               {{ ($item['is_display']==1) ? 'checked' : '' }} name="is_actived"
                                               onchange="index.change_display('{{$item['campaign_id']}}',this)">
                                        <span></span>
                                    </label>
                                </span>
                            </td>
                            <td class="text-center">
                                @if($item['campaign_code'] != null
                                    && $item['campaign_type'] != null
                                    && $item['start_date'] != null
                                    && $item['end_date']
                                    && $item['is_actived'] == 1)
                                    <i class="la la-check"></i>
                                @else
                                    <i class="la la-close"></i>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </table>
    </div>
</div>
{{ $detailListPaging->links('helpers.paging', ['display' => false]) }}
