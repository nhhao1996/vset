<button type="button" id="action-button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_0" style="display: none;">Launch Modal</button>
<div class="modal fade show" id="kt_modal_0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('admin::notification.create.detail_form.landing_page.title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;">

                <form method="GET" autocomplete="off" id="form-filter">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="view" value="modal">
                        <input type="hidden" name="page" value="1" id="page">
                        <input type="hidden" value="brand_message_board" id="detail_type">
                        <input type="hidden" name="end_point_value" value="{{$end_point_value}}" id="end_point_value">
                        <!--end: Search Form -->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2">
                                    <input class="form-control "
                                           value="{{ isset($filter['keyword_page_card_content$page_card_content_name']) ? $filter['keyword_page_card_content$page_card_content_name'] : '' }}"
                                           name="keyword_page_card_content$page_card_content_name" type="text"
                                           placeholder="@lang('Tên trang')">
                                </div>
                                <div class="col-lg-2">
                                    <select type="text" name="page_card_content$is_activated" class="form-control --select2 ss--width-100">
                                        <option value="">@lang('Trạng thái')</option>
                                        <option value="1"
                                                {{isset($filter['page_card_content$is_activated']) && $filter['page_card_content$is_activated'] == 1 ? 'selected' : ''}}>
                                            @lang('Hoạt động')
                                        </option>
                                        <option value="0"
                                                {{isset($filter['page_card_content$is_activated']) && $filter['page_card_content$is_activated'] == 0 ? 'selected' : ''}}>
                                            @lang('Không hoạt động')
                                        </option>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" onclick="pageBanner.loadListPaging()" class="btn btn-primary kt-margin-l-5">
                                        @lang('Tìm kiếm')
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="content-product-ajax">
                            {!! $html !!}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-btn">
                    @lang('admin::notification.create.detail_form.order.index.BTN_CLOSE')
                </button>
                <button type="button" class="btn btn-primary" id="choose-landing-page">
                    @lang('admin::notification.create.detail_form.landing_page.add')
                </button>
            </div>
        </div>
    </div>
</div>
