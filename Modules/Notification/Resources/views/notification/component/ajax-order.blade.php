<table class="table table-striped">
    <thead>
    <tr>
        <th></th>
        <th>{{ $trans['create']['detail_form']['order']['index']['ORDER_CODE'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['TIME_ORDER'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['TIME_SHIP'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['CUSTOMER_NAME'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['PHONE'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['STORE_NAME'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['TOTAL_MONEY'] }}</th>
        <th>{{ $trans['create']['detail_form']['order']['index']['STATUS'] }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($detailListPaging as $item)
        <tr>
            <td>
                <label class="kt-radio">
                    <input type="radio" class="order-radio" name="order_radio"
                           value="{{ $item['mystore_user_id'] }}" data-code="{{ $item['order_list_id'] }}"
                    >
                    <span></span>
                </label>
            </td>
            <td>
                <a class="kt-link" href="{{route('product.order.show',$item['order_list_id'])}}" title="{{$item['order_code']}}">
                    {{subString($item['order_code'])}}
                </a>
            </td>
            <td><p title="{{date("H:i:s d/m/Y",strtotime($item['order_date']))}}">{{date("H:i:s d/m/Y",strtotime($item['order_date']))}}</p></td>
            <td>
                <?php $tmp = ''; ?>
                @if(count($item['shipping_time_explode']) > 0)
                    @foreach($item['shipping_time_explode'] as $k => $v)
                        @if($k==0)
                            <?php $tmp = $tmp.__('admin::notification.index.from').$v.':00 '.\Carbon\Carbon::parse($item['shipping_date'])->format('d/m/Y'); ?>
                        @else
                            <?php $tmp = $tmp.' &#013;'; ?>
                            <?php $tmp = $tmp.__('admin::notification.index.come').$v.':00 '.\Carbon\Carbon::parse($item['shipping_date'])->format('d/m/Y'); ?>
                        @endif
                    @endforeach
                @endif
                <p title="{{$tmp}}">
                    @if(count($item['shipping_time_explode']) > 0)
                        @foreach($item['shipping_time_explode'] as $k => $v)
                            @if($k==0)
                                @lang('admin::notification.index.from') {{$v.':00 '.\Carbon\Carbon::parse($item['shipping_date'])->format('d/m/Y')}}
                                <br>
                            @else
                                @lang('admin::notification.index.come') {{$v.':00 '.\Carbon\Carbon::parse($item['shipping_date'])->format('d/m/Y')}}
                            @endif
                        @endforeach
                    @endif
                </p>
            </td>
            <td><p title="{{$item['user_name']}}">{{subString($item['user_name'])}}</p></td>
            <td><p title="{{$item['phone']}}">{{subString($item['phone'])}}</p></td>
            <td><p title="{{$item['store_name']}}">{{subString($item['store_name'])}}</p></td>
            <td><p title="{{number_format($item['order_total_amount'])}}">{{subString(number_format($item['order_total_amount']))}}</p></td>
            <td>
                <p
                        @if($item['order_status'] == 'O')
                        title="@lang('admin::notification.index.not-processed')"
                        @elseif($item['order_status'] == 'S')
                        title="@lang('admin::notification.index.on-delivery')"
                        @elseif($item['order_status'] == 'C')
                        title="@lang('admin::notification.index.deliver-claimant')"
                        @elseif($item['order_status'] == 'P')
                        title="@lang('admin::notification.index.partial-delivery')"
                        @elseif($item['order_status'] == 'L')
                        title="@lang('admin::notification.index.cancel')"
                        @endif
                >
                    @if($item['order_status'] == 'O')
                        @lang('admin::notification.index.not-processed')
                    @elseif($item['order_status'] == 'S')
                        @lang('admin::notification.index.on-delivery')
                    @elseif($item['order_status'] == 'C')
                        @lang('admin::notification.index.deliver-claimant')
                    @elseif($item['order_status'] == 'P')
                        @lang('admin::notification.index.partial-delivery')
                    @elseif($item['order_status'] == 'L')
                        @lang('admin::notification.index.cancel')
                    @endif
                </p>
            </td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenu" data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        @lang('product::cart.list-cart.ACTION')
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu">
                        <a class="dropdown-item"
                           href="{{route('product.order.show',$item['order_list_id'])}}">@lang('product::cart.list-cart.VIEW-DETAILS')</a>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $detailListPaging->links('helpers.paging', ['display' => false]) }}
