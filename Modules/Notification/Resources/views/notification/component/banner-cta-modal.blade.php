<button type="button" id="action-button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_0" style="display: none;">Launch Modal</button>
<div class="modal fade show" id="kt_modal_0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('admin::notification.create.detail_form.loyalty_game_detail.title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;">

                <form method="GET" autocomplete="off" id="form-filter">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="view" value="modal">
                        <input type="hidden" name="page" value="1" id="page">
                        <input type="hidden" value="loyalty_game_detail" id="detail_type">
                        <input type="hidden" name="end_point_value" value="{{$end_point_value}}" id="end_point_value">
                        <!--end: Search Form -->
                        <div class="form-group">
                            <div class="row">
                                <div class="form-group col-lg-2">
                                    <input class="form-control"
                                           value="{{isset($filter['keyword_game$game_code']) ? $filter['keyword_game$game_code'] : ''}}" type="text"
                                           name="keyword_game$game_code" placeholder="@lang('game::game.index.program_code')">
                                </div>
                                <div class="form-group col-lg-2">
                                    <input class="form-control"
                                           value="{{isset($filter['keyword_game$game_name']) ? $filter['keyword_game$game_name'] : ''}}" type="text"
                                           name="keyword_game$game_name" placeholder="@lang('game::game.index.program_name')">
                                </div>
                                <div class="form-group col-lg-2">
                                    <select style="width: 100%" type="text" name="keyword_game_category$cate_id" class="form-control --select2 ss--width-100 select_fix">
                                        <option value="">@lang('game::game.index.type_game')</option>
                                        @foreach($listCate as $item)
                                            <option value="{{$item['cate_id']}}" {{isset($filter['keyword_game_category$cate_id']) && $filter['keyword_game_category$cate_id'] == $item['cate_id'] ? 'selected' : ''}}>{{$item['cate_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-2">
                                    <select type="text" name="keyword_game$is_actived" class="form-control --select2 ss--width-100">
                                        <option value="">@lang('game::game.index.status')</option>
                                        <option value="1" {{isset($filter['keyword_game$is_active']) && $filter['keyword_game$is_active'] == '1' ? 'selected' : ''}}>@lang('game::game.index.action')</option>
                                        <option value="0" {{isset($filter['keyword_game$is_active']) && $filter['keyword_game$is_active'] == '0' ? 'selected' : ''}}>@lang('game::game.index.deaction')</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-2">
                                    <input type="text" class="form-control" id="from_date" name="start_date" readonly="" value="{{isset($filter['start_date']) ? $filter['start_date'] : ''}}" placeholder="@lang('game::game.index.date_start')">
                                </div>
                                <div class="form-group col-lg-2">
                                    <input type="text" class="form-control" id="to_date" name="end_date" readonly="" value="{{isset($filter['end_date']) ? $filter['end_date'] : ''}}" placeholder="@lang('game::game.index.date_end')">
                                </div>
                                <div class="form-group col-lg-10">
                                </div>
                                <div class="form-group col-lg-2">
                                    <a class="btn btn-secondary " style="float:right"
                                       href="javascript:void(0)" onclick="pageBanner.resetFormSearch()">@lang('product::list-store-connect.list.delete')</a>
                                    <button type="button" onclick="pageBanner.loadListPaging()" class="btn btn-primary kt-margin-l-5 mr-3" style="float: right">
                                        @lang('product::list-store-connect.list.search')
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="content-product-ajax">
                            {!! $html !!}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-btn">
                    @lang('admin::notification.create.detail_form.order.index.BTN_CLOSE')
                </button>
                <button type="button" class="btn btn-primary" id="choose-page">
                    @lang('admin::notification.create.detail_form.loyalty_game_detail.add')
                </button>
            </div>
        </div>
    </div>
</div>
<link href="{{asset('static/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}"
      rel="stylesheet" type="text/css"/>
<script src="{{asset('static/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"
        type="text/javascript"></script>
<script>
    $('.select_fix').select2();
    $('#from_date').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        // startDate: '0d',
        rtl: KTUtil.isRTL(),
        orientation: "bottom left",
    });
    $('#to_date').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        // startDate: '0d',
        rtl: KTUtil.isRTL(),
        orientation: "bottom left",
    });
</script>
