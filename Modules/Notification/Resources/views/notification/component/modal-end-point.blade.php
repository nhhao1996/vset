<div class="modal fade" id="modal-end-point" role="dialog" style="display: none">
    <div class="modal-dialog modal-dialog-centered modal-lg-email-auto">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_index">
                    {{__('Chọn đích đến')}}
                </h4>
            </div>
            <div class="m-widget4  m-section__content" id="load">
                <div class="modal-body">
                    {!! $html !!}
                </div>
                <div class="modal-footer">
                    <div class="m-form__actions m--align-right btn_receipt w-100">
                        <a href="javascript:void(0)" data-dismiss="modal" class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                           <span>
                            <i class="la la-arrow-left"></i>
                               <span>{{__('HỦY')}}</span>
                           </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
