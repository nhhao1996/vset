<button type="button" id="action-button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_0" style="display: none;">Launch Modal</button>
<div class="modal fade show" id="kt_modal_0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('admin::notification.create.detail_form.product.title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;">

                <form action="{{route('product.index')}}" id="form-filter">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <input type="text" class="form-control"
                                       name="keyword_product_master$product_name"
                                       placeholder="@lang('product::product.index.PRODUCT_NAME')"
                                       value="{{isset($filter['keyword_product_master$product_name'])
                                       ?$filter['keyword_product_master$product_name']:''}}">
                            </div>
                            <input type="hidden" name="view" value="modal">
                            <input type="hidden" value="product" id="detail_type">
                            <input type="hidden" name="page" value="1" id="page">
                            <input type="hidden" name="end_point_value" value="{{$end_point_value}}" id="end_point_value">
                            <div class="col-lg-3">
                                <input type="text" class="form-control"
                                       name="keyword_product_uom$sku_seller"
                                       value="{{isset($filter['keyword_product_uom$sku_seller'])
                                       ?$filter['keyword_product_uom$sku_seller']:''}}"
                                       placeholder="@lang('product::product.index.SELLER_SKU')">
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control"
                                       name="keyword_product_uom$sku_sale"
                                       placeholder="@lang('product::product.index.SKU_SALE')"
                                       value="{{isset($filter['keyword_product_uom$sku_sale'])
                                       ?$filter['keyword_product_uom$sku_sale']:''}}">
                            </div>
                            <div class="col-lg-3">
                                <select name="product_uom$stock_in_trade" class="form-control --select2">
                                    <option value="">
                                        @lang('product::product.index.INVENTORY')
                                    </option>
                                    <option {{isset($filter['product_uom$stock_in_trade'])&&$filter['product_uom$stock_in_trade']==1?'selected':''}}  value="1">
                                        @lang('product::product.index.SALE')
                                    </option>
                                    <option {{isset($filter['product_uom$stock_in_trade'])&&$filter['product_uom$stock_in_trade']==0?'selected':''}} value="0">
                                        @lang('product::product.index.UNSALE')
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
{{--                            <div class="col-lg-3">--}}
{{--                                <select name="product_master$allow_sales" class="form-control --select2">--}}
{{--                                    <option value="">@lang('admin::notification.index.sell-status')</option>--}}
{{--                                    <option {{isset($filter['product_master$allow_sales'])&&$filter['product_master$allow_sales']==1?'selected':''}} value="1">--}}
{{--                                        @lang('product::product.index.IS_ACTIVED')--}}
{{--                                    </option>--}}
{{--                                    <option {{isset($filter['product_master$allow_sales'])&&$filter['product_master$allow_sales']==0?'selected':''}} value="0">--}}
{{--                                        @lang('product::product.index.UN_IS_ACTIVED')--}}
{{--                                    </option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-3">--}}
{{--                                <select name="product_profile$is_published" class="form-control --select2">--}}
{{--                                    <option value="">--}}
{{--                                        @lang('product::product.index.IS_ALLOWS')--}}
{{--                                    </option>--}}
{{--                                    <option {{isset($filter['product_profile$is_published']) && $filter['product_profile$is_published']==1?'selected':''}} value="1">--}}
{{--                                        @lang('product::product.index.IS_SHOW_APP')--}}
{{--                                    </option>--}}
{{--                                    <option {{isset($filter['product_profile$is_published']) && $filter['product_profile$is_published']==0?'selected':''}} value="0">--}}
{{--                                        @lang('product::product.index.UN_IS_SHOW_APP')--}}
{{--                                    </option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
                            <div class="col-lg-9">
                            </div>
                            <div class="col-lg-3">
                                <button type="button" onclick="pageBanner.loadListPaging()" class="btn btn-primary kt-margin-l-5" style="float: right">
                                    @lang('product::product.index.SEARCH')
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="content-product-ajax">
                        {!! $html !!}
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-btn">
                    @lang('admin::notification.create.detail_form.order.index.BTN_CLOSE')
                </button>
                <button type="button" class="btn btn-primary" id="choose-product">
                    @lang('admin::notification.create.detail_form.product.index.BTN_ADD')
                </button>
            </div>
        </div>
    </div>
</div>
