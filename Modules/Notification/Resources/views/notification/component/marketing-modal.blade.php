<button type="button" id="action-button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_0" style="display: none;">Launch Modal</button>
<div class="modal fade show" id="kt_modal_0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $trans['create']['detail_form']['market']['title'] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-3">
                            <input type="text" class="form-control"
                                   name="campaign_description"
                                   placeholder="{{ $trans['create']['detail_form']['market']['index']['CAMPAIGN_DESCRIPTION'] }}"
                            >
                        </div>
                        <div class="col-lg-3">
                            <select class="form-control" id="campaign_type" name="campaign_type" style="width: 100%;">
                                <option value="">
                                    {{ $trans['create']['detail_form']['market']['index']['CAMPAIGN_TYPE'] }}
                                </option>
                                <option value="on-invoice">
                                    {{ $trans['create']['detail_form']['market']['index']['PROMOTION'] }}
                                </option>
                                <option value="display">
                                    {{ $trans['create']['detail_form']['market']['index']['DISPLAY'] }}
                                </option>
                                <option value="accummulative">
                                    {{ $trans['create']['detail_form']['market']['index']['LOYALTY'] }}
                                </option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <select class="form-control" id="is_display" name="is_display" style="width: 100%;">
                                <option value="">
                                    {{ $trans['create']['detail_form']['market']['index']['IS_DISPLAY']['TITLE'] }}
                                </option>
                                <option value="1">
                                    {{ $trans['create']['detail_form']['market']['index']['IS_DISPLAY']['ON'] }}
                                </option>
                                <option value="0">
                                    {{ $trans['create']['detail_form']['market']['index']['IS_DISPLAY']['OFF'] }}
                                </option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <button type="button" class="btn btn-primary" id="submit-search" style="float: right">
                                {{ $trans['create']['group']['BTN_SEARCH'] }}
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="kt-section__content" id="item-list">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-btn">
                    {{ $trans['create']['detail_form']['order']['index']['BTN_CLOSE'] }}
                </button>
                <button type="button" class="btn btn-primary" id="choose-market">
                    {{ $trans['create']['detail_form']['market']['index']['BTN_ADD'] }}
                </button>
            </div>
        </div>
    </div>
</div>
