@extends('layout')
@section('title_header')

@endsection
@section('content')
    <div class="m-portlet m-portlet--head-sm">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                     <span class="m-portlet__head-icon">
                         <i class="fa fa-plus-circle"></i>
                     </span>
                    <h2 class="m-portlet__head-text">
                        {{__('CHI TIẾT THÔNG BÁO MỚI')}}
                    </h2>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>
        <form id="form-add">
            {!! csrf_field() !!}
            <div class="m-portlet__body">
                <h5 class="m-section__heading">{{__('Thông tin người nhận')}}</h5>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Người nhận:</label>
                    <div class="col-9">
                        <div class="kt-checkbox-list ">
                            <label class="kt-checkbox d-block">
                                <input type="checkbox" class="from-all" name="from[]" value="all" {{$detail['from_type'] == 'all' ? 'checked' : ''}}> {{__('Tất cả')}}
                                <span></span>
                            </label>
                            <label class="kt-checkbox d-block">
                                <input type="checkbox" class="rank" name="from[]" value="1" {{$detail['from_type'] == 'group' &&  isset($customerGroup[1]) ? 'checked' : ''}}> {{__('Mới')}}
                                <span></span>
                            </label>
                            <label class="kt-checkbox d-block">
                                <input type="checkbox" class="rank" name="from[]" value="2" {{$detail['from_type'] == 'group' &&  isset($customerGroup[2]) ? 'checked' : ''}}> {{__('Bạc')}}
                                <span></span>
                            </label>
                            <label class="kt-checkbox d-block">
                                <input type="checkbox" class="rank" name="from[]" value="3" {{$detail['from_type'] == 'group' &&  isset($customerGroup[3]) ? 'checked' : ''}}>  {{__('Vàng')}}
                                <span></span>
                            </label>
                            <label class="kt-checkbox d-block">
                                <input type="checkbox" class="rank" name="from[]" value="4" {{$detail['from_type'] == 'group' &&  isset($customerGroup[4]) ? 'checked' : ''}}>  {{__('Kim cương')}}
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                <h5 class="m-section__heading">{{__('Cấu hình tặng thưởng')}}</h5>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="row">
                            <label class="col-lg-3 col-form-label">Voucher:</label>
                            <div class="col-9">
                                <label class="kt-checkbox d-inline-block">
                                    <input type="radio" class="voucher" value="voucher" name="reward[]" {{isset($reward['voucher'] )? 'checked' : ''}}>
                                    <span></span>
                                </label>
                                <button type="button" class="form-control w-25 d-inline-block select-voucher" onclick="script.showPopup()" disabled data-toggle="modal" data-target="#popup_service">Chọn voucher</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 pt-3">
                        <div class="row">
                            <label class="col-lg-3 col-form-label">Tiền mặt (Vnđ):</label>
                            <div class="col-9">
                                <label class="kt-checkbox d-inline-block">
                                    <input type="radio" class="money" value="money" name="reward[]" {{isset($reward['money'] )? 'checked' : ''}}>
                                    <span></span>
                                </label>
                                <input type="text" name="value" class="form-control w-25 d-inline-block input-money number-money" disabled value="{{isset($reward['money'] )? number_format((int)$reward['money']['value'], isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0) : ''}}">
                            </div>
                        </div>
                    </div>
                </div>
                <h5 class="m-section__heading">{{__('Cấu hình thời gian')}}</h5>
                <div class="m-form__group form-group row">
                    <label class="col-lg-3 col-form-label">{{__('Thời gian gửi thông báo')}}: <b class="text-danger">*</b></label>
                    <div class="col-lg-9">
                        <div class="m-radio-list">
                            <label class="m-radio m-radio--success d-none">
                                <input type="radio" name="send_time_radio" value="1" checked> {{__('Gửi cho một tập khách hàng tùy chọn')}}
                                <span></span>
                            </label>
                        </div>
                        <div id="schedule-time" >
                            <div class="row">
                                <div class="col-xl-5 d-none">
                                    <select class="form-control " name="schedule_time">
                                        <option value="specific_time">
                                            @lang('admin::notification.create.form.SPECIFIC_TIME')
                                        </option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <div class="input-group date" id="specific_time_display">
                                        <input type="text" class="form-control" name="specific_time" value="{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$detail['send_at'])->format('d-m-Y H:i')}}"
                                               placeholder="@lang('admin::notification.create.form.placeholder.SPECIFIC_TIME')" id="specific_time" >
                                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-check glyphicon-th"></i>
                                        </span>
                                        </div>
                                    </div>
                                    <div id="non_specific_time_display" style="display: none;">
                                        <div class="row">
                                            <div class="col-xl-7">
                                                <input type="text"
                                                       class="form-control"
                                                       name="non_specific_time"
                                                       placeholder="@lang('admin::notification.create.form.placeholder.NON_SPECIFIC_TIME')"
                                                       id="non_specific_time">
                                            </div>
                                            <div class="col-xl-5">
                                                <select class="form-control" name="time_type">
                                                    <option value="hour">
                                                        @lang('admin::notification.create.form.HOUR')
                                                    </option>
                                                    <option value="minute">
                                                        @lang('admin::notification.create.form.MINUTE')
                                                    </option>
                                                    <option value="day">
                                                        @lang('admin::notification.create.form.DAY')
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h5 class="m-section__heading">{{__('Nội dung thông báo')}}</h5>
                <div class="m-form__group form-group row">
                    <label class="col-lg-3 col-form-label">Background:</label>
                    <div class="col-lg-2">
                        <div class="form-group m-form__group m-widget19">
                            <div class="m-widget19__pic">
                                @if($detail['background'] != null)
                                    <img class="m--bg-metal  m-image  img-sd" id="blah" height="150px"
                                         src="{{$detail['background']}}"
                                         alt="{{__('Hình ảnh')}}"/>
                                @else
                                    <img class="m--bg-metal  m-image  img-sd" id="blah" height="150px"
                                         src="https://vignette.wikia.nocookie.net/recipes/images/1/1c/Avatar.svg/revision/latest/scale-to-width-down/480?cb=20110302033947"
                                         alt="{{__('Hình ảnh')}}"/>
                                @endif

                            </div>
{{--                            <input type="hidden" id="background" name="background">--}}
{{--                            <input accept="image/jpeg,image/png,image/jpeg,jpg|png|jpeg"--}}
{{--                                   data-msg-accept="{{__('Hình ảnh không đúng định dạng')}}"--}}
{{--                                   id="getFile" type='file'--}}
{{--                                   onchange="script.uploadAvatar(this);"--}}
{{--                                   class="form-control"--}}
{{--                                   style="display:none"/>--}}
{{--                            <div class="m-widget19__action" style="max-width: 170px">--}}
{{--                                <a href="javascript:void(0)"--}}
{{--                                   onclick="document.getElementById('getFile').click()"--}}
{{--                                   class="btn  btn-sm m-btn--icon color w-100">--}}
{{--                                            <span class="m--margin-left-20">--}}
{{--                                                <i class="fa fa-camera"></i>--}}
{{--                                                <span>--}}
{{--                                                    {{__('Tải ảnh lên')}}--}}
{{--                                                </span>--}}
{{--                                            </span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="m-form__group form-group row">
                    <label class="col-3 col-form-label">{{__('Tiêu đề thông báo (VI)')}}: <b class="text-danger">*</b> </label>
                    <div class="col-9">
                        <input class="form-control" name="title_vi" type="text"
                               value="{{$detail['title_vi']}}" placeholder="{{__('Tiêu đề thông báo (VI)')}}">
                        @if ($errors->has('title_vi'))
                            <div class="form-control-feedback">{{ $errors->first('title_vi') }}</div>
                        @endif
                    </div>
                </div>
                <div class="m-form__group form-group row">
                    <label class="col-3 col-form-label">{{__('Tiêu đề thông báo (EN)')}}: <b class="text-danger">*</b> </label>
                    <div class="col-9">
                        <input class="form-control" name="title_en" type="text"
                               value="{{$detail['title_en']}}" placeholder="{{__('Tiêu đề thông báo (EN)')}}">
                        @if ($errors->has('title_en'))
                            <div class="form-control-feedback">{{ $errors->first('title_en') }}</div>
                        @endif
                    </div>
                </div>
                <div class="m-form__group form-group row">
                    <label class="col-3 col-form-label">{{__('Chi tiết thông báo (VI)')}}: <b class="text-danger">*</b></label>
                    <div class="col-9">
                        <textarea class="form-control description" name="description_vi"
                                  rows="5">{!! $detail['description_vi'] !!}</textarea>
                        @if ($errors->has('description_vi'))
                            <div class="form-control-feedback">{{ $errors->first('description_vi') }}</div>
                        @endif
                    </div>
                </div>
                <div class="m-form__group form-group row">
                    <label class="col-3 col-form-label">{{__('Chi tiết thông báo (EN)')}}: <b class="text-danger">*</b></label>
                    <div class="col-9">
                        <textarea class="form-control description description_en" name="description_en"
                                  rows="5">{!! $detail['description_en'] !!}</textarea>
                        @if ($errors->has('description_en'))
                            <div class="form-control-feedback">{{ $errors->first('description_en') }}</div>
                        @endif
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                <table class="table add_service_session">
                    <thead>
                    <th>Tên dịch vụ</th>
                    <th></th>
                    </thead>
                    <tbody class="list_add_service">
                        @foreach($rewardMap as $item)
                            <tr>
                                <td>{{$item['service_name_vi']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m--align-right">
                        <a href="{{route('admin.notification')}}"
                           class="btn btn-metal bold-huy m-btn m-btn--icon m-btn--wide m-btn--md">
                            <span>
                                <i class="la la-arrow-left"></i>
                                <span>{{__('Quay lại')}}</span>
                            </span>
                        </a>
{{--                        <a href="{{route('admin.notification.edit',['id' => $detail['notification_template_id']])}}"--}}
{{--                                class="btn btn-success color_button son-mb  m-btn m-btn--icon m-btn--wide m-btn--md m--margin-left-10">--}}
{{--                            <span>--}}
{{--                                <i class="la la-check"></i>--}}
{{--                                <span>{{__('Chỉnh sửa')}}</span>--}}
{{--                            </span>--}}
{{--                        </a>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- Modal action--}}
    <div id="end-point-modal"></div>
    <div id="group-modal"></div>

    <div class="modal fade" id="popup_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Danh sách dịch vụ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="search_service">
                        <div class="form-group">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <input type="text" class="form-control" id="service_name" name="service_name" placeholder="Tên dịch vụ">
                                    </div>
                                    <div class="col-4">
                                        <select class="form-control select-fix" id="service_category" name="service_category">
                                            <option value="">Danh mục</option>
                                            @foreach($listCategoryService as $item)
                                                <option value="{{$item['service_category_id']}}">{{$item['name_vi']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-secondary" onclick="script.deleteSearch()">Xóa</button>
                                        <button type="button" class="btn btn-primary" onclick="script.searchPopup(1)">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-group table-service">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="script.addListService()" data-dismiss="modal">Chọn</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("after_style")
    <link rel="stylesheet" href="{{asset('static/backend/css/son.css')}}">
    <link rel="stylesheet" href="{{asset('static/backend/css/customize.css')}}">
@stop
@section('after_script')
    <script>
        var decimal_number = 0;
        var numberImage = 0;
        var numberImageMobile = 0;
    </script>

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input').prop('disabled',true);
            $('textarea').prop('disabled',true);
            // $('.description').summernote("disable");
            // $('.description_en').summernote("disable");
        })
    </script>
@stop
