<div class="table-responsive">
    <table class="table table-striped m-table ss--header-table ss--nowrap">
        <thead>
        <tr>
            <th class="ss--font-size-th" id="th_stt">
                @lang('STT')
            </th>
            <th class="ss--font-size-th" id="th_title">
                @lang('admin::notification.index.table.header.TITLE')
            </th>
{{--            <th class="ss--font-size-th" id="th_is_sent">--}}
{{--                @lang('admin::notification.index.table.header.NOTIFICATIONS_IS_SENT')--}}
{{--            </th>--}}
{{--            <th class="ss--font-size-th" id="th_read_notification">--}}
{{--                @lang('admin::notification.index.table.header.RATE_READ_NOTIFICATION')--}}
{{--            </th>--}}
            <th class="ss--font-size-th" id="td_send_time">
                @lang('admin::notification.index.table.header.SEND_TIME')
            </th>
{{--            <th class="ss--font-size-th" id="th_active">--}}
{{--                @lang('admin::notification.index.table.header.ACTIVE')--}}
{{--            </th>--}}
            <th class="ss--font-size-th" id="th_is_send">
                @lang('admin::notification.index.table.header.IS_SEND')
            </th>
            <th class="ss--font-size-th">
                @lang('admin::notification.index.table.header.ACTION')
            </th>
        </tr>
        </thead>
        <tbody>
        @if(isset($LIST))
            @foreach($LIST as $key => $noti)
                @if($noti['title_vi'] == null)
                    @continue
                @endif
                <tr>
                    <td>
                        {{($LIST->currentPage() - 1)*$LIST->perPage() + $key + 1}}
                    </td>
                    <td>
                        <p title="{{ $noti['title_vi'] }}">
                            {{ subString($noti['title_vi']) }}
                        </p>
                    </td>
                    <td>
                        {{\Carbon\Carbon::parse($noti['send_at'])->format('d-m-Y H:i')}}
{{--                        <span class="m-switch m-switch--icon m-switch--success m-switch--sm">--}}
{{--                                <label style="margin: 0 0 0 10px; padding-top: 4px">--}}
{{--                                    <input type="checkbox" class="manager-btn is_actived"--}}
{{--                                           data-id="{{ $noti['notification_detail_id'] }}"--}}
{{--                                           data-non-specific-value="{{ $noti['schedule_value'] }}"--}}
{{--                                           data-non-specific-type="{{ $noti['schedule_value_type'] }}"--}}
{{--                                           disabled--}}
{{--                                           @if($noti['is_actived'] == 1) checked="checked" @endif--}}
{{--                                    >--}}
{{--                                    <span></span>--}}
{{--                                </label>--}}
{{--                         </span>--}}
                    </td>
                    <td id="status-{{ $noti['notification_detail_id'] }}">
                        @if($noti['send_status'] == 'sent')
                            @lang('admin::notification.index.search.IS_SEND.SENT')
                        @elseif($noti['send_status'] == 'not')
                            @lang('admin::notification.index.search.IS_SEND.DONT_SEND')
                        @else
                            @lang('Đang chờ gửi')
                        @endif
                    </td>
                    <td>
{{--                        @if(in_array('admin.notification.detail', session('routeList')))--}}
                            <a href="{{route('admin.notification.detail',['id'=>$noti['notification_template_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Chi tiết')}}">
                                <i class="la la-eye"></i>
                            </a>
{{--                        @endif--}}
                        @if($noti['send_status'] == 'pending')
{{--                        @if(in_array('admin.notification.edit', session('routeList')))--}}
                            <a href="{{route('admin.notification.edit',['id'=>$noti['notification_template_id']])}}"
                               class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                               title="{{__('Cập nhật')}}">
                                <i class="la la-edit"></i>
                            </a>
{{--                        @endif--}}
{{--                        @if(in_array('admin.notification.destroy', session('routeList')))--}}
                            <button onclick="removeItem({{$noti['notification_template_id']}})"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                    title="{{__('Xoá')}}">
                                <i class="la la-trash"></i>
                            </button>
{{--                        @endif--}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ $LIST->links('helpers.paging') }}
