<?php

Route::group(['middleware' => ['web', 'auth', 'account'], 'prefix' => 'notification', 'namespace' => 'Modules\Notification\Http\Controllers'], function () {

    Route::group(['prefix' => 'config'], function () {
        Route::get('', 'ConfigController@index')->name('config');
        Route::get('edit/{key}', 'ConfigController@edit')->name('config.edit');
        Route::post('update', 'ConfigController@update')->name('config.update');
        Route::post('change-status', 'ConfigController@changeStatusAction')->name('config.change-status');
        Route::post('upload', 'ConfigController@uploadAction')->name('config.upload');
    });

    Route::get('/', 'NotificationController@index')->name('admin.notification');
    Route::post('list', 'NotificationController@listAction')->name('notification.list');
    Route::post('update-is-actived/{id}', 'NotificationController@updateIsActived')
        ->name('admin.notification.updateIsActived');
    Route::get('create', 'NotificationController@create')->name('admin.notification.create');
    Route::post('store', 'NotificationController@store')->name('admin.notification.store');
    Route::get('edit/{id}', 'NotificationController@edit')->name('admin.notification.edit');
    Route::get('show/{id}', 'NotificationController@detail')->name('admin.notification.detail');
    Route::post('update/{id}', 'NotificationController@update')->name('admin.notification.update');
    Route::post('/upload', 'NotificationController@upload')->name('admin.notification.upload');
    Route::get('/ajax-detail-end-point', 'NotificationController@detailEndPoint')
        ->name('admin.notification.detailEndPoint');
    Route::post('delete/{id}', 'NotificationController@destroy')->name('admin.notification.destroy');
    Route::get('/ajax-group', 'NotificationController@groupList')
        ->name('admin.notification.groupList');
    Route::post('/get-list-service', 'NotificationController@getListService')
        ->name('admin.notification.getListService');
    Route::post('/add-list-service', 'NotificationController@addListService')
        ->name('admin.notification.addListService');
    Route::post('/remove-service', 'NotificationController@removeService')
        ->name('admin.notification.removeService');

});