<?php


namespace Modules\Notification\Repositories\Config;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Notification\Models\ConfigNotificationGroupTable;
use Modules\Notification\Models\ConfigNotificationTable;
use Modules\Notification\Models\NotificationTemplateAutoTable;

class ConfigRepo implements ConfigRepoInterface
{
    protected $config;

    public function __construct(
        ConfigNotificationTable $config
    ) {
        $this->config = $config;
    }

    /**
     * Data view index
     *
     * @return array|mixed
     */
    public function dataIndex()
    {
        $mGroup = new ConfigNotificationGroupTable();
        $getGroup = $mGroup->getGroup();
        $getConfig = $this->config->getConfig();

        if (count($getConfig) > 0) {
            $getConfig = $this->array_group_by($getConfig->toArray(), 'config_notification_group_id');
        }

        return [
            'dataGroup' => $getGroup,
            'dataConfig' => $getConfig
        ];
    }

    /**
     * Function group by
     *
     * @param array $array
     * @param $key
     * @return array|null
     */
    private function array_group_by(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }
        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;
            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && property_exists($value, $_key)) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }
            if ($key === null) {
                continue;
            }
            $grouped[$key][] = $value;
        }
        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();
            foreach ($grouped as $key => $value) {
                $params = array_merge([$value], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }
        return $grouped;
    }

    /**
     * Lấy thông tin cấu hình thông báo
     *
     * @param $key
     * @return array|mixed
     */
    public function getInfo($key)
    {
        $getInfo = $this->config->getInfo($key);

        $typeAvatar = '';
        $sizeAvatar = '';
        $widthAvatar = '';
        $heightAvatar = '';

        $avatar = str_replace( '/', '', $getInfo['avatar'] );
        if (file_exists($avatar)) {
            $getImageSize = getimagesize($avatar);
            $typeAvatar = strtoupper(substr($avatar, strrpos($avatar, '.') + 1));
            $widthAvatar = $getImageSize[0];
            $heightAvatar = $getImageSize[1];
            $sizeAvatar = (int)round(filesize($avatar) / 1024);
        }

        $typeBackground = '';
        $sizeBackground = '';
        $widthBackground = '';
        $heightBackground = '';

        $background = str_replace(  '/', '', $getInfo['detail_background'] );
        if (file_exists($background)) {
            $getImageSize = getimagesize($background);
            $typeBackground = strtoupper(substr($background, strrpos($background, '.') + 1));
            $widthBackground = $getImageSize[0];
            $heightBackground = $getImageSize[1];
            $sizeBackground = (int)round(filesize($background) / 1024);
        }

        return [
            'item' => $getInfo,
            'sizeAvatar' => $sizeAvatar,
            'widthAvatar' => $widthAvatar,
            'heightAvatar' => $heightAvatar,
            'sizeBackground' => $sizeBackground,
            'heightBackground' => $heightBackground,
            'widthBackground' => $widthBackground
        ];
    }

    /**
     * Chỉnh sửa cấu hình thông báo
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function update($input)
    {
        try {
            if (isset($input['type_bonus'])) {
                $input['money'] = str_replace(',', '', $input["money"]);
                if ($input['type_bonus'] == 'voucher' && $input['voucher'] == null) {
                    return response()->json([
                        'error' => true,
                        'message' => __('Yêu cầu chọn dịch vụ thưởng')
                    ]);
                }

                if ($input['type_bonus'] == 'money' && $input['money'] < 0 || $input['money'] >= 1000000000000000) {
                    return response()->json([
                        'error' => true,
                        'message' => __('Tiền mặt phải trong khoảng 0 -> 999,999,999,999,999')
                    ]);
                }
            }
            DB::beginTransaction();

            $mTemplateAuto = new NotificationTemplateAutoTable();

            $dataConfig = [
                'send_type' => $input['send_type'],
                'schedule_unit' => $input['schedule_unit'],
                'value' => $input['value'],
                'updated_by' => Auth()->id()
            ];
            //Update config notification
            $this->config->edit($dataConfig, $input['key']);

            $dataTemplateAuto = [
                'title_vi' => $input['title_vi'],
                'title_en' => $input['title_en'],
                'message_vi' => $input['message_vi'],
                'message_en' => $input['message_en'],
                'detail_content_vi' => $input['detail_content_vi'],
                'detail_content_en' => $input['detail_content_en'],
                'type_bonus' => isset($input['type_bonus']) ? $input['type_bonus'] : null,
                'service_id' => isset($input['type_bonus']) && $input['type_bonus'] == 'voucher' ? $input['voucher'] : null,
                'money' => isset($input['type_bonus']) && $input['type_bonus'] == 'money' ? $input['money'] : null,
            ];
            if ($input['avatar'] != null) {
                $dataTemplateAuto['avatar'] =  url('/').'/' . $this->moveImage($input['avatar'], NOTIFICATION_PATH);
            } else {
                $dataTemplateAuto['avatar'] = $input['avatar_old'];
            }
            if ($input['detail_background'] != null) {
                $dataTemplateAuto['detail_background'] =  url('/').'/' . $this->moveImage($input['detail_background'], NOTIFICATION_PATH);
            } else {
                $dataTemplateAuto['detail_background'] = $input['detail_background_old'];
            }
            //Update notification template auto
            $mTemplateAuto->edit($dataTemplateAuto, $input['key']);

            DB::commit();
            return response()->json([
                'error' => false,
                'message' => __('Chỉnh sửa cấu hình thông báo thành công')
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => __('Chỉnh sửa cấu hình thông báo thất bại')
            ]);
        }
    }

    /**
     * Thay đổi trạng thái
     *
     * @param $input
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function changeStatus($input)
    {
        try {
            if ($input['is_active'] == 1) {
                $input['updated_by'] = Auth::id();
            }

            $this->config->edit($input, $input['key']);

            return response()->json([
                'error' => false,
                'message' => __('Thay đổi trạng thái thành công')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('Thay đổi trạng thái thất bại')
            ]);
        }
    }

    /**
     * Upload hình ảnh
     *
     * @param $input
     * @return mixed|string
     */
    public function uploadImage($input)
    {
        $upload = $this->uploadImageTemp($input['file'], $input['link']);

        return $upload;
    }

    /**
     * Lưu ảnh vào folder temp
     *
     * @param $file
     * @param $link
     * @return string
     */
    private function uploadImageTemp($file, $link)
    {
        $time = Carbon::now();
        $file_name = rand(0, 9) . time() .
            date_format($time, 'd') .
            date_format($time, 'm') .
            date_format($time, 'Y') . $link . $file->getClientOriginalExtension();

        $file->move(TEMP_PATH, $file_name);

        return [
            'success' => 1,
            'file' => $file_name
        ];
    }

    /**
     * Move ảnh từ folder temp sang folder chính
     *
     * @param $filename
     * @param $PATH
     * @return mixed|string
     */
    public function moveImage($filename, $PATH)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = $PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory($PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }


}