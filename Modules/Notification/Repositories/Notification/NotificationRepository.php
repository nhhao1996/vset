<?php


namespace Modules\Notification\Repositories\Notification;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Models\CustomerGroupTable;
use Modules\Admin\Models\ServiceSerialTable;
use Modules\Admin\Models\WalletTable;
use Modules\Notification\Models\CustomerGroupFilterTable;
use Modules\Notification\Models\NotificationCustomerGroupTable;
use Modules\Notification\Models\NotificationRewardMapTable;
use Modules\Notification\Models\NotificationRewardTable;
use Modules\Notification\Models\NotificationTypeTable;
use Modules\Notification\Models\BrandTable;
use Modules\Notification\Models\FaqTable;
use Modules\Notification\Models\FaqGroupTable;
use Modules\Notification\Models\NotificationTemplateTable;
use Modules\Notification\Models\NotificationDetailTable;
use Modules\Notification\Models\NotificationQueueTable;
use Modules\Notification\Models\NotificationTable;
use Modules\Notification\Models\ProductChildTable;
use Modules\Notification\Models\UserTable;
use Modules\Notification\Models\MyStoreFilterGroupTable;
use Modules\Notification\Http\Api\PushNotification;
use Carbon\Carbon;
use Modules\Voucher\Models\ServiceCategoryTable;
use Modules\Voucher\Models\ServiceTable;
use Modules\Voucher\Models\ServiceVoucherTemplateTable;

class NotificationRepository implements NotificationRepositoryInterface
{
    /**
     * Khai báo biến
     */
    protected $notificationType;
    protected $notificationTemplate;
    protected $notificationDetail;
    protected $notificationQueue;
    protected $brand;
    protected $faq;
    protected $faqGroup;
    protected $user;
    protected $notification;
    protected $pushNotification;
    protected $filterGroup;
    protected $mCustomerGroupFilter;
    protected $serviceCategory;
    protected $service;
    protected $notiCustomerGroup;
    protected $notiReward;
    protected $notiRewardMap;
    protected $serviceVoucherTemplate;
    protected $wallet;
    protected $serviceSerial;

    public function __construct(
        UserTable $mUser,
        BrandTable $mBrand,
        FaqTable $mFaq,
        FaqGroupTable $mFaqGroup,
        NotificationTypeTable $mNotificationType,
        NotificationQueueTable $mNotificationQueue,
        NotificationDetailTable $mNotificationDetail,
        NotificationTemplateTable $mNotificationTemplate,
        NotificationTable $mNotification,
        PushNotification $apiPushNotification,
        MyStoreFilterGroupTable $mFilterGroup,
        CustomerGroupFilterTable $mCustomerGroupFilter,
        ServiceCategoryTable $serviceCategory,
        ServiceTable $service,
        NotificationCustomerGroupTable $notiCustomerGroup,
        NotificationRewardTable $notiReward,
        NotificationRewardMapTable $notiRewardMap,
        ServiceVoucherTemplateTable $serviceVoucherTemplate,
        WalletTable $wallet,
        ServiceSerialTable $serviceSerial
    )
    {
        $this->user = $mUser;
        $this->brand = $mBrand;
        $this->faq = $mFaq;
        $this->faqGroup = $mFaqGroup;
        $this->notificationType = $mNotificationType;
        $this->notificationQueue = $mNotificationQueue;
        $this->notificationDetail = $mNotificationDetail;
        $this->notificationTemplate = $mNotificationTemplate;
        $this->notification = $mNotification;
        $this->pushNotification = $apiPushNotification;
        $this->filterGroup = $mFilterGroup;
        $this->mCustomerGroupFilter = $mCustomerGroupFilter;
        $this->serviceCategory = $serviceCategory;
        $this->service = $service;
        $this->notiCustomerGroup = $notiCustomerGroup;
        $this->notiReward = $notiReward;
        $this->notiRewardMap = $notiRewardMap;
        $this->serviceVoucherTemplate = $serviceVoucherTemplate;
        $this->wallet = $wallet;
        $this->serviceSerial = $serviceSerial;
    }

    /**
     * Lấy dánh sách thông báo
     *
     * @param $filter
     * @return mixed
     */
    public function getNotiList($filter)
    {
        unset($filter['is_brand']);
        $notiList = $this->notificationTemplate->getList($filter);

        $arrListIdDetail = collect($notiList->items())->pluck('notification_detail_id');

        $arrCountNoti = $this->notification->getAllByDetailTemplate($arrListIdDetail);

        return [
            'noti_list' => $notiList,
            'noti_count' => $arrCountNoti
        ];
    }

    /**
     * Lấy danh sách notification type
     *
     * @return mixed
     */
    public function getNotificationTypeList($filter = [])
    {
        return $this->notificationType->getList($filter);
    }

    /**
     * Lấy danh sách chi tiết đích đến
     *
     * @param $data
     * @return mixed
     */
    public function getDetailEndPoint($data)
    {
//        isset($data['filter']) ? $filter = $data['filter'] : $filter = null;
        if ($data['detail_type'] == "product") {
            $mProductChild = new ProductChildTable();

            return \View::make('notification::notification.component.product', [
                'LIST' => $mProductChild->getList($data['filter'])
            ]);
        } elseif ($data['detail_type'] == "faq") {
            return [
                'faqList' => $this->faq->getListModal($data['filter']),
                'groupList' => $this->faqGroup->getListAll()
            ];
        }
    }

    /**
     * Lấy danh sách nhóm
     *
     * @param $data
     * @return array
     */
    public function getGroupList($data)
    {
        return $this->mCustomerGroupFilter->getListNew($data['filter']);
    }

    /**
     * Lưu thông báo
     *
     * @param $data
     * @return mixed|string
     */
    public function store($data)
    {

        try {
            DB::beginTransaction();
            if (!isset($data['from']) || count($data['from']) == 0) {
                return response()->json([
                    'error' => true,
                    'message' => __('Vui lòng chọn nhóm người nhận thông báo')
                ]);
            }
            if (isset($data['reward'])) {
                if ($data['reward'][0] == 'voucher') {
                    if (!isset($data['service_list']) || count($data['service_list']) == 0) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Yêu cầu chọn dịch vụ để tặng thưởng')
                        ]);
                    }
                } else if ($data['reward'][0] == 'money'){
                    if (!isset($data['value'])) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Yêu cầu nhập tiền thưởng')
                        ]);
                    }
                    if (str_replace(',', '', $data['value']) < 0 || str_replace(',', '', $data['value']) >= 1000000000000000) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Số tiền không đúng')
                        ]);
                    }
                }
            } else {
                return response()->json([
                    'error' => true,
                    'message' => __('Yêu cầu chọn loại tặng thưởng')
                ]);
            }


//            $client = new \GuzzleHttp\Client(['base_uri' => URL_NOTI]);
//            $data['lang'] = 'vi';
//            $data['background'] = url('/').'/' .$this->transferTempfileToAdminfile($data['background'], str_replace('', '', $data['background']));
//            $response = $client->request('POST',  '/notification/add', [
//                'json' => $data
//            ]);





            $from_type = 'group';
            if (($key = array_search('all', $data['from'])) !== false) {
                $from_type = 'all';
            }

            $notiTemplate = [
                'title_vi' => strip_tags($data['title_vi']),
                'title_en' => strip_tags($data['title_en']),
                'description_vi' => $data['description_vi'],
                'description_en' => $data['description_en'],
                'action_group' => 0,
                'from_type' => $from_type,
                'send_type' => 'immediately',
                'send_at' => Carbon::createFromFormat('d-m-Y H:i',$data['specific_time'])->format('Y-m-d H:i:s'),
//                'send_at' => Carbon::now(),
                'schedule_option' => 'specific',
                'send_status' => 'pending',
                'is_actived' => 1,
            ];

            $notificationTemplate = new NotificationTemplateTable();
            $notiCustomerGroup = new NotificationCustomerGroupTable();
            $notiReward = new NotificationRewardTable();
            $notificationDetail = new NotificationDetailTable();
            $user = new UserTable();
            $serviceVoucherTemplate = new ServiceVoucherTemplateTable();
            $wallet = new WalletTable();
            $serviceSerial = new ServiceSerialTable();
            $notiRewardMap = new NotificationRewardMapTable();


            $createNotiTemplate = $notificationTemplate->createNotiTemplate($notiTemplate);

            if ($from_type == 'group') {
                $arrMap = [];
                foreach ($data['from'] as $item) {
                    $arrMap[] = [
                        'member_level_id' => $item,
                        'notification_template_id' => $createNotiTemplate
                    ];
                }
                $notiCustomerGroup->insertGroup($arrMap);
            }

            $reward = null;
            if (isset($data['reward'])){
                $voucher = 0;
                foreach ($data['reward'] as $item) {
                    $reward = $item;
                    $arrReward = [];
                    $arrRewardMap = [];
                    if ($item == 'money') {
                        $arrReward = [
                            'notification_template_id' => $createNotiTemplate,
                            'key_reward' => $item,
                            'value' => isset($data['value']) ? str_replace(',', '', $data['value']) : 0
                        ];
                        $addReward = $notiReward->insertReward($arrReward);
                    } else {
                        $arrReward = [
                            'notification_template_id' => $createNotiTemplate,
                            'key_reward' => $item,
                            'value' => ''
                        ];
                        $addReward = $notiReward->insertReward($arrReward);
                        $dataService = session()->get('service_list');
                        if (count($dataService) != 0) {
                            foreach ($dataService as $value) {
                                $arrRewardMap[] = [
                                    'notification_reward_id' => $addReward,
                                    'service_id' => $value
                                ];
                            }
                            $this->notiRewardMap->insertRewardMap($arrRewardMap);
                        }
                    }
                }
            }

            if (isset($data['background']) && $data['background'] != '') {

                $old_path = TEMP_PATH . '/' . $data['background'];
                $new_path = NOTIFICATION_PATH . date('Ymd') . '/' . $data['background'];
                Storage::disk('public')->makeDirectory(NOTIFICATION_PATH . date('Ymd'));
                Storage::disk('public')->move($old_path, $new_path);

                $data['background'] = url('/').'/' .$new_path;
            }


//            foreach ($data['from'] as $item) {
//                if ($item == 'all') {
//                    $listUser = $user->getListAll();
//
//                } else {
//                    $listUser = $user->getListAll($item);
//                }
//            }

            $notiDetail = [
                'background' => isset($data['background']) && $data['background'] != '' ? $data['background'] : '',
                'content_vi' => $data['description_vi'],
                'content_en' => $data['description_en']
            ];

            $detailNoti = $notificationDetail->createNotiDetailGetId($notiDetail);
            $updateNotiTemplate = $notificationTemplate->updateNoti($createNotiTemplate,$detailNoti);

            $bonusMoney = [];
            $bonusVoucher = [];
//            if ($reward != null) {
//                foreach ($data['from'] as $item) {
//                    if ($item == 'all') {
//                        $listUser = $user->getListAll();
//
//                    } else {
//                        $listUser = $user->getListAll($item);
//                    }
//
//                    foreach ($listUser as $itemUser) {
//                        if ($reward == 'money'){
//                            $bonusMoney[] = [
//                                'customer_id' => $itemUser['customer_id'],
//                                'type' => 'bonus',
//                                'total_money' => strip_tags(str_replace(',', '', $data['value'])),
//                                'created_at' => Carbon::now(),
//                                'created_by' => $itemUser['customer_id']
//                            ];
//                        }
//
//                        if ($reward == 'voucher'){
//                            if (isset($data['service_list'])) {
//                                $listService = $serviceVoucherTemplate->getListService($data['service_list']);
//                                foreach ($listService as $itemService) {
//
//                                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//                                    $charactersLength = strlen($characters);
//                                    $randomString = '';
//                                    for ($i = 0; $i < 10; $i++) {
//                                        $randomString .= $characters[rand(0, $charactersLength - 1)];
//                                    }
//
//
//                                    $bonusVoucher[] = [
//                                        'service_id' => $itemService['service_id'],
//                                        'service_voucher_template' => $itemService['service_voucher_template_id'],
//                                        'customer_id' => $itemUser['customer_id'],
//                                        'type' => 'gift',
//                                        'serial' => $randomString,
//                                        'service_serial_status' => 'new',
//                                        'is_actived' => 1,
//                                        'created_at' => Carbon::now(),
//                                        'updated_at' => Carbon::now(),
//                                        'actived_at' => Carbon::now(),
//                                        'created_by' => Auth::id(),
//                                        'updated_by' => Auth::id(),
//                                    ];
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//
//            if (count($bonusMoney) != 0) {
//                $wallet->createMultipleWallet($bonusMoney);
//            }
//
//            if (count($bonusVoucher) != 0) {
//                $serviceSerial->addServiceSerial($bonusVoucher);
//            }
            DB::commit();

            return response()->json([
                'error' => false,
                'message' => __('Tạo thông báo thành công')
            ]);


        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => __('Tạo thông báo thất bại')
            ]);
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //Chuyển file từ folder temp sang folder chính
    private function transferTempfileToAdminfile($filename)
    {
        $old_path = TEMP_PATH . '/' . $filename;
        $new_path = NOTIFICATION_PATH . date('Ymd') . '/' . $filename;
        Storage::disk('public')->makeDirectory(NOTIFICATION_PATH . date('Ymd'));
        Storage::disk('public')->move($old_path, $new_path);
        return $new_path;
    }

    /**
     * Lấy chi tiết thông báo
     *
     * @param $id
     * @return mixed
     */
    public function getNotiById($id)
    {
        return $this->notificationDetail->getOne($id);
    }

    /**
     * Cập nhật thông báo
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        try {

            DB::beginTransaction();
            if (!isset($data['from']) || count($data['from']) == 0) {
                return response()->json([
                    'error' => true,
                    'message' => __('Vui lòng chọn nhóm người nhận thông báo')
                ]);
            }
            if (isset($data['reward'])) {
                if ($data['reward'][0] == 'voucher') {
                    if (!isset($data['service_list']) || count($data['service_list']) == 0) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Yêu cầu chọn dịch vụ để tặng thưởng')
                        ]);
                    }
                } else if ($data['reward'][0] == 'money'){
                    if (!isset($data['value'])) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Yêu cầu nhập tiền thưởng')
                        ]);
                    }
                    if (str_replace(',', '', $data['value']) < 0 || str_replace(',', '', $data['value']) >= 1000000000000000) {
                        return response()->json([
                            'error' => true,
                            'message' => __('Số tiền không đúng')
                        ]);
                    }
                }
            } else {
                return response()->json([
                    'error' => true,
                    'message' => __('Yêu cầu chọn loại tặng thưởng')
                ]);
            }


//            $client = new \GuzzleHttp\Client(['base_uri' => URL_NOTI]);
//            $data['lang'] = 'vi';
//            $data['background'] = url('/').'/' .$this->transferTempfileToAdminfile($data['background'], str_replace('', '', $data['background']));
//            $response = $client->request('POST',  '/notification/add', [
//                'json' => $data
//            ]);





            $from_type = 'group';
            if (($key = array_search('all', $data['from'])) !== false) {
                $from_type = 'all';
            }
            $notiTemplate = [
                'title_vi' => strip_tags($data['title_vi']),
                'title_en' => strip_tags($data['title_en']),
                'description_vi' => $data['description_vi'],
                'description_en' => $data['description_en'],
                'action_group' => 0,
                'from_type' => $from_type,
                'send_type' => 'immediately',
                'send_at' => Carbon::createFromFormat('d-m-Y H:i',$data['specific_time'])->format('Y-m-d H:i:s'),
//                'send_at' => Carbon::now(),
                'schedule_option' => 'specific',
                'send_status' => 'pending',
                'is_actived' => 1,
            ];
            $notificationTemplate = new NotificationTemplateTable();
            $notiCustomerGroup = new NotificationCustomerGroupTable();
            $notiReward = new NotificationRewardTable();
            $notificationDetail = new NotificationDetailTable();
            $user = new UserTable();
            $serviceVoucherTemplate = new ServiceVoucherTemplateTable();
            $wallet = new WalletTable();
            $serviceSerial = new ServiceSerialTable();
            $notiRewardMap = new NotificationRewardMapTable();


            $createNotiTemplate = $notificationTemplate->updateNotiTemplate($id,$notiTemplate);

//            if ($from_type == 'group') {
//                $arrMap = [];
//                foreach ($data['from'] as $item) {
//                    $arrMap[] = [
//                        'member_level_id' => $item,
//                        'notification_template_id' => $createNotiTemplate
//                    ];
//                }
//                $notiCustomerGroup->insertGroup($arrMap);
//            }

            $reward = null;
            if (isset($data['reward'])){
                $voucher = 0;
                foreach ($data['reward'] as $item) {
                    $reward = $item;
                    $arrReward = [];
                    $arrRewardMap = [];
                    $notiReward->deleteReward($id);
                    if ($item == 'money') {
                        $arrReward = [
                            'notification_template_id' => $id,
                            'key_reward' => $item,
                            'value' => isset($data['value']) ? str_replace(',', '', $data['value']) : 0
                        ];
                        $addReward = $notiReward->insertReward($arrReward);
                    } else {
                        $arrReward = [
                            'notification_template_id' => $id,
                            'key_reward' => $item,
                            'value' => ''
                        ];
                        $addReward = $notiReward->insertReward($arrReward);
                        $dataService = session()->get('service_list');
                        if (count($dataService) != 0) {
                            foreach ($dataService as $value) {
                                $arrRewardMap[] = [
                                    'notification_reward_id' => $addReward,
                                    'service_id' => $value
                                ];
                            }
                            $this->notiRewardMap->insertRewardMap($arrRewardMap);
                        }
                    }
                }
            }


            if ($data['background'] == $data['background_old']){
                $data['background'] = $data['background_old'];
            } else {
                $old_path = TEMP_PATH . '/' . $data['background'];
                $new_path = NOTIFICATION_PATH . date('Ymd') . '/' . $data['background'];
                Storage::disk('public')->makeDirectory(NOTIFICATION_PATH . date('Ymd'));
                Storage::disk('public')->move($old_path, $new_path);

                $data['background'] = url('/').'/' .$new_path;
            }
            $notiDetail = [
                'background' => isset($data['background']) && $data['background'] != '' ? $data['background'] : '',
                'content_vi' => $data['description_vi'],
                'content_en' => $data['description_en']
            ];

            $detailNoti = $notificationDetail->createNotiDetailGetId($notiDetail);
            $updateNotiTemplate = $notificationTemplate->updateNoti($id,$detailNoti);

            $bonusMoney = [];
            $bonusVoucher = [];
            DB::commit();


            return response()->json([
                'error' => false,
                'message' => __('Chỉnh sửa thông báo thành công')
            ]);


        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => __('Chỉnh sửa thông báo thất bại')
            ]);
        }
    }

    /**
     * Xóa theo detail id
     *
     * @param $id
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $getDetail = $this->notificationTemplate->getDetail($id);
            $this->notificationTemplate->deleteTemplate($id);
            $this->notificationDetail->deleteNotiDetail($getDetail['notification_detail_id']);
            $this->notiCustomerGroup->deleteGroup($id);
            $getRewart = $this->notiReward->getListByTemplateByTemplateId($id);
            $this->notiReward->deleteReward($id);
            $this->notiRewardMap->deleteRewardMap($getRewart['notification_reward_id']);

            DB::commit();

            return [
                'error' => false,
                'message' => 'Xóa thông báo thành công'
            ];
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
            return [
                'error' => true,
                'message' => 'Xóa thông báo thất bại'
            ];
        }
    }

    public function getListCategoryService()
    {
        return $this->serviceCategory->getOption();
    }

    public function getListService(array $filter = [])
    {
        if (session()->has('service_list')){
            $filter['service_list'] = session()->get('service_list');
        }
        $list = $this->service->getListService($filter);
        $view = view('notification::notification.popup.table',[
            'list' => $list,
            'filter' => $filter
        ])->render();
        return [
            'view' => $view
        ];
    }

    public function addListService($data)
    {
        if (isset($data['listService'])) {
            if (session()->has('service_list')){
                $dataService = session()->get('service_list');
                $dataService = array_merge($dataService,$data['listService']);
                session()->put('service_list',$dataService);
                $list = $this->service->getListServiceSession($dataService);
                $view = view('notification::notification.popup.add-table',['list' => $list])->render();
                return [
                    'view' => $view,
                    'error' => false
                ];
            }
        }
        return [
            'error' => true
        ];
    }

    public function removeService($service_id)
    {
        if (session()->has('service_list')){
            $dataService = session()->get('service_list');
            if (($key = array_search($service_id['service_id'], $dataService)) !== false) {
                unset($dataService[$key]);
                session()->put('service_list',$dataService);
                $list = $this->service->getListServiceSession($dataService);
                $view = view('notification::notification.popup.add-table',['list' => $list])->render();
                return [
                    'view' => $view,
                    'error' => false
                ];
            }
        }
        return [
            'error' => true
        ];
    }

    public function getDetail($id)
    {
        $detail = $this->notificationTemplate->getDetail($id);
        $customerGroup = $this->notiCustomerGroup->getListByTemplateId($id);
        if (count($customerGroup) != 0) {
            $customerGroup = collect($customerGroup)->keyBy('member_level_id');
        }
        $reward = $this->notiReward->getListByTemplateId($id);
        $rewardMap = [];
        if (count($reward) != 0) {
            $reward = collect($reward)->keyBy('key_reward');
            foreach ($reward as $item) {
                if ($item['key_reward'] == 'voucher'){
                    $rewardMap = $this->notiRewardMap->getListByRewardId($item['notification_reward_id']);
                }
            }
        }
        return [
            'detail' => $detail,
            'customerGroup' => $customerGroup,
            'reward' => $reward,
            'rewardMap' => $rewardMap,
        ];
    }

    public function getAllService()
    {
        return $this->service->getAllList();
    }

}
