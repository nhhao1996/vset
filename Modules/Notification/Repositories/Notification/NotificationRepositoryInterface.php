<?php

namespace Modules\Notification\Repositories\Notification;

interface NotificationRepositoryInterface
{
    /**
     * Lấy danh sách notification type
     *
     * @return mixed
     */
    public function getNotificationTypeList($filter = []);

    /**
     * Lấy danh sách chi tiết đích đến
     *
     * @param $data
     * @return mixed
     */
    public function getDetailEndPoint($data);

    /**
     * Lấy danh sách nhóm
     *
     * @param $data
     */
    public function getGroupList($data);

    /**
     * Lưu thông báo
     *
     * @param $data
     */
    public function store($data);

    /**
     * Lấy chi tiết thông báo
     *
     * @param $id
     * @return mixed
     */
    public function getNotiById($id);

    /**
     * Cập nhật thông báo
     *
     * @param $id
     * @param $data
     */
    public function update($id, $data);

    /**
     * Lấy dánh sách thông báo
     *
     * @param $filter
     * @return mixed
     */
    public function getNotiList($filter);

    /**
     * Xóa theo detail id
     *
     * @param $id
     */
    public function destroy($id);

//    Lấy danh sách danh mục dịch vụ
    public function getListCategoryService();

    /**
     * Lấy danh sách dịch vụ
     * @param array $filter
     * @return mixed
     */
    public function getListService(array $filter=[]);

    /**
     * Thêm danh sách dịch vụ vào session
     * @param $data
     * @return mixed
     */
    public function addListService($data);

    /**
     * Xóa dịch vụ
     * @return mixed
     */
    public function removeService($service_id);

//    Lấy chi tiết Thông báo template
    public function getDetail($id);

    /**
     * Lấy danh sách dịch vụ
     * @return mixed
     */
    public function getAllService();
}
