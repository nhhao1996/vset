<?php


namespace Modules\Notification\Http\Controllers;


use Illuminate\Http\Request;
use Modules\Notification\Http\Requests\Config\UpdateRequest;
use Modules\Notification\Repositories\Config\ConfigRepoInterface;
use Modules\Notification\Repositories\Notification\NotificationRepositoryInterface;

class ConfigController extends Controller
{
    protected $config;
    protected $notification;

    public function __construct(
        ConfigRepoInterface $config,
        NotificationRepositoryInterface $iNotification
    ) {
        $this->config = $config;
        $this->notification = $iNotification;
    }

    /**
     * View cấu hình thông báo
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        $data = $this->config->dataIndex();
        return view('notification::config.index', $data);
    }

    /**
     * View chỉnh sửa cấu hình thông báo
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit(Request $request)
    {
        $data = $this->config->getInfo($request->key);
        $data['listCategoryService'] = $this->notification->getAllService();
        return view("notification::config.edit", $data);
    }

    /**
     * Chỉnh sửa cấu hình thông báo
     *
     * @param UpdateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request)
    {
        $data = $this->config->update($request->all());

        return $data;
    }

    /**
     * Thay đổi trạng thái
     *
     * @param Request $request
     * @return mixed
     */
    public function changeStatusAction(Request $request)
    {
        $data = $this->config->changeStatus($request->all());

        return $data;
    }

    /**
     * Upload hình ảnh
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAction(Request $request)
    {
        if ($request->file('file') != null) {
            $data = $this->config->uploadImage($request->all());

            return response()->json($data);
        }
    }
}