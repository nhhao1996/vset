<?php

namespace Modules\Notification\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Modules\Notification\Repositories\Notification\NotificationRepositoryInterface;
use Illuminate\Http\Response;
use Modules\Notification\Requests\Notification\StoreRequest;
use Modules\Notification\Requests\Notification\UpdateRequest;

//use function React\Promise\reject;

class NotificationController extends Controller
{
    /**
     * Khai báo biến
     *
     */
    protected $uploadManager;
    protected $notification;

    public function __construct(
        NotificationRepositoryInterface $iNotification
    )
    {
        $this->notification = $iNotification;
    }

    /**
     * Show danh sách thông báo
     * @param Request $request
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $filter['is_brand'] = 0;
        $list = $this->notification->getNotiList($filter);

        return view('notification::notification.index', [
            'FILTER' => $this->filters(),
            'LIST' => $list['noti_list'],
            'notiCount' => $list['noti_count'],
        ]);
    }

    // FUNCTION  FILTER LIST ITEM
    protected function filters()
    {
        return [
            'send_status' => [
                'data' => [
                    '' => 'Trạng thái gửi thông báo',
                    'pending' => 'Đang chờ gửi',
                    'sent' => 'Đã gửi'
                ]
            ]
        ];
    }

    /**
     * Danh sách thông báo
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function listAction(Request $request)
    {
        $filters = $request->only(['page', 'display', 'search_keyword', 'is_actived','send_status']);

        $list = $this->notification->getNotiList($filters);

        return view('notification::notification.list', [
                'LIST' => $list['noti_list'],
                'FILTER' => $filters,
                'notiCount' => $list['noti_count'],
            ]
        );
    }

    /**
     * Giao diện trang thêm
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        session()->forget('service_list');
        session()->put('service_list',[]);
        $notiTypeList = $this->notification->getNotificationTypeList();
        $listCategoryService = $this->notification->getListCategoryService();
        return view('notification::notification.create', [
            'notiTypeList' => $notiTypeList,
            'listCategoryService' => $listCategoryService
        ]);
    }

    /**
     * Lưu thông báo
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $data['service_list'] = $request->session()->get('service_list');

            $result = $this->notification->store($data);
            return $result;
//            return response()->json([
//                'error' => false,
//                'message' => __('admin::validation.notification.create_success')
//            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('admin::validation.notification.create_fail')
            ]);
        }
    }

    /**
     * Lấy chi tiết đích đến theo detail_type
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detailEndPoint(Request $request)
    {
        $data = $request->all();
        $data['filter']['page'] = (isset($_GET['page'])) ? $_GET['page'] : 1;
        $viewHtml = $this->notification->getDetailEndPoint($data);
        $trans = trans('admin::notification');

        if ($data['detail_type'] == "product") {
            if ($data['view'] == "modal") {
                return view('notification::notification.component.modal-end-point', [
                    'trans' => $trans,
                    'html' => $viewHtml,
                ]);
            }
        } elseif ($data['detail_type'] == "faq") {
//            if ($data['view'] == "modal") {
//                return view('notification::notification.component.faq-modal', [
//                    'trans' => $trans,
//                    'groupList' => $detailList['groupList']
//                ]);
//            } else {
//                return view('notification::notification.component.ajax-faq', [
//                    'trans' => $trans,
//                    'detailListPaging' => $detailList['faqList']['paging']
//                ]);
//            }
        }
    }

    /**
     * Lấy danh sách nhóm, phân trang
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function groupList(Request $request)
    {
        $data = $request->all();
        $trans = trans('admin::notification');

        if ($data['view'] == "modal") {
            return view('notification::notification.component.group-modal', [
                'trans' => $trans
            ]);
        } else {
            $data['filter']['page'] = (isset($_GET['page'])) ? $_GET['page'] : 1;
            $data['filter']['perpage'] = 1000;
            $list = $this->notification->getGroupList($data);

            return view('notification::notification.component.ajax-group', [
                'trans' => $trans,
//                    'detailList' => $detailList['non_paging'],
                'groupList' => $list
            ]);
        }
    }

    /**
     * Trang chi tiết
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        session()->forget('service_list');
        session()->put('service_list',[]);
        $notiTypeList = $this->notification->getNotificationTypeList();
        $listCategoryService = $this->notification->getListCategoryService();
        $detail = $this->notification->getDetail($id);
        return view('notification::notification.detail', [
            'notiTypeList' => $notiTypeList,
            'listCategoryService' => $listCategoryService,
            'detail' => $detail['detail'],
            'customerGroup' => $detail['customerGroup'],
            'reward' => $detail['reward'],
            'rewardMap' => $detail['rewardMap'],
        ]);
    }

    /**
     * Show giao diện chỉnh sửa thông báo
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        session()->forget('service_list');
        session()->put('service_list',[]);
        $notiTypeList = $this->notification->getNotificationTypeList();
        $listCategoryService = $this->notification->getListCategoryService();
        $detail = $this->notification->getDetail($id);
        if (isset($detail['rewardMap']) && count($detail['rewardMap']) != 0) {
            $service = collect($detail['rewardMap'])->pluck('service_id')->toArray();
            session()->put('service_list',$service);
        }
        return view('notification::notification.edit', [
            'notiTypeList' => $notiTypeList,
            'listCategoryService' => $listCategoryService,
            'detail' => $detail['detail'],
            'customerGroup' => $detail['customerGroup'],
            'reward' => $detail['reward'],
            'rewardMap' => $detail['rewardMap'],
        ]);
    }

    /**
     * Cập nhật thông báo
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        try {
            $data = $request->all();
            $data['service_list'] = $request->session()->get('service_list');
            $result = $this->notification->update($id,$data);
            return $result;
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => __('Cập nhật thông báo tặng thưởng thất bại')
            ]);
        }
    }

    public function updateIsActived($id, Request $request)
    {
        $data = $request->all();
        $data['is_brand'] = 0;
        $result = $this->notification->updateIsActived($id, $data);

        if ($result['status'] == false) {
            if ($result['message'] == 'sent') {
                return response()->json(["success" => 0, "message" => __('admin::validation.notification.sent')]);
            } else {
                return response()
                    ->json(["success" => 0, "message" => __('admin::validation.notification.over_time')]);
            }
        } else {
            return response()->json(["success" => 1, "send_at" => $result['sendAt']]);
        }
    }

    /**
     * Kiểm tra có file hay không sau đó gọi hàm upload lên server
     *
     * @param Request $request
     * @return Response
     */
    public function upload(Request $request)
    {
        if ($request->file('file') != null) {
            $file = $this->uploadImageTemp($request->file('file'));

            return response()->json(["file" => $file, "success" => "1"]);
        }
    }

    /**
     * Lưu file lên server
     *
     * @param $file
     * @return string
     */
    private function uploadImageTemp($file)
    {
        $result = $this->uploadManager->doUpload($file);

        return $result['public_path'];
    }

    public function destroy($id)
    {
        $result = $this->notification->destroy($id);
        return $result;
    }

    public function getListService(Request $request) {
        $param = $request->all();
        $list = $this->notification->getListService($param);
        return \response()->json($list);
    }

    public function addListService(Request $request)
    {
        $param = $request->all();
        $addList = $this->notification->addListService($param);
        return \response()->json($addList);
    }

    public function removeService(Request $request)
    {
        $param = $request->all();
        $removeService = $this->notification->removeService($param);
        return \response()->json($removeService);
    }
}
