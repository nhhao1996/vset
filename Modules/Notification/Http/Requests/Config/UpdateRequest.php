<?php
/**
 * Created by PhpStorm
 * User: Mr Son
 * Date: 07-04-02020
 * Time: 10:09 AM
 */

namespace Modules\Notification\Http\Requests\Config;


use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_type' => 'required',
            'title_vi' => 'required|max:250',
            'title_en' => 'required|max:250',
            'message_vi' => 'required|max:1000',
            'message_en' => 'required|max:1000',
            'detail_content_vi' => 'required',
            'detail_content_en' => 'required',
        ];
    }

    /*
     * function custom messages
     */
    public function messages()
    {
        return [
            'send_type.required' => __('Hãy chọn loại gửi'),
            'title_vi.required' => __('Hãy nhập tiêu đề'),
            'title_vi.max' => __('Tiêu đề tối đa 250 kí tự'),
            'message_vi.required' => __('Hãy nhập nội dung'),
            'message_vi.max' => __('Nội dung tối đa 1000 kí tự'),
            'detail_content_vi.required' => __('Hãy chọn loại gửi'),
            'title_en.required' => __('Hãy nhập tiêu đề'),
            'title_en.max' => __('Tiêu đề tối đa 250 kí tự'),
            'message_en.required' => __('Hãy nhập nội dung'),
            'message_en.max' => __('Nội dung tối đa 1000 kí tự'),
            'detail_content_en.required' => __('Hãy chọn loại gửi')
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'title' => 'strip_tags|trim',
            'message' => 'strip_tags|trim'
        ];
    }
}