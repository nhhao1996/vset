<?php

namespace Modules\Dashbroad\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Modules\Dashbroad\Repositories\DashbroadRepositoryInterface;

class DashbroadController extends Controller
{

    protected $dashbroad;

    public function __construct(DashbroadRepositoryInterface $dashbroad)
    {
        $this->dashbroad = $dashbroad;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $status = 'new';

        $orders = $this->dashbroad->getOrders($status);

        $appointment = $this->dashbroad->getAppointment($status);
        $totalcustomer = $this->dashbroad->getTotalCustomer();
        $totalcustomerOnMonth = $this->dashbroad->getTotalCustomerOnMonth();

        return view('dashbroad::index.index', [
            'orders' => $orders,
            'appointment' => $appointment,
            'totalcustomer' => $totalcustomer,
            'totalcustomerOnMonth' => $totalcustomerOnMonth
        ]);
    }

    public function getListOrder(Request $request)
    {
        $param = $request->all();

        $filter['pagination'] = $param['pagination'];
        $filter['search'] = $param['query']['generalSearch'];

        $orders = $this->dashbroad->listOrder($filter);

        return response()->json($orders);
    }

    public function getListAppointment(Request $request)
    {

        $param = $request->all();

        $filter['pagination'] = $param['pagination'];
        $filter['search'] = $param['query']['search'];

        $appointment = $this->dashbroad->listAppointment($filter);

        return response()->json($appointment);
    }


    public function getListBirthday(Request $request)
    {

        $param = $request->all();

        $filter['pagination'] = $param['pagination'];
        $filter['search'] = $param['query']['search_customer'];

        $appointment = $this->dashbroad->listBirthday($filter);
//        dd($appointment);
        return response()->json($appointment);
    }

    public function getAppointmentByDate()
    {

        $column = [];
        for ($i = 1; $i <= 7; $i++) {
            $day = Carbon::now()->addDay($i);

            $column[] = [

                'date' => $day->format('d/m'),
                'appointment' => $this->dashbroad->getAppointmentByDate($day->format('Y-m-d'))

            ];
        }

        return response()->json($column);
    }


    public function getOrderByMonthYear(Request $request)
    {

        $param = $request->all();
        $year = Carbon::now()->year;

        $column = [];

        if (isset($param['year']) != '') {
            $year = $param['year'];
        }

        if (isset($param['month']) != '') {
            $month = $param['month'];
            $days = Carbon::createFromDate($year, $month);
            for ($i = 1; $i <= $days->daysInMonth; $i++) {
                $column[] = [
                    'month' => $i . '/' . $month,
                    'order' => $this->dashbroad->getOrderbyDateMonth($i, $month, $year)
                ];
            }

        } else {
            for ($i = 1; $i <= 12; $i++) {
                $day = Carbon::now()->month($i);

                $column[] = [
                    'month' => $day->format('m'),
                    'order' => $this->dashbroad->getOrderbyMonthYear($i, $year)
                ];
            }
        }

        return response()->json($column);
    }

    public function getOrderByObjectType(Request $request)
    {

        $param = $request->all();

        $date = Carbon::now()->format('Y-m-d');

        $fromDate = $date . ' 00:00:00';
        $todate = $date . ' 23:59:59';

        if (count($param) > 0) {
            $fromDate = $param['formDate'] . ' 00:00:00';
            $todate = $param['toDate'] . ' 23:59:59';
        }

        $arrDate = [$fromDate, $todate];

        $service_card = $this->dashbroad->getOrderByObjectType('service_card', $arrDate);
        $services = $this->dashbroad->getOrderByObjectType('service', $arrDate);
        $products = $this->dashbroad->getOrderByObjectType('product', $arrDate);
        $member_card = $this->dashbroad->getOrderByObjectType('member_card', $arrDate);

        $arrData = array_merge($service_card, $services, $products, $member_card);

        return response()->json($arrData);

    }


    public function getTopService(Request $request)
    {

        $param = $request->all();

        $date = Carbon::now()->format('Y-m-d');

        $fromDate = $date . ' 00:00:00';
        $todate = $date . ' 23:59:59';

        if (count($param) > 0) {
            $fromDate = $param['formDate'] . ' 00:00:00';
            $todate = $param['toDate'] . ' 23:59:59';
        }

        $arrDate = [$fromDate, $todate];

        $list = $this->dashbroad->getTopService($arrDate);

        return response()->json($list);

    }
}