<?php
/**
 * Created by PhpStorm.
 * User: tuanva
 * Date: 2019-03-26
 * Time: 09:27
 */

namespace Modules\Dashbroad\Repositories;


use Modules\Dashbroad\Models\AppointmentTable;
use Modules\Dashbroad\Models\CustomerTable;
use Modules\Dashbroad\Models\OrderTable;

class DashbroadRepository implements DashbroadRepositoryInterface
{

    protected $orders;
    protected $appointment;
    protected $customer;


    public function __construct(OrderTable $orders, AppointmentTable $appointment, CustomerTable $customer)
    {
        $this->orders = $orders;
        $this->appointment = $appointment;
        $this->customer = $customer;
    }


    public function getAppointmentByDate($date){

        return $this->appointment->appointmentByDate($date);
    }

    public function getOrders($status){

        return $this->orders->getOrders($status);
    }

    public function getAppointment($status){
        return $this->appointment->getAppointment($status);
    }

    public function getOrderbyMonthYear($month,$year){

        return $this->orders->orderByMonthYear($month,$year);
    }

    public function getOrderbyDateMonth($date,$month,$year){

        return $this->orders->orderByDateMonth($date,$month,$year);
    }

    public function listOrder($filter = []){

        return $this->orders->getDataTable($filter);
    }

    public function listAppointment($filter = []){

        return $this->appointment->getDataTable($filter);
    }

    public function listBirthday($filter = []){
        return $this->customer->getDataTable($filter);
    }

    public function getOrderByObjectType($type,$date){
        return $this->orders->getOrderByObjectType($type,$date);
    }

    public function getTopService($date){
        return $this->orders->getTopService($date);
    }

    public function getTotalCustomer(){
        return $this->customer->getTotal();
    }

    public function getTotalCustomerOnMonth(){
        return $this->customer->getTotalOnMonth();
    }
}