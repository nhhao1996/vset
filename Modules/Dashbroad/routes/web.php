<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'dashbroad', 'namespace' => 'Modules\Dashbroad\Http\Controllers'], function()
{

    Route::get('/', 'DashbroadController@index')->name('dashbroad');
    Route::post('/get-list-order', 'DashbroadController@getListOrder')->name('dashbroad.list-order');
    Route::post('/get-list-appointment', 'DashbroadController@getListAppointment')->name('dashbroad.list-appointment');
    Route::post('/get-list-birthday', 'DashbroadController@getListBirthday')->name('dashbroad.list-birthday');
    Route::get('/get-appointment-by-date', 'DashbroadController@getAppointmentByDate')->name('dashbroad.appointment-by-date');
    Route::post('/get-order-by-month-year', 'DashbroadController@getOrderByMonthYear')->name('dashbroad.order-by-month-year');
    Route::post('/get-order-by-object-type', 'DashbroadController@getOrderByObjectType')->name('dashbroad.order-by-object-type');
    Route::post('/get-top-service', 'DashbroadController@getTopService')->name('dashbroad.get-top-service');

});
