<div class="row" id="m--star-dashbroad">
    <div class="col-lg-3 col-xs-6">
        <div class="m-portlet m--bg-success m-portlet--bordered-semi m-portlet--full-height  m-portlet--head-sm tongtien">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                            {{__('Đơn hàng')}} <small>{{__('Trong ngày')}}</small>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget25">
                    <span class="m-widget25__price m--font-brand">{{$orders}}</span>
                    <span class="m-widget25__desc">{{__('Đơn hàng')}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="m-portlet m--bg-brand m-portlet--bordered-semi m-portlet--full-height  m-portlet--head-sm dathanhtoan">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                            {{__('Lịch hẹn')}} <small>{{__('Trong ngày')}}</small>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget25">
                    <span class="m-widget25__price m--font-brand">{{$appointment}}</span>
                    <span class="m-widget25__desc">{{__('Lịch hẹn')}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="m-portlet m--bg-danger m-portlet--bordered-semi m-portlet--full-height  m-portlet--head-sm chuathanhtoan">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                            {{__('Khách hàng')}}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget25">
                    <span class="m-widget25__price m--font-brand">{{$totalcustomer}}</span>
                    <span class="m-widget25__desc">{{__('Khách hàng')}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="m-portlet m--bg-warning m-portlet--bordered-semi m-portlet--full-height  m-portlet--head-sm sotienhuy">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text m--font-light">
                        {{__('Khách hàng')}} <small>{{__('Trong tháng')}}</small>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget25">
                    <span class="m-widget25__price m--font-brand">{{$totalcustomerOnMonth}}</span>
                    <span class="m-widget25__desc">{{__('Khách hàng')}}</span>
                </div>
            </div>
        </div>
    </div>
</div>