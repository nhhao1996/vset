@extends('layout')
@section('title_header')
    <a href="javascript:;" class="m-menu__link m-menu__toggle" title="">
        <i class="m-menu__link-icon flaticon-line-graph"></i><h3 class="m-menu__link-text">{{__('Trang chủ')}}</h3>
    </a>
@endsection
@section('content')
    <div id="m-dashbroad">
        @include('dashbroad::inc.stars')
        <div class="m-portlet m-portlet--head-sm m-portlet--tabs" id="m_portlet_tools_5">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_order" role="tab">
                                <i class="flaticon-bag"></i> {{__('Đơn hàng trong ngày')}}
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item" onclick="Appointments.tab_appointment()">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_appointment" role="tab">
                                <i class="flaticon-event-calendar-symbol"></i> {{__('Lịch hẹn trong ngày')}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_order" role="tabpanel">
                        @include('dashbroad::inc.list-order')
                    </div>
                    <div class="tab-pane" id="m_appointment" role="tabpanel">
                        @include('dashbroad::inc.list-appointments')
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="m-portlet m-portlet--head-sm">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-calendar-1"></i>
                            </span>
                                <h3 class="m-portlet__head-text">
                                    {{__('Lịch hẹn 7 ngày tiếp')}}
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="m_appointments" style="height: 350px;"></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="m-portlet m-portlet--head-sm">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-cart"></i>
                            </span>
                                <h3 class="m-portlet__head-text">
                                    {{__('Đơn hàng')}}
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">

                            <div class="form-group m-form__group m--margin-right-5">
                                <select name="month" class="form-control m-bootstrap-select m_selectpicker" data-width="100px" id="m_month">
                                    <option value="" title="{{__('Tất cả')}}">{{__('Tất cả')}}</option>
                                    @for($i=1;$i<=12;$i++)
                                        <option value="{{$i}}" title="@lang("Tháng $i")">
                                            @lang("Tháng $i")
                                        </option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form-group m-form__group">
                                <select name="year" class="form-control m-bootstrap-select m_selectpicker" data-width="100px" id="m_year">
                                    @for($i=0;$i<=4;$i++)
                                        <option value="{{\Carbon\Carbon::now()->subYear($i)->year}}">
                                            {{ \Carbon\Carbon::now()->subYear($i)->year}}
                                        </option>
                                    @endfor
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="m_orders" style="height: 350px;"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="m-portlet m-portlet--head-sm">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                        <i class="flaticon-notes"></i>
                    </span>
                                <h3 class="m-portlet__head-text">
                                    {{__('Thông tin bán hàng')}}
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="m-input-icon m-input-icon--right" id='m_daterangepicker_6'>
                                <input name="date" type="text" class="form-control m-input" placeholder="Chọn ngày" value="{{\Carbon\Carbon::now()->format('d/m/Y')}} - {{\Carbon\Carbon::now()->format('d/m/Y')}}">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
                            </div>

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="m_amcharts_8" style="height: 350px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="m-portlet m-portlet--head-sm">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                        <i class="flaticon-graphic-2"></i>
                    </span>
                                <h5 class="m-portlet__head-text">
                                    {{__('Top 10 dịch vụ')}}
                                </h5>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="m-input-icon m-input-icon--right m-input--sm" id='m_daterangepicker_7'>
                                <input name="date" type="text" class="form-control m-input" placeholder="Chọn ngày" value="{{\Carbon\Carbon::now()->format('d/m/Y')}} - {{\Carbon\Carbon::now()->format('d/m/Y')}}">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="m_amcharts_5" style="height: 350px;"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_4">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="flaticon-calendar"></i>
                    </span>
                        <h5 class="m-portlet__head-text">
                            {{__('Danh sách sinh nhật')}} <small class="birtday"> {{\Carbon\Carbon::now()->addDay(0)->format('d/m')}} - {{\Carbon\Carbon::now()->addDay(6)->format('d/m')}}</small>
                        </h5>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">
                @include('dashbroad::inc.list-birthday')
            </div>
        </div>
    </div>

@stop

@section('after_script')

    <script src="{{asset('static/backend/js/admin/service/autoNumeric.min.js?v='.time())}}"></script>
    <script>
        var decimal_number = {{isset(config()->get('config.decimal_number')->value) ? config()->get('config.decimal_number')->value : 0}};
    </script>
    <script type="text/javascript" src="{{asset('static/backend/js/dashbroad/orders.js?v='.time())}}"></script>
    <script type="text/javascript" src="{{asset('static/backend/js/dashbroad/appointment.js?v='.time())}}"></script>
    <script type="text/javascript" src="{{asset('static/backend/js/dashbroad/birthday.js?v='.time())}}"></script>
    <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
    <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>

    <script type="text/javascript" src="{{asset('static/backend/js/dashbroad/all.js')}}"></script>
    <style>
        .amcharts-chart-div > a {
            display: none !important;
        }
    </style>
    <script>
        $('#m_appointment').on('change',function (e) {
            // alert('dd')
        });
    </script>
@stop