<?php
define('PAGING_ITEM_PER_PAGE', 10);
define('LOGIN_HOME_PAGE', 'admin.buy-bonds-request');
define('TEMP_PATH', 'temp_upload');
define('SERVICE_CARD_PATH', 'uploads/admin/service_card/');
define('STORE_UPLOADS_PATH', 'uploads/admin/store/');
define('SERVICES_UPLOADS_PATH', 'uploads/services/services/');
define('STAFF_UPLOADS_PATH', 'uploads/admin/staff/');
define('CONTRACT_SERIAL_UPLOADS_PATH', 'uploads/admin/contract_serial/');
define('CUSTOMER_UPLOADS_PATH', 'uploads/admin/customer/');
define('STOCK_UPLOADS_PATH', 'uploads/admin/stock/');
define('DIVIDEND_UPLOADS_PATH', 'uploads/admin/dividend/');
define('SERVICE_UPLOADS_PATH', 'uploads/admin/service/');
define('BRANCH_UPLOADS_PATH', 'uploads/admin/branch/');
define('PRODUCT_UPLOADS_PATH', 'uploads/admin/product/');
define('SPA_INFO_UPLOADS_PATH', 'uploads/admin/spa-info/');
define('BANNER_UPLOADS_PATH', 'uploads/admin/banner/');
define('CONFIG_SERVICE_CARD', 'uploads/admin/config-print-service-card/');
define('SEND_EMAIL_CARD','uploads/admin/send-email-card/');
define('VOUCHER_PATH','uploads/admin/voucher/');
define('NOTIFICATION_PATH','uploads/notification/config/');
define('USER_CARRIER_PATH','uploads/delivery/user-carrier/');
define('NEW_PATH', 'uploads/admin/new/');
define('DELIVERY_HISTORY_PATH', 'uploads/admin/delivery-history/');
define('CONFIG_GENERAL_PATH', 'uploads/admin/config-general/');
define('PROMOTION_PATH','uploads/promotion/promotion/');
define('CUSTOMER_LEAD_PATH','uploads/customer-lead/customer-lead/');
define('VOUCHER_VSET_PATH','uploads/voucher/');

define('CONFIG_EMAIL_TEMPLATE', 'uploads/admin/config-email-template/');
define('UPLOAD_FILE_EXCEL', 'uploads/admin/excel/');


define('BASE_URL_API', env('BASE_URL_API'));
define('URL_NOTI', env('URL_NOTI'));
define('CODE_SUCCESS', 0);
define('LOYALTY_API_URL', env('LOYALTY_API_URL'));
define('PIOSPA_QUEUE_URL', env('PIOSPA_QUEUE_URL'));

define('END_POINT_PAGING', 10);

define('DOMAIN_PIOSPA', env('DOMAIN_PIOSPA'));

define('Logo', 'asd');

define('LIMIT_ITEM', 50);

define('FILTER_ITEM_PAGE', 10);



if (! function_exists('subString')) {
    function subString($value, $limit = 50, $end = '...')
    {
        return \Illuminate\Support\Str::limit($value, $limit, $end);
    }
}

if (!function_exists('getValueByLang')) {
    function getValueByLang($fieldName, $locale = null)
    {
        if (!$locale) $locale = App::getLocale();
        return $fieldName . $locale;
    }
}


