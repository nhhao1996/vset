```
TRUNCATE TABLE bank;
TRUNCATE TABLE branches;
TRUNCATE TABLE customer_appointments;
TRUNCATE TABLE customer_code;
TRUNCATE TABLE customer_contract;
TRUNCATE TABLE customer_contract_file;
TRUNCATE TABLE customer_contract_interest_by_date;
TRUNCATE TABLE customer_contract_interest_by_month;
TRUNCATE TABLE customer_contract_interest_by_time;
TRUNCATE TABLE customer_contract_serial;
TRUNCATE TABLE customer_device;
TRUNCATE TABLE customer_group_condition;
TRUNCATE TABLE customer_group_define_detail;
TRUNCATE TABLE customer_group_detail;
TRUNCATE TABLE customer_group_filter;
TRUNCATE TABLE customer_groups;
TRUNCATE TABLE customer_sms_history;
TRUNCATE TABLE customer_sources;
TRUNCATE TABLE customers;
TRUNCATE TABLE departments;
TRUNCATE TABLE email_config;
TRUNCATE TABLE email_log;
TRUNCATE TABLE email_otp_log;
TRUNCATE TABLE email_provider;
TRUNCATE TABLE email_templates;
TRUNCATE TABLE faq;
TRUNCATE TABLE faq_group;
TRUNCATE TABLE job_email;
TRUNCATE TABLE jobs;
TRUNCATE TABLE jobs_email_log;
TRUNCATE TABLE map_role_group_staff;
TRUNCATE TABLE migrations;
TRUNCATE TABLE notification;
TRUNCATE TABLE notification_detail;
TRUNCATE TABLE notification_queue;
TRUNCATE TABLE notification_template;
TRUNCATE TABLE order_commission;
TRUNCATE TABLE order_log;
TRUNCATE TABLE order_services;
TRUNCATE TABLE order_sources;
TRUNCATE TABLE orders;
TRUNCATE TABLE otp_log;
TRUNCATE TABLE page;
TRUNCATE TABLE page_banner;
TRUNCATE TABLE page_card;
TRUNCATE TABLE pages;
TRUNCATE TABLE payments;
TRUNCATE TABLE product_model;
TRUNCATE TABLE receipt_detail_image;
TRUNCATE TABLE receipt_details;
TRUNCATE TABLE receipts;
TRUNCATE TABLE reset_rank_log;
TRUNCATE TABLE role_actions;
TRUNCATE TABLE role_group;
TRUNCATE TABLE service_images;
TRUNCATE TABLE service_serial;
TRUNCATE TABLE sms_config;
TRUNCATE TABLE sms_log;
TRUNCATE TABLE sms_setting_brandname;
TRUNCATE TABLE sms_vendor_config;
TRUNCATE TABLE staff_title;
TRUNCATE TABLE staffs;
TRUNCATE TABLE staffs_forgot_pass;
TRUNCATE TABLE support_request;
TRUNCATE TABLE user_contacts;
TRUNCATE TABLE withdraw_request;
TRUNCATE TABLE withdraw_request_group;
DROP TABLE IF EXISTS `telescope_entries_tags`;
DROP TABLE IF EXISTS `telescope_monitoring`;
DROP TABLE IF EXISTS `telescope_entries`;
CREATE TABLE `telescope_entries` (
  `sequence` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT 1,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  KEY `telescope_entries_batch_id_index` (`batch_id`),
  KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`),
  KEY `telescope_entries_family_hash_index` (`family_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `telescope_entries_tags`;
CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  KEY `telescope_entries_tags_tag_index` (`tag`),
  CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `telescope_monitoring`;
CREATE TABLE `telescope_monitoring` (
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

TRUNCATE TABLE staffs;
INSERT INTO `staffs` (`staff_id`, `department_id`, `branch_id`, `staff_title_id`, `user_name`, `password`, `salt`, `full_name`, `birthday`, `gender`, `phone1`, `phone2`, `email`, `facebook`, `date_last_login`, `is_admin`, `is_actived`, `is_deleted`, `staff_avatar`, `address`, `created_by`, `updated_by`, `created_at`, `updated_at`, `remember_token`, `is_master`, `staff_code`) VALUES
(1,	1,	44,	1,	'admin',	'$2y$10$m6LtpQRwAMETXg7lvrzrhuah1F6T4MkbSIm/lj07L3XMJOIs1l5s.',	'900150983cd24fb0d6963f7d28e17f72',	'admin',	'1994-05-29 00:00:00',	'female',	'0967005205',	NULL,	'admin@gmail.com',	NULL,	NULL,	1,	1,	0,	'https://cms-traiphieu.vsetgroup.vn/uploads/admin/staff/20200904/2159918475904092020_staff.jpg',	'24 trường chinh , gò dầu tây ninh',	1,	1,	'2018-10-02 19:16:53',	'2020-09-04 11:59:28',	'ACwIGKsz2KC0RTUWIg54Gj3CnIKrgTsoryVAKiO6uYljBv5UtQ84bzH5A7hu',	0,	NULL);
```
