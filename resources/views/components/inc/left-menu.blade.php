<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light border-left-0">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="fix-scroll-bar m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative; height: 100%; overflow-y:auto">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow m-scroller">
            @php $tempCategoryHide=[]; @endphp
            @foreach((array)$key = session('key') as $k => $item)
                @php $counts=0; @endphp

                    @if(count($item['menu']) != 0)
                    <li class="m-menu__item m-menu__item--submenu " aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="{{$item['menu_category_id'] == 11 ? route($item['menu'][0]['admin_menu_route']) : 'javascript:;'}} " class="m-menu__link m-menu__toggle"><i class="{{$item['menu_category_icon']}} font-size-fix"></i><span class="m-menu__link-text">{{$item['menu_category_name']}}</span><i class="m-menu__ver-arrow la la-angle-right {{$item['menu_category_id'] == 11 ? 'd-none' : ''}}"></i></a>
                        <div class="m-menu__submenu " m-hidden-height="280" style="display: none; overflow: hidden;"><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                @foreach($item['menu'] as $menu)
                                    @if($menu['admin_menu_route'] != 'admin.stock-tutorial')
                                        @if($menu['admin_menu_id']!=1 && in_array($menu['admin_menu_route'],session('routeList')))
                                            @php $counts=1;@endphp
                                            <li class="m-menu__item pl-2  {{ Request::routeIs($menu['admin_menu_route']) ? 'active' : '' }}">
                                                <a href="{{route($menu['admin_menu_route'])}}" class="m-menu__link">
                                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                                    <span class="m-menu__link-text">@lang($menu['admin_menu_name'])</span>
                                                </a>
                                            </li>
                                        @else
                                            @if(Auth::user()->is_admin==0)
                                                @if(in_array($menu['admin_menu_route'],session('routeList')))
                                                    <li class="m-menu__item pl-2  {{ Request::routeIs($menu['admin_menu_route']) ? 'active' : '' }}">
                                                        <a href="{{route($menu['admin_menu_route'])}}" class="m-menu__link">
                                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                                            <span class="m-menu__link-text">@lang($menu['admin_menu_name'])</span>
                                                        </a>
                                                    </li>
                                                @endif
                                            @else
                                                <li class="m-menu__item pl-2  {{ Request::routeIs($menu['admin_menu_route']) ? 'active' : '' }}">
                                                    <a href="{{route($menu['admin_menu_route'])}}" class="m-menu__link">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                                        <span class="m-menu__link-text">@lang($menu['admin_menu_name'])</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @endif

            @endforeach

        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>
@foreach($tempCategoryHide as $key=>$value)
    {{--    @php var_dump($value); @endphp--}}
    <input type="hidden" value="{{$value}}" class="hidecate">
@endforeach
<script type="text/javascript">
    // var testimonials = document.querySelectorAll('.hidecate');
    // Array.prototype.forEach.call(testimonials, function (elements, index) {
    //     var aa = elements.value;
    //     document.getElementsByClassName(aa)[0].style.visibility = 'hidden';
    //
    //     const removeElements = (elms) => elms.forEach(el => el.remove());
    //
    //     removeElements(document.querySelectorAll("." + aa));
    // });
    // for (var i = 0; i < items.length; i++)
    //     arraytemp.push(items[i].name);
    // console.log(arraytemp);
    var appBanners = document.getElementsByClassName('hidecate');

    for (var i = 0; i < appBanners.length; i++) {
        // appBanners[i].style.display = 'none';
        const  elem = document.getElementsByClassName(appBanners[i].value);

        while (elem.length > 0 )
            elem[0].remove();
    }



</script>
<!-- END: Left Aside aa-->